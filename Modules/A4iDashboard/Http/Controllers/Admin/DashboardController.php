<?php

namespace Modules\A4iDashboard\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Mail;
use Modules\A4iDashboard\Models\User;
use Modules\A4iSeason\Models\Harvest;
use Modules\A4iSeason\Models\Season;
use Response;
use View;

class DashboardController extends Controller
{
    public function getIndex(Request $request)
    {
        $data['total_users_count'] = User::count();
        $data['today_users_count'] = User::whereDate('created_at', DB::raw('CURDATE()'))->count();
        $data['page_title'] = 'Trang thống kê ';
        $data['page_type'] = 'list';
        $data['seasons'] = $this->getDataSeason($request);
        $data['harvests'] = $this->getDataHarvests($request);
        if ($request->wantsJson() || $request->ajax()) {
            if ($request->type == 'seasons') {
                $data = $this->getDataSeason($request);
                return Response::json(View::make('a4idashboard::load_more_ajax', ['data' => $data, 'type' => 'season'])->render());
            } elseif ($request->type == 'harvests') {
                $data = $this->getDataHarvests($request);
                return Response::json(View::make('a4idashboard::load_more_ajax', ['data' => $data, 'type' => 'harvest'])->render());
            }
        }
        return view('a4idashboard::dashboard', $data);
    }

    function getDataSeason($request)
    {
        return Season::whereDate('expected_date', '<=', strftime("%Y-%m-%d", strtotime(date("d-m-Y", time()) . '+' . 1 . ' month ')))
            ->whereDate('expected_date', '>=', date("Y-m-d"))
            ->orderBy('expected_date', 'desc')->paginate(8);
    }

    function getDataHarvests($request)
    {
        return Harvest::where('implementation_date', '>', strftime("%Y-%m-%d", strtotime(date("d-m-Y", time()) . '-' . 1 . ' month')))
            ->where('implementation_date', '<=', date("Y-m-d"))
            ->orderBy('implementation_date', 'desc')->paginate(8);
    }

    public function tooltipInfo(Request $request)
    {
        $modal = new $request->modal;
        $data['item'] = $modal->find($request->id);
        $data['tooltip_info'] = $request->tooltip_info;

        return view('admin.common.modal.tooltip_info', $data);
    }


}
