<?php

namespace Modules\A4iDashboard\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bill extends Model
{
    use SoftDeletes;

    protected $table = 'bills';

    protected $fillable = [
        'auto_renew',
    ];

    protected $dates = ['deleted_at'];
    protected $softDelete = true;

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public  function service(){
        return $this->belongsTo(Service::class, 'service_id');
    }

}
