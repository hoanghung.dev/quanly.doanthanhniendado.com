<?php

namespace Modules\A4iDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $table = 'categories';

    protected $fillable = ['id',
        'seo_keywords','seo_description','seo_title','link','updated_at','intro_en','name_en','name_vi' , 'slug' , 'intro_vi' , 'image' , 'parent_id' , 'user_id' , 'status', 'type', 'order_no', 'created_at', 'show_menu', 'banner','featured','banner_sidebar','show_homepage',
    ];

    protected $appends = ['child_ids'];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    public  function product(){
        return $this->hasMany(Product::class,'category_id','id');
    }

    public function childs()
    {
        return $this->hasMany($this, 'parent_id', 'id')->orderBy('order_no', 'asc');
    }

    public function getChildIdsAttribute()
    {
        $id_arr = [];
        $childs = Category::select('id')->where('status', 1)->where('parent_id', @$this->attributes['id'])->get();
        foreach ($childs as $child) {
            $id_arr[] = $child->id;
            $childs2 = Category::select('id')->where('status', 1)->where('parent_id', $child->id)->get();
            foreach ($childs2 as $child2) {
                $id_arr[] = $child2->id;
            }
        }
        return $id_arr;
    }
}
