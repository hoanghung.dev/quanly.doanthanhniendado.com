<?php

namespace Modules\A4iDashboard\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\A4iLocation\Models\District;
use Modules\A4iLocation\Models\Province;
use Modules\A4iLocation\Models\Ward;

class Organization extends Model
{
    protected $table = 'organizations';
    protected $fillable = [
        'name', 'email', 'address', 'phone','image', 'province_id', 'district_id', 'ward_id'
    ];


    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }

    public function ward()
    {
        return $this->belongsTo(Ward::class, 'ward_id');
    }


}
