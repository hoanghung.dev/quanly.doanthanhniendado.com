@extends(config('core.admin_theme').'.template')
@section('main')
    <div class="content-wrapper">
        <div class="banner-top">
            <img src="{{ asset('public/filemanager/userfiles/' . @\App\Models\Setting::where('name', 'banner_dashboard_top')->first()->value) }}">
        </div>

        <section class="content" style=" margin: 10px; ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div style="display: flex" class="box-header with-border p-1">
                        <div class="ml-3">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24"
                                 version="1.1" class="kt-svg-icon kt-svg-icon--default kt-svg-icon--md">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                    <path d="M1.4152146,4.84010415 C11.1782334,10.3362599 14.7076452,16.4493804 12.0034499,23.1794656 C5.02500006,22.0396582 1.4955883,15.9265377 1.4152146,4.84010415 Z"
                                          fill="#000000" opacity="0.3"></path>
                                    <path d="M22.5950046,4.84010415 C12.8319858,10.3362599 9.30257403,16.4493804 12.0067693,23.1794656 C18.9852192,22.0396582 22.5146309,15.9265377 22.5950046,4.84010415 Z"
                                          fill="#000000" opacity="0.3"></path>
                                    <path d="M12.0002081,2 C6.29326368,11.6413199 6.29326368,18.7001435 12.0002081,23.1764706 C17.4738192,18.7001435 17.4738192,11.6413199 12.0002081,2 Z"
                                          fill="#000000" opacity="0.3"></path>
                                </g>
                            </svg>
                        </div>
                        <h3 class="kt-portlet__head-title ml-3 pt-1">Vụ mùa vừa thu hoạch</h3>
                    </div>
                    <a href="/admin/season?status=1&view=all" class="btn read-more">Xem tất cả >></a>
                </div>
            </div>
            <div class="row prd list-harvest">
                @foreach($harvests as $item)
                    @include('a4idashboard::partials.item_harvest')
                @endforeach
            </div>
            <div class="col-md-12 col-12 col-xs-12" style="display:flex;justify-content: center">
                @if($harvests->lastPage() > 1)
                    <span class="btn center btn-success btn-loadmore"
                          title="Click để xem thêm"
                          onclick="loadmore('harvests','.list-harvest','{{$harvests->lastPage()}}')">Xem thêm</span>
                @endif
            </div>
        </section>
        <script>
            var page_harvests = 2;
            var page_seasons = 2;

            function loadmore(type, list, per_page) {

                let page = (type == 'harvests') ? page_harvests : page_seasons;
                $.ajax({
                    type: "GET",
                    url: "?page=" + page + "&type=" + type,
                    cache: false,
                    success: function (data) {
                        $(list).append(data);
                        if (per_page == page) {
                            $(list).parent().find('.btn-loadmore').remove();
                        } else {
                            if (type == 'harvests') {
                                page_harvests = page_harvests + 1;
                            } else {
                                page_seasons = page_seasons + 1;
                            }
                        }

                    }
                });
            }
        </script>
        <section class="content" style=" margin: 10px; ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div style="display: flex" class="box-header with-border p-1">
                        <div class="ml-3">

                            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24"
                                 version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24"/>
                                    <path d="M1.4152146,4.84010415 C11.1782334,10.3362599 14.7076452,16.4493804 12.0034499,23.1794656 C5.02500006,22.0396582 1.4955883,15.9265377 1.4152146,4.84010415 Z"
                                          fill="#000000" opacity="0.3"/>
                                    <path d="M22.5950046,4.84010415 C12.8319858,10.3362599 9.30257403,16.4493804 12.0067693,23.1794656 C18.9852192,22.0396582 22.5146309,15.9265377 22.5950046,4.84010415 Z"
                                          fill="#000000" opacity="0.3"/>
                                    <path d="M12.0002081,2 C6.29326368,11.6413199 6.29326368,18.7001435 12.0002081,23.1764706 C17.4738192,18.7001435 17.4738192,11.6413199 12.0002081,2 Z"
                                          fill="#000000" opacity="0.3"/>
                                </g>
                            </svg>
                        </div>
                        <h3 class="kt-portlet__head-title ml-3">Vụ mùa sắp thu hoạch</h3>
                    </div>
                    <a href="/admin/season?status=1&view=all" class="btn read-more">Xem tất cả >></a>
                </div>
            </div>
            <div class="row prd list-seasons">
                @foreach($seasons as $item)
                    @include('a4idashboard::partials.item_season')
                @endforeach
            </div>
            <div class="col-md-12 col-12 col-xs-12" style="display:flex;justify-content: center">
                @if($seasons->lastPage() > 1)
                    <span class="btn btn-success btn-loadmore"
                          title="Click để xem thêm"
                          onclick="loadmore('seasons','.list-seasons','{{$seasons->lastPage()}}')">Xem thêm</span>
                @endif
            </div>
        </section>

    </div>
@endsection
@section('custom_head')

    <style type="text/css">


        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }

        .box.box-info img {
            max-width: 150px;
            max-height: 150px;
        }

    </style>

    <style>
        .banner-top {
            width: 100%;
            display: inline-block;
            max-height: 180px;
            overflow: hidden;
            margin-top: -25px;
        }

        .banner-top img {
            width: 100%;
            height: auto;
        }

        .btn-loadmore {
            cursor: pointer;
        }

        .title_prd {
            padding: 5px 5px 0 5px;
        }

        .page-numbers > li {
            padding: 5px 10px;
            border: 1px solid #ccc;
            margin: 0 5px;
        }

        .title_prd, .title_prd > a > h3, .content_prd {
            max-width: 100%;
        }

        .image_prd {
            width: 100%;
            height: 200px;
            overflow: hidden;
            display: inline-block;
        }

        .image_prd img {
            width: 100%;
        }

        .content_prd {
            background: #fff;
            padding-bottom: 30px;
            border: 1px solid #ccc;
            box-shadow: 2px 2px 2px 2px #cccccc4d;
        }

        .title_prd > a > h3 {
            font-size: 16px;
            color: #000;
            text-align: center;
        }

        .price_prd, .san_luong_prd {
            margin: 0 10px;
        }

        .price_prd {
            color: red;
        }

        .address_prd, .people_prd, .thu_hoach_prd {
            padding: 3px 10px;
        }

        .prd::-webkit-resizer {
            width: 3px;
        }

        .bg_prd {
            padding: 10px;
        }

        .box-header.with-border {
            border-bottom: 1px solid #f4f4f4;
        }

        .box-header {
            cursor: pointer;
        }

        .box-header {
            background-color: #f7fafa;
        }

        .box-header {
            color: #444;
            display: block;
            position: relative;
        }

        .price_prd {
            display: inline-block;
            float: left;
        }

        .san_luong_prd {
            display: inline-block;
            float: right;
        }

        .thu_hoach_prd, .address_prd {
            font-size: 12px;
        }

        .read-more {
            color: #0b57d5;
            padding: 5px 10px;
            margin-bottom: 10px;
            text-decoration: underline;
            position: absolute;
            right: 0;
            top: 5px;
        }

        .pagination .active {
            background: #1c2cdc9e;
            color: #fff;
            border: none !important;
            font-weight: 600;
        }

    </style>
@endsection
