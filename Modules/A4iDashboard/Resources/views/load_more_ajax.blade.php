@foreach($data as $item)
    @if($type == 'harvest')
        @include('a4idashboard::partials.item_harvest')
    @else
        @include('a4idashboard::partials.item_season')
    @endif
@endforeach
