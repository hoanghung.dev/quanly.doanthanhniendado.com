<?php

namespace Modules\A4iFertilizerWarehouse\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\EworkingAdmin\Models\Admin;
use Modules\A4iLand\Models\Land;
use Modules\A4iSeason\Models\Season;

class FertilizerWarehouse extends Model
{

    protected $table = 'fertilizer_warehouses';
    public $timestamps = false;
    protected $fillable = [
        'name', 'image', 'fertilizer_type', 'quantity', 'time'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
    public function land()
    {
        return $this->belongsTo(Land::class, 'land_id');
    }
    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }

}
