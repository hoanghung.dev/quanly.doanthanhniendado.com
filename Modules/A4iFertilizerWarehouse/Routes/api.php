<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::group(['prefix' => 'fertilizer_warehouses', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\FertilizerWarehouseController@index')->middleware('api_permission:fertilizer_warehouse_view');
        Route::post('', 'Admin\FertilizerWarehouseController@store')->middleware('api_permission:fertilizer_warehouse_add');
        Route::get('{id}', 'Admin\FertilizerWarehouseController@show')->middleware('api_permission:fertilizer_warehouse_view');
        Route::post('{id}', 'Admin\FertilizerWarehouseController@update')->middleware('api_permission:fertilizer_warehouse_edit');
        Route::delete('{id}', 'Admin\FertilizerWarehouseController@delete')->middleware('api_permission:fertilizer_warehouse_delete');
    });

});