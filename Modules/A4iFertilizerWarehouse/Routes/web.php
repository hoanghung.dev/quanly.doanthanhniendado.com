<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'fertilizer_warehouse'], function () {
        Route::get('', 'Admin\FertilizerWarehouseController@getIndex')->middleware('permission:fertilizer_warehouse_view');
        Route::get('publish', 'Admin\FertilizerWarehouseController@getPublish')->name('season.publish')->middleware('permission:fertilizer_warehouse_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\FertilizerWarehouseController@add')->middleware('permission:fertilizer_warehouse_add');
        Route::get('delete/{id}', 'Admin\FertilizerWarehouseController@delete')->middleware('permission:fertilizer_warehouse_delete');
        Route::post('multi-delete', 'Admin\FertilizerWarehouseController@multiDelete')->middleware('permission:fertilizer_warehouse_delete');
        Route::get('search-for-select2', 'Admin\FertilizerWarehouseController@searchForSelect2')->name('season.search_for_select2')->middleware('permission:fertilizer_warehouse_view');
        Route::get('{id}', 'Admin\FertilizerWarehouseController@update')->middleware('permission:fertilizer_warehouse_edit');
        Route::post('{id}', 'Admin\FertilizerWarehouseController@update')->middleware('permission:fertilizer_warehouse_edit');
    });
});