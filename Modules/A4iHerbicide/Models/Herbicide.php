<?php

namespace Modules\A4iHerbicide\Models;


use Illuminate\Database\Eloquent\Model;
use Modules\A4iLand\Models\Land;
use Modules\A4iSeason\Models\Season;
class Herbicide extends Model
{

    protected $table = 'herbicides';
    public $timestamps = false;
    protected $fillable = [
        'name', 'image', 'quantity', 'execution_time',
    ];


    public function land()
    {
        return $this->belongsTo(Land::class, 'land_id');
    }
    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }

}
