@if(in_array('data_view', $permissions))
    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i
                                class="kt-menu__link-icon flaticon-graphic"></i>
                    </span><span class="kt-menu__link-text">Dữ liệu</span><i
                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                    class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/herbicide" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Thuốc diệt cỏ</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/fertilizer_warehouse" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Kho phân bón</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/medicine_warehouse" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Thuốc bảo vệ thực vật</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/trace_element" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Nguyên tố vi lượng</span></a></li>
            </ul>
        </div>
    </li>
@endif