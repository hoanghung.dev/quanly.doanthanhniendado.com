<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'herbicide'], function () {
        Route::get('', 'Admin\HerbicideController@getIndex')->middleware('permission:herbicide_view');
        Route::get('publish', 'Admin\HerbicideController@getPublish')->name('season.publish')->middleware('permission:herbicide_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\HerbicideController@add')->middleware('permission:herbicide_add');
        Route::get('delete/{id}', 'Admin\HerbicideController@delete')->middleware('permission:herbicide_delete');
        Route::post('multi-delete', 'Admin\HerbicideController@multiDelete')->middleware('permission:herbicide_delete');
        Route::get('search-for-select2', 'Admin\HerbicideController@searchForSelect2')->name('season.search_for_select2')->middleware('permission:herbicide_view');
        Route::get('{id}', 'Admin\HerbicideController@update')->middleware('permission:herbicide_edit');
        Route::post('{id}', 'Admin\HerbicideController@update')->middleware('permission:herbicide_edit');
    });
});