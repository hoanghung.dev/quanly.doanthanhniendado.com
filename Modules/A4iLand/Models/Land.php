<?php

namespace Modules\A4iLand\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\A4iLocation\Models\District;
use Modules\A4iLocation\Models\Ward;
use Modules\A4iLocation\Models\Province;
use Modules\A4iSeason\Models\Season;
use App\Models\Admin;

class Land extends Model
{
    protected $table = 'lands';

    protected $fillable = [
        'name', 'image', 'address', 'admin_id', 'province_id', 'district_id', 'ward_id', 'acreage',
        'type_owneds', 'status', 'intro', 'image_extra', 'shape', 'land_type', 'land_parameters', 'water_parameters', 'lat', 'long', 'link_gg_map',
        'water_parameters_image', 'land_parameters_image'
    ];

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }

    public function ward()
    {
        return $this->belongsTo(Ward::class, 'ward_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

    public function season()
    {
        return $this->hasOne(Season::class, 'land_id');
    }

    public function seasoning()
    {
        return $this->hasOne(Season::class, 'land_id')->where('status', 1);
    }


}
