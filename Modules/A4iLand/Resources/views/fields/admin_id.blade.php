<div class="form-group" id="form-group-{{ $field['name'] }}">
    <label for="{{ $field['name'] }}" class="col-xs-12 control-label">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)<span class="color_btd">*</span>@endif</label>
    <div class="col-xs-12">
        @if (isset($result))
            <input type="text" value="{{ @$result->admin->name }} - {{ @$result->admin->email }}" class="form-control" disabled>
        @else
            <input type="text" value="{{ \Auth::guard('admin')->user()->name }} - {{ \Auth::guard('admin')->user()->email }}" class="form-control" disabled>
        @endif
    </div>
</div>