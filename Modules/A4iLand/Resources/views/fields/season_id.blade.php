<div class="form-group" id="form-group-{{ $field['name'] }}">
    <label for="{{ $field['name'] }}" class="col-xs-12 control-label">{{ @$field['label'] }} : <a class="btn-link" href="/admin/edit_season/{{ @$result->season->id}}">{{ @$result->season->tree_name }}</a></label>
    <br>
    <div class="col-xs-12">
        <a class="btn-link" href="/admin/season">Xem tất cả mùa vụ</a>
    </div>
</div>