@if(isset($item->ward->name))<a
        href="/admin/land?search=true&view=all&ward_id={{ @$item->ward->id }}">{{ @$item->ward->name }}</a>, @endif
@if(isset($item->district->name))<a
        href="/admin/land?search=true&view=all&district_id={{ @$item->district->id }}">{{ @$item->district->name }}</a>, @endif
@if(isset($item->province->name))<a
        href="/admin/land?search=true&view=all&province_id={{ @$item->province->id }}">{{ @$item->province->name }}</a>@endif