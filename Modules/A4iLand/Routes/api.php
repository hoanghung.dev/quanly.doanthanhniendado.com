<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::group(['prefix' => 'lands', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\LandController@index')->middleware('api_permission:land_view');
        Route::post('', 'Admin\LandController@store')->middleware('api_permission:land_add');
        Route::get('{id}', 'Admin\LandController@show')->middleware('api_permission:land_view');
        Route::post('{id}', 'Admin\LandController@update')->middleware('api_permission:land_edit');
        Route::delete('{id}', 'Admin\LandController@delete')->middleware('api_permission:land_delete');
    });

    Route::group(['prefix' => 'trace_elements', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\TraceElementController@index');
    });
});