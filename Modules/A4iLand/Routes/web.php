<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'land'], function () {
        Route::get('', 'Admin\LandController@getIndex')->name('land')->middleware('permission:land_view');
        Route::get('publish', 'Admin\LandController@getPublish')->name('land.publish')->middleware('permission:land_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\LandController@add')->middleware('permission:land_add');
        Route::get('delete/{id}', 'Admin\LandController@delete')->middleware('permission:land_delete');
        Route::post('multi-delete', 'Admin\LandController@multiDelete')->middleware('permission:land_delete');
        Route::get('search-for-select2', 'Admin\LandController@searchForSelect2')->name('land.search_for_select2')->middleware('permission:land_view');
        Route::get('{id}', 'Admin\LandController@update')->middleware('permission:land_view');
        Route::post('{id}', 'Admin\LandController@update')->middleware('permission:land_edit');
    });
});
