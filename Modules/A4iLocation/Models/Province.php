<?php

namespace Modules\A4iLocation\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
	protected $table = 'provinces';

    public $timestamps = false;
}
