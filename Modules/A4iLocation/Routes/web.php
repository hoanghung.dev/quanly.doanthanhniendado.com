<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'province'], function () {
        Route::get('', 'Admin\ProvinceController@getIndex')->name('land')->middleware('permission:province_view');
        Route::get('publish', 'Admin\ProvinceController@getPublish')->name('province.publish')->middleware('permission:province_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ProvinceController@add')->middleware('permission:province_add');
        Route::get('delete/{id}', 'Admin\ProvinceController@delete')->middleware('permission:province_delete');
        Route::post('multi-delete', 'Admin\ProvinceController@multiDelete')->middleware('permission:province_delete');
        Route::get('search-for-select2', 'Admin\ProvinceController@searchForSelect2')->name('province.search_for_select2');
        Route::get('{id}', 'Admin\ProvinceController@update')->middleware('permission:province_view');
        Route::post('{id}', 'Admin\ProvinceController@update')->middleware('permission:province_edit');
    });


    Route::group(['prefix' => 'district'], function () {
        Route::get('', 'Admin\DistrictController@getIndex')->name('land')->middleware('permission:district_view');
        Route::get('publish', 'Admin\DistrictController@getPublish')->name('district.publish')->middleware('permission:district_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\DistrictController@add')->middleware('permission:district_add');
        Route::get('delete/{id}', 'Admin\DistrictController@delete')->middleware('permission:district_delete');
        Route::post('multi-delete', 'Admin\DistrictController@multiDelete')->middleware('permission:district_delete');
        Route::get('search-for-select2', 'Admin\DistrictController@searchForSelect2')->name('district.search_for_select2');
        Route::get('{id}', 'Admin\DistrictController@update')->middleware('permission:district_view');
        Route::post('{id}', 'Admin\DistrictController@update')->middleware('permission:district_edit');
    });

    Route::group(['prefix' => 'ward'], function () {
        Route::get('', 'Admin\WardController@getIndex')->name('land')->middleware('permission:ward_view');
        Route::get('publish', 'Admin\WardController@getPublish')->name('ward.publish')->middleware('permission:ward_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\WardController@add')->middleware('permission:ward_add');
        Route::get('delete/{id}', 'Admin\WardController@delete')->middleware('permission:ward_delete');
        Route::post('multi-delete', 'Admin\WardController@multiDelete')->middleware('permission:ward_delete');
        Route::get('search-for-select2', 'Admin\WardController@searchForSelect2')->name('ward.search_for_select2');
        Route::get('{id}', 'Admin\WardController@update')->middleware('permission:ward_view');
        Route::post('{id}', 'Admin\WardController@update')->middleware('permission:ward_edit');
    });
});