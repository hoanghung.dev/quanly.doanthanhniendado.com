<?php

namespace Modules\A4iMedicineWarehouse\Http\Controllers\Api\Admin;

use Modules\A4iDrainage\Models\Drainage;
use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\A4iFertilizerWarehouse\Models\FertilizerWarehouse;
use Modules\A4iMedicineWarehouse\Models\MedicineWarehouse;
use Modules\A4iSeason\Models\Fertilizer;
use Validator;

class MedicineWarehouseController extends Controller
{

    protected $module = [
        'code' => 'medicine_warehouse',
        'table_name' => 'medicine_warehouses',
        'label' => 'Kho thuốc bảo vệ thực vật',
        'modal' => 'Modules\A4iMedicineWarehouse\Models\MedicineWarehouse',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'admin_id' => [
            'query_type' => '='
        ],
        'land_id' => [
            'query_type' => '='
        ],
        'season_id' => [
            'query_type' => '='
        ]
    ];

    protected $method_fertilizer = [
        1 => 'Hạt',
        2 => 'Dung dịch',
    ];

    public function index(Request $request)
    {

        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = MedicineWarehouse::selectRaw('medicine_warehouses.id, medicine_warehouses.name, medicine_warehouses.quantity, medicine_warehouses.execution_time,medicine_warehouses.image')
                ->whereRaw($where);


            //  Sort
            $listItem = $this->sort($request, $listItem);

            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());
            foreach ($listItem as $k => $item) {
                $item->image = $item->image != null ? asset('public/filemanager/userfiles/' . $item->image) : '';

            }


            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {
            $item = Fertilizer::leftJoin('seasons', 'seasons.id', '=', 'fertilizers.season_id')
                ->leftJoin('fertilizer_warehouses', 'fertilizer_warehouses.id', '=', 'fertilizers.fertilizer_warehouse_id')
                ->selectRaw('fertilizers.id, fertilizers.quantity, fertilizers.time, fertilizers.image, fertilizers.fertilizer_warehouse_id, fertilizer_warehouses.name')
                ->where('fertilizers.id', $id)->first();
            $item->image = $item->image != null ? asset('public/filemanager/userfiles/' . $item->image) : '';
            $item->fertilizer_warehouse = [
                'id' => $item->fertilizer_warehouse_id,
                'name' => $item->name,
            ];
            unset($item->fertilizer_warehouse_id);
            unset($item->name);

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }


            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {


        $data = $request->all();
        //  Tùy chỉnh dữ liệu insert
        $data['admin_id'] = \Auth::guard('api')->id();
        if ($request->has('image')) {
            if (is_array($request->file('image'))) {
                foreach ($request->file('image') as $image) {
                    $data['image'] = CommonHelper::saveFile($image, 'fertilizer');
                }
            } else {
                $data['image'] = CommonHelper::saveFile($request->file('image'), 'fertilizer');
            }
        }

        $item = Fertilizer::create($data);

        return $this->show($item->id);

    }


    public function update(Request $request, $id)
    {

        $item = Fertilizer::find($id);
        if (!is_object($item)) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }

        $data = $request->except('api_token');
        //  Tùy chỉnh dữ liệu insert

        if ($request->has('image')) {
            if (is_array($request->file('image'))) {
                foreach ($request->file('image') as $image) {
                    $data['image'] = CommonHelper::saveFile($image, 'fertilizer');
                }
            } else {
                $data['image'] = CommonHelper::saveFile($request->file('image'), 'fertilizer');
            }
        }

        foreach ($data as $k => $v) {
            $item->{$k} = $v;
        }
        $item->save();

        return $this->show($item->id);
    }


    public function delete($id)
    {
        if (Fertilizer::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
