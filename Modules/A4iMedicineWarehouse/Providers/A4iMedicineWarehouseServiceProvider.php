<?php

namespace Modules\A4iMedicineWarehouse\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class A4iMedicineWarehouseServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //Đăng ký quyền
            $this->registerPermission();
        }
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['medicine_warehouse_view', 'medicine_warehouse_add', 'medicine_warehouse_edit', 'medicine_warehouse_delete', 'medicine_warehouse_publish',]);
            return $per_check;
        }, 1, 1);
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['data_view']);
            return $per_check;
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('a4imedicinewarehouse.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php', 'a4imedicinewarehouse'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/a4imedicinewarehouse');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/a4imedicinewarehouse';
        }, \Config::get('view.paths')), [$sourcePath]), 'a4imedicinewarehouse');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/a4imedicinewarehouse');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'a4imedicinewarehouse');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'a4imedicinewarehouse');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
