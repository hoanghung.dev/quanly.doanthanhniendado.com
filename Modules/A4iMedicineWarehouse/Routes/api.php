<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::group(['prefix' => 'medicine_warehouses', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\MedicineWarehouseController@index')->middleware('api_permission:medicine_warehouse_view');
        Route::get('{id}', 'Admin\MedicineWarehouseController@show')->middleware('api_permission:medicine_warehouse_view');
        Route::post('', 'Admin\MedicineWarehouseController@store')->middleware('api_permission:medicine_warehouse_add');
        Route::post('{id}', 'Admin\MedicineWarehouseController@update')->middleware('api_permission:medicine_warehouse_edit');
        Route::delete('{id}', 'Admin\MedicineWarehouseController@delete')->middleware('api_permission:medicine_warehouse_delete');
    });

});