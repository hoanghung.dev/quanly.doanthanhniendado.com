<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'medicine_warehouse'], function () {
        Route::get('', 'Admin\MedicineWarehouseController@getIndex')->middleware('permission:medicine_warehouse_view');
        Route::get('publish', 'Admin\MedicineWarehouseController@getPublish')->name('season.publish')->middleware('permission:medicine_warehouse_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\MedicineWarehouseController@add')->middleware('permission:medicine_warehouse_add');
        Route::get('delete/{id}', 'Admin\MedicineWarehouseController@delete')->middleware('permission:medicine_warehouse_delete');
        Route::post('multi-delete', 'Admin\MedicineWarehouseController@multiDelete')->middleware('permission:medicine_warehouse_delete');
        Route::get('search-for-select2', 'Admin\MedicineWarehouseController@searchForSelect2')->name('season.search_for_select2')->middleware('permission:medicine_warehouse_view');
        Route::get('{id}', 'Admin\MedicineWarehouseController@update')->middleware('permission:medicine_warehouse_edit');
        Route::post('{id}', 'Admin\MedicineWarehouseController@update')->middleware('permission:medicine_warehouse_edit');
    });
});