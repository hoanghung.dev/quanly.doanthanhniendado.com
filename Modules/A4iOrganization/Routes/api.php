<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::group(['prefix' => 'organizations', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\OrganizationController@index');
        Route::get('{id}', 'Admin\OrganizationController@show');
        Route::post('', 'Admin\OrganizationController@store');
        Route::post('{id}', 'Admin\OrganizationController@update');
        Route::delete('{id}', 'Admin\OrganizationController@delete');
    });
});