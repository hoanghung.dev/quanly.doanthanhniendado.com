<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'organization'], function () {
        Route::get('', 'Admin\OrganizationController@getIndex')->name('organization')->middleware('permission:organization_view');
        Route::get('publish', 'Admin\OrganizationController@getPublish')->name('organization.publish')->middleware('permission:organization_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\OrganizationController@add')->middleware('permission:organization_add');
        Route::get('delete/{id}', 'Admin\OrganizationController@delete')->middleware('permission:organization_delete');
        Route::post('multi-delete', 'Admin\OrganizationController@multiDelete')->middleware('permission:organization_delete');
        Route::get('search-for-select2', 'Admin\OrganizationController@searchForSelect2')->name('organization.search_for_select2')->middleware('permission:organization_view');
        Route::get('{id}', 'Admin\OrganizationController@update')->middleware('permission:organization_view');
        Route::post('{id}', 'Admin\OrganizationController@update')->middleware('permission:organization_edit');
    });
});