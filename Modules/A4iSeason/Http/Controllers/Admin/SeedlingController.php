<?php

namespace Modules\A4iSeason\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Validator;
use Illuminate\Http\Request;
use Auth;

class SeedlingController extends CURDBaseController
{

    protected $module = [
        'code' => 'seedling',
        'table_name' => 'seeds',
        'label' => 'Giống',
        'modal' => '\Modules\A4iSeason\Models\Seed',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'class' => '', 'label' => 'Hình ảnh'],
            ['name' => 'name_supplier', 'type' => 'text', 'class' => '', 'label' => 'Tên cơ sở bán'],
            ['name' => 'category_seeds', 'type' => 'select', 'options' =>
                [
                    1 => 'Hạt giống',
                    2 => 'Cây nuôi cấy mô',
                    3 => 'Cây triết',
                    4 => 'Cây ghép',
                ], 'class' => '', 'label' => 'Loại giống'],
            ['name' => 'address', 'type' => 'text', 'class' => '', 'label' => 'Địa chỉ'],
            ['name' => 'email', 'type' => 'text', 'class' => '', 'label' => 'Email'],
            ['name' => 'tel', 'type' => 'text', 'class' => '', 'label' => 'Điện thoại'],
            ['name' => 'website', 'type' => 'text', 'class' => '', 'label' => 'Trang web'],
            ['name' => 'action', 'type' => 'custom', 'td' => 'a4iseason::list.td.action', 'class' => '', 'label' => '#'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'category_seeds', 'type' => 'select', 'options' =>
                    [
                        1 => 'Hạt giống',
                        2 => 'Cây nuôi cấy mô',
                        3 => 'Cây triết',
                        4 => 'Cây ghép',
                    ], 'class' => '', 'label' => 'Loại giống'],
                ['name' => 'implementation_date', 'type' => 'date', 'class' => '', 'label' => 'Ngày thực hiện', 'object' => 'details'],
                ['name' => 'image', 'type' => 'file_image', 'class' => '', 'label' => 'Hình ảnh'],
                ['name' => 'multiple_image', 'type' => 'multiple_image_dropzone', 'class' => '', 'label' => 'Ảnh thêm', 'count' => 6],
                ['name' => 'cost', 'type' => 'text', 'class' => 'number_price', 'label' => 'Chi phí', 'group_class' => 'col-xs-6 col-md-6'],
                ['name' => 'amount', 'type' => 'number', 'class' => '', 'label' => 'Số lượng', 'group_class' => 'col-xs-6 col-md-6'],
            ],
            'nha_cung_cap' => [
                ['name' => 'name_supplier', 'type' => 'text', 'class' => '', 'label' => 'Tên'],
                ['name' => 'address', 'type' => 'text', 'class' => '', 'label' => 'Địa chỉ'],
                ['name' => 'province_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Tỉnh/Thành', 'model' => \Modules\A4iLocation\Models\Province::class, 'display_field' => 'name',],
                ['name' => 'district_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Quận/Huyện', 'model' => \Modules\A4iLocation\Models\District::class, 'display_field' => 'name',],
                ['name' => 'ward_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Phường/Xã', 'model' => \Modules\A4iLocation\Models\Ward::class, 'display_field' => 'name',],
                ['name' => 'email', 'type' => 'text', 'class' => '', 'label' => 'Email'],
                ['name' => 'tel', 'type' => 'number', 'class' => '', 'label' => 'Điện thoại'],
                ['name' => 'website', 'type' => 'text', 'class' => '', 'label' => 'Trang web']
            ]
        ],
    ];

    protected $filter = [
        'category_seeds' => [
            'label' => 'Loại giống',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Chọn loại giống',
                1 => 'Hạt giống',
                2 => 'Cây nuôi cấy mô',
                3 => 'Cây triết',
                4 => 'Cây ghép',
            ]
        ],
        'name_supplier' => [
            'label' => 'Tên nhà cung cấp',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'email' => [
            'label' => 'Email',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'website' => [
            'label' => 'Website',
            'type' => 'text',
            'query_type' => 'like'
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('a4iseason::childs.list')->with($data);

    }

    public function appendWhere($query, $request)
    {
        $query = $query->where('season_id', $request->season_id);
        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);


                return view('a4iseason::childs.add')->with($data);

            } else if ($_POST)
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('multiple_image')) {
                        $data['multiple_image'] = implode('|', $request->multiple_image);
                    }
                    $data['season_id'] = $request->season_id;
                    $data['admin_id'] = \Auth::guard('admin')->id();
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    $url = '';
                    if ($request->has('season_id')) {
                        $url .= '?season_id=' . $request->season_id;
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . $url);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add' . $url);
                    }

                    return redirect('admin/' . $this->module['code'] . $url);


        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();

        }
    }


    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('a4iseason::childs.edit')->with($data);
            } else if ($_POST) {


                $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                //  Tùy chỉnh dữ liệu insert
                if ($request->has('multiple_image')) {
                    $data['multiple_image'] = implode('|', $request->multiple_image);
                }
                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                $url = '';
                if ($request->has('season_id')) {
                    $url .= '?season_id=' . $request->season_id;
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id . $url);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add' . $url);
                }

                return redirect('admin/' . $this->module['code'] . $url);
            }

        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xuất bản!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);
            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            $url = '';
            if ($request->has('season_id')) {
                $url .= '?season_id=' . $request->season_id;
            }
            return redirect('admin/' . $this->module['code'] . $url);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
}
