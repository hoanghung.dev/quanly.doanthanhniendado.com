<?php

namespace Modules\A4iSeason\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Modules\A4iLocation\Models\Province;
use Validator;
use Illuminate\Http\Request;
use Auth;

class WeatherInformationController extends CURDBaseController
{

    protected $module = [
        'code' => 'weather_information',
        'table_name' => 'weather_informations',
        'label' => 'Thông tin thời tiết',
        'modal' => '\Modules\A4iSeason\Models\WeatherInformation',
        'list' => [
            ['name' => 'warning_from', 'type' => 'select', 'options' =>
                [
                    1 => 'Cảm biến',
                    2 => 'Hệ thống IoT',
                    3 => 'Khác',
                ], 'class' => '', 'label' => 'Cảnh báo từ'],
            ['name' => 'humidity', 'type' => 'text', 'class' => '', 'label' => 'Độ ẩm'],
            ['name' => 'wind_speed', 'type' => 'text', 'class' => '', 'label' => 'Tốc độ gió'],
            ['name' => 'amount_rain', 'type' => 'text', 'class' => '', 'label' => 'Lượng mưa'],
            ['name' => 'wind_direction', 'type' => 'text', 'class' => '', 'label' => 'Hướng gió'],
            ['name' => 'temperature', 'type' => 'text', 'class' => '', 'label' => 'Nhiệt độ'],
            ['name' => 'implementation_date', 'type' => 'date_vi', 'class' => '', 'label' => 'Ngày thực hiện'],
            ['name' => 'action', 'type' => 'custom', 'td' => 'a4iseason::list.td.action', 'class' => '', 'label' => '#'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'humidity', 'type' => 'text', 'class' => '', 'label' => 'Độ ẩm', 'group_class' => 'col-xs-6 col-md-6'],
                ['name' => 'wind_speed', 'type' => 'text', 'class' => '', 'label' => 'Tốc độ gió', 'group_class' => 'col-xs-6 col-md-6'],
                ['name' => 'amount_rain', 'type' => 'text', 'class' => '', 'label' => 'Lượng mưa', 'group_class' => 'col-xs-6 col-md-6'],
                ['name' => 'wind_direction', 'type' => 'text', 'class' => '', 'label' => 'Hướng gió', 'group_class' => 'col-xs-6 col-md-6'],
                ['name' => 'CO2', 'type' => 'text', 'class' => '', 'label' => 'CO2', 'group_class' => 'col-xs-6 col-md-6'],
                ['name' => 'temperature', 'type' => 'text', 'class' => '', 'label' => 'Nhiệt độ', 'group_class' => 'col-xs-6 col-md-6'],
                ['name' => 'natural_disaster', 'type' => 'select', 'options' =>
                    [
                        1 => 'Bão',
                        2 => 'Lũ lụt',
                        3 => 'Hạn hán',
                    ], 'class' => '', 'label' => 'Thiên tai', 'group_class' => 'col-xs-6 col-md-6'],
                ['name' => 'implementation_date', 'type' => 'date_vi', 'class' => '', 'label' => 'Ngày thực hiện', 'group_class' => 'col-xs-6 col-md-6'],

            ],
            'location_tab' => [
                ['name' => 'warning_from', 'type' => 'select', 'options' =>
                    [
                        1 => 'Cảm biến',
                        2 => 'Hệ thống IoT',
                        3 => 'Khác',

                    ], 'class' => '', 'label' => 'Cảnh báo từ'],

            ]
        ],
    ];

    protected $filter = [
        'warning_from' => [
            'label' => 'Cảnh báo từ',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Cảnh báo từ',
                1 => 'Cảm biến',
                2 => 'Hệ thống IoT',
                3 => 'Khác',
            ]
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);
        return view('a4iseason::childs.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        $query = $query->where('season_id', $request->season_id);
        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('a4iseason::childs.add')->with($data);

            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
//                    'category_seeds' => 'required',
//                    'name_supplier' => 'required',
//                    'email' => 'required'
                ], [
//                    'category_seeds.required' => 'Bắt buộc phải chọn loại giống',
//                    'name_supplier.required' => 'Bắt buộc phải nhập tên nhà cung cấp',
//                    'email.required' => 'Bắt buộc phải nhập email',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    $data['season_id'] = $request->season_id;
                    $data['admin_id'] = \Auth::guard('admin')->id();
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {


                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    $url = '';
                    if ($request->has('season_id')) {
                        $url .= '?season_id=' . $request->season_id;
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . $url);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add' . $url);
                    }

                    return redirect('admin/' . $this->module['code'] . $url);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('a4iseason::childs.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
//                    'category_seeds' => 'required',
//                    'name_supplier' => 'required',
//                    'email' => 'required'
                ], [
//                    'category_seeds.required' => 'Bắt buộc phải chọn loại giống',
//                    'name_supplier.required' => 'Bắt buộc phải nhập tên nhà cung cấp',
//                    'email.required' => 'Bắt buộc phải nhập email',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    $url = '';
                    if ($request->has('season_id')) {
                        $url .= '?season_id=' . $request->season_id;
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id . $url);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add' . $url);
                    }

                    return redirect('admin/' . $this->module['code'] . $url);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xuất bản!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            $url = '';
            if ($request->has('season_id')) {
                $url .= '?season_id=' . $request->season_id;
            }

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code'] . $url);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
}
