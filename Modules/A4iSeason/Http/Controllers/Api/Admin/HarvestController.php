<?php

namespace Modules\A4iSeason\Http\Controllers\Api\Admin;

use Modules\A4iSeason\Http\Helpers\SeasonHelper;
use Modules\A4iSeason\Models\{Disease,
    Drainage,
    Season,
    Seed,
    PlantMedicine,
    Nutrition,
    TechnicalCare,
    InvestmentMaterial};
use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\A4iSeason\Models\Harvest;
use ParagonIE\Sodium\Core\Curve25519\H;
use Validator;

class HarvestController extends Controller
{

    protected $module = [
        'code' => 'harvest',
        'table_name' => 'harvests',
        'label' => 'Lịch sử thu hoạch',
        'modal' => 'Modules\A4iSeason\Models\Harvest',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'admin_id' => [
            'query_type' => '='
        ],
        'land_id' => [
            'query_type' => '='
        ],
        'season_id' => [
            'query_type' => '='
        ]
    ];
    protected $method_hervest = [
        1 => 'Chuyển sang trồng cây khác',
        2 => 'Trồng tiếp nhưng theo chu kỳ mới',
        3 => 'Cho đất nghỉ, để ải',
    ];
    protected $price_unit_type = [
        '/kg' => '/kg',
        '/tấn' => '/tấn',
    ];
    protected $quantity_unit_type = [
        'kg' => 'kg',
        'tấn' => 'tấn',
    ];

    public function index(Request $request)
    {

        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Harvest::leftJoin('seasons', 'seasons.id', '=', 'harvests.season_id')
                ->join('lands', 'lands.id', '=', 'harvests.land_id')
                ->selectRaw('harvests.id,
                 harvests.quantity,
                 harvests.productivity,
                 harvests.method,
                  harvests.price,
                  harvests.implementation_date,
                  harvests.image,
                   lands.id as land_id,
                 lands.acreage as lands_acreage')
                ->whereRaw($where);


            //  Sort
            $listItem = $this->sort($request, $listItem);

            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());

            foreach ($listItem as $k => $v) {
                $v->method = array_key_exists($v->method, $this->method_hervest) ? $this->method_hervest[$v->method] : '';
                $v->image = asset('public/filemanager/userfiles/' . $v->image);
                $v->land_acreage = [
                    'land_id' => $v->land_id,
                    'acreage' => $v->lands_acreage,
                ];
                unset($v->land_id);
                unset($v->lands_acreage);
            }


            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {

            $item = Harvest::leftJoin('seasons', 'seasons.id', '=', 'harvests.season_id')
                ->join('lands', 'lands.id', '=', 'harvests.land_id')
                ->selectRaw('harvests.id,
                 harvests.quantity,
                 harvests.productivity,
                 harvests.method,
                  harvests.price,
                  harvests.implementation_date,
                  harvests.image,
                  harvests.lands_acreage,
                  harvests.quantity_unit,
                  harvests.price_unit,
                  harvests.season_id,
                 lands.acreage as lands_acreage')->where('harvests.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }
            $item->price_unit_optons = $this->price_unit_type;
            $item->quantity_unit_optons = $this->quantity_unit_type;
            $item->total_revenue = SeasonHelper::getPriceHarvest($item);
            $item->total_cost = SeasonHelper::getCostSeason($item->season_id,$item);
            $item->image = asset('public/filemanager/userfiles/' . $item->image);
            foreach (explode('|', $item->image_extra) as $img) {
                if ($img != '') {
                    $image_extra[] = asset('public/filemanager/userfiles/' . $img);
                }
            }
            $item->method = array_key_exists($item->method, $this->method_hervest) ? $this->method_hervest[$item->method] : '';
            unset($item->season_id);



            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], [
            'name.required' => 'Bắt buộc phải nhập tên',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('api')->id();

            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = CommonHelper::saveFile($image, 'harvest');
                    }
                } else {
                    $data['image'] = CommonHelper::saveFile($request->file('image'), 'harvest');
                }
            }
            $item = Harvest::create($data);
            $season = Season::find($item->season_id);
            if (in_array($request->get('method'), ['1', '3'])) {   //  Dừng mùa vụ
                $season->status = 0;
                $season->save();
            } else {
                $season->status = 1;
                $season->save();
            }
            return $this->show($item->id);
        }
    }


    public function update(Request $request, $id)
    {

        $item = Harvest::find($id);
        if (!is_object($item)) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }

        $data = $request->except('api_token');
        //  Tùy chỉnh dữ liệu insert

        if ($request->has('image')) {
            if (is_array($request->file('image'))) {
                foreach ($request->file('image') as $image) {
                    $data['image'] = CommonHelper::saveFile($image, 'harvest');
                }
            } else {
                $data['image'] = CommonHelper::saveFile($request->file('image'), 'harvest');
            }
        }

        foreach ($data as $k => $v) {
            $item->{$k} = $v;
        }
        $item->save();


        return $this->show($item->id);
    }


    public function delete($id)
    {
        if (Harvest::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }


    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
