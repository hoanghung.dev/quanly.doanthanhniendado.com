<?php

namespace Modules\A4iSeason\Http\Controllers\Api\Admin;

use \Modules\A4iSeason\Models\Disease;
use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\A4iSeason\Models\InvestmentMaterial;
use Modules\A4iSeason\Models\PlantMedicine;
use Validator;

class InvestmentMaterialController extends Controller
{

    protected $module = [
        'code' => 'investment_material',
        'table_name' => 'investment_materials',
        'label' => 'Nguyên liệu đầu vào',
        'modal' => 'Modules\A4iSeason\Models\InvestmentMaterial',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'admin_id' => [
            'query_type' => '='
        ],
        'land_id' => [
            'query_type' => '='
        ],
        'season_id' => [
            'query_type' => '='
        ]
    ];


    public function index(Request $request)
    {

        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = InvestmentMaterial::selectRaw('investment_materials.id,
            investment_materials.name,
             investment_materials.fertilizer_id,
              investment_materials.plant_medicine_id,
               investment_materials.herbicide_id,
                investment_materials.amount,
                 investment_materials.time,
                  investment_materials.image_seedling,
                   investment_materials.image_bill,
                   investment_materials.facility_name,
                   investment_materials.tel,
                   investment_materials.email,
                   investment_materials.cost,
                   investment_materials.province_id,
                   investment_materials.district_id,
                   investment_materials.ward_id,
                   investment_materials.village')
                ->whereRaw($where);


            //  Sort
            $listItem = $this->sort($request, $listItem);

            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());
            foreach ($listItem as $k => $item) {
                $item->image_seedling = asset('public/filemanager/userfiles/' . $item->image_seedling);
                $item->province = [
                    'id' => @$item->province->id,
                    'name' => @$item->provinces->name,
                ];
                $item->district = [
                    'id' => @$item->district->id,
                    'name' => @$item->districts->name,
                ];
                $item->ward = [
                    'id' => @$item->ward->id,
                    'name' => @$item->wards->name,
                ];


                $item->fertilizer = [
                    'id' => @$item->fertilizers->id,
                    'name' => @$item->fertilizers->name,
                ];
                $item->plant_medicine = [
                    'id' => @$item->plant_medicines->id,
                    'name' => @$item->plant_medicines->name,
                ];
                $item->herbicide = [
                    'id' => @$item->herbicides->id,
                    'name' => @$item->herbicides->name,
                ];
                unset($item->fertilizer_id);
                unset($item->plant_medicine_id);
                unset($item->herbicide_id);
                unset($item->fertilizers);
                unset($item->plant_medicines);
                unset($item->herbicides);
            }
            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {

            $item = InvestmentMaterial::
            selectRaw('investment_materials.id,
            investment_materials.name,
             investment_materials.fertilizer_id,
              investment_materials.plant_medicine_id,
               investment_materials.herbicide_id,
                investment_materials.amount,
                 investment_materials.time,
                  investment_materials.image_seedling,
                   investment_materials.image_bill,
                   investment_materials.facility_name,
                   investment_materials.tel,
                   investment_materials.email,
                   investment_materials.cost,
                   investment_materials.province_id,
                   investment_materials.district_id,
                   investment_materials.ward_id,
                   investment_materials.village')
                ->where('investment_materials.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }


            $item->image_seedling = asset('public/filemanager/userfiles/' . $item->image_seedling);
            foreach (explode('|', $item->image_extra) as $img) {
                if ($img != '') {
                    $image_extra[] = asset('public/filemanager/userfiles/' . $img);
                }
            }
            $item->image_bill = asset('public/filemanager/userfiles/' . $item->image_bill);
            foreach (explode('|', $item->image_extra) as $img) {
                if ($img != '') {
                    $image_extra[] = asset('public/filemanager/userfiles/' . $img);
                }
            }
            $item->province = [
                'id' => @$item->province->id,
                'name' => @$item->provinces->name,
            ];
            $item->district = [
                'id' => @$item->district->id,
                'name' => @$item->districts->name,
            ];
            $item->ward = [
                'id' => @$item->ward->id,
                'name' => @$item->wards->name,
            ];


            $item->fertilizer = [
                'id' => @$item->fertilizers->id,
                'name' => @$item->fertilizers->name,
            ];
            $item->plant_medicine = [
                'id' => @$item->plant_medicines->id,
                'name' => @$item->plant_medicines->name,
            ];
            $item->herbicide = [
                'id' => @$item->herbicides->id,
                'name' => @$item->herbicides->name,
            ];
            unset($item->fertilizer_id);
            unset($item->plant_medicine_id);
            unset($item->herbicide_id);
            unset($item->fertilizers);
            unset($item->plant_medicines);
            unset($item->herbicides);

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], [
            'name.required' => 'Bắt buộc phải nhập tên',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('api')->id();
            if ($request->has('image_seedling')) {
                if (is_array($request->file('image_seedling'))) {
                    foreach ($request->file('image_seedling') as $image) {
                        $data['image_seedling'] = CommonHelper::saveFile($image, 'investment_material');
                        $data['image_bill'] = CommonHelper::saveFile($image, 'investment_bill');
                    }
                } else {
                    $data['image_seedling'] = CommonHelper::saveFile($request->file('image_seedling'), 'investment_material');
                    $data['image_bill'] = CommonHelper::saveFile($request->file('image_bill'), 'investment_bill');
                }
            }

            $item = InvestmentMaterial::create($data);

            return $this->show($item->id);
        }
    }


    public function update(Request $request, $id)
    {

        $item = InvestmentMaterial::find($id);
        if (!is_object($item)) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }

        $data = $request->except('api_token');
        //  Tùy chỉnh dữ liệu insert

        if ($request->has('image_seedling')) {
            if (is_array($request->file('image_seedling'))) {
                foreach ($request->file('image_seedling') as $image) {
                    $data['image_seedling'] = CommonHelper::saveFile($image, 'investment_material');
                }
            } else {
                $data['image_seedling'] = CommonHelper::saveFile($request->file('image'), 'investment_material');
            }
        }

        foreach ($data as $k => $v) {
            $item->{$k} = $v;
        }
        $item->save();

        return $this->show($item->id);
    }


    public function delete($id)
    {
        if (InvestmentMaterial::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
