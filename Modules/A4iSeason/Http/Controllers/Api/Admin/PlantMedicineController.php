<?php

namespace Modules\A4iSeason\Http\Controllers\Api\Admin;

use \Modules\A4iSeason\Models\Disease;
use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\A4iSeason\Models\PlantMedicine;
use Validator;

class PlantMedicineController extends Controller
{

    protected $module = [
        'code' => 'plant_medicine',
        'table_name' => 'plant_medicines',
        'label' => 'Thuốc bảo vệ thực vật',
        'modal' => 'Modules\A4iSeason\Models\PlantMdicine',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'admin_id' => [
            'query_type' => '='
        ],
        'land_id' => [
            'query_type' => '='
        ],
        'season_id' => [
            'query_type' => '='
        ]
    ];


    public function index(Request $request)
    {

        try {


            //  Filter
            $where = $this->filterSimple($request);
            $listItem = PlantMedicine::leftJoin('seasons', 'seasons.id', '=', 'plant_medicines.season_id')
                ->leftJoin('medicine_warehouses', 'medicine_warehouses.id', '=', 'plant_medicines.medicine_warehous_id')
                ->selectRaw('plant_medicines.id, plant_medicines.quantity, plant_medicines.execution_time, plant_medicines.implementation_date, plant_medicines.effect, plant_medicines.image, plant_medicines.cost,
                 plant_medicines.medicine_warehous_id, medicine_warehouses.name')
                ->whereRaw($where);



            //  Sort
            $listItem = $this->sort($request, $listItem);
            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());
            foreach ($listItem as $k => $item) {
                $item->medicine_warehous = [
                    'id' => $item->medicine_warehous_id,
                    'name' => $item->name,
                ];
                $item->image = $item->image != null ? asset('public/filemanager/userfiles/' . $item->image) : '';
                unset($item->medicine_warehous_id);
                unset($item->name);
            }
            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {


            $item = PlantMedicine::leftJoin('seasons', 'seasons.id', '=', 'plant_medicines.season_id')
                ->leftJoin('medicine_warehouses', 'medicine_warehouses.id', '=', 'plant_medicines.medicine_warehous_id')
                ->selectRaw('plant_medicines.id, plant_medicines.quantity, plant_medicines.effect, plant_medicines.implementation_date, plant_medicines.image, plant_medicines.cost, plant_medicines.medicine_warehous_id, medicine_warehouses.name')
                ->where('plant_medicines.id', $id)->first();
            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            $item->medicine_warehous = [
                'id' => $item->medicine_warehous_id,
                'name' => $item->name,
            ];
            unset($item->medicine_warehous_id);
            unset($item->name);
            $item->image = asset('public/filemanager/userfiles/' . $item->image);
            foreach (explode('|', $item->image_extra) as $img) {
                if ($img != '') {
                    $image_extra[] = asset('public/filemanager/userfiles/' . $img);
                }

            }





            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {


            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('api')->id();
            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = CommonHelper::saveFile($image, 'plant_medicine');
                    }
                } else {
                    $data['image'] = CommonHelper::saveFile($request->file('image'), 'plant_medicine');
                }
            }

            $item = PlantMedicine::create($data);

            return $this->show($item->id);
        }



    public function update(Request $request, $id)
    {

        $item = PlantMedicine::find($id);
        if (!is_object($item)) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }

        $data = $request->except('api_token');
        //  Tùy chỉnh dữ liệu insert

        if ($request->has('image')) {
            if (is_array($request->file('image'))) {
                foreach ($request->file('image') as $image) {
                    $data['image'] = CommonHelper::saveFile($image, 'plant_medicine');
                }
            } else {
                $data['image'] = CommonHelper::saveFile($request->file('image'), 'plant_medicine');
            }
        }

        foreach ($data as $k => $v) {
            $item->{$k} = $v;
        }
        $item->save();

        return $this->show($item->id);
    }


    public function delete($id)
    {
        if (PlantMedicine::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
