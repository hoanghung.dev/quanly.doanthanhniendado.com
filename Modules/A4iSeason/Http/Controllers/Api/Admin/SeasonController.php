<?php

namespace Modules\A4iSeason\Http\Controllers\Api\Admin;

use App\Http\Controllers\Admin\ImportController;
use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\A4iOrganization\Models\Organization;
use Modules\A4iSeason\Models\Disease;
use Modules\A4iSeason\Models\Drainage;
use Modules\A4iSeason\Models\Fertilizer;
use Modules\A4iSeason\Models\Harvest;
use Modules\A4iSeason\Models\InvestmentMaterial;
use Modules\A4iSeason\Models\Nutrition;
use Modules\A4iSeason\Models\PlantMedicine;
use Modules\A4iSeason\Models\Season;
use Modules\A4iLand\Models\Land;
use Illuminate\Http\Request;
use Modules\A4iSeason\Models\Seed;
use Modules\A4iSeason\Models\TechnicalCare;
use Validator;

class SeasonController extends Controller
{

    protected $module = [
        'code' => 'season',
        'table_name' => 'seasons',
        'label' => 'Mùa vụ',
        'modal' => 'Modules\A4iSeason\Models\Season',
    ];

    protected $filter = [
        'quantity_expected' => [
            'query_type' => 'like'
        ],
        'tree_name' => [
            'query_type' => 'like'
        ],
        'admin_id' => [
            'query_type' => '='
        ],
        'land_id' => [
            'query_type' => '='
        ],
        'status' => [
            'query_type' => '='
        ],
        'from_date' => [
            'query_type' => 'custom'
        ],
        'to_date' => [
            'query_type' => 'custom'
        ],
        'province_id' => [
            'query_type' => 'custom'
        ],
        'district_id' => [
            'query_type' => 'custom'
        ],
        'ward_id' => [
            'query_type' => 'custom'
        ],
    ];

    protected $land_type_owneds = [
        1 => 'Đất sử hữu',
        2 => 'Đất thuê',
        3 => 'Đất mượn',
        4 => 'Khác',
    ];

    protected $status = [
        1 => 'Đang diễn ra',
        0 => 'Kết thúc',
    ];

    public function index(Request $request)
    {
        try {


            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Season::leftJoin('lands', 'lands.id', '=', 'seasons.land_id')
                ->selectRaw('seasons.id, seasons.tree_name, seasons.quantity_expected, seasons.expected_date, seasons.image, seasons.status, seasons.image_present, 
                lands.id as land_id, lands.lat as land_lat, lands.long as land_long, lands.address as land_address, lands.province_id, lands.district_id, lands.ward_id, lands.type as land_type, lands.quantity as land_quantity')->whereRaw($where);

            if (!is_null($request->get('from_date'))) {
                $listItem = $listItem->where('expected_date', '>=', date('Y-m-d 00:00:00', strtotime($request->get('from_date'))));
            }
            if (!is_null($request->get('to_date'))) {
                $listItem = $listItem->where('expected_date', '<=', date('Y-m-d 23:59:59', strtotime($request->get('to_date'))));
            }
            if (!is_null($request->get('province_id'))) {
                $land_ids = Land::where('province_id', $request->province_id)->pluck('id')->toArray();
                $listItem = $listItem->whereIn('land_id', $land_ids);
            }
            if (!is_null($request->get('district_id'))) {
                $land_ids = Land::where('district_id', $request->district_id)->pluck('id')->toArray();
                $listItem = $listItem->whereIn('land_id', $land_ids);
            }
            if (!is_null($request->get('ward_id'))) {
                $land_ids = Land::where('ward_id', $request->ward_id)->pluck('id')->toArray();
                $listItem = $listItem->whereIn('land_id', $land_ids);
            }
//            dd($listItem);
            //  Sort
            $listItem = $this->sort($request, $listItem);
            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());

            foreach ($listItem as $k => $v) {
                $v->lands = [
                    'id' => $v->land_id,
                    'link_gg_map' => 'https://www.google.com/maps?q=' . $v->land_lat . ',' . $v->land_long,
                    'address' => $v->land_address,
                    'quantity' => $v->land_quantity,
                    'province' => [
                        'id' => @$v->province_id,
                        'name' => @$v->provinces->name,
                    ],
                    'district' => [
                        'id' => @$v->district_id,
                        'name' => @$v->districts->name,
                    ],
                    'ward' => [
                        'id' => @$v->ward_id,
                        'name' => @$v->wards->name,
                    ],
                    'type' => array_key_exists($v->land_type, $this->land_type_owneds) ? $this->land_type_owneds[$v->land_type] : '',
                ];
                $v->image = asset('public/filemanager/userfiles/' . $v->image);
                $v->status = @$this->status[$v->status];
                unset($v->provinces);
                unset($v->districts);
                unset($v->wards);
                unset($v->province_id);
                unset($v->district_id);
                unset($v->ward_id);
                unset($v->land_lat);
                unset($v->land_long);
                unset($v->land_address);
                unset($v->land_quantity);
                unset($v->land_type);
                unset($v->land_id);
            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {


            $item = Season::leftJoin('lands', 'lands.id', '=', 'seasons.land_id')
                ->leftJoin('admin', 'seasons.admin_id', '=', 'admin.id')
                ->leftJoin('provinces', 'admin.province_id', '=', 'provinces.id')
                ->leftJoin('districts', 'admin.district_id', '=', 'districts.id')
                ->leftJoin('wards', 'admin.ward_id', '=', 'wards.id')
                ->selectRaw('seasons.id, seasons.tree_name, seasons.start_date, seasons.quantity_expected, seasons.admin_id, seasons.expected_date, seasons.status, seasons.image, seasons.image_present,
                lands.id as land_id, lands.link_gg_map as land_link_gg_map, lands.address as land_address, lands.province_id, lands.district_id, lands.ward_id, lands.type as land_type, lands.quantity as land_quantity,
                 admin.id as admin_id,admin.name as admin_name, admin.image as admin_image, admin.gender as admin_gender, admin.birthday as admin_birthday, admin.address as admin_address, admin.tel as admin_tel, admin.province_id as admin_province_id, admin.province_id as admin_province_id, admin.district_id as admin_district_id, admin.ward_id as admin_ward_id,
                 provinces.name as province_name,
                 districts.name as district_name,
                 wards.name as ward_name
                 ')->where('seasons.id', $id)->first();
            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }
            $harvests = \Modules\A4iSeason\Models\Harvest::where('season_id', $id)->orderBy('id', 'desc')->get();
            $harvest_last = \Modules\A4iSeason\Models\Harvest::where('season_id', $id)->orderBy('id', 'desc')->first();
            $total_price = 0;
            $total_cost = 0;
            $list_harvers = [];
            $list_harvers[] = [
                'id' => null,
                'time' => null,
                'cost' => \Modules\A4iSeason\Http\Helpers\SeasonHelper::getCostSeason($id, false, $harvest_last),
                'price' => null,
            ];
            foreach ($harvests as $key => $harvest) {
                $tong_thu = \Modules\A4iSeason\Http\Helpers\SeasonHelper::getPriceHarvest($harvest);
                $total_price += $tong_thu;
                $season_id = $id;

                $tong_chi = \Modules\A4iSeason\Http\Helpers\SeasonHelper::getCostSeason($season_id, $harvest);
                $total_cost += $tong_chi;
                $list_harvers[] = [
                    'id' => $harvest->id,
                    'time' => $harvest->implementation_date,
                    'cost' => $tong_chi,
                    'price' => $tong_thu,
                ];
            }

            $history_harvers = [

                'list_harvers' => $list_harvers,
                'total_cost' => \Modules\A4iSeason\Http\Helpers\SeasonHelper::getCostSeason($id),
                'total_price' => $total_price,
            ];
            $item->history_harvers = $history_harvers;
            $item->image = asset('public/filemanager/userfiles/' . $item->image);
            foreach (explode('|', $item->image_present) as $img) {
                if ($img != '') {
                    $image_present[] = asset('public/filemanager/userfiles/' . $img);
                }
            }
            $item->image_present = @$image_present;

            $item->status = @$this->status[$item->status];
            $item->land = [
                'id' => $item->land_id,
                'link_gg_map' => $item->land_link_gg_map,
                'address' => $item->land_address,
                'quantity' => $item->land_quantity,
                'province' => [
                    'id' => @$item->province_id,
                    'name' => @$item->provinces->name,
                ],
                'district' => [
                    'id' => @$item->district_id,
                    'name' => @$item->districts->name,
                ],
                'ward' => [
                    'id' => @$item->ward_id,
                    'name' => @$item->wards->name,
                ],
                'type' => array_key_exists($item->land_type, $this->land_type_owneds) ? $this->land_type_owneds[$item->land_type] : '',
            ];
            unset($item->land_id);
            unset($item->land_link_gg_map);
            unset($item->land_address);
            unset($item->land_quantity);
            unset($item->land_type);
            unset($item->province_id);
            unset($item->district_id);
            unset($item->ward_id);
            unset($item->provinces);
            unset($item->districts);
            unset($item->wards);
            $item->admin = [
                'id' => $item->admin_id,
                'name' => $item->admin_name,
                'image' => asset('public/filemanager/userfiles/' . $item->admin_image),
                'gender' => $item->admin_gender,
                'birthday' => $item->admin_birthday,
                'address' => $item->admin_address,
                'tel' => $item->admin_tel,
                'province' => [
                    'id' => @$item->admin_province_id,
                    'name' => @$item->province_name,
                ],
                'district' => [
                    'id' => @$item->admin_district_id,
                    'name' => @$item->district_name,
                ],
                'ward' => [
                    'id' => @$item->admin_ward_id,
                    'name' => @$item->ward_name,
                ],
            ];
            unset($item->admin_id);
            unset($item->admin_name);
            unset($item->admin_image);
            unset($item->admin_gender);
            unset($item->admin_birthday);
            unset($item->admin_address);
            unset($item->admin_tel);
            unset($item->province_name);
            unset($item->admin_province_id);
            unset($item->admin_district_id);
            unset($item->district_name);
            unset($item->admin_ward_id);
            unset($item->ward_name);


            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tree_name' => 'required'
        ], [
            'tree_name.required' => 'Bắt buộc phải nhập tên',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('api')->id();

            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = $data['image_present'][] = CommonHelper::saveFile($image, 'season');
                    }
                } else {
                    $data['image'] = $data['image_present'][] = CommonHelper::saveFile($request->file('image'), 'season');
                }
                $data['image_present'] = implode('|', $data['image_present']);
            }

            $item = Season::create($data);

            return $this->show($item->id);
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'tree_name' => 'required'
        ], [
            'tree_name.required' => 'Bắt buộc phải nhập tên',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {

            $item = Season::find($id);

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Validate errors',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            $data = $request->except('api_token');

            //  Tùy chỉnh dữ liệu insert
            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($image, 'season');
                    }
                } else {
                    $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($request->file('image'), 'season');
                }
                $data['image_extra'] = implode('|', $data['image_extra']);
            }

            foreach ($data as $k => $v) {
                $item->{$k} = $v;
            }
            $item->save();
            return $this->show($item->id);
        }
    }

    function tong_thu($harvest)
    {
        $tong_thu = 0;
        if ($harvest->quantity_unit == 'tấn') {
            $tong_thu = (int)$harvest->quantity * 1000;
            if ($harvest->price_unit == '/tấn') {
                $tong_thu *= ((int)$harvest->price / 1000);
            } else {
                $tong_thu *= (int)$harvest->price;
            }
        } elseif ($harvest->quantity_unit == 'kg') {
            $tong_thu = (int)$harvest->quantity;
            if ($harvest->price_unit == '/tấn') {
                $tong_thu *= ((int)$harvest->price / 1000);
            } else {
                $tong_thu *= (int)$harvest->price;
            }
        }
        return $tong_thu;
    }

    public function delete($id)
    {
        if (Season::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . $this->module['table_name'] . '.id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
