<?php

namespace Modules\A4iSeason\Http\Controllers\Api\Admin;

use Modules\A4iSeason\Models\Drainage;
use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use \Modules\A4iSeason\Models\Nutrition;
use Modules\A4iSeason\Models\Seed;
use Validator;

class SeedlingController extends Controller
{

    protected $module = [
        'code' => 'seedlings',
        'table_name' => 'seeds',
        'label' => 'Giống',
        'modal' => 'Modules\A4iSeason\Models\Seed',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'admin_id' => [
            'query_type' => '='
        ],
        'land_id' => [
            'query_type' => '='
        ],
        'season_id' => [
            'query_type' => '='
        ]
    ];
    protected $method_seeds = [
        1 => 'Hạt giống',
        2 => 'Cây nuôi cấy mô',
        3 => 'Cây triết',
        4 => 'Cây ghép',
    ];

    public function index(Request $request)
    {

        try {


            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Seed::leftJoin('seasons', 'seasons.id', '=', 'seeds.season_id')
                ->selectRaw('seeds.id, seeds.category_seeds, seeds.name_supplier, seeds.address, seeds.tel, seeds.email, seeds.website, seeds.image, seeds.amount, seeds.province_id, seeds.district_id, seeds.ward_id, seeds.cost')
                ->whereRaw($where);


            //  Sort
            $listItem = $this->sort($request, $listItem);

            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());
            foreach ($listItem as $k => $v) {
                $v->category_seeds = array_key_exists($v->category_seeds, $this->method_seeds) ? $this->method_seeds[$v->category_seeds] : '';
                $v->image = asset('public/filemanager/userfiles/' . $v->image);

            }
            foreach ($listItem as $k => $item) {
                $item->province = [
                    'id' => @$item->province->id,
                    'name' => @$item->provinces->name,
                ];
                $item->district = [
                    'id' => @$item->district->id,
                    'name' => @$item->districts->name,
                ];
                $item->ward = [
                    'id' => @$item->ward->id,
                    'name' => @$item->wards->name,
                ];

                unset($item->province_id);
                unset($item->district_id);
                unset($item->ward_id);
            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {


            $item = Seed::leftJoin('seasons', 'seasons.id', '=', 'seeds.season_id')
                ->selectRaw('seeds.id, seeds.category_seeds, seeds.name_supplier, seeds.address, seeds.email, seeds.tel, seeds.website, seeds.image, seeds.amount, seeds.province_id, seeds.district_id, seeds.ward_id, seeds.cost')->where('seeds.id', $id)->first();
            foreach (explode('|', $item->image) as $img) {
                if ($img != '') {
                    $image[] = asset('public/filemanager/userfiles/' . $img);
                }
            }

            $item->image = @$image;
//            $item->image = asset('public/filemanager/userfiles/' . $item->image);


            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }
            $item->category_seeds = array_key_exists($item->category_seeds, $this->method_seeds) ? $this->method_seeds[$item->category_seeds] : '';
            $item->province = [
                'id' => @$item->province->id,
                'name' => @$item->provinces->name,
            ];
            $item->district = [
                'id' => @$item->district->id,
                'name' => @$item->districts->name,
            ];
            $item->ward = [
                'id' => @$item->ward->id,
                'name' => @$item->wards->name,
            ];

            unset($item->province_id);
            unset($item->district_id);
            unset($item->ward_id);

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {

        $data = $request->all();
        //  Tùy chỉnh dữ liệu insert
        $data['admin_id'] = \Auth::guard('api')->id();
        if ($request->has('image')) {
            if (is_array($request->file('image'))) {
                foreach ($request->file('image') as $image) {
                    $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($image, 'seed');
                }
            } else {
                $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($request->file('image'), 'seed');
            }
            $data['image_extra'] = implode('|', $data['image_extra']);
        }

        $item = Seed::create($data);

        return $this->show($item->id);
    }


    public function update(Request $request, $id)
    {

        $item = Seed::find($id);
        if (!is_object($item)) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }

        $data = $request->except('api_token');
        //  Tùy chỉnh dữ liệu insert

        if ($request->has('image')) {
            if (is_array($request->file('image'))) {
                foreach ($request->file('image') as $image) {
                    $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($image, 'seed');
                }
            } else {
                $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($request->file('image'), 'seed');
            }
            $data['image_extra'] = implode('|', $data['image_extra']);
        }

        foreach ($data as $k => $v) {
            $item->{$k} = $v;
        }
        $item->save();

        return $this->show($item->id);
    }


    public function delete($id)
    {
        if (Seed::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
