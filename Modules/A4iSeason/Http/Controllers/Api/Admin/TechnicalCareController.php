<?php

namespace Modules\A4iSeason\Http\Controllers\Api\Admin;

use \Modules\A4iSeason\Models\Disease;
use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\A4iSeason\Models\PlantMedicine;
use Modules\A4iSeason\Models\TechnicalCare;

use Validator;

class TechnicalCareController extends Controller
{

    protected $module = [
        'code' => 'technical_care',
        'table_name' => 'technical_cares',
        'label' => 'Lịch sử kỹ thuật chăm sóc',
        'modal' => 'Modules\A4iSeason\Models\TechnicalCare',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'admin_id' => [
            'query_type' => '='
        ],
        'land_id' => [
            'query_type' => '='
        ],
        'season_id' => [
            'query_type' => '='
        ]
    ];


    protected $type_technology = [
        1 => 'Tỉa cành',
        2 => 'Vun gốc',
        3 => 'Bao trái',
        4 => 'Làm cỏ',
        5 => 'Thụ phấn ong',
    ];
    protected $method_weeding = [
        1 => 'Làm thủ công',
        2 => 'Làm bằng máy',
    ];


    public function index(Request $request)
    {

        try {


            //  Filter
            $where = $this->filterSimple($request);
            $listItem = TechnicalCare::leftJoin('seasons', 'seasons.id', '=', 'technical_cares.season_id')
                ->selectRaw('technical_cares.id, technical_cares.technology, technical_cares.method, technical_cares.image, technical_cares.image_extra, technical_cares.implementation_date,technical_cares.name_herbicide, technical_cares.admin_id, technical_cares.cost')
                ->whereRaw($where);


            //  Sort
            $listItem = $this->sort($request, $listItem);

            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());
            foreach ($listItem as $k => $v) {
                $v->technical_care = [
                    'technology' => array_key_exists($v->technology, $this->type_technology) ? $this->type_technology[$v->technology] : '',
                ];
                $v->image = asset('public/filemanager/userfiles/' . $v->image);
                $v->image_extra = asset('public/filemanager/userfiles/' . $v->image_extra);

                unset($v->technology);


            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {


            $item = TechnicalCare::leftJoin('seasons', 'seasons.id', '=', 'technical_cares.season_id')
                ->selectRaw('technical_cares.id, technical_cares.technology , technical_cares.image, technical_cares.method, technical_cares.implementation_date, technical_cares.admin_id, technical_cares.image_extra, technical_cares.name_herbicide, technical_cares.cost')
                ->where('technical_cares.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }


            $item->image = asset('public/filemanager/userfiles/' . $item->image);
            foreach (explode('|', $item->image_extra) as $img) {
                if ($img != '') {
                    $image_extra[] = asset('public/filemanager/userfiles/' . $img);
                }
            }
            $item->image_extra = @$image_extra;


            $item->technical_care = [
                'technology' => array_key_exists($item->technology, $this->type_technology) ? $this->type_technology[$item->technology] : '',
            ];
            $item->method = [
                'method_weeding' => array_key_exists($item->method, $this->method_weeding) ? $this->method_weeding[$item->method] : '',
            ];
            unset($item->technology);
            unset($item->method_weeding);



            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {

            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('api')->id();
            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = CommonHelper::saveFile($image, 'technical_care');
                    }
                } else {
                    $data['image'] = CommonHelper::saveFile($request->file('image'), 'technical_care');
                }
            }
            if ($request->has('image_extra')) {
                if (is_array($request->file('image_extra'))) {
                    foreach ($request->file('image_extra') as $image) {
                        $data['image'] = CommonHelper::saveFile($image, 'technical_care');
                    }
                } else {
                    $data['image_extra'] = CommonHelper::saveFile($request->file('image_extra'), 'technical_care');
                }
            }


            $item = TechnicalCare::create($data);

            return $this->show($item->id);
        }



    public function update(Request $request, $id)
    {

        $item = TechnicalCare::find($id);
        if (!is_object($item)) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }

        $data = $request->except('api_token');
        //  Tùy chỉnh dữ liệu insert

        if ($request->has('image')) {
            if (is_array($request->file('image'))) {
                foreach ($request->file('image') as $image) {
                    $data['image'] = CommonHelper::saveFile($image, 'technical_care');
                }
            } else {
                $data['image'] = CommonHelper::saveFile($request->file('image'), 'technical_care');
            }
        }

        foreach ($data as $k => $v) {
            $item->{$k} = $v;
        }
        $item->save();

        return $this->show($item->id);
    }


    public function delete($id)
    {
        if (TechnicalCare::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
