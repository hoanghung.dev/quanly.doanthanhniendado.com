<?php

//namespace App\Http\Helpers;
namespace Modules\A4iSeason\Http\Helpers;

use App\Models\Category;
use App\Models\Meta;
use App\Models\RoleAdmin;
use Auth;
use Illuminate\Support\Facades\Cache;
use Modules\EworkingJob\Models\Task;
use Session;
use View;

class SeasonHelper
{

//$season_id = id mua vu
//$harvest = obj thu hoach
    public static function getCostSeason($season_id, $harvest = false, $prev_harvast = false)
    {
        //  Lấy lần thu hoạch trước đó
        if ($harvest && !$prev_harvast) {
            $prev_harvast = \Modules\A4iSeason\Models\Harvest::where('season_id', $season_id);
            $prev_harvast = $prev_harvast->where('id', '!=', $harvest->id);
            $prev_harvast = $prev_harvast->whereDate('implementation_date', '<=', date('Y-m-d', strtotime($harvest->implementation_date)));
            $prev_harvast = $prev_harvast->orderBy('id', 'desc')->first();
        }
        $tong_chi = 0;
        //  Tuy vấn theo season_id
        $drainage_price = \Modules\A4iSeason\Models\Drainage::where('season_id', $season_id);
        $seed_price = \Modules\A4iSeason\Models\Seed::where('season_id', $season_id);
        $plant_medicine_price = \Modules\A4iSeason\Models\PlantMedicine::where('season_id', $season_id);
        $disease_price = \Modules\A4iSeason\Models\Disease::where('season_id', $season_id);
        $nutrition_price = \Modules\A4iSeason\Models\Nutrition::where('season_id', $season_id);
        $technical_care_price = \Modules\A4iSeason\Models\TechnicalCare::where('season_id', $season_id);
        $investment_material_price = \Modules\A4iSeason\Models\InvestmentMaterial::where('season_id', $season_id);
        $fertilizer_price = \Modules\A4iSeason\Models\Fertilizer::where('season_id', $season_id);

        if (isset($prev_harvast) && is_object($prev_harvast)) {
            $implementation_date = date('Y-m-d', strtotime($prev_harvast->implementation_date));
            $drainage_price = $drainage_price->whereDate('implementation_date', '>', $implementation_date);
            $seed_price = $seed_price->whereDate('implementation_date', '>', $implementation_date);
            $plant_medicine_price = $plant_medicine_price->whereDate('implementation_date', '>', $implementation_date);
            $disease_price = $disease_price->whereDate('implementation_date', '>', $implementation_date);
            $nutrition_price = $nutrition_price->whereDate('implementation_date', '>', $implementation_date);
            $technical_care_price = $technical_care_price->whereDate('implementation_date', '>', $implementation_date);
            $investment_material_price = $investment_material_price->whereDate('implementation_date', '>', $implementation_date);
            $fertilizer_price = $fertilizer_price->whereDate('implementation_date', '>', $implementation_date);
        }

        if ($harvest) {
            $implementation_date = date('Y-m-d', strtotime($harvest->implementation_date));
            $drainage_price = $drainage_price->whereDate('implementation_date', '<=', $implementation_date);
            $seed_price = $seed_price->whereDate('implementation_date', '<=', $implementation_date);
            $plant_medicine_price = $plant_medicine_price->whereDate('implementation_date', '<=', $implementation_date);
            $disease_price = $disease_price->whereDate('implementation_date', '<=', $implementation_date);
            $nutrition_price = $nutrition_price->whereDate('implementation_date', '<=', $implementation_date);
            $technical_care_price = $technical_care_price->whereDate('implementation_date', '<=', $implementation_date);
            $investment_material_price = $investment_material_price->whereDate('implementation_date', '<=', $implementation_date);
            $fertilizer_price = $fertilizer_price->whereDate('implementation_date', '<=', $implementation_date);
        }

        $drainage_price = $drainage_price->sum('cost');
        $seed_price = $seed_price->sum('cost');
        $plant_medicine_price = $plant_medicine_price->sum('cost');
        $disease_price = $disease_price->sum('cost');
        $nutrition_price = $nutrition_price->sum('cost');
        $technical_care_price = $technical_care_price->sum('cost');
        $investment_material_price = $investment_material_price->sum('cost');
        $fertilizer_price = $fertilizer_price->sum('cost');

        if ($fertilizer_price > 0) {
            $tong_chi += $fertilizer_price;
        }
        if ($drainage_price > 0) {
            $tong_chi += $drainage_price;
        }
        if ($seed_price > 0) {
            $tong_chi += $seed_price;
        }
        if ($plant_medicine_price > 0) {
            $tong_chi += $plant_medicine_price;
        }
        if ($disease_price > 0) {
            $tong_chi += $disease_price;
        }
        if ($nutrition_price > 0) {
            $tong_chi += $nutrition_price;
        }
        if ($technical_care_price > 0) {
            $tong_chi += $technical_care_price;
        }
        if ($investment_material_price > 0) {
            $tong_chi += $investment_material_price;
        }
        return $tong_chi;
    }

    public static function getPriceHarvest($harvest)
    {
        $tong_thu = 0;
        if ($harvest->quantity_unit == 'tấn') {
            $tong_thu = (int)$harvest->quantity * 1000;
            if ($harvest->price_unit == '/tấn') {
                $tong_thu *= ((int)$harvest->price / 1000);
            } else {
                $tong_thu *= (int)$harvest->price;
            }
        } elseif ($harvest->quantity_unit == 'kg') {
            $tong_thu = (int)$harvest->quantity;
            if ($harvest->price_unit == '/tấn') {
                $tong_thu *= ((int)$harvest->price / 1000);
            } else {
                $tong_thu *= (int)$harvest->price;
            }
        }
        return $tong_thu;
    }
}
