<?php

namespace Modules\A4iSeason\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\A4iLand\Models\Land;
use Modules\EworkingAdmin\Models\Admin;

class Disease extends Model
{

    protected $table = 'diseases';
    protected $fillable = [
        'phenomena', 'image', 'pathogen', 'fertilizer_formula', 'quantity', 'time', 'admin_id', 'season_id', 'disease_image', 'cost', 'implementation_date'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
    public function land()
    {
        return $this->belongsTo(Land::class, 'land_id');
    }
    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }

}
