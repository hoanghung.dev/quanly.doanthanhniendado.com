<?php

namespace Modules\A4iSeason\Models;

use Illuminate\Database\Eloquent\Model;

class Herbicide extends Model
{

    protected $table = 'herbicides';
    public $timestamps = false;
    protected $fillable = [
        'name', 'image', 'quantity', 'execution_time', 'season_id', 'admin_id'
    ];


    public function land()
    {
        return $this->belongsTo(Land::class, 'land_id');
    }
    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }

}
