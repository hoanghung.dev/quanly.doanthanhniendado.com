<?php
namespace Modules\A4iSeason\Models;

use App\Http\Helpers\CommonHelper;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use DB;
use Modules\A4iLocation\Models\{Ward,District,Province};
use Modules\A4iAdmin\Models\Admin;
class InvestmentMaterial extends Model
{
//    protected $timestamps = false;
    public $timestamps = false;
    protected $table = 'investment_materials';

    protected $fillable = [
        'name', 'amount', 'image_seedling', 'image_bill', 'time', 'admin_id', 'facility_name', 'tel', 'expected_date', 'email', 'province_id', 'district_id',
        'ward_id', 'season_id', 'village', 'fertilizer_id', 'plant_medicine_id', 'herbicide_id', 'cost', 'implementation_date'
    ];


    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }
    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function ward()
    {
        return $this->belongsTo(Ward::class, 'ward_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
    public function fertilizer()
    {
        return $this->belongsTo(Fertilizer::class, 'fertilizer_id');
    }
    public function plant_medicine()
    {
        return $this->belongsTo(PlantMedicine::class, 'plant_medicine_id');
    }
    public function herbicide()
    {
        return $this->belongsTo(Herbicide::class, 'herbicide_id');
    }

    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }
}
