<?php

namespace Modules\A4iSeason\Models;

use Illuminate\Database\Eloquent\Model;

class PlantGrowth extends Model
{

    protected $table = 'plant_growths';
    public $timestamps = false;
    protected $fillable = [
        'seedling', 'image', 'time', 'admin_id', 'season_id'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
    public function land()
    {
        return $this->belongsTo(Land::class, 'land_id');
    }
    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }

}
