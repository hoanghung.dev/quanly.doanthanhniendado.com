<?php

namespace Modules\A4iSeason\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\A4iLand\Models\Land;
use Modules\A4iMedicineWarehouse\Models\MedicineWarehouse;
use Modules\EworkingAdmin\Models\Admin;

class PlantMedicine extends Model
{

    protected $table = 'plant_medicines';
    protected $fillable = [
        'name', 'image', 'quantity', 'execution_time', 'admin_id', 'effect', 'season_id', 'medicine_warehous_id', 'cost', 'implementation_date'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
    public function land()
    {
        return $this->belongsTo(Land::class, 'land_id');
    }
    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');

    }
    public function medicine_warehouse()
    {
        return $this->belongsTo(MedicineWarehouse::class, 'medicine_warehous_id');

    }

}
