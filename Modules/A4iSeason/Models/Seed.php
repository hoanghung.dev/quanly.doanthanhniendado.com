<?php


namespace Modules\A4iSeason\Models;

use App\Http\Helpers\CommonHelper;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use DB;
use Modules\A4iLocation\Models\{Ward,District,Province};


class Seed extends Model
{
    protected $table = 'seeds';

    protected $fillable = [
        'category_seeds', 'name_supplier', 'address', 'email', 'tel', 'website', 'season_id', 'image', 'admin_id', 'cost', 'amount', 'district_id', 'province_id', 'ward_id',
        'image_extra','implementation_date'
    ];


    public function admin()
    {
        return $this->belongsTo(\Modules\EworkingAdmin\Models\Admin::class, 'admin_id');
    }
    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }
    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }
    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function ward()
    {
        return $this->belongsTo(Ward::class, 'ward_id');
    }
}
