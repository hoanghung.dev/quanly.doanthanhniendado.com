<?php

namespace Modules\A4iSeason\Models;

use Illuminate\Database\Eloquent\Model;

class TechnicalCare extends Model
{

    protected $table = 'technical_cares';
    public $timestamps = false;
    protected $fillable = [
        'technology', 'image', 'admin_id', 'season_id', 'method', 'implementation_date', 'image_extra', 'name_herbicide', 'cost'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
    public function land()
    {
        return $this->belongsTo(Land::class, 'land_id');
    }
    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }
    public function herbicide()
    {
        return $this->belongsTo(Herbicide::class, 'herbicide_id');
    }

}
