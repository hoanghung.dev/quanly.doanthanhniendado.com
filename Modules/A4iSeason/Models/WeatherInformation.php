<?php

/**
 * Admin Model
 *
 * Admin Model manages Admin operation.
 *
 * @category   Admin
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\A4iSeason\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Modules\A4iLocation\Models\Province;

class WeatherInformation extends Model
{
//    protected $timestamps = false;
    public $timestamps = false;
    protected $table = 'weather_informations';

    protected $fillable = [
        'id','warning_from', 'warning_value', '', 'temperature', 'date', 'humidity', 'wind_speed', 'amount_rain', 'wind_direction', 'CO2', 'season_id',
        'amount_of_rain', 'admin_id', 'natural_disaster'
    ];


    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }
    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }


}
