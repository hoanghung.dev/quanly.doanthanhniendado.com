<?php

namespace Modules\A4iSeason\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\A4iOrganization\Models\Organization;
use Modules\A4iSeason\Models\Herbicide;
use Modules\A4iSeason\Models\Season;

class Weeding extends Model
{

    protected $table = 'weedings';
//    public $timestamps = false;
    protected $fillable = [
        'methods', 'time', 'herbicide_name', 'admin_id', 'image', 'season_id'
    ];

    public function admin()
    {
        return $this->belongsTo(\Modules\EworkingAdmin\Models\Admin::class, 'admin_id');
    }
    public function land()
    {
        return $this->belongsTo(Organization::class, 'land_id');
    }
    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }
    public function herbicides()
    {
        return $this->belongsTo(Herbicide::class, 'herbicide_id');
    }

}
