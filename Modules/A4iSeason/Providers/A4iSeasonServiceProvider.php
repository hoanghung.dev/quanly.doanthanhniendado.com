<?php

namespace Modules\A4iSeason\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class A4iSeasonServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Đăng ký quyền
            $this->registerPermission();
            $this->registerPermissionSeed();
            $this->registerPermissionDrainage();
            $this->registerPermissionPlantMedicine();
            $this->registerPermissionNutrition();
            $this->registerPermissionWeeding();
            $this->registerPermissionFertilizer();
            $this->registerPermissionDisease();
            $this->registerPermissionPlantGrowth();
            $this->registerPermissionTechnicalCare();
            $this->registerPermissionHarvest();
            $this->registerPermissionWeatherInformation();
            $this->registerPermissionInvestmentMaterial();
            $this->registerPermissionAnalysis();
            $this->rendAsideMenu();
        }
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['season_view', 'season_add', 'season_edit', 'season_delete']);
            return $per_check;
        }, 1, 1);
    }

    public function registerPermissionSeed()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['seedling_view', 'seedling_add', 'seedling_edit', 'seedling_delete']);
            return $per_check;
        }, 1, 1);
    }

    public function registerPermissionDrainage()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['drainage_view', 'drainage_add', 'drainage_edit', 'drainage_delete']);
            return $per_check;
        }, 1, 1);
    }

    public function registerPermissionWeeding()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['weeding_view', 'weeding_add', 'weeding_edit', 'weeding_delete']);
            return $per_check;
        }, 1, 1);
    }

    public function registerPermissionFertilizer()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['fertilizer_view', 'fertilizer_add', 'fertilizer_edit', 'fertilizer_delete']);
            return $per_check;
        }, 1, 1);
    }

    public function registerPermissionNutrition()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['nutrition_view', 'nutrition_add', 'nutrition_edit', 'nutrition_delete']);
            return $per_check;
        }, 1, 1);
    }

    public function registerPermissionPlantGrowth()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['plant_growth_view', 'plant_growth_add', 'plant_growth_edit', 'plant_growth_delete']);
            return $per_check;
        }, 1, 1);
    }

    public function registerPermissionTechnicalCare()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['technical_care_view', 'technical_care_add', 'technical_care_edit', 'technical_care_delete']);
            return $per_check;
        }, 1, 1);
    }

    public function registerPermissionHarvest()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['harvest_view', 'harvest_add', 'harvest_edit', 'harvest_delete']);
            return $per_check;
        }, 1, 1);
    }

    public function registerPermissionWeatherInformation()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['weather_information_view', 'weather_information_add', 'weather_information_edit', 'weather_information_delete']);
            return $per_check;
        }, 1, 1);
    }

    public function registerPermissionInvestmentMaterial()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['investment_material_view', 'investment_material_add', 'investment_material_edit', 'investment_material_delete']);
            return $per_check;
        }, 1, 1);
    }

    public function registerPermissionPlantMedicine()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['plant_medicine_view', 'plant_medicine_add', 'plant_medicine_edit', 'plant_medicine_delete']);
            return $per_check;
        }, 1, 1);
    }

    public function registerPermissionDisease()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['disease_view', 'disease_add', 'disease_edit', 'disease_delete']);
            return $per_check;
        }, 1, 1);
    }

    public function registerPermissionAnalysis()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['analysis_view', 'analysis_add', 'analysis_edit', 'analysis_delete']);
            return $per_check;
        }, 1, 1);
    }

    public function rendAsideMenu()
    {
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('a4iseason::partials.aside_menu.aside_menu_dashboard_after');
        }, 1, 1);
    }


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('a4iseason.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php', 'a4iseason'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/a4iseason');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/a4iseason';
        }, \Config::get('view.paths')), [$sourcePath]), 'a4iseason');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/a4iseason');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'a4iseason');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'a4iseason');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
