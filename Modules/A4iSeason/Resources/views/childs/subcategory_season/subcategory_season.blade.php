<?php
if (!isset($_GET['season_id']) && isset($result)) {
    $_GET['season_id'] = $result->id;
    $season = $result;
} else {
    $season = \Modules\A4iSeason\Models\Season::find($_GET['season_id']);
}

?>
<div class="row">
    <div class="col-md-12">
        <p class="pull-right btn-action-job" @if($module['code'] != 'season') style=" padding: 0 20px;" @endif>
            <a href="{{ '/admin/season/'. @$_GET['season_id']}}"
               class="{{$module['code'] == 'season' && last(request()->segments()) != 'timeline'  ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon-list"></i>Thông tin mùa vụ</a>
            <a href="/admin/drainage?season_id={{ @$_GET['season_id']}}"
               class="{{$module['code'] == 'drainage' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon2-drop"></i>Tưới tiêu</a>
            <a href="/admin/seedling?season_id={{ @$_GET['season_id']}}"
               class="{{$module['code'] == 'seedling' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon2-correct"></i>Giống</a>
            {{--<a href="/admin/weeding?season_id={{ @$_GET['season_id']}}"
               class="{{$module['code'] == 'weeding' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon-cart"></i>Làm cỏ</a>--}}
            <a href="/admin/plant_growth?season_id={{ @$_GET['season_id']}}"
               class="{{$module['code'] == 'plant_growth' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon2-graphic"></i>Sinh trưởng cây trồng</a>
            <a href="/admin/technical_care?season_id={{ @$_GET['season_id']}}"
               class="{{$module['code'] == 'technical_care' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon2-protected"></i>Kỹ thuật chăm sóc</a>
            <a href="/admin/weather_information?season_id={{ @$_GET['season_id']}}"
               class="{{$module['code'] == 'weather_information' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon2-wifi"></i>Thông tin thời tiết</a>
            <a href="/admin/fertilizer?season_id={{ @$_GET['season_id']}}"
               class="{{$module['code'] == 'fertilizer' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon2-rubbish-bin-delete-button"></i>Phân bón</a>
            <a href="/admin/nutrition?season_id={{ @$_GET['season_id']}}"
               class="{{$module['code'] == 'nutrition' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon2-line-chart"></i>Dinh dưỡng</a>
            <a href="/admin/disease?season_id={{ @$_GET['season_id']}}"
               class="{{$module['code'] == 'disease' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon2-warning"></i>Dịch bệnh</a>
            <a href="/admin/plant_medicine?season_id={{ @$_GET['season_id']}}"
               class="{{$module['code'] == 'plant_medicine' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon-security"></i>Thuốc bảo vệ thực vật</a>
            <a href="/admin/investment_material?season_id={{ @$_GET['season_id']}}"
               class="{{$module['code'] == 'investment_material' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon-download-1"></i>Nguyên liệu đầu vào</a>
            {{--<a href="/admin/analysis?season_id={{ @$_GET['season_id']}}"--}}
               {{--class="{{$module['code'] == 'analysis' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i--}}
                        {{--class="flaticon2-drop"></i>Phân tích</a>--}}
            <a href="/admin/harvest?season_id={{ @$_GET['season_id']}}"
               class="{{$module['code'] == 'harvest' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon-shopping-basket"></i>Thu hoạch</a>
            @if(isset($_GET['season_id']))
                <a href="/admin/season/{{ @$_GET['season_id'] }}/timeline?season_id={{ @$_GET['season_id']}}"
                   class="{{last(request()->segments()) == 'timeline' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                            class="flaticon-download-1"></i>Lịch sử vụ mùa</a>
            @endif
        </p>
    </div>
</div>
@if($season->status == 0 && $season->admin_id == \Auth::guard('admin')->user()->id)
    <div class="col-md-12">
        <div class="alert alert-solid-danger alert-bold" role="alert">
            <div class="alert-text">Mùa vụ này đã kết thúc! Không thể chỉnh sửa. Để mở lại mùa vụ này, vui lòng <a href="/admin/harvest/edit-last-item?season_id={{ $season->id }}">Click Vào Đây</a> để sửa lại option "Sau khi thu hoạch" thành "Trồng tiếp nhưng theo chu kỳ mới"</div>
        </div>
    </div>
@endif

