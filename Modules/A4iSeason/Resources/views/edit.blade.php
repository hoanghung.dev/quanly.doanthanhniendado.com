@extends(config('core.admin_theme').'.template')
@section('main')
    <form class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid {{ @$module['code'] }}"
          action="" method="POST"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <input name="return_direct" value="save_continue" type="hidden">
        @include('a4iseason::childs.subcategory_season.subcategory_season')
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                     id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg" style="">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Chỉnh sửa {{ $module['label'] }}
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="/admin/{{ $module['code'] }}{{ $result->admin_id != \Auth::guard('admin')->user()->id ? '?view=all' : '' }}"
                               class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Quay lại</span>
                            </a>
                            <div class="btn-group">
                                @if(($result->status == 1 && $result->admin_id == \Auth::guard('admin')->user()->id) || in_array('super_admin', $permissions))
                                    @if(in_array($module['code'].'_edit', $permissions))
                                        <button type="submit" class="btn btn-brand">
                                            <i class="la la-check"></i>
                                            <span class="kt-hidden-mobile">Lưu</span>
                                        </button>
                                        <button type="button"
                                                class="btn btn-brand dropdown-toggle dropdown-toggle-split"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <a class="kt-nav__link save_option" data-action="save_continue">
                                                        <i class="kt-nav__link-icon flaticon2-reload"></i>
                                                        Lưu và tiếp tục
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a class="kt-nav__link save_option" data-action="save_exit">
                                                        <i class="kt-nav__link-icon flaticon2-power"></i>
                                                        Lưu & Thoát
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a class="kt-nav__link save_option" data-action="save_create">
                                                        <i class="kt-nav__link-icon flaticon2-add-1"></i>
                                                        Lưu và tạo mới
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-6">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Thông tin cơ bản
                            </h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="kt-form">
                        <div class="kt-portlet__body">
                            <div class="kt-section kt-section--first">
                                @foreach($module['form']['general_tab'] as $field)
                                    @php
                                        $field['value'] = @$result->{$field['name']};
                                    @endphp
                                    <div class="form-group-div form-group {{ @$field['group_class'] }}"
                                         id="form-group-{{ $field['name'] }}">
                                        @if($field['type'] == 'custom')
                                            @include($field['field'], ['field' => $field])
                                        @else
                                            <label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
                                                    <span class="color_btd">*</span>@endif</label>
                                            <div class="col-xs-12">
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                <span class="form-text text-muted">{!! @$field['des'] !!}</span>
                                                <span class="text-danger">{{ $errors->first($field['name']) }}</span>
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->

                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Lịch sửa tiền
                            </h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <?php
                    $harvests = \Modules\A4iSeason\Models\Harvest::where('season_id', $result->id)->orderBy('id', 'desc')->get();
                    $harvest_last = \Modules\A4iSeason\Models\Harvest::where('season_id', $result->id)->orderBy('id', 'desc')->first();
                    $total_price = 0;
                    ?>
                    <div class="kt-portlet__body">
                        <div class="kt-widget6">
                            <div class="kt-widget6__head">
                                <div class="kt-section kt-section--first">
                                    <span>Lịch sử thu hoạch</span>
                                </div>
                            </div>

                            <div class="kt-widget6__body">
                                <?php
                                $k = $harvests->count();
                                ?>

                                @if(\Modules\A4iSeason\Http\Helpers\SeasonHelper::getCostSeason($result->id, false, $harvest_last) != 0)
                                    <div class="kt-widget6__item">
                                            <span class="kt-font-bold">Lần
                                                {{ $k + 1 }} :</span>
                                        <span style="margin-left: 120px" class="kt-font-danger kt-font-bold"></span>
                                        <span class="kt-font-danger kt-font-bold">chi: {{ number_format(\Modules\A4iSeason\Http\Helpers\SeasonHelper::getCostSeason($result->id, false, $harvest_last), 0, '.', '.') }}đ</span>
                                    </div>
                                @endif

                                @foreach($harvests as $key => $harvest)
                                    <?php
                                    $tong_thu = \Modules\A4iSeason\Http\Helpers\SeasonHelper::getPriceHarvest($harvest);
                                    $total_price += $tong_thu;
                                    ?>
                                    <?php
                                    //  Lấy lần thu hoạch trước đó
                                    $tong_chi = \Modules\A4iSeason\Http\Helpers\SeasonHelper::getCostSeason($result->id, $harvest);
                                    ?>

                                    <div class="kt-widget6__item">
                                            <span class="kt-font-bold">Lần
                                                {{$k--}} :</span>
                                        <span style="margin-left: 120px" class="kt-font-danger kt-font-bold">thu: {{ number_format($tong_thu, 0, '.', '.') }}đ</span>
                                        <span class="kt-font-danger kt-font-bold">chi: {{ number_format($tong_chi, 0, '.', '.') }}đ</span>
                                    </div>
                                @endforeach
                            </div>
                            <div class="kt-widget6__foot">
                                <div class="kt-widget6__item">
                                    <span>Tổng thu:</span>
                                    <span class="kt-font-danger kt-font-bold">{{ number_format($total_price , 0, '.', '.') }}đ</span>
                                </div>
                                <div class="kt-widget6__item">
                                    <span>Tổng chi:</span>
                                    <span class="kt-font-danger kt-font-bold">{{ number_format(\Modules\A4iSeason\Http\Helpers\SeasonHelper::getCostSeason($result->id), 0, '.', '.') }}đ</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end::Form-->
                </div>
            </div>

            <div class="col-xs-12 col-md-6">
                <!--begin::Portlet-->

                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                @if(isset($module['form']['image_tab']))
                                    Ảnh
                                @elseif(isset($module['form']['nha_cung_cap']))
                                    Thông tin nhà cung cấp
                                @elseif(isset($module['form']['time_tab']))
                                    Thời gian
                                @endif
                            </h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="kt-form">
                        <div class="kt-portlet__body">
                            <div class="kt-section kt-section--first">
                                @if(isset($module['form']['image_tab']))
                                    @foreach($module['form']['image_tab'] as $field)
                                        @php
                                            $field['value'] = @$result->{$field['name']};

                                        @endphp
                                        <div class="form-group-div form-group {{ @$field['group_class'] }}"
                                             id="form-group-{{ $field['name'] }}">
                                            <label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
                                                    <span class="color_btd">*</span>@endif</label>
                                            <div class="col-xs-12">
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                <span class="form-text text-muted">{!! @$field['des'] !!}</span>
                                                <span class="text-danger">{{ $errors->first($field['name']) }}</span>
                                            </div>
                                        </div>
                                    @endforeach

                                @elseif(isset($module['form']['nha_cung_cap']))

                                    @foreach($module['form']['nha_cung_cap'] as $field)
                                        @php
                                            $field['value'] = @$result->{$field['name']};
                                        @endphp
                                        <div class="form-group-div form-group {{ @$field['group_class'] }}"
                                             id="form-group-{{ $field['name'] }}">
                                            <label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
                                                    <span class="color_btd">*</span>@endif</label>
                                            <div class="col-xs-12">
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                <span class="form-text text-muted">{!! @$field['des'] !!}</span>
                                                <span class="text-danger">{{ $errors->first($field['name']) }}</span>
                                            </div>
                                        </div>
                                    @endforeach

                                @elseif(isset($module['form']['time_tab']))
                                    @foreach($module['form']['time_tab'] as $field)
                                        @php
                                            $field['value'] = @$result->{$field['name']};
                                        @endphp
                                        <div class="form-group-div form-group {{ @$field['group_class'] }}"
                                             id="form-group-{{ $field['name'] }}">
                                            <label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
                                                    <span class="color_btd">*</span>@endif</label>
                                            <div class="col-xs-12">
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                <span class="form-text text-muted">{!! @$field['des'] !!}</span>
                                                <span class="text-danger">{{ $errors->first($field['name']) }}</span>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>

            @include('a4iseason::partials.nguoi_canh_tac')

            <!--end::Portlet-->
            </div>
        </div>
    </form>

@endsection
@section('custom_head')
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset(config('core.admin_asset').'/css/form.css') }}">
@endsection
@section('custom_footer')
    <script src="{{ asset(config('core.admin_asset').'/js/pages/crud/metronic-datatable/advanced/vertical.js') }}"
            type="text/javascript"></script>

    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce_editor.js') }}"></script>
    <script type="text/javascript">
        editor_config.selector = ".editor";
        editor_config.path_absolute = "{{ (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]" }}/";
        tinymce.init(editor_config);
    </script>
    <script type="text/javascript" src="{{ asset(config('core.admin_asset').'/js/form.js') }}"></script>

    @if(!in_array('super_admin', $permissions))
        @if($result->status == 0 || $result->admin_id != \Auth::guard('admin')->user()->id)
            <script>
                $('input, select, textarea').attr('disabled', 'disabled');
            </script>
        @endif
    @endif

@endsection
