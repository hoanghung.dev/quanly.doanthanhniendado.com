@if(isset($item->land->ward->name))<a
        href="/admin/season?search=true&view=all&ward_id={{ @$item->land->ward->id }}">{{ @$item->land->ward->name }}</a>, @endif
@if(isset($item->land->district->name))<a
        href="/admin/season?search=true&view=all&district_id={{ @$item->land->district->id }}">{{ @$item->land->district->name }}</a>, @endif
@if(isset($item->land->province->name))<a
        href="/admin/season?search=true&view=all&province_id={{ @$item->land->province->id }}">{{ @$item->land->province->name }}</a>@endif