<a href="/admin/{{ $module['code'] }}/{{ $item->id }}{{ isset($_GET['season_id']) ? '?season_id=' . $_GET['season_id'] : '' }}"
   target="_blank">
    {{ @$item->{$field['object']}->{$field['display_field']} }}
</a>
<div class="row-actions" style="    font-size: 13px;">
    <span class="edit" title="ID của bản ghi">ID: {{ @$item->id }} | </span>
    <span class="edit"><a
                href="{{ url('/admin/'.$module['code'].'/' . $item->id) }}{{ isset($_GET['season_id']) ? '?season_id=' . $_GET['season_id'] : '' }}"
                title="Sửa bản ghi này">Sửa</a> | </span>
    @if(@\Modules\A4iSeason\Models\Season::find(@$_GET['season_id'])->status == 1 && @\Modules\A4iSeason\Models\Season::find(@$_GET['season_id'])->admin_id == \Auth::guard('admin')->user()->id)
        @if(in_array($module['code'] . '_delete', $permissions))
            <span class="trash"><a class="delete-warning"
                                   href="{{ url('/admin/'.$module['code'].'/delete/' . $item->id) }}{{ isset($_GET['season_id']) ? '?season_id=' . $_GET['season_id'] : '' }}"
                                   title="Xóa bản ghi">Xóa</a> | </span>
        @endif
    @endif
</div>
@if(isset($field['tooltip_info']))
    <div id="tooltip-info-{{$field['name']}}" class="div-tooltip_info" data-modal="{{ $module['modal'] }}"
         data-tooltip_info="{{ json_encode($field['tooltip_info']) }}"><img style="margin-top: 20%;"
                                                                            src="/public/images_core/icons/loading.gif">
    </div>
@endif