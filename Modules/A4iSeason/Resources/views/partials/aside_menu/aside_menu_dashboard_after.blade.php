@if(in_array('season_view', $permissions))
    <li class="kt-menu__item " aria-haspopup="true">
        <a href="/admin/season" class="kt-menu__link ">
            <span class="kt-menu__link-icon">
                <i class="kt-menu__link-icon flaticon2-supermarket"></i>
            </span>
            <span class="kt-menu__link-text">Mùa vụ của tôi</span>
        </a>
    </li>
@endif