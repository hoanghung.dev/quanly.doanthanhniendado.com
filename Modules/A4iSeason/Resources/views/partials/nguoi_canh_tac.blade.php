<div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside">
    <!--Begin::Portlet-->
    <div class="kt-portlet kt-portlet--height-fluid-">
        <div class="kt-portlet__head kt-portlet__head--noborder">
        </div>
        <div class="kt-portlet__body">
            <!--begin::Widget -->
            <div class="kt-widget kt-widget--user-profile-2">
                <div class="kt-widget__head">
                    <div class="kt-widget__media">
                        <img style="max-width:150px;padding-top:5px; cursor: pointer;"
                             src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$result->admin->image, 120, 120) }}"
                             class="file_image_thumb kt-widget__img kt-hidden-" title="CLick để phóng to ảnh">
                        <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                            MP
                        </div>
                    </div>
                    <div class="kt-widget__info">
                        <a href="/admin/profile/{{ @$result->admin->id }}"
                           class="kt-widget__username">{{ @$result->admin->name }}</a>
                        <span class="kt-widget__desc">
                            {{ @$result->admin->organization->name }}
                        </span>
                    </div>
                </div>

                <div class="kt-widget__body">
                    {{--<div class="kt-widget__section">--}}
                    {{--Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of--}}
                    {{--classical.--}}
                    {{--</div>--}}
                    {{--{{dd(\Modules\A4iLand\Models\Land::where('admin_id',1)->count())}}--}}


                    <div class="kt-widget__item">
                        <div class="kt-widget__contact">
                            <span class="kt-widget__label">Email:</span>
                            <a href="#" class="kt-widget__data">{{ @$result->admin->email }}</a>
                        </div>
                        <div class="kt-widget__contact">
                            <span class="kt-widget__label">Điện thoại:</span>
                            <a href="#" class="kt-widget__data">{{ @$result->admin->tel }}</a>
                        </div>
                        <div class="kt-widget__contact">
                            <span class="kt-widget__label">Địa chỉ:</span>
                            <span class="kt-widget__data">{{ @$result->admin->address }}</span>
                        </div>
                    </div>

                    <div class="kt-widget__content">
                        <div class="kt-widget__stats" style="margin-left: 20%">
                            <div class="kt-widget__icon">
                                <i class="flaticon-earth-globe"></i>
                            </div>
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Mảnh đất của tôi</span>
                                <span class="kt-widget__value">{{ number_format(\Modules\A4iLand\Models\Land::where('admin_id', @$result->admin->id)->count(), 0, '.', ',') }}</span>
                            </div>
                        </div>

                        <div class="kt-widget__stats">
                            <div class="kt-widget__icon">
                                <i class="flaticon-shopping-basket"></i>
                            </div>
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Mùa vụ của tôi</span>
                                <span class="kt-widget__value">{{ number_format(\Modules\A4iSeason\Models\Season::where('admin_id', @$result->admin->id)->where('status', 1)->count(), 0, '.', ',') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-widget__footer">
                    <a href="/admin/profile/{{ @$result->admin->id }}" type="button"
                       class="btn btn-label-success btn-lg btn-upper">Xem profile</a>
                </div>
            </div>
            <!--end::Widget -->
        </div>
    </div>
    <!--End::Portlet-->

</div>
