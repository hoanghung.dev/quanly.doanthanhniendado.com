@extends(config('core.admin_theme').'.template')
@section('main')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid {{ @$module['code'] }}"
         style="background: #f2f3f8; margin-top: -25px; padding-top: 25px">
        @include('a4iseason::childs.subcategory_season.subcategory_season')

        <div class="row">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {!! $page_title !!}
                        </h3>
                    </div>

                </div>
                <div class="kt-portlet__body">
                    <!--Begin::Timeline 3 -->
                    <div class="kt-timeline-v2">
                        <div class="kt-timeline-v2__items  kt-padding-top-25 kt-padding-bottom-30">
                            @foreach($items as $k => $arr)
                                <div class="kt-timeline-v2__item">
                                    <span style="margin-left: -19px; font-size: 14px;"
                                          class="kt-timeline-v2__item-time m-timeline-2__item-text">{{ date('d-m-Y', $k) }}</span>

                                    <div class="kt-timeline-v2__item-cricle">
                                        <i class="fa fa-genderless kt-font-danger"></i>
                                    </div>
                                    @foreach($arr as $item)
                                        <div class="kt-timeline-v2__item-text  kt-padding-top-10">
                                            {!! $item['title'] !!}
                                            <br>
                                            {!! $item['content'] !!}
                                        </div>
                                        <div class="kt-list-pics kt-list-pics--sm kt-padding-l-20">
                                            @foreach($item['images'] as $img)
                                                <a href="{{ @$img['href'] }}"><img src="{{ $img['src'] }}"
                                                                                   title="{{ @$img['title'] }}"
                                                                                   class="file_image_thumb"></a>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>

                    </div>
                </div>
                <!--End::Timeline 3 -->
            </div>
        </div>
    </div>
@endsection

@section('custom_head')
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset('/public/backend/themes/metronic1/css/style.bundle.css') }}">
    <style>
        @media (min-width: 768px) {
            .m-timeline-2:before {
                left: 6.89rem;
            }

            .m-timeline-2 .m-timeline-2__items .m-timeline-2__item .m-timeline-2__item-text {
                padding-left: 7rem;
            }

            .m-timeline-2 .m-timeline-2__items .m-timeline-2__item .m-timeline-2__item-cricle {
                left: 6.1rem;
            }
        }

        @media (min-width: 1025px) {
            .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
                padding-top: 54px;
            }
        }

    </style>
@endsection

