<?php

namespace Modules\A4iSetting\Providers;

use App\Models\Roles;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class A4iSettingServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Cấu hình phân quyền trong setting
            $this->addSettingRole();

            $this->rendHeaderTopbar();

            $this->rendAsideMenuUser();

            $this->rendAsideMenu();

            $this->addSettingBanner();
        }
    }

    public function addSettingRole()
    {
        \Eventy::addFilter('setting.custom_module', function ($module) {
            $module['tabs']['role_tab'] = [
                'label' => 'Cấu hình phân quyền',
                'icon' => '<i class="flaticon2-console"></i>',
                'intro' => '',
                'td' => [
                    ['name' => 'role_default_id', 'type' => 'select2_model', 'label' => 'Quyền mặc định sau khi đăng ký tài khoản',
                        'model' => Roles::class, 'display_field' => 'display_name',],
                ]
            ];
            return $module;
        }, 1, 1);
    }

    public function rendAsideMenu()
    {
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('a4isetting::partials.aside_menu.dashboard_after');
        }, 0, 1);
    }

    public function rendAsideMenuUser()
    {
        \Eventy::addFilter('aside_menu.user', function () {
            print '';
        }, 1, 1);
    }

    public function rendHeaderTopbar()
    {
        \Eventy::addFilter('block.header_topbar', function () {
            print view('a4isetting::partials.quick_actions');
        }, 1, 1);
    }

    public function addSettingBanner()
    {
        \Eventy::addFilter('setting.custom_module', function ($module) {
            $module['tabs']['banner_tab'] = [
                'label' => 'Cấu hình banner',
                'icon' => '<i class="flaticon2-photograph"></i>',
                'intro' => '',
                'td' => [
                    ['name' => 'banner_dashboard_top', 'type' => 'file_editor', 'label' => 'Ảnh bìa trang  thống kê',],
                    ['name' => 'background_login', 'type' => 'file_editor', 'label' => 'Ảnh nền trang đăng nhập',],
                ]
            ];
            return $module;
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('a4isetting.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php', 'a4isetting'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/a4isetting');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/a4isetting';
        }, \Config::get('view.paths')), [$sourcePath]), 'a4isetting');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/a4isetting');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'a4isetting');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'a4isetting');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
