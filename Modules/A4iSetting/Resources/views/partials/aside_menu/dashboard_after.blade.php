<li class="kt-menu__section ">
    <h4 class="kt-menu__section-text">Tra cứu</h4>
    <i class="kt-menu__section-icon flaticon-more-v2"></i>
</li>
<li class="kt-menu__item " aria-haspopup="true">
    <a href="/admin/season?view=all" class="kt-menu__link ">
            <span class="kt-menu__link-icon">
                <i class="kt-menu__link-icon flaticon-search-magnifier-interface-symbol"></i>
            </span>
        <span class="kt-menu__link-text">Tra cứu Mùa vụ</span>
    </a>
</li>
<li class="kt-menu__item " aria-haspopup="true">
    <a href="/admin/land?view=all" class="kt-menu__link ">
            <span class="kt-menu__link-icon">
                <i class="kt-menu__link-icon flaticon2-magnifier-tool"></i>
            </span>
        <span class="kt-menu__link-text">Tra cứu đất</span>
    </a>
</li>
<li class="kt-menu__section ">
    <h4 class="kt-menu__section-text">Dữ liệu</h4>
    <i class="kt-menu__section-icon flaticon-more-v2"></i>
</li>