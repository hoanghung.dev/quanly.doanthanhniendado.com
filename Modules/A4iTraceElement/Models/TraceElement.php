<?php

namespace Modules\A4iTraceElement\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TraceElement extends Model
{
    protected $table = 'trace_elements';
    protected $fillable = [
        'name', 'description'
    ];
    public function trace_element()
    {
        return $this->belongsTo(Ward::class, 'trace_element_id');
    }

}
