<?php

namespace Modules\AppCoffeeBill\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'area';
    protected $guarded =[];
    public $timestamps = false;
}
