<?php
namespace Modules\AppCoffeeBill\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\AppCoffeeTheme\Entities\Area;
use Modules\EworkingCompany\Models\Company;
use Modules\EworkingUser\Models\User;
use Modules\AppCoffeeBill\Models\Order;

class Bill extends Model
{

    protected $table = 'bills';

    protected $fillable = [
        'receipt_method' , 'user_gender', 'date' , 'coupon_code' , 'note' , 'status' , 'total_price' , 'user_id', 'user_tel', 'user_name', 'user_email', 'user_address', 'user_wards', 'user_city_id'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function orders() {
        return $this->hasMany(Order::class, 'bill_id', 'id');
    }

    public function area() {
        return $this->belongsTo(\Modules\AppCoffeeBill\Models\Area::class, 'area_id');
    }
    public function choose_table() {
        return $this->belongsTo(Choose_table::class, 'choose_table_id');
    }
}
