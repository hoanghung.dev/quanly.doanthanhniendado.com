<?php

namespace Modules\AppCoffeeBill\Models;

use Illuminate\Database\Eloquent\Model;

class Choose_table extends Model
{
    protected $table = 'choose_table';
    protected $guarded =[];
    public $timestamps = false;
}
