<?php
//if (@$item){
//    $items =@$item;
//}else{
//    $items =@$result;
//}

$fields = [
    0 => 'Chờ duyệt',
    1 => 'Đang giao',
    2 => 'Hoàn thành',
    3 => 'Hủy',
];
$status = '';
if ($item->status == 0) {
    $status = 'Chờ duyệt';
} elseif ($item->status == 1) {
    $status = 'Đang giao';
} elseif ($item->status == 2) {
    $status = 'Hoàn thành';
} elseif ($item->status == 3) {
    $status = 'Hủy';
}

?>
<span class="change-new-status" data-status="{{$item->status}}"
      style="cursor: pointer;
      padding: 11px 10px 10px 10px;
      margin-right: -5px;
      border: 1px solid #e2e5ec;">{{$status}}</span>
<select style="width: 10px; cursor: pointer; display: inline-block; border-radius: unset"
        class="form-control change-fast-status select-status"
        name="status">
    @foreach ($fields as $value => $name)
        <option value='{{ $value }}' {{ ($value == $item->status) ? 'selected' : ''}} >{{ $name }}</option>
    @endforeach
</select>
