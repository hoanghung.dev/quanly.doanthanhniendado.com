<?php
$status_span = '';
if ($bill->status == 0) {
    $status_span = 'Chờ duyệt';
} elseif ($bill->status == 1) {
    $status_span = 'Đang giao';
} elseif ($bill->status == 2) {
    $status_span = 'Hoàn thành';
} elseif ($bill->status == 3) {
    $status_span = 'Hủy';
}
?>
<tr data-row="0" class="kt-datatable__row" id="bill-id-{{$bill->id}}" style="left: 0px;">
    <td style="display: none;" class="id id-{{$bill->id}}">{{$bill->id}}</td>
    <td class="kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check" data-field="ID"><span
                style="width: 20px;">
            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                <input name="id[]" type="checkbox" class="ids" value="{{$bill->id}}">
                <span></span>
            </label>
        </span>
    </td>
    <td data-field="user_name" class="kt-datatable__cell item-user_name">
        <a href="/admin/bill/{{$bill->id}}" target="_blank">
            {{@$bill->user->name}}
        </a>
    </td>
    <td data-field="choose_table_id" class="kt-datatable__cell item-choose_table_id">
        {{@$bill->choose_table->position}}
    </td>
    <td data-field="area_id" class="kt-datatable__cell item-area_id">
        {{@$bill->area->id}}
    </td>
    <td data-field="total_price" class="kt-datatable__cell item-total_price">
        <span>{{number_format($bill->total_price,0,'','.')}}đ</span>
    </td>
    <td data-field="status" class="kt-datatable__cell item-status">
        <span class="change-new-status" data-bill_id="{{$bill->id}}"
              data-status="{{$bill->status}}" style="cursor: pointer;
      padding: 11px 10px 10px 10px;
      margin-right: -5px;
      border: 1px solid #e2e5ec;">{{$status_span}}</span>
        <select style="width: 10px; cursor: pointer; display: inline-block; border-radius: unset"
                class="form-control change-fast-status select-status" data-bill_id="{{$bill->id}}" name="status">
            <option value="0" selected="">Chờ duyệt</option>
            <option value="1">Đang giao</option>
            <option value="2">Hoàn thành</option>
            <option value="3">Hủy</option>
        </select>
    </td>
    <td data-field="updated_at" class="kt-datatable__cell item-updated_at">
        {{\App\Http\Helpers\CommonHelper::formatTimeDay($bill->updated_at)}}
    </td>
    <td data-field="date" class="kt-datatable__cell item-date">
        {{(!empty($bill->date))? \App\Http\Helpers\CommonHelper::formatTimeDay($bill->date) :''}}
    </td>
    <td data-field="show" class="kt-datatable__cell item-show">
        <span style="width: 50px; cursor: pointer!important" data-bill_id="{{$bill->id}}"
              class=" go_home_{{$bill->id}} btn btn-sm btn-label-brand btn-bold">Xem</span>
    </td>
    <td data-field="edit_bill" class="kt-datatable__cell item-edit_bill">
        <a href="/admin/bill/{{$bill->id}}">
            <span style="width: 100px; cursor: pointer!important" data-bill_id="{{$bill->id}}" class="btn btn-sm btn-label-brand btn-bold">Chỉnh sửa</span>
        </a>
    </td>
</tr>