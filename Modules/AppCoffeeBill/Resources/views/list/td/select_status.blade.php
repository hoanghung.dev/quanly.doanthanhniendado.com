<?php
//if (@$item){
//    $items =@$item;
//}else{
//    $items =@$result;
//}

$fields = [
    0 => 'Chờ duyệt',
    1 => 'Đang giao',
    2 => 'Hoàn thành',
    3 => 'Hủy',
];
$status = '';
if ($item->status == 0) {
    $status = 'Chờ duyệt';
} elseif ($item->status == 1) {
    $status = 'Đang giao';
} elseif ($item->status == 2) {
    $status = 'Hoàn thành';
} elseif ($item->status == 3) {
    $status = 'Hủy';
}

?>
<span class="change-new-status" data-status="{{$item->status}}"
      style="cursor: pointer;
      padding: 11px 10px 10px 10px;
      margin-right: -5px;
      border: 1px solid #e2e5ec;">{{$status}}</span>
<select style="width: 10px; cursor: pointer; display: inline-block; border-radius: unset"
        class="form-control change-fast-status select-status"
        name="status">
    @foreach ($fields as $value => $name)
        <option value='{{ $value }}' {{ ($value == $item->status) ? 'selected' : ''}} >{{ $name }}</option>
    @endforeach
</select>
{{--@endif--}}
<script>
    $(document).ready(function () {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1000",
            "hideDuration": "1000",
            "timeOut": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        $('.select-status_' + '{{$item->id}}').change(function () {
            //    Cập nhật lại trong bảng bill cột status và Đổi text của thẻ span
            var status = $(this).val();
            var bill_id = '{{$item->id}}';
            $.ajax({
                url: '{{route('get-change-status-bill')}}',
                data: {
                    'bill_id': bill_id,
                    'status': status,
                },
                type: 'get',
                success: function (result) {
                    window.location.reload();
                    toastr.success('Đổi trạng thái thành công!');
                    {{--$('.change-new-status_' + '{{$item->id}}').text(result.status);--}}
{{--                    $('.change-new-status_' + '{{$item->id}}').attr('data-status', result.data_status);--}}

                }
            });

        });
        //Khi click vào text của status
        var status = $(this).data('status');
        $('body').on('click', '.change-new-status_' + '{{$item->id}}', function () {
            var status = $(this).data('status');
            var bill_id = '{{$item->id}}';
            var that = $(this);
                $.ajax({
                    url: '{{route('get-change-new-status-bill')}}',
                    data: {
                        'bill_id': bill_id,
                        'status': status,
                    },
                    type: 'get',
                    success: function (result) {
                        if(result.stt ===true){
                            window.location.reload();
                            toastr.success('Đổi trạng thái thành công!');
                            // that.text(result.status);
                            // that.attr('data-status', result.data_status);

                        }
                    }
                });
        });
    });
</script>
