<?php
$areas = \Modules\AppCoffeeTheme\Entities\Area::all();
?>
<style>
    .pay_this_tab-group {
        margin-bottom: 1px;
    }

    .pay_this_tab-default {
        margin-bottom: 15px;
    }

    .pay_this_tab-heading {
        cursor: pointer;
    }

    .pay_this_tab-heading {
        border: 1px solid #e5e5e5;
        border-radius: 3px;
        padding: 15px 10px;
    }

    .pay_this_tab-heading > h4 {
        margin: 0;
        font-size: 16px;
    }

    .pay_this_tab-heading > h4 > a {
        color: #0088cc;
    }

    div#pay_this_tab_collapse1 {
        border: 1px solid #e5e5e5;
        margin-top: -2px;
        border-top: none;
        padding: 10px;
    }

    .pay_this_tab-body > label {
        display: inline-block;
        padding: 10px 20px;
    }

    .table_choose {
        position: relative;
        cursor: pointer;
    }

    .table_choose > .table_number {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        color: #fff;
        height: 135px;
        line-height: 135px;
        text-align: center;
        font-size: 24px;
    }

    .table_choose > img {
        width: 100%;
        height: 100%;
    }


</style>
<div class="pay_this_tab">
    @foreach($areas as $k=>$area)
        <div class="pay_this_tab-group">
            <div class="pay_this_tab-default">
                <div class="pay_this_tab-heading {{($k==0)?'data_area':''}}"
                     data-area="{{$area->id}}">
                    <h4 class="pay_this_tab-title">
                        <a href="#collapse1">{{$area->name}}</a>
                    </h4>
                </div>
                <div id="pay_this_tab_collapse1" class="pay_this_tab-collapse ">
                    <div class="pay_this_tab-body">
                        @foreach($area->choose_table as $i=>$choose_table)
                            <?php
                            $count_bill = \Modules\AppCoffeeBill\Models\Bill::where('choose_table_id', $choose_table->id)->whereIn('status', [0, 1])->count();
                            ?>
                            <label   data-table="{{$choose_table->id}}" data-area="{{$area->id}}" class="table_a" >
                                <div  class=" table_choose table_{{$choose_table->id.'_'.$area->id}} table_{{$choose_table->id}}">
                                    <span class="table_number">{{$choose_table->position}}</span>
                                    @if($count_bill == 0)
                                        <img src="{{asset('public/frontend/themes/appcoffee/image/icon_table.png')}}"
                                             alt="Bàn số {{$choose_table->position}}">
                                    @else
                                        <img src="{{asset('public/frontend/themes/appcoffee/image/icon_table_selected.png')}}"
                                             alt="Bàn số {{$choose_table->position}}">
                                    @endif
                                    <input type="radio" name="choosed_table"
                                           value="{{$choose_table->id}}" hidden>
                                </div>
                            </label>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
