{{--            popup thông tin chi tiết đơn hàng--}}
<style>
    .total_cat {
        padding: 10px;
        text-align: right;
    }

    .bill-info {
        display: flex;
        border-bottom: 1px solid #f5f5f5;
    }

    .col-left, .col-right {
        width: 50%;
        padding: 10px 0;
    }

    .col-left {
        font-weight: bold;
    }

    .col-right > p {
        border-bottom: 1px dashed #ccc;
    }

    .thong-tin-khach-hang, .thong-tin-khach-hang-dat-ban {
        width: 500px;
        margin: auto;
        position: fixed;
        z-index: 9999999;
        background: #fff;
        box-shadow: 0 0 12px 0 #ccc;
        border-radius: 5px;
        left: 0;
        right: 0;
        top: 10%;
        padding: 20px;
        font-size: 16px;
    }

    .actions {
        margin-top: 20px;
    }

    .change_status_fasst {
        width: 45%;
    }

    .close_popup {
        width: 45%;
        margin-right: 10%;
        cursor: pointer;
        display: block;
        text-transform: uppercase;
        font-weight: bold;
    }


    .col-left-product {
        font-weight: bold;
        padding: 10px 0;
    }

    .title_pro_cart {
        font-weight: bold;
        padding: 5px;
    }

    .cart_popup_detail {
        display: flex;
    }

    .img_pro_cart {
        width: 15%;
    }

    .img_pro_cart > img {
        width: 100%;
    }

    .count_pro_cart p {
        margin: 0;
    }

    .cart_content_right {
        width: 85%;
    }

    .cart_content {
        border-bottom: 1px dashed;
        padding: 10px;
    }

    .cart_content:last-child {
        border-bottom: 1px solid #f5f5f5;
    }

    .info_cart {
        padding: 5px;
    }
</style>
<?php
$fields = [
    0 => 'Chờ duyệt',
    1 => 'Đang giao',
    2 => 'Hoàn thành',
    3 => 'Hủy',
];
?>
<div class="thong-tin-khach-hang">
    <div class="" style="
    max-height: 400px;
    overflow-y: auto;
">
        <div class="bill-info">
            <div class="col-left">Tên khách:</div>
            <div class="col-right">{{@$bill->user_name}}</div>
        </div>
        <div class="bill-info">
            <div class="col-left">Số điện thoại:</div>
            <div class="col-right">{{@$bill->user_tel}}</div>
        </div>
        <div class="bill-info">
            <div class="col-left">Địa chỉ:</div>
            <div class="col-right">{{@$bill->user_address}}</div>
        </div>
        <div class="bill-info">
            <div class="col-left">Thời gian đặt hàng:</div>
            <div class="col-right">{{\App\Http\Helpers\CommonHelper::formatTimeDay(@$bill->updated_at)}}</div>
        </div>
        <div class="bill-info">
            <div class="col-left">Thời gian muốn nhận:</div>
            <div class="col-right">{{\App\Http\Helpers\CommonHelper::formatTimeDay(@$bill->date)}}</div>
        </div>
        <div class="bill-info-product">
            <div class="col-left-product">Sản phẩm:</div>
            <div class="col-right-product">
                @if(@$bill->orders->count()>0)
                    @foreach(@$bill->orders as $k=>$v)
                        <div class="cart_content">
                            <div class="cart_popup_detail">
                                <div class="img_pro_cart">
                                    <img src="{{asset('public/filemanager/userfiles/'.$v->product_image)}}"
                                         alt="{{$v->product_name}}">

                                </div>
                                <div class="cart_content_right">
                                    <div class="title_pro_cart">{{$v->product_name}}</div>
                                    <div class="info_cart">
                                        <div class="count_pro_cart">
                                            <p><span style="font-weight: bold">Số lượng: </span>{{$v->quantity}}</p>
                                            <p>
                                                <span style="font-weight: bold">Giá:  </span>{{number_format($v->product_price*$v->quantity,0,'','.')}}
                                                <sup>đ</sup></p>
                                        </div>
                                        <div class="remove_pro_cart" data-bill_id="_{{$bill->id}}"
                                             style="text-align: right">
                                            <i>xóa</i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <p>Chưa có mặt hàng nào được chọn!</p>
                @endif
            </div>
        </div>
        <div class="bill-info">
            <div class="col-left">Ghi chú:</div>
            <div class="col-right">{{@$bill->note}}</div>
        </div>

    </div>
    <div class="total_cat">
        <span style="font-weight: bold">Tổng tiền:</span><span style="color: red">{{number_format($bill->total_price,0,'','.')}}<sup>đ</sup></span>
    </div>
    <div class="actions" style="display: flex">
        <div class="close_popup btn btn-danger">
            <span>Đóng</span>
        </div>
        <div class="change_status_fasst">
            <select style=" cursor: pointer; "
                    class="form-control change-fast-status select-status_popup_{{$bill->id}}"
                    name="status">
                @foreach ($fields as $value => $name)
                    <option value='{{ $value }}' {{ ($value == $bill->status) ? 'selected' : ''}} >{{ $name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="thong-tin-khach-hang-dat-ban">
    <div class="">
        <div class="bill-info">
            <div class="col-left">Khu vực</div>
            <div class="col-right">{{@$bill->area->name}}</div>
        </div>
        <div class="bill-info">
            <div class="col-left">Số bàn</div>
            <div class="col-right">{{@$bill->choose_table->position}}</div>
        </div>
        <div class="bill-info-product">
            <div class="col-left-product">Sản phẩm:</div>
            <div class="col-right-product">
                @if(@$bill->orders->count()>0)
                    @foreach(@$bill->orders as $k => $v)
                        @include('appcoffeebill::partials.cart_item')
                    @endforeach
                @else
                    <p>Chưa có mặt hàng nào được chọn!</p>
                @endif
            </div>
        </div>
        <div class="bill-info">
            <div class="col-left">Ghi chú:</div>
            <div class="col-right">{{@$bill->note}}</div>
        </div>
        <div class="actions" style="display: flex">
            <div class="close_popup btn btn-danger">
                <span>Đóng</span>
            </div>
            <div class="change_status_fasst">
                <select style=" cursor: pointer; "
                        class="form-control change-fast-status select-status_popup_this_{{$bill->id}}"
                        name="status">
                    @foreach ($fields as $value => $name)
                        <option value='{{ $value }}' {{ ($value == $bill->status) ? 'selected' : ''}} >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

</div>
<script>
    $(document).ready(function () {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1000",
            "hideDuration": "1000",
            "timeOut": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        $('.select-status_popup_' + '{{$bill->id}}').change(function () {
            //    Cập nhật lại trong bảng bill cột status và Đổi text của thẻ span
            var status = $(this).val();
            var bill_id = '{{$bill->id}}';
            $.ajax({
                url: '{{route('get-change-status-bill')}}',
                data: {
                    'bill_id': bill_id,
                    'status': status,
                },
                type: 'get',
                success: function (result) {
                    window.location.reload();
                    toastr.success('Đổi trạng thái thành công!');
                    {{--$('.change-new-status_' + '{{$bill->id}}').attr('data-status', result.data_status);--}}

                }
            });

        });
        $('.remove_pro_cart').click(function () {
            var bill_id = $(this).data('bill_id');
            $.ajax({
                url: '{{route('get-remove-pro-bill')}}',
                data: {
                    'bill_id': bill_id,
                },
                type: 'get',
                success: function (result) {
                    window.location.reload();
                    toastr.success('Đổi trạng thái thành công!');
                    {{--$('.change-new-status_' + '{{$bill->id}}').attr('data-status', result.data_status);--}}

                }
            });

        });
        $('.select-status_popup_this_' + '{{$bill->id}}').change(function () {
            //    Cập nhật lại trong bảng bill cột status và Đổi text của thẻ span
            var status = $(this).val();
            var bill_id = '{{$bill->id}}';
            $.ajax({
                url: '{{route('get-change-status-bill')}}',
                data: {
                    'bill_id': bill_id,
                    'status': status,
                },
                type: 'get',
                success: function (result) {
                    window.location.reload();
                    toastr.success('Đổi trạng thái thành công!');
                    {{--$('.change-new-status_' + '{{$bill->id}}').attr('data-status', result.data_status);--}}

                }
            });

        });
    });
</script>