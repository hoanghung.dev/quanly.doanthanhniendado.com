<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'bill'], function () {
        Route::get('load-table-status', 'Admin\BillController@loadTableStatus');
        Route::get('get-table-info', 'Admin\BillController@getTableInfo');
        Route::get('get-data-bill-item', 'Admin\BillController@getDataBillItem');
        Route::get('get-data-bill-table', 'Admin\BillController@getDataBillTable');
        Route::get('get-change-status-bill', 'Admin\BillController@getChangeStatusBill')->name('get-change-status-bill');
        Route::get('get-change-new-status-bill', 'Admin\BillController@getChangeNewStatusBill')->name('get-change-new-status-bill');
        Route::get('detail-bill-user', 'Admin\BillController@detailBillUser')->name('detail-bill-user');
        Route::get('detail-bill-table', 'Admin\BillController@detailBillTable')->name('detail-bill-table');
        Route::get('get-remove-pro-bill', 'Admin\BillController@getRemoveProBill')->name('get-remove-pro-bill');

        Route::get('', 'Admin\BillController@getIndex')->name('bill')->middleware('permission:bill_view');
        Route::get('processing', 'Admin\BillController@getIndexProcess')->name('bill_processing')->middleware('permission:bill_view');
        Route::get('publish', 'Admin\BillController@getPublish')->name('bill.publish')->middleware('permission:bill_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\BillController@add')->middleware('permission:bill_add');
        Route::get('delete/{id}', 'Admin\BillController@delete')->middleware('permission:bill_delete');
        Route::post('multi-delete', 'Admin\BillController@multiDelete')->middleware('permission:bill_delete');
        Route::get('search-for-select2', 'Admin\BillController@searchForSelect2')->name('bill.search_for_select2')->middleware('permission:bill_view');
        Route::get('print/{id}', 'Admin\BillController@print')->middleware('permission:bill_view');
        Route::get('{id}', 'Admin\BillController@update')->middleware('permission:bill_view');
        Route::post('{id}', 'Admin\BillController@update')->middleware('permission:bill_edit');
    });
});
