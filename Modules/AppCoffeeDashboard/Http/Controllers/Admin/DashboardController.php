<?php

namespace Modules\AppCoffeeDashboard\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\Admin;
use App\Models\Setting;

use Auth;
use DB;
use Illuminate\Http\Request;
use Mail;


class DashboardController extends Controller
{
    protected $module = [
    ];

    public function dashboard()
    {
        $data['page_title'] = 'Thống kê';
        $data['page_type'] = 'list';
        $this_month = date('m');
        $next_month = strftime('%m',strtotime(strtotime($this_month) . " +1 month"));
        $data['total_bill'] = \Modules\WebBill\Models\Bill::select('id', 'total_price')->where('status', '<>', 3)->get();
        $total_price=0;
        foreach ($data['total_bill'] as $total_price_bill){
            $total_price += $total_price_bill->total_price;
        }
        $data['total_bills'] = \Modules\WebBill\Models\Bill::select('id')->get()->count();
        $data['total_bills_validity'] = \Modules\WebBill\Models\Bill::select('id')->where('status',1)->get()->count();
        $data['total_user'] = \Modules\WebBill\Models\Bill::select('user_name')->get()->count();
        $data['total_price'] = $total_price;


        return view('appcoffeedashboard::dashboard', $data);
    }


    public function tooltipInfo(Request $request)
    {
        $modal = new $request->modal;
        $data['item'] = $modal->find($request->id);
        $data['tooltip_info'] = $request->tooltip_info;

        return view('admin.common.modal.tooltip_info', $data);
    }

    public function ajax_up_file(Request $request)
    {
        if ($request->has('file')) {
            $file = CommonHelper::saveFile($request->file('file'));
        }
        return $file;
    }
}
