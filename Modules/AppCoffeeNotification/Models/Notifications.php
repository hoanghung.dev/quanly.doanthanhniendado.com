<?php

namespace Modules\AppCoffeeNotification\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\EworkingAdmin\Models\Admin;
use Modules\EworkingCompany\Models\Company;
use Modules\EworkingJob\Models\Job;
use Modules\EworkingJob\Models\Task;
use Modules\EworkingProject\Models\Project;
class Notifications extends Model
{
    protected $table = "notifications";
    protected $guarded = [];


    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function job()
    {
        return $this->belongsTo(Job::class, 'job_id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function task()
    {
        return $this->belongsTo(Task::class, 'task_id');
    }


    public function from_admin()
    {
        return $this->belongsTo(Admin::class,'from_admin_id');
    }

    public function to_admin()
    {
        return $this->belongsTo(Admin::class,'to_admin_id');
    }

}
