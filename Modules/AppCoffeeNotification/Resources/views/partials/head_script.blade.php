<link rel="manifest" href="/public/manifest.json">
<!-- OneSignal -->

<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>

<script>
    var OneSignal = window.OneSignal || [];
    console.log(OneSignal);
    /* Why use .push? See: http://stackoverflow.com/a/38466780/555547 */
    OneSignal.push(function () {
        OneSignal.init({
            appId: "420af10d-5030-4f34-af19-68078fd6467c",
        });
        /* In milliseconds, time to wait before prompting user. This time is relative to right after the user presses <ENTER> on the address bar and navigates to your page */
        setTimeout(promptAndSubscribeUser, 3000);
    });

    function promptAndSubscribeUser() {
        OneSignal.isPushNotificationsEnabled(function (isEnabled) {
            if (!isEnabled) {
                OneSignal.showNativePrompt();
            } else {
                console.log(isEnabled)
            }
        });
    }
</script>