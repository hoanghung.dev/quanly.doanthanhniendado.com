<?php


Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::group(['prefix' => 'notifications', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\NotificationController@index');
        Route::get('/count-no-readed', 'Admin\NotificationController@count_no_readed');
        Route::get('{id}/read', 'Admin\NotificationController@read');
    });
});