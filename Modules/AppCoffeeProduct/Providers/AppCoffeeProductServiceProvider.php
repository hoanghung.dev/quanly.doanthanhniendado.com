<?php

namespace Modules\AppCoffeeProduct\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class AppCoffeeProductServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('AppCoffeeProduct', 'Database/Migrations'));

        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Custom setting
            $this->registerPermission();

            //  Cấu hình menu trái
            $this->rendAsideMenu();
        }
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['product_view', 'product_add', 'product_edit', 'product_delete', 'product_publish',
                'category_product_view', 'category_product_add', 'category_product_edit', 'category_product_delete', 'category_product_publish',
                'tag_product_view', 'tag_product_add', 'tag_product_edit', 'tag_product_delete', 'tag_product_publish',]);
            return $per_check;
        }, 1, 1);
    }

    public function rendAsideMenu() {
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('appcoffeeproduct::partials.aside_menu.dashboard_after_product');
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('AppCoffeeProduct', 'Config/config.php') => config_path('appcoffeeproduct.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('AppCoffeeProduct', 'Config/config.php'), 'appcoffeeproduct'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/appcoffeeproduct');

        $sourcePath = module_path('AppCoffeeProduct', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/appcoffeeproduct';
        }, \Config::get('view.paths')), [$sourcePath]), 'appcoffeeproduct');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/appcoffeeproduct');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'appcoffeeproduct');
        } else {
            $this->loadTranslationsFrom(module_path('AppCoffeeProduct', 'Resources/lang'), 'appcoffeeproduct');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('AppCoffeeProduct', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
