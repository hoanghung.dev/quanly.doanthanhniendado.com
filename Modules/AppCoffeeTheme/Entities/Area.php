<?php

namespace Modules\AppCoffeeTheme\Entities;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $guarded = [];
    protected $table = 'area';
    public function choose_table()
    {
        return $this->hasMany(ChooseTable::class, 'area_id', 'id');
    }

}
