<?php

namespace Modules\AppCoffeeTheme\Entities;

use Illuminate\Database\Eloquent\Model;

class ChooseTable extends Model
{
    protected $guarded = [];
    protected $table = 'choose_table';

}
