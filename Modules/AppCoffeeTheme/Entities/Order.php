<?php

namespace Modules\AppCoffeeTheme\Entities;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];
    protected $table = 'orders';
    public function bill()
    {
        return $this->belongsTo(Bill::class, 'bill_id', 'id');
    }
}
