<?php

namespace Modules\AppCoffeeTheme\Entities;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = [];
    protected $table = 'posts';
}
