<?php

namespace Modules\AppCoffeeTheme\Entities;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];
    protected $table = 'products';
//    public function Category()
//    {
//        return $this->belongsTo(Product::class, 'product_id', 'id');
//    }
}
