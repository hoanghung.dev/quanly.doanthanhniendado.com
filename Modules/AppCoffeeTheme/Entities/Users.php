<?php

namespace Modules\AppCoffeeTheme\Entities;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $guarded = [];
    protected $table = 'users';
}
