<?php

namespace Modules\AppCoffeeTheme\Http\Controllers;

use App\Http\Helpers\CommonHelper;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AppCoffeeTheme\Entities\Area;
use Modules\AppCoffeeTheme\Entities\Categories;
use Modules\AppCoffeeTheme\Entities\Choose_table;
use Modules\AppCoffeeTheme\Entities\Post;
use Modules\AppCoffeeTheme\Entities\Product;
use Modules\AppCoffeeTheme\Http\Requests\RegisterRequest;
use Modules\AppCoffeeTheme\Http\Requests\LoginRequest;
use Modules\AppCoffeeTheme\Http\Requests\ForgotRequest;
use Mail;
use Session;
use Validator;
use Auth;


class AppCoffeeThemeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['products'] = Product::select(['image', 'name', 'final_price', 'base_price', 'id'])->orderBy('name', 'asc')->limit(30)->get();
        return view('appcoffeetheme::layouts.index', $data);
    }

    //  Trang danh mục
    public function list($slug)
    {
        $category = Categories::where('slug', $slug)->first();
        if (!is_object($category)) {
            abort(404);
        }
        $data['category'] = $category;
        $data['slug1'] = $slug;

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $category->meta_title != '' ? $category->meta_title : $category->name,
            'meta_description' => $category->meta_description != '' ? $category->meta_description : $category->name,
            'meta_keywords' => $category->meta_keywords != '' ? $category->meta_keywords : $category->name,
        ];
        view()->share('pageOption', $pageOption);

        if (in_array($category->type, [0, 1])) {         //  Danh mục Tin tức
            $posts = CommonHelper::getFromCache('posts_by_multi_cat_like_category_id' . $category->id . @$_GET['page']);
            if (!$posts) {
                $posts = Post::where('status', 1)->where(function ($query) use ($category) {
//                    $cat_childs = $category->childs; //  Lấy các id của danh mục con
                    $query->Where('multi_cat', 'LIKE', '%|' . $category->id . '|%');  // truy vấn các tin thuộc danh mục hiện tại
//                    foreach ($cat_childs as $cat_child) {
//                        $query->orWhere('multi_cat', 'LIKE', '%|' . $cat_child->id . '|%');    //  truy vấn các tin thuộc các danh mục con của danh mục hiện tại
//                    }
                })->orderBy('order_no', 'desc')->orderBy('updated_at', 'desc')->paginate(12);
                CommonHelper::putToCache('posts_by_multi_cat_like_category_id' . $category->id . @$_GET['page'], $posts);
            }
            $data['posts'] = $posts;
            $data['categories'] = Categories::where('status', 1)->where('id', '<>', $category->id)->get();
            return view('appcoffeetheme::layouts.news.news')->with($data);
        } elseif (in_array($category->type, [5, 6])) {
            //  Sản phẩm
            $products = CommonHelper::getFromCache('posts_by_multi_cat_like_category_id' . $category->id . @$_GET['page']);
            if (!$products) {
                $products = Product::where('status', 1)->where(function ($query) use ($category) {
//                    $cat_childs = $category->childs; //  Lấy các id của danh mục con
                    $query->Where('multi_cat', 'LIKE', '%|' . $category->id . '|%');  // truy vấn các product thuộc danh mục hiện tại
//                    foreach ($cat_childs as $cat_child) {
//                        $query->orWhere('multi_cat', 'LIKE', '%|' . $cat_child->id . '|%');    //  truy vấn các tin thuộc các danh mục con của danh mục hiện tại
//                    }
                })->orderBy('order_no', 'desc')->orderBy('updated_at', 'desc')->paginate(12);
                CommonHelper::putToCache('posts_by_multi_cat_like_category_id' . $category->id . @$_GET['page'], $products);
            }

            $data['products'] = $products;

            return view('appcoffeetheme::layouts.index')->with($data);
        }
    }

    public function oneParam($slug1)
    {
        $cate = Categories::where('slug', $slug1)->where('status', 1)->first();
//        if (is_object($cate)) {
//            dd(1);
//            return redirect($slug1 . '/san-pham');
//        }

        //  Chuyen den trang danh sanh tin tuc || Danh sach san pham
        return $this->list($slug1);
    }

    //  VD: hobasoft/card-visit  | danh-muc-cha/danh-muc-con | danh-muc/bai-viet.html | danh-muc/san-pham.html
    public function twoParam($slug1, $slug2)
    {

        if (strpos($slug2, '.html')) {  //  Nếu là trang chi tiết
            $slug2 = str_replace('.html', '', $slug2);
            if (Post::where('slug', $slug2)->where('status', 1)->count() > 0) {
                //  Chi tiet tin tuc
                return $this->detailPost($slug2);
            }
            abort(404);
        }

        return $this->list($slug2);
    }

//    chi tiết tin tức
    public function detailPost($slug)
    {

        $slug = str_replace('.html', '', $slug);
        $data['post'] = Post::where('slug', $slug)->first();
        if (!is_object($data['post'])) {
            abort(404);
        }

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['post']->meta_title != '' ? $data['post']->meta_title : $data['post']->name,
            'meta_description' => $data['post']->meta_description != '' ? $data['post']->meta_description : $data['post']->name,
            'meta_keywords' => $data['post']->meta_keywords != '' ? $data['post']->meta_keywords : $data['post']->name,
        ];
        view()->share('pageOption', $pageOption);

        return view('appcoffeetheme::layouts.news.detail_new', $data);
    }


    public function getLogin()
    {
        $data['page_title'] = 'Đăng nhập';
        $data['page_type'] = 'list';
        return redirect('/dang-nhap');
    }

    public function authenticate(LoginRequest $request)
    {
        $user = User::where('email', $request['email'])->first();
        if (!is_object($user)) {
            return \response()->json([
                'status' => false,
                'val' => "Vui lòng kiểm tra lại Email/Mật khẩu của bạn"
            ]);
        }

        if (@$user->status == 0) {
            return \response()->json([
                'status' => false,
                'val' => "Vui lòng kiểm tra lại Email/Mật khẩu của bạn"
            ]);
        }

        if (@$user->status == -1) {
            return \response()->json([
                'status' => false,
                'val' => "Vui lòng kiểm tra lại Email/Mật khẩu của bạn"
            ]);
        }

        if (\Auth::guard('user')->attempt(['email' => trim($request['email']), 'password' => trim($request['password'])], true)) {

            return \response()->json([
                'status' => true,
                'val' => "Bạn đăng nhập thành công"
            ]);
        } else {
            return \response()->json([
                'status' => false,
                'val' => "Vui lòng kiểm tra lại Email/Mật khẩu của bạn"
            ]);
        }
    }




    public function getRegister()
    {
        $data['page_title'] = 'Đăng ký';
        $data['page_type'] = 'list';
        return view('appcoffeetheme::pages.auth.register', $data);
    }

    public function postRegister(RegisterRequest $request)
    {
        $data =  $request->except('_token');
        $data['password'] = $data['password_md5'] = bcrypt($data['password']);
        $data['api_token'] = base64_encode(rand(1, 100) . time());

        $user = new \Modules\AppCoffeeTheme\Models\User();

        foreach ($data as $k => $v) {
            $user->{$k} = $v;
        }

        $user->save();


        return \response()->json([
            'status' => true,
            'val' => "Bạn đã đăng ký thành công"
        ]);

    }

    public function getForgotPassword(Request $request, $change_password)
    {
        if (!$_POST) {

            $query = \Modules\AppCoffeeTheme\Models\User::where('change_password', $change_password);
            if (!$query->exists() || !isset($change_password)) {
                abort(404);
            }
            $data['page_title'] = 'Lấy lại mật khẩu';
            $data['page_type'] = 'list';
            return view('appcoffeetheme::pages.auth.change_password')->with($data);
        } else {

            if ($request->password == $request->re_password) {
                $user = \Modules\AppCoffeeTheme\Models\User::where('change_password', $change_password)->first();
                $user->password = bcrypt($request->password);
                $user->change_password = $user->id . '_' . time();
                $user->save();
                if (\Auth::guard('user')->attempt(['email' => trim($user->email), 'password' => trim($request['re_password'])], true)) {
                   return redirect('/');
                }
            } else {
                return back()->with('alert_re_password', 'Nhập lại mật khâu không khớp!');
            }
        }
    }


    public function getEmailForgotPassword(ForgotRequest $request)
    {
        if (!$_POST) {
            $data['page_title'] = 'Quên mật khẩu';

            return view('appcoffeetheme::pages.auth.forgot_password')->with($data);

        } else {
            $query = \Modules\AppCoffeeTheme\Models\User::where('email', $request->email);

            if (!$query->exists()) {
                return back()->with('success', 'Email chưa được đăng ký');
            }
            $user = $query->first();
            $user->change_password = $user->id . '_' . time();
            $user->save();

            try {
                \Eventy::action('admin.restorePassword', [
                    'link' => \URL::to('forgot-password/' . @$user->change_password),
                    'name' => @$user->name,
                    'email' => $user->email
                ]);
                return \response()->json([
                    'status' => true,
                    'val' => "Bạn đã đăng ký thành công"
                ]);
            } catch (\Exception $ex) {
                CommonHelper::one_time_message('error', 'Xin vui lòng thử lại!');
            }
        }
    }



}
