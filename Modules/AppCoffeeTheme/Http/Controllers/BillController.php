<?php

namespace Modules\AppCoffeeTheme\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Cart;
use Modules\AppCoffeeTheme\Entities\Bill;
use Modules\AppCoffeeTheme\Entities\Order;
use Modules\AppCoffeeTheme\Entities\Users;
use Modules\AppCoffeeTheme\Http\Requests\CartRequest;

class BillController extends Controller
{
    public function addBill(Request $request)
    {
        if ($request->type == 'home') {
            $user = Users::create([
                'tel' => $request->tel,
                'name' => $request->name,
                'address' => $request->address,
            ]);
            $bill = Bill::create([
                'user_id' => $user->id,
                'user_tel' => $request->tel,
                'user_name' => $request->name,
                'user_address' => $request->address,
                'date' => $request->time,
                'note' => $request->note,
                'total_price' => Cart::total(0, '', '')
            ]);

            foreach (Cart::content() as $k => $orders) {
                $order = Order::create([
                    'bill_id' => $bill->id,
                    'product_price' => $orders->options->priceOfProduct,
                    'product_name' => $orders->name,
                    'product_image' => $orders->options->image,
                    'quantity' => $orders->qty,
                    'product_id' => $orders->id,
                    'price' => $orders->price
                ]);

            }
            Cart::destroy();
        }
        if ($request->type == 'shop') {
            {

                // tai quan - tim don hang truoc no dat
                $bill = Bill::where('choose_table_id', $request->choose_table_id)
                    ->where('area_id', $request->area_id)->whereIn('status', [0, 1])->first();
                if (is_object($bill)) {
                    //  tao them order vao don hang nay & cap nhat lai gia cua don hang
                    $bill = Bill::updateOrCreate(
                        ['id' => $bill->id],
                        [
                            'note' => $request->note,
                            'total_price' => $bill->total_price + Cart::total(0, '', '')
                        ]
                    );
                    foreach (Cart::content() as $k => $orders) {
                        $order = Order::create([
                            'bill_id' => $bill->id,
                            'product_price' => $orders->options->priceOfProduct,
                            'product_name' => $orders->name,
                            'product_image' => $orders->options->image,
                            'quantity' => $orders->qty,
                            'product_id' => $orders->id,
                            'price' => $orders->price
                        ]);
                    }

                    Cart::destroy();
                } else {
//                    dd(Cart::content() );
                    $bill = Bill::create([
                        'note' => $request->note,
                        'area_id' => $request->area_id,
                        'choose_table_id' => $request->choose_table_id,
                        'total_price' => Cart::total(0, '', '')
                    ]);
                    foreach (Cart::content() as $k => $orders) {
                        $order = Order::create([
                            'bill_id' => $bill->id,
                            'product_price' => $orders->options->priceOfProduct,
                            'product_name' => $orders->name,
                            'product_image' => $orders->options->image,
                            'quantity' => $orders->qty,
                            'product_id' => $orders->id,
                            'price' => $orders->price
                        ]);
                    }

                    Cart::destroy();
                }
            }

        }

        \Pusher::trigger('bill', 'update', [
            'bill_id' => $bill->id
        ]);
//        dd($bill->id);
        if ($request->ajax()) {
            return response()->json([
                'cart' => '',
            ]);
        }
    }
}