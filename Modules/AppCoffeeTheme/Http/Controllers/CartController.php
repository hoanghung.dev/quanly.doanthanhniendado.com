<?php

namespace Modules\AppCoffeeTheme\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Cart;
use Modules\AppCoffeeTheme\Entities\Product;

class CartController extends Controller
{
    public function addToCart(request $r)
    {
        $product = Product::find($r->product_id);
        $carts = Cart::add([
            'id' => $product->id,
            'name' => $product->name,
            'qty' => $r->quatity,
            'price' => $product->final_price * $r->quatity,
            'options' => [
                'priceOfProduct' => $product->final_price,
                'image' => $product->image
            ]
        ]);

    }

    public function updateCountCart(request $r)
    {
        $cart = \Cart::content();
        $total = \Cart::total(0, '', '.');
        $html = '';
        $html .= $cart->count() . ' - ' . $total . '<sup>đ</sup>';
        if ($r->ajax()) {
            return response()->json([
                'html' => $html,
            ]);
        }
    }

    public function loadContentCart()
    {
        return view('appcoffeetheme::cart_content');
    }

    public function updateChangeProductInCart(request $r)
    {
        if ($r->count_product == 0 || $r->count_product == ''){
            $r->count_product=1;
        }
//        if($r->count_product<0){
//            $r->count_product=$r->count_product*(-1);
//        }
            $upadteCart = \Cart::update($r->rowId, ['qty' => $r->count_product, 'price' => $r->priceOfProduct]);

        if ($r->ajax()) {
            return \response()->json([
                'priceProduct' => number_format($upadteCart->price * $r->count_product, 0, '', '.'),
                'totalPrice' => \Cart::total(0, '', '.')
            ]);
        }
    }

    public function removeProductInCart(request $r)
    {

        \Cart::remove($r->rowId);

        if ($r->ajax()) {
            return \response()->json([
                'totalPrice' => \Cart::total(0, '', '.')
            ]);
        }
    }
}
