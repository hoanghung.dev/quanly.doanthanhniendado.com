<?php

namespace Modules\AppCoffeeTheme\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AppCoffeeTheme\Entities\Area;
use Modules\AppCoffeeTheme\Entities\Categories;
use Modules\AppCoffeeTheme\Entities\Post;
use Modules\AppCoffeeTheme\Entities\Product;

class NewController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
//        $data['news'] = Post
        $data['menu_childs'] = Categories::where('type', 5)->whereNotNull('parent_id')->get();
        $data['menus'] = Categories::where('type', 5)->whereNull('parent_id')->get();
        $data['products'] = Product::where('type', 1)->where('featured', 1)->orderBy('id', 'desc')->get();
        $data['areas'] = Area::all();
        return view('appcoffeetheme::layouts.news.news', $data);
    }

}
