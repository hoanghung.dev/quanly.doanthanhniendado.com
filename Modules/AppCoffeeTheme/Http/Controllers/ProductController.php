<?php

namespace Modules\AppCoffeeTheme\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AppCoffeeTheme\Entities\Categories;
use Modules\AppCoffeeTheme\Entities\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function searchProduct(Request $request)
    {
        $data['products'] = Product::select('name', 'final_price', 'base_price', 'image', 'id')->where('status', 1);
        if ($request->keyword != null) {
            $data['products'] = $data['products']->where('name', 'like', '%' . $request->keyword . '%');
        }
        if ($request->menu_slug != null) {
            $category = Categories::where('slug', $request->menu_slug)->first();
            $data['products'] = $data['products']->where(function ($query) use ($category) {
                $query->Where('multi_cat', 'LIKE', '%|' . $category->id . '|%');  // truy vấn các product thuộc danh mục hiện tại
                $cat_childs = $category->childs; //  Lấy các id của danh mục con
                foreach ($cat_childs as $cat_child) {
                    $query->orWhere('multi_cat', 'LIKE', '%|' . $cat_child->id . '|%');    //  truy vấn các tin thuộc các danh mục con của danh mục hiện tại
                }
            });
        }
        $data['products'] = $data['products']->get();
        return view('appcoffeetheme::pages.product.search', $data);
    }

}
