<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */
namespace Modules\ThemeEdu\Models ;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model {

    protected $table = 'courses';

    protected $appends = ['category'];

    public function getCategoryAttribute()
    {
        $cat_ids = explode('|', @$this->attributes['multi_cat']);
        return Category::whereIn('id', $cat_ids)->first();
    }

}