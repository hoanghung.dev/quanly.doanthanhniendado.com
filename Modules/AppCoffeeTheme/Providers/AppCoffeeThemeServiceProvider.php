<?php

namespace Modules\AppCoffeeTheme\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class AppCoffeeThemeServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('AppCoffeeTheme', 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('AppCoffeeTheme', 'Config/config.php') => config_path('appcoffeetheme.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('AppCoffeeTheme', 'Config/config.php'), 'appcoffeetheme'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/appcoffeetheme');

        $sourcePath = module_path('AppCoffeeTheme', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/appcoffeetheme';
        }, \Config::get('view.paths')), [$sourcePath]), 'appcoffeetheme');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/appcoffeetheme');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'appcoffeetheme');
        } else {
            $this->loadTranslationsFrom(module_path('AppCoffeeTheme', 'Resources/lang'), 'appcoffeetheme');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('AppCoffeeTheme', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
