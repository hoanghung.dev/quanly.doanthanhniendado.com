<?php
$cart = \Cart::content();
$total = \Cart::total(0, '', ',');
?>
<div class="cart_popup_content pad_popup">
@if($cart->count()>0)
    @foreach($cart as $k=>$v)
        @include('appcoffeetheme::partials.cart_item')
    @endforeach
@else
    <p>Bạn chưa chọn sản phẩm nào!</p>
@endif
</div>
{{--</div>--}}
<div class="cart_popup_total">
    <b>Tổng: <span style="color: red">{{$total}}</span><sup style="color: red">đ</sup></b>
</div>