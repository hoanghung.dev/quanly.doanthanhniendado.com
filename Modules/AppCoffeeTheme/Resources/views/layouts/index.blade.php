@extends('appcoffeetheme::layouts.master.master')

@section('content')
    <?php
    $menu_childs = \Modules\AppCoffeeTheme\Entities\Categories::where('type', 5)->whereNotNull('parent_id')->get();
    $menus = \Modules\AppCoffeeTheme\Entities\Categories::where('type', 5)->whereNull('parent_id')->get();
    ?>
    <div role="main">
        <div class="search-items-container">
            <nav class="stardust-tabs-header-wrapper" style="top: 54px;">
                <ul class="stardust-tabs-header stardust-tabs-header--light stardust_overflow">
                    @foreach($menus as $k => $menu)
                        <li class="stardust-tabs-header__tab tabs_header_click"
                            onclick="searchProduct(null, '{{ $menu->slug }}');">
                            <div class="search-items-tabs-header">{{$menu->name}}</div>
                        </li>
                    @endforeach
                </ul>
                <div class="search-items-container__filter-bar-wrapper"
                     style="top: 97px;transform: translateY(-47.7px);height: auto;transform: unset;">

                    @foreach($menus as $k=>$v)
                        <div class="filter-bar hairline-border-bottom" style="{{($k!=0)?'display: none':''}}">
                            @foreach($menu_childs as $menu_child)
                                @if($menu_child->parent_id == $v->id)
                                    <button class="stardust-button filter-bar-item hairline-border typo-r12 filter-bar-products btn-{{ $menu_child->id }}"
                                            onclick="searchProduct(null, '{{ $menu_child->slug }}'); activeItemMenu('{{ $menu_child->id }}');"><span
                                                class="filter-icon-container">
                                                    <a class="menu-item">
                                                        <svg
                                                                class="stardust-icon stardust-icon-tick filter-icon-container__icon"
                                                                enable-background="new 0 0 15 15" viewBox="0 0 15 15"><path
                                                                    stroke="none"
                                                                    d="m6.5 13.6c-.2 0-.5-.1-.7-.2l-5.5-4.8c-.4-.4-.5-1-.1-1.4s1-.5 1.4-.1l4.7 4 6.8-9.4c.3-.4.9-.5 1.4-.2.4.3.5 1 .2 1.4l-7.4 10.3c-.2.2-.4.4-.7.4 0 0 0 0-.1 0z"></path></svg></span>
                                        <div class="filter-bar-item__inner">{{$menu_child->name}}</div>
                                        </a>
                                    </button>
                                @endif
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </nav>
            <div class="search-items-container__tab-panel ">

                <div class="item-card-list get-search list-product">

                    @foreach($products as $k=>$product)
                        <div class="item-card-list__item-card-wrapper" data-id="{{ $product->id }}"
                             data-price="{{ $product->final_price }}">
                            <a class="_38_74r" href="#">
                                <div class="VqaMtr">
                                    <div class="_3ZDC1p _1tDEiO">
                                        <img width="invalid-value" height="invalid-value"
                                             class="_1T9dHf _3XaILN"
                                             style="object-fit: contain"
                                             src="{{asset('public/filemanager/userfiles/'.$product->image)}}">
                                    </div>
                                    @if($product->final_price !=0 && $product->final_price < $product->base_price)
                                        <div class="FsMOUG">
                                            <div class="badge-text badge-text--promotion">
                                                <div class="badge-text__text typo-r12">
                                                    <div class="badge__promotion">{{CEIL(($product->base_price - $product->final_price)*100 / $product->base_price)}}
                                                        %
                                                        <div class="badge__promotion-off">GIẢM</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="_26_GQ0">
                                    <div class="typo-r14 _2-Dw3_ i5LZ4j">{{$product->name}}</div>
                                    <div class="item-promotion-labels _1xDT8N _2ApczS"></div>
                                    <div class="_34q0o4 price">
                                        @if($product->final_price >0 && $product->final_price < $product->base_price)
                                            <div class="_1w9jLI typo-l12 _3EWU1m price_base">{{number_format($product->base_price, 0, '', '.')}}
                                                <span>₫</span></div>
                                            <div class="kD6vRc"></div>
                                            <div class="_1w9jLI typo-r16 _1k557v" style="max-width: calc(100% - 24px);">
                                                <span class="price_down">{{number_format($product->final_price, 0, '', '.')}}</span>
                                                <span class="_1q8LLE">₫</span>
                                            </div>
                                        @elseif($product->final_price ==0 && $product->base_price>0 )
                                            <div class="_1w9jLI typo-r16 _1k557v" style="max-width: calc(100% - 24px);">
                                                <span class="price_down">{{number_format($product->base_price, 0, '', '.')}}</span>
                                                <span class="_1q8LLE">₫</span>
                                            </div>
                                        @elseif($product->base_price ==0 && $product->final_price>0 )
                                            <div class="_1w9jLI typo-r16 _1k557v" style="max-width: calc(100% - 24px);">
                                                <span class="price_down">{{number_format($product->final_price, 0, '', '.')}}</span>
                                                <span class="_1q8LLE">₫</span>
                                            </div>
                                        @endif
                                    </div>

                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
