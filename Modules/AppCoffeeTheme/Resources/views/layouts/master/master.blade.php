<?php
$cart = \Cart::content();
$total = \Cart::total(0, '', ',');
//\Cart::destroy();
$areas = \Modules\AppCoffeeTheme\Entities\Area::all();
//dd(Cart::content());
?>
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Module AppCoffeeTheme</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/215.618021bfedc51cdf010d.css"
          rel="stylesheet">

    <link rel="stylesheet" type="text/css"
          href="{{ asset('public/libs/toastr/toastr.min.css') }}">

    <link rel="apple-touch-icon" size="120x120"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/ios_icon_120x120.08d46c48da55b89fdef12e80e76e9cdb.png">
    <link rel="apple-touch-icon" size="180x180"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/ios_icon_180x180.224ab8f100a145e21847f33b65f45db0.png">
    <link rel="apple-touch-startup-image"
          media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/ios_splash_screen_640x1136.44f3b1919faea215b1e8a8f33bc85eb2.png">
    <link rel="apple-touch-startup-image"
          media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/ios_splash_screen_750x1334.e7f516f762373cc6c316d37455600b8a.png">
    <link rel="apple-touch-startup-image"
          media="(device-width: 621px) and (device-height: 1104px) and (-webkit-device-pixel-ratio: 3)"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/ios_splash_screen_1242x2208.0030ddfcb47d1a8f7f34ea1f488e063d.png">
    <link rel="apple-touch-startup-image"
          media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/ios_splash_screen_1125x2436.d03df48b337e7e7f19d94203a800123b.png">
    <link rel="apple-touch-startup-image"
          media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/ios_splash_screen_828x1792.848d89109908599fcf6edf6e0b7c6cde.png">
    <link rel="apple-touch-startup-image"
          media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3)"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/ios_splash_screen_1242x2688.806794fa2096fd0d63f138f9380033ab.png">
    <link as="style"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/215.618021bfedc51cdf010d.css"
          rel="preload">
    <link as="script"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/bundle.618021bfedc51cdf010d.js"
          rel="preload">
    <link as="script"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/modules.415010830c21b9f4f17a.js"
          rel="preload">
    <link as="script"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/webpack-runtime.5407922baf58f47f23bc.js"
          rel="preload">
    <link rel="preload" as="fetch" crossorigin="anonymous"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/translation/vi.1580354445.json">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('public/frontend/themes/appcoffee/css/custom.css') }}?v={{ date('s') }}">
    <link rel="stylesheet" type="text/css"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/165.9bba75cdb6a1bd96e4be.css">
    <link rel="stylesheet" type="text/css"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/1.c2326b81f53a35644d18.css">
    <link rel="stylesheet" type="text/css"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/3.deff3e271deadc0c25ed.css">
    <link rel="stylesheet" type="text/css"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/157.bb66ce3faeb24eb68921.css">
    <link rel="canonical" href="https://shopee.vn/" data-rh="true">
    <link rel="stylesheet" type="text/css"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/20.c464d5e616c8b1a284c9.css">
    <link rel="stylesheet" type="text/css"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/49.a09595981a4e3dab5544.css">
    <link rel="stylesheet" type="text/css"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/41.2891c43066722e9b446e.css">
    <link rel="stylesheet" type="text/css"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/167.b8a91a2fee3e92e1fed4.css">

    <link rel="stylesheet" type="text/css"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/43.9966f1806cac3bd3bfbb.css">
    <link rel="stylesheet" type="text/css"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/32.3a75f8ad6f3a4e6ac781.css">
    <link rel="stylesheet" type="text/css"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/38.6a4a774b8294ce976a23.css">
    <link rel="stylesheet" type="text/css"
          href="https://deo.shopeemobile.com/shopee/shopee-mobilemall-live-sg/assets/45.9f5cad757f9ba4e719cb.css">
    <style>
        .forgot_form {
            position: fixed;
            top: 10%;
            background: #fff;
            z-index: 999;
            left: 37%;

        }

        .register_form {
            position: fixed;
            top: 10%;
            background: #fff;
            z-index: 999;
            left: 37%;
        }

        .login_form {
            position: fixed;
            top: 10%;
            background: #fff;
            z-index: 999;
            left: 37%;

        }

        .cart_popup_content {
            max-height: 300px;
            overflow-y: auto;
        }

        .table_choose {
            position: relative;
            /*display: inline-block;*/
            /*margin: 0 10px 20px 0;*/
            cursor: pointer;
        }

        .table_choose > .table_number {
            /*content: '1';*/
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            color: #fff;
            height: 98px;
            line-height: 98px;
            text-align: center;
            font-size: 16px;
        }

        .pay_this_tab {
            height: 350px;
            overflow-y: auto;
        }

        input, textarea {
            border: 1px solid rgba(0, 0, 0, .14);
            border-radius: 2px;
            font-size: 16px;
        }

        /*.pay_this_tab {*/
        /*    height: 50vh;*/
        /*    overflow: auto;*/
        /*}*/
        .item-card-list {
            margin-top: 40px;
        }

        .app-container {
            margin-top: 0;
        }

        .stardust_overflow::-webkit-scrollbar {
            height: 7px;
        }

        .stardust_overflow::-webkit-scrollbar-thumb {
            background: #ccc;
        }

        .filter-bar::-webkit-scrollbar {
            height: 7px;
        }

        .filter-bar::-webkit-scrollbar-thumb {
            background: #ccc;
        }

        .stardust_overflow {
            max-width: 100%;
            overflow-x: auto;
        }

        .stardust_overflow > li {
            width: 20%;
            overflow-x: hidden;
        }

        .filter-bar > .filter-bar-item {
            min-width: calc(20% + 1px - .5rem) !important;
        }

        .filter-bar {
            overflow-x: auto;
        }

        .stardust_dot {
            position: relative;
        }

        .menu_login {
            display: none;
            background: #fff;
            border: 1px solid #ddd;
            padding: 5px;
            margin: 0;
            border-radius: 3px;
        }

        .stardust_dot:hover .menu_login {
            display: block;
            position: absolute;
            top: 100%;
            width: 100px;
            right: 0;

        }

        .pay_this_tab_active {
            display: block;
        }

        .pay_this_tab_no_active {
            display: none;
        }

        .menu_login > li:hover {
            background: #f2f2f2;
        }

        .menu_login > li > a {
            color: #000;
            cursor: pointer;
            display: block;
        }

        .menu_login > li {
            list-style: none;
            padding: 5px 0;
            border-bottom: 1px solid #ddd;
        }

        .icon_menu {
            padding-left: 20px;
        }

        .icon_menu > li {
            list-style: none;
            height: 3px;
            border: 1px solid #ccc;
            margin-bottom: 3px;
            width: 20px;
            background: #ccc;
            cursor: pointer;
        }

        .total_pro {
            display: flex;
        }

        .count_pro_pop > div > input {
            padding: 5px;
            margin: 0 5px;

            font-size: 30px;
        }

        @media (min-width: 469px) and (max-width: 768px) {
            .pay_popup {
                left: 15% !important;
                right: 15% !important;
            }
        }

        @media (max-width: 768px) {
            .count_pro_pop > div > input {
                margin: 0 !important;
                width: 100%;
            }

            /*.total_pro {*/
            /*    width: 70% !important;*/
            /*}*/
            .count_pro_pop {
                width: 100%;
            }
        }

        @media (max-width: 468px) {
            .pay_popup {
                left: 2% !important;
                right: 2% !important;
            }

            .count_pro_pop {
                width: 100%;
            }

            .stardust_overflow > li {
                width: 25%;
                overflow-x: hidden;
            }

            .cart_mobie {
                display: block !important;
            }

            .stardust_dot {
                margin-left: 10px;
            }

            .filter-bar > .filter-bar-item {
                min-width: calc(25% + 1px - .5rem) !important;
            }

        }


        /*    popup product */
        /*.pro_popup_detail {*/
        /*    display: flex;*/
        /*    font-size: 16px;*/
        /*}*/

        .pro_popup {
            display: none;
        }

        .login_form {
            display: none;
        }

        .register_form {
            display: none;
        }

        .forgot_form {
            display: none;
        }

        .cart_popup_detail {
            display: flex;
            border-bottom: 1px solid #f5f5f5;
            padding: 10px 0;
        }

        .count_pro_pop {
            /*width: 50%;*/
            margin: auto;
            /*padding-left: 5px;*/
            /*padding-right: 5px;*/
            display: flex;
        }


        .price_pro_pop {
            margin: auto;
            /*display: flex;*/
            font-size: 16px;
        }

        .pad_popup {
            padding: 20px;
        }

        /*span.oder_pro, span.close_pro {*/
        /*    background: #3a5998;*/
        /*    padding: 10px 15px;*/
        /*    border-radius: 3px;*/
        /*    font-size: 14px;*/
        /*    margin: auto;*/
        /*    cursor: pointer;*/
        /*    display: inline-block;*/
        /*}*/
        span.close_pro {
            background-color: #fff;
            color: rgba(0, 0, 0, .54);
            /*margin-right: 20px;*/
        }

        span.oder_pro {
            color: #fff;
        }

        span.oder_pro1 {
            color: #fff;
        }

        button.oder_pro1 {
            color: #fff;
        }

        /*.pro_popup {*/
        /*    top: 10vh;*/
        /*    font-size: 12px;*/
        /*    z-index: 999;*/
        /*    background: #fff;*/
        /*    position: fixed;*/
        /*    left: 30%;*/
        /*    right: 30%;*/
        /*    border: 1px solid red;*/
        /*    border-radius: 3px;*/
        /*    padding: 20px;*/
        /*}*/

        @media (max-width: 468px) {
            /*.pro_popup_detail {*/
            /*    display: block;*/
            /*}*/
            .pro_popup {
                font-size: 14px;
            }

            /*.price_pro_pop {*/
            /*    width: 100%;*/
            /*    padding-top: 10px;*/
            /*}*/
            .count_pro_pop > input {
                width: 100%;
            }
        }

        /*    popup cart */
        .cart_popup {
            display: none;
        }

        /*.cart_popup_detail {*/
        /*    display: flex;*/
        /*    border-bottom: 1px solid red;*/
        /*    padding: 10px 0;*/
        /*    text-align: center;*/
        /*}*/

        .pro_del_cart {
            width: 5%;
            margin: auto;
        }

        .img_pro_cart {
            position: relative;
            width: 20%;
            margin: auto;
        }

        .img_pro_cart > img {
            width: 100%;
        }

        .title_pro {
            width: 100%;
            text-align: left;
            margin: auto;
            padding-left: 5px;
            padding-right: 5px;
            font-size: 16px;
            font-weight: 600;
        }

        .count_pro {
            width: 60%;
            margin: auto;
            padding-left: 5px;
            padding-right: 5px;
        }

        .count_pro > input {
            width: 100%;
            margin-top: 5px;
            padding: 5px;
        }

        .price_pro {
            width: 40%;
            margin: auto;
            padding-left: 5px;
            padding-right: 5px;
            font-size: 16px;
            color: red;
        }

        button.oder_cart {
            background: none;
            padding: 10px 15px;
            border: 1px solid red;
            margin-top: 25px;
            float: right;
        }

        /*.cart_popup {*/
        /*    top: 10vh;*/
        /*    font-size: 12px;*/
        /*    z-index: 999;*/
        /*    background: #fff;*/
        /*    position: fixed;*/
        /*    margin-left: 70px;*/
        /*    width: 450px;*/
        /*    border: 1px solid red;*/
        /*    border-radius: 3px;*/
        /*    padding: 20px;*/
        /*}*/

        .cart_popup_total {
            padding: 20px;
            font-size: 16px;
            text-align: right;
        }

        @media (max-width: 468px) {
            .cart_popup, .pro_popup {
                left: 5%;
                right: 5%;
                margin-left: unset;
                width: unset;
            }

            /*.img_pro {*/
            /*    display: none;*/
            /*}*/
            .img_pro > b {
                display: none;
            }

            .title_pro, .price_pro {
                font-size: 14px;
            }

            /*.count_pro {*/
            /*    width: 30%;*/
            /*}*/
            .price_pro {
                width: 50%;
            }

            .count_pro {
                width: 50%;
            }
        }

        .delete_product_cart {
            border: 1px solid red;
            border-radius: 50%;
            padding: 1px 5px;
            cursor: pointer;
        }

        /*    Thanh toán*/
        .pay_popup, .cart_popup, .pro_popup {
            display: none;
            top: 10vh;
            font-size: 12px;
            z-index: 999;
            background: #fff;
            position: fixed;
            left: 0;
            right: 0;
            border-radius: 3px;
            /*padding: 20px;*/
            width: 500px;
            margin: auto;
            box-shadow: 0 3px 10px 0 rgba(0, 0, 0, .14);
        }

        /*.pay_popup{*/
        /*    bottom: 30px;*/
        /*    top: 30px;*/
        /*    overflow-y: auto;*/
        /*}*/
        span.oder_pay, span.close_pay {
            background: #3a5998;
            display: inline-block;
            width: 50%;
            padding: 10px 0;
            border-radius: 3px;
            font-size: 16px;
            /*margin: auto;*/
            cursor: pointer;
        }

        span.oder_pro, span.close_pro {
            background: #3a5998;
            /*padding: 10px 80px;*/
            border-radius: 3px;
            font-size: 14px;
            cursor: pointer;
            display: inline-block;
            width: 50%;
            padding: 10px 0;
        }

        span.oder_pro1, span.close_pro1 {
            background: #3a5998;
            /*padding: 10px 95px;*/
            border-radius: 3px;
            font-size: 14px;
            /*margin: auto;*/
            cursor: pointer;
            display: inline-block;
            width: 50%;
            padding: 10px 0;
        }

        button.oder_pro1, button.close_pro1 {
            background: #3a5998;
            /*padding: 10px 95px;*/
            border-radius: 3px;
            font-size: 14px;
            /*margin: auto;*/
            cursor: pointer;
            display: inline-block;
            width: 50%;
            padding: 10px 0;
        }

        .table_choose > img {
            width: 100%;
            height: 100%;
        }

        .pay_this_tab-body > label {
            display: inline-block;
            width: 98px;
            height: 98px;
            padding: 10px 20px;
        }

        .pay_input > textarea {
            width: 438px;
            padding: 10px;
            font-size: 16px;
        }

        .pay_input > input {
            width: 438px;
            padding: 10px;
            font-size: 16px;
        }

        .pay_this_tab-group > textarea {
            width: 438px;
            padding: 10px;
            font-size: 16px;
        }

        @media (min-width: 469px) and (max-width: 768px) {
            .pay_input > textarea, .pay_this_tab-group > textarea, .pay_input > input {
                width: 238px;
            }

            .pay_popup, .cart_popup, .pro_popup, .login_form, .register_form, .forgot_form {
                left: 0 !important;
                right: 0 !important;
                width: 400px;
            }

            .pay_this_tab-body > label {
                display: inline-block;
                width: 90px;
                height: 90px;
                padding: 10px;
            }

            .pay_this_tab {
                height: 280px;
            }

            .table_choose > .table_number {
                height: 90px;
                line-height: 90px;
            }

            /*span.oder_pro, span.close_pro {*/
            /*    padding: 10px 55px;*/
            /*}*/
            /*span.oder_pro1, span.close_pro1 {*/
            /*    padding: 10px 70px;*/
            /*}*/
        }

        @media (max-width: 468px) {
            .pay_input > textarea, .pay_this_tab-group > textarea, .pay_input > input {
                width: 238px;
            }

            .pay_this_tab {
                height: 280px;
            }

            .pay_popup, .cart_popup, .pro_popup {
                left: 0 !important;
                right: 0 !important;
                width: 300px;
            }

            .pay_this_tab-body > label {
                display: inline-block;
                width: 57px;
                height: 57px;
                padding: 10px;
            }

            .table_choose > .table_number {
                height: 57px;
                line-height: 57px;
                font-size: 11px;
            }

            /*span.oder_pay, span.close_pay {*/
            /*    padding: 10px 13px !important;*/
            /*}*/
            /*span.oder_pro, span.close_pro {*/
            /*    padding: 10px 30px !important;*/
            /*}*/
            /*span.oder_pro1, span.close_pro1 {*/
            /*    padding: 10px 45px !important;*/
            /*}*/
        }


        .pay_input {
            margin: 10px 0;
            font-size: 14px;
        }


        .pay_tab {
            text-align: right;
            margin-bottom: 22px;
        }

        .pay_home {
            border-right: 1px solid rgb(234, 234, 234);
            padding-right: 22px;
            font-size: 16px;
            cursor: pointer;
        }

        .pay_this {
            font-size: 16px;
            cursor: pointer;
            padding-left: 20px;
        }

        .pay_active {
            color: red;
        }

        span.close_pay, span.close_pro, span.close_pro1 {
            background-color: #fff;
            color: rgba(0, 0, 0, .54);
            margin-right: 1%;
        }

        span.oder_pay {
            color: #fff;
        }

        .pay_this_tab-heading > h4 {
            margin: 0;
            font-size: 16px;
        }

        .pay_this_tab-heading > h4 > a {
            color: #0088cc;
        }

        .pay_this_tab-heading {
            border: 1px solid #e5e5e5;
            border-radius: 3px;
            padding: 15px 10px;
        }

        .pay_this_tab-default {
            margin-bottom: 15px;
        }

        div#pay_this_tab_collapse1 {
            border: 1px solid #e5e5e5;
            margin-top: -2px;
            border-top: none;
            padding: 10px;
            /*display: none;*/
        }

        .pay_this_tab-group {
            margin-bottom: 1px;

        }

        .pay_this_tab-heading {
            cursor: pointer;
        }


        .col-left-pro {
            font-size: 20px;
            text-align: center;
        }

        .remove_pro_cart {
            /*!*position: absolute;*!*/
            /*right: -10px;*/
            /*top: -10px;*/
            color: red;
        }

        .prices_pro_pop {
            width: 50%;
            margin: auto;
            padding-left: 5px;
            font-size: 16px;
        }

        .cart_content_right {
            width: 80%;
            padding: 10px 0 10px 10px;
        }

        .info_cart {
            display: flex;
        }

        .count_pro_cart {
            display: flex;
            width: 65%;
        }

        .title_pro_cart {
            text-align: left;
            padding: 5px;
            font-size: 14px;
            font-weight: bold;
            text-transform: uppercase;
        }

        .price_pro_cart {
            margin: auto 0;
            font-size: 16px;
            text-align: right;
            width: 35%;
        }

        .down_pro_cart, .up_pro_cart {
            line-height: 21px;
        }

        @media (max-width: 468px) {
            /*.info_cart {*/
            /*    display: block;*/
            /*}*/
            .title_pro_cart {
                font-size: inherit;
            }

            .down_pro_cart, .up_pro_cart {
                line-height: 26px;
            }

            .count_pro_cart {
                width: unset;
            }

            .price_pro_cart {
                padding-top: 5px;
            }

            .pro_del_cart {
                width: 10%;
            }
        }

        .col-left-pro_price {
            font-size: 30px;
            color: red;
            text-align: center;
            font-weight: bold;
        }

        .filter-bar-products.active {
            background: #ee4d2d !important;
            font-weight: bold !important;
            color: #fff !important;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body style="position:relative;">
<div id="app">
    <div class="app-container">
        <div class="search-page">
            <svg class="scroll-to-top" xmlns="http://www.w3.org/2000/svg" width="44" height="44" version="1"
                 style="display: none;">
                <defs>
                    <circle id="b" cx="22" cy="21" r="20"></circle>
                    <filter id="a" width="118%" height="118%" x="-9%" y="-6%" filterUnits="objectBoundingBox">
                        <feOffset dy="1" in="SourceAlpha" result="f"></feOffset>
                        <feGaussianBlur in="f" result="f" stdDeviation="1"></feGaussianBlur>
                        <feComposite in="f" in2="SourceAlpha" operator="out" result="f"></feComposite>
                        <feColorMatrix in="f" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.09 0"></feColorMatrix>
                    </filter>
                </defs>
                <g fill="none" fill-rule="evenodd">
                    <use fill="#000" filter="url(#a)" xlink:href="#b"></use>
                    <use fill="#fff" xlink:href="#b"></use>
                    <circle cx="22" cy="21" r="20"></circle>
                    <path fill="#EE4D2D" d="M12 13h20v2H12zm11 7v13h-2V20l-6 6-1-2 8-8 9 8-2 2z"></path>
                </g>
            </svg>
            <div class="shopee-navbar navbar-search">
                <div class="shopee-navbar__left-btn">
                    <a href="/" title='cafe'>
                        <img src="https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-1/c44.0.200.200a/p200x200/44908363_318111248987179_8344394270920146944_n.png?_nc_cat=104&amp;_nc_ohc=qPwNdqYfy5MAX_NK5aR&amp;_nc_ht=scontent.fhan2-4.fna&amp;oh=f94f070b3bcbfefc326209259dbd968e&amp;oe=5ED96BB4"
                             style="
    max-width: 36px;
">
                    </a>
                </div>
                <div class="shopee-navbar__main-content">
                    <div class="search-bar">
                        <svg viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg" class="search-bar__search-icon">
                            <g fill-rule="evenodd">
                                <path d="M6.5 12C9.537 12 12 9.537 12 6.5S9.537 1 6.5 1 1 3.463 1 6.5 3.463 12 6.5 12z"></path>
                                <path d="M10.69873047 10.76025391L13 13" stroke-linecap="round"
                                      stroke-linejoin="round"></path>
                            </g>
                        </svg>
                        <form role="search" class="search-bar__form" style="
    margin-bottom: 0px;
">
                            <div class="stardust-input"><input class="stardust-input__element search-bar__input"
                                                               type="search" aria-label="Tìm Shopee" autocomplete="off"
                                                               name="search" placeholder="Tìm kiếm" value="">
                            </div>
                        </form>
                        <span class="search-bar__search-icon" id="remove-input-search"
                              style="cursor: pointer; display: none;"
                              onclick="$('input[name=search]').val(''); $(this).hide(); searchProduct(null, null);">
                            x
                        </span>
                        <div></div>
                    </div>
                </div>
                <div class="shopee-navbar__right-part">
                    <div class="shopee-navbar__right-part-btn">
                        <div class="navbar-search__filter-wrapper cart_mobie" id="cart_mobie1">
                            <svg class="shopee-svg-icon navbar__link-icon icon-shopping-cart-2 cart_mobie"
                                 viewBox="0 0 26.6 25.6"
                                 style="
    fill: currentColor;
    width: 26px;
    height: 26px;
    cursor: pointer;
    color: #ee4d2d;
    stroke: #ee4d2d;
    font-size: 1.0625rem;
    color: #ee4d2d;
    margin-right: .625rem;
    font-family: Helvetica Neue,Helvetica,Arial,文泉驛正黑,WenQuanYi Zen Hei,Hiragino Sans GB,儷黑 Pro,LiHei Pro,Heiti TC,微軟正黑體,Microsoft JhengHei UI,Microsoft JhengHei,sans-serif;
">
                                <polyline fill="none" points="2 1.7 5.5 1.7 9.6 18.3 21.2 18.3 24.6 6.1 7 6.1"
                                          stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10"
                                          stroke-width="2.5"></polyline>
                                <circle cx="10.7" cy="23" r="2.2" stroke="none"></circle>
                                <circle cx="19.7" cy="23" r="2.2" stroke="none"></circle>
                            </svg>
                            <span class="navbar-search__filter-text typo-r10 " id="count_cart_header"
                                  style="font-weight: bold;">{!! ($cart->count()>0)? $cart->count().' - '.$total.'<sup>đ</sup>':'' !!}</span>
                        </div>
                    </div>
                </div>

                @if(\Auth::guard('user')->check())

                    <div class="stardust-popover__target stardust_dot">
                        <style>

                        </style>

                        <ul class="icon_menu">
                            <div class="shopee-navbar__left-btn">
                                <img src="{{@\App\Http\Helpers\CommonHelper::getUrlImageThumb(\Auth::guard('user')->user()->image,26)}}"
                                     alt="{{@\Auth::guard('user')->user()->name}}">
                            </div>
                        </ul>
                        <ul class="menu_login">
                            <li><a href="/tin-tuc">Tin tức</a></li>
                            <li><a href="dang-xuat">Đăng xuất</a></li>
                        </ul>
                    </div>

                @else
                    <div class="stardust-popover__target stardust_dot">
                        <style>

                        </style>
                        <ul class="icon_menu">
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <ul class="menu_login">
                            <li><a href="/tin-tuc">Tin tức</a></li>
                            <li class="register_account"><span>Đăng ký</span></li>
                            <li class="login_account"><span>Đăng nhập</span></li>
                        </ul>
                    </div>
                @endif
            </div>
            <div class="stardust-drawer"></div>
            @yield('content')
        </div>
        <div class="iframe-modal-provider__iframe-container iframe-modal-provider__iframe-container--hide">
            <div class="stardust-spinner--hidden stardust-spinner">
                <div class="stardust-spinner__background">
                    <div class="stardust-spinner__main">
                        <svg width="34" height="12" viewBox="-1 0 33 12">
                            <circle class="stardust-spinner__spinner" cx="4" cy="6" r="4" fill="#EE4D2D"></circle>
                            <circle class="stardust-spinner__spinner" cx="16" cy="6" r="4" fill="#EE4D2D"></circle>
                            <circle class="stardust-spinner__spinner" cx="28" cy="6" r="4" fill="#EE4D2D"></circle>
                        </svg>
                    </div>
                </div>
            </div>
        </div>


        <!--        Popup sản phẩm-->

        <div class="pro_popup">
            <form method="post" style="margin-bottom: 0">
                <div class="pro_popup_content pad_popup">

                    <div class="pro_content">

                        <div class="pro_popup_detail">
                            <p class="col-left-pro">Số lượng:</p>
                            <div class="count_pro_pop">
                                <div class="total_pro" style="margin: auto;">
                                    <span id="down_pro" class="fa fa-minus"
                                          style="cursor: pointer;border: 1px solid #dbdbdb;padding: 15px 20px;font-size: 30px;margin-right: -1px;"></span>
                                    <input type="number" name="count_product" value="1" min=1
                                           style="text-align: center;width: 100px; margin: 0">
                                    <span id="up_pro" class="fa fa-plus"
                                          style="cursor: pointer;border: 1px solid #dbdbdb;padding: 15px 20px;font-size: 30px;margin-left: -1px;"></span>
                                </div>

                            </div>
                        </div>

                        <div class="price_pro_pop">
                            <input name="popup_product_price" value="0" type="hidden">
                            <input name="popup_product_id" value="0" type="hidden">
                            <p class="col-left-pro_price">Giá: <span
                                        id="popup_product_price">--<sup>đ</sup></span></p>
                        </div>
                    </div>


                </div>
                <div style="text-align: center;background: #f5f5f5;padding: 10px 20px;display: flex">
                    <span class="close_pro1">Hủy</span>
                    <span class="oder_pro1 add-to-cart">Đặt</span>
                </div>
            </form>
        </div>
        <!--        End Popup sản phẩm -->

        <!--        Popup giỏ hàng -->
        <div class="cart_popup">
            <form method="post" style="margin-bottom: 0">
                <p style="font-size: 1.25rem;font-weight: 400;color: rgba(0,0,0,.8);text-transform: capitalize;margin: 0;  padding: 20px">
                    Giỏ hàng</p>
                <div class="cart_content_parent">
                    <div class="cart_popup_content pad_popup">
                        @if($cart->count()>0)
                            @foreach($cart as $k=>$v)
                                @include('appcoffeetheme::partials.cart_item')
                            @endforeach
                        @else
                            <p>Bạn chưa chọn sản phẩm nào!</p>
                        @endif


                    </div>
                    <div class="cart_popup_total">
                        <b>Tổng: <span style="color: red">{{$total}}</span><sup style="color: red">đ</sup></b>
                    </div>
                </div>


                <div style="text-align: center;background: #f5f5f5;padding: 20px;display: flex">
                    <span class="close_pro">Đóng</span>
                    <span class="oder_pro" id="cart_pay">Thanh toán</span>
                </div>


            </form>
        </div>
        <!--        End Popup giỏ hàng -->

        <!--    Form Thanh toán    -->
        <div class="pay_popup">
            <form method="post" style="margin-bottom: 0" class="thanh-toan">
                <div class="pay_popup_content pad_popup">

                    <div class="pay_content">
                        <div class="pay_popup_detail">
                            <p style="font-size: 1.25rem;font-weight: 400;color: rgba(0,0,0,.8);text-transform: capitalize;margin: 0;margin-bottom: 40px;">
                                Thanh Toán</p>
                            <div class="pay_tab">
                                <span class="pay_home pay_active" data-type="home">Tại nhà</span>
                                <span class="pay_this" data-type="shop">Tại quán</span>
                            </div>
                            <div class="pay_home_tab">
                                <div class="pay_input">
                                    <input type="text" name="tel" required placeholder="Số điện thoại">
                                    <p id="tel_error" style="color: red; padding: 0 10px"></p>
                                </div>

                                <div class="pay_input">
                                    <input type="text" name="name" required placeholder="Tên">
                                    <p id="name_error" style="color: red; padding: 0 10px"></p>
                                </div>

                                <div class="pay_input">
                                    <input type="text" name="address" required placeholder="Địa chỉ">
                                    <p id="address_error" style="color: red; padding: 0 10px"></p>
                                </div>

                                <div class="pay_input">
                                    <input type="datetime-local" name="time" placeholder="Muốn nhận hàng lúc">
                                </div>

                                <div class="pay_input">
                                    <textarea name="note" rows="3" placeholder="Ghi chú"></textarea>
                                </div>
                            </div>

                            <!--                            tại quán-->
                            <div class="pay_this_tab" style="display: none">


                                @foreach($areas as $k=>$area)
                                    <div class="pay_this_tab-group">
                                        <div class="pay_this_tab-default">
                                            <div class="pay_this_tab-heading {{($k==0)?'data_area':''}}"
                                                 data-area="{{$area->id}}">
                                                <h4 class="pay_this_tab-title">
                                                    <a href="#collapse1">{{$area->name}}</a>
                                                </h4>
                                            </div>
                                            <div id="pay_this_tab_collapse1" class="pay_this_tab-collapse "
                                                 style="{{($k!=0)?'display:none':''}}">
                                                <div class="pay_this_tab-body">
                                                    @foreach($area->choose_table as $i=>$choose_table)

                                                        <label>
                                                            <div class="table_choose">
                                                                <span class="table_number">{{$choose_table->position}}</span>
                                                                <img src="{{asset('public/frontend/themes/appcoffee/image/icon_table.png')}}"
                                                                     alt="Bàn số {{$choose_table->position}}">
                                                                <input type="radio" name="choosed_table"
                                                                       value="{{$choose_table->id}}" hidden>
                                                            </div>
                                                        </label>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="pay_this_tab-group">
                                    <textarea rows="5" name="note" placeholder="Ghi chú"></textarea>
                                </div>
                            </div>

                        </div>
                    </div>


                </div>
                <div style="text-align: center; background: #f5f5f5; padding: 10px 20px;display: flex">
                    <span class="close_pay">Quay lại giỏ hàng</span>
                    <span class="oder_pay">Hoàn thành</span>
                </div>
            </form>
        </div>
        <!--    End Form Thanh toán    -->


        {{--quên mk--}}
        <div class="forgot_form">
            <div style="margin-bottom: 0">
                <div class="pro_popup_content pad_popup">
                    <div id="error">

                    </div>
                    @if (Session('success'))
                        <div class="alert bg-success" role="alert">
                            {!!session('success')!!}
                        </div>
                    @endif
                    @if(Session::has('message') && !Auth::check())
                        <div class="alert text-center text-white" role="alert"
                             style=" margin: 0; font-size: 16px;">
                            <a href="#" style="float:right;" class="alert-close" data-dismiss="alert">&times;</a>
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    <div class="pro_content">
                        <p style="font-size: 1.25rem;font-weight: 400;color: rgba(0,0,0,.8);text-transform: capitalize;margin: 0;margin-bottom: 40px;">
                            Quên mật khẩu</p>

                        <div class="pay_home_tab">
                            <div class="pay_input">
                                <input type="email" name="email" id="email" placeholder="Email">
                            </div>
                        </div>
                    </div>
                </div>
                {{ csrf_field() }}
                <div style="text-align: center;background: #f5f5f5;padding: 10px 20px;display: flex">
                    <span class="close_pro1">Hủy</span>
                    <button type="submit" class="oder_pro1" id="btn_forgot">Gửi yêu cầu</button>
                </div>
            </div>
        </div>
        {{--end quên mk--}}












        {{--đang nhập--}}
        <div class="login_form">
            <div style="margin-bottom: 0">
                <div class="pro_popup_content pad_popup">
                    <div id="error_logout">

                    </div>
                    @if (Session('success'))
                        <div class="alert bg-success" role="alert">
                            {!!session('success')!!}
                        </div>
                    @endif
                    @if(Session::has('message') && !Auth::check())
                        <div class="alert text-center text-white" role="alert"
                             style=" margin: 0; font-size: 16px;">
                            <a href="#" style="float:right;" class="alert-close" data-dismiss="alert">&times;</a>
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    <div class="pro_content">
                        <p style="font-size: 1.25rem;font-weight: 400;color: rgba(0,0,0,.8);text-transform: capitalize;margin: 0;margin-bottom: 40px;">
                            Đăng nhập</p>

                        <div class="pay_home_tab">
                            <div class="pay_input">
                                <input type="email" name="email" id="email_login" required placeholder="Email">
                            </div>

                            <div class="pay_input">
                                <input type="password" name="password" id="password_login" required placeholder="Mật khẩu">
                            </div>
                            <span class="forgot_password">Quên mật khẩu ?</span>
                        </div>
                    </div>
                </div>
                {{ csrf_field() }}
                <div style="text-align: center;background: #f5f5f5;padding: 10px 20px;display: flex">
                    <span class="close_pro1">Hủy</span>
                    <button type="submit" class="oder_pro1" id="id_login">Đăng nhập</button>
                </div>
            </div>
        </div>
        {{--end đang nhập--}}

        {{--đang ký--}}
        <div class="register_form">
            <div style="margin-bottom: 0">
                <div class="pro_popup_content pad_popup">
                    <div id="error_register" class="pro_content">

                    </div>
                    <div class="pro_content">
                        <p style="font-size: 1.25rem;font-weight: 400;color: rgba(0,0,0,.8);text-transform: capitalize;margin: 0;margin-bottom: 40px;">
                            Đăng ký</p>

                        <div class="pay_home_tab">
                            <div class="pay_input">
                                <input type="text" id="name" placeholder="Họ Tên">
                            </div>
                            <div class="pay_input">
                                <input type="text" id="tel" placeholder="Số điện thoại">
                            </div>
                            <div class="pay_input">
                                <input type="email" id="email_register" placeholder="Email">
                            </div>
                            <div class="pay_input">
                                <input type="password" id="password_register" placeholder="Mật khẩu">
                            </div>
                            <div class="pay_input">
                                <input type="text" id="address" placeholder="Địa chỉ khách hàng">
                            </div>
                        </div>
                    </div>
                </div>
                {{ csrf_field() }}
                <div style="text-align: center;background: #f5f5f5;padding: 10px 20px;display: flex">
                    <span class="close_pro1">Hủy</span>
                    <button type="button" class="oder_pro1" id="id_submit">Đăng ký</button>
                </div>
            </div>
        </div>
        {{--end đang ký--}}


    </div>
</div>
<div class="che_man" style="
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    display: none;
    background: rgba(0,0,0,.4);
    z-index: 998;
    "></div>

<script src="{{ asset('public/libs/toastr/toastr.min.js') }}"></script>
<script>
    Number.prototype.formatMoney = function (c, d, t) {
        var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };

    {{--quên mk--}}
    $(document).ready(function () {
        $('#btn_forgot').on('click', function () {
            let email = $('#email').val();
            let url = "/quen-mat-khau";

            jQuery.ajax({
                url: url,
                type: "POST",
                data: {
                    email: email,
                    _token: '{!! csrf_token() !!}'
                },
                success: function (data) {
                    if (data.status === true) {
                        toastr.success('Email sẽ được gửi trong ít phút. Bạn vui lòng kiểm tra mail để lấy lại mật khẩu!', {fadeAway: 1000})
                        location.reload()

                    }
                    if (data.status == false) {
                        toastr.error('Email chưa được đăng ký trên hệ thống', {fadeAway: 1000})
                    }

                },
                error: function (errors) {
                    $.each(errors.responseJSON.errors, function (key, val) {
                        toastr.error(val, {fadeAway: 1000})
                        return false
                    })

                },
            })
        })
    })

    {{--đăng nhập--}}
    $(document).ready(function () {
        $('#id_login').on('click', function () {
            let email = $('#email_login').val();
            let password = $('#password_login').val();
            let url = "/dang-nhap";
            jQuery.ajax({
                url: url,
                type: "POST",
                data: {
                    email: email,
                    password: password,
                    _token: '{!! csrf_token() !!}'
                },
                success: function (data) {
                    if (data.status == true) {
                        toastr.success('Đăng nhập thành công', {fadeAway: 1000})
                        location.reload()
                    }
                    if (data.status == false) {
                        toastr.error('Tài khoản mật khẩu không đúng', {fadeAway: 1000})
                    }
                },
                error: function (errors) {
                    $.each(errors.responseJSON.errors, function (key, val) {
                        // $('#error_logout').append("<p class='alert alert-danger'>"+val+ "</p>")
                        toastr.error(val, {fadeAway: 1000})

                        return false
                    })

                },
            })
        })
    })


    {{--đăng ký--}}
    $(document).ready(function () {
        $('#id_submit').on('click', function () {
            let name = $('#name').val();
            let email = $('#email_register').val();
            let tel = $('#tel').val();
            let password = $('#password_register').val();
            let address = $('#address').val();
            let url = "/dang-ky";
            jQuery.ajax({
                url: url,
                type: "POST",
                data: {
                    name: name,
                    email: email,
                    tel: tel,
                    password: password,
                    address: address,
                    _token: '{!! csrf_token() !!}'
                },

                success: function (data) {
                    if (data.status === true) {
                        $(".register_form").hide();
                        $(".login_form").show();
                        $('#name').val('');
                        $('#email_register').val('');
                        $('#tel').val('');
                        $('#password_register').val('');
                        $('#address').val('');
                        $('#success_register').append("<p class='alert alert-danger'>" + data.val + "</p>")

                    }
                },

                error: function (errors) {
                    $.each(errors.responseJSON.errors, function (key, val) {
                        // $('#error_register').append("<p class='alert alert-danger'>"+val+ "</p>")
                        toastr.error(val, {fadeAway: 1000})
                        return false
                    })

                },

            })
        })
    })


    function loading() {
        if ($('.list-product').find('#loading').length == 0) {
            $('.list-product').append('<div id="loading" style="width: 100%;height: 100%;z-index: 999999;top: 0;text-align: center;"><img style="margin-top: 10px;" src="/public/images_core/icons/loading.gif"></div>');
        } else {
            $('#loading').show();
        }
    }

    function stopLoading() {
        $('#loading').hide();
    }


    function updateCartIcon() {
        var product_id = $('input[name=popup_product_id]').val();
        var quatity = $('input[name=count_product]').val();
        $.ajax({
            url: 'update-count-cart',
            data: {
                'product_id': product_id,
                'quatity': quatity
            },
            type: 'GET',
            success: function (result) {
                $('#count_cart_header').html(result.html);
            }
        });
    }

    $(document).ready(function () {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-left",
            "onclick": null,
            "showDuration": "1000",
            "hideDuration": "1000",
            "timeOut": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        $('body').on('click', '.tabs_header_click', function () {
            $(this).addClass('stardust-tabs-header__tab--active');
            $(this).siblings().removeClass('stardust-tabs-header__tab--active');
        });

        $('body').on('click', '.add-to-cart', function () {
            var product_id = $('input[name=popup_product_id]').val();
            var quatity = $('input[name=count_product]').val();
            $.ajax({
                url: 'add-to-cart',
                data: {
                    'product_id': product_id,
                    'quatity': quatity
                },
                type: 'GET',
                success: function () {
                    updateCartIcon();
                    $(".pro_popup").hide();
                    $(".pay_popup").hide();
                    $(".cart_popup").hide();
                    $(".che_man").hide();
                    toastr.success('Đặt hàng thành công!');
                }
            });
        });

        $('body').on('click', '.forgot_password', function () {
            $(".pro_popup").hide();
            $(".pay_popup").hide();
            $(".cart_popup").hide();
            $(".register_form").hide();
            $(".login_form").hide();
            $(".forgot_form").show();
            $(".che_man").show();

        });
        $('body').on('click', '.register_account', function () {
            $(".pro_popup").hide();
            $(".pay_popup").hide();
            $(".cart_popup").hide();
            $(".register_form").show();
            $(".login_form").hide();
            $(".che_man").show();

        });
        $('body').on('click', '.login_account', function () {
            $(".pro_popup").hide();
            $(".pay_popup").hide();
            $(".cart_popup").hide();
            $(".login_form").show();
            $(".che_man").show();

        });
        $('body').on('click', '#cart_mobie1', function () {
            $(".pro_popup").hide();
            $(".pay_popup").hide();
            $(".cart_popup").show();
            $(".che_man").show();

        });
        $('body').on('click', '.che_man', function () {
            $(".pro_popup").hide();
            $(".pay_popup").hide();
            $(".cart_popup").hide();
            $(".register_form").hide();
            $(".login_form").hide();
            $(".forgot_form").hide();
            $(".che_man").hide();
        });
        //Nút đóng của giỏ hàng
        $('body').on('click', '.close_pro', function () {
            $(".cart_popup").hide();
            $(".pro_popup").hide();
            $(".pay_popup").hide();
            $(".che_man").hide();
        });

        $('body').on('click', '.close_pro1', function () {
            $(".cart_popup").hide();
            $(".pro_popup").hide();
            $(".che_man").hide();
            $(".login_form").hide();
            $(".register_form").hide();
            $(".forgot_form").hide();
        });
        //click vào sản phẩm

        $('body').on('click', '.item-card-list__item-card-wrapper', function () {
            $(".cart_popup").hide();
            $(".pay_popup").hide();
            $(".pro_popup").show();
            $(".che_man").show();

            var product_id = $(this).data('id');
            var product_price = $(this).data('price');
            $('#popup_product_price').html(product_price + 'đ');
            $('input[name=popup_product_id]').val(product_id);
            $('input[name=count_product]').val(1);
            $('input[name=popup_product_price]').val(product_price);
        });
        //thanh toán
        $('body').on('click', '#cart_pay', function () {
            $(".cart_popup").hide();
            $(".pro_popup").hide();
            $(".pay_popup").show();
            $(".che_man").show();

        });
        //Nút quay về giỏ hàng
        $('body').on('click', '.close_pay', function () {
            $(".pay_popup").hide();
            $(".pro_popup").hide();
            $(".cart_popup").show();
        });
        //Nút hoàn thành

        $('body').on('click', '.oder_pay', function () {
            var tel = $('input[name=tel]').val();
            var name = $('input[name=name]').val();
            var address = $('input[name=address]').val();
            var time = $('input[name=time]').val();
            var note = $('input[name=note]').val();
            var type = $('.pay_active').data('type');
            var data_area = $('.data_area').data('area');
            var choose_table_id = $('input[name=choosed_table]:checked').val();
            // if(tel ==='' || !isNumber(tel)){
            //     $('#tel_error').text('Số điện thoại không được để trống và phải là số');
            // }else{
            //     $('#tel_error').text();
            // }
            //
            // if(name===''){
            //     $('#name_error').text('Tên không được để trống');
            // }else{
            //     $('#name_error').text();
            // }
            // if(address===''){
            //     $('#address_error').text('Địa chỉ không được để trống');
            // }else{
            //     $('#address_error').text();
            // }
            $.ajax({
                url: 'add-bill',
                data: {
                    'tel': tel,
                    'name': name,
                    'address': address,
                    'time': time,
                    'note': note,
                    'area_id': data_area,
                    'choose_table_id': choose_table_id,
                    'type': type
                },
                type: 'get',
                success: function (result) {
                    toastr.success('Bạn đã đặt hàng thành công!');
                    $('#count_cart_header').html(result.cart);
                    $(".pay_popup").hide();
                    $(".che_man").hide();
                },
                // error: function(){
                //     $('#name_error').text('Tên không được để trống và phải là số');
                //     $('#address_error').text('Địa chỉ không được để trống');
                //     $('#tel_error').text('Số điện thoại không được để trống và phải là số');
                // }
            });
        });


        $('body').on('click', '.pay_home', function () {
            $(".pay_this").removeClass('pay_active');
            $(this).addClass('pay_active');
            $(".pay_home_tab").show();
            $(".pay_this_tab").hide();
        });
        $('body').on('click', '.pay_this', function () {
            $(this).addClass('pay_active');
            $(".pay_home").removeClass('pay_active');
            $(".pay_home_tab").hide();
            $(".pay_this_tab").show();
        });
        // các khu vuc trong tại quán
        $(".pay_this_tab-heading").each(function (index) {
            $(".pay_this_tab-heading:eq(" + index + ")").click(function () {
                $(this).addClass('data_area');
                $(".pay_this_tab-heading:gt(" + index + ")").removeClass('data_area')
                $(".pay_this_tab-heading:lt(" + index + ")").removeClass('data_area')
                $(".pay_this_tab-collapse:eq(" + index + ")").toggle();
                $(".pay_this_tab-collapse:gt(" + index + ")").hide();
                $(".pay_this_tab-collapse:lt(" + index + ")").hide();
            });
        });
        // $(".pay_this_tab-heading").click(function () {
        //
        //     $(this).parent(".pay_this_tab-default").children(".pay_this_tab-collapse").toggle();
        // });

        // các tab phần lọc
        $(".stardust-tabs-header__tab").each(function (index) {

            $('body').on('click', '.stardust-tabs-header__tab:eq(' + index + ')', function () {
                // $(".stardust-tabs-header__tab:eq(" + index + ")").click(function () {
                $(".filter-bar:eq(" + index + ")").show();
                $(".filter-bar:gt(" + index + ")").hide();
                $(".filter-bar:lt(" + index + ")").hide();
            });
        });


//    tăng giảm sản phẩm

        $(".down_pro_cart").each(function (index) {
            $('body').on('click', ".down_pro_cart:eq(" + index + ")", function () {
                let count_product = $("input[name='count_product_cart']:eq(" + index + ")").val();
                count_product--;
                if (count_product <= 1) {
                    count_product = 1;
                }
                $("input[name='count_product_cart']:eq(" + index + ")").val(count_product);
                var rowId = $('input[name=rowId]:eq(' + index + ')').val();
                var priceOfProduct = $('input[name=rowId]:eq(' + index + ')').data('priceofproduct');
                console.log(priceOfProduct)
                $.ajax({
                    url: 'update-change-product-in-cart',
                    data: {
                        'count_product': count_product,
                        'rowId': rowId,
                        'priceOfProduct': priceOfProduct
                    },
                    type: 'get',
                    success: function (result) {
                        $(".price_pro_cart>span:eq(" + index + ")").text(result.priceProduct);
                        $(".cart_popup_total>b>span").text(result.totalPrice);
                    }
                });
            });
        });
        // $('body').on('each', ".up_pro_cart", function () {
        $(".up_pro_cart").each(function (index) {
            $('body').on('click', ".up_pro_cart:eq(" + index + ")", function () {
                let count_product = $("input[name='count_product_cart']:eq(" + index + ")").val();
                count_product++;
                $("input[name='count_product_cart']:eq(" + index + ")").val(count_product);
                // console.log(count_product);
                var rowId = $('input[name=rowId]:eq(' + index + ')').val();

                var priceOfProduct = $('input[name=rowId]:eq(' + index + ')').data('priceofproduct');
                $.ajax({
                    url: 'update-change-product-in-cart',
                    data: {
                        'count_product': count_product,
                        'rowId': rowId,
                        'priceOfProduct': priceOfProduct
                    },
                    type: 'get',
                    success: function (result) {
                        $(".price_pro_cart>span:eq(" + index + ")").text(result.priceProduct);
                        $(".cart_popup_total>b>span").text(result.totalPrice);
                        updateCartIcon();
                    }
                });
            });
        });

        $('body').on('click', 'p.remove_pro_cart', function () {
            var rowid = $(this).parents('.price_pro_cart').children('input[name=rowId]').val();
            var object = $(this);
            $.ajax({
                url: 'remove-product-in-cart',
                data: {
                    'rowId': rowid
                },
                type: 'get',
                success: function (result) {
                    $(".cart_popup_total>b>span").text(result.totalPrice);
                    loadContentCart();
                    updateCartIcon();
                }
            });
        });
        $('body').on('click', "input[name=count_product_cart]", function () {
            var price_product = $(this).data('price_product');
            var rowId = $(this).data('row_id');
            var count_product = $(this).val();
            var that = $(this);
            $.ajax({
                url: 'update-change-product-in-cart',
                data: {
                    'count_product': count_product,
                    'rowId': rowId,
                    'priceOfProduct': price_product
                },
                type: 'get',
                success: function (result) {
                    that.parent('.count_pro_cart').parent('.info_cart').children(".price_pro_cart").children("span").text(result.priceProduct);
                    $(".cart_popup_total>b>span").text(result.totalPrice);
                }
            });
        });
        $('body').on('click', "#down_pro", function () {
            let count_product = $("input[name='count_product']").val();
            count_product--;
            if (count_product <= 1) {
                count_product = 1;
            }
            $("input[name='count_product']").val(count_product);
            var product_price = parseInt($('input[name=popup_product_price]').val());
            var product_total_down = product_price * count_product;
            $('#popup_product_price').html(product_total_down);
        });
        $('body').on('click', "#up_pro", function () {
            let count_product = $("input[name='count_product']").val();
            count_product++;
            $("input[name='count_product']").val(count_product);

            var product_price = parseInt($('input[name=popup_product_price]').val());
            var product_total_price = product_price * count_product;

            $('#popup_product_price').html(product_total_price);
        });


        //    Chọn bàn

        $('body').on('click', ".pay_this_tab-body>label", function () {
            $(this).find("img").attr("src", "{{asset('public/frontend/themes/appcoffee/image/icon_table_selected.png')}}");
            $(this).siblings().find("img").attr("src", "{{asset('public/frontend/themes/appcoffee/image/icon_table.png')}}");
            $(this).parents('.pay_this_tab-group').siblings().find("img").attr("src", "{{asset('public/frontend/themes/appcoffee/image/icon_table.png')}}");
        });

        $('body').on('click', "#cart_mobie1", function () {
            loadContentCart();
        });

        $('body').on('keyup', 'input[name=search]', function () {
            // $('input[name=search]').keyup(function () {
            var keyword = $(this).val();
            searchProduct(keyword, null);
        });
    });

    function loadContentCart() {
        $.ajax({
            url: 'load-content-cart',
            data: {},
            type: 'get',
            success: function (html) {
                $('.cart_content_parent').html(html);
            }
        });
    }


    //    Tìm kiếm trả luôn kết quả
    function searchProduct(keyword, menu_slug) {
        loading();
        $.ajax({
            url: 'search--product',
            data: {
                'keyword': keyword,
                'menu_slug': menu_slug
            },
            type: 'get',
            success: function (html) {
                stopLoading();
                $('.list-product').html(html);
            },
            error: function () {
                stopLoading();
                alert('Có lỗi xảy ra. Vui lòng load lại trang và thử lại!');
            }
        });
    }

    function activeItemMenu(id) {
        $('.btn-' + id).addClass('active');
        $('.btn-' + id).siblings().removeClass('active');
        $('.btn-' + id).parents('.filter-bar').siblings().find('.filter-bar-products').removeClass('active');
    }
</script>
</body>
</html>
