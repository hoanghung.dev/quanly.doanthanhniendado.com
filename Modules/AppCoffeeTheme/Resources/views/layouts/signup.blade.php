<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body {font-family: Arial, Helvetica, sans-serif;}
        * {box-sizing: border-box}

        /* Full-width input fields */
        input[type=text], input[type=password] {
            width: 100%;
            padding: 15px;
            margin: 5px 0 22px 0;
            display: inline-block;
            border: none;
            background: #f1f1f1;
        }

        input[type=text]:focus, input[type=password]:focus {
            background-color: #ddd;
            outline: none;
        }

        hr {
            border: 1px solid #f1f1f1;
            margin-bottom: 25px;
        }

        /* Set a style for all buttons */
        button {
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
            opacity: 0.9;
        }

        button:hover {
            opacity:1;
        }

        /* Extra styles for the cancel button */
        .cancelbtn {
            padding: 14px 20px;
            background-color: #f44336;
        }

        /* Float cancel and signup buttons and add an equal width */
        .cancelbtn, .signupbtn {
            float: left;
            width: 50%;
        }

        /* Add padding to container elements */
        .container {
            padding: 16px;
        }

        /* Clear floats */
        .clearfix::after {
            content: "";
            clear: both;
            display: table;
        }
        .container>form{
            width: 40vw;
            margin: auto;
        }
        /* Change styles for cancel button and signup button on extra small screens */
        @media screen and (max-width: 300px) {
            .cancelbtn, .signupbtn {
                width: 100%;
            }
        }
        @media screen and (max-width: 468px) {
            .container>form{
                width: 80vw;
            }
        }
        @media (min-width: 469px) and (max-width: 768px) {
            .container>form{
                width: 70vw;
            }
        }

    </style>
</head>

<body>
<div class="container">
    <form action="" style="border:1px solid #ccc">
        <div class="container">
            <h1>Đăng ký</h1>
            <p>Vui lòng điền đầy đủ các thông tin dưới đây</p>
            <hr>

            <label for="email"><b>Email</b></label>
            <input type="text" placeholder="Nhập Email" name="email" required>

            <label for="text"><b>Số điện thoại</b></label>
            <input type="text" placeholder="Nhập số điện thoại" name="tel" required>

            <label for="psw"><b>Mật khẩu</b></label>
            <input type="password" placeholder="Nhập mật khẩu" name="psw" required>

            <label for="psw-repeat"><b>Nhập lại mật khẩu</b></label>
            <input type="password" placeholder="Nhập lại mật khẩu" name="psw-repeat" required>


            <div class="clearfix">
                <button type="button" class="cancelbtn">Hủy</button>
                <button type="submit" class="signupbtn">Đăng ký</button>
            </div>
        </div>
    </form>
</div>


</body>
</html>
