<?php
namespace Modules\BannerApp\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class BannerApp extends Model
{

    protected $table = 'banners';

    public $timestamps = false;
    protected $fillable = [
        'name', 'link', 'image', 'created_at', 'updated_at', 'location', 'status','order_no'
    ];



}
