<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'bannerapp'], function () {
        Route::get('', 'Admin\BannerAppController@getIndex')->name('bannerapp');
        Route::get('publish', 'Admin\BannerAppController@getPublish')->name('bannerapp.publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\BannerAppController@add');
        Route::get('delete/{id}', 'Admin\BannerAppController@delete');
        Route::post('multi-delete', 'Admin\BannerAppController@multiDelete');
        Route::get('search-for-select2', 'Admin\BannerAppController@searchForSelect2')->name('bannerapp.search_for_select2');
        Route::get('{id}', 'Admin\BannerAppController@update');
        Route::post('{id}', 'Admin\BannerAppController@update');
    });
});
