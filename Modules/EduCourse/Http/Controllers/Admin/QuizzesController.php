<?php

namespace Modules\EduCourse\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\EduCourse\Models\Category;
use Modules\EduCourse\Models\Lesson;
use Modules\EduCourse\Models\Quizzes;
use Validator;

class QuizzesController extends CURDBaseController
{
    protected $orderByRaw = 'order_no desc, id asc';
    protected $module = [
        'code' => 'quizzes',
        'table_name' => 'quizzes',
        'label' => 'Bài kiểm tra',
        'modal' => '\Modules\EduCourse\Models\Quizzes',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'width' => '60px'],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên '],
            ['name' => 'time', 'type' => 'text', 'label' => 'Thời gian làm bài'],
            ['name' => 'accumulated_points', 'type' => 'number', 'label' => 'Điểm tích lũy tối đa'],
            ['name' => 'multi_cat', 'type' => 'custom', 'td' => 'educourse::list.td.multi_cat', 'label' => 'Danh mục', 'object' => 'category'],
            ['name' => 'lesson_id', 'type' => 'relation', 'label' => 'Danh mục bài học', 'object' => 'lesson', 'display_field' => 'name'],
//            ['name' => 'topic_id', 'type' => 'text', 'label' => 'Danh mục nội dung tiết học'],
//            ['name' => 'code', 'type' => 'text', 'label' => 'Mã'],
//            ['name' => 'final_price', 'type' => 'price_vi', 'label' => 'Giá bán'],
//            ['name' => 'multi_cat', 'type' => 'custom', 'td' => 'educourse::list.td.multi_cat', 'label' => 'Danh mục', 'object' => 'category_product'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên '],
                ['name' => 'multi_cat', 'type' => 'custom', 'field' => 'educourse::form.fields.multi_cat', 'label' => 'Danh mục', 'model' => Category::class,
                    'object' => 'category', 'display_field' => 'name', 'multiple' => true,'where' => 'type=4','des' => 'Danh mục đầu tiên chọn là danh mục chính'],
                ['name' => 'time', 'type' => 'text', 'class' => 'required', 'label' => 'Thời gian làm bài'],
                ['name' => 'lesson_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Danh mục bài học', 'model' => Lesson::class, 'display_field' => 'name',],
//                ['name' => 'code', 'type' => 'text', 'label' => 'Mã sản phẩm', 'group_class' => 'col-md-4'],
//                ['name' => 'base_price', 'type' => 'text', 'label' => 'Giá ban đầu', 'class' => 'number_price', 'group_class' => 'col-md-4'],
//                ['name' => 'final_price', 'type' => 'text', 'label' => 'Giá bán', 'class' => 'number_price', 'group_class' => 'col-md-4'],
//                ['name' => 'multi_cat', 'type' => 'custom', 'field' => 'educourse::form.fields.multi_cat', 'label' => 'Danh mục sản phẩm', 'model' => \Modules\EduCourse\Models\Category::class,
//                    'object' => 'category_product', 'display_field' => 'name', 'multiple' => true, 'where' => 'type=5', 'des' => 'Danh mục đầu tiên chọn là danh mục chính'],
//                ['name' => 'tags', 'type' => 'custom', 'field' => 'educourse::form.fields.tags', 'label' => 'Từ khóa sản phẩm', 'model' => \Modules\EduCourse\Models\Category::class,
//                    'object' => 'tag_product', 'display_field' => 'name', 'multiple' => true, 'where' => 'type=6'],
                ['name' => 'intro', 'type' => 'textarea', 'label' => 'Mô tả ngắn'],
                ['name' => 'accumulated_points', 'type' => 'number', 'label' => 'Điểm tích lũy tối đa'],
//                ['name' => 'content', 'type' => 'textarea_editor', 'label' => 'Nội dung'],
                ['name' => 'link_google_form', 'type' => 'text', 'label' => 'Link bài kiểm tra'],
                ['name' => 'link_google_form_answer', 'type' => 'text', 'label' => 'Link đáp án bài kiểm tra'],
                ['name' => 'order_no', 'type' => 'number', 'label' => 'Thứ tự ưu tiên', 'value' => 0, 'group_class' => 'col-md-3', 'des' => 'Số to ưu tiên hiển thị trước'],
//                ['name' => 'file_audio_name', 'type' => 'text', 'label' => 'Tên bài nghe'],
//                ['name' => 'file_audio_intro', 'type' => 'text',  'label' => 'Thông tin bài nghe'],
//                ['name' => 'status', 'type' => 'checkbox', 'label' => 'Kích hoạt', 'value' => 1, 'group_class' => 'col-md-6'],
//                ['name' => 'order_no', 'type' => 'number', 'label' => 'Thứ tự ưu tiên', 'value' => 0, 'group_class' => 'col-md-6', 'des' => 'Số to ưu tiên hiển thị trước'],
            ],

            'image_tab' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Ảnh'],
//                ['name' => 'image_extra', 'type' => 'multiple_image', 'count' => '6', 'label' => 'Ảnh khác'],
            ],

//            'seo_tab' => [
//                ['name' => 'slug', 'type' => 'slug', 'class' => 'required', 'label' => 'Slug', 'des' => 'Đường dẫn sản phẩm trên thanh địa chỉ'],
//                ['name' => 'meta_title', 'type' => 'text', 'label' => 'Meta title'],
//                ['name' => 'meta_description', 'type' => 'text', 'label' => 'Meta description'],
//                ['name' => 'meta_keywords', 'type' => 'text', 'label' => 'Meta keywords'],
//            ],
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên ',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'time' => [
            'label' => 'Thời gian làm bài',
            'type' => 'text',
            'query_type' => 'like'
        ],
//        'final_price' => [
//            'label' => 'Giá bán',
//            'type' => 'number',
//            'query_type' => 'like'
//        ],
//        'topic_id' => [
//            'label' => 'Danh mục nội dung tiết học',
//            'type' => 'select2_ajax_model',
//            'display_field' => 'name',
//            'object' => 'category_product',
//            'model' => \Modules\EduCourse\Models\Topic::class,
//            'query_type' => 'custom'
//        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('educourse::quizzes.list')->with($data);
    }
    public function appendWhere($query, $request)
    {
        $query = $query->where('course_id', $request->course_id);
        return $query;
    }

//    public function appendWhere($query, $request)
//    {
//        //  Lấy các sản phẩm trong kho
//        $query = $query->where('company_id', null);
//
//        //  Lọc theo danh mục
//        if (!is_null($request->get('category_id'))) {
//            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->category_id . '|%');
//        }
//
//        return $query;
//    }

    public function add(Request $request)
    {

        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('educourse::quizzes.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    $stt1=trim(strstr($request->link_google_form,'d/'),'/edit');
                    $stt2=explode('/edit',strstr($request->link_google_form_answer,'d/'));

                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['category_id'] = $request->multi_cat[0];
                    }
                    if ($request->has('link_google_form')) {
                        $data['google_form_id'] = $stt1;
                    }
                    if ($request->has('link_google_form_answer')) {
                        $data['google_form_answer_id'] = trim($stt2[0],'d/');
                    }
                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;


                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        CommonHelper::flushCache();
//                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    } elseif ($request->return_direct == 'save_editor') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . '/editor');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);

        //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

//        $items = @explode('/d/', trim($item->link_google_form,'/edit') );
//        dd($items);
        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('educourse::quizzes.edit')->with($data);
        } else if ($_POST) {
            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ], [
                'name.required' => 'Bắt buộc phải nhập tên',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                $stt1=trim(strstr($request->link_google_form,'d/'),'/edit');
                $stt2=explode('/edit',strstr($request->link_google_form_answer,'d/'));
                //  Tùy chỉnh dữ liệu insert
                if ($request->has('multi_cat')) {
                    $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                    $data['category_id'] = $request->multi_cat[0];
                }
                if ($request->has('link_google_form')) {
                    $data['google_form_id'] = $stt1;
                }
                if ($request->has('link_google_form_answer')) {
                    $data['google_form_answer_id'] = trim($stt2[0],'d/');
                }
                #

                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::flushCache();
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache();
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache();
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function getToMyCompany(Request $request, $id)
    {
        $product_new = $this->duplicate($request, $id);
        $order = \Modules\JdesSetting\Models\Editor::where('product_ids', 'like', '%|'.$id.'|%')->first();
        $order->product_ids = $order->product_ids . $product_new->id . '|';
        $order->save();
        CommonHelper::one_time_message('success', 'Đã lấy sản phẩm về kho. Sản phẩm mới đã được tạo.');
        return redirect('/admin/product/' . $product_new->id);
    }

    public function duplicate(Request $request, $id)
    {
        $poduct = Lesson::find($id);
        $poduct_new = $poduct->replicate();
        $poduct_new->slug = $poduct->slug . $poduct_new->company_id;
        $poduct_new->company_id = \Auth::guard('admin')->user()->last_company_id;
        $poduct_new->admin_id = \Auth::guard('admin')->user()->id;
        $poduct_new->save();
        return $poduct_new;
    }
}
