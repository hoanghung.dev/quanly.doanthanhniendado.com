<?php

namespace Modules\EduCourse\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\ThemeEdu\Models\Contact;

class Course extends Model
{
    protected $table = 'courses';
    protected $guarded = [];
//    protected $fillable = [
//        'admin_id','category_id',
//    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
    public function admin()
    {
        return $this->belongsTo(Admin::class, 'lecturer_id');
    }
    public function delete()
    {
        $this->lesson()->delete();
        $this->lessonitem()->delete();
        $this->topic()->delete();
        $this->recording()->delete();
        parent::delete();
    }
    public function lesson()
    {
        return $this->hasMany(Lesson::class, 'course_id', 'id');
    }

    public function lessonitem()
    {
        return $this->hasMany(LessonItem::class, 'course_id', 'id')->orderBy('order_no', 'desc')->orderBy('id', 'asc');
    }

    public function topic()
    {
        return $this->hasMany(Topic::class, 'course_id', 'id')->orderBy('order_no', 'desc')->orderBy('id', 'asc');
    }

    public function recording()
    {
        return $this->hasMany(Recording::class, 'course_id', 'id')->orderBy('order_no', 'desc')->orderBy('id', 'asc');
    }

}
