@if(in_array('course_view', $permissions))
    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i
                                class="kt-menu__link-icon flaticon-folder-1"></i>
                    </span><span class="kt-menu__link-text">Khóa học</span><i
                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                    class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/category" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Chuyên mục khóa học</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/course" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Tất cả khóa học</span></a></li>
                    {{--<li class="kt-menu__item " aria-haspopup="true"><a--}}
                                {{--href="/admin/lesson" class="kt-menu__link "><i--}}
                                    {{--class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span--}}
                                    {{--class="kt-menu__link-text">Bài học</span></a></li>--}}
                {{--<li class="kt-menu__item " aria-haspopup="true"><a--}}
                            {{--href="/admin/lesson-item" class="kt-menu__link "><i--}}
                                {{--class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span--}}
                                {{--class="kt-menu__link-text">Các tiết học</span></a></li>--}}
                {{--<li class="kt-menu__item " aria-haspopup="true"><a--}}
                            {{--href="/admin/topic" class="kt-menu__link "><i--}}
                                {{--class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span--}}
                                {{--class="kt-menu__link-text">Nội dung tiết học</span></a></li>--}}
                {{--<li class="kt-menu__item " aria-haspopup="true"><a--}}
                            {{--href="/admin/recording" class="kt-menu__link "><i--}}
                                {{--class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span--}}
                                {{--class="kt-menu__link-text">Bài nghe của tiết học</span></a></li>--}}
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/quizzes" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Bài kiểm tra</span></a></li>
                {{--<li class="kt-menu__item " aria-haspopup="true"><a--}}
                            {{--href="/admin/quiz" class="kt-menu__link "><i--}}
                                {{--class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span--}}
                                {{--class="kt-menu__link-text">Nội dung bài kiểm tra</span></a></li>--}}
                {{--<li class="kt-menu__item " aria-haspopup="true"><a--}}
                            {{--href="/admin/quizzes-history" class="kt-menu__link "><i--}}
                                {{--class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span--}}
                                {{--class="kt-menu__link-text">Lịch sử bài kiểm tra</span></a></li>--}}
            </ul>
        </div>
    </li>
@endif