<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->text('password');
            $table->tinyInteger('source')->default('1');
            $table->string('channel')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('center')->nullable();
            $table->text('avatar')->nullable();
            $table->tinyInteger('status')->default('1');
            $table->string('facebook')->nullable();
            $table->string('zalo')->nullable();
            $table->tinyInteger('gender')->default(1);
            $table->string('birthday')->nullable();
            $table->string('change_password')->
            nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
