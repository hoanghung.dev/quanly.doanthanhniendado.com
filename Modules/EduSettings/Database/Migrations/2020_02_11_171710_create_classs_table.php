<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClasssTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('number_time');
            $table->unsignedBigInteger('teacher_id');
            $table->unsignedBigInteger('tutors_id')->nullable();
            $table->string('start');
            $table->string('address');
            $table->string('room');
            $table->tinyInteger('monday')->default(0);
            $table->string('start_monday')->nullable();
            $table->string('end_monday')->nullable();
            $table->tinyInteger('tuesday')->default(0);
            $table->string('start_tuesday')->nullable();
            $table->string('end_tuesday')->nullable();
            $table->tinyInteger('wednesday')->default(0);
            $table->string('start_wednesday')->nullable();
            $table->string('end_wednesday')->nullable();
            $table->tinyInteger('thurday')->default(0);
            $table->string('start_thurday')->nullable();
            $table->string('end_thurday')->nullable();
            $table->tinyInteger('friday')->default(0);
            $table->string('start_friday')->nullable();
            $table->string('end_friday')->nullable();
            $table->tinyInteger('saturday')->default(0);
            $table->string('start_saturday')->nullable();
            $table->string('end_saturday')->nullable();
            $table->tinyInteger('sunday')->default(0);
            $table->string('start_sunday')->nullable();
            $table->string('end_sunday')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classs');
    }
}
