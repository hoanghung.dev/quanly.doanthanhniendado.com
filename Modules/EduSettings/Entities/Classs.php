<?php

namespace Modules\EduSettings\Entities;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class Classs extends Model
{
    protected $table = 'classs';
    protected $fillable = ['name', 'number_time', 'teacher_id', 'tutors_id', 'start', 'address', 'room', 'monday'
    , 'start_monday', 'end_monday', 'tuesday', 'start_tuesday', 'end_tuesday', 'wednesday', 'start_wednesday',
    'end_wednesday', 'thurday', 'start_thurday', 'end_thurday', 'friday', 'start_friday', 'end_friday', 'saturday',
    'start_saturday', 'end_saturday', 'sunday', 'start_sunday', 'end_sunday'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function teacher(){
        return $this->belongsTo(Admin::class,'teacher_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tutors(){
        return $this->belongsTo(Admin::class,'tutors_id','id');
    }
}
