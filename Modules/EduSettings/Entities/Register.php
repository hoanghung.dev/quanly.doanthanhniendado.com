<?php

namespace Modules\EduSettings\Entities;

use App\Models\Admin;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Register extends Authenticatable
{
    protected $table = 'registers';
    protected $fillable = ['student_id', 'class_id','status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(Admin::class, 'user_id');
    }

    protected $hidden = [
        'password'
    ];

}
