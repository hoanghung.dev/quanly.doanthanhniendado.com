<?php

namespace Modules\EduSettings\Entities;

use App\Models\Admin;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Student extends Authenticatable
{

    protected $guard = 'student';
    protected $guard_name = 'student';

    protected $table = 'students';
    protected $fillable = ['code', 'name', 'phone', 'email', 'password', 'source', 'channel', 'user_id', 'center', 'avatar', 'status', 'facebook', 'zalo', 'gender', 'birthday'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(Admin::class, 'user_id');
    }

    protected $hidden = [
        'password'
    ];

}
