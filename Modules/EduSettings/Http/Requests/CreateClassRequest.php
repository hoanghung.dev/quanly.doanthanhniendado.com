<?php

namespace Modules\EduSettings\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateClassRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5',
            'number_time' => 'required|numeric',
            'teacher_id' => 'required',
            'start' => 'required',
            'address' => 'required',
            'room' => 'required',
            'monday' => 'nullable',
            'start_monday' => 'nullable',
            'end_monday' => 'nullable',
            'tuesday' => 'nullable',
            'start_tuesday' => 'nullable',
            'end_tuesday' => 'nullable',
            'wednesday' => 'nullable',
            'start_wednesday' => 'nullable',
            'end_wednesday' => 'nullable',
            'thurday' => 'nullable',
            'start_thurday' => 'nullable',
            'end_thurday' => 'nullable',
            'friday' => 'nullable',
            'start_friday' => 'nullable',
            'end_friday' => 'nullable',
            'saturday' => 'nullable',
            'start_saturday' => 'nullable',
            'end_saturday' => 'nullable',
            'sunday' => 'nullable',
            'start_sunday' => 'nullable',
            'end_sunday' => 'nullable',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => 'Tên lớp học',
            'number_time' => 'Thời gian học',
            'teacher_id' => 'Giảng viên',
            'start' => 'Ngày bắt đầu',
            'address' => 'Địa chỉ',
            'room' => 'Phòng học',
            'monday' => 'Thứ 2',
            'start_monday' => 'Thời gian bắt đầu học thứ 2',
            'end_monday' => 'Thời gian kết thúc học thứ 2',
            'tuesday' => 'Thứ 3',
            'start_tuesday' => 'Thời gian bắt đầu học thứ 3',
            'end_tuesday' => 'Thời gian kết thúc học thứ 3',
            'wednesday' => 'Thứ 4',
            'start_wednesday' => 'Thời gian bắt đầu học thứ 4',
            'end_wednesday' => 'Thời gian hết thúc học thứ 4',
            'thurday' => 'Thứ 5',
            'start_thurday' => 'Thời gian bắt đầu học thứ 5',
            'end_thurday' => 'Thời gian kết thúc học thứ 5',
            'friday' => 'Thứ 6',
            'start_friday' => 'Thời gian bắt đầu học thứ 6',
            'end_friday' => 'Thời gian kết thúc học thứ 6',
            'saturday' => 'Thứ 7',
            'start_saturday' => 'Thời gian bắt đầu học thứ 7',
            'end_saturday' => 'Thời gian kết thứ học thứ 7',
            'sunday' => 'Chủ nhật',
            'start_sunday' => 'Thời gian bắt đầu học chủ nhật',
            'end_sunday' => 'Thời gian kết thức học chủ nhật'
        ];
    }
}
