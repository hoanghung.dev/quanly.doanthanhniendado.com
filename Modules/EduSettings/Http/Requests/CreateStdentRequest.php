<?php

namespace Modules\EduSettings\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateStdentRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5',
            'email' => 'unique:students,email|email|max:255',
            'phone' => 'unique:students,phone|min:10|max:11',
            'password' => 'required|min:8',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
