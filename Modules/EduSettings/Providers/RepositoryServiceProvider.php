<?php

namespace Modules\EduSettings\Providers;

use Illuminate\Support\ServiceProvider;
use function Psy\bin;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            \Modules\EduSettings\Repositories\Student\StudentRepository::class,
            \Modules\EduSettings\Repositories\Student\StudentRepositoryEloquent::class
        );
        $this->app->bind(
            \Modules\EduSettings\Repositories\Classs\ClassRepository::class,
            \Modules\EduSettings\Repositories\Classs\ClassRepositoryEloquent::class
        );
        //:end-bindings:
    }
}
