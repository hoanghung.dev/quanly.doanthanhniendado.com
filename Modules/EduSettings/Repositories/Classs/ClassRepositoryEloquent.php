<?php

namespace Modules\EduSettings\Repositories\Classs;


use Modules\EduSettings\Repositories\Student\StudentRepository;

class ClassRepositoryEloquent extends StudentRepository
{
    public function getModel()
    {
        return \Modules\EduSettings\Entities\Classs::class;
    }

}

