<?php

namespace Modules\EduSettings\Repositories\Student;


class StudentRepositoryEloquent extends StudentRepository
{
    public function getModel()
    {
        return \Modules\EduSettings\Entities\Student::class;
    }

}
