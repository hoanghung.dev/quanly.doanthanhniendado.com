<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'student'], function () {
        Route::get('/', 'EduSettingsController@getIndex')->middleware('permission:student_view');
        Route::get('add', 'EduSettingsController@getAdd')->middleware('permission:student_add');
        Route::post('add', 'EduSettingsController@add')->middleware('permission:student_add');
        Route::get('delete/{id}', 'EduSettingsController@delete')->middleware('permission:student_delete');
        Route::post('multi-delete', 'EduSettingsController@multiDelete')->middleware('permission:student_delete');
        Route::get('search-for-select2', 'EduSettingsController@searchForSelect2')->name('user.search_for_select2')->middleware('permission:student_view');
        Route::get('{id}', 'EduSettingsController@getUpdate');
        Route::post('{id}', 'EduSettingsController@update')->middleware('permission:student_edit');
    });

    Route::group(['prefix' => 'class'], function () {
        Route::get('/', 'ClassController@index')->middleware('permission:student_view');
        Route::get('add', 'ClassController@create')->middleware('permission:student_add');
        Route::post('add', 'ClassController@store')->middleware('permission:student_add');
        Route::get('delete/{id}', 'ClassController@delete')->middleware('permission:student_delete');
        Route::post('multi-delete', 'ClassController@multiDelete')->middleware('permission:student_delete');
        Route::get('search-for-select2', 'ClassController@searchForSelect2')->name('user.search_for_select2')->middleware('permission:student_view');
        Route::get('{id}', 'ClassController@getUpdate');
        Route::post('{id}', 'ClassController@update')->middleware('permission:student_edit');
    });
});
