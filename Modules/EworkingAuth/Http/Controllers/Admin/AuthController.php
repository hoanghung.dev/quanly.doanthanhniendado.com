<?php

namespace Modules\EworkingAuth\Http\Controllers\Admin;

use App\Models\Admin;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Session;
use Auth;
use Mail;
use App\Http\Helpers\CommonHelper;
use URL;

class AuthController extends Controller
{

    public function authenticate(Request $request)
    {
        $admin = Admin::where('email', $request['email'])->first();
        if (!is_object($admin)) {
            CommonHelper::one_time_message('danger', 'Vui lòng kiểm tra lại Email/Mật khẩu của bạn');
            return redirect('admin/login');
        }
        if (@$admin->status == 0) {
            CommonHelper::one_time_message('danger', 'Tài khoản của bạn chưa được kích hoạt!');
            return redirect('admin/login');
        }

        if (@$admin->status == -1) {
            CommonHelper::one_time_message('danger', 'Tài khoản của bạn đã bị khóa!');
            return redirect('admin/login');
        }

        if (\Auth::guard('admin')->attempt(['email' => trim($request['email']), 'password' => trim($request['password'])])) {
            if ($admin->last_company_id == null && !CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'super_admin')) {
                return redirect('/create-company');
            }

            return redirect()->intended('admin/dashboard');
        } else {
            CommonHelper::one_time_message('danger', 'Vui lòng kiểm tra lại Email/Mật khẩu của bạn');
            return redirect('admin/login');
        }
    }
}
