<?php
Route::group(['prefix' => 'admin', 'middleware' => 'no_auth:admin'], function () {
    Route::post('authenticate', 'Admin\AuthController@authenticate');
});