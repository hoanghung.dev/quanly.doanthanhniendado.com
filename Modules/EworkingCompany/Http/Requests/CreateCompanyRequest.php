<?php

namespace Modules\EworkingCompany\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_company' => 'required',
            'short_name_company' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name_company.required' => 'Bắt buộc phải nhập tên công ty!',
            'short_name_company.required' => 'Bắt buộc phải nhập tên ngắn gọn công ty!',
        ];
    }
}
