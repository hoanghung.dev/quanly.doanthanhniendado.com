<?php

namespace Modules\EworkingCompany\Models;

use App\Models\Roles;
use Illuminate\Database\Eloquent\Model;
use Modules\EworkingAdmin\Models\Admin;
use Modules\EworkingInternalNotification\Models\InternalNotification;
use Modules\EworkingInviteCompany\Models\InviteHistory;
use Modules\EworkingJob\Models\Job;
use Modules\EworkingJob\Models\JobType;
use Modules\EworkingJob\Models\RoleAdmin;
use Modules\EworkingJob\Models\Subject;
use Modules\EworkingJob\Models\Task;
use Modules\EworkingProject\Models\MoneyHistory;
use Modules\EworkingProject\Models\Project;
use Modules\EworkingService\Models\Service;
use Modules\EworkingService\Models\ServiceHistory;
use Modules\EworkingUser\Models\User;

class Company extends Model
{
    protected $table = 'companies';

    public $timestamps = false;
    protected $fillable = [
        'id','name', 'link', 'image', 'intro', 'id', 'url', 'short_name', 'email', 'tel', 'admin_id', 'msdn', 'address', 'exp_date', 'account_max', 'service_id', 'company_id', 'slug'
    ];

    public function invite_history()
    {
        return $this->hasMany(InviteHistory::class, 'company_id', 'id');
    }

    public function service_history()
    {
        return $this->hasMany(ServiceHistory::class, 'company_id', 'id');
    }

    public function service()
    {
        return $this->hasOne(Service::class, 'id', 'service_id');
    }

    public function admin()
    {
        return $this->hasOne(Admin::class, 'id', 'admin_id');
    }

    public function internal_notifications()
    {
        return $this->hasMany(InternalNotification::class, 'company_id', 'id');
    }

    public function job()
    {
        return $this->hasMany(Job::class, 'company_id', 'id');
    }

    public function job_types()
    {
        return $this->hasMany(JobType::class, 'company_id', 'id');
    }

    public function money_history()
    {
        return $this->hasMany(MoneyHistory::class, 'company_id', 'id');
    }

    public function projects()
    {
        return $this->hasMany(Project::class, 'company_id', 'id');
    }

    public function roles()
    {
        return $this->hasMany(Roles::class, 'company_id', 'id');
    }

    public function role_admin()
    {
        return $this->hasMany(RoleAdmin::class, 'company_id', 'id');
    }

    public function subjects()
    {
        return $this->hasMany(Subject::class, 'company_id', 'id');
    }

    public function tasks()
    {
        return $this->hasMany(Task::class, 'company_id', 'id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'company_id', 'id');
    }

    public function delete()
    {
        $this->users()->delete();
        $this->internal_notifications()->delete();
        $this->job()->delete();
        $this->job_types()->delete();
        $this->money_history()->delete();
        $this->projects()->delete();
        $this->roles()->delete();
        $this->role_admin()->delete();
        $this->service_history()->delete();
        $this->invite_history()->delete();
        $this->subjects()->delete();
        $this->tasks()->delete();

        // delete the user
        return parent::delete();
    }
}
