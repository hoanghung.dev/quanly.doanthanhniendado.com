<!DOCTYPE html>
<html>
<head>
    {{--    @include('admin.common.head_meta')--}}
    @include('admin.themes.metronic1.partials.head_meta')
    <link rel="stylesheet" type="text/css"
          href="{{ asset('public/backend/create_company/css/opensans-font.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('public/backend/create_company/css/montserrat-font.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('public/backend/create_company/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css') }}">
    <!-- Main Style Css -->
    <link rel="stylesheet" href="{{ asset('public/backend/create_company/css/style.css') }}"/>
    <link href="{{ asset('public/libs/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css"/>
</head>
<body>

<div class="page-content">
    <div class="wizard-heading">Chào mừng bạn đến với {{ @$settings['name'] }}!</div>
    @if(\App\Models\RoleAdmin::where('admin_id', \Auth::guard('admin')->user()->id)->where('status', 1)->count() == 0)
        <p style="
    color: rgba(255, 255, 255, 0.92);
    font-family: 'Montserrat', sans-serif;
    font-weight: 700;
    ">Bạn chưa tham gia công ty nào!</p>
        <p style="
    color: rgba(255, 255, 255, 0.92);
    font-family: 'Montserrat', sans-serif;
    font-weight: 700;
">Vui lòng tạo mới công ty của bạn hoặc nhận lời mời tham gia 1 công ty!.</p>
    @else
        <p style="
    color: rgba(255, 255, 255, 0.92);
    font-family: 'Montserrat', sans-serif;
    font-weight: 700;
">Tạo mới công ty của bạn</p>
    @endif

    <div class="wizard-v7-content">
        <div class="wizard-form">
            <form class="form-register" method="post" enctype="multipart/form-data">
                <div id="form-total" class="wizard clearfix vertical">
                    <div class="steps clearfix">
                        <ul role="tablist">
                            <li id="form-total-t-0 one" class="first current create-company">
                                <div class="title">
                                    <p class="step-icon">
                                        <span>1</span>
                                    </p>
                                    <div class="step-text">
                                        <span class="step-inner-1">Tạo công ty</span>
                                    </div>
                                </div>

                            </li>
                            <li id="two" class="invite">
                                <div class="title">
                                    <p class="step-icon">
                                        <span>2</span>
                                    </p>
                                    <div class="step-text">

                                        <span class="step-inner-1"><b
                                                    style="border: 1px solid #fff; border-radius: 50%; padding: 0 5px;">{{ number_format(count($invites)) }}</b> Lời mời</span>
                                    </div>
                                </div>

                            </li>
                            <li style="background: none;    margin-bottom: 0px;"><a class="create_company_btn"
                                                                                    href="/admin/dashboard"><i
                                            class="fa fa-home"
                                            aria-hidden="true"></i>
                                    Về trang chủ</a>
                            </li>
                            <li style="background: none">
                                <a class="create_company_btn" href="/admin/logout"><i class="fa fa-sign-out"
                                                                                      aria-hidden="true"></i>
                                    Đăng xuất</a>
                            </li>
                        </ul>
                    </div>
                    <div class="content clearfix">
                        <!-- SECTION 1 -->
                        <div class="section-1">
                            <section id="form-total-p-0" class="body current" aria-hidden="false">
                                <div class="inner">
                                    <div class="wizard-header">
                                        <h3 class="heading">Tạo mới công ty</h3>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-holder form-holder-2">
                                            <label for="short_name_company">Tên ngắn gọn <span
                                                        style="color: red">*</span></label>
                                            <input type="text" name="short_name_company" id="short_name_company"
                                                   class="form-control" placeholder="Tên ngắn gọn">
                                            @if ($errors->has('short_name_company'))
                                                <p class="text-danger" style=" margin: 0">{{$errors->first('short_name_company')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder form-holder-2">
                                            <label for="name_company">Tên công ty <span
                                                        style="color: red">*</span></label>
                                            <input type="text" name="name_company" id="name_company"
                                                   class="form-control" placeholder="Tên công ty">
                                            @if ($errors->has('name_company'))
                                                <p class="text-danger" style=" margin: 0">{{$errors->first('name_company')}}</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-holder form-holder-2">
                                            <label for="img">Chọn logo</label>
                                            <input type="file" name="img" id="img" class="form-control"
                                                   placeholder="Chọn logo">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder form-holder-2">
                                            <label for="your_email">Email</label>
                                            <input type="email" name="your_email" id="your_email" class="form-control"
                                                   placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder form-holder-2">
                                            <label for="tel">Số điện thoại</label>
                                            <input type="text" name="tel" id="tel" class="form-control"
                                                   placeholder="Số điện thoại">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder form-holder-2">
                                            <label for="code_company">Mã số doanh nghiệp</label>
                                            <input type="text" name="code_company" id="code_company"
                                                   class="form-control" placeholder="Mã số doanh nghiệp">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder form-holder-2">
                                            <label for="website">Website</label>
                                            <input type="text" name="website" id="website" class="form-control"
                                                   placeholder="Website">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder form-holder-2">
                                            <label for="address">Địa chỉ</label>
                                            <input type="text" name="address" id="address" class="form-control"
                                                   placeholder="Địa chỉ">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder form-holder-2">
                                            <label for="info">Mô tả</label>
                                            <textarea name="info" id="info" class="form-control" style="width: 100%;"
                                                      placeholder="Mô tả" rows="5"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder form-holder-2">
                                            <button type="submit">Tạo công ty</button>

                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="section-2" style="display: none">
                            <section id="form-total-p-1" class="body" aria-hidden="true">
                                <div class="inner">
                                    @if(Session::has('thongbao') && !Auth::check())
                                        <div class="alert text-center" role="alert"
                                             style=" margin: 0; font-size: 16px; color: red;">
                                            <a href="#" style="float:right;" class="alert-close" data-dismiss="alert">&times;</a>
                                            {{ Session::get('thongbao') }}
                                        </div>
                                    @endif
                                    <div class="wizard-header">
                                        <h3 class="heading">Mời tham gia công ty</h3>
                                    </div>
                                    @foreach($invites as $val)
                                        <div class="form-row invite-company-item">
                                            <div class="border-invite">
                                                <div class="content_invite">
                                                    <span>Công ty <b>{{ @$val->company->name }}</b> mời bạn tham gia vào vị trí <b>{{@$val->role->display_name}}</b></span>
                                                    <p>Nội dung: {!! @$val->intro !!}</p>
                                                </div>
                                                <a class="accept_company" data-company_id="{{ @$val->company_id }}"
                                                   data-role_id="{{ $val->role_id }}" data-email="{{ @$val->email }}"
                                                   style="cursor: pointer;">Xác
                                                    nhận</a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="{{ asset('public/backend/create_company/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('public/libs/toastr/toastr.min.js') }}"></script>
@if(Session::has('message'))
    <script>
        toastr.{{ Session::get('alert-class') }}("{!! Session::get('message') !!}");
    </script>
@endif
<script>

    $('.accept_company').click(function () {
        var company_id = $(this).data('company_id');
        var role_id = $(this).data('role_id');
        var email = $(this).data('email');
        var object = $(this);
        $.ajax({
            url: '/admin/invite_company/ajax-accept-join-company',
            data: {
                company_id: company_id,
                role_id: role_id,
                email: email
            },
            success: function (resp) {
                if (resp.status == true) {
                    toastr.success(resp.msg);
                    object.parents('.invite-company-item').remove();
                } else {
                    toastr.error(resp.msg);
                }
            },
            error: function () {
                toastr.error("Có lỗi xảy ra! Vui lòng load lại website và thử lại");
            }
        });
    });

    $(document).ready(function () {
        $('body').on('click', 'li.create-company', function () {
            $('.section-1').css('display', 'block');
            $('.section-2').css('display', 'none');
            $('.create-company').addClass('current');
            $('.invite').removeClass('current');
        });
        $('body').on('click', 'li.invite', function () {
            $('.section-2').css('display', 'block');
            $('.section-1').css('display', 'none');
            $('.invite').addClass('current');
            $('.create-company').removeClass('current');
        });


        // Auto render slug
        $('input[name=name_company]').keyup(function () {
            var str = $(this).val();
            str = str.toLowerCase();
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
            str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
            str = str.replace(/đ/g, "d");
            str = str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:| |\&|~/g, "-");
            str = str.replace(/-+-/g, "-"); //thay thế 2- thành 1-
            str = str.replace(/^\-+|\-+$/g, "");//cắt bỏ ký tự - ở đầu và cuối chuỗi
            // $('input[name=slug]').val(str);
            $('input[name=url]').val(str);
        });
        $('').click(function () {
            $('li#one').addClass('current');
            $('li#two').removeClass('current');
        });
        $('li#two').click(function () {
            $('li#two').addClass('current');
            $('li#one').removeClass('current');
        });
    });
</script>
</body>
</html>