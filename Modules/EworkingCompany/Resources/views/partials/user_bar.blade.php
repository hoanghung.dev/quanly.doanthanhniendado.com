<?php
$data = \DB::table('companies')
    ->join('role_admin', 'companies.id', '=', 'role_admin.company_id')
    ->selectRaw('companies.name, companies.short_name, companies.id, companies.image')
    ->where('role_admin.status', 1)
    ->where('role_admin.admin_id', \Auth::guard('admin')->user()->id)
    ->get();
?>
@foreach($data as $v)
    <a href="{{URL::to('/admin/company/apply-witch/'.$v->id)}}"
       class="kt-notification__item " style="@if(\Auth::guard('admin')->user()->last_company_id == $v->id ) border: 1.5px solid #469408b0;   @endif margin:10px;">
        <div class="kt-notification__item-icon">
            <img style="max-width: 30px; max-height: 30px;"
                 src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image,100,100)}}"
                 title="{{$v->name}}"
                 alt="{{$v->name}}">
        </div>
        <div class="kt-notification__item-details">
            <div class="kt-notification__item-title kt-font-bold">
                {{$v->short_name}}
            </div>
        </div>
    </a>
@endforeach