<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'company'], function () {
        Route::get('', 'Admin\CompanyController@getIndex')->name('company')->middleware('permission:super_admin');
        Route::get('publish', 'Admin\CompanyController@getPublish')->name('company.publish')->middleware('permission:company_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\CompanyController@add')->middleware('permission:company_add');
        Route::get('delete', 'Admin\CompanyController@deleteByAdmin');
        Route::get('delete/{id}', 'Admin\CompanyController@delete');

//        Route::get('create', 'Admin\CompanyController@getCreateCompanyByAdmin');
//        Route::post('create', 'Admin\CompanyController@postCreateCompanyByAdmin');
        Route::post('multi-delete', 'Admin\CompanyController@multiDelete')->middleware('permission:company_delete');
        Route::get('logout/{id}', 'Admin\CompanyController@logout');
        Route::get('search-for-select2', 'Admin\CompanyController@searchForSelect2')->name('company.search_for_select2')->middleware('permission:company_view');

        Route::get('apply-witch/{id}', 'Admin\CompanyController@switchCompany')->name('company.switch');
        Route::get('{id}', 'Admin\CompanyController@update')->middleware('permission:company_view');
        Route::post('{id}', 'Admin\CompanyController@update')->middleware('permission:company_edit');
    });
});

Route::group(['prefix' => '', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::get('create-company', 'Admin\CompanyController@getCreateCompany');
});
Route::post('create-company', 'Admin\CompanyController@postCreateCompany');
