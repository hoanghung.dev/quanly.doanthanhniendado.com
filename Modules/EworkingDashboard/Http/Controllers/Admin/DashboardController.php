<?php

namespace Modules\EworkingDashboard\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Auth;
use DB;
use Illuminate\Http\Request;
use Mail;
use Modules\EworkingAdmin\Models\Admin;
use Modules\EworkingCompany\Models\Company;
use Modules\EworkingInternalNotification\Models\InternalNotification;
use Modules\EworkingJob\Models\Job;
use Modules\EworkingJob\Models\Subject;
use Modules\EworkingJob\Models\Task;
use Modules\EworkingNotification\Models\Notifications;
use Modules\EworkingProject\Models\Project;
use Modules\EworkingService\Models\Service;
use Modules\EworkingService\Models\ServiceHistory;
use Modules\EworkingUser\Models\User;

class DashboardController extends Controller
{
    protected $module = [
    ];

    public function dashboard()
    {
        if (CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'super_admin')) {
            return redirect('/admin/dashboard/software');
        }

        if (\Auth::guard('admin')->user()->company_ids == null) {
            CommonHelper::one_time_message('error', 'Bạn chưa tham gia công ty nào. Vui lòng tạo công ty mới hoặc chấp nhận lời mời tham gia công ty!');
            return redirect('/create-company');
        }
        $data['total_users_count'] = User::count();
        $data['today_users_count'] = User::whereDate('created_at', DB::raw('CURDATE()'))->count();


        $company_ids = trim(\Auth::guard('admin')->user()->company_ids, '|');
        $arr = explode('|', $company_ids);
        $data['datas'] = Company::whereIn('id', $arr)->select('short_name', 'image', 'name', 'id')->get();
        $company = $arr;
        $data['company'] = $company;

        $data['page_title'] = 'Thống kê chung';
        $data['page_type'] = 'list';
//      Thông báo
        $company_ids_notification = $arr;
        $data['notification'] = Notifications::whereIn('company_id', $company_ids_notification)
            ->where('to_admin_id', \Auth::guard('admin')->user()->id)->orderBy('id', 'desc')->get();
//        Thống kê công việc(Nhiệm vụ)
        $toDay = date('Y-m-d');
       $data_deadline = \Modules\EworkingJob\Http\Helpers\EworkingJobHelper::deadline($arr,\Auth::guard('admin')->user()->id);
       $data = array_merge($data_deadline['data'],$data) ;
        return view('eworkingdashboard::dashboard', $data);
    }

    public function company()
    {
        $data['total_users_count'] = User::count();
        $data['today_users_count'] = User::whereDate('created_at', DB::raw('CURDATE()'))->count();
        $data['page_title'] = 'Thống kê công ty';
        $data['page_type'] = 'list';

        $company_ids = trim(Auth::guard('admin')->user()->company_ids, '|');

        $company = [];
        if (!empty($company_ids)) {
            $company = explode('|', $company_ids);
        }
        $data['company'] = $company;
        $data['job_doing'] = Task::where('status', 2)->where('company_id', \Auth::guard('admin')->user()->last_company_id)->count();
        $data['job_success_in_month'] = Task::where('end_date', 'like', date('Y') . '-' . date('m') . '-%')->where('company_id', \Auth::guard('admin')->user()->last_company_id)->count();
        $data['job_success_in_year'] = Task::where('end_date', 'like', date('Y') . '-%')->where('company_id', \Auth::guard('admin')->user()->last_company_id)->count();
        $data['project_total'] = Project::where('company_id', \Auth::guard('admin')->user()->last_company_id)->whereNotNull('name')->where('name', '<>', '')
            ->count();
        $data['user'] = Admin::where('company_ids', 'like', '%|' . \Auth::guard('admin')->user()->last_company_id . '|%')->count();
        $data['list_user'] = \App\Models\RoleAdmin::where('company_id', Auth::guard('admin')->user()->last_company_id)->where('status', 1)->get();
        if($data['list_user']->count() <=10){
            $data['chart_height'] = '150px';
        }elseif ($data['list_user']->count() <=20){
            $data['chart_height'] = '300px';
        }elseif ($data['list_user']->count() <=30){
            $data['chart_height'] = '450px';
        }elseif ($data['list_user']->count() <=40){
            $data['chart_height'] = '600px';
        }elseif ($data['list_user']->count() <=50){
            $data['chart_height'] = '750px';
        }
        $toDay = date('Y-m-d');
        if (!empty($company)) {
            $beforeDeadlines = Job::where('company_id', Auth::guard('admin')->user()->last_company_id)
                ->where('status', 1)->get();
            $arrbeforeDeadlines = [];
            $arrId = [];
            foreach ($beforeDeadlines as $key => $before) {
                $query = Task::where('job_id', $before->id)->whereIn('task_type', [1, 2, 3])->where(function ($query) use ($company, $toDay) {
                    $findCompany = Company::where('id', Auth::guard('admin')->user()->last_company_id);
                    if ($findCompany->exists()) {
                        $findCompany = $findCompany->first();
                        $DayAfter = date('Y-m-d', strtotime('+' . @$findCompany->before_deadline . ' day'));
                        $query->whereDate('tasks.end_date', '>=', $toDay)
                            ->whereDate('tasks.end_date', '<=', $DayAfter);
                    }
                });

                $end_date = $query->max('end_date');
                $start_date = $query->min('start_date');

                if (!is_null($end_date) && !is_null($start_date)) {
                    $before->start_date = $start_date;
                    $before->end_date = $end_date;
                    $arrId[] = $before->id;

                    $arrbeforeDeadlines[] = $before;

                }
            }

            $afterDeadlines = Job::where('company_id', Auth::guard('admin')->user()->last_company_id)
                ->whereNotIn('id', $arrId)
                ->where('status', 1)->get();
            $arrAfterDeadlines = [];
            foreach ($afterDeadlines as $key => $after) {
                $query = Task::where('job_id', $after->id)->whereIn('task_type', [1, 2, 3])->where(function ($query) use ($company, $toDay) {
//                    foreach ($company as $com) {
                    $findCompany = Company::where('id', Auth::guard('admin')->user()->last_company_id);
                    if ($findCompany->exists()) {
                        $findCompany = $findCompany->first();
                        $DayAfter = date('Y-m-d', strtotime('+' . @$findCompany->before_deadline . ' day'));
                        $query->whereDate('tasks.end_date', '>=', $toDay)
                            ->whereDate('tasks.end_date', '<=', $DayAfter);
                    }
//                    }
                });
                $end_date = $query->max('end_date');
                $start_date = $query->min('start_date');

                if (!is_null($end_date) && !is_null($start_date)) {
                    $after->start_date = $start_date;
                    $after->end_date = $end_date;
                    $arrbeforeDeadlines[] = $after;
                }
            }


            //            afterDeadlinesUser
            $arrbeforeDeadlineUser = [];
            foreach ($arrbeforeDeadlines as $arrbefore) {
                $admin = \Modules\EworkingJob\Http\Helpers\EworkingJobHelper::getAdminInJob($arrbefore->id);
                if (in_array(Auth::guard('admin')->user()->id, explode('|', $admin))) {
                    $arrbeforeDeadlineUser[] = $arrbefore;
                }
            }
            //            beforeDeadlinesUser
            $arrAfterDeadlinesUser = [];
            foreach ($arrAfterDeadlines as $arrAfter) {
                $admin = \Modules\Modules\EworkingJob\Http\Helpers\EworkingJobHelper::getAdminInJob($arrAfter->id);
//                dd(Auth::guard('admin')->user()->id)
                if (in_array(Auth::guard('admin')->user()->id, explode('|', $admin))) {
                    $arrAfterDeadlinesUser[] = $arrAfter;
                }
            }
//            dd([$arrAfterDeadlinesUser, $arrbeforeDeadlineUser]);

            $data['internalNotifications'] = InternalNotification::where('company_id', \Auth::guard('admin')->user()->last_company_id)
                ->where(function ($query) {
                    $query->where('admin_ids', 'like', '|' . \Auth::guard('admin')->user()->id . '|');
                    $query->orWhereNull('admin_ids');
                })
                ->where(function ($query) {
                    $query->where('deadline', '>=', date('Y-m-d'));
                    $query->orWhereNull('deadline');
                })
                ->orderBy('id', 'desc')
                ->get();
//            $jobDeadlinesUser = array_merge($jobafterDeadlines, $jobbeforeDeadlines);
            sort($arrbeforeDeadlines);
            ksort($arrAfterDeadlines);
            ksort($arrAfterDeadlinesUser);
            sort($arrbeforeDeadlineUser);
            $data['DeadlinesUser'] = 1;
            $data['beforeDeadlines'] = $arrbeforeDeadlines;
            $data['afterDeadlines'] = $arrAfterDeadlines;
            $data['arrAfterDeadlinesUser'] = $arrAfterDeadlinesUser;
            $data['arrbeforeDeadlineUser'] = $arrbeforeDeadlineUser;
        }
        return view('eworkingdashboard::dashboard_company', $data);
    }

    public function software()
    {
        //  Check permission


        $data['page_title'] = 'Thống kê phần mềm';
        $data['page_type'] = 'list';

        $data['service_history_pending'] = @ServiceHistory::where('status', 0)->get();
        $data['company_all'] = @Company::select('id')->whereDate('exp_date', '>=', date('Y-m-d'))->get()->count();

        $data['admin_all'] = @Admin::select('status')->where('status', 1)->get()->count();
        $data['service_all'] = @Service::select('id')->get()->count();
        $data['service_history_all'] = @ServiceHistory::select('status')->where('payment', 0)->where('status', 1)->orwhere('status', -1)->get()->count();
        $data['money_service_all'] = @ServiceHistory::select('status', 'payment')->where('status', 1)->orwhere('status', -1)->where('payment', '>=', 0)->sum('payment');
        $data['subject_null_all'] = @Subject::select('company_id')->whereNull('company_id')->get()->count();
        return view('eworkingdashboard::dashboard_software', $data);
    }


    public function tooltipInfo(Request $request)
    {
        $modal = new $request->modal;
        $data['item'] = $modal->find($request->id);
        $data['tooltip_info'] = $request->tooltip_info;

        return view('admin.common.modal.tooltip_info', $data);
    }

    public function ajax_up_file(Request $request)
    {
        if ($request->has('file')) {
            $file = CommonHelper::saveFile($request->file('file'));
        }
        return $file;
    }
}
