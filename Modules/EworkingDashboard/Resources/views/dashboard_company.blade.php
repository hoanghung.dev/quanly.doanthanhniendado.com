@extends(config('core.admin_theme').'.template')
@section('main')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        {{--Danh sách Công ty--}}
        <div class="row">
            <div class="col-xs-12 col-lg-12 col-xl-12 col-lg-12 order-lg-1 order-xl-1">
                @include('eworkingdashboard::partials.list_companies')
            </div>
        </div>

        {{--Công việc--}}
        <div class="row">
            <div class="col-xs-12 col-lg-12 col-xl-12 col-lg-12 order-lg-1 order-xl-1">
                <!--begin:: Widgets/Finance Summary-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                <i class="icon-cursor font-dark hide"></i>
                                Công việc
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-widget12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-lg-4 ">
                                    <p class="text-center bold">Hiện tại</p>
                                    <canvas id="myChart_now" height="{{$chart_height}}"></canvas>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-lg-4">
                                    <p class="text-center bold ">Tháng {{date('m')}}</p>
                                    <canvas id="myChart_month" height="{{$chart_height}}"></canvas>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-lg-4">
                                    <p class="text-center bold">Năm {{date('Y')}}</p>
                                    <canvas id="myChart_year" height="{{$chart_height}}"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6 col-lg-6 col-xl-6 col-lg-6 order-lg-1 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Công việc
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-widget12">
                            <div class="kt-widget12__content">
                                <div class="kt-widget12__item">
                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">Công việc đang làm</span>
                                        <b class="kt-widget12__value"><span
                                                    title="Số công việc đang thực hiện">{{ number_format($job_doing, 0, '.', '.')  }}</span>/
                                            <span title="Số công việc thực hiện trong tháng">{{ number_format($job_success_in_month, 0, '.', '.')  }}</span>/
                                            <span title="Số công việc kể từ đầu năm">{{ number_format($job_success_in_year, 0, '.', '.')  }}</span>
                                        </b>
                                    </div>

                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">Tổng số dự án</span>
                                        <b class="kt-widget12__value">{{ number_format($project_total, 0, '.', '.')  }}</b>
                                        <span class="kt-widget12__value"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if (CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'money_history_view'))
                <div class="col-xs-6 col-lg-6 col-xl-6 col-lg-6 order-lg-1 order-xl-1">
                    <!--begin:: Widgets/Finance Summary-->
                    <div class="kt-portlet kt-portlet--height-fluid">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title bold uppercase">
                                    Lịch sử tiền
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            @php
                                $common_total_vi = \Modules\EworkingProject\Models\Project::select('common_total_vi')->where('company_id', \Auth::guard('admin')->user()->last_company_id)->sum('common_total_vi');
                        //        tiền chi
                                $debit = \Modules\EworkingProject\Models\MoneyHistory::where('type', 1)->where('company_id', \Auth::guard('admin')->user()->last_company_id)->select('value')->sum('value');
                        //        tiền thu
                                $credited = \Modules\EworkingProject\Models\MoneyHistory::where('type', 0)->where('company_id', \Auth::guard('admin')->user()->last_company_id)->select('value')->sum('value');
                        //        số dư
                                $surplus_vi = (int) $credited - (int) $debit;
                        //        Tiền chưa thu
                                $common_rest_vi = (int) $common_total_vi - (int) $credited;
                            @endphp
                            <div class="kt-widget12">
                                <div class="kt-widget12__content">
                                    <div class="kt-widget12__item">
                                        {{--<div class="kt-widget12__info">--}}
                                        {{--<span class="kt-widget12__desc">Tổng tiền dịch vụ</span>--}}
                                        {{--<span class="kt-widget12__value">{{ number_format(\App\Models\ServiceHistory::where('payment','>',0)->where('company_id', \Auth::guard('admin')->user()->last_company_id)->sum('payment'), 0, '.', '.') }}<sup>đ</sup></span>--}}
                                        {{--</div>--}}

                                        <div class="kt-widget12__info">
                                            <span class="kt-widget12__desc">Tổng tiền dự án</span>
                                            <span class="kt-widget12__value">{{ number_format($common_total_vi, 0, '.', '.') }}<sup>đ</sup></span>
                                        </div>
                                    </div>
                                    <div class="kt-widget12__item">
                                        <div class="kt-widget12__info">
                                            <span class="kt-widget12__desc">Tổng tiền đã thu</span>
                                            <span class="kt-widget12__value">{{ number_format(@$credited, 0, '.', '.') }}<sup>đ</sup></span>
                                        </div>

                                        <div class="kt-widget12__info">
                                            <span class="kt-widget12__desc">Tổng tiền đã chi</span>
                                            <span class="kt-widget12__value">{{ number_format($debit, 0, '.', '.') }}<sup>đ</sup></span>
                                        </div>
                                    </div>
                                    <div class="kt-widget12__item">
                                        <div class="kt-widget12__info">
                                            <span class="kt-widget12__desc">Số dư</span>
                                            <span class="kt-widget12__value">{{ number_format($surplus_vi, 0, '.', '.') }}<sup>đ</sup></span>
                                        </div>

                                        <div class="kt-widget12__info">
                                            <span class="kt-widget12__desc">Tổng tiền chưa thu</span>
                                            <span class="kt-widget12__value">{{ number_format($common_rest_vi, 0, '.', '.') }}<sup>đ</sup></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        {{-- Nhắc nhở deadlinE--}}
        @include('eworkingdashboard::promt')
        {{--thong bao noi bo--}}
        <div class="row">
            <div class="col-xl-12 order-lg-2 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Thông báo nội bộ
                            </h3>
                        </div>

                    </div>
                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                             id="kt_datatable_latest_orders" style="">
                            <table class="kt-datatable__table" style="display: block; max-height: 500px;">
                                <thead class="kt-datatable__head">
                                <tr class="kt-datatable__row" style="left: 0px;">
                                    <th class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 252px;">Tiêu đề</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 200px;">Nội dung</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 100px;">Ngày gửi</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 100px;">Người gửi</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="kt-datatable__body ps ps--active-y"
                                       style="max-height: 450px; overflow: auto!important;">

                                @if(isset($internalNotifications) && count($internalNotifications) > 0)
                                    @foreach($internalNotifications as $noti_id)
                                        <tr data-row="0" class="kt-datatable__row" style="left: 0px;"
                                            style="border: 2px solid #ccc;">
                                            <td class="kt-datatable__cell">
                                                       <span style="width: 252px;">
                                                           <span class="kt-font-bold">{{@$noti_id->title}}</span>
                                                       </span>
                                            </td>
                                            <td class="kt-datatable__cell">
                                                       <span style="width: 200px;">
                                                           <span class="kt-font-bold">{!! @$noti_id->title !!}</span>
                                                       </span>
                                            </td>
                                            <td class="kt-datatable__cell">
                                                       <span style="width: 100px;">
                                                           <span class="kt-font-bold">{{ date('d/m/Y',strtotime($noti_id->created_at))}}</span>
                                                       </span>
                                            </td>
                                            <td class="kt-datatable__cell">
                                                       <span style="width: 100px;">

                                                           <div class="board-header-facepile js-fill-facepile js-list-draggable-board-members">
                                                                   <div class="member js-member ui-draggable">
                                                                       <a href="/admin/profile/{{@$noti_id->admin_id}}">
                                                                       <img class="member-avatar" height="30" width="30"
                                                                            src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$noti_id->Admin_ids->image , 30, 30) }}"
                                                                            alt="{{ @$noti_id->Admin_ids->name }}"
                                                                            title="{{ @$noti_id->Admin_ids->name  }}">
                                                                           </a>
                                                                   </div>
                                                           </div>
                                                       </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                        <td class="kt-datatable__cell">
                                                   <span style="width: 100%; text-align: center">
                                                       <span class="kt-font-bold">Chưa có thông báo nào!</span>
                                                   </span>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('custom_head')
    {{--    <link href="https://www.keenthemes.com/preview/metronic/theme/assets/global/css/components.min.css" rel="stylesheet"--}}
    {{--          type="text/css">--}}
    <style type="text/css">
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
    <style type="text/css">
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>

@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

    <script>
        var ctx = document.getElementById('myChart_now').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: [
                    @foreach($list_user as $val)
                        '{{@$val->admin->name}}',
                    @endforeach
                ],
                datasets: [{
                    label: "Số công việc",
                    data: [
                        @foreach ($list_user as $value)
                        {{\Modules\EworkingJob\Models\Task::whereIn('task_type',[1,2,3])->where('admin_ids','like','%|' . $value->admin_id . '|%')->where('company_id', \Auth::guard('admin')->user()->last_company_id)->whereDate('end_date','>=',date('Y-m-d'))->where('status',2)->get()->count()}},
                        @endforeach
                    ],
                    backgroundColor: [

                        @foreach ($list_user as $v)
                                @if(@\App\Models\RoleAdmin::where('admin_id',$v->id)->where('company_id',\Auth::guard('admin')->user()->last_company_id)->first()->status != 1)
                            '#333',
                        @else
                            '#39cccc',
                        @endif
                        @endforeach
                    ],
                    borderColor: [
                        @foreach ($list_user as $key)
                            'rgba(255, 99, 132, 1)',
                        @endforeach

                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            beginAtZero: true,
                        }
                    }]
                },
                legend: {
                    display: false,
                },
            }
        });

        var ctx = document.getElementById('myChart_month').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: [
                    @foreach($list_user as $val)
                        '{{@$val->admin->name}}',
                    @endforeach
                ],
                datasets: [{
                    data: [
                        @foreach ($list_user as $value)
                        {{\Modules\EworkingJob\Models\Task::whereIn('task_type',[1,2,3])->where('admin_ids','like','%|' . $value->admin_id . '|%')->where('company_id', \Auth::guard('admin')->user()->last_company_id)->where('company_id', \Auth::guard('admin')->user()->last_company_id)->whereDate('end_date', 'like', date('Y').'-'.date('m') .'-%')->get()->count()}},
                        @endforeach
                    ],
                    label: "Số công việc",

                    backgroundColor: [

                        @foreach ($list_user as $v)
                            @if(@\App\Models\RoleAdmin::where('admin_id',$v->id)->where('company_id',\Auth::guard('admin')->user()->last_company_id)->first()->status != 1)
                                '#333',
                            @else
                                '#e69a2a',
                            @endif
                        @endforeach
                    ],
                    borderColor: [
                        @foreach ($list_user as $key)
                            'rgba(255, 99, 132, 1)',
                        @endforeach

                    ],
                    borderWidth: 1
                }],

            },
            options: {
                // title: {
                //     display: true,
                //     text: 'Custom Chart Title'
                // },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        }
                    }]
                },
                legend: {
                    display: false,
                }
            }
        });

        var ctx = document.getElementById('myChart_year').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'horizontalBar',
            title: 'abc',
            data: {
                labels: [
                    @foreach($list_user as $val)
                        '{{@$val->admin->name}}',
                    @endforeach
                ],
                datasets: [{
                    label: "Số công việc",

                    data: [
                        @foreach ($list_user as $value)
                        {{\Modules\EworkingJob\Models\Task::whereIn('task_type',[1,2,3])->where('admin_ids','like','%|' . $value->admin_id . '|%')->where('company_id', \Auth::guard('admin')->user()->last_company_id)->whereDate('end_date', 'like', date('Y') .'-%')->get()->count()}},
                        @endforeach
                    ],
                    backgroundColor: [

                        @foreach ($list_user as $v)
                                @if(@\App\Models\RoleAdmin::where('admin_id',$v->id)->where('company_id',\Auth::guard('admin')->user()->last_company_id)->first()->status != 1)
                            '#333',
                        @else
                            '#00c0ef',
                        @endif
                        @endforeach
                    ],
                    borderColor: [
                        @foreach ($list_user as $key)
                            'rgba(255, 99, 132, 1)',
                        @endforeach

                    ],
                    borderWidth: 1
                }],

            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        }
                    }]
                },
                legend: {
                    display: false,
                }
            }
        });
    </script>
@endpush
