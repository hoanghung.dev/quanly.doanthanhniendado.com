@foreach($afterDeadlines as $afterDeadline)

    <?php
    $days = (strtotime(@$afterDeadline->end_date) - strtotime(date('Y-m-d'))) / (60 * 60 * 24);
    if ($days < 0) {
        $bg_deadline = '#fff';
    } elseif ($days == 0) {
        $bg_deadline = 'Tomato';
    } elseif ($days > 0 && $days <= 2) {
        $bg_deadline = 'LightSalmon';
    } elseif ($days > 2 && $days <= 7) {
        $bg_deadline = 'MistyRose';
    } elseif ($days > 0 && $days > 7) {
        $bg_deadline = 'SeaShell';
    }

    ?>
    <tr data-row="0" class="kt-datatable__row"
        style="left: 0px;background: {{$bg_deadline}}"
        style="border: 2px solid #ccc; background: {{@$bg_deadline}}">

        <td class="kt-datatable__cell">
                                                <span style="width: 200px;">
                                                    <span class="kt-font-bold">
                                                        <a href="{{route('project.edit',['id'=>@$afterDeadline->project->id])}}">{{@$afterDeadline->project->name}}</a></span>
                                                </span>
        </td>
        <td class="kt-datatable__cell">
                                                <span style="width: 252px;">
                                                    <span class="kt-font-bold">
                                                        <a href="{{ route('job.edit', ['id' => @$afterDeadline->id])}}">{{@$afterDeadline->name}}</a></span>
                                                </span>
        </td>
        <td class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <?php
                                                    $adminAfterDeadline = \Modules\EworkingJob\Http\Helpers\EworkingJobHelper::getAdminInJob($afterDeadline->id);
                                                    ?>
                                                    @include('admin.themes.metronic1.list.td.admins', ['admin_ids' => $adminAfterDeadline])
                                                </span>
        </td>
        <td class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">{{ date('d/m/Y', strtotime(@$afterDeadline->start_date)) }}</span>
                                                </span>
        </td>
        <td class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">{{ date('d/m/Y', strtotime(@$afterDeadline->end_date)) }}</span>
                                                </span>
        </td>
        <td class="kt-datatable__cell">
                                                <span style="width: 150px;">
                                                    <span class="kt-font-bold">{{ @$afterDeadline->Company->short_name }}</span>
                                                </span>
        </td>
        <td class="kt-datatable__cell">
                                                <span style="width: 50px;">
                                                    <a href="{{ route('job.edit',@$afterDeadline->id)}}"
                                                       class="btn btn-sm btn-label-brand btn-bold">
                                                        <span class="kt-font-bold">
                                                            Xem
                                                        </span>
                                                    </a>
                                                </span>
        </td>

    </tr>
@endforeach
@foreach($beforeDeadlines as $beforeDeadline)
    <?php
    $days = (strtotime(@$beforeDeadline->end_date) - strtotime(date('Y-m-d'))) / (60 * 60 * 24);
    if ($days < 0) {
        $bg_deadline = '#fff';
    } elseif ($days == 0) {
        $bg_deadline = 'Tomato';
    } elseif ($days > 0 && $days <= 2) {
        $bg_deadline = 'LightSalmon';
    } elseif ($days > 2 && $days <= 7) {
        $bg_deadline = 'MistyRose';
    } elseif ($days > 0 && $days > 7) {
        $bg_deadline = 'SeaShell';
    }
    ?>
    <tr data-row="0" class="kt-datatable__row"
        style="left: 0px;background: {{$bg_deadline}}"
        style="border: 2px solid #ccc; background: {{@$bg_deadline}}">
        <td class="kt-datatable__cell">
                                                <span style="width: 200px;">
                                                    <span class="kt-font-bold">
                                                        <a href="{{route('project.edit',['id'=>@$beforeDeadline->project->id])}}">{{@$beforeDeadline->project->name}}</a>
                                                    </span>
                                                </span>
        </td>
        <td class="kt-datatable__cell">
                                                <span style="width: 252px;">
                                                    <span class="kt-font-bold">
                                                         <a href="{{ route('job.edit', ['id' => @$beforeDeadline->id])}}">{{@$beforeDeadline->name}}</a>
                                                    </span>
                                                </span>
        </td>

        <td class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <?php
                                                    $adminBeforeDeadline = \Modules\EworkingJob\Http\Helpers\EworkingJobHelper::getAdminInJob($beforeDeadline->id);
                                                    ?>
                                                    @include('admin.themes.metronic1.list.td.admins', ['admin_ids' => $adminBeforeDeadline])
                                                </span>
        </td>
        <td class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">{{ date('d/m/Y', strtotime(@$beforeDeadline->start_date)) }}</span>
                                                </span>
        </td>
        <td class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">{{ date('d/m/Y', strtotime(@$beforeDeadline->end_date)) }}</span>
                                                </span>
        </td>
        <td class="kt-datatable__cell">
                                                <span style="width: 150px;">
                                                    <span class="kt-font-bold">{{ @$beforeDeadline->Company->short_name }}</span>
                                                </span>
        </td>
        <td class="kt-datatable__cell">
                                                <span style="width: 50px;">
                                                    <a class=" btn btn-sm btn-label-brand btn-bold view-detail"
                                                       href="{{ route('job.edit',@$beforeDeadline->id)}}">
                                                    <span class="kt-font-bold">Xem</span></a>
                                                </span>
        </td>

    </tr>
@endforeach

