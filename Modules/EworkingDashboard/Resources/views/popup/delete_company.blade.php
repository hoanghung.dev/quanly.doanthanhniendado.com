<div class="modal fade" id="delete-warning-company-modal" role="dialog" style="z-index:1060;">
    <div class="modal-dialog">
        <form class="modal-content" style="width:100%;height:100%" action="/admin/company/delete">
            {!! csrf_field() !!}
            <input type="hidden" name="id" value="">
            <div class="modal-header">
                <h4 class="modal-title">Xác nhận xóa</h4>
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="modal-body">
                <div class="form-group-div form-group">
                    <label >Xác nhận mật khẩu để xóa</label>
                    <div class="col-xs-12">
                        <input type="password" name="password" class="form-control" required>
                    </div>
                </div>
                <p>Bạn sắp xóa 1 công ty. Sau khi xóa sẽ không thể phục hồi lại dữ liệu này</p>
                <p>Bạn có muốn tiếp tục không?</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" id="delete-company-modal-yes" type="submit">Có, Xóa Ngay!</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Không, Quay lại</button>
            </div>
        </form>
    </div>
</div>
@push('scripts')
    <script type="text/javascript">
        $(document).on('click', '.delete-company', function () {
            $('#delete-warning-company-modal').modal('show');
            $('#delete-warning-company-modal input[name=id]').val($(this).data('company_id'));
        });
    </script>
@endpush