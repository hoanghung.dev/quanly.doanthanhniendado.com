<div class="modal fade" id="delete-warning-modal" role="dialog" style="z-index:1060;">
    <div class="modal-dialog">
        <div class="modal-content" style="width:100%;height:100%">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p>Bạn sắp thoát khỏi công ty. Sau khi thoát sẽ không thể phục hồi lại dữ liệu bản ghi này</p>
                <p>Bạn có muốn tiếp tục không?</p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger" id="delete-modal-yes" href="javascript:void(0)">Có, Thoát Ngay!</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Không, Quay lại</button>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script type="text/javascript">
        $(document).on('click', '.out-company', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $('#delete-modal-yes').attr('href', url);
            $('#delete-warning-modal').modal('show');
        });
    </script>
@endpush