<?php

namespace Modules\EworkingInternalNotification\Http\Controllers\Api\Admin;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Auth;
use Modules\EworkingAdmin\Models\Admin;
use Modules\EworkingInternalNotification\Models\InternalNotification;

class internalNotificationController extends Controller
{

    protected $module = [
        'code' => 'internal_notification',
        'table_name' => 'internal_notifications',
        'label' => 'Thông báo nội bộ',
        'modal' => 'Modules\EworkingInternalNotification\Models\InternalNotification',
    ];

    protected $filter = [
        'title' => [
            'query_type' => 'like'
        ],
        'company_id' => [
            'query_type' => '=',
        ],
    ];

    public function index(Request $request)
    {
        try {
            //  Check permission
            if (!CommonHelper::has_permission(\Auth::guard('api')->id(), 'internal_notification_view')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không đủ quyền'
                        ]
                    ],
                    'data' => null,
                    'code' => 403
                ]);
            }

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = InternalNotification::select('id', 'content', 'title')->whereRaw($where);
            $listItem = $listItem->where(function ($query) {
                $query->where('admin_ids', 'like', '|' . \Auth::guard('api')->id() . '|');
                $query->orWhereNull('admin_ids');
            })->where(function ($query) {
                $query->where('deadline', '>=', date('Y-m-d'));
                $query->orWhereNull('deadline');
            });

            //  Sort
            $listItem = $this->sort($request, $listItem);
            $listItem = $listItem->paginate(20)->appends($request->all());

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . $this->module['table_name'] .  '.id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
