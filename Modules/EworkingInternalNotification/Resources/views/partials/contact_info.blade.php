<!--begin::Portlet-->
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Danh sách liên hệ
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <div class="kt-form">
        <div class="kt-portlet__body">
            <div class="kt-section kt-section--first">
                <?php
                $contacts = json_decode(@$result->contact_info);
                ?>
                <?php
                $field = ['name' => 'contact_info', 'type' => 'dynamic', 'class' => 'form-action', 'label' => 'Danh sách liên hệ', 'cols' => ['Họ & tên', 'SĐT']];
                ?>
                <div class="form-group form-group-dynamic" id="form-group-{{ $field['name'] }}">
                    <div class="col-xs-12">
                        @include("eworkinguser::td.dynamic4", ['field' => $field, 'data' => $contacts])
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end::Form-->
</div>
<!--end::Portlet-->
<style>
    .fieldwrapper > div {
        display: inline-block;
    }
</style>
<script>
    $('.add-contact-info').click(function () {
        console.log('fd');
    });
</script>