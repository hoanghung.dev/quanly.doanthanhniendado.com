<?php

namespace Modules\EworkingInviteCompany\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use App\Models\{Admin, RoleAdmin, Roles};
use Auth;
use DB;
use Illuminate\Http\Request;
use Mail;
use Modules\EworkingCompany\Http\Controllers\Admin\CompanyController;
use Modules\EworkingCompany\Models\Company;
use Modules\EworkingInviteCompany\Models\InviteHistory;
use Modules\EworkingMail\Http\Controllers\EworkingMailController;
use Session;
use Validator;

class InviteCompanyController extends CURDBaseController
{
    protected $orderByRaw = 'status asc, id desc';

    protected $module = [
        'code' => 'invite_company',
        'label' => 'Lịch sử mời thành viên',
        'modal' => '\Modules\EworkingInviteCompany\Models\InviteHistory',
        'list' => [
            ['name' => 'email', 'type' => 'text', 'label' => 'Email'],
            ['name' => 'role_id', 'type' => 'relation', 'label' => 'Quyền', 'object' => 'role', 'display_field' => 'display_name', ''],
            ['name' => 'status', 'type' => 'custom', 'td' => 'eworkinginvitecompany::list.td.status', 'label' => 'Trạng thái'],
            ['name' => 'created_at', 'type' => 'custom', 'td' => 'eworkinginvitecompany::list.td.time', 'label' => 'Thời gian (Mời | Tham gia)'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'email', 'type' => 'text', 'class' => 'require', 'label' => 'Email của người muốn mời', 'group_class' => 'col-md-6'],
                ['name' => 'role_id', 'type' => 'custom', 'field' => 'eworkinginvitecompany::form.fields.select_role', 'class' => 'require', 'label' => 'Mời vào vị trí', 'model' => \App\Models\Roles::class, 'display_field' => 'display_name', 'group_class' => 'col-md-6'],
//                ['name' => 'intro', 'type' => 'textarea', 'label' => 'Lời mời']
            ],
        ],
    ];

    protected $filter = [
        'email' => [
            'label' => 'Email',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'options' => [
                '' => 'Trạng thái',
                -1 => 'Hủy',
                0 => 'Đang chờ',
                1 => 'Đã tham gia',
            ],
            'query_type' => 'like'
        ],
    ];

    public function getIndex(Request $request)
    {


        $data = $this->getDataList($request);

        return view('eworkinginvitecompany::list')->with($data);
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('eworkinginvitecompany::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'email' => 'required|email|max:255'
                ], [
                    'email.required' => 'Bắt buộc phải nhập Email',
                    'email.max' => 'Email tối đa 255 ký tự',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert

                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    $data['admin_id'] = \Auth::guard('admin')->user()->id;
                    $data['status'] = 0;

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }
                    if ($this->model->save()) {
                        //  Thông báo

//                        try {
                        DB::beginTransaction();
                        $company_invite = Company::where('id', $data['company_id'])->first();
                        $dataNotification = [
                            'name' => 'Thông báo!',
                            'content' => 'Công ty ' . $company_invite->short_name . ' mời bạn vào công ty với vị trí ' . Roles::find($request->role_id)->display_name,
                            'link' => \URL::to('admin/invite_history') . '?mail=' . $request->email,
                            'readed' => 0,
                            'type' => 4, //hành động đến công việc
                            //0 xem all, 1 xem theo nguoi
                            'to_admin_id' => @Admin::where('email', $request->email)->first()->id,
                            'company_id' => $data['company_id']
                        ];

                        \Eventy::action('notification.add', $dataNotification);
                        //  Sự kiện sau khi tạo lời mời
                        \Eventy::action('inviteCompany.add', [
                            'email' => $this->model->email,
                            'company_name' => $company_invite->short_name,
                            'link' => \URL::to('admin/invite_history') . '?mail=' . $this->model->email,
                        ]);
                        //  Vô hiệu các lời mời đang chờ trước đó
                        InviteHistory::where('email', $data['email'])->where('status', 0)->where('id', '!=', $this->model->id)->update(['status' => -1]);

                        $this->afterAddLog($request, $this->model);
                        DB::commit();
                        CommonHelper::one_time_message('success', 'Đã gửi lời mời đến ' . $request->email);
//                        } catch (\Exception $ex) {
//                            DB::rollBack();
//                            CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
//                        }

                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }
                    //  Kiểm tra số thành viên tối đa công ty được phép có
                    $count_member_of_company = RoleAdmin::where('company_id', \Auth::guard('admin')->user()->last_company_id)->where('status', 1)->count();
                    $company = Company::select(['account_max', 'short_name', 'id'])->where('id', \Auth::guard('admin')->user()->last_company_id)->first();
                    if ($count_member_of_company >= $company->account_max) {
                        return back()->with('alert', 'Cảnh báo: Công ty của bạn đã đạt tối đa số thành viên. Vui lòng nâng cấp gói để mời thêm thành viên!');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
                return back();
            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function getLocationCompany(Request $request)
    {

        $attr = Roles::select('display_name', 'id')->where('company_id', $request->id)->get();
        $html = '';
        foreach ($attr as $value) {
            $html .= '<option value="' . $value->id . '">' . $value->display_name . '</option>';

        }
        return response()->json([
            'html' => $html
        ]);

    }

    public function appendWhere($query, $request)
    {
        $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
        return $query;
    }

    public function ajaxAcceptJoinCompany(Request $request)
    {
        return $this->acceptJoinCompany([
            'email' => $request->email,
            'company_id' => $request->company_id,
            'role_id' => $request->role_id
        ]);
    }

    /*
     * Chấp nhận tham gia công ty
     * */
    public function acceptJoinCompany($data)
    {

        $admin = Admin::where('email', $data['email'])->first();

        //  Kiem tra co ton tai loi moi nay khong
        $history_invite = InviteHistory::where('email', $data['email'])->where('company_id', $data['company_id'])
            ->where('role_id', $data['role_id'])->where('status', 0)->first();
        if (!is_object($history_invite)) {
            return response()->json([
                'status' => false,
                'msg' => 'Không có lời mời này!'
            ]);
        }

        //  Kiểm tra số thành viên tối đa công ty được phép có
        $count_member_of_company = RoleAdmin::where('company_id', $data['company_id'])->where('status', 1)->count();
        $company = Company::select(['account_max', 'short_name', 'id'])->where('id', $data['company_id'])->first();

//        if ($count_member_of_company >= 2) {
        if ($count_member_of_company >= $company->account_max) {
            return [
                'status' => false,
                'msg' => 'Công ty bạn muốn tham gia hiện tại đang đạt tối đa số thành viên được phép của gói dịch vụ!'
            ];
        }

        //  Update company_ids cho bang admin
        $companyController = new CompanyController();
        $companyController->updateFieldCompanyIds($admin, $company->id);

        $admin->last_company_id = $data['company_id'];
        $admin->save();

        //  Update role_admin - quyen cho thanh vien
        RoleAdmin::updateOrCreate([
            'admin_id' => $admin->id,
            'company_id' => $data['company_id']
        ], [
            'role_id' => $data['role_id'],
            'status' => 1
        ]);

        //  Update trạng thái lời mời => đã chấp nhận  InviteHistory
        $history_invite->status = 1;
        $history_invite->save();

        return [
            'status' => true,
            'published' => true,
            'msg' => 'Bạn vừa chấp nhận lời mời từ công ty ' . $company->short_name
        ];
    }
}
