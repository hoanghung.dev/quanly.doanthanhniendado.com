<?php

namespace Modules\EworkingInviteCompany\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Http\Helpers\CommonHelper;
use App\Models\Roles;
use Auth;
use Eventy;
use Illuminate\Http\Request;
use Mail;
use Modules\A4iAdmin\Models\Admin;
use Modules\EworkingSetting\Http\Controllers\EworkingMailController;
use Session;
use Validator;

//use Modules\EworkingMail\Http\Controllers\EworkingMailController;

class InviteHistoryController extends CURDBaseController
{
    protected $_base;

    protected $whereRaw = 'status = 0';

    public function __construct()
    {
        parent::__construct();
        $this->_base = new BaseController();
    }

    protected $module = [
        'code' => 'invite_history',
        'label' => 'Lời mời đang chờ duyệt',
        'modal' => '\Modules\EworkingInviteCompany\Models\InviteHistory',
        'list' => [
            ['name' => 'company_id', 'type' => 'relation', 'label' => 'Công ty', 'object' => 'company', 'display_field' => 'name'],
            ['name' => 'role_id', 'type' => 'relation', 'label' => 'Vị trí', 'object' => 'role', 'display_field' => 'display_name', ''],
//            ['name' => 'intro', 'type' => 'text', 'label' => 'Lời mời'],
            ['name' => 'created_at', 'type' => 'custom', 'td' => 'eworkinginvitecompany::list.td.time', 'label' => 'Thời gian'],
            ['name' => 'status', 'type' => 'custom', 'td' => 'eworkinginvitecompany::list.td.invite_history_status', 'label' => 'Trạng thái'],
        ],
    ];
    protected $filter = [
        'company_id' => [
            'label' => 'Tên công ty',
            'type' => 'text',
            'display_field' => 'short_name',
            'model' => \Modules\EworkingCompany\Models\Company::class,
            'query_type' => 'like'
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'options' => [
                '' => 'Trạng thái',
                -1 => 'Hủy',
                0 => 'Đang chờ',
                1 => 'Đã tham gia',
            ],
            'query_type' => 'like'
        ],
    ];

    public function getIndex(Request $request)
    {
        if ($request->has('mail') && $request->mail != \Auth::guard('admin')->user()->email) {
            \Auth::guard('admin')->logout();
            return redirect('admin/login?mail=' . $request->mail);
        }
        $data = $this->getDataList($request);

        return view('eworkinginvitecompany::invite_company.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        $query = $query->where('email', \Auth::guard('admin')->user()->email);
        return $query;
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
                return back();
            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function getPublish(Request $request)
    {
        try {
            $id = $request->get('id', 0);
            $item = $this->model->find($id);
            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if ($item->email != \Auth::guard('admin')->user()->email) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền chấp nhận!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0) {
                $inviteCompanyController = new InviteCompanyController();
                $invite = $inviteCompanyController->acceptJoinCompany([
                    'email' => $item->email,
                    'company_id' => $item->company_id,
                    'role_id' => $item->role_id
                ]);
                $findGiamDoc = Roles::where('name', 'super_admin_company_' . $item->company_id);
//                Tìm thấy giám đốc thì gửi cho giám đốc
                if ($findGiamDoc->exists()) {
                    $GiamDoc = $findGiamDoc->first()->admin;
                } else {
//                    không thấy thì gửi cho người mời
                    $GiamDoc = Admin::find($item->admin_id);
                }

                if (!$invite['status']) {
                    $dataNotification = [
                        'name' => 'Đủ nhân viên',
                        'content' => 'Thành viên ' . $item->email . ' đã chấp nhận nhưng full slot',
                        'link' => route('invite_company'),
                        'readed' => 0,
                        'type' => 9,
                        'from_admin_id' => Auth::guard('admin')->user()->id,
                        'to_admin_id' => (int)$GiamDoc->id,
                        'job_id' => '',
                        'company_id' => $item->company_id,
                    ];
                    \Eventy::action('service_history.full_Slot', [
                        'name' => $GiamDoc->name,
                        'email' => $GiamDoc->email,
                    ]);

                    \Eventy::action('notification.add', $dataNotification);
                    return response()->json($invite);
                } else {
                    return response()->json($invite);
                }
            }
//            return response()->json([
//                'status' => true,
//                'published' => true,
//                'msg' => 'ádasdads.'
//            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }
}
