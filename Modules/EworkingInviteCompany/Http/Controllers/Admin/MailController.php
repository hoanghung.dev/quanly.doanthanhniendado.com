<?php

namespace Modules\EworkingInviteCompany\Http\Controllers\Admin;

use App\Mail\MailServer;
use App\Models\Setting;
use Auth;
use Illuminate\Routing\Controller;
use Mail;
use URL;

class MailController extends Controller
{
    protected $_mailSetting;

    public function __construct()
    {
        $this->_mailSetting = Setting::whereIn('type', ['mail'])->pluck('value', 'name')->toArray();
    }

    /**
     * Gửi mail khi nhân viên được mời chấp nhận, mà cty đã đủ người
     * $info: thông tin nhân viên
     */
    public function SendMailFullSlot($info)
    {

        $user = (object)[
            'email' => $info['email'],
            'name' => $info['name'],
        ];
        $data = [
            'view' => 'test',
            'user' => $user,

            'name' => $this->_mailSetting['mail_name'],
            'subject' => 'Thư phản hồi của ' . $info['name']
        ];
        \Mail::to($user)->send(new MailServer($data));
        return true;
    }
}
