<?php

namespace Modules\EworkingInviteCompany\Models;

use App\Models\Roles;
use Illuminate\Database\Eloquent\Model;
use Modules\EworkingAdmin\Models\Admin;
use Modules\EworkingCompany\Models\Company;

class InviteHistory extends Model
{
    protected $table = 'invite_history';

    protected $fillable = [
        'id', 'company_id', 'role_id', 'email', 'status', 'admin_id', 'intro'
    ];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function role()
    {
        return $this->belongsTo(Roles::class, 'role_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

    public function role_this_company()
    {
        return $this->belongsTo(Roles::class, 'role_id');
    }
}
