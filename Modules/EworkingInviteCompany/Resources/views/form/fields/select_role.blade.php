<?php
$roles = \App\Models\Roles::where('company_id', \Auth::guard('admin')->user()->last_company_id)->pluck('display_name', 'id');
$data[] = old($field['name']) != null ? old($field['name']) : @$field['value'];
?>
<select class="form-control {{ $field['class'] or '' }}" id="{{ $field['name'] }}" {!! @$field['inner'] !!}
{{ strpos($field['class'], 'require') !== false ? 'required' : '' }}
name="{{ $field['name'] }}@if(isset($field['multiple'])){{ '[]' }}@endif"
        @if(isset($field['multiple'])) multiple @endif>
    @foreach ($roles as $id => $name)
        <option value='{{ $id }}' {{ in_array($id, $data) ? 'selected' : '' }}>{{ $name }}</option>
    @endforeach
</select>