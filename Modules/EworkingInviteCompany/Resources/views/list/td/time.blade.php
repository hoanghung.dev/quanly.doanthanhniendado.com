@if($item->status == 1)
    {{ date('H:i d/m/Y', strtotime($item->updated_at)) }}
@elseif($item->status == 0)
    {{ date('H:i d/m/Y', strtotime($item->created_at)) }}
@else
    {{ date('H:i d/m/Y', strtotime($item->updated_at)) }}
@endif