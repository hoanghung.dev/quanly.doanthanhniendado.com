<?php

namespace Modules\EworkingJob\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\EworkingJob\Models\Subject;
use Modules\EworkingJob\Models\Task;
use Session;
use Validator;

class SubjectController extends CURDBaseController
{
    protected $_base;

    public function __construct()
    {
        parent::__construct();
        $this->_base = new BaseController();
    }

    protected $module = [
        'code' => 'subject',
        'table_name' => 'subjects',
        'label' => 'Bộ môn',
        'modal' => '\Modules\EworkingJob\Models\Subject',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên'],
            ['name' => 'intro', 'type' => 'text', 'label' => 'Mô tả'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên bộ môn'],
                ['name' => 'intro', 'type' => 'textarea', 'label' => 'Mô tả'],
            ],
            'general_tab2' => [
                'label' => 'Nhiệm vụ trong bộ môn',
                'class' => 'col-xs-12 col-md-8 padding-left',
                'td' => [
                    ['name' => 'iframe', 'type' => 'iframe', 'label' => 'Các nhiệm vụ trong bộ môn', 'src' => '/admin/task?subject_id={id}', 'inner' => 'style="min-height: 785px;"'],
                ],
            ],
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'intro' => [
            'label' => 'Mô tả',
            'type' => 'text',
            'query_type' => 'like'
        ],
    ];

    public function appendWhere($query, $request)
    {
        $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
        $query = $query->where('name', '!=', '')->where('type', 0);
        return $query;
    }

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('eworkingjob::subject.list')->with($data);
    }

    public function getAllFormFiled()
    {
        $fields = [];
        $type = ['text', 'textarea'];

        foreach ($this->module['form'] as $tab) {
            foreach ($tab as $field) {
                if (in_array(@$field['type'], $type)) {
                    $fields[] = $field;
                }
            }
        }
//        dd($td);
        return $fields;
    }

    public function searchForSelect2(Request $request)
    {


        $data = $this->model->select([$request->col, 'id'])->where($request->col, 'like', '%' . $request->keyword . '%');

        if (@$request->company_id != null) {
            $data = $data->where('company_id', $request->company_id);
        }
//        $data = $data->limit(5)->get();
        $data = $data->where('type', 0)->limit(5)->get();
        return response()->json([
            'status' => true,
            'items' => $data
        ]);
    }

    public function update(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
                return back();
            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('eworkingjob::subject.edit')->with($data);
            } else if ($_POST) {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                //  Tùy chỉnh dữ liệu insert
                unset($data['iframe']);
                #
                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }


    public function getPublish(Request $request)
    {
        try {

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xuất bản!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
                return back();
            }

            $item->delete();
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return back();

        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function add(Request $request)
    {
        $item = new Subject();
        $item->company_id = \Auth::guard('admin')->user()->last_company_id;
        $item->admin_id = \Auth::guard('admin')->user()->id;
        $item->save();
        return redirect('/admin/subject/' . $item->id);
    }

    public function insertSubject(Request $request)
    {
//        dd($request->all());
        $job_id = $request->job_id;
        $subject = Subject::find($request->id);
        $arr_task = null;
        $task_ids = explode('|', $request->task_ids);
//        dd($task_ids);

        foreach ($subject->tasks as $task) {

            if (in_array($task->id, $task_ids)) {
                $arr_task[] = (object)[
                    'id' => $task->id,
                    'name' => $task->name,
                    'start_date' => null,
                    'end_date' => null,
                    'admin_ids' => '',
                    'status' => 1,
                ];
            }
        }

        $data = [
            'id' => $request->id,
            'name' => $subject->name,
            'data' => $arr_task
        ];

        $dir_name = config('eworkingjob.job_data_draf') . $job_id;
        $filename = $dir_name . '/draft.txt';

        $message_data = [];
        if (file_exists($filename)) { //kiem tra xem file ton tai thi lay ra content
            $message_data = (array)json_decode(file_get_contents($filename));
        }
        $exists_subject = 0;
        foreach ($message_data as $data_draft) {
            if (isset($data_draft->add_subject)) { //neu co obj general roi thi luu thay doi
                $exists_subject = 1;

                $data_draft->add_subject = array_merge((array)$data_draft->add_subject, [(object)$data]);
            }
        }
        $draft['add_subject'][] = $data;

        if ($exists_subject == 0) {
//            them moi data
            $this->_base->logsFileText($dir_name, $filename, $draft);
        } else {
////            luu thay doi
            file_put_contents($filename, json_encode($message_data));
        }
        $task_draft = [];
        return view('eworkingjob::.subject.collapse_subject', compact('subject', 'task_draft', 'job_id'));
    }

//    public function insertSubject(Request $request)
//    {
//        dd($request->all());
//        $task_ids = explode('|', $request->task_ids);
//        $subject = $this->cloneSubject(
//            $request->id,
//            $data = [
//                'admin_id' => Auth::guard('admin')->user()->id,
//                'company_id' => Auth::guard('admin')->user()->last_company_id,
//                'job_id' => $request->job_id
//            ],
//            $task_ids
//        );
//        return view('eworkingjob::.subject.collapse_subject', compact('subject'));
//    }

    /*
     * Nhân bản 1 bộ môn
     * $subject_id , $subject : thông  tin  bộ môn muốn nhân bản
     * $data : tùy chỉnh cột bộ môn mới nhân bản
     * $task_ids : các task sẽ chèn vào subject mới
     * */
    public function cloneSubject($subject_id, $data = [], $task_ids = false, $subject = false)
    {
        if (!$subject) {
            $subject = Subject::find($subject_id);
        }
        $subject_new = $subject->replicate();
        $subject_new->clone_of = $subject->id;
        foreach ($data as $k => $v) {
            $subject_new->{$k} = $v;
        }
        $subject_new->save();
        $tasks = Task::where('subject_id', $subject->id)->get();

        foreach ($tasks as $task) {
            if (!$task_ids || (is_object($tasks) && in_array($task->id, $task_ids))) {
                $task_new = $task->replicate();
                $task_new->job_id = $subject_new->job_id;
                $task_new->subject_id = $subject_new->id;
                $task_new->clone_of = $task->id;
                $task_new->admin_id = Auth::guard('admin')->user()->id;
                $task_new->company_id = Auth::guard('admin')->user()->last_company_id;
                $task_new->admin_ids = null;
                $task_new->end_date = null;
                $task_new->start_date = null;
                $task_new->save();
            }
        }
        return $subject_new;
    }

    public function get_html_popup_add_subject(Request $request)
    {
        $tasks = Task::where('subject_id', $request->subject_id)->get();
        $html = '<div class="kt-checkbox-list">';
        if (count($tasks) > 0) {
            foreach ($tasks as $item) {
                $html .= '<label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
								<input name="tasks" class="tasks" checked value="' . $item->id . '" type="checkbox">' . $item->name . '
                                <span></span>
							</label>';

            }
        } else {
            $html .= '<label class="text-danger"> - Không tồn tại nhiệm vụ</label>';
        }
        $html .= '</div>';
        return $html;
    }

    public function getByJobID($job_id)
    {
        $dir_name = config('eworkingjob.job_data_draf') . $job_id;
        $filename = $dir_name . '/draft.txt';
        $message_data = [];
        if (file_exists($filename)) { //kiem tra xem file ton tai thi lay ra content
            $message_data = (array)json_decode(file_get_contents($filename));
        }

        $data['status_draft'] = false;
        $status_draft_in_subject = false;
        $status_delete_subject = false;
        foreach ($message_data as $data_draft) {
            if (isset($data_draft->main_adit_subject)) { //neu co obj general roi thi luu thay doi
                foreach ($data_draft->main_adit_subject as $key => $task) {
                    $data_draft->main_adit_subject_new[$task->id] = $task;
                    $data['status_draft'] = true;
                }
                $main_adit_subject_new = $data_draft->main_adit_subject_new;
            }
            if (isset($data_draft->add_task_in_subject)) {
                foreach ($data_draft->add_task_in_subject as $key => $task) {
                    $status_draft_in_subject = true;
                    $data_draft_in_subject[$task->subject_id][] = $task;
                }
            }

            if (isset($data_draft->main_delete_subject)) { //neu co đối tượng roi thi update
                $status_delete_subject = true;
                $delete_subject = $data_draft->main_delete_subject;
            }
        }
        $data['delete_subject'] = $status_delete_subject ? $delete_subject : null;
        $data['task_draft_in_subject'] = $status_draft_in_subject ? $data_draft_in_subject : null;
        $data['task_draft'] = $data['status_draft'] ? $main_adit_subject_new : null;
        $data['subjects'] = \Modules\EworkingJob\Models\Subject::where('job_id', $job_id)->paginate(5);
        $data['job_id'] = $job_id;
        return view('eworkingjob::form.subjects_content')->with($data);
    }

    public function CreateDefaultSubjectForCompany($company)
    {
        $subjects_default = Subject::whereNull('company_id')->get();

        foreach ($subjects_default as $subject) {
            $this->cloneSubject(false, $data = ['company_id' => $company->id], false, $subject);
        }
        return true;
    }
}
