<?php

namespace Modules\EworkingJob\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\EworkingJob\Models\Subject;
use Modules\EworkingJob\Models\Task;
use Validator;

class SubjectWarehouseController extends CURDBaseController
{

    protected $module = [
        'code' => 'subject_warehouse',
        'table_name' => 'subjects',
        'label' => 'Kho bộ môn',
        'modal' => '\Modules\EworkingJob\Models\Subject',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên'],
            ['name' => 'intro', 'type' => 'text', 'label' => 'Mô tả'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên bộ môn'],
                ['name' => 'intro', 'type' => 'textarea', 'label' => 'Mô tả'],
            ],
            'general_tab2' => [
                'label' => 'Nhiệm vụ trong bộ môn',
                'class' => 'col-xs-12 col-md-8 padding-left',
                'td' => [
                    ['name' => 'iframe', 'type' => 'iframe', 'label' => 'Các nhiệm vụ trong bộ môn', 'src' => '/admin/task?subject_id={id}', 'inner' => 'style="min-height: 785px;"'],
                ],
            ],
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'intro' => [
            'label' => 'Mô tả',
            'type' => 'text',
            'query_type' => 'like'
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('eworkingjob::subject.warehouse.list')->with($data);
    }

    public function appendWhere($listItem, $request)
    {
        $listItem = $listItem->whereNull('company_id');
        return $listItem;
    }

    public function getAllFormFiled()
    {
        $fields = [];
        $type = ['text', 'textarea'];

        foreach ($this->module['form'] as $tab) {
            foreach ($tab as $field) {
                if (in_array(@$field['type'], $type)) {
                    $fields[] = $field;
                }
            }
        }
//        dd($td);
        return $fields;
    }

    public function update(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('eworkingjob::subject.edit')->with($data);
            } else if ($_POST) {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                //  Tùy chỉnh dữ liệu insert
                unset($data['iframe']);
                #
                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }


    public function getPublish(Request $request)
    {
        try {

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            $item->delete();
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return back();

        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function add(Request $request)
    {
        $item = new Subject();
        $item->admin_id = \Auth::guard('admin')->user()->id;
        $item->save();
        return redirect('/admin/subject_warehouse/' . $item->id);
    }

    public function insertSubject(Request $request)
    {
        $task_ids = explode('|', $request->task_ids);
        $subject = $this->cloneSubject(
            $request->id,
            $data = [
                'admin_id' => Auth::guard('admin')->user()->id,
                'company_id' => Auth::guard('admin')->user()->last_company_id,
                'job_id' => $request->job_id
            ],
            $task_ids
        );
        return view('eworkingjob::.subject.warehouse.collapse_subject', compact('subject'));
    }

    /*
     * Nhân bản 1 bộ môn
     * $subject_id , $subject : thông  tin  bộ môn muốn nhân bản
     * $data : tùy chỉnh cột bộ môn mới nhân bản
     * $task_ids : các task sẽ chèn vào subject mới
     * */
    public function cloneSubject($subject_id, $data = [], $task_ids = false, $subject = false)
    {
        if (!$subject) {
            $subject = Subject::find($subject_id);
        }
        $subject_new = $subject->replicate();
        $subject_new->clone_of = $subject->id;
        foreach ($data as $k => $v) {
            $subject_new->{$k} = $v;
        }
        $subject_new->save();
        $tasks = Task::where('subject_id', $subject->id)->get();

        foreach ($tasks as $task) {
            if (!$task_ids || (is_object($tasks) && in_array($task->id, $task_ids))) {
                $task_new = $task->replicate();
                $task_new->job_id = $subject_new->job_id;
                $task_new->subject_id = $subject_new->id;
                $task_new->clone_of = $task->id;
                $task_new->admin_id = Auth::guard('admin')->user()->id;
                $task_new->company_id = Auth::guard('admin')->user()->last_company_id;
                $task_new->admin_ids = null;
                $task_new->end_date = null;
                $task_new->start_date = null;
                $task_new->save();
            }
        }
        return $subject_new;
    }

    public function get_html_popup_add_subject(Request $request)
    {
        $tasks = Task::where('subject_id', $request->subject_id)->get();
        $html = '';
        foreach ($tasks as $item) {
            $html .= '<li><label style="cursor: pointer;"><input name="tasks" class="tasks" type="checkbox" checked value="' . $item->id . '">' . $item->name . '</label></li>';
        }
        return $html;
    }

    public function getByJobID($job_id)
    {
        $data['subjects'] = \Modules\EworkingJob\Models\Subject::where('job_id', $job_id)->paginate(5);
        return view('eworkingjob::form.subjects_content')->with($data);
    }

    public function CreateDefaultSubjectForCompany($company)
    {
        $subjects_default = Subject::whereNull('company_id')->get();

        foreach ($subjects_default as $subject) {
            $this->cloneSubject(false, $data = ['company_id' => $company->id], false, $subject);
        }
        return true;
    }
}
