<?php

namespace Modules\EworkingJob\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\EworkingJob\Models\Subject;
use Modules\EworkingJob\Models\Task;
use Validator;

class TaskController extends CURDBaseController
{

    protected $_base;

    public function __construct()
    {
        parent::__construct();
        $this->_base = new BaseController();
    }

//    protected $whereRaw = "subject_id is null AND project_id is null";

    protected $module = [
        'code' => 'task',
        'table_name' => 'tasks',
        'label' => 'Nhiệm vụ',
        'modal' => '\Modules\EworkingJob\Models\Task',
        'list' => [
            ['name' => 'name', 'type' => 'custom', 'td' => 'eworkingjob::job.td.text_task', 'label' => 'Tiêu đề'],
//            ['name' => 'content', 'type' => 'text', 'label' => 'Mô tả'],
            ['name' => 'admin_ids', 'type' => 'select2', 'label' => 'Người làm', 'model' => \App\Models\Admin::class, 'display_field' => 'name'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tiêu đề'],
                ['name' => 'admin_ids', 'type' => 'custom', 'field' => 'eworkingjob::form.select2_model', 'label' => 'Người làm', 'model' => \App\Models\Admin::class, 'display_field' => 'name', 'multiple' => true],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'Kích hoạt', 'value' => 1],
            ],
        ]
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tiêu đề',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'content' => [
            'label' => 'Mô tả',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'subject_id' => [
            'label' => '',
            'type' => 'text',
            'query_type' => '=',
            'class' => 'hidden',
        ],
        'job_id' => [
            'label' => '',
            'type' => 'text',
            'query_type' => '=',
            'class' => 'hidden',
        ],
    ];

    protected $statusTask = [
        "1" => 'Chờ duyệt',
        "2" => 'Đang làm',
        "3" => 'Làm lại',
        "4" => 'Chưa làm',
        "5" => 'Hoàn thành',
        "6" => 'Hủy'
    ];

    public function getTaskInSubject(Request $request)
    {
        $subject = Subject::findOrFail($request->id);
        $data['tasks'] = $subject->tasks;
        return view('eworkingjob::subject.list_ajax')->with($data);
    }

    public function getTask(Request $request)
    {
        $task = Task::findOrfail($request->id);
        $value = trim($task->admin_ids, '|');
        $value = trim($value, '|');
        $admin_ids = explode('|', $value);
        $query = \App\Models\Admin::whereIn('id', $admin_ids)->pluck('id');
        $start_date = date('d-m-Y', strtotime($task->start_date));
        $end_date = date('d-m-Y', strtotime($task->end_date));
        return response()->json([
            'id' => $task->id,
            'name' => $task->name,
            'content' => $task->content,
            'status' => $task->status,
            'start_date' => ($task->start_date == null) ? date('d-m-Y') : $start_date,
            'end_date' => ($task->end_date == null) ? date('d-m-Y') : $end_date,
            'admin_ids' => '|' . implode('|', $query->toArray()) . '|',
            'subject_id' => $task->subject_id,
        ]);
    }

//    public function afterDelete($request, $item)
//    {
//        $dataHistory = [
//            'from_admin_id' => Auth::guard('admin')->user()->id,
//            'company_id' => Auth::guard('admin')->user()->last_company_id,
//            'content' => 'Nhiệm vụ ' . $item->name . ' được loại bỏ khỏi công việc',
//            'type' => 2,
//            'link' => '/admin/job/' . $item->job_id,
//            'job_id' => $item->job_id,
//        ];
//        $this->_base->createHistory($dataHistory);
//    }

    public function addReturn($request)
    {

        if ($request->has('subject_id')) {
            return redirect('admin/' . $this->module['code'] . '?subject_id=' . $request->subject_id);
        } else {
            return redirect('admin/' . $this->module['code']);
        }
    }

    public function updateReturn($request, $item = false)
    {
        if ($request->has('subject_id')) {
            return redirect('admin/' . $this->module['code'] . '?subject_id=' . $request->subject_id);
        } else {
            return redirect('admin/' . $this->module['code']);
        }
    }

    public function deleteReturn($request)
    {
        if ($request->has('job')) {
            return back();
        }
        if ($request->has('subject_id')) {
            return redirect('admin/' . $this->module['code'] . '?subject_id=' . $request->subject_id);
        } else {
            return redirect('admin/' . $this->module['code']);
        }
    }

    public function postEditTask(Request $request)
    {
        $data = $request->all();

        $data['start_date'] = date('Y/m/d', strtotime($request->start_date));
        $data['delete_status'] = false;
        $data['admin_id'] = Auth::guard('admin')->user()->id;
        $data['end_date'] = date('Y/m/d', strtotime($request->end_date));
        if ($request->has('task_id')) {
            $data['id'] = $request->task_id != null ? $request->task_id : time();
        } elseif ($request->has('id')) {
            $data['id'] = $request->id != null ? $request->id : time();
        } else {
            $data['id'] = time();
        }
        $data['status'] = $request->has('status') && $request->status != null ? $request->status : 1;
//        dd($data);
        if (is_array($request->admin_ids) && !is_null($request->admin_ids)) {
            $data['admin_ids'] = '|' . implode('|', $request->admin_ids) . '|';
        } elseif (is_string($request->admin_ids) && !is_null($request->admin_ids)) {
            $data['admin_ids'] = $request->admin_ids;
        } else {
            $data['admin_ids'] = '';
        }
        $dir_name = config('eworkingjob.job_data_draf') . $request->job_id;
        $filename = $dir_name . '/draft.txt';
        $message_data = [];
        if (file_exists($filename)) { //kiem tra xem file ton tai thi lay ra content
            $message_data = (array)json_decode(file_get_contents($filename));
        }
        $exists_tasks = 0;
        unset($data['type']);
        $data_save[] = (object)$data;
//        dd($data);
//      Thêm NHÁP : Thêm 1 task nháp không thuộc bộ môn (CHƯA có trong dữ liệu)
        if ($data['type_task'] == 'single-add-task-draft') {
            foreach ($message_data as $data_draft) {
//                Tìm ra đối tượng chứa task không thuộc bộ môn
                if (isset($data_draft->add_tasks)) { //neu co đối tượng roi thi update
                    $exists_tasks = 1;
                    $data_draft->add_tasks = array_merge((array)$data_draft->add_tasks, $data_save);
                }
            }
//            Chưa có thì thêm 1 đối tượng mới
            $draft['add_tasks'][] = $data;
        }
        if ($data['type_task'] == 'main-delete-subject') {
            $subject = Subject::where('id', $data['subject_id'])->first();
            if (is_object(@$subject)) {
                foreach ($message_data as $data_draft) {
//                Tìm ra đối tượng chứa task không thuộc bộ môn

                    if (isset($data_draft->main_delete_subject)) { //neu co đối tượng roi thi update
                        $exists_tasks = 1;
                        $data_draft->main_delete_subject = array_merge((array)$data_draft->main_delete_subject, [$subject->id]);
                    }
                }

//            Chưa có thì thêm 1 đối tượng mới
                $draft['main_delete_subject'][] = $subject->id;
            }
        }
//sadasd
//        dd($data);
//        Thêm NHÁP : Sửa task, xóa task, hoàn thành task không thuộc bộ môn (CHƯA có trong dữ liệu)
        if ($data['type_task'] == 'single-edit-task-draft' || $data['type_task'] == 'single-delete-task-draft' || $data['type_task'] == 'single-finish-task-draft') {

            $data['subject_id'] = '';
            unset($data['type']);
            foreach ($message_data as $data_draft) {
                //                Tìm ra đối tượng chứa task không thuộc bộ môn
                if (isset($data_draft->add_tasks)) {
                    $exists_tasks = 1;
                    foreach ($data_draft->add_tasks as $key => $task) {
//                Kiểm tra xem task đã tồn tại hay chưa
                        if ($task->id == (int)$data['task_id']) {
                            if ($data['type_task'] == "single-delete-task-draft") {
//                                Tồn tại thì xóa
                                unset($data_draft->add_tasks[$key]);
                                $data_draft->add_tasks = array_values($data_draft->add_tasks);
                            } else if ($data['type_task'] == "single-finish-task-draft") {
//                                 Tồn tại thì hoàn thành
                                $data_draft->add_tasks[$key]->status = $data_draft->add_tasks[$key]->status == 1 ? 0 : 1;
                            } else {
//                                Tồn tại thì cập nhật mới
                                $data_draft->add_tasks[$key] = (object)$data;
                            }
                        }
                    }
                }
            }
        }
////        Thêm NHÁP : Sửa task, xóa task, hoàn thành task trong 1 bộ môn (CHƯA có trong dữ liệu)
        if ($data['type_task'] == 'edit-task-subject' || $data['type_task'] == 'delete-task-subject' || $data['type_task'] == 'finish-task-subject' || $data['type_task'] == 'add-task-subject' || $data['type_task'] == 'draft-delete-subject') {
            foreach ($message_data as $data_draft) {
                if (isset($data_draft->add_subject)) { //Tìm đối tượng chứa task
                    $exists_tasks = 1;
                    foreach ($data_draft->add_subject as $key => $subject) {
//                        Kiểm tra xem bộ môn có tồn tại ko
                        if ($subject->id == $data['subject_id']) {
                            if ($data['type_task'] == 'draft-delete-subject') {
                                unset($data_draft->add_subject[$key]);
                                $data_draft->add_subject = array_values($data_draft->add_subject);
                            } else if ($data['type_task'] == "add-task-subject") {
                                //  Thêm task mới cho bộ môn
                                $exists_tasks = 2;
                                $data_draft->add_subject[$key]->data = array_merge((array)$data_draft->add_subject[$key]->data, $data_save);
                            } else {
                                $exists_tasks = 3;
                                foreach ($subject->data as $key_task => $task) {
                                    //  Task tồn tại trong bộ môn
                                    if ($task->id == $data['task_id']) {
                                        if ($data['type_task'] == "delete-task-subject") {
                                            //  Xóa task tồn tại trong bộ môn
                                            unset($subject->data[$key_task]);
                                            $subject->data = array_values($subject->data);
                                        } else if ($data['type_task'] == "finish-task-subject") {
                                            //  Hoàn thành task tồn tại trong bộ môn
                                            $subject->data[$key_task]->status = $subject->data[$key_task]->status == 1 ? 0 : 1;
                                        } else {
                                            //  cập nhật task tồn tại trong bộ môn
                                            $subject->data[$key_task] = (object)$data;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
//            Chưa tồn tại đối tượng bộ môn thì thêm mới
            if ($data['type_task'] == "add-task-subject") {
                $draft['add_subject'][] = $data;
            }
        }

//      SỬA NHÁP : Sửa task, hoàn thành task trong bộ môn (có trong dữ liệu)
        if ($data['type_task'] == 'main-edit-task-subject' || $data['type_task'] == 'finish-main-edit-task-subject' || $data['type_task'] == 'main-delete-task-subject') {
            if ($data['type_task'] == 'main-delete-task-subject') {
                $data['delete_status'] = true;
            }
            foreach ($message_data as $data_draft) {

                if (isset($data_draft->main_adit_subject)) {

                    //Kiểm tra sự tồn tại của đối tượng chứa
                    $exists_tasks = 1;
                    foreach ($data_draft->main_adit_subject as $key => $task_in_subject) {

                        $data_draft->main_adit_subject[$key]->id = $data_draft->main_adit_subject[$key]->task_id;
                        if ($task_in_subject->id == $data['id']) {
                            $exists_tasks = 2;
                            //Tồn tại task thì cập nhật
                            $data_draft->main_adit_subject[$key] = (object)$data;
                        }
                    }
                    if ($exists_tasks == 1) {
                        //Ko tồn tại task thì thêm mới
                        $data_draft->main_adit_subject = array_merge((array)$data_draft->main_adit_subject, [(object)$data]);
                    }

                }
            }

            //Chưa tồn tại thì khởi tạo đối tượng
            $draft['main_adit_subject'][] = $data;
        }

        //      SỬA NHÁP : Thêm task, sửa task thuộc 1 bộ môn đã có trong cơ sở dữ liệu

        if ($data['type_task'] == 'main-add-task-subject-draft' || $data['type_task'] == 'main-finish-task-subject-draft' || $data['type_task'] == 'main-edit-task-subject-draft' || $data['type_task'] == 'main-delete-task-subject-draft') {

            foreach ($message_data as $data_draft) {

                if (isset($data_draft->add_task_in_subject)) { //neu co obj roi thi update
                    $exists_tasks = 1;

                    foreach ($data_draft->add_task_in_subject as $key => $task_in_subject) {
                        if ($task_in_subject->id == (int)$data['id']) {
                            $exists_tasks = 2;
                            if ($data['type_task'] == "main-delete-task-subject-draft") {
                                //  Xóa task tồn tại trong bộ môn
                                unset($data_draft->add_task_in_subject[$key]);
                                $data_draft->add_task_in_subject = array_values($data_draft->add_task_in_subject);
                            } else {
                                $data_draft->add_task_in_subject[$key] = (object)$data;
                            }

                        }
                    }

                    if ($exists_tasks == 1) {
                        $data_draft->add_task_in_subject = array_merge((array)$data_draft->add_task_in_subject, $data_save);
                    }
                }
            }
            //Chưa tồn tại thì khởi tạo đối tượng
            $draft['add_task_in_subject'][] = $data;
        }

        //      SỬA NHÁP : Sửa task, hoàn thành task (có trong dữ liệu) không thuộc bộ môn

        if ($data['type_task'] == 'main-edit-task' || $data['type_task'] == 'main-delete-task' || $data['type_task'] == 'finish-main-edit-task') {
            if ($data['type_task'] == 'main-delete-task') {
                $data['delete_status'] = true;
            }
            foreach ($message_data as $data_draft) {
                if (isset($data_draft->main_adit_task)) { //neu co obj roi thi update
                    $exists_tasks = 1;

                    foreach ($data_draft->main_adit_task as $key_task_draft => $task_draft) {
                        if ($task_draft->id == $data['id']) {
                            $exists_tasks = 2;

                            $data_draft->main_adit_task[$key_task_draft] = (object)$data;

                        }
                    }

                    if ($exists_tasks == 1) {
                        $data_draft->main_adit_task = array_merge((array)$data_draft->main_adit_task, [(object)$data]);
                    }
                }
            }


            $draft['main_adit_task'][] = $data;
        }


        if ($exists_tasks == 0) {
//            them moi data
            $this->_base->logsFileText($dir_name, $filename, $data = $draft);
        } else {
////            luu thay doi
            file_put_contents($filename, json_encode($message_data));
        }

        return response()->json([
            'status' => true,
        ]);
    }


    public function getAdminAddTask($request, $task)
    {
        $new = null;
        if (!is_null($request->admin_ids)) {
            foreach ($request->admin_ids as $v) {
                if (!in_array($v, explode('|', $task->admin_ids))) {
                    $new[] = $v;
                }
            }
        }
        return $new;
    }

    public function addSingleAdmin(Request $request)
    {
//        dd($request->all());
        $admin = '';
        if ($request->has('admin_ids') && is_array($request->admin_ids)) {
            $admin = '|' . implode('|', $request->admin_ids) . '|';
        }
        if ($request->has('admin_ids') && is_string($request->admin_ids)) {
            $admin = $request->admin_ids;
        }

        $data = [
            'end_date' => $request->end_date != '' ? date('Y/m/d', strtotime($request->end_date)) : date('Y/m/d'),
            'start_date' => $request->start_date != '' ? date('Y/m/d', strtotime($request->start_date)) : date('Y/m/d'),
            'admin_ids' => $admin,
            'admin_id' => Auth::guard('admin')->user()->id,
        ];
        $data['status'] = $request->status;
        $dir_name = config('eworkingjob.job_data_draf') . $request->job_id;
        $filename = $dir_name . '/draft.txt';
        $message_data = [];
        if (file_exists($filename)) { //kiem tra xem file ton tai thi lay ra content
            $message_data = (array)json_decode(file_get_contents($filename));
        }

        $exists_sub_manage = 0;
        foreach ($message_data as $data_draft) {
            if (isset($data_draft->sub_manage)) { //neu co obj general roi thi luu thay doi
                $data_draft->sub_manage = (object)$data;
                $exists_sub_manage = 1;
            }
        }

        if ($exists_sub_manage == 0) {
            $draft['sub_manage'] = $data;
//            them moi data
            $this->_base->logsFileText($dir_name, $filename, $data = $draft);
        } else {
//            luu thay doi
            file_put_contents($filename, json_encode($message_data));
        }

        return response()->json([
            'status' => true,
        ]);
    }

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('eworkingjob::task.list')->with($data);
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('eworkingjob::task.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    unset($data['admin_ids']);
                    $data['status'] = $request->has('status') ? 1 : 0;
                    if ($request->has('admin_ids')) {
                        $data['admin_ids'] = '|' . implode('|', $request->admin_ids) . '|';
                    }
                    $data['subject_id'] = $request->subject_id;

                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return $this->addReturn($request);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
                return back();
            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('eworkingjob::task.edit')->with($data);
            } else if ($_POST) {

                $data['status'] = $request->has('status') ? 1 : 0;


                $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                //  Tùy chỉnh dữ liệu insert

                unset($data['admin_ids']);
                $data['status'] = $request->has('status') ? 1 : 0;
                if ($request->has('admin_ids')) {
                    $data['admin_ids'] = '|' . implode('|', $request->admin_ids) . '|';
                }
                $data['subject_id'] = $request->subject_id;
                #
                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }
                return $this->updateReturn($request, $item);
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xuất bản!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
                return back();
            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return $this->deleteReturn($request);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function updateStatus(Request $request, $id)
    {
        $task = Task::findOrFail($id);
        $task->status = $request->status;
        $task->save();
        CommonHelper::one_time_message('success', 'Cập nhật trạng thái thành công!');
        return back();
    }

    public function getByJobID($job_id)
    {
        $dir_name = config('eworkingjob.job_data_draf') . $job_id;
        $filename = $dir_name . '/draft.txt';
        $message_data = [];
        if (file_exists($filename)) { //kiem tra xem file ton tai thi lay ra content
            $message_data = (array)json_decode(file_get_contents($filename));
        }

        $status_draft = false;
        foreach ($message_data as $data_draft) {
            if (isset($data_draft->main_adit_task)) { //neu co obj general roi thi luu thay doi
                foreach ($data_draft->main_adit_task as $key => $task) {
                    $data_draft->main_adit_task_new[$task->id] = $task;
                    $status_draft = true;
                }
                $main_adit_task_new = $data_draft->main_adit_task_new;
            }
        }
        $data['task_single'] = Task::where('job_id', $job_id)->where('task_type', 2)->paginate(5);
        $data['task_draft'] = $status_draft ? $main_adit_task_new : null;

        $data['job_id'] = $job_id;
        return view('eworkingjob::form.task_content')->with($data);
    }
}
