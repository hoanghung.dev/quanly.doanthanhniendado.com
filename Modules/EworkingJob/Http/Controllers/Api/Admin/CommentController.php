<?php

namespace Modules\EworkingJob\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Auth;
use Modules\EworkingJob\Models\Job;

class CommentController extends Controller
{

    public function showComment($id)
    {
        try {
            //  Check permission
            if (!CommonHelper::has_permission(\Auth::guard('api')->id(), 'job_view')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không đủ quyền'
                        ]
                    ],
                    'data' => null,
                    'code' => 403
                ]);
            }

            $dir_name = config('eworkingjob.job_data_draf') . $id;
            $filename = $dir_name . '/comment.txt';
            $message_data = null;
            if (file_exists($filename)) {
                $message_data = (array)json_decode(file_get_contents($filename));
            }
            $data_new = null;
            if (count($message_data) > 0 && $message_data != null) {
                foreach ($message_data as $key => $value) {
                    $admin = \App\Models\Admin::select('id', 'name', 'image')->where('id', $value->user_id)->first();
                    $value->admin = (object)[
                        'id' => $admin->id,
                        'name' => $admin->name,
                        'avatar' => CommonHelper::getUrlImageThumb($admin->image, 'false', 'false'),
                    ];
                    unset($value->name);
                    unset($value->avt);
                    unset($value->avt_for_realtime);
                    unset($value->user_id);
                    unset($value->message);
                    unset($value->image);

                }
            }
            if (count($message_data) > 0) {

                return response()->json([
                    'status' => true,
                    'msg' => '',
                    'errors' => (object)[],
                    'data' => $message_data,
                    'code' => 201
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }


        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }
}
