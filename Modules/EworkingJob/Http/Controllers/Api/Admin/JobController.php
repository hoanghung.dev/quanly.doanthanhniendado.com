<?php

namespace Modules\EworkingJob\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\RoleAdmin;
use Auth;
use Illuminate\Http\Request;
use Modules\EworkingJob\Models\Job;
use Modules\EworkingJob\Models\JobType;
use Modules\EworkingJob\Models\Task;

class JobController extends Controller
{

    protected $module = [
        'code' => 'job',
        'table_name' => 'jobs',
        'label' => 'Công việc',
        'modal' => 'Modules\EworkingJob\Models\Job',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'short_name' => [
            'query_type' => 'like'
        ],
        'company_id' => [
            'query_type' => '='
        ],
    ];

    public function index(Request $request)
    {
        try {
            //  Check permission
            if (!CommonHelper::has_permission(\Auth::guard('api')->id(), 'job_view')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không đủ quyền'
                        ]
                    ],
                    'data' => null,
                    'code' => 403
                ]);
            }

            //  Filter
            $where = $this->filterSimple($request);

            $listItem = Job::leftJoin('companies', 'companies.id', '=', 'jobs.company_id')
                ->selectRaw('jobs.id, jobs.name,  jobs.project_id, 
                    companies.id as company_id, companies.short_name as company_short_name')->whereRaw($where);
            if (!isset($request->company_id)) {
                $company_ids = explode('|', trim(\Auth::guard('api')->user()->company_ids, '|'));
                $listItem = $listItem->whereIn('company_id', $company_ids);
            } else {
                $listItem = $listItem->where('company_id', $request->company_id);
            }
            //  Sort
            $listItem = $this->sort($request, $listItem);
            $listItem = $listItem->paginate(20)->appends($request->all());

            foreach ($listItem as $k => $item) {
                $item->name = $item->project_id != null ? $item->name.'- Dự án: '.@$item->project->name : $item->name;
                //  Lấy màu sắc cho job
                $job_end_date = @Task::where('job_id', $item->id)->where('task_type', [1, 2, 3])->orderBy('end_date', 'desc')->first()->end_date;
                $days = (strtotime($job_end_date) - strtotime(date('Y-m-d'))) / (60 * 60 * 24);
                $bg_deadline = '';
                if ($days < 0) {
                    $bg_deadline = '#fff';
                } elseif ($days == 0) {
                    $bg_deadline = 'Tomato';
                } elseif ($days > 0 && $days <= 2) {
                    $bg_deadline = 'LightSalmon';
                } elseif ($days > 2 && $days <= 7) {
                    $bg_deadline = 'MistyRose';
                } elseif ($days > 0 && $days > 7) {
                    $bg_deadline = 'SeaShell';
                }
                $item->color = $bg_deadline;

                $item->deadline = [
                    'status' => $job_end_date != null ? 1 : 0,
                    'msg' => $job_end_date != null ? 'Hoàn thành' : 'Chưa hoàn thành',
                    'date' => $job_end_date
                ];
//                $item->deadline = $job_end_date;

                //  Lấy company
                $item->company = [
                    'id' => $item->company_id,
                    'short_name' => $item->company_short_name
                ];

                unset($item->company_id);
                unset($item->company_short_name);
            }
            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {
            //  Check permission
            if (!CommonHelper::has_permission(\Auth::guard('api')->id(), 'job_view')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không đủ quyền'
                        ]
                    ],
                    'data' => null,
                    'code' => 403
                ]);
            }

            $item = Task::leftJoin('companies', 'companies.id', '=', 'tasks.company_id')->leftJoin('jobs', 'jobs.id', '=', 'tasks.job_id')
                ->selectRaw('tasks.id, tasks.name, tasks.end_date, companies.short_name as companies_short_name, jobs.name as jobs_name')
                ->where('tasks.job_id', $id)->orderBy('end_date', 'desc')->get();
            foreach ($item as $k => $v) {
                $v->company = [
                    'name' => $v->companies_short_name
                ];
                $v->job = [
                    'name' => $v->jobs_name,
//                    'end_date' => $item->first()->end_date
                ];
                unset($v->companies_short_name);
                unset($v->jobs_name);
            }
            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . $this->module['table_name'] . '.id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }

        $jobtype = $this->JobTypeId();
        if (!empty($jobtype)) {
            $where .= " AND job_type_id IN(" . implode(',', $jobtype) . ")";
        }
        return $where;
    }

    public function JobTypeId()
    {
        // Truy vấn các công việc thuộc phòng ban mà thành viên được phêp xem
        $find_role_id = RoleAdmin::where('admin_id', \Auth::guard('api')->user()->id)
            ->where('company_id', \Auth::guard('api')->user()->last_company_id)
            ->first();
        $role_id = '';
        if (!is_null($find_role_id)) {
            $role_id = $find_role_id->role_id;
        }

        $jobTypeIds = JobType::where('role_ids', 'like', '%|' . $role_id . '|%')
            ->pluck('id')
            ->toArray();
        return $jobTypeIds;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
