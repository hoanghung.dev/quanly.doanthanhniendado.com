<?php

namespace Modules\EworkingJob\Http\Helpers;

use App\Models\Category;
use App\Models\Meta;
use Auth;
use Modules\EworkingCompany\Models\Company;
use Modules\EworkingJob\Models\Job;
use Modules\EworkingJob\Models\Task;
use Session;
use View;

class EworkingJobHelper
{

    public static function deadline($company, $admin_id)
    {
//        Thống kê công việc(Nhiệm vụ)
        $toDay = date('Y-m-d');
        if (!empty($company)) {
            $beforeDeadlines = Job::whereIn('company_id', $company)
                ->where('status', 1)->get();
            $arrbeforeDeadlines = [];
            $arrId = [];
            foreach ($beforeDeadlines as $key => $before) {
                $query = Task::where('job_id', $before->id)->whereIn('task_type', [1, 2, 3])->where(function ($query) use ($company, $toDay) {
                    foreach ($company as $com) {
                        $findCompany = Company::where('id', $com);
                        if ($findCompany->exists()) {
                            $findCompany = $findCompany->first();
                            $DayAfter = date('Y-m-d', strtotime('+' . @$findCompany->before_deadline . ' day'));
                            $query->whereDate('tasks.end_date', '>=', $toDay)
                                ->whereDate('tasks.end_date', '<=', $DayAfter);
                        }
                    }
                });
                $end_date = $query->max('end_date');
                $start_date = $query->min('start_date');

                if (!is_null($end_date) && !is_null($start_date)) {
                    $before->start_date = $start_date;
                    $before->end_date = $end_date;
                    $arrId[] = $before->id;
                    if ($arrbeforeDeadlines != []) {
                        if (strtotime($before->end_date) < strtotime($arrbeforeDeadlines[0]->end_date)) {
                            array_unshift($arrbeforeDeadlines, $before);
                        } else {
                            $arrbeforeDeadlines[] = $before;
                        }
                    } else {
                        array_unshift($arrbeforeDeadlines, $before);
                    }
                }
            }

            $afterDeadlines = Job::whereIn('company_id', $company)
                ->whereNotIn('id', $arrId)
                ->where('status', 1)->get();
            $arrAfterDeadlines = [];
            foreach ($afterDeadlines as $key => $after) {
                $after->company;
                $after->project = [
                    'id' => @$after->project->id,
                    'name' => @$after->project->name
                ];
                $query = Task::where('job_id', $after->id)->whereIn('task_type', [1, 2, 3])->where(function ($query) use ($company, $toDay) {
                    foreach ($company as $com) {
                        $findCompany = Company::where('id', $com);
                        if ($findCompany->exists()) {
                            $findCompany = $findCompany->first();
                            $DayAfter = date('Y-m-d', strtotime('+' . @$findCompany->before_deadline . ' day'));
                            $query->whereDate('tasks.end_date', '>=', $toDay)
                                ->whereDate('tasks.end_date', '<=', $DayAfter);
                        }
                    }
                });
                $end_date = $query->max('end_date');
                $start_date = $query->min('start_date');

                if (!is_null($end_date) && !is_null($start_date)) {
                    $after->start_date = $start_date;
                    $after->end_date = $end_date;
                    if ($arrAfterDeadlines != []) {
                        if (strtotime($after->end_date) > strtotime($arrAfterDeadlines[0]->end_date)) {
                            array_unshift($arrAfterDeadlines, $after);
                        } else {
                            $arrbeforeDeadlines[] = $before;
                        }
                    } else {
                        array_unshift($arrAfterDeadlines, $after);
                    }
                }
            }


            //afterDeadlinesUser
            $arrbeforeDeadlineUser = [];
            foreach ($arrbeforeDeadlines as $arrbefore) {
                $arrbefore->company;
                $arrbefore->project = [
                    'id' => @$arrbefore->project->id,
                    'name' => @$arrbefore->project->name
                ];
                $admin = EworkingJobHelper::getAdminInJob($arrbefore->id);
                if (in_array($admin_id, explode('|', $admin))) {
                    $arrbeforeDeadlineUser[] = $arrbefore;
                }
            }
            //beforeDeadlinesUser
            $arrAfterDeadlinesUser = [];
            foreach ($arrAfterDeadlines as $arrAfter) {
                $arrAfter->company;
                $arrAfter->project = [
                    'id' => @$arrAfter->project->id,
                    'name' => @$arrAfter->project->name
                ];
                $admin = EworkingJobHelper::getAdminInJob($arrAfter->id);
                if (in_array($admin_id, explode('|', $admin))) {
                    $arrAfterDeadlinesUser[] = $arrAfter;
                }
            }
            sort($arrbeforeDeadlines);
            ksort($arrAfterDeadlines);
            sort($arrbeforeDeadlineUser);
            ksort($arrAfterDeadlinesUser);
            return [
                'data' => [
                    'beforeDeadlines' => $arrbeforeDeadlines,
                    'afterDeadlines' => $arrAfterDeadlines,
                    'arrAfterDeadlinesUser' => $arrAfterDeadlinesUser,
                    'arrbeforeDeadlineUser' => $arrbeforeDeadlineUser,
                ]
            ];
        } else {
            return [
                'data' => [
                    'beforeDeadlines' => '',
                    'afterDeadlines' => '',
                    'arrAfterDeadlinesUser' => '',
                    'arrbeforeDeadlineUser' => '',
                ]
            ];
        }
    }

    public static function getAdminInJob($job_id)
    {
        $adminIds = Task::where('job_id', $job_id)->whereIn('task_type', [1, 2, 3])->pluck('admin_ids')->toArray();
        $stringAdmin = '';
        foreach ($adminIds as $key => $id) {
            $char = $key == 0 ? '' : '|';
            if ($id != '') {
                $stringAdmin .= $char . trim($id, '|');
            }
        }
        $string = trim($stringAdmin, '|');
        $listID = explode('|', $string);
        return implode('|', array_unique($listID));
    }
}
