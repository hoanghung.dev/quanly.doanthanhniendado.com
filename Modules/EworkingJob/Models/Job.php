<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */

namespace Modules\EworkingJob\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\EworkingCompany\Models\Company;
use Modules\EworkingNotification\Models\Notifications;

class Job extends Model
{

    protected $table = 'jobs';

    protected $guarded = [];
//    protected $fillable = [
//        'name', 'end_date'
//    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'employees_id');
    }

    public function subject()
    {
        return $this->hasMany(Subject::class, 'job_id', 'id');
    }

    public function task()
    {
        return $this->hasMany(Task::class, 'job_id', 'id');
    }
//
//    public function deleteEverythingRelated()
//    {
//        $subject_query = Subject::where('job_id', $this->attributes['id']);
//        Task::where('job_id', $this->attributes['id'])->orWhere(function ($query) use ($subject_query) {
//            $query->whereIn('subject_id', $subject_query->pluck('id')->toArray());
//        })->delete();
//        $subject_query->delete();
//        return true;
//    }

//
    public function delete()
    {
        $this->task()->delete();
        $this->subject()->delete();
        $this->notification()->delete();
        parent::delete();
    }

    public function job_type()
    {
        return $this->belongsTo(JobType::class, 'job_type_id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function Company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function notification()
    {
        return $this->hasMany(Notifications::class, 'job_id', 'id');
    }
}
