<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */

namespace Modules\EworkingJob\Models;

use Illuminate\Database\Eloquent\Model;

class JobType extends Model
{

    protected $table = 'job_types';
    public $timestamps = false;
    protected $fillable = [
        'name', 'end_date'
    ];

    public function jobs()
    {
        return $this->hasMany(Job::class, 'job_type_id', 'id');
    }
}
