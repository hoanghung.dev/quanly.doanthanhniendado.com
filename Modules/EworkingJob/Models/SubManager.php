<?php

namespace Modules\EworkingJob\Models;

use Illuminate\Database\Eloquent\Model;

class SubManager extends Model
{

    protected $table = 'submanager';

    protected $fillable = [
        'start_date', 'end_date', 'admin_ids','job_id'
    ];

    public $timestamps = false;
}
