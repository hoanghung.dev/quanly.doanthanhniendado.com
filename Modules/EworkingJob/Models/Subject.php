<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */

namespace Modules\EworkingJob\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{

    protected $table = 'subjects';

    protected $guarded = [];

    public $timestamps = false;

    public function tasks()
    {
        return $this->hasMany(Task::class, 'subject_id', 'id');
    }

    public function delete()
    {
        $this->tasks()->delete();
        parent::delete();
    }
}
