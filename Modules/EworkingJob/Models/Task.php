<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */

namespace Modules\EworkingJob\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\EworkingCompany\Models\Company;

class Task extends Model
{

    protected $table = 'tasks';

    protected $guarded = [];

    public $timestamps = false;

    public function job()
    {
        return $this->belongsTo(Job::class, 'job_id');
    }

    public function Company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }
}
