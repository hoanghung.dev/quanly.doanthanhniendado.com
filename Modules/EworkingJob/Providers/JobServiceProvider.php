<?php

namespace Modules\EworkingJob\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\EworkingJob\Http\Controllers\Admin\JobTypeController;
use Modules\EworkingJob\Http\Controllers\Admin\SubjectController;

class JobServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {

        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
//        dd(config('eworkingjob.job_data_draf'));
        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Custom setting
            $this->registerPermission();

            //  Cấu hình menu trái
            $this->rendAsideMenu();

            //  Thêm phân quyền
//            $this->rendRoleForPermissionJobType();
        }
        // Công ty mới đăng ký => Tạo bộ môn mặc định | Tạo loại công việc mặc định
        \Eventy::addAction('company.register', function ($company) {
            $subjectController = new SubjectController();
            $subjectController->CreateDefaultSubjectForCompany($company);
        }, 1, 1);
    }

    public function rendRoleJobType()
    {
        \Eventy::addFilter('eworkingrole.role_edit_list_per', function () {
            print view('eworkingjob::partials.role_edit_list_per');
        }, 3, 1);
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['project_view', 'project_add', 'project_edit', 'project_delete',
                'job_view', 'job_add', 'job_edit', 'job_delete',
                'subject_view', 'subject_add', 'subject_edit', 'subject_delete',
                'subject_warehouse_view', 'subject_warehouse_add', 'subject_warehouse_edit', 'subject_warehouse_delete',
                'task_view', 'task_add', 'task_edit', 'task_delete',]);
            return $per_check;
        }, 1, 1);
    }

    public function rendAsideMenu()
    {
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('eworkingjob::partials.aside_menu.aside_menu_dashboard_after');
        }, 3, 1);
    }

    public function rendRoleForPermissionJobType()
    {
        \Eventy::addFilter('eworkingrole.role_edit_list_per', function () {
            print view('eworkingjob::partials.role_edit_list_per');
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('eworkingjob.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php', 'eworkingjob'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/eworkingjob');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/eworkingjob';
        }, \Config::get('view.paths')), [$sourcePath]), 'eworkingjob');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/eworkingjob');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'eworkingjob');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'eworkingjob');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
