@php

    $fields = [];
    foreach ($module['form'] as $tab) {

        foreach ($tab['td'] as $row) {
            $row['value'] = isset($result) ? @$result->{$row['name']} : @$row['value'];
            $fields[] = $row;
        }
    }
    $form_data = [
        'action' => URL::to('/admin/'.$module['code'].'/add'),
        'td' => $fields
    ];
@endphp
@extends(config('core.admin_theme').'.template')
@include($module['path']."::form.multi_box", $form_data)
@section('custom_head')
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset(config('core.admin_asset').'/css/form.css') }}">
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset('public/libs/tag/jquery.mentionsInput.css') }}">
@endsection
@push('scripts')
    <script src="{{ asset(config('core.admin_asset').'/js/pages/crud/metronic-datatable/advanced/vertical.js') }}"
            type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce_editor.js') }}"></script>
    <script type="text/javascript">
        editor_config.selector = ".editor";
        editor_config.path_absolute = "{{ (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]" }}/";
        tinymce.init(editor_config);
    </script>
    {{--<script type="text/javascript" src="{{ asset('public/libs/tag/jquery.mentionsInput.js') }}"></script>--}}
    {{--<script type="text/javascript" src="{{ asset('public/libs/tag/jquery.events.input.js') }}"></script>--}}
    {{--<script type="text/javascript" src="{{ asset('public/libs/tag/jquery.elastic.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset(config('core.admin_asset').'/js/form.js') }}"></script>
    <script type="text/javascript" src="{{ asset(config('core.admin_asset').'/js/format_money.js') }}"></script>
    <script type="text/javascript" src="{{ asset(config('core.admin_asset').'/js/list.js') }}"></script>
{{--    <script src="{{ asset('Modules\EworkingJob\Resources\assets\js\custom.js') }}"></script>--}}
@endpush
