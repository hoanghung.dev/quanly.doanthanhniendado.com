

<script src='https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js'></script>
<script src="{{asset('public/backend/js/pusher.min.js')}}"></script>
<script>
    $('body').on('mouseover', '.p-other', function () {
        $(this).parent().next().show();
    });
    $('body').on('mouseout', '.p-other', function () {
        $(this).parent().next().hide();
    });

    {{--tag ten --}}
    $(function () {
        $('textarea.mention,textarea.text-msg').mentionsInput({
            onDataRequest: function (mode, query, callback) {
                $.ajax({
                    url: "/admin/list-admin-ajax",
                    type: 'GET',
                    data: {},
                    success: function (responseData) {
                        responseData = responseData.json_admin;
                        responseData = _.filter(responseData, function (item) {
                            return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1;
                        });
                        callback.call(this, responseData);
                    }
                });
            }
        });


        $('.get-mentions').click(function () {
            $('textarea.mention').mentionsInput('getMentions', function (data) {
                alert(JSON.stringify(data));
            });
        });

    });


    // //ajax edit date cong viec
    // $('body').on('click', '.js-select-member', function () {
    //     alert('akshdkaygsdhaygsd');
    // });
    // //hien popup tag ten
    // $('body').on('keyup', '#input-type-comment', function () {
    //     var string = $(this).val();
    //     if (string.search('@') != -1) {
    //         $('.add-mention.pop-over').show();
    //     } else {
    //         $('.add-mention.pop-over').hide();
    //     }
    // });

    //search admin
    function listAdmin() {
        $.ajax({
            url: "/admin/list-admin-ajax",
            type: 'GET',
            data: {},
            success: function (result) {
                alert(result);
            }
        });
        return result;
    }

    function searchAdmin(idUl, idInput) {
        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById(idInput);
        filter = input.value.toUpperCase();
        ul = document.getElementById(idUl);
        li = ul.getElementsByTagName("li");
        var number = 0;
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            txtValue = a.textContent || a.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                number++;
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
        return number;
    }

    //hien popup tag ten
    $('body').on('click', '.js-comment-mention-member', function () {
        $('.add-mention.pop-over').show();
    });
    //an popup tag ten
    $('body').on('click', '.add-mention.pop-over .pop-over-header-close-btn', function () {
        $('.add-mention.pop-over').hide();
    });
    //xoa comment
    $('body').on('click', '.comment-job .delete-comment', function () {
        if (confirm('Bạn có chắc chắn xóa bình luận !')) {
            var id_comment = $(this).data('id')
            var _delete = $(this).parents('.comment-job');
            $.ajax({
                url: "{{route('delete-comment')}}",
                type: 'GET',
                data: {
                    'id': '{{$id_job}}',
                    'id_comment': id_comment,
                },
                success: function (result) {
                    if (result.status) {
                        _delete.remove();
                        toastr.success("Đã xóa bình luận!");
                        $('')
                    } else {
                        toastr.error("Xin vui lòng thử lại!");
                    }
                }
            });
        }
    });
    //sua comment
    $('body').on('click', '#save-comment', function () {
        var modalCmt = $('#dialog-edit-comment');
        var id_comment = modalCmt.data('id');
        $('textarea.text-msg').mentionsInput('val', function (text) {
            $('.content-edit-comment').val(text);
        });
        var msg_new = modalCmt.find('.content-edit-comment').val();
        var img_new = modalCmt.find('.text-img').val();
        $.ajax({
            url: "{{route('edit-comment')}}",
            type: 'GET',
            data: {
                'type': 0,
                'id': '{{$id_job}}',
                'img_new': img_new,
                'msg_new': msg_new,
                'id_comment': id_comment,

            },
            success: function (result) {
                if (result.status) {
                    window.location.reload();
                } else {
                    toastr.error("Xin vui lòng thử lại!");
                }
            }
        });
    });

    //open edit comment
    $('body').on('click', '.comment-job .edit-comment', function () {
        var modalCmt = $('#dialog-edit-comment');
        var boxCmt = $(this).parents('.kt-todo__comment');
        var id_comment = $(this).data('id');
        var tag_comment = $(this).data('tag');
        modalCmt.find('input[name=tag-comment]').val(tag_comment);
        modalCmt.attr('data-id', id_comment);
        modalCmt.modal();
        let content = boxCmt.find('.kt-todo__text').text();
        modalCmt.find('.text-msg').val(content.trim());

    });
</script>
<script>
    $(document).ready(function () {
        //bam vao nut chat
        $('body').on('click', '.comment-controls .btn', function () {
            chatRealtime();
        });
        //enter chat
        $(document).on('keypress', function (e) {
            if (e.which == 13) {
                chatRealtime();
            }
        });

        //function xu li
        function chatRealtime() {
            let value = $('.comment-box-input').val().trim();
            if (value != '') {
                $('textarea.mention').mentionsInput('val', function (text) {
                    $('#comment-box-input').val(text);
                });
            }
            var image = $('#file-comment').val();
            var name = '{{$id_job}}';
            var content = $('#comment-box-input').val();
            console.log(content);
            if (content == '' && image == '') {
                msg = $('#msg');
                if (msg != '') {
                    msg.html('<p id="text-msg" class="text-danger text-center">Xin mời nhập vào nội dung chat !</p>');
                    setTimeout(function () {
                        msg.empty();
                    }, 4000);
                }
                return false;
            }

            if (image != '' && image != null) {
                file(name);
            }
            $('#file').val(null);
            if (content != '' && content != null) {
                $.ajax({
                    url: "{{route('commentJob')}}",
                    type: 'GET',
                    data: {
                        'channel': name,
                        'message': content,
                    },
                    success: function (result) {
                        $('.list-comment-job .text-center').empty();
                        $('.comment-box-input').val(null);
                        $('#comment-box-input').val(null);
                    }
                });
            }
        }

        //chat co anh
        function file(name) {
            //Lấy ra files
            var file_data = $('#file-comment').prop('files');
            var i;
            var img_true = 0;
            var length = file_data.length;
            var form_data = new FormData();

            for (i = 0; i < length; i++) {
                //lấy ra kiểu file
                var type = file_data[i].type;
                //Xét kiểu file được upload
                var match = ["image/gif", "image/png", "image/jpg", "image/jpeg"];
                //kiểm tra kiểu file
                if (type == match[0] || type == match[1] || type == match[2] || type == match[3]) {
                    img_true++;
                    //thêm files vào trong form data
                    form_data.append('file[]', file_data[i]);

                } else {
                    $('#file-comment').val('');
                    $('#msg').text('Chỉ được upload file ảnh');
                    return false;
                }
            }

            if (img_true == length) {
                //sử dụng ajax post
                $.ajax({
                    url: '{{route('commentImg')}}', // gửi đến file upload
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function (res) {
                        var obj = JSON.parse(res);
                        if (obj.status) {
                            $.ajax({
                                url: "{{route('commentJob')}}",
                                type: 'GET',
                                data: {
                                    'channel': name,
                                    'image': obj.image,
                                },
                                success: function (result) {
                                    $('.list-comment-job .text-center').empty();
                                }
                            });
                        }
                    }
                });
            }
            return false;
        }
    });

    //click vao chon file
    $('body').on('click', '.js-comment-add-attachment', function () {
        $('#file-comment').click();
    });

    //hien ten anh chon
    // function changeImg(input) {
    //     $('#name-file').text(input.files[0].name);
    // }
</script>
<script>

    Pusher.logToConsole = true;
    var pusher = new Pusher('{{$settings["PUSHER_APP_KEY"]}}', {
        cluster: '{{$settings["PUSHER_APP_CLUSTER"]}}',
        forceTLS: true
    });
    var name = '{{$id_job}}';
    var channel = pusher.subscribe(name);
    channel.bind('chat', function (data) {
                {{--console.log('{{Auth::guard('admin')->user()->id}}' + '|' + data.user_id);--}}
        let edit = ('{{Auth::guard('admin')->user()->id}}' == data.user_id) ? ' <div class="kt-widget2__actions"><a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="false"><i class="flaticon-more-1"></i></a><div class="dropdown-menu dropdown-menu-fit dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(538px, 34px, 0px); top: 0px; left: 0px; will-change: transform;"> <ul class="kt-nav"> <li class="kt-nav__item"> <a data-id="' + data.id + '" data-tag="' + data.tag + '" class="kt-nav__link edit-comment"> <i class="kt-nav__link-icon flaticon2-edit"></i> <span class="kt-nav__link-text">Sửa bình luận</span> </a> </li> <li class="kt-nav__item"> <a data-id="' + data.id + '" class="kt-nav__link delete-comment"> <i class="kt-nav__link-icon la la-close"></i> <span class="kt-nav__link-text">Xóa bình luận</span> </a> </li> </ul></div> </div>' : '';
        let html = '<div style="padding-top: 5px;" class="kt-todo__comment comment-job cmt-' + data.id + '">\n' +
            '                                                        <div class="kt-todo__box header-content-chat">\n' +
            '                                                            <span title="' + data.name + '" class="kt-media kt-media--sm" data-toggle="expand"\n' +
            '                                                                  style="background-image: url(' + data.avt_for_realtime + ');background-size: contain;">\n' +
            '                                                                <span></span>\n' +
            '                                                            </span><div class="right-cmt">\n' +
            '                                                            <a href="/admin/profile/' + data.id + '" title="' + data.name + '" class="kt-todo__username name-comment">\n' + data.name + '</a>\n' +
            '                                                            <span class="kt-todo__date" style="font-style: italic">\n' + data.time +
            '                                                            </span></div>\n' + edit +
            '                                                        </div>\n' +
            '                                                        <span class="kt-todo__text">\n' + data.message + '</span>\n' +
            '                                                    </div>';

        $('.list-comment-job').append(html);
    });
</script>
