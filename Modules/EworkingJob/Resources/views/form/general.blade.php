<style>
    .kt-section {
        margin-bottom: 0px !important;
    }
</style>
<?php
//        dd($message_data );
$status_draft = false;
foreach ($message_data as $data_draft) {
    if (isset($data_draft->general)) { //neu co obj general roi thi luu thay doi
        $result = $data_draft->general;
        $status_draft = true;
    }
}
?>
{{--@if(count($message_data) > 0 && $message_data != null)--}}
<div class="tab-pane active" id="kt_widget5_tab1_content" aria-expanded="true">
    <div class="kt-widget5">
        <div class="kt-widget5__item" style="margin-bottom: 0px;padding-bottom: 0px;">
            <div class="kt-widget5__content">
                <div class="kt-widget5__section general-value">
                    <div style="display: flex;">
                        <a class="kt-widget5__title" style="font-size: 2.0rem;">
                            <span class="kt-font-info job-name">{{@$result->name}}</span>
                        </a>
                    </div>
                    <div class="kt-widget5__info">
                        <p>
                            <span>Loại công việc:</span>
                            <span class="kt-font-info job-type">{{$status_draft ? $result->job_type_name : @$result->job_type->name}}</span>
                        </p>
                        <p>
                            <span>Thuộc dự án:</span>
                            <span class="kt-font-info job-project">
                                {{$status_draft ? $result->project_name :@$result->project->name}}
                            </span>
                        </p>
                    </div>
                    <p class="kt-widget5__desc">
                        Ghi chú công việc : <span class="kt-font-info job-intro">
                              {{@$result->intro}}
                        </span>
                    </p>
                </div>
            </div>
            <div class="kt-widget5__content data-job"
                 data-name="{{@$result->name}}"
                 data-job_type_id="{{@$result->job_type_id}}"
                 data-project_id="{{@$result->project_id}}"
                 data-intro="{{@$result->intro}}">
            </div>
        </div>
    </div>
</div>
