<style>
    .icon-header {
        cursor: pointer;
    }
</style>
<?php
$dir_name = config('eworkingjob.job_data_draf') . $result->id;
$filename = $dir_name . '/draft.txt';

$data_update = (file_exists($filename)) ? true : false;
?>
<div class="row">
    <div class="col-md-12">
        <p class="pull-right btn-action-job">
            @if($data_update)
                <a href="{{URL::to(Request::path())}}?reset=1"
                   class="icon-header btn btn-twitter btn-elevate btn-pill text-light"><i
                            class="flaticon2-refresh-1"></i>Reset
                    dữ liệu</a>
            @endif
            <span title="Thêm nhiệm vụ không thuộc bộ môn"
                  class="icon-header btn-add-task btn btn-facebook btn-elevate btn-pill text-light"><i
                        class="la la-plus-circle"></i>Thêm nhiệm vụ</span>
            <span class="icon-header btn-insert-subject btn btn-instagram btn-elevate btn-pill text-light"><i
                        class="la la-plus-circle"></i>Thêm bộ môn</span>
            <a href="{{URL::to('/admin/project/'.@$result->project_id)}}"
               class="icon-header btn btn-linkedin  btn-elevate btn-pill text-light"><i
                        class="la la-arrow-circle-o-left"></i>Quay lại dự án</a>
            <a href="{{URL::to('/admin/job')}}" class="icon-header btn btn-primary btn-elevate btn-pill text-light"><i
                        class="la la-backward"></i>Quay lại danh sách</a>
            @if($data_update)
                <span title="Lưu nội dung"
                      class=" icon-header update-job-click btn-success btn btn-elevate btn-pill text-light"><i
                            class="la la-check-square-o"></i>Hoàn thành</span>
            @endif

        </p>
    </div>
</div>
<script>
    $(document).ready(function () {
        // update-job-click
        $('body').on('click', '.update-job-click', function () {
            // console.log( getValueJobUpdate('.job-value'));
            $.ajax({
                url: '/{{Request::path()}}',
                type: 'POST',
                data: {
                    value: getValueJobUpdate('.job-value')
                },
                success: function (data) {
                    if (data.status) {
                        // window.location.reload();
                        window.location.href = '/admin/job';
                    } else {
                        toastr.error("Thất bại!");
                    }
                },
                error: function () {
                    alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
                }
            });
        });
    });
    getValueJobUpdate = (selector_get_value) => {
        let value = '[';
        $(selector_get_value).each(function (index) {
            let selector = $(this);
            (index != 0) ? value += ',' : '';
            value += '{"' + selector.data('name') + '":"' + selector.data('value') + '"}';

        });
        value += ']';
        return value;
    }
</script>
