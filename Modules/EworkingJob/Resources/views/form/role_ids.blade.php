<ul class="p-0" id="subject-tasks">
    <div class="kt-checkbox-list">
        <?php
        $roles = \App\Models\Roles::where('company_id', \Auth::guard('admin')->user()->last_company_id)->pluck('display_name', 'id');
        $checked = explode('|', @$result->role_ids);
        ?>
        @foreach($roles as $k => $v)
            <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
                <input name="{{ $field['name'] }}[]" class="$field['name']" value="{{ $k }}" type="checkbox" {{ in_array($k, $checked) || !isset($result) ? 'checked' : '' }}>{{ $v }}
                <span></span>
            </label>
        @endforeach
    </div>
</ul>