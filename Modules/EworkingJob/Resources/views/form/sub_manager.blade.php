<?php
$admin_single = \Modules\EworkingJob\Models\Task::where('job_id', $result->id)->where('task_type', 1)->first();
?>
<?php
$dir_name = config('eworkingjob.job_data_draf') . $result->id;
$filename = $dir_name . '/draft.txt';
$message_data = [];
if (file_exists($filename)) { //kiem tra xem file ton tai thi lay ra content
    $message_data = (array)json_decode(file_get_contents($filename));
}

$status_draft = false;
//dd($message_data);
foreach ($message_data as $data_draft) {
    if (isset($data_draft->sub_manage)) { //neu co obj general roi thi luu thay doi
        $admin_single = $data_draft->sub_manage;
        $status_draft = true;
    }
}
//dd($message_data);
?>
<div class="col-xl-12">
    <!--begin:: Widgets/Tasks -->
    <div class="kt-portlet__body" style="padding: 0">
        <div class="tab-content">
            <div class="tab-pane active" id="kt_widget2_tab1_content">
                <div class="kt-widget2">
                    <div class="kt-widget2__item">

                        <div class="kt-widget2__info">
                            <a class="kt-widget2__title value-submanager"
                               <?php
                               $start_date = @$admin_single->start_date != null ? date('d-m-Y', strtotime(@$admin_single->start_date)) : date('d-m-Y');
                               $end_date = @$admin_single->end_date != null ? date('d-m-Y', strtotime(@$admin_single->end_date)) : date('d-m-Y');
                               $status = @$admin_single->status == 1 ? 0 : 1;
                               ?>
                               data-success="{{URL::to('/admin/job/edit-single?job_id='.$result->id.'&start_date='.$start_date.'&end_date='.$end_date.'&admin_ids='.@$admin_single->admin_ids.'&status='.$status)}}"
                               data-status="{{@$admin_single->status == 1 || !is_object($admin_single) ?  1 :0}}"
                               data-start_date="{{$start_date}}"
                               data-end_date="{{$end_date}}"
                               data-admin_ids="{{@$admin_single->admin_ids}}">
                                Danh sách phụ trách chung
                            </a>
                            <a class="kt-widget2__username">
                                @if(is_object($admin_single))
                                    {{$admin_single->start_date != null ||$admin_single->start_date != '' ? date('d-m-Y',strtotime($admin_single->start_date)) : 'Chưa có'}}
                                    - {{$admin_single->end_date != null || $admin_single->end_date != '' ? date('d-m-Y',strtotime($admin_single->end_date)) : 'Chưa có'}}
                                @else
                                    Chưa tạo
                                @endif
                            </a>
                        </div>
                        <div>
                            <span class="kt-badge  kt-badge--{{@$admin_single->status == 1 || !is_object($admin_single) ?  'warning' :'success'}} kt-badge--inline">{{@$admin_single->status == 1 || !is_object($admin_single) ? 'Đang làm' : 'Hoàn thành'}}</span>
                        </div>
                        <div class="kt-widget2__admins">
                            @if(is_object($admin_single))
                                @include(config('core.admin_theme').'.list.td.admins', ['admin_ids' => $admin_single->admin_ids])
                            @else
                                Chưa có người phụ trách
                            @endif
                        </div>
                        <div>
                            <a style="top: 5px;" class="btn btn-clean btn-sm btn-icon btn-icon-md btn-add"
                               data-toggle="dropdown" aria-expanded="false">
                                <i class="flaticon-more-1"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right"
                                 x-placement="bottom-end"
                                 style="position: absolute; transform: translate3d(538px, 34px, 0px); top: 0px; left: 0px; will-change: transform;">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a class="kt-nav__link edit-submanager" title="Sửa phụ trách chung">
                                            <i class="kt-nav__link-icon flaticon2-edit"></i>
                                            <span class="kt-nav__link-text">Chỉnh sửa</span>
                                        </a>
                                    </li>
                                    @if(in_array(Auth::guard('admin')->user()->id,explode('|',@$admin_single->admin_ids)) && !$status_draft)
                                        <li class="kt-nav__item">
                                            <a class="kt-nav__link success-submanager">
                                                <i class="kt-nav__link-icon flaticon-refresh"></i>
                                                <span class="kt-nav__link-text">Chuyển trạng thái</span>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--end:: Widgets/Tasks -->
</div>
