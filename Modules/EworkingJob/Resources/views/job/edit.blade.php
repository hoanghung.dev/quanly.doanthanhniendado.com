@extends(config('core.admin_theme').'.template')
@section('main')
    <form class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid {{ @$module['code'] }}"
          action="" method="POST"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="kt-portlet" style="padding: 10px  10px 0 10px">
            @include('eworkingjob::form.header_job')
        </div>

        <div class="row">
            @if(isset($module['col'] ))
                @foreach($module['col'] as $col)
                    <div class="{{$col['class']}}" style="padding-right:0;padding-left:10px">
                    @foreach($module['form'] as $key =>$tab)
                        @if(in_array($key,$col['tab_name']))
                            <!--begin::Portlet-->
                                <div class="kt-portlet">
                                    <div class="kt-portlet__head">
                                        <div class="kt-portlet__head-label">
                                            <h3 class="kt-portlet__head-title">
                                                {!! str_replace('{id}',$result->id,@$tab['label'])  !!}
                                            </h3>
                                        </div>
                                    </div>
                                    <!--begin::Form-->
                                    <div class="kt-form">
                                        <div class="kt-portlet__body" id="{{$key}}" style=" padding-top: 10px;">
                                            <div class="kt-section kt-section--first">

                                                @foreach($tab['td'] as $field)
                                                    @if(isset($field['view'] ) && $field['view'] == 'custom')
                                                        @include($field['type'])
                                                        {{--<p>{{$field['type']}}</p>--}}
                                                    @else
                                                        @php
                                                            $field['value'] = @$result->{$field['name']};
                                                        @endphp
                                                        <div class="form-group-div form-group {{ @$field['group_class'] }}"
                                                             id="form-group-{{ $field['name'] }}">
                                                            <label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
                                                                    <span class="color_btd">*</span>@endif</label>
                                                            <div class="col-xs-12">
                                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                                <span class="text-danger">{{ $errors->first($field['name']) }}</span>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Form-->
                                </div>
                                <!--end::Portlet-->
                            @endif
                        @endforeach
                    </div>
                @endforeach
            @else
                @foreach($module['form'] as $key =>$tab)
                <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    {!! str_replace('{id}',$result->id,@$tab['label'])  !!}
                                </h3>
                            </div>
                        </div>
                        <!--begin::Form-->
                        <div class="kt-form">
                            <div class="kt-portlet__body" id="{{$key}}" style="padding-top: 10px;">
                                <div class="kt-section kt-section--first">

                                    @foreach($tab['td'] as $field)
                                        @if(isset($field['view'] ) && $field['view'] == 'custom')
                                            @include($field['type'])
                                            {{--<p>{{$field['type']}}</p>--}}
                                        @else
                                            @php
                                                $field['value'] = @$result->{$field['name']};
                                            @endphp
                                            <div class="form-group-div form-group {{ @$field['group_class'] }}"
                                                 id="form-group-{{ $field['name'] }}">
                                                <label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
                                                        <span class="color_btd">*</span>@endif</label>
                                                <div class="col-xs-12">
                                                    @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                    <span class="text-danger">{{ $errors->first($field['name']) }}</span>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach

                                </div>
                            </div>
                        </div>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                @endforeach
            @endif
        </div>
    </form>
    @if(isset($module['footer']))
        @foreach($module['footer'] as $include)
            @include($include)
        @endforeach
    @endif
@endsection
@section('custom_head')
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset(config('core.admin_asset').'/css/form.css?v=1') }}">
    <style>
        span#select2-job_type_id-rd-container {
            min-height: 38px;
        }
    </style>
@endsection
@push('scripts')
    <script src="{{ asset(config('core.admin_asset').'/js/pages/crud/metronic-datatable/advanced/vertical.js') }}"
            type="text/javascript"></script>

    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce_editor.js') }}"></script>
    <script type="text/javascript">
        editor_config.selector = ".editor";
        editor_config.path_absolute = "{{ (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]" }}/";
        tinymce.init(editor_config);
    </script>
    <script type="text/javascript" src="{{ asset('public/libs/tag/jquery.mentionsInput.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/libs/tag/jquery.events.input.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/libs/tag/jquery.elastic.js') }}"></script>
    <script type="text/javascript" src="{{ asset(config('core.admin_asset').'/js/form.js') }}"></script>
{{--    <script src="{{ asset('Modules\EworkingJob\Resources\assets\js\custom.js') }}"></script>--}}
    <script>
        $(document).ready(function () {

            $('body').on('click', '.get-subject', function () {
                let obj = $(this);
                obj.children().hide();
                let body = obj.parents('.kt-portlet__head').parent().find('.kt-portlet__body');
                body.slideToggle('slow');
                obj.children().show();
                obj.children().toggleClass('flaticon2-up').toggleClass('flaticon2-down');
            });
        });
    </script>
@endpush
