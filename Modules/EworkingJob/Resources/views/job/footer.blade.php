<input name="id" value="{{ $result->id }}" type="hidden">
@push('scripts')
    <script>
        $(document).ready(function () {
            $('.info_box input, .info_box select, .info_box textarea.form').change(function () {
                var name = $(this).attr('name').replace('[]', '');
                var val = $(this).val();
                var id = $('input[name=id]').val();
                if ($.isArray(val)) {
                    val = val.join('|');
                }
                $.ajax({
                    url: '/admin/job/ajax-update',
                    type: 'POST',
                    data: {
                        id: id,
                        name: name,
                        val: val
                    },
                    success: function (data) {
                        if (data.status) {
                            toastr.success("Đã cập nhật!");
                        } else {
                            toastr.error("Cập nhật thất bại!");
                        }
                    },
                    error: function () {
                        console.log('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
                    }
                });
            });

            $('body').on('click', '.delete-ajax', function () {
                var href = $(this).data('href');
                var object = $(this);
                $.ajax({
                    url: href,
                    type: 'GET',
                    success: function (data) {
                        object.parents('.subject-items').remove();
                        toastr.success("Đã xóa");
                    },
                    error: function () {
                        console.log('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
                    }
                });
            });
        });
    </script>
@endpush
