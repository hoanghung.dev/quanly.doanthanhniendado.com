<?php
$query = \Modules\EworkingProject\Models\Project::select('id','name')->where('id', $item->project_id);
?>
@if($query->exists())
    <?php
    $project = $query->first();
    ?>
    <a href="{{route('project.edit',['id'=>@$project->id])}}">{{@$project->name}}</a>
@endif

