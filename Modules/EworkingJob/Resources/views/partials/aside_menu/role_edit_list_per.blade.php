<li role="treeitem" aria-selected="false" aria-level="1" aria-labelledby="j3_1_anchor"
    aria-expanded="true" id="j3_1" class="jstree-node  jstree-open">
    <label style="cursor: pointer" for="loai_cv"
           class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
        <input style="height: 20px; width: 18px; float: left; margin-right: 5px;"
               type="checkbox"
               name="loai_cv"
               id="loai_cv"
               value="1">
        Loại công việc
        <span></span>
    </label>
    <ul role="group" class="jstree-children">
        <?php $jobTypes = \Modules\EworkingJob\Models\JobType::select(['id', 'name'])->get();?>
        @foreach($jobTypes as $v)
            <li role="treeitem" aria-selected="false" aria-level="2"
                aria-labelledby="j3_3_anchor" id="j3_3" class="jstree-node  jstree-leaf">
                <label style="cursor: pointer" for="p{{ $v['id'] }}"
                       class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
                    <input style="height: 20px; width: 18px; float: left; margin-right: 5px;"
                           type="checkbox"
                           name="{{ @$field['name'] }}_job_type[]"
                           id="p{{ $v['id'] }}"
                           value="{{ $v->id }}" {{ (in_array(@$result->id, explode('|', $v->role_ids))) ? 'checked' : '' }}>
                    Xem công việc {{ $v->name }}
                    <span></span>
                </label>
            </li>
        @endforeach
    </ul>
</li>