<div class="modal fade" id="popup-edit-general-job" role="dialog">
    <form id="form-edit-general-job" action="post" autocomplete="off">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content " style="height:auto">
                <div class="modal-header">
                    <h4 style="display:inline-block;" class="title-name">Thông tin chung</h4>
                    <button type="button" class="close" data-dismiss="modal"></button>
                </div>
                <div class="modal-content">

                    <div class="row" style="margin: 15px">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="usr">Tên công việc</label>
                                        <input type="text" name="name" id="name" class="form-control value">
                                        <input type="text" name="type" id="name" style="display: none">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="usr">Loại công việc</label>
                                        <?php
                                        $jobTypes = \Modules\EworkingJob\Models\JobType::where('company_id', \Auth::guard('admin')->user()->last_company_id)
                                            ->pluck('name', 'id');
                                        ?>
                                        <select class="form-control" name="job_type_id">
                                            <option value="">Chọn loại công việc</option>
                                            @foreach($jobTypes as $id => $name)
                                                <option value="{{ $id }}">{{ $name }}</option>
                                            @endforeach
                                        </select>
                                        <p class="form-text text-muted">
                                            Chưa có loại công việc này?
                                            <a href="/admin/job_type/add" target="_blank">Tạo mới</a>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="usr">Chọn dự án</label>
                                        <?php
                                        //
                                        $field = ['name' => 'project_id', 'type' => 'eworkingjob::form.select2_ajax_model', 'class' => 'required', 'label' => 'Chọn dự án',
                                            'where_this_company' => true, 'object' => 'project', 'model' => \Modules\EworkingJob\Models\Project::class, 'display_field' => 'name'];
                                        if (isset($project_name) && isset($project_id)) {
                                            $field['disabled'] = '';
                                        }
                                        ?>
                                        @include($field['type'], ['field' => $field])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="usr">Ghi chú công việc</label>
                                        <div class='input-group date' id='start_date'>
                                            <textarea name="intro" type="text" id="" class="value" cols="55"
                                                      rows="4"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit">
                        <i class="icon-header fa fa-check-circle"></i>
                        Hoàn thành
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function () {
        //them thong tin chung
        $('body').on('click', '.add-job', function (event) {
            event.preventDefault();
            let modal = $('#popup-edit-general-job');
            let form = modal.find('form');
            form.validate().resetForm();
            form[0].reset();
            @if (isset($project_name) && isset($project_id))
            modal.find('select[name=project_id]').select2('trigger', 'select', {
                data: {
                    id: '{{$project_id}}',
                    text: '{{$project_name}}',
                }
            });
            @endif
            modal.find('input[name=type]').val('add');
            modal.find('.title-name').text($(this).attr('title'));
            modal.modal();
        });
        //sửa thong tin chung
        $('body').on('click', '.edit-general', function () {
            let modal = $('#popup-edit-general-job');
            loading();
            let find_data = $('.kt-widget5__content.data-job');
            modal.find('select[name=project_id]').select2('trigger', 'select', {
                data: {
                    id: find_data.data('project_id'),
                    text: $('.job-project').text().trim(),
                }
            });
            modal.find('input[name=name]').val(find_data.data('name'));
            modal.find('select[name=job_type_id]').val(find_data.data('job_type_id'));
            modal.find('textarea[name=intro]').val(find_data.data('intro'));
            modal.find('input[name=type]').val('edit');
            modal.find('.title-name').text($(this).attr('title'));
            stopLoading();
            modal.modal();
        });

        $('#form-edit-general-job').validate({
            // onfocusout: false,
            // onkeyup: false,
            // onclick: false,
            rules: {
                name: {
                    required: true,
                },
                job_type_id: {
                    required: false,
                },
                project_id: {
                    required: false,
                },
            },
            messages: {
                // project_id: {
                //     required: "<p style='color: red'>Dự án không được để trống</p>",
                // },
                // job_type_id: {
                //     required: "<p style='color: red'>Loại không được để trống</p>",
                // },
                name: {
                    required: "<p style='color: red'>Tên công việc không được để trống</p>",
                },
            },
            submitHandler: function (form) {
                //code in her
                event.preventDefault();
                let modal = $('#popup-edit-general-job');
                if (modal.find('input[name=type]').val() == 'edit') {
                    $.ajax({
                        // url: '/' + location.pathname.substring(1),
                        url: '{{route('draft')}}',
                        type: 'POST',
                        data: {
                            name: modal.find('input[name=name]').val(),
                            job_type_id: modal.find('select[name=job_type_id]').val(),
                            project_id: modal.find('select[name=project_id]').val(),
                            intro: modal.find('textarea[name=intro]').val(),
                            job_id: '{{@$result->id}}',
                        },
                        success: function (resp) {
                            if (resp.status) {
                                window.location.reload();
                            } else {
                                toastr.error(resp.error[0]);
                            }
                        },
                        error: function () {
                            alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
                        }
                    });
                } else if (modal.find('input[name=type]').val() == 'add') {
                    $.ajax({
                        url: '{{route('job.add')}}',
                        type: 'POST',
                        data: {
                            name: modal.find('input[name=name]').val(),
                            job_type_id: modal.find('select[name=job_type_id]').val(),
                            project_id: modal.find('select[name=project_id]').val(),
                            intro: modal.find('textarea[name=intro]').val(),
                        },
                        success: function (resp) {
                            if (resp.status) {
                                window.location.reload();
                            } else {
                                toastr.error(resp.error[0]);
                            }
                        },
                        error: function () {
                            alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
                        }
                    });
                }
                return false;
            }
        });
    });
</script>


