<div class="modal fade" id="popup_add_subject" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content " style="height:auto">
            <div class="modal-header">
                <h4 style="display:inline-block;">Thêm bộ môn</h4>
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="modal-content">
                <div class="row" style="margin: 15px">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                $arrSubject = \Modules\EworkingJob\Models\Subject::where('type', 0)->where('company_id', Auth::guard('admin')->user()->last_company_id)->pluck('name', 'id')->toArray();
                                ?>
                                <select class="form-control" name="subject_id" id="subject_id">
                                    <option value="">Chọn bộ môn</option>
                                    @foreach($arrSubject as $id=>$name)
                                        <option value="{{$id}}">{{$name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-md-5">Nhiệm vụ thuộc bộ môn</label>
                            <div class="col-md-12">
                                <ul class="p-0" id="subject-tasks"></ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success text-light" id="add-subject">
                    <i class="icon-header fa fa-check-circle "></i>
                    Hoàn thành</a>
            </div>
        </div>
    </div>
</div>

<script>


    $(document).ready(function () {
        $('body').on('click', '.btn-insert-subject', function () {
            var modal = $('#popup_add_subject');
            modal.find('#name').val();
            $('#popup_add_subject').modal();
        });

        $('body').on('change', 'select[name=subject_id]', function () {
            var subject_id = $(this).val();
            if (subject_id != '') {
                $.ajax({
                    url: '{{ route('subject.get_html_popup_add_subject') }}',
                    type: 'GET',
                    data: {
                        subject_id: subject_id
                    },
                    success: function (resp) {
                        $('#subject-tasks').html(resp);
                    },
                    error: function () {
                        alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
                    }
                })
            }
        });
        //them bo mon vao cong viec
        $('body').on('click', '#add-subject', function () {
            let modal = $('#popup_add_subject');
            let subject_id = modal.find('select[name=subject_id]').val();
            let tasks = '';
            $('#subject-tasks .tasks:checked').each(function () {
                tasks = (tasks == '') ? tasks + $(this).val() : tasks + '|' + $(this).val();
            });
            $.ajax({
                url: '{{route('insert-subject')}}',
                type: 'GET',
                data: {
                    id: subject_id,
                    job_id: '{{$result->id}}',
                    task_ids: tasks,
                },
                success: function (resp) {
                    window.location.reload();
                    // $('#subject').append(resp);
                    // toastr.success('Thêm thành công !');
                    // $('#popup_add_subject .close').click();
                },
                error: function () {
                    alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
                }
            });
        });

    });
</script>
