<div class="modal fade" id="popup-add-task" role="dialog">
    <form id="form-task" action="post" autocomplete="off">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content " style="height:auto">
                <div class="modal-header">
                    <h4 class="title-popup" style="display:inline-block;">Thêm nhiệm vụ</h4>
                    <button type="button" class="close" data-dismiss="modal"></button>
                </div>
                <div class="modal-content">

                    <div class="row" style="margin: 15px">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="usr">Tên nhiệm vụ</label>
                                        <input type="text" name="subject_name" id="name" class="form-control">
                                        <input type="text" id="id-task" class="hidden">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="usr">Ngày bắt đầu</label>
                                        <div class='input-group date' id='start_date'>
                                            <input type='text' name="start_date" class="form-control datetimepicker"/>
                                            <span class="input-group-addon select-datetimepicker">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <input type='text' id="status" style="display:none;"/>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="usr">Ngày hết hạn</label>
                                        <div class='input-group date' id='end_date'>
                                            <input type='text' name="end_date" class="form-control datetimepicker"/>
                                            <span class="input-group-addon select-datetimepicker">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="form-group">--}}
                            {{--<label for="usr">Mô tả</label>--}}
                            {{--<textarea name="content" id="edit-texarea" cols="30" rows="3"></textarea>--}}
                            {{--</div>--}}
                            <?php
                            $arr = \App\Models\RoleAdmin::where('company_id', Auth::guard('admin')->user()->last_company_id)->where('status', 1)->get();
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="usr">Người làm</label>
                                        <select style="width: 100%" class="form-control admin_ids select2-admin_ids"
                                                id="admin_ids"
                                                name="admin_ids" multiple>
                                            @foreach ($arr as $value)
                                                <option value='{{ @$value->admin_id }}'>{{ @$value->admin->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit">
                        <i class="icon-header fa fa-check-circle"></i>
                        Hoàn thành
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function () {

        $('body').on('click', '.btn-insert-task', function () {
            var modal = $('#popup-add-task');
            modal.find('.title-popup').text($(this).attr('title'));
            modal.data('type', 'insert');
            modal.data('type-task', $(this).data('type-task'));
            getModal(modal, $(this));
        });

        $('body').on('click', '.btn-add-task', function () {
            var modal = $('#popup-add-task');
            modal.find('.title-popup').text($(this).attr('title'));
            modal.data('type', 'add');
            modal.data('type-task', 'single-add-task-draft');
            getModal(modal, $(this));
        });

        function getModal(modal, obj) {
            var form = $('#form-task');
            form.validate().resetForm();
            form[0].reset();
            modal.find('#admin_ids option').each(function () {
                $(this).prop("selected", false);
            });
            // $('.error.invalid-feedback').remove();
            // $('input.is-invalid').removeClass('is-invalid');
            // console.log(obj.data('sub-id'));
            // modal.find('#name').val(null);
            // modal.find('#status').val(0);
            // modal.find('#edit-texarea').val(null);
            modal.find("input[name='start_date']").val('{{date('d-m-Y')}}').datepicker({format: "dd-mm-yyyy",});
            modal.find("input[name='end_date']").val('{{date('d-m-Y')}}').datepicker({format: "dd-mm-yyyy",});
            // modal.find('#admin_ids').val('');
            modal.find('#id-task').val('');
            modal.data('sub-id', obj.data('sub-id'));
            modal.data('type-task', obj.data('type-task'));
            modal.modal();
            $('#admin_ids').select2();

        }


        // form popup
        $('#form-task').validate({
            onfocusout: false,
            onkeyup: false,
            onclick: false,
            rules: {
                subject_name: {
                    required: true,
                },
                start_date: {
                    required: true,
                },
                admin_ids: {
                    required: true,
                },
                // status: {
                //     required: true,
                // }
            },
            messages: {
                subject_name: {
                    required: "<p style='color: red'>Têm nhiệm vụ không được để trống</p>",
                },
                start_date: {
                    required: "<p style='color: red'>Ngày bắt đầu không được để trống</p>",
                },
                // status: {
                //     required: "<p style='color: red'>Trạng thái không được để trống</p>",
                // },
                admin_ids: {
                    required: "<p style='color: red'>Người thực hiện công việc không được để trống</p>",
                },
            },
            submitHandler: function (form) {
                //code in here
                event.preventDefault();
                var url = '/admin/job/post-edit-task';
                var modal = $('#popup-add-task');
                console.log(modal.data('sub-id'));
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        name: modal.find('#name').val(),
                        // status: modal.find('#status').val(),
                        // contend: modal.find('#edit-texarea').val(),
                        start_date: modal.find('#start_date input').val(),
                        end_date: modal.find('#end_date input').val(),
                        admin_ids: modal.find('#admin_ids').val(),
                        type: modal.data('type'),
                        subject_id: modal.data('sub-id'),
                        task_id: modal.find('#id-task').val(),
                        job_id: '{{$result->id}}',
                        type_task: modal.data('type-task'),
                        status: modal.find('#status').val(),
                    },
                    success: function (data) {
                        if (data.status) {
                            window.location.reload();
                            /*toastr.success("cập nhật thành công!");
                            $('#dialog-edit .close').click();*/
                        } else {
                            toastr.error("Thất bại!");
                        }
                    }
                });
            }
        });
    });
</script>
