<div class="subject-{{$subject->id}} kt-portlet kt-portlet--collapse" style="border: 1px solid;" data-ktportlet="true"
     id="kt_portlet_tools_1">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                {{$subject->name}}
            </h3>
            <span style="margin-left: 10px;">( Có {{count($subject->tasks)}} nhiệm vụ )</span>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-group">
                <a data-sub-id="{{$subject->id}}" title="Thêm nhiệm vụ vào bộ môn"
                   data-type-task="main-add-task-subject-draft"
                   class="btn btn-sm btn-icon btn-clean btn-icon-md btn-insert-task"><i
                            class="flaticon2-add"></i></a>
                <a data-id="{{$subject->id}}" title="Xem bộ môn"
                   class="btn btn-sm btn-icon btn-clean btn-icon-md get-subject"><i
                            class="flaticon2-down"></i></a>
                <?php
                $draft = \Modules\EworkingJob\Models\Job::find($subject->job_id);
                ?>
                <a title="Xóa bộ môn"
                   href="{{URL::to('/admin/job/post-edit-task?subject_id='.$subject->id.'&type_task=main-delete-subject&job_id='.$job_id)}}"
                   class="btn btn-sm btn-icon btn-clean btn-icon-md delete-task"><i
                            class="flaticon2-cancel"></i></a>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        @include('eworkingjob::task.task',['tasks'=>$subject->tasks])
    </div>
</div>
