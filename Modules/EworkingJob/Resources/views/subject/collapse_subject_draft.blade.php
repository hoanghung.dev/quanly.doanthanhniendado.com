<div class="subject-{{$subject->id}} kt-portlet kt-portlet--collapse" style="border: 1px solid;" data-ktportlet="true"
     id="kt_portlet_tools_1">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                <span class="kt-badge kt-badge--danger kt-badge--inline text-light">Draft</span>
                {{$subject->name}}
            </h3>
            <span style="margin-left: 10px;">( Có {{$subject->data != null ? count($subject->data) : 0}} nhiệm vụ )</span>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-group">
                <a data-sub-id="{{$subject->id}}"
                   data-type-task="add-task-subject"
                   title="Thêm nhiệm vụ vào bộ môn {{$subject->name}}"
                   class="btn btn-sm btn-icon btn-clean btn-icon-md btn-insert-task"><i
                            class="flaticon2-add"></i></a>
                <a title="Xem bộ môn"
                   class="btn btn-sm btn-icon btn-clean btn-icon-md get-subject"><i
                            class="flaticon2-down"></i></a>
                <a title="Xóa bộ môn"
                   href="{{URL::to('/admin/job/post-edit-task?subject_id='.$subject->id.'&type_task=draft-delete-subject&job_id='.$job_id)}}"
                   class="btn btn-sm btn-icon btn-clean btn-icon-md delete-task"><i
                            class="flaticon2-cancel"></i></a>
            </div>
        </div>
    </div>
    {{--{{dd()}}--}}
    <div class="kt-portlet__body">
        @if($subject->data != null)
            @include('eworkingjob::task.task_draft',['tasks'=>$subject->data])
        @endif
    </div>
</div>
