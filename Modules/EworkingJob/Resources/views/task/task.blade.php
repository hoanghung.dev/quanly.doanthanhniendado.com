<div class="kt-portlet__content">
    <div class="tab-content">
        <div class="tab-pane active" id="kt_widget11_tab1_content">
            <!--begin::Widget 11-->
            <div class="kt-widget11">
                <div class="table-responsive">
                    <table class="table">
                        <tbody class="body">
                        <?php
                        $status_draft = false;
                        if (isset($task_draft_in_subject) && $task_draft_in_subject != null && array_key_exists($subject->id, $task_draft_in_subject)) {
                            $main = 'main-';
                            $tasks_draft = $task_draft_in_subject[$subject->id];
                            $status_draft = true;
                        }
                        ?>
                        @if ( $status_draft)
                            @include('eworkingjob::task.task_draft_item',['tasks'=>$tasks_draft])
                        @endif
                        @include('eworkingjob::task.task_main_item',['main'=>'main-','tasks'=>$tasks])
                        </tbody>
                    </table>
                </div>
                {{--<div class="kt-widget11__action kt-align-right">--}}
                {{--<button type="button" class="btn btn-label-success btn-sm btn-bold">View All Records</button>--}}
                {{--</div>--}}
            </div>
            <!--end::Widget 11-->
        </div>
    </div>
</div>
