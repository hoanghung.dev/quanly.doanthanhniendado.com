<div class="kt-portlet__content">
    <div class="tab-content">
        <div class="tab-pane active" id="kt_widget11_tab1_content">
            <!--begin::Widget 11-->
            <div class="kt-widget11">
                <div class="table-responsive">
                    <table class="table">
                        <tbody class="body">
                        @if(count($tasks) > 0)
                            @foreach($tasks as $key=>$task)
                                {{--{{dd($tasks)}}--}}
                                <tr class="task-{{@$task->id}}" style="border: 1px dashed #5867dd;">
                                    <td>
                                        <span class="kt-widget11__title"><span
                                                    class="kt-badge kt-badge--danger kt-badge--inline text-light">Draft</span> {{$task->name}}</span>
                                        <span class="kt-widget11__sub">
                                                        <span class="kt-font-info">{{$task->start_date != null ? date('d-m-Y',strtotime($task->start_date)) : 'Ngày bắt đầu'}}</span> -
                                                            <span
                                                                    class="kt-font-info">{{$task->end_date != null ? date('d-m-Y',strtotime($task->end_date)) : 'Ngày kết thúc'}}
                                                            </span>
                                                        </span>
                                    </td>
                                    <td>
                                        @include(config('core.admin_theme').'.list.td.admins', ['admin_ids' => $task->admin_ids])
                                    </td>

                                    <td>
                                                    <span class="kt-badge @if($task->status == 0) kt-badge--success @else kt-badge--warning @endif kt-badge--inline">@if($task->status == 0)
                                                            Hoàn thành @else Đang làm @endif</span>
                                    </td>
                                    <td class="kt-align-right kt-font-brand kt-font-bold">
                                        <div class="kt-widget2__actions">
                                            <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md"
                                               data-toggle="dropdown">
                                                <i class="flaticon-more-1"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                                <ul class="kt-nav">
                                                    @if(in_array(Auth::guard('admin')->user()->id,explode('|',@$task->admin_ids )))
                                                        <li class="kt-nav__item">
                                                            <a href="{{URL::to('/admin/job/post-edit-task?task_id='.$task->id.'&type_task=finish-task'.@$type_edit.'&job_id='.$job_id.'&subject_id='.@$subject_id)}}"
                                                               data-type-finish="{{ ($task->status == 0) ? '1' : '0' }}"
                                                               class="kt-nav__link finish-task">
                                                                <i class="kt-nav__link-icon flaticon2-checkmark"></i>
                                                                <span class="kt-nav__link-text">
                                                                {{ ($task->status == 0) ? 'Hủy hoàn thành' : 'Hoàn thành' }}
                                                            </span>
                                                            </a>
                                                        </li>
                                                    @endif
                                                    <li class="kt-nav__item"
                                                        data-type-task="edit-task{{@$type_edit}}"
                                                        data-status="{{@$task->status}}"
                                                        data-name="{{$task->name}}"
                                                        data-admin_ids="{{$task->admin_ids}}"
                                                        data-start_date="{{$task->start_date != null ? date('d-m-Y',strtotime($task->start_date)) : date('d-m-Y')}}"
                                                        data-end_date="{{$task->end_date != null ? date('d-m-Y',strtotime($task->end_date)) : date('d-m-Y') }}"
                                                        onclick="getTask($(this))"
                                                        data-id="{{@$task->id}}"
                                                        data-sub-id="{{@$subject_id}}"
                                                        title="Sửa nhiệm vụ">
                                                        <a class="kt-nav__link">
                                                            <i class="kt-nav__link-icon flaticon2-edit"></i>
                                                            <span class="kt-nav__link-text">Sửa</span>
                                                        </a>
                                                    </li>
                                                    <li class="kt-nav__item">
                                                        <a href="{{URL::to('/admin/job/post-edit-task?task_id='.$task->id.'&type_task=delete-task'.@$type_edit.'&job_id='.$job_id.'&subject_id='.@$subject_id)}}"
                                                           class="kt-nav__link delete-task">
                                                            <i class="kt-nav__link-icon flaticon2-delete"></i>
                                                            <span class="kt-nav__link-text">Xóa</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!--end::Widget 11-->
        </div>
    </div>
</div>
