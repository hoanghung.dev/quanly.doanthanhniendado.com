<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//C:\xampp\htdocs\eworking\Modules\EworkingJob\Http\Controllers\Admin\JobController.php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'job'], function () {
        Route::get('', 'Admin\JobController@getIndex')->name('job')->middleware('permission:job_view');
        Route::get('publish', 'Admin\JobController@getPublish')->name('job.publish')->middleware('permission:job_publish');
        Route::post('add', 'Admin\JobController@add')->middleware('permission:job_add')->name('job.add');
//        Route::match(array('GET', 'POST'), '{id}/draft', 'Admin\JobController@draft')->name('job.draft');
        Route::get('delete/{id}', 'Admin\JobController@delete')->middleware('permission:job_delete');
        Route::post('multi-delete', 'Admin\JobController@multiDelete')->middleware('permission:job_delete');
        Route::get('search-for-select2', 'Admin\JobController@searchForSelect2')->name('job.search_for_select2')->middleware('permission:job_view');

        Route::post('ajax-update', 'Admin\JobController@ajaxUpdate');
        Route::get('insert-subject', 'Admin\SubjectController@insertSubject')->name('insert-subject');
        Route::post('post-edit-task', 'Admin\TaskController@postEditTask')->name('post-edit-task');
        Route::post('edit-single', 'Admin\TaskController@addSingleAdmin')->name('edit-single');

        Route::get('get-by-id/{id}', 'Admin\JobController@getByIdProject')->name('job.get-by-id');
//        draft ganeral
        Route::post('draft-ganeral', 'Admin\JobController@draftGeneral')->name('draft');
        Route::get('{id}', 'Admin\JobController@update')->middleware('permission:job_view')->name('job.edit');
        Route::post('{id}', 'Admin\JobController@update')->middleware('permission:job_edit');
    });

    Route::group(['prefix' => 'job_type'], function () {
        Route::get('', 'Admin\JobTypeController@getIndex')->name('job_type')->middleware('permission:job_view');
        Route::match(array('GET', 'POST'), 'add', 'Admin\JobTypeController@add')->middleware('permission:job_view');
        Route::get('delete/{id}', 'Admin\JobTypeController@delete')->middleware('permission:job_view');
        Route::post('multi-delete', 'Admin\JobTypeController@multiDelete')->middleware('permission:job_view');
        Route::get('search-for-select2', 'Admin\JobTypeController@searchForSelect2')->name('job_type.search_for_select2')->middleware('permission:job_view');
        Route::get('{id}', 'Admin\JobTypeController@update')->middleware('permission:job_view');
        Route::post('{id}', 'Admin\JobTypeController@update')->middleware('permission:job_view');
    });


    Route::group(['prefix' => 'subject'], function () {
        Route::get('', 'Admin\SubjectController@getIndex')->name('subject')->middleware('permission:subject_view');
        Route::get('publish', 'Admin\SubjectController@getPublish')->name('subject.publish')->middleware('permission:subject_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\SubjectController@add')->middleware('permission:subject_add');
        Route::get('delete/{id}', 'Admin\SubjectController@delete')->middleware('permission:subject_delete');
        Route::post('multi-delete', 'Admin\SubjectController@multiDelete')->middleware('permission:subject_delete');

        Route::get('search-for-select2', 'Admin\SubjectController@searchForSelect2')->name('subject.search_for_select2')->middleware('permission:subject_view');

        Route::get('get_html_popup_add_subject', 'Admin\JobController@get_html_popup_add_subject')->name('subject.get_html_popup_add_subject');

        Route::get('add-ajax', 'Admin\SubjectController@get_html_popup_add_subject')->name('subject.get_html_popup_add_subject');

        Route::get('get-ajax', 'Admin\TaskController@getTaskInSubject')->name('subject.get-task');
        Route::get('get-by-id/{id}', 'Admin\SubjectController@getByJobID')->name('subject.get-by-id');
        Route::get('{id}', 'Admin\SubjectController@update')->middleware('permission:subject_view');
        Route::post('{id}', 'Admin\SubjectController@update')->middleware('permission:subject_edit');
    });

    Route::group(['prefix' => 'subject_warehouse'], function () {
        Route::get('', 'Admin\SubjectWarehouseController@getIndex')->name('subject_warehouse')->middleware('permission:subject_warehouse_view');
        Route::match(array('GET', 'POST'), 'add', 'Admin\SubjectWarehouseController@add')->middleware('permission:subject_warehouse_add');
        Route::get('delete/{id}', 'Admin\SubjectWarehouseController@delete')->middleware('permission:subject_warehouse_delete');
        Route::post('multi-delete', 'Admin\SubjectWarehouseController@multiDelete')->middleware('permission:subject_warehouse_delete');
        Route::get('{id}', 'Admin\SubjectWarehouseController@update')->middleware('permission:subject_warehouse_view');
        Route::post('{id}', 'Admin\SubjectWarehouseController@update')->middleware('permission:subject_warehouse_edit');
    });

    Route::group(['prefix' => 'task'], function () {
        Route::get('', 'Admin\TaskController@getIndex')->name('task')->middleware('permission:task_view');
        Route::get('publish', 'Admin\TaskController@getPublish')->name('task.publish')->middleware('permission:task_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TaskController@add')->middleware('permission:task_add');
        Route::get('delete/{id}', 'Admin\TaskController@delete')->middleware('permission:task_delete');
        Route::post('multi-delete', 'Admin\TaskController@multiDelete')->middleware('permission:task_delete');
        Route::get('search-for-select2', 'Admin\TaskController@searchForSelect2')->name('task.search_for_select2')->middleware('permission:task_view');
        Route::get('update-status/{id}', 'Admin\TaskController@updateStatus')->name('task.update-status');
        Route::get('get-by-id/{id}', 'Admin\TaskController@getByJobID')->name('task.get-by-id');
        Route::get('{id}', 'Admin\TaskController@update')->middleware('permission:task_view');
        Route::post('{id}', 'Admin\TaskController@update')->middleware('permission:task_edit');
    });


    Route::get('edit-comment', 'Admin\CommentController@getEditComment')->name('edit-comment');
    Route::get('delete-comment', 'Admin\CommentController@getDeleteComment')->name('delete-comment');


    Route::get('list-admin-ajax', 'Admin\CommentController@listAdminAjax');
    Route::get('read', 'Admin\NotificationController@read')->name('read');
    Route::get('get-attr-service', 'Admin\InviteCompanyController@getAttrService');
    Route::get('get-tag', 'Admin\CommentController@getTag')->name('get-tag');
    Route::get('get-task', 'Admin\TaskController@getTask')->name('get-task');

    Route::get('comment', 'Admin\CommentController@commentJob')->name('commentJob');
    Route::post('comment-img', 'Admin\CommentController@commentImg')->name('commentImg');
    Route::get('get-chat-job', 'Admin\CommentController@getChatJob')->name('get-chat-job');

});
