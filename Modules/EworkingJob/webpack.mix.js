const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js([
    __dirname + '/Resources/assets/css/custom.css'
], 'js/custom.js')
    .styles([
            __dirname + '/Resources/assets/Resources/assets/js/custom.js']
        , 'css/custom.css');

if (mix.inProduction()) {
    mix.version();
}
