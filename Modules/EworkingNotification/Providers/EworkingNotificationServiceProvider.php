<?php

namespace Modules\EworkingNotification\Providers;

use App\Models\Setting;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Modules\EworkingNotification\Http\Controllers\EworkingNotificationController;
use Modules\ThemeSemicolonwebJdes\Models\Notifications;

class EworkingNotificationServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Cấu hình header
            $this->rendHeaderTopbar();

            //  Cấu hình dịch vụ trong setting
            $this->addSettingService();

            $this->configPusher();

            $this->rendOnesignalScript();
        }

        \Eventy::addAction('notification.add', function ($notiData) {
            Notifications::create($notiData);

            $eworkingNotificationController = new EworkingNotificationController();
//            $eworkingNotificationController->pushToPusher($notiData);
            $eworkingNotificationController->pushToAppMobile($notiData);
            return true;
        }, 1, 1);
    }

    public function rendOnesignalScript() {
        \Eventy::addFilter('head_script.script', function () {
            print view('eworkingnotification::partials.head_script');
        }, 1, 1);

        \Eventy::addFilter('footer_script.script', function () {
            print view('eworkingnotification::partials.footer_script');
        }, 1, 1);
    }

    public function rendHeaderTopbar()
    {
        \Eventy::addFilter('block.header_topbar', function () {
            print view('eworkingnotification::partials.header_topbar_notification');
        }, 2, 1);
    }

    public function addSettingService()
    {
        \Eventy::addFilter('setting.custom_module', function ($module) {
            $module['tabs']['pusher'] = [
                'label' => 'Cấu hình pusher',
                'icon' => '<i class="flaticon-mail"></i>',
                'intro' => '',
                'td' => [
                    ['name' => 'PUSHER_APP_ID', 'type' => 'text', 'label' => 'ID Pusher'],
                    ['name' => 'PUSHER_APP_KEY', 'type' => 'text', 'label' => 'Key Pusher'],
                    ['name' => 'PUSHER_APP_SECRET', 'type' => 'text', 'label' => 'Secret Pusher'],
                    ['name' => 'PUSHER_APP_CLUSTER', 'type' => 'text', 'label' => 'Cluster'],
                ]
            ];
            return $module;
        }, 1, 1);
    }

    public function configPusher()
    {
        $settings = Setting::whereIn('type', ['pusher'])->pluck('value', 'name')->toArray();
        if (count($settings) > 0) {
            config(['pusher.connections' => [
                'main' => [
                    'auth_key' => $settings['PUSHER_APP_KEY'],
                    'secret' => $settings['PUSHER_APP_SECRET'],
                    'app_id' => $settings['PUSHER_APP_ID'],
                    'options' => [
                        'cluster' => $settings['PUSHER_APP_CLUSTER']
                    ],
                    'host' => null,
                    'port' => null,
                    'timeout' => null,
                ],

                'alternative' => [
                    'auth_key' => 'your-auth-key',
                    'secret' => 'your-secret',
                    'app_id' => 'your-app-id',
                    'options' => [],
                    'host' => null,
                    'port' => null,
                    'timeout' => null,
                ],
            ]]);
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('eworkingnotification.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php', 'eworkingnotification'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/eworkingnotification');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/eworkingnotification';
        }, \Config::get('view.paths')), [$sourcePath]), 'eworkingnotification');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/eworkingnotification');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'eworkingnotification');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'eworkingnotification');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
