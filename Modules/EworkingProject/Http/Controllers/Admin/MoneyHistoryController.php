<?php

/**
 * Widget Controller
 *
 * Widget Controller manages Widget by admin.
 *
 * @category   Widget
 * @package    hobasoft
 * @author     hobasoft.com
 * @copyright  2018 hobasoft.com
 * @license
 * @version    1.3
 * @link       http://hobasoft.com
 * @email      webhobasoft@gmail.com
 * @since      Version 1.0
 * @deprecated None
 */

namespace Modules\EworkingProject\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use \Modules\EworkingProject\Models\MoneyHistory;
use Symfony\Component\HttpFoundation\Request;

class MoneyHistoryController extends CURDBaseController
{

    protected $module = [
        'code' => 'money_history',
        'table_name' => 'money_history',
        'icon' => '<i class="kt-font-brand flaticon2-avatar"></i>',
        'label' => 'Lịch sử giao dịch',
        'modal' => 'Modules\EworkingProject\Models\MoneyHistory',
        'list' => [
            'view' => 'list_common.child',
            'td' => [
                ['name' => 'created_at', 'type' => 'datetime_vi', 'label' => 'Ngày'],
                ['name' => 'id', 'type' => 'text_edit', 'label' => 'Số GD'],
                ['name' => 'intro', 'type' => 'text', 'label' => 'Diễn giải'],
                ['name' => 'debit', 'type' => 'price', 'label' => 'Ghi nợ'],
                ['name' => 'credited', 'type' => 'price', 'label' => 'Ghi có'],
                ['name' => 'surplus_vi', 'type' => 'price', 'label' => 'Số dư'],
            ],
        ],
        'form' => [
            'view' => 'form_common.simple_blank',        //  simple | tab | multi_box
            'tabs' => [
                'general_tab' => [
                    'label' => 'Thông tin chung',
                    'td' => [
//                        ['name' => 'intro', 'type' => 'textarea', 'class' => 'required', 'label' => 'Diễn giải'],
                        ['name' => 'debit', 'type' => 'number', 'label' => 'Ghi nợ'],
                        ['name' => 'credited', 'type' => 'number', 'label' => 'Ghi có'],
                        ['name' => 'sub_partner', 'type' => 'select', 'label' => 'Thầu phụ', 'options' => [
                            'Thầu A',
                            'Thầu A',
                            'Thầu A',
                        ]],
                        ['name' => 'date', 'type' => 'date', 'label' => 'Ngày thực tế'],
                    ],
                ],
            ],
        ]
    ];

    protected $filter = [
        'intro' => [
            'label' => 'Số GD',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'debit' => [
            'label' => 'Ghi nợ',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'credited' => [
            'label' => 'Ghi có',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'from_to_date' => [
            'label' => 'Ngày',
            'type' => 'from_to_date',
            'query_type' => 'from_to_date'
        ],
    ];

    public function appendWhere($query, $request)
    {
        $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
        return $query;
    }

    public function addReturn($request)
    {
        if ($request->has('project_id')) {
            return redirect('admin/' . $this->module['code'] . '?project_id=' . $request->project_id);
        } else {
            return redirect('admin/' . $this->module['code']);
        }
    }

    public function ajaxUpdate(Request $request)
    {
        $date = date('Y-m-d', strtotime(str_replace(strstr($request->date, ' '), '', $request->date)));
        $moneyHistory = MoneyHistory::updateOrCreate(
            [
                'id' => $request->id,
                'project_id' => $request->project_id
            ],
            [
                'value' => $request->value,
                'type' => $request->type,
                'sub_partner' => $request->sub_partner,
                'date' => $date,
                'admin_id' => $request->admin_id,
                'intro' => $request->intro,
            ]);
        $this->updateMoneyProject($moneyHistory->project_id);
        return response()->json([
            'status' => true,
            'data' => $moneyHistory
        ]);
    }
    
    public function updateMoneyProject($project_id) {

        return true;
    }

    public function afterAdd($request, $item)
    {
        $this->countingBalance($request, $item);
    }

    public function afterUpdate($request, $item)
    {
        $this->countingBalance($request, $item);
    }

    public function beforeDelete($request, $item = false, $ids = false)
    {
        //  Trước khi xóa bản ghi thi tính toán lại số dư. Số dư sẽ không được tính bản ghi đó vào
        if ($item) {
            $ids[] = $item->id;
        }
        foreach ($ids as $id) {
            //  update lại tiền tất cả các id sau của id bị xóa
            $next_ids = $this->model->where('id', '>', $id)->get();
            foreach ($next_ids as $next) {
                $this->countingBalance($request, $next);
            }
        }
        return true;
    }

    /*
     * Tính toán lại số dư cho bản ghi $item
     * */
    public function countingBalance($request, $item)
    {
        if ($this->model->where('project_id', $item->project_id)->count() == 1) {    //  Nếu là bản ghi đầu tiên thì lấy số dư là số hiện tại
            if ($item->credited != 0) { //  Cộng tiền
                $item->surplus_vi = $item->credited;
            } else {
                $item->surplus_vi = 0 - $item->debit;
            }
            $item->save();
            return true;
        }

        $credited = $this->model->where('project_id', $item->project_id)->where('id', '<=', $item->id)->sum('credited');
        $debit = $this->model->where('project_id', $item->project_id)->where('id', '<=', $item->id)->sum('debit');
        $item->surplus_vi = (int)$credited - (int)$debit;
        $item->save();
        return true;
    }

    public function del(Request $request)
    {
        MoneyHistory::destroy($request->id);
        CommonHelper::one_time_message('success', 'Xóa thành công!');
        return back();
    }

    public function getByIdProject($projet_id)
    {
        $data['money_historys'] = MoneyHistory::where('project_id', $projet_id)->paginate(5);
        $data['subPartners'] = \Modules\EworkingProject\Models\SubPartner::pluck('name', 'id')->toArray();
        return view('eworkingproject::form.fields.lich_su_tien')->with($data);
    }
}
