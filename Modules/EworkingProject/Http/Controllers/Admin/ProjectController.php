<?php

namespace Modules\EworkingProject\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\EworkingProject\Models\Project;
use Validator;

class ProjectController extends CURDBaseController
{
    protected $_base;

    public function __construct()
    {
        parent::__construct();
        $this->_base = new BaseController();
    }

    protected $module = [
        'code' => 'project',
        'table_name' => 'projects',
        'label' => 'Dự án',
        'modal' => '\Modules\EworkingProject\Models\Project',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên', 'sort' => true],
            ['name' => 'user_id', 'type' => 'relation', 'label' => 'Khách hàng', 'object' => 'user', 'display_field' => 'short_name'],
            ['name' => 'common_total_vi', 'type' => 'price_vi', 'label' => 'Tổng $', 'sort' => true],
            ['name' => 'common_paid', 'type' => 'custom', 'td' => 'eworkingproject::list.td.common_paid', 'label' => 'Đã trả'],
            ['name' => 'common_rest', 'type' => 'custom', 'td' => 'eworkingproject::list.td.common_rest', 'label' => 'Còn thiếu'],
            ['name' => 'progress', 'type' => 'custom', 'td' => 'eworkingproject::list.td.progress', 'label' => 'Tiền dự án',],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên dự án'],
                ['name' => 'common_total_vi', 'type' => 'text', 'class' => 'number_price', 'label' => 'Tổng tiền'],
//                        ['name' => 'common_paid_vi', 'type' => 'text', 'class' => 'number_price', 'inner' => 'disabled',
//                            'hidden_input' => true, 'label' => 'Đã trả'],
//                        ['name' => 'common_rest_vi', 'type' => 'text', 'class' => 'number_price', 'inner' => 'disabled',
//                            'hidden_input' => true, 'label' => 'Còn thiếu'],
//                        ['name' => 'status', 'type' => 'select', 'label' => 'Trạng thái', 'options' => [
//                            1 => 'Đang làm',
//                            2 => 'Hoàn thành',
//                            3 => 'Hủy'
//                        ]],
//                        ['name' => 'contract_date', 'type' => 'datetimepicker', 'label' => 'Ngày ký hợp đồng'],
                ['name' => 'user_id', 'type' => 'custom', 'field' => 'eworkingproject::form.fields.user_id', 'label' => 'Đối tác', 'model' => \Modules\EworkingUser\Models\User::class, 'display_field' => 'short_name'],
                ['name' => 'intro', 'type' => 'textarea', 'label' => 'Ghi chú'],
            ],

            'job_tab' => [
                ['name' => 'cong_viec', 'view' => 'custom', 'type' => 'eworkingproject::form.fields.cong_viec'],
//                        ['name' => 'iframe', 'type' => 'iframe', 'label' => 'Lịch sử tiền', 'src' => '/admin/job?view=blank&project_id={id}', 'inner' => 'style="height:510px"'],
            ],
            'history_money' => [
                ['name' => 'progess', 'type' => 'eworkingproject::form.fields.progress', 'view' => 'custom', 'label' => 'Số tiền đã nhận/ tổng tiền dự án',
                    'data' => [
                        'value' => 'common_paid_vi',
                        'total' => 'common_total_vi'
                    ]],
                ['name' => 'lich_su_tien', 'view' => 'custom', 'type' => 'eworkingproject::form.fields.lich_su_tien', 'label' => ''],
            ],
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên',
            'type' => 'text',
            'query_type' => 'like'
        ],
    ];

    protected $listStatus = [
        1 => 'Đang làm',
        2 => 'Hoàn thành',
        3 => 'Hủy'
    ];

    public function add(Request $request)
    {
        if ($request->ajax()) {
            $item = new Project();
            $item->company_id = Auth::guard('admin')->user()->last_company_id;
            $item->name = $request->name;
            $item->intro = $request->intro;
            $item->common_total_vi = $request->common_total_vi;
            $item->user_id = $request->user_id;
            if ($item->save()) {
                CommonHelper::one_time_message('success', 'Tạo thành công!');
//                if ($request->ajax()) {
                return response()->json([
                    'status' => true,
                    'link' => route('project.edit', $item->id)
                ]);
//                }
            } else {
                CommonHelper::one_time_message('success', 'Tạo dự án thất bại!');
//                if ($request->ajax()) {
                return response()->json([
                    'status' => false
                ]);
//                }
            }
        }
//        return redirect('/admin/' . $this->module['code'] . '/' . $item->id);
    }

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('eworkingproject::list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
        $query = $query->where('name', '!=', '');
        return $query;
    }

    public
    function draftGeneral(Request $request)
    {
        if (!$request->ajax()) abort(404);
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ], [
            'name.required' => 'Bắt buộc phải nhập tiêu đề',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'error' => $validator->errors()->all(),
            ]);
        } else {
            $data = $request->all();

            $data['job_type_name'] = $data['job_type_id'] == null ? null : JobType::find($data['job_type_id'])->name;
            $data['project_name'] = $data['project_id'] == null ? null : Project::find($data['project_id'])->name;
            $dir_name = config('eworkingjob.job_data_draf') . $data['job_id'];
            $filename = $dir_name . '/draft.txt';

            $message_data = [];
            if (file_exists($filename)) { //kiem tra xem file ton tai thi lay ra content
                $message_data = (array)json_decode(file_get_contents($filename));
            }

            $exists_general = 0;
            foreach ($message_data as $data_draft) {
                if (isset($data_draft->general)) { //neu co obj general roi thi luu thay doi
                    $data_draft->general = (object)$data;
                    $exists_general = 1;
                }
            }

            if ($exists_general == 0) {
                $draft['general'] = $data;
//            them moi data
                $this->_base->logsFileText($dir_name, $filename, $data = $draft);
            } else {
//            luu thay doi
                file_put_contents($filename, json_encode($message_data));
            }

            return response()->json([
                'status' => true,
                'data' => $data
            ]);
        }
    }


    public function update(Request $request)
    {
        try {
            $item = $this->model->find($request->id);
            //  Chỉ sửa được liệu công ty mình đang vào
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
                return back();
            }
            if (!is_object($item)) abort(404);
            if (!$_POST) {

                $data = $this->getDataUpdate($request, $item);
                return view('eworkingproject::edit')->with($data);
            } else if ($_POST) {
                if ($request->ajax()) {
                    $validator = Validator::make($request->all(), [
                        'name' => 'required'
                    ], [
                        'name.required' => 'Bắt buộc phải nhập tên',
                    ]);

                    if ($validator->fails()) {

                        return response()->json([
                            'status' => false,
                            'error' => $validator->errors()->all(),
                        ]);
                    }
                }

//                $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//
//                unset($data['thau_phu']);
//                unset($data['progess']);
//                unset($data['cong_viec']);
//                unset($data['lich_su_tien']);
//                #
//                foreach ($data as $k => $v) {
//                    $item->$k = $v;
//                }
//
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {
            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xuất bản!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
                return back();
            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {

            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
}
