<?php

namespace Modules\EworkingProject\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\EworkingAdmin\Models\Admin;
use Modules\EworkingJob\Models\Job;
use Modules\EworkingJob\Models\Task;
use Modules\EworkingProject\Models\MoneyHistory;
use Modules\EworkingProject\Models\Project;

class ProjectController extends Controller
{

    protected $module = [
        'code' => 'project',
        'table_name' => 'projects',
        'label' => 'Dự án',
        'modal' => 'Modules\EworkingProject\Models\Project',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'company_id' => [
            'query_type' => 'like',
        ],
    ];

    public function index(Request $request)
    {
        try {
            //  Check permission
            if (!CommonHelper::has_permission(\Auth::guard('api')->id(), 'project_view')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không đủ quyền'
                        ]
                    ],
                    'data' => null,
                    'code' => 403
                ]);
            }

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Project::leftJoin('users', 'users.id', '=', 'projects.user_id')
                ->selectRaw('projects.id, projects.name, projects.user_id, projects.common_total_vi, 
                users.short_name as user_short_name')
                ->where('projects.name', '!=', '')
                ->where('projects.company_id', \Auth::guard('api')->user()->last_company_id)
                ->whereRaw($where);

            //  Sort
            $listItem = $this->sort($request, $listItem);
            $listItem = $listItem->paginate(20)->appends($request->all());

            foreach ($listItem as $k => $item) {
                //  Lấy thông tin đối tác
                $item->user = [
                    'id' => $item->user_id,
                    'short_name' => $item->user_short_name
                ];
                unset($item->user_id);
                unset($item->user_short_name);

                //  Lấy thông tin tiền
                $item->common_paid = \Modules\EworkingProject\Models\MoneyHistory::where('project_id', $item->id)->where('type', 0)->sum('value');
                $item->common_rest = (int)$item->common_total_vi - \Modules\EworkingProject\Models\MoneyHistory::where('project_id', $item->id)->where('type', 0)->sum('value');
            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {
            //  Check permission
            if (!CommonHelper::has_permission(\Auth::guard('api')->id(), 'project_view')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không đủ quyền'
                        ]
                    ],
                    'data' => null,
                    'code' => 403
                ]);
            }

            $item = Project::leftJoin('users', 'users.id', '=', 'projects.user_id')
                ->selectRaw('projects.*, users.id as user_id, users.short_name as user_short_name')
                ->where('projects.id', $id)->first();
            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            //  Lấy đối tác
            $item->user = [
                'id' => $item->user_id,
                'short_name' => $item->user_short_name
            ];
            unset($item->user_id);
            unset($item->user_short_name);
            #

            //  Đếm số nhân viên làm dự án
            $admin_ids = [];
            $job_ids = Job::where('project_id', $id)->pluck('id');
            foreach ($job_ids as $job_id) {
                $admin_ids = explode('|', \Modules\EworkingJob\Http\Helpers\EworkingJobHelper::getAdminInJob($job_id));
            }
            $item->total_admin = @Admin::join('role_admin', 'role_admin.admin_id', '=', 'admin.id')
                ->selectRaw('admin.name as admin_name, admin.id as admin_id, role_admin.role_id as role_admin_role_id')
                ->whereIn('role_admin.admin_id', $admin_ids)
                ->where('admin.status', 1)
                ->where('role_admin.company_id', \Auth::guard('api')->user()->last_company_id)
                ->where('role_admin.status', 1)
                ->get()->count();
            #

            //  Lấy tiền dự án
            $item->common_paid = \Modules\EworkingProject\Models\MoneyHistory::where('project_id', $item->id)->where('type', 0)->sum('value');
            $item->common_rest = (int)$item->common_total_vi - \Modules\EworkingProject\Models\MoneyHistory::where('project_id', $item->id)->where('type', 0)->sum('value');
            #

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }


    public function moneyHistory(Request $request)
    {
        try {

            //  Check permission
            if (!CommonHelper::has_permission(\Auth::guard('api')->id(), 'project_view')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không đủ quyền'
                        ]
                    ],
                    'data' => null,
                    'code' => 403
                ]);
            }

            $items = MoneyHistory::leftJoin('project_sub_partners', 'project_sub_partners.id', '=', 'money_history.sub_partner')
                ->selectRaw('money_history.*, 
                    project_sub_partners.id as project_sub_partner_id, project_sub_partners.name as project_sub_partner_name, project_sub_partners.price as project_sub_partner_price')
                ->where('money_history.project_id', $request->project_id)->get();

            foreach ($items as $item) {
                $item->project_sub_partner = [
                    'id' => $item->project_sub_partner_id,
                    'name' => $item->project_sub_partner_name,
                    'price' => $item->project_sub_partner_price,
                ];
                unset($item->project_sub_partner_id);
                unset($item->project_sub_partner_name);
                unset($item->project_sub_partner_price);
                unset($item->sub_partner);
            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $items,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    //Danh sách người thực hiện trong dự án
    public function adminInProject($id)
    {
        try {

            //  Check permission
            if (!CommonHelper::has_permission(\Auth::guard('api')->id(), 'project_view')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không đủ quyền'
                        ]
                    ],
                    'data' => null,
                    'code' => 403
                ]);
            }
            $item = Project::leftJoin('jobs', 'jobs.project_id', '=', 'projects.id')
                ->leftJoin('tasks', 'tasks.job_id', '=', 'jobs.id')
                ->selectRaw('tasks.admin_ids')
                ->where('projects.id', $id);
            $stringAdmin = '';
            foreach ($item->pluck('admin_ids')->toArray() as $key => $id) {
                $char = $key == 0 ? '' : '|';
                if ($id != '') {
                    $stringAdmin .= $char . trim($id, '|');
                }
            }
            $string = trim($stringAdmin, '|');
            $adminIds = array_unique(explode('|', $string));

            $listItem = null;
            foreach ($adminIds as $v) {
                $query = Admin::leftJoin('role_admin', 'role_admin.admin_id', '=', 'admin.id')
                    ->leftJoin('roles', 'roles.id', '=', 'role_admin.role_id')
                    ->selectRaw('admin.id, admin.name, roles.display_name, admin.image')
                    ->whereIn('admin.id', $adminIds)
                    ->where('admin.status', 1)
                    ->where('role_admin.admin_id', $v)
                    ->where('role_admin.status', 1)
                    ->where('role_admin.company_id', \Auth::guard('api')->user()->last_company_id);
                if ($query->exists()) {
                    $item = $query->first();
                    $listItem[] = (object)[
                        'id' => @$item->id,
                        'name' => @$item->name,
                        'image' => asset('public/filemanager/userfiles/' . $item->image),
                        'role_name' => @$item->display_name,
                    ];
                }
            }

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function workProgressOfProject($id)
    {
        try {

            //  Check permission
            if (!CommonHelper::has_permission(\Auth::guard('api')->id(), 'project_view')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không đủ quyền'
                        ]
                    ],
                    'data' => null,
                    'code' => 403
                ]);
            }
            $adminIds = [];
            $item = @Job::select('id', 'name')->where('project_id', $id)->get();
            foreach ($item as $job) {
                $adminIds += Task::where('job_id', $job->id)->pluck('admin_ids')->toArray();
                $stringAdmin = '';
                foreach ($adminIds as $key => $id) {
                    $char = $key == 0 ? '' : '|';
                    if ($id != '') {
                        $stringAdmin .= $char . trim($id, '|');
                    }
                }
                $string = trim($stringAdmin, '|');
                $adminIds = array_unique(explode('|', $string));
                if ($adminIds[0] == '') {
                    $job->total_human = 0;
                } else {
                    $job->total_human = count($adminIds);
                }

            }

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function taskListForeachMember(request $request, $id)
    {
        try {

            //  Check permission
            if (!CommonHelper::has_permission(\Auth::guard('api')->id(), 'project_view')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không đủ quyền'
                        ]
                    ],
                    'data' => null,
                    'code' => 403
                ]);
            }
            $item = Task::select('name', 'end_date')->where('job_id', $id);
            if (isset($request->admin_id)) {
                $item = $item->where('admin_ids', 'like', '%|' . $request->admin_id . '|%')->get();
            } else {
                $item = $item->get();
            }


            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . $this->module['table_name'] .  '.id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
