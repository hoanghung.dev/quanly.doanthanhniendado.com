{{--@if(isset($result))--}}
    {{--<script>--}}
        {{--$(document).ready(function () {--}}
            {{--let rs_job = $('.rs-subpartner');--}}
            {{--KTApp.block(rs_job, {--}}
                {{--overlayColor: "#ff19600",--}}
                {{--type: "v2",--}}
                {{--state: "success",--}}
                {{--size: "lg"--}}
            {{--});--}}

            {{--//hien popup sua nhiem vu--}}
            {{--$.ajax({--}}
                {{--url: '/admin/project/get-sub-partner/' + '{{$result->id}}',--}}
                {{--type: 'GET',--}}
                {{--success: function (res) {--}}
                    {{--rs_job.html(res);--}}
                {{--}, error: function () {--}}
                    {{--alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');--}}
                {{--}--}}
            {{--});--}}
            {{--$('body').on('click', '.pagination-subpartner ul li', function (event) {--}}
                {{--event.preventDefault();--}}
                {{--let url = $(this).children('a').attr('href');--}}
                {{--if (url != undefined) {--}}
                    {{--$.ajax({--}}
                        {{--url: url,--}}
                        {{--type: 'GET',--}}
                        {{--beforeSend: function () {--}}
                            {{--// Handle the beforeSend event--}}
                            {{--KTApp.block(rs_job, {--}}
                                {{--overlayColor: "#000",--}}
                                {{--type: "v1",--}}
                                {{--state: "success",--}}
                                {{--size: "lg"--}}
                            {{--});--}}
                        {{--},--}}
                        {{--success: function (res) {--}}
                            {{--rs_job.html(res);--}}
                        {{--}, error: function () {--}}
                            {{--alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');--}}
                        {{--}--}}
                    {{--});--}}
                {{--}--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
<?php

$subpartners = \Modules\EworkingProject\Models\SubPartner::where('project_id', $result->id)->get();
?>
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Thông tin thầu phụ <a style="position: absolute;right: 10px;top: 10px;"
                                          class="btn btn-icon btn btn-label btn-label-brand btn-bold btn-add-sub-partner btn-add"
                                          title="Thêm mới thầu phụ"><i
                                class="flaticon2-add-1"></i></a>
                </h3>
            </div>
        </div>
        <!--begin::Form-->
        <div class="kt-form">
            <div class="kt-portlet__body" style="    padding-top: 10px;">
                <div class="kt-section kt-section--first">
                    <table class="table table-sub-partner">
                        <thead>
                        <tr>
                            <th>Tên thầu
                            </th>
                            <th>Số tiền
                            </th>
                            <th>
                            </th>
                        </tr>
                        </thead>
                        <tbody class="rs-subpartner">
                        {{--@else--}}
                            @if(count($subpartners) > 0)
                                @foreach($subpartners as $subpartner)
                                    <tr>
                                        <td>{{$subpartner->name}}</td>
                                        <td>{{number_format($subpartner->price)}}</td>
                                        <td style="width: 25%;">
                                            <a class="edit-sub-partner btn-sub-partner" title="Chỉnh sửa thầu phụ"
                                               data-id="{{$subpartner->id}}"
                                               data-name="{{$subpartner->name}}"
                                               data-price="{{$subpartner->price}}"
                                               style="padding: 0 10px;">
                                                <i class="flaticon-edit"></i></a>
                                            <a class="delete-warning btn-sub-partner" title="Xóa thầu phụ"
                                               style="padding: 0 0px;"
                                               href="{{route('sub-partner.del')}}?id={{$subpartner->id}}">
                                                <i class="flaticon-delete"></i></a>

                                        </td>
                                    </tr>
                                @endforeach
                                {{--<tr>--}}
                                    {{--<td colspan="3">--}}
                                        {{--{{$subpartners->render(config('core.admin_theme').'.partials.paginate',['class'=>'subpartner'])}}--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                            @else
                                <tr>
                                    <td>Không tồn tại thầu phụ !</td>
                                </tr>
                            @endif

                        {{--@Endif--}}
                        {{--@if(isset($result))--}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
{{--@endif--}}
