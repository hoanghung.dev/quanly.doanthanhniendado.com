<?php
$value = \Modules\EworkingProject\Models\MoneyHistory::where('project_id', $item->id)->where('type', 0)->sum('value');
$total = $item->common_total_vi;
$chia = ($value == 0 || $total == 0) ? 0 : (@$value / @$total) * 100;
?>
<div class="progress">
    <div class="progress-bar" role="progressbar" style="width: {{ $chia }}%"
         aria-valuenow="{{ $chia }}" aria-valuemin="0" aria-valuemax="100"></div>
</div>