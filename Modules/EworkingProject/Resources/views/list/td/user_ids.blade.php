<?php
if (isset($item->{$field['name']})) {
    $user_ids = $item->{$field['name']};
}
?>
@if (isset($user_ids))
    <?php
    $user_ids = is_array($user_ids) ? $user_ids : explode('|', $user_ids);
    $admins = \Modules\EworkingUser\Models\User::select(['id', 'name', 'image'])->whereIn('id', $user_ids)->get();
    ?>
    @if(count($admins) > 0)
        <div class="kt-widget__details">
            <div class="kt-section__content" style="padding: 0 0 0 25px;">
                <div class="kt-media-group">
                    <?php
                    $countArray = (count($admins) >= 5) ? 5 : count($admins);
                    ?>
                    @for($i = 0; $i < $countArray; $i ++)
                        <?php
                        $admin = $admins[$i];
                        ?>
                        <a href="{{URL::to('/admin/profile/'.$admin->id)}}"
                           class="kt-media kt-media--sm kt-media--circle"
                           data-toggle="kt-tooltip" data-skin="brand"
                           data-placement="top" title=""
                           data-original-title="{{ $admin['name'] }}">
                            <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($admin['image'], 30, 30) }}"
                                 alt="{{ $admin['name'] }}" title="{{ $admin['name'] }}">
                        </a>
                    @endfor
                    @if(count($admins) > 5)
                        <a href="#"
                           class="kt-media kt-media--sm kt-media--circle"
                           data-toggle="kt-tooltip" data-skin="brand"
                           data-placement="top" title=""
                           data-original-title="Micheal York">
                            <span>{{count($admins) - 5}}</span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    @endif
@endif
