<div class="modal fade" id="popup-add-money-history" role="dialog" style="z-index: 100000">
    <form id="form-money-history" action="post" autocomplete="off">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content " style="height:auto">
                <div class="modal-header">
                    <h4 style="display:inline-block;" class="title-money-history">Thêm lịch sử tiền</h4>
                    <button type="button" class="close" data-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group value">
                                <label for="usr">Giá trị</label>
                                <input type="text" name="value" class="form-control number_price">
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-6">
                            <div class="form-group credited">
                                <label for="usr">Phân loại</label>
                                <select name="type" class="form-control">
                                    <option value="0">Tiền về</option>
                                    <option value="1">Tiền xuất</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <?php
                                $subPartners = \Modules\EworkingProject\Models\SubPartner::where('project_id', $result->id)->pluck('name', 'id')->toArray();
                                ?>
                                <label for="usr">Thầu phụ</label>
                                <select name="sub_partner" class="form-control">
                                    <option>Chọn thầu phụ</option>
                                    @foreach($subPartners as $idP => $partners)
                                        <option value="{{$idP }}">{{$partners}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="usr">Ngày thực tế</label>
                                <div class="input-group date" id="date">
                                    <input type="text" name="date" class="form-control datetimepicker">
                                    <span class="input-group-addon select-datetimepicker">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                </div>
                                <input type="text" name="project_id" value="{{$result->id}}" class="hidden">
                                <input type="text" name="id" class="hidden">
                                <input type="text" name="admin_id" value="{{Auth::guard('admin')->user()->id}}"
                                       class="hidden">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12">
                            <label for="usr">Mô tả</label>
                            <textarea name="intro" cols="60" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {{--<button class="btn btn-default" data-dismiss="modal"></button>--}}
                    <button class="btn btn-success" type="submit"><i class="icon-header fa fa-check-circle"></i> Hoàn
                        thành
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $('body').on('click', '.btn-add-money-history', function () {
        loading();
        let modal = $('#popup-add-money-history');
        modal.find(".title-money-history").text($(this).attr('title'));
        modal.find('#date').datepicker({format: "dd-mm-yyyy"});
        stopLoading();
        modal.modal();
    });
    $('body').on('click', '.edit-money-history', function () {
        loading();
        let modal = $('#popup-add-money-history');
        modal.find(".title-money-history").text($(this).attr('title'));
        modal.find("input[name='id']").val($(this).data('id'));
        modal.find("input[name='value']").val($(this).data('value'));
        modal.find(".value .number_price").val($(this).data('value'));
        modal.find("select[name='sub_partner']").val($(this).data('sub_partner'));
        modal.find("select[name='type']").val($(this).data('type'));
        modal.find("input[name='date']").val($(this).data('date'));
        modal.find("textarea[name='intro']").val($(this).data('intro'));
        modal.find('#date').datepicker({format: "dd-mm-yyyy"});
        stopLoading();
        modal.modal();
    });
    $(document).ready(function () {
        // form popup
        $('#form-money-history').validate({
            onfocusout: false,
            onkeyup: false,
            onclick: false,
            rules: {
                date: {
                    required: true,
                },
                value: {
                    required: true,
                }
            },
            messages: {
                value: {
                    required: "<p style='color: red'>Giá trị không được để trống</p>",
                },
                date: {
                    required: "<p style='color: red'>Ngày giao dịch không được để trống</p>",
                },
            },
            submitHandler: function (form) {
                //code in here
                let modal = $('#popup-add-money-history');
                let btn = modal.find('.btn');
                btn.children().hide();
                btn.attr("disabled", true);
                btn.addClass('kt-spinner').addClass('btn-outline').addClass('kt-spinner--sm').addClass('kt-spinner--light');

                setTimeout(function () {
                    var url = '{{route('money.ajax-update')}}';
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: $('#form-money-history').serializeArray(),
                        success: function (data) {
                            if (data.status) {
                                window.location.reload();
                                toastr.success("cập nhật thành công!");
                                $('#popup-add-money-history .close').click();
                            } else {
                                toastr.error("Thất bại!");
                                $('#popup-add-money-history .close').click();
                            }
                        }
                    });
                    btn.children().show();
                    btn.removeClass('kt-spinner').removeClass('btn-outline').removeClass('kt-spinner--sm').removeClass('kt-spinner--light');
                }, 1000);
            }
        });
    });
</script>
