<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::group(['prefix' => 'projects', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\ProjectController@index')->name('user')->middleware('api_permission:project_view');
        Route::get('/{id}', 'Admin\ProjectController@show')->middleware('api_permission:project_view');

        Route::get('{id}/admins', 'Admin\ProjectController@adminInProject')->middleware('api_permission:project_view');

        Route::get('/jobs/{id}', 'Admin\ProjectController@workProgressOfProject')->middleware('api_permission:job_view');
//        Route::get('/jobs/tasks/{id}', 'Admin\ProjectController@taskListForeachMember')->middleware('api_permission:job_view');

    });

    Route::group(['prefix' => 'money-history', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\ProjectController@moneyHistory')->middleware('api_permission:project_view');
    });
});