<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'project'], function () {
        Route::get('', 'Admin\ProjectController@getIndex')->name('project')->middleware('permission:project_view');
        Route::get('publish', 'Admin\ProjectController@getPublish')->name('project.publish')->middleware('permission:project_publish');
        Route::post('add', 'Admin\ProjectController@add')->middleware('permission:project_add')->name('project.add');

        Route::get('delete/{id}', 'Admin\ProjectController@delete')->middleware('permission:project_delete');
        Route::post('multi-delete', 'Admin\ProjectController@multiDelete')->middleware('permission:project_delete');

        Route::get('search-for-select2', 'Admin\ProjectController@searchForSelect2')->name('project.search_for_select2');

        // money-history
        Route::get('get-money-history/{id}', 'Admin\MoneyHistoryController@getByIdProject')->name('sub-partner.get-money-history')->middleware('permission:money_history_view');
        Route::post('update-money-history', 'Admin\MoneyHistoryController@ajaxUpdate')->name('money.ajax-update')->middleware('permission:money_history_edit');
        Route::get('del-money-history', 'Admin\MoneyHistoryController@del')->name('money.del')->middleware('permission:money_history_delete');

        // sub-partner
        Route::post('update-sub-partner', 'Admin\SubPartnerController@ajaxUpdate')->name('sub-partner.ajax-update');
        Route::get('del-sub-partner', 'Admin\SubPartnerController@del')->name('sub-partner.del');
        Route::get('get-sub-partner/{id}', 'Admin\SubPartnerController@getByIdProject')->name('sub-partner.get-by-id');

        Route::get('{id}', 'Admin\ProjectController@update')->middleware('permission:project_view')->name('project.edit');
        Route::post('{id}', 'Admin\ProjectController@update')->middleware('permission:project_edit');
    });

});
