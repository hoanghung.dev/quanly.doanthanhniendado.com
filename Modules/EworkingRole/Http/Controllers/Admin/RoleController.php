<?php

namespace Modules\EworkingRole\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use App\Models\Admin;
use \Modules\EworkingJob\Models\JobType;
use App\Models\PermissionRole;
use App\Models\RoleAdmin;
use App\Models\Roles;
use Auth;
use Illuminate\Http\Request;
use Validator;

class RoleController extends CURDBaseController
{

    protected $module = [
        'code' => 'role',
        'label' => 'Nhóm quyền',
        'modal' => '\App\Models\Roles',
        'list' => [
            ['name' => 'display_name', 'type' => 'text_edit', 'label' => 'Quyền'],
            ['name' => 'description', 'type' => 'text', 'label' => 'Mô tả'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'display_name', 'type' => 'text', 'class' => 'require', 'label' => 'Tên'],
                ['name' => 'description', 'type' => 'textarea', 'label' => 'Mô tả'],
            ],
        ],
    ];

    protected $filter = [
        'display_name' => [
            'label' => 'Quyền',
            'type' => 'text',
            'query_type' => 'like'
        ],
    ];

    public function getIndex(Request $request)
    {


        $data = $this->getDataList($request);

        return view('eworkingrole::list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id)->whereNotNull('company_id');
        return $query;
    }

    public function add(Request $request)
    {
        try {

            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('eworkingrole::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'display_name' => 'required'
                ], [
                    'display_name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    $data['admin_id'] = \Auth::guard('admin')->user()->id;
                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }
                    if ($this->model->save()) {
                        $this->model->name = str_slug($data['display_name'], '_') . '_' . $this->model->id;
                        $this->model->save();
                        foreach ($request->permission as $key => $value) {
                            PermissionRole::firstOrCreate(['permission_id' => $value, 'role_id' => $this->model->id]);
                        }
                        $this->addJobType($request, $this->model);

                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    /*
     * Cập nhật quyền xem các phòng ban ở công việc
     * */
    public function addJobType($request, $item)
    {
        //  Update những jobType được chọn
        $job_type_update = [];
        if ($request->has('permission_job_type')) {
            foreach ($request->permission_job_type as $job_type_id) {
                $job_type = JobType::find($job_type_id);
                if (is_object($job_type)) {
                    $job_type_update[] = $job_type_id;
                    $role_ids = explode('|', $job_type->role_ids);
                    $roles = '';
                    foreach ($role_ids as $id) {
                        if ($id != '' && $id != $item->id) {
                            $roles .= '|' . $id;
                        }
                    }
                    $job_type->role_ids = $roles . '|' . $item->id . '|';
                    $job_type->save();
                }
            }
        }

        //  Xóa role_id khỏi những jobTyp không được chọn mà vẫn có role_id từ trước
        $job_type_no_update = JobType::whereNotIn('id', $job_type_update)->where('role_ids', 'like', '%|' . $item->id . '|%')
            ->where('company_id', \Auth::guard('admin')->user()->last_company_id)->get();
        foreach ($job_type_no_update as $job_type) {
            $role_ids = explode('|', $job_type->role_ids);
            $roles = '';
            foreach ($role_ids as $id) {
                if ($id != '' && $id != $item->id) {
                    $roles .= '|' . $id;
                }
            }
            $job_type->role_ids = $roles . '|';
            $job_type->save();
        }
    }

    public function update(Request $request)
    {
        try {
                

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false
                && !CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền sửa!');
                return back();
            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('eworkingrole::edit')->with($data);
            } else if ($_POST) {

                    

                $validator = Validator::make($request->all(), [
                    'display_name' => 'required'
                ], [
                    'display_name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    #
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        $stored_permissions = Roles::permission_role($request->id);
                        foreach ($stored_permissions as $key => $value) {
                            if (!in_array($value, $request->permission))
                                PermissionRole::where(['permission_id' => $value, 'role_id' => $request->id])->delete();
                        }

                        if (is_array($request->permission)) {
                            foreach ($request->permission as $key => $value) {
                                PermissionRole::firstOrCreate(['permission_id' => $value, 'role_id' => $request->id]);
                            }
                        }

                        $this->addJobType($request, $item);

                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {



            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xuất bản!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {

                

            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
                return back();
            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {

                

            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    /*
     * Nhân bản quyền
     * $role_id : id quyền muốn nhân bản
     * $role: quyền muốn nhân bản
     * $data: tuỳ chỉnh các cột chèn vào quyền
     * */
    public function duplicateRole($role_id, $company_id,  $role = false, $data = [], $admin_id = false)
    {
        if (!$role) {
            $role = $this->model->find($role_id);
        }

        $role_new = $role->replicate();
        foreach ($data as $key => $value) {
            $role_new->{$key} = $value; // the new company_id
        }
        $role_new->admin_id = $admin_id ? $admin_id : \Auth::guard('admin')->user()->id;
        $role_new->save();

        RoleAdmin::create([
            'admin_id' => $admin_id ? $admin_id : \Auth::guard('admin')->user()->id,
            'role_id' => $role_new->id,
            'company_id' => $company_id
        ]);

        foreach ($role->permission_role($role->id) as $permission_id) {
            PermissionRole::create([
                'role_id' => $role_new->id,
                'permission_id' => $permission_id,
            ]);
        }
        return $role_new;
    }
}
