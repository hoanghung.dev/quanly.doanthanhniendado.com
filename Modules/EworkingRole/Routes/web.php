<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'role'], function () {
        Route::get('', 'Admin\RoleController@getIndex')->name('role')->middleware('permission:role_view');
        Route::match(array('GET', 'POST'), 'add', 'Admin\RoleController@add')->middleware('permission:role_add');
        Route::get('delete/{id}', 'Admin\RoleController@delete')->middleware('permission:role_delete');
        Route::post('multi-delete', 'Admin\RoleController@multiDelete')->middleware('permission:role_delete');
        Route::get('search-for-select2', 'Admin\RoleController@searchForSelect2')->name('role.search_for_select2')->middleware('permission:role_view');
        Route::get('{id}', 'Admin\RoleController@update')->middleware('permission:role_view');
        Route::post('{id}', 'Admin\RoleController@update')->middleware('permission:role_edit');
    });
});

Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'role_software'], function () {
        Route::get('', 'Admin\RoleSoftwareController@getIndex')->name('role_software')->middleware('permission:role_software_view');
        Route::match(array('GET', 'POST'), 'add', 'Admin\RoleSoftwareController@add')->middleware('permission:role_software_add');
        Route::get('delete/{id}', 'Admin\RoleSoftwareController@delete')->middleware('permission:role_software_delete');
        Route::post('multi-delete', 'Admin\RoleSoftwareController@multiDelete')->middleware('permission:role_software_delete');
        Route::get('search-for-select2', 'Admin\RoleSoftwareController@searchForSelect2')->name('role_software.search_for_select2')->middleware('permission:role_software_view');
        Route::get('{id}', 'Admin\RoleSoftwareController@update')->middleware('permission:role_software_view');
        Route::post('{id}', 'Admin\RoleSoftwareController@update')->middleware('permission:role_software_edit');
    });
});
