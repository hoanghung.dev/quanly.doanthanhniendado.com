<?php

$data = \Modules\EworkingService\Models\Service::where('display', 1)->orderBy($field['display_field'], 'asc')->get();
$value = [];
if (isset($field['multiple']) && isset($result)) {
    if (is_array($result->{$field['name']}) || is_object($result->{$field['name']})) {
        foreach ($result->{$field['name']} as $item) {
            $value[] = $item->id;
        }
    } elseif (is_string($result->{$field['name']})) {
        $value = explode('|', $result->{$field['name']});
    }
} else {
    if (old($field['name']) != null) $value[] = old($field['name']);
    if (isset($field['value'])) $value[] = $field['value'];
}
if(isset($_GET['service_id'])) $value[] = $_GET['service_id'];
?>
<select class="form-control {{ $field['class'] or '' }}" id="{{ $field['name'] }}"
        {{ strpos($field['class'], 'require') !== false ? 'required' : '' }}
        name="{{ $field['name'] }}{{ isset($field['multiple']) ? '[]' : '' }}" {{ isset($field['multiple']) ? 'multiple' : '' }}>
    @foreach ($data as $v)
        <option value='{{ $v->id }}' {{ in_array($v->id, $value) ? 'selected':'' }}>{{ $v->{$field['display_field']} }}{{ isset($field['display_field2']) ? ' - ' . $v->{$field['display_field2']} . ' tài khoản' : '' }}</option>
    @endforeach
</select>
<span class="text-danger">{{ $errors->first($field['name']) }}</span>