<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'service'], function () {
        Route::get('', 'Admin\ServiceController@getIndex')->name('service')->middleware('permission:service_view');
        Route::get('show', 'Admin\ServiceController@show')->name('service.show')->middleware('permission:service_history_view');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ServiceController@add')->middleware('permission:service_add');
        Route::get('delete/{id}', 'Admin\ServiceController@delete')->middleware('permission:service_delete');
        Route::post('multi-delete', 'Admin\ServiceController@multiDelete')->middleware('permission:service_delete');
        Route::get('search-for-select2', 'Admin\ServiceController@searchForSelect2')->name('service.search_for_select2')->middleware('permission:service_view');
        Route::get('get-price', 'Admin\ServiceController@ajaxResolePrice');
        Route::get('{id}', 'Admin\ServiceController@update')->middleware('permission:service_view');
        Route::post('{id}', 'Admin\ServiceController@update')->middleware('permission:service_edit');
    });
    Route::group(['prefix' => 'service_history'], function () {
        Route::get('', 'Admin\ServiceHistoryController@getIndex')->middleware('permission:service_history_view');
        Route::get('publish', 'Admin\ServiceHistoryController@getPublish')->middleware('permission:service_history_publish');
        Route::get('ajax-publish', 'Admin\ServiceHistoryController@ajaxActiveServiceHistory')->middleware('permission:service_history_publish');
        Route::get('link-publish', 'Admin\ServiceHistoryController@linkActiveServiceHistory')->middleware('permission:service_history_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ServiceHistoryController@add')->middleware('permission:service_history_add');
        Route::get('delete/{id}', 'Admin\ServiceHistoryController@delete')->middleware('permission:service_history_delete');
        Route::post('multi-delete', 'Admin\ServiceHistoryController@multiDelete')->middleware('permission:service_history_delete');
        Route::get('search-for-select2', 'Admin\ServiceHistoryController@searchForSelect2')->middleware('permission:service_history_view');
        Route::get('{id}', 'Admin\ServiceHistoryController@update')->middleware('permission:service_history_view');
        Route::post('{id}', 'Admin\ServiceHistoryController@update')->middleware('permission:service_history_edit');
    });

    Route::get('get-service-history', 'Admin\ServiceHistoryController@getServiceHistory');

    Route::get('service-history/get-money', 'Admin\ServiceHistoryController@ajaxResolePrice');
});
