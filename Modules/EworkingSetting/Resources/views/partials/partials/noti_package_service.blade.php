@if(in_array('service_history_add', $permissions) && !in_array('super_admin', $permissions))
    <?php
    if (!isset($company_choosing)) {
        $company_choosing = \Modules\EworkingCompany\Models\Company::find(@\Auth::guard('admin')->user()->last_company_id);
        View::share('company_choosing', $company_choosing);
    }
    ?>
    <span style="line-height: 53px"><a href="/admin/service_history"
                class="{{ time() > strtotime('-3 day', strtotime(@$company_choosing->exp_date)) ? 'text-highline' : '' }}">Gói đang dùng: {{@$company_choosing->account_max}} người - Ngày hết hạn: {{date('d/m/Y', strtotime(@$company_choosing->exp_date))}}
    </a></span>
@endif