<?php

namespace Modules\HandymanClientsay\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Validator;

class ClientSayController extends CURDBaseController
{
    protected $module = [
        'code' => 'client_say',
        'table_name' => 'clients_say',
        'label' => 'handymanclientsay::admin.review_of_customer',
        'modal' => '\Modules\HandymanClientsay\Models\ClientSay',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'handymanclientsay::admin.image'],
            ['name' => 'name', 'type' => 'text_edit_trans', 'label' => 'handymanclientsay::admin.name'],
//            ['name' => 'name', 'type' => 'text_edit', 'label' => 'handymanclientsay::admin.name'],
            ['name' => 'intro', 'type' => 'text_edit_trans', 'label' => 'handymanclientsay::admin.intro'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name_vi', 'type' => 'text', 'class' => 'required', 'label' => 'handymanclientsay::admin.name_vi'],
                ['name' => 'name_en', 'type' => 'text', 'class' => 'required', 'label' => 'handymanclientsay::admin.name_en'],
                ['name' => 'name_hu', 'type' => 'text', 'class' => 'required', 'label' => 'handymanclientsay::admin.name_hu'],
                ['name' => 'intro_vi', 'type' => 'textarea', 'label' => 'handymanclientsay::admin.intro_vi'],
                ['name' => 'intro_en', 'type' => 'textarea', 'label' => 'handymanclientsay::admin.intro_en'],
                ['name' => 'intro_hu', 'type' => 'textarea', 'label' => 'handymanclientsay::admin.intro_hu'],
//                ['name' => 'content', 'type' => 'textarea_editor', 'label' => 'handymanclientsay::admin.content'],
                ['name' => 'content_vi', 'type' => 'textarea_editor2', 'label' => 'handymanclientsay::admin.content_vi'],
                ['name' => 'content_en', 'type' => 'textarea_editor2', 'label' => 'handymanclientsay::admin.content_en'],
                ['name' => 'content_hu', 'type' => 'textarea_editor2', 'label' => 'handymanclientsay::admin.content_hu'],


            ],

            'info_tab' => [
                ['name' => 'image', 'type' => 'file_image', 'label' => 'handymanclientsay::admin.image'],

            ],

        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'handymanclientsay::admin.name',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'intro' => [
            'label' => 'handymanclientsay::admin.intro',
            'type' => 'text',
            'query_type' => 'like'
        ],

    ];

    public function getIndex(Request $request)
    {


        $data = $this->getDataList($request);

        return view('handymanclientsay::list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
        }

        return $query;
    }

    public function add(Request $request)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('handymanclientsay::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name_vi' => 'required',
                    'name_en' => 'required',
                    'name_hu' => 'required'
                ], [
                    'name_vi.required' => 'Bắt buộc phải nhập tên',
                    'name_en.required' => 'Bắt buộc phải nhập tên',
                    'name_hu.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('handymanclientsay::edit')->with($data);
            } else if ($_POST) {

                $validator = Validator::make($request->all(), [
                    'name_vi' => 'required',
                    'name_en' => 'required',
                    'name_hu' => 'required'
                ], [
                    'name_vi.required' => 'Bắt buộc phải nhập tên',
                    'name_en.required' => 'Bắt buộc phải nhập tên',
                    'name_hu.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
