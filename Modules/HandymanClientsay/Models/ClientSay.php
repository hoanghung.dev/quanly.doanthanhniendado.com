<?php
namespace Modules\HandymanClientsay\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientSay extends Model
{

    protected $table = 'clients_say';
    public $timestamps = false;



    protected $fillable = [
        'name_vi','name_en','name_hu', 'image', 'intro_vi','intro_en','intro_hu','content_vi','content_en','content_hu'
    ];



}
