<?php

namespace Modules\HandymanServicesBooking\Models;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
//use Modules\EworkingUser\Models\BillPayment;

class Booking extends Model
{

    protected $table = 'bookings';

    protected $fillable = [
        'user_id', 'service_id', 'date_start', 'time_start', 'time_end','note' ,'price','payed','created_at','updated_at','status','admin_id'

    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }
    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

}
