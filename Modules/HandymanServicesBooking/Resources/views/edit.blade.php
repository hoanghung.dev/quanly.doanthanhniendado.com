@extends(config('core.admin_theme').'.template')
@section('main')
    <form class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid {{ @$module['code'] }}"
          action="" method="POST"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <input name="return_direct" value="save_continue" type="hidden">



    <?php
        $service_field = '';
        if ($result->service_fields!=Null){
            $service_fields = (array)json_decode($result->service_fields);

            foreach ($service_fields as $k=>$v){
                $field_name = \Modules\HandymanServicesBooking\Models\ServiceFields::find($k)->{'name_'.$language};
                $service_field .= $field_name.' : '.$v.'<br>';
            }
        }

    ?>


        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                     id="kt_page_   portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg" style="">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">{{trans('handymanservicesbooking::admin.info_order')}}</h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="/admin/{{ $module['code'] }}" class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">{{trans('handymanservicesbooking::admin.back')}}</span>
                            </a>
                            @if(in_array($module['code'].'_edit', $permissions))
                            <div class="btn-group">

                                    <button type="submit" class="btn btn-brand">
                                        <i class="la la-check"></i>
                                        <span class="kt-hidden-mobile">{{trans('handymanservicesbooking::admin.save')}}</span>
                                    </button>
                                    <button type="button"
                                            class="btn btn-brand dropdown-toggle dropdown-toggle-split"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_continue">
                                                    <i class="kt-nav__link-icon flaticon2-reload"></i>
                                                    {{trans('handymanservicesbooking::admin.save_and_continue')}}
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_exit">
                                                    <i class="kt-nav__link-icon flaticon2-power"></i>
                                                    {{trans('handymanservicesbooking::admin.save_and_quit')}}
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_create">
                                                    <i class="kt-nav__link-icon flaticon2-add-1"></i>
                                                    {{trans('handymanservicesbooking::admin.save_and_create')}}
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>

        <div class="row">
{{--            Thông tin đơn--}}
            <div class="col-xl-12 col-md-6 order-lg-2 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase" style="font-weight: 600">
                                {{trans('handymanservicesbooking::admin.info_order')}}
                            </h3>
                        </div>
                    </div>

                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <!--begin: Datatable -->
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                             id="kt_datatable_latest_orders" style="">
                            <table class="kt-datatable__table" style=" width: 100%;">
                                <thead class="kt-datatable__head" style="    overflow: unset;">
                                <tr class="kt-datatable__row" style="left: 0px;">
                                    <th class="kt-datatable__cell kt-datatable__cell--sort" style="width: 40%">
                                        <span>{{trans('handymanservicesbooking::admin.request')}}</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort" style="width: 60%;">
                                        <span>{{trans('handymanservicesbooking::admin.info')}}</span>
                                    </th>

                                </tr>
                                </thead>
                                <tbody class="kt-datatable__body ps ps--active-y"
                                       style="">
                                        <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                            <td data-field="ShipDate" class="kt-datatable__cell" style="width: 40%;">
                                                            <span>
                                                                <span class="kt-font-bold">{{trans('handymanservicesbooking::admin.job')}}</span>
                                                            </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell"  style="width: 60%;">

                                                    <span>
                                                                <span class="kt-font-bold">{{@$result->service->{'name_'.$language} }}</span>
                                                            </span>
                                            </td>
                                        </tr>
                                        <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                            <td data-field="ShipDate" class="kt-datatable__cell" style="width: 40%;">
                                                    <span>
                                                        <span class="kt-font-bold">{{trans('handymanservicesbooking::admin.workday')}}</span>
                                                    </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell"  style="width: 60%;">

                                                    <span>
                                                        <span class="kt-font-bold">{{date('d/m/Y',strtotime(@$result->date_start))}}</span>
                                                    </span>
                                            </td>
                                        </tr>
                                        <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                            <td data-field="ShipDate" class="kt-datatable__cell" style="width: 40%;">
                                                    <span>
                                                        <span class="kt-font-bold">{{trans('handymanservicesbooking::admin.work_time')}}</span>
                                                    </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell" style="width: 60%;">

                                                    <span>
                                                        <span class="kt-font-bold">{{@$result->time_start .' tới '. @$result->time_end}}</span>
                                                    </span>
                                            </td>
                                        </tr>
                                        <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                            <td data-field="ShipDate" class="kt-datatable__cell" style="width: 40%;">
                                                    <span>
                                                        <span class="kt-font-bold">{{trans('handymanservicesbooking::admin.parameter')}}</span>
                                                    </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell" style="width: 60%;">
                                                    <span>
                                                        <span class="kt-font-bold">{!! $service_field !!}</span>
                                                    </span>
                                            </td>
                                        </tr>
                                        <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                            <td data-field="ShipDate" class="kt-datatable__cell"  style="width: 40%;">
                                                    <span>
                                                        <span class="kt-font-bold">{{trans('handymanservicesbooking::admin.note')}}</span>
                                                    </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell"  style="width: 60%;">

                                                    <span>
                                                        <span class="kt-font-bold">{{@$result->note}}</span>
                                                    </span>
                                            </td>
                                        </tr>
                                        <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                            <td data-field="ShipDate"  style="width: 40%;" class="kt-datatable__cell">
                                                    <span>
                                                        <span class="kt-font-bold" style="font-weight: 600!important;">{{trans('handymanservicesbooking::admin.total_price')}}</span>
                                                    </span>
                                            </td>

                                            <td data-field="ShipDate" style="width: 60%;" class="kt-datatable__cell">
                                                    <span>
                                                        <span class="kt-font-bold" style="font-weight: 600!important;">{{number_format(@$result->price, 0, ',', '.')}}<sup>{{trans('handymanservicesbooking::admin.đ')}}</sup></span>
                                                    </span>
                                            </td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable -->
                        {{--<div class="paginate">{{$bill_news->appends(Request::all())->links()}}</div>--}}
                    </div>

                </div>
            </div>

{{--        Thông tin khách--}}
            <div class="col-xl-12 col-md-6 order-lg-2 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase" style="font-weight: 600">
                                {{trans('handymanservicesbooking::admin.info_customer')}}
                            </h3>
                        </div>
                    </div>

                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <!--begin: Datatable -->
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                             id="kt_datatable_latest_orders" style="">
                            <table class="kt-datatable__table" style=" width: 100%;">
                                <thead class="kt-datatable__head" style="    overflow: unset;">
                                    <tr class="kt-datatable__row" style="left: 0px;">
                                    <th class="kt-datatable__cell kt-datatable__cell--sort" style="width: 40%">
                                        <span>{{trans('handymanservicesbooking::admin.request')}}</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort"  style="width: 60%;">
                                        <span>{{trans('handymanservicesbooking::admin.info')}}</span>
                                    </th>

                                </tr>
                                </thead>
                                <tbody class="kt-datatable__body ps ps--active-y"
                                       style="">
                                <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                    <td data-field="ShipDate" class="kt-datatable__cell" style="width: 40%;">
                                                    <span>
                                                        <span class="kt-font-bold">{{trans('handymanservicesbooking::admin.customer_name')}}</span>
                                                    </span>
                                    </td>

                                    <td data-field="ShipDate" class="kt-datatable__cell" style="width: 60%">
                                                    <span>
                                                        <span class="kt-font-bold">{{@$result->user->name}}</span>
                                                    </span>
                                    </td>
                                </tr>

                                <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                    <td data-field="ShipDate" class="kt-datatable__cell" style="width: 40%;">
                                                    <span>
                                                        <span class="kt-font-bold">{{trans('handymanservicesbooking::admin.phone')}}</span>
                                                    </span>
                                    </td>

                                    <td data-field="ShipDate" class="kt-datatable__cell" style="width: 60%;">
                                                    <span>
                                                        <span class="kt-font-bold">{{@$result->user->tel}}</span>
                                                    </span>
                                    </td>
                                </tr>
                                <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                    <td data-field="ShipDate" class="kt-datatable__cell" style="width: 40%;">
                                                    <span>
                                                        <span class="kt-font-bold">{{trans('handymanservicesbooking::admin.email')}}</span>
                                                    </span>
                                    </td>

                                    <td data-field="ShipDate" class="kt-datatable__cell" style="width: 60%;">
                                                    <span>
                                                        <span class="kt-font-bold">{{@$result->user->email}}</span>
                                                    </span>
                                    </td>
                                </tr>
                                <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                    <td data-field="ShipDate" class="kt-datatable__cell" style="width: 40%;">
                                                    <span>
                                                        <span class="kt-font-bold">{{trans('handymanservicesbooking::admin.address')}}</span>
                                                    </span>
                                    </td>

                                    <td data-field="ShipDate" class="kt-datatable__cell" style="width: 60%;">
                                                    <span>
                                                        <span class="kt-font-bold">{{@$result->user->address}}</span>
                                                    </span>
                                    </td>
                                </tr>
                                <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                    <td data-field="ShipDate" class="kt-datatable__cell" style="width: 40%;">
                                                    <span>
                                                        <span class="kt-font-bold">{{trans('handymanservicesbooking::admin.gender')}}</span>
                                                    </span>
                                    </td>

                                    <td data-field="ShipDate" class="kt-datatable__cell" style="width: 60%;">
                                                    <span>
                                                        <span class="kt-font-bold">{{(@$result->user->gender==0)?'Nam':'Nữ'}}</span>
                                                    </span>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable -->
                        {{--<div class="paginate">{{$bill_news->appends(Request::all())->links()}}</div>--}}
                    </div>

                </div>
            </div>
{{--        Swuar đơn--}}
            @if(in_array('view_all_data', $permissions))
            <div class="col-xs-12 col-md-6">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{trans('handymanservicesbooking::admin.edit_bill')}}
                            </h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="kt-form">
                        <div class="kt-portlet__body">
                            <div class="kt-section kt-section--first">
                                @foreach($module['form']['general_tab'] as $field)
                                    @php
                                        $field['value'] = @$result->{$field['name']};
                                    @endphp

                                    <div class="form-group-div form-group {{ @$field['group_class'] }}"
                                         id="form-group-{{ $field['name'] }}">

                                        @if($field['name'] !='status')
                                            @if($field['type'] == 'custom')
                                                @include($field['field'], ['field' => $field])
                                            @else

                                                <label for="{{ $field['name'] }}">{{ trans(@$field['label']) }} @if(strpos(@$field['class'], 'require') !== false)
                                                        <span class="color_btd">*</span>@endif</label>

                                                <div class="col-xs-12">
                                                    @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                    <span class="text-danger">{{ $errors->first($field['name']) }}</span>
                                                </div>
                                            @endif
                                        @endif

                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
            @endif

{{--            Thông tin Người nhận đơn--}}
            <div class="col-xl-12 col-md-6 order-lg-2 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase" style="font-weight: 600">
                                {{trans('handymanservicesbooking::admin.status_order')}}.
                            </h3>
                        </div>
                    </div>

                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <!--begin: Datatable -->
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                             id="kt_datatable_latest_orders" style="">
                            <table class="kt-datatable__table" style=" width: 100%;">
                                <thead class="kt-datatable__head" style="    overflow: unset;">
                                <tr class="kt-datatable__row" style="left: 0px;">
                                    <th class="kt-datatable__cell kt-datatable__cell--sort" style="width: 30%">
                                        <span>{{trans('handymanservicesbooking::admin.status')}}</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort" style="width: 30%">
                                        <span>{{trans('handymanservicesbooking::admin.employees')}}</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort" style="width: 30%">
                                        <span>{{trans('handymanservicesbooking::admin.action')}}</span>
                                    </th>

                                </tr>
                                </thead>
                                <tbody class="kt-datatable__body ps ps--active-y"
                                       style="">
{{--                                {{dd(1)}}--}}
                                <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                    <td data-field="ShipDate" class="kt-datatable__cell"  style="width: 30%">
                                        @foreach($module['form']['general_tab'] as $field)
                                        @if($field['name'] =='status')
                                            @if($field['type'] == 'custom')

                                                @include($field['field'], ['field' => $field])
                                            @else

                                                <label for="{{ $field['name'] }}">{{ trans(@$field['label']) }} @if(strpos(@$field['class'], 'require') !== false)
                                                        <span class="color_btd">*</span>@endif</label>

                                                <div class="col-xs-12">
                                                    @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                    <span class="text-danger">{{ $errors->first($field['name']) }}</span>
                                                </div>
                                            @endif
                                        @endif
                                            @endforeach
                                    </td>
                                    <td data-field="ShipDate" class="kt-datatable__cell"  style="width: 30%">
                                        <span>@include('handymanservicesbooking::list.td.admin_id')</span>
                                    </td>
                                    <style>
                                        .btn_submit{
                                            color: #fff!important;
                                            background-color: #5867dd;
                                            border-color: #5867dd;
                                            outline: none !important;
                                            vertical-align: middle;
                                        }
                                    </style>
                                    <td data-field="ShipDate" class="kt-datatable__cell"  style="width: 30%">
                                        @include('handymanservicesbooking::list.td.btn_booking')
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable -->
                        {{--<div class="paginate">{{$bill_news->appends(Request::all())->links()}}</div>--}}
                    </div>

                </div>
            </div>
        </div>

    </form>
@endsection
@section('custom_head')
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset(config('core.admin_asset').'/css/form.css') }}">
@endsection
@section('custom_footer')
    <script src="{{ asset(config('core.admin_asset').'/js/pages/crud/metronic-datatable/advanced/vertical.js') }}"
            type="text/javascript"></script>

    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce_editor.js') }}"></script>
    <script type="text/javascript">
        editor_config.selector = ".editor";
        editor_config.path_absolute = "{{ (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]" }}/";
        tinymce.init(editor_config);
    </script>
    <script type="text/javascript" src="{{ asset(config('core.admin_asset').'/js/form.js') }}"></script>
@endsection
