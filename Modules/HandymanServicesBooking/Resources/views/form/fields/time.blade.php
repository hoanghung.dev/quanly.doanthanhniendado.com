<label>{{ trans(@$field['label']) }}</label>
<input type="time" name="{{ @$field['name'] }}" class="form-control {{ @$field['class'] }}"
       id="{{ $field['name'] }}" {!! @$field['inner'] !!}
       value="{{ old($field['name']) != null ? old($field['name']) : @$field['value'] }}"
        {{ strpos(@$field['class'], 'require') !== false ? 'required' : '' }}
>