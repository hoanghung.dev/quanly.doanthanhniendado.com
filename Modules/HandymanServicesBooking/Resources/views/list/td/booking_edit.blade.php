
<a href="/admin/{{ $module['code'] }}/{{ $item->id }}"
   target="_blank">
    {{ @$item->{$field['object']}->{$field['display_field'] . '_' . $language} }}
</a>
<div class="row-actions" style="    font-size: 13px;">
    <span class="edit" title="ID của bản ghi">{{trans('handymanservicesbooking::admin.id')}}: {{ @$item->id }} | </span>
    <span class="edit"><a
                href="{{ url('/admin/'.$module['code'].'/' . $item->id) }}"
                title="xem">{{trans('handymanservicesbooking::admin.see')}}</a></span>
</div>
