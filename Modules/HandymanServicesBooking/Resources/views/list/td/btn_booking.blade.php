<?php
if (@$item){
    $items =@$item;
}else{
    $items =@$result;
}
?>
@if(empty($items->admin_id))
    <span class="btn btn_submit btn-primary oder_now_{{@$items->id}}" style=" padding: 0.4em; cursor: pointer">{{trans('handymanservicesbooking::admin.take_bill')}}</span>
@endif
<script>
    $(document).ready(function(){
        let oder_now = '.oder_now_'+'{{$items->id}}';
        $(oder_now).each(function(){
            $(oder_now).click(function(){
                $.ajax({
                    type: "GET",
                    url: '{{route('add-admin-in-booking')}}',
                    data: {
                        admin_id: '{{\Auth::guard('admin')->user()->id}}',
                        booking_id: '{{$items->id}}',
                    },
                    success: function(){
                        window.location.reload()
                    }
                });
            });
        });
    });
</script>