<?php
if (@$item){
    $items =@$item;
}else{
    $items =@$result;
}

    $fields=[
            'Pending' => 'Pending',
            'Received' => 'Received',
            'Complete' => 'Complete',
            'No Complete' => 'No Complete',
            'Cancel' => 'Cancel',
    ];

?>
<select class="form-control select-status_{{$items->id}}"
        {{ ( $items->status == 'Pending')? 'disabled' : ''}} name="status" >
    @foreach ($fields as $value => $name)
        <option value='{{ $value }}' {{ ($value == $items->status) ? 'selected' : ''}} {{ ($value == 'Pending') ? 'disabled' : ''}}>{{ $name }}</option>
    @endforeach
</select>
{{--@endif--}}
<script>
    $(document).ready(function(){
        let oder_now = '.select-status_'+'{{$items->id}}';
        $(oder_now).each(function(){
            $(oder_now).change(function(){
                $.ajax({
                    type: "GET",
                    url: '{{route('change-status')}}',
                    data: {
                        status: $(oder_now).val(),
                        booking_id: '{{$items->id}}',
                    },
                    success: function(){
                        window.location.reload()
                    }
                });
            });
        });
    });
</script>
