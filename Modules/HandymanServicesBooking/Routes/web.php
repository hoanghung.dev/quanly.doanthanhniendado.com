<?php
Route::get('add-admin-in-booking', 'Admin\BookingController@addAdminInBooking')->name('add-admin-in-booking');
Route::get('change-status', 'Admin\BookingController@changeStatus')->name('change-status');
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions','locale']], function () {
    Route::group(['prefix' => 'booking'], function () {

        Route::get('', 'Admin\BookingController@getIndex')->name('booking')->middleware('permission:booking_view');
        Route::get('publish', 'Admin\BookingController@getPublish')->name('booking.publish')->middleware('permission:booking_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\BookingController@add')->middleware('permission:booking_add');
        Route::get('delete/{id}', 'Admin\BookingController@delete')->middleware('permission:booking_delete');
        Route::post('multi-delete', 'Admin\BookingController@multiDelete')->middleware('permission:booking_delete');
        Route::get('search-for-select2', 'Admin\BookingController@searchForSelect2')->name('booking.search_for_select2')->middleware('permission:booking_view');
        Route::get('{id}', 'Admin\BookingController@update')->middleware('permission:booking_view');
        Route::post('{id}', 'Admin\BookingController@update');

    });

    Route::group(['prefix' => 'service'], function () {
        Route::get('', 'Admin\ServiceController@getIndex')->name('service')->middleware('permission:service_view');
        Route::get('publish', 'Admin\ServiceController@getPublish')->name('booking.publish')->middleware('permission:booking_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ServiceController@add')->middleware('permission:service_add');
        Route::get('delete/{id}', 'Admin\ServiceController@delete')->middleware('permission:service_delete');
        Route::post('multi-delete', 'Admin\ServiceController@multiDelete')->middleware('permission:service_delete');
        Route::get('search-for-select2', 'Admin\ServiceController@searchForSelect2')->name('service.search_for_select2')->middleware('permission:service_view');
        Route::get('{id}', 'Admin\ServiceController@update')->middleware('permission:service_view');
        Route::post('{id}', 'Admin\ServiceController@update')->middleware('permission:service_edit');
    });

    Route::group(['prefix' => 'service_field'], function () {
        Route::get('', 'Admin\ServiceFieldsController@getIndex')->name('service_field')->middleware('permission:service_field_view');
        Route::get('publish', 'Admin\ServiceFieldsController@getPublish')->name('service.publish')->middleware('permission:service_field_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ServiceFieldsController@add')->middleware('permission:service_field_add');
        Route::get('delete/{id}', 'Admin\ServiceFieldsController@delete')->middleware('permission:service_field_delete');
        Route::post('multi-delete', 'Admin\ServiceFieldsController@multiDelete')->middleware('permission:service_field_delete');
        Route::get('search-for-select2', 'Admin\ServiceFieldsController@searchForSelect2')->name('service_field.search_for_select2')->middleware('permission:service_field_view');
        Route::get('{id}', 'Admin\ServiceFieldsController@update')->middleware('permission:service_field_view');
        Route::post('{id}', 'Admin\ServiceFieldsController@update')->middleware('permission:service_field_edit');
    });
});
