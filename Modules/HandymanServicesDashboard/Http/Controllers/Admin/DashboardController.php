<?php

namespace Modules\HandymanServicesDashboard\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\Admin;
use Auth;
use DB;
use Illuminate\Http\Request;
use Mail;
use Modules\HandymanServicesBooking\Models\Booking;
use Modules\HandymanServicesBooking\Models\User;

class DashboardController extends Controller
{
    protected $module = [
    ];

    public function dashboard()
    {
        $data['page_title'] = 'Thống kê';
        $data['page_type'] = 'list';
        $data['total_paid'] = Booking::select('id', 'price')->where('payed',1)->where('price','>',0)->get();
        $data['total_unpaid'] = Booking::select('id', 'price')->where('payed',0)->where('price','>',0)->get();
        $total_paids=0;
        foreach ($data['total_paid'] as $total_price_bill){
            $total_paids += $total_price_bill->price;
        }
        $total_unpaids=0;
        foreach ($data['total_unpaid'] as $total_price_bills){
            $total_unpaids += $total_price_bills->price;
        }
        $data['total_bills'] = Booking::select('id')->get()->count();
        $data['total_admins'] = Admin::select('id')->get()->count();
        $data['total_users'] = User::select('id')->get()->count();
        $data['total_happening_bill'] = Booking::select('id')->where('status',1)->get()->count();
        $data['total_waiting_bill'] = Booking::select('id')->where('status',0)->get()->count();
        $data['total_paids'] = $total_paids;
        $data['total_unpaids'] = $total_unpaids;
        $data['bill_happening'] = Booking::where('status','<>','Complete')->where('status','<>','Pending')->orderBy('id', 'desc')->paginate(10);

        if (CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')){
            return view('handymanservicesdashboard::dashboard', $data);
        }else{
            return redirect('/admin/booking');
        }
    }

    public function tooltipInfo(Request $request)
    {
        $modal = new $request->modal;
        $data['item'] = $modal->find($request->id);
        $data['tooltip_info'] = $request->tooltip_info;

        return view('admin.common.modal.tooltip_info', $data);
    }

    public function ajax_up_file(Request $request)
    {
        if ($request->has('file')) {
            $file = CommonHelper::saveFile($request->file('file'));
        }
        return $file;
    }
}
