
@extends(config('core.admin_theme').'.template')
@section('main')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-xs-12 col-lg-12 col-xl-12 col-lg-12 order-lg-1 order-xl-1">
                <!--begin:: Widgets/Finance Summary-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                {{ trans('handymanservicesdashboard::admin.overview') }}
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-widget12">
                            <div class="kt-widget12__content">
                                <div class="kt-widget12__item">
                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">{{ trans('handymanservicesdashboard::admin.total_orders') }}</span>
                                        <span class="kt-widget12__value">{{number_format(@$total_bills, 0, '.', '.')}}</span>
                                    </div>

                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">{{ trans('handymanservicesdashboard::admin.total_customers') }}</span>
                                        <span class="kt-widget12__value">{{number_format(@$total_users, 0, '.', '.')}}</span>
                                    </div>

                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">{{ trans('handymanservicesdashboard::admin.total_staff') }}</span>
                                        <span class="kt-widget12__value">{{number_format(@$total_admins, 0, '.', '.')}}</span>
                                    </div>

                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">{{ trans('handymanservicesdashboard::admin.total_orders_happening') }}</span>
                                        <span class="kt-widget12__value">{{number_format(@$total_happening_bill, 0, '.', '.')}}</span>
                                    </div>

                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">{{ trans('handymanservicesdashboard::admin.total_orders_waiting') }}</span>
                                        <span class="kt-widget12__value">{{number_format(@$total_waiting_bill, 0, '.', '.')}}</span>
                                    </div>
                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">{{ trans('handymanservicesdashboard::admin.total_price_paid') }}</span>
                                        <span class="kt-widget12__value">{{number_format(@$total_paids, 0, '.', '.')}}<sup>{{ trans('handymanservicesdashboard::admin.đ') }}</sup></span>
                                    </div>
                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">{{ trans('handymanservicesdashboard::admin.total_price_unpaid') }}</span>
                                        <span class="kt-widget12__value">{{number_format(@$total_unpaids, 0, '.', '.')}}<sup>{{ trans('handymanservicesdashboard::admin.đ') }}</sup></span>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Finance Summary-->
            </div>

            <div class="col-xl-12 order-lg-2 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                {{ trans('handymanservicesdashboard::admin.order_new_happening') }}
                            </h3>
                        </div>
                    </div>

                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <!--begin: Datatable -->
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                             id="kt_datatable_latest_orders" style="">
                            <table class="kt-datatable__table" style=" width: 100%;">
                                <thead class="kt-datatable__head" style="    overflow: unset;">
                                <tr class="kt-datatable__row" style="left: 0px;">
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 25px;">{{ trans('handymanservicesdashboard::admin.number') }}</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 150px;">{{ trans('handymanservicesdashboard::admin.request') }}</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">{{ trans('handymanservicesdashboard::admin.workday') }}</span></th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">{{ trans('handymanservicesdashboard::admin.price') }}</span></th>

                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 150px;">{{ trans('handymanservicesdashboard::admin.note') }}</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">{{ trans('handymanservicesdashboard::admin.pay') }}</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">{{ trans('handymanservicesdashboard::admin.status') }}</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">{{ trans('handymanservicesdashboard::admin.employees') }}</span>
                                    </th>
{{--                                    <th class="kt-datatable__cell kt-datatable__cell--sort">--}}
{{--                                        <span style="width: 100px;">Hành động</span>--}}
{{--                                    </th>--}}
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 50px;">{{ trans('handymanservicesdashboard::admin.see') }}</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="kt-datatable__body ps ps--active-y"
                                       style="">
                                @if($bill_happening->count()>0)
                                    @foreach($bill_happening as $k=>$v)
                                            <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                                <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 25px;">
                                                        <span class="kt-font-bold">{{$k+1}}</span>
                                                    </span>
                                                </td>

                                                <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 150px;">
                                                        <a href="/admin/booking/{{$v->id}}"><span class="kt-font-bold">{{@$v->service->name_vi}}</span></a>
                                                    </span>
                                                </td>

                                                <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 100px;">
                                                        <span class="kt-font-bold">{{date('d-m-Y',strtotime(@$v->date_start))}}</span>
                                                    </span>
                                                </td>

                                                <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 100px;">
                                                        <span class="kt-font-bold">{{number_format(@$v->price, 0, '.', '.')}}{{ trans('handymanservicesdashboard::admin.đ') }}</span>
                                                    </span>
                                                </td>

                                                <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 150px;">
                                                        <span class="kt-font-bold">{{@$v->note}}</span>
                                                    </span>
                                                </td>

                                                <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 100px;">
                                                        <span class="kt-font-bold">{{(@$v->payed==0)? trans('handymanservicesdashboard::admin.unpaid'):trans('handymanservicesdashboard::admin.paid') }}</span>
                                                    </span>
                                                </td>

                                                <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 100px;">
                                                        <span class="kt-font-bold">{{@$v->status}}</span>
                                                    </span>
                                                </td>

                                                <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 100px;">
                                                        <span class="kt-font-bold">{{@$v->admin->name}}</span>
                                                    </span>
                                                </td>
                                                <td data-field="ShipDate" class="kt-datatable__cell">
                                                        <span style="width: 50px;">
                                                            <a href="/admin/booking/{{$v->id}}" class="btn btn-sm btn-label-brand btn-bold">{{ trans('handymanservicesdashboard::admin.see') }}</a>
                                                        </span>
                                                </td>
                                            </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable -->
                        <div class="paginate">{{$bill_happening->appends(Request::all())->links()}}</div>
                    </div>

                </div>
            </div>


        </div>
    </div>

@endsection
@section('custom_head')
{{--    <link href="https://www.keenthemes.com/preview/metronic/theme/assets/global/css/components.min.css" rel="stylesheet"--}}
{{--          type="text/css">--}}
    <style type="text/css">
        .kt-datatable__cell>span>a.cate {
            color: #5867dd;
            margin-bottom: 3px;
            background: rgba(88, 103, 221, 0.1);
            height: auto;
            display: inline-block;
            width: auto;
            padding: 0.15rem 0.75rem;
            border-radius: 2px;
        }
        .paginate>ul.pagination>li{
            padding: 5px 10px;
            border: 1px solid #ccc;
            margin: 0 5px;
            cursor: pointer;        }

        .paginate>ul.pagination span{
            color: #000;
        }
        .paginate>ul.pagination>li.active{
            background: #0b57d5;
            color: #fff!important;
        }
        .paginate>ul.pagination>li.active span{
            color: #fff!important;
        }
        .kt-widget12__desc, .kt-widget12__value {
            text-align: center;
        }

        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99 list_user
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
    <style type="text/css">
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>

@endsection
@push('scripts')
    <script src="{{ url('public/libs/chartjs/js/Chart.bundle.js') }}"></script>
    <script src="{{ url('public/libs/chartjs/js/utils.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script>
        $(document).ready(function () {
            $('#active_service a').click(function (event) {
                event.preventDefault();
                var object = $(this);
                $.ajax({
                    url: '/admin/service_history/ajax-publish',
                    data: {
                        id: object.data('service_history_id')
                    },
                    success: function (result) {
                        if (result.status == true) {
                            toastr.success(result.msg);
                            object.parents('tr').remove();
                        } else {
                            toastr.error(result.msg);
                        }
                    },
                    error: function (e) {
                        console.log(e.message);
                    }
                })
            });




        })
    </script>
    <script>
        var lineChartData = {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','December'],
            datasets: [{
                label: 'Số lượng hóa đơn',
                borderColor: window.chartColors.red,
                backgroundColor: window.chartColors.red,
                fill: false,
                data: [
                //    dữ liệu của số lượng hóa đơn
                    <?php $total_bill_month=[];
                    for ($i=0;$i<12;$i++){
                        $month = strftime('%m',strtotime(strtotime(date('01')) . " +".$i." month"));
                        $total_bill_month[]= \Modules\WebBill\Models\Bill::select('id')->where('created_at','like','%-'.$month.'-%')
                            ->where('created_at','like',date('Y').'-%')->get()->count();
                        echo($total_bill_month[$i].",");
                    }
                    ?>

                ],
                yAxisID: 'y-axis-1',
            }, {
                label: 'Số tiên thu được',
                borderColor: window.chartColors.blue,
                backgroundColor: window.chartColors.blue,
                fill: false,
                data: [
                //    dữ liệu số tiên thu được theo tháng
                    <?php $total_price1=[];
                        for ($i=0;$i<12;$i++){
                            $month = strftime('%m',strtotime(strtotime(date('01')) . " +".$i." month"));
                            $total_price1[]= \Modules\WebBill\Models\Bill::where('created_at','like','%-'.$month.'-%')->where('created_at','like',date('Y').'-%')->get()->sum('total_price');
                            echo($total_price1[$i].",");
                        }
                    ?>

                ],
                yAxisID: 'y-axis-2'
            }]
        };

        window.onload = function() {
            var ctx = document.getElementById('myChart').getContext('2d');
            window.myLine = Chart.Line(ctx, {
                data: lineChartData,
                options: {
                    responsive: true,
                    hoverMode: 'index',
                    stacked: false,
                    title: {
                        display: true,
                        // text: 'Chart.js Line Chart - Multi Axis'
                    },
                    scales: {
                        yAxes: [{
                            type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                            display: true,
                            position: 'left',
                            id: 'y-axis-1',
                            ticks: {
                                beginAtZero: true,
                            }

                        }, {
                            type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                            display: true,
                            position: 'right',
                            id: 'y-axis-2',
                            ticks: {
                                beginAtZero: true,
                            },
                            // grid line settings
                            gridLines: {
                                drawOnChartArea: false, // only want the grid lines for one axis to show up
                            },
                        }],
                    }
                }
            });
        };
    </script>
@endpush

