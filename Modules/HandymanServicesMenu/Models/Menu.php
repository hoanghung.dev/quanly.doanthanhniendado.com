<?php

namespace Modules\HandymanServicesMenu\Models;

use Illuminate\Database\Eloquent\Model;
//use Modules\EworkingUser\Models\BillPayment;

class Menu extends Model
{

    protected $table = 'menus';

    protected $fillable = [
        'name_vi', 'type', 'url', 'parent_id', 'location','category_post_id' ,'post_id','name_en','created_at','updated_at','name_hu'

    ];


}
