<?php

namespace Modules\HandymanServicesMenu\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class HandymanServicesMenuServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('HandymanServicesMenu', 'Database/Migrations'));
        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Custom setting
            $this->registerPermission();

            //  Cấu hình menu trái
            $this->rendAsideMenu();
        }
    }


    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['menus_view', 'menus_add', 'menus_edit', 'menus_delete', 'menus_publish',]);
            return $per_check;
        }, 1, 1);
    }


    public function rendAsideMenu()
    {
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('handymanservicesmenu::partials.aside_menu.dashboard_after_menu');
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('HandymanServicesMenu', 'Config/config.php') => config_path('handymanservicesmenu.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('HandymanServicesMenu', 'Config/config.php'), 'handymanservicesmenu'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/handymanservicesmenu');

        $sourcePath = module_path('HandymanServicesMenu', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/handymanservicesmenu';
        }, \Config::get('view.paths')), [$sourcePath]), 'handymanservicesmenu');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/handymanservicesmenu');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'handymanservicesmenu');
        } else {
            $this->loadTranslationsFrom(module_path('HandymanServicesMenu', 'Resources/lang'), 'handymanservicesmenu');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('HandymanServicesMenu', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
