<?php
$data[] = old($field['name']) != null ? old($field['name']) : @$field['value'];
$parent_ids = \Modules\HandymanServicesMenu\Models\Menu::where('type','<>',2)->where('status',1)->get();

?>
<label for="type">{{ trans('handymanservicesmenu::admin.choose') }} {{ trans($field['label']) }}</label>
<select class="form-control {{ $field['class'] or '' }}" id="{{ $field['name'] }}"
{{ strpos(@$field['class'], 'require') !== false ? 'required' : '' }}
name="{{ $field['name'] }}">
    <option value=0>Chọn {{ trans($field['label']) }}</option>
    @foreach ($parent_ids as $value => $name)
        <option value='{{ $name->id }}' {{ in_array($name->id, $data) ? 'selected' : '' }}>{{ $name->name_vi }}</option>
    @endforeach
</select>