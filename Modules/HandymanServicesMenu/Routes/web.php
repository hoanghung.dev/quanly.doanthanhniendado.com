<?php
Route::get('location-menu', 'Admin\MenuController@locationMenu');
Route::get('type-menu', 'Admin\MenuController@typeMenu');

Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions','locale']], function () {
    Route::group(['prefix' => 'menus'], function () {
        Route::get('', 'Admin\MenuController@getIndex')->name('menus')->middleware('permission:menus_view');
        Route::match(array('GET', 'POST'), 'add', 'Admin\MenuController@add')->middleware('permission:menus_add');
        Route::get('delete/{id}', 'Admin\MenuController@delete')->middleware('permission:menus_delete');
        Route::post('multi-delete', 'Admin\MenuController@multiDelete')->middleware('permission:menus_delete');
        Route::get('search-for-select2', 'Admin\MenuController@searchForSelect2')->name('menus.search_for_select2')->middleware('permission:menus_view');
        Route::get('{id}', 'Admin\MenuController@update')->middleware('permission:menus_view');
        Route::post('{id}', 'Admin\MenuController@update')->middleware('permission:menus_edit');
    });
});

