<?php

namespace Modules\HandymanServicesTheme\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Validator;

class BannerController extends CURDBaseController
{

    protected $orderByRaw = 'status desc, id desc';

    protected $module = [
        'code' => 'banner',
        'table_name' => 'banners',
        'label' => 'handymanservicestheme::admin.banner',
        'modal' => '\Modules\HandymanServicesTheme\Models\Banner',
        'list' => [
            ['name' => 'image_en', 'type' => 'image', 'label' => 'handymanservicestheme::admin.image', 'width' => '60px'],
            ['name' => 'name', 'type' => 'text_edit_trans', 'label' => 'handymanservicestheme::admin.banner_name'],
//            ['name' => 'name_en', 'type' => 'text', 'label' => 'Tên banner(en)'],
//            ['name' => 'name_hu', 'type' => 'text', 'label' => 'Tên banner(hu)'],
            ['name' => 'location', 'type' => 'select', 'label' => 'handymanservicestheme::admin.location', 'options' => [
                'slide_home' => 'handymanservicestheme::admin.slide_home',
                'banners_home' => 'handymanservicestheme::admin.banners_home',
                'banner_right' => 'handymanservicestheme::admin.banner_right',
                'banner_user' => 'handymanservicestheme::admin.banner_user',
                'banner_news' => 'handymanservicestheme::admin.banner_news',
                'background_image' => 'handymanservicestheme::admin.background_image',
                'banners_partner' => 'handymanservicestheme::admin.banner_partner'
            ],],
            ['name' => 'order_no', 'type' => 'text', 'label' => 'handymanservicestheme::admin.number'],
            ['name' => 'status', 'type' => 'status', 'label' => 'handymanservicestheme::admin.status'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name_vi', 'type' => 'text', 'class' => 'required', 'label' => 'handymanservicestheme::admin.name_vi'],
                ['name' => 'name_en', 'type' => 'text', 'class' => 'required', 'label' => 'handymanservicestheme::admin.name_en'],
                ['name' => 'name_hu', 'type' => 'text', 'class' => 'required', 'label' => 'handymanservicestheme::admin.name_hu'],
                ['name' => 'location', 'type' => 'select', 'options' => [
                    'slide_home' => 'handymanservicestheme::admin.slide_home',
                    'banners_home' => 'handymanservicestheme::admin.banners_home',
                    'banner_right' => 'handymanservicestheme::admin.banner_right',
                    'banner_user' => 'handymanservicestheme::admin.banner_user',
                    'banner_news' => 'handymanservicestheme::admin.banner_news',
                    'background_image' => 'handymanservicestheme::admin.background_image',
                    'banners_partner' => 'handymanservicestheme::admin.banner_partner'
                ], 'label' => 'handymanservicestheme::admin.location'],
                ['name' => 'order_no', 'type' => 'number', 'label' => 'handymanservicestheme::admin.number'],
                ['name' => 'intro_vi', 'type' => 'textarea_editor', 'label' => 'handymanservicestheme::admin.info_vi'],
                ['name' => 'intro_en', 'type' => 'textarea_editor', 'label' => 'handymanservicestheme::admin.info_en'],
                ['name' => 'intro_hu', 'type' => 'textarea_editor', 'label' => 'handymanservicestheme::admin.info_hu'],
                ['name' => 'link', 'type' => 'text', 'label' => 'handymanservicestheme::admin.link'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'handymanservicestheme::admin.status', 'value' => 0],
            ],

            'info_tab' => [
                ['name' => 'image_vi', 'type' => 'file_editor', 'label' => 'handymanservicestheme::admin.image_vi'],
                ['name' => 'image_en', 'type' => 'file_editor', 'label' => 'handymanservicestheme::admin.image_en'],
                ['name' => 'image_hu', 'type' => 'file_editor', 'label' => 'handymanservicestheme::admin.image_hu'],
            ],

        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'handymanservicestheme::admin.banner_name',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'location' => [
            'label' => 'handymanservicestheme::admin.location',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'handymanservicestheme::admin.location',
                'slide_home' => 'handymanservicestheme::admin.slide_home',
                'banners_home' => 'handymanservicestheme::admin.banners_home',
                'banner_right' => 'handymanservicestheme::admin.banner_right',
                'banner_user' => 'handymanservicestheme::admin.banner_user',
                'banner_news' => 'handymanservicestheme::admin.banner_news',
                'background_image' => 'handymanservicestheme::admin.background_image',
                'banners_partner' => 'handymanservicestheme::admin.banner_partner'
            ]
        ],
        'order_no' => [
            'label' => 'handymanservicestheme::admin.number',
            'type' => 'number',
            'query_type' => '='
        ],
    ];

    public function getIndex(Request $request)
    {


        $data = $this->getDataList($request);

        return view('handymanservicestheme::banner.list')->with($data);
    }

    public function add(Request $request)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('handymanservicestheme::banner.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name_vi' => 'required',
                    'name_en' => 'required',
                    'name_hu' => 'required'
                ], [
                    'name_vi.required' => 'Bắt buộc phải nhập',
                    'name_en.required' => 'Bắt buộc phải nhập',
                    'name_hu.required' => 'Bắt buộc phải nhập',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('handymanservicestheme::banner.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'name_vi' => 'required',
                    'name_en' => 'required',
                    'name_hu' => 'required'
                ], [
                    'name_vi.required' => 'Bắt buộc phải nhập',
                    'name_en.required' => 'Bắt buộc phải nhập',
                    'name_hu.required' => 'Bắt buộc phải nhập',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }


    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
