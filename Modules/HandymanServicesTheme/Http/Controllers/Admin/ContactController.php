<?php

namespace Modules\HandymanServicesTheme\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\ThemeHandymanServices\Models\Category;
use Validator;

class ContactController extends CURDBaseController
{
    protected $module = [
        'code' => 'contact',
        'table_name' => 'contacts',
        'label' => 'handymanservicestheme::admin.contact',
        'modal' => '\Modules\HandymanServicesTheme\Models\Contact',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'handymanservicestheme::admin.name'],
            ['name' => 'tel', 'type' => 'text', 'label' => 'handymanservicestheme::admin.phone'],
            ['name' => 'email', 'type' => 'text', 'label' => 'handymanservicestheme::admin.email'],
            ['name' => 'status', 'type' => 'custom','td' => 'handymanservicestheme::td.status', 'label' => 'handymanservicestheme::admin.status'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'handymanservicestheme::admin.name'],
//                ['name' => 'name_en', 'type' => 'text', 'class' => 'required', 'label' => 'handymanservicestheme::admin.name_en'],
//                ['name' => 'name_hu', 'type' => 'text', 'class' => 'required', 'label' => 'handymanservicestheme::admin.name_hu'],
                ['name' => 'tel', 'type' => 'number', 'label' => 'handymanservicestheme::admin.phone'],
                ['name' => 'email', 'type' => 'text', 'label' => 'handymanservicestheme::admin.email'],
                ['name' => 'address', 'type' => 'text', 'label' => 'handymanservicestheme::admin.address'],
//                ['name' => 'gender', 'type' => 'select', 'options' => [
//                    '0' => 'Nam',
//                    '1' => 'Nữ',
//                    '2' => 'Khác',
//                ], 'label' => 'Giới tính'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'handymanservicestheme::admin.seen','value' => 1, 'group_class' => 'col-md-6'],


            ],

            'info_tab' => [
                ['name' => 'content', 'type' => 'textarea', 'label' => 'handymanservicestheme::admin.content'],
//                ['name' => 'content_en', 'type' => 'textarea', 'label' => 'handymanservicestheme::admin.content_en'],
//                ['name' => 'content_hu', 'type' => 'textarea', 'label' => 'handymanservicestheme::admin.content_hu'],

            ],

        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'handymanservicestheme::admin.name',
            'type' => 'text',
            'query_type' => 'like'
        ],
//        'name_en' => [
//            'label' => 'handymanservicestheme::admin.name_en',
//            'type' => 'text',
//            'query_type' => 'like'
//        ],
//        'name_hu' => [
//            'label' => 'handymanservicestheme::admin.name_hu',
//            'type' => 'text',
//            'query_type' => 'like'
//        ],
        'tel' => [
            'label' => 'handymanservicestheme::admin.phone',
            'type' => 'number',
            'query_type' => 'like'
        ],
        'email' => [
            'label' => 'handymanservicestheme::admin.email',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'status' => [
            'label' => 'handymanservicestheme::admin.status',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'handymanservicestheme::admin.status',
                0 => 'handymanservicestheme::admin.no_active',
                1 => 'handymanservicestheme::admin.active',
            ]
        ],
    ];

    public function getIndex(Request $request)
    {

        $data = $this->getDataList($request);

        return view('handymanservicestheme::contact.list')->with($data);
    }
    public function appendWhere($query, $request)
    {
        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
        }

        return $query;
    }
    public function add(Request $request)
    {
        try {

            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('handymanservicestheme::contact.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('handymanservicestheme::contact.edit')->with($data);
            } else if ($_POST) {

                

                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {

                

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {

            

            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {

            

            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
