<?php

namespace Modules\HandymanServicesTheme\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Validator;

class WidgetController extends CURDBaseController
{
    protected $orderByRaw = 'status desc, id desc';

    protected $module = [
        'code' => 'widget',
        'table_name' => 'handymanservicestheme::admin.widget',
        'label' => 'Widget',
        'modal' => '\Modules\HandymanServicesTheme\Models\Widget',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit_trans', 'label' => 'handymanservicestheme::admin.name'],
            ['name' => 'location', 'type' => 'select', 'label' => 'handymanservicestheme::admin.location', 'options' => [
                'home_content' => 'handymanservicestheme::admin.home_content',
                'contact0' => 'handymanservicestheme::admin.contact0',
                'footer_0' => 'handymanservicestheme::admin.footer_0',
                'footer_1' => 'handymanservicestheme::admin.footer_1',
                'footer_2' => 'handymanservicestheme::admin.footer_2',
                'footer_3' => 'handymanservicestheme::admin.footer_3',
                'footer_bot_1' => 'handymanservicestheme::admin.footer_bot_1',
                'footer_bot_2' => 'handymanservicestheme::admin.footer_bot_2',
                'title3' => 'handymanservicestheme::admin.title3',
                'title4' => 'handymanservicestheme::admin.title4',
                'title5' => 'handymanservicestheme::admin.title5',
                'title6' => 'handymanservicestheme::admin.title6',
                'title7' => 'handymanservicestheme::admin.title7',
                'background_image' => 'handymanservicestheme::admin.background_image'
            ],],
            ['name' => 'order_no', 'type' => 'text', 'label' => 'handymanservicestheme::admin.number'],
            ['name' => 'status', 'type' => 'status', 'label' => 'handymanservicestheme::admin.status'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name_vi', 'type' => 'text', 'class' => 'required', 'label' => 'handymanservicestheme::admin.name_vi'],
                ['name' => 'name_en', 'type' => 'text', 'class' => 'required', 'label' => 'handymanservicestheme::admin.name_en'],
                ['name' => 'name_hu', 'type' => 'text', 'class' => 'required', 'label' => 'handymanservicestheme::admin.name_hu'],
                ['name' => 'content_vi', 'type' => 'textarea_editor', 'label' => 'handymanservicestheme::admin.content_vi'],
                ['name' => 'content_en', 'type' => 'textarea_editor', 'label' => 'handymanservicestheme::admin.content_en'],
                ['name' => 'content_hu', 'type' => 'textarea_editor', 'label' => 'handymanservicestheme::admin.content_hu'],
                ['name' => 'order_no', 'type' => 'number', 'label' => 'handymanservicestheme::admin.number'],


            ],

            'info_tab' => [
                ['name' => 'location', 'type' => 'select', 'options' => [
                    'home_content' => 'handymanservicestheme::admin.home_content',
                    'contact0' => 'handymanservicestheme::admin.contact0',
                    'footer_0' => 'handymanservicestheme::admin.footer_0',
                    'footer_1' => 'handymanservicestheme::admin.footer_1',
                    'footer_2' => 'handymanservicestheme::admin.footer_2',
                    'footer_3' => 'handymanservicestheme::admin.footer_3',
                    'footer_bot_1' => 'handymanservicestheme::admin.footer_bot_1',
                    'footer_bot_2' => 'handymanservicestheme::admin.footer_bot_2',
                    'title3' => 'handymanservicestheme::admin.title3',
                    'title4' => 'handymanservicestheme::admin.title4',
                    'title5' => 'handymanservicestheme::admin.title5',
                    'title6' => 'handymanservicestheme::admin.title6',
                    'title7' => 'handymanservicestheme::admin.title7',
                    'background_image' => 'handymanservicestheme::admin.background_image'
                ], 'label' => 'handymanservicestheme::admin.location'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'handymanservicestheme::admin.active', 'value' => 0],


            ],

        ],
    ];

    protected $filter = [
        'name_vi' => [
            'label' => 'handymanservicestheme::admin.name_vi',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'name_en' => [
            'label' => 'handymanservicestheme::admin.name_en',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'name_hu' => [
            'label' => 'handymanservicestheme::admin.name_hu',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'location' => [
            'label' => 'handymanservicestheme::admin.location',
            'type' => 'select',
            'options' => [
                '' => 'handymanservicestheme::admin.status',
                'home_content' => 'handymanservicestheme::admin.home_content',
                'contact0' => 'handymanservicestheme::admin.contact0',
                'footer_0' => 'handymanservicestheme::admin.footer_0',
                'footer_1' => 'handymanservicestheme::admin.footer_1',
                'footer_2' => 'handymanservicestheme::admin.footer_2',
                'footer_3' => 'handymanservicestheme::admin.footer_3',
                'footer_bot_1' => 'handymanservicestheme::admin.footer_bot_1',
                'footer_bot_2' => 'handymanservicestheme::admin.footer_bot_2',
                'title3' => 'handymanservicestheme::admin.title3',
                'title4' => 'handymanservicestheme::admin.title4',
                'title5' => 'handymanservicestheme::admin.title5',
                'title6' => 'handymanservicestheme::admin.title6',
                'title7' => 'handymanservicestheme::admin.title7',
                'background_image' => 'handymanservicestheme::admin.background_image'
            ],
            'query_type' => 'like'
        ],

    ];

    public function getIndex(Request $request)
    {
        

        $data = $this->getDataList($request);

        return view('handymanservicestheme::widget.list')->with($data);
    }

    public function add(Request $request)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('handymanservicestheme::widget.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name_vi' => 'required',
                    'name_en' => 'required',
                    'name_hu' => 'required'
                ], [
                    'name_vi.required' => 'Bắt buộc phải nhập',
                    'name_en.required' => 'Bắt buộc phải nhập',
                    'name_hu.required' => 'Bắt buộc phải nhập',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {
                

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('handymanservicestheme::widget.edit')->with($data);
            } else if ($_POST) {



                $validator = Validator::make($request->all(), [
                    'name_vi' => 'required',
                    'name_en' => 'required',
                    'name_hu' => 'required'
                ], [
                    'name_vi.required' => 'Bắt buộc phải nhập',
                    'name_en.required' => 'Bắt buộc phải nhập',
                    'name_hu.required' => 'Bắt buộc phải nhập',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {

                

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {

                

            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {

                

            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
