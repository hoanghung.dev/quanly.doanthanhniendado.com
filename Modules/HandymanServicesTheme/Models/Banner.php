<?php
namespace Modules\HandymanServicesTheme\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{

    protected $table = 'banners';


    protected $fillable = [
        'name_vi','name_en','name_hu', 'intro', 'link', 'image_vi','image_en','image_hu', 'created_at', 'updated_at', 'location', 'status','order_no'
    ];



}
