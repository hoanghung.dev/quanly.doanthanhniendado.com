<?php

namespace Modules\HandymanServicesTheme\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class HandymanServicesThemeServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('HandymanServicesTheme', 'Database/Migrations'));
        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Custom setting
            $this->registerPermission();


            //  Cấu hình menu trái
            $this->rendAsideMenu();

            //  Đăng ký quyền
            $this->registerPermission();
        }
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['theme']);
            return $per_check;
        }, 1, 1);
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['banner_view', 'banner_add', 'banner_edit', 'banner_delete', 'banner_publish',]);
            return $per_check;
        }, 1, 1);
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['contact_view', 'contact_add', 'contact_edit', 'contact_delete', 'contact_edit',]);
            return $per_check;
        }, 1, 1);
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['post_view', 'post_add', 'post_edit', 'post_delete', 'post_publish',
                'category_post_view', 'category_post_add', 'category_post_edit', 'category_post_delete', 'category_post_publish',
                'tag_post_view', 'tag_post_add', 'tag_post_edit', 'tag_post_delete', 'tag_post_publish',]);
            return $per_check;
        }, 1, 1);

        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['widget_view', 'widget_add', 'widget_edit', 'widget_delete', 'widget_publish',]);
            return $per_check;
        }, 1, 1);


    }

    public function rendAsideMenu() {
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('handymanservicestheme::partials.aside_menu.aside_menu_dashboard_after');
        }, 100, 1);
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('handymanservicestheme::partials.aside_menu.dashboard_after_contact');
        }, 1, 1);
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('handymanservicestheme::partials.aside_menu.dashboard_after_post');
        }, 1, 1);


    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('HandymanServicesTheme', 'Config/config.php') => config_path('handymanservicestheme.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('HandymanServicesTheme', 'Config/config.php'), 'handymanservicestheme'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/handymanservicestheme');

        $sourcePath = module_path('HandymanServicesTheme', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/handymanservicestheme';
        }, \Config::get('view.paths')), [$sourcePath]), 'handymanservicestheme');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/handymanservicestheme');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'handymanservicestheme');
        } else {
            $this->loadTranslationsFrom(module_path('HandymanServicesTheme', 'Resources/lang'), 'handymanservicestheme');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('HandymanServicesTheme', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
