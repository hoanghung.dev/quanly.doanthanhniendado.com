<label for="{{ $field['name'] }}">{{ trans(@$field['label']) }} @if(strpos(@$field['class'], 'require') !== false)
        <span class="color_btd">*</span>@endif</label>
<div class="col-xs-12">
    @include(config('core.admin_theme').".form.fields.select2_ajax_model_trans", ['field' => $field])
    <span class="form-text text-muted">{{trans(@$field['des'])}}</span>
    <span class="text-danger">{{ $errors->first($field['name']) }}</span>
    <span class="form-text text-muted"><a href="/admin/category_post/add" class="btn-popup-create-category_product">{{ trans('handymanservicestheme::admin.create_cate') }}</a></span>
</div>
