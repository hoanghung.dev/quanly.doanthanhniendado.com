<span class="kt-badge {{($item->{$field['name']}==1)?'kt-badge--success' : 'kt-badge--danger'}} kt-badge--inline kt-badge--pill publish" data-url="{{@$field['url']}}" data-id="{{ $item->id }}"
          style="cursor:pointer;" data-column="{{ $field['name'] }}">@if($item->{$field['name']}==1) {{ trans('handymanservicestheme::admin.seen') }} @else {{ trans('handymanservicestheme::admin.not_seen') }} @endif</span>


