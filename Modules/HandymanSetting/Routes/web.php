<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::prefix('handymansetting')->group(function() {
//    Route::get('/', 'HandymanSettingController@index');
//});
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions','locale']], function () {
    Route::group(['prefix' => 'profile'], function () {
        Route::match(array('GET', 'POST'), '', 'HandymanSettingController@profile')->name('admin.profile');
//        Route::match(array('GET', 'POST'), 'change-password', 'HandymanSettingController@changePassword');
//        Route::match(array('GET', 'POST'), '{id}', 'HandymanSettingController@profileAdmin')->name('admin.profile_admin');
    });
});
