<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'banner'], function () {
        Route::get('', 'Admin\BannerController@getIndex')->name('banner')->middleware('permission:banner_view');
        Route::get('publish', 'Admin\BannerController@getPublish')->name('banner.publish')->middleware('permission:banner_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\BannerController@add')->middleware('permission:banner_add');
        Route::get('delete/{id}', 'Admin\BannerController@delete')->middleware('permission:banner_delete');
        Route::post('multi-delete', 'Admin\BannerController@multiDelete')->middleware('permission:banner_delete');
        Route::get('search-for-select2', 'Admin\BannerController@searchForSelect2')->name('banner.search_for_select2')->middleware('permission:banner_view');

        Route::get('{id}', 'Admin\BannerController@update')->middleware('permission:banner_view');
        Route::post('{id}', 'Admin\BannerController@update')->middleware('permission:banner_edit');
    });
});
