<?php

namespace Modules\JdesBill\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\JdesBill\Models\Bill;
use Validator;

class BillController extends CURDBaseController
{

    protected $orderByRaw = 'status ASC';

    protected $module = [
        'code' => 'bill',
        'table_name' => 'bills',
        'label' => 'Đơn hàng',
        'modal' => '\Modules\JdesBill\Models\Bill',
        'list' => [
            ['name' => 'user_name', 'type' => 'text_edit', 'label' => 'Khách hàng'],
            ['name' => 'user_tel', 'type' => 'text', 'label' => 'SĐT'],
            ['name' => 'user_email', 'type' => 'text', 'label' => 'Email'],
            ['name' => 'count_product', 'type' => 'custom', 'td' => 'jdesbill::list.td.count_product', 'label' => 'Tổng SP'],
            ['name' => 'total_price', 'type' => 'price_vi', 'label' => 'Tổng tiền'],
            ['name' => 'updated_at', 'type' => 'datetime_vi', 'label' => 'Ngày đặt'],
            ['name' => 'date', 'type' => 'datetime_vi', 'label' => 'Ngày nhận'],
            ['name' => 'company_id', 'type' => 'relation', 'object' => 'company', 'display_field' => 'name', 'label' => 'C.ty',],
            ['name' => 'status', 'type' => 'select', 'label' => 'Trạng thái', 'options' => [
                0 => 'Mới tạo',
                1 => 'Chờ xưởng duyệt',
                2 => 'Đang làm',
                3 => 'Hoàn thành',
                4 => 'Hủy',
            ],
            ],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'total_price', 'type' => 'text', 'class' => 'number_price', 'label' => 'Tổng tiền'],
//                ['name' => 'coupon_code', 'type' => 'number', 'label' => 'Mã giảm giá', 'group_class' => 'col-md-6'],
                ['name' => 'date', 'type' => 'datetime-local', 'label' => 'Thời gian nhận hàng', 'group_class' => 'col-md-6'],
                ['name' => 'status', 'type' => 'select', 'options' => [
                    0 => 'Mới tạo',
                    1 => 'Chờ xưởng duyệt',
                    2 => 'Đang làm',
                    3 => 'Hoàn thành',
                    4 => 'Hủy',
                ], 'label' => 'Trang thái'],
                ['name' => 'receipt_method', 'type' => 'select', 'options' => [
                    'Thanh toán tiền mặt khi nhận hàng' => 'Thanh toán tiền mặt khi nhận hàng',
                    'Thanh toán bằng thẻ quốc tế Visa, Master, JCB' => 'Thanh toán bằng thẻ quốc tế Visa, Master, JCB',
                    'Thẻ ATM nội địa/Internet Banking (Miễn phí thanh toán)' => 'Thẻ ATM nội địa/Internet Banking (Miễn phí thanh toán)'
                ], 'label' => 'Phương thức thanh toán'],


            ],

            'info_tab' => [
                ['name' => 'user_name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên Khách hàng'],
                ['name' => 'user_tel', 'type' => 'text', 'class' => '', 'label' => 'Số điện thoại'],
                ['name' => 'user_email', 'type' => 'text', 'class' => '', 'label' => 'Email'],
                ['name' => 'user_gender', 'type' => 'text', 'class' => '', 'label' => 'Giới tính'],
                ['name' => 'user_address', 'type' => 'text', 'class' => '', 'label' => 'Địa chỉ nhận hàng'],
                ['name' => 'note', 'type' => 'textarea_editor', 'class' => '', 'label' => 'Chú ý'],
            ],
        ],
    ];

    protected $filter = [
        'user_name' => [
            'label' => 'Tên khách hàng',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'user_tel' => [
            'label' => 'SĐT',
            'type' => 'number',
            'query_type' => 'like'
        ],
        'user_email' => [
            'label' => 'Email',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'total_price' => [
            'label' => 'Tổng tiền',
            'type' => 'number',
            'query_type' => 'like'
        ],
        'updated_at' => [
            'label' => 'Ngày đặt',
            'type' => 'date',
            'query_type' => 'custom'
        ],
        'date' => [
            'label' => 'Ngày nhận',
            'type' => 'date',
            'query_type' => 'custom'
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Vị trí',
                0 => 'Mới tạo',
                1 => 'Chờ xưởng duyệt',
                2 => 'Đang làm',
                3 => 'Hoàn thành',
                4 => 'Hủy',
            ],
        ],

    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('jdesbill::list')->with($data);
    }

    public function print($id, Request $request)
    {
        $data['page_title'] = 'In báo giá';
        $bill = Bill::findOrFail($id);
        $data['result'] = $bill;
        $data['header_print'] = @$bill->company->header_print;
        $data['footer_print'] = @$bill->company->footer_print;
        return view('jdesbill::print')->with($data);
    }

    public function appendWhere($query, $request)
    {
        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
        }

        return $query;
    }

    public function add(Request $request)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('jdesbill::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'user_name' => 'required'
                ], [
                    'user_name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;
                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);


            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('jdesbill::edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'user_name' => 'required'
                ], [
                    'user_name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;


                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return redirect()->back()->withInput();
        }
    }


    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

//            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
