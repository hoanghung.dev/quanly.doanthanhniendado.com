<?php
return [
    'review_of_customer' => 'Ügyfél-visszajelzés',
    'edit' => 'szerkesztése',
    'back' => 'vissza',
    'save' => 'Mentés',
    'save_and_continue' => 'Mentés és folytatás',
    'save_and_quit' => 'Mentés és kilépés',
    'save_and_create' => 'Mentés és létrehozás',
    'basic_information' => 'alapinformációk',
    'search' => 'keresés',
    'action' => 'akció',
    'choose_action' => 'válasszon akciót',
    'export_excel' => 'Exportálja az Excel fájlt',
    'multi_delete' => 'Több törlése',
    'create_new' => 'újat készíteni',
    'filter' => 'szűrő',
    'reset' => 'Visszaállítás',
    'display' => 'kijelző',
    'id' => 'ID',
    'image' => 'kép',
    'name' => 'név',
    'describe' => 'leírni',
    'content' => 'tartalom',

];