<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions','locale']], function () {
    Route::group(['prefix' => 'client_say'], function () {
        Route::get('', 'Admin\ClientSayController@getIndex')->name('client_say')->middleware('permission:client_say_view');
        Route::get('publish', 'Admin\ClientSayController@getPublish')->name('client_say.publish')->middleware('permission:client_say_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ClientSayController@add')->middleware('permission:client_say_add');
        Route::get('delete/{id}', 'Admin\ClientSayController@delete')->middleware('permission:client_say_delete');
        Route::post('multi-delete', 'Admin\ClientSayController@multiDelete')->middleware('permission:client_say_delete');
        Route::get('search-for-select2', 'Admin\ClientSayController@searchForSelect2')->name('client_say.search_for_select2')->middleware('permission:client_say_view');
        Route::get('{id}', 'Admin\ClientSayController@update')->middleware('permission:client_say_view');
        Route::post('{id}', 'Admin\ClientSayController@update')->middleware('permission:client_say_edit');
    });
});
