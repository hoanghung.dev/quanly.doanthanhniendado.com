<?php

namespace Modules\JdesCompany\Http\Controllers\Admin;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;
use App\Mail\MailServer;
use Mail;
use URL;


class MailController extends Controller
{

    protected $_mailSetting;

    public function __construct()
    {
        $this->_mailSetting = Setting::whereIn('type', ['mail'])->pluck('value', 'name')->toArray();
    }
    
    /**
     *  Gửi mail khi mời tham gia công ty
     *
     *
     */
    public function inviteCompanySendMail($model)
    {
        $user = (object)[
            'email' => $model['email'],
            'name' => 'Bạn',
            'company_name' => $model['company_name'],
            'link' => $model['link']
        ];
        $data = [
            'view' => 'eworkingcompany::emails.invite',
            'user' => $user,
            'name' => $this->_mailSetting['mail_name'],
            'subject' => 'Thư mời tham gia công ty!',
        ];
        Mail::to($user)->send(new MailServer($data));
    }

    /**
     *  Gửi mail mời lại thành viên vào công ty
     * Khi click vào chỗ trạng thái của các thành viên
     * $data: bản ghi tài khoản vừa được mời lại
     *
     */
    public function reInviteSendMail($item)
    {
        $user = (object)[
            'email' => $item['email'],
            'name' => $item['name'],
            'company_name' => $item['company_name'],
            'role_name' => $item['role_name'],
            'link' => $item['link']
        ];

        $data = [
            'view' => 'eworkingcompany::emails.re_invite',
            'user' => $user,
            'name' => $this->_mailSetting['mail_name'],
            'subject' => 'Thư mời tham gia lại công ty!',
        ];
        Mail::to($user)->send(new MailServer($data));
    }

    /**
     *  Gửi mail khi kích thành viên khỏi công ty
     * $data: rỗng
     * $admin_id : id của tài khoản bị kick
     * $company_id: id công ty
     *
     */
    public function deActiveAdminFromCompanySendMail($company_id)
    {
        $user = (object)[
            'email' => $company_id['email'],
            'name' => $company_id['name'],
            'company_name' => $company_id['company_name']
        ];

        $data = [
            'view' => 'eworkingcompany::emails.deActive_account_company',
            'user' => $user,
            'name' => $this->_mailSetting['mail_name'],
            'subject' => 'Bạn đã bị kick khỏi công ty ' . $user->company_name . '!',
        ];
        Mail::to($user)->send(new MailServer($data));
    }

    /**
     *  Gửi mail khi có thành viên tham gia công ty nhưng cty đó lại đang trong tình trạng đạt giới hạn thành viên của gói!
     * Khi click vào chỗ chờ duyệt tham gia cty của menu lời mời chờ duyệt
     * $data: bản ghi tài khoản vừa được mời lại
     *
     */
    public function inviteHistorySendMail($item)
    {
        $user = (object)[
            'email' => $item['email'],
            'name' => $item['name'],
            'company_name' => $item['company_name'],
            'role_name' => $item['role_name'],
            'link' => $item['link']
        ];

        $data = [
            'view' => 'eworkingcompany::emails.re_invite',
            'user' => $user,

            'name' => $this->_mailSetting['mail_name'],
            'subject' => 'Vượt quá số lượng thành viên cho phép',
        ];
        Mail::to($user)->send(new MailServer($data));
    }
}
