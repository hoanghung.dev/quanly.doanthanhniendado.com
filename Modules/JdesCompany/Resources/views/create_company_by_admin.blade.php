<?php
?>
@extends(config('core.admin_theme').'.template')
@section('main')

    <!-- begin:: Content -->
    <form class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" action="{{ @$action }}"
          method="POST"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                     id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg" style="">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Tạo mới {{ $module['label'] }}
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="/admin/{{ $module['code'] }}" class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Quay lại</span>
                            </a>
                            <div class="btn-group">
                                @if(in_array($module['code'].'_add', $permissions))
                                    <button type="submit" class="btn btn-brand">
                                        <i class="la la-check"></i>
                                        <span class="kt-hidden-mobile">Lưu</span>
                                    </button>
                                    <button type="button"
                                            class="btn btn-brand dropdown-toggle dropdown-toggle-split"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_continue">
                                                    <i class="kt-nav__link-icon flaticon2-reload"></i>
                                                    Lưu và tiếp tục
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_exit">
                                                    <i class="kt-nav__link-icon flaticon2-power"></i>
                                                    Lưu & Thoát
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_create">
                                                    <i class="kt-nav__link-icon flaticon2-add-1"></i>
                                                    Lưu và tạo mới
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>

        <!--Begin::App-->
        <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">
            <!--Begin:: App Content-->
            <div class="kt-grid__item kt-grid__item--fluid kt-app__content" style="margin: 0">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">Thông tin cơ bản</h3>
                                </div>
                            </div>

                            <div class="kt-form kt-form--label-right">
                                <div class="kt-portlet__body">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            {{-- short_name --}}
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Tên ngắn gọn</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" name="short_name_company"
                                                           class="form-control require" id="short_name_company">
                                                    <span class="text-danger">{{ $errors->first('short_name_company') }}</span>
                                                </div>
                                            </div>
                                            {{-- name --}}
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Tên công ty</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" name="name_company"
                                                           class="form-control require" id="name_company">
                                                    <span class="text-danger">{{ $errors->first('name_company') }}</span>
                                                </div>
                                            </div>
{{--                                            image--}}
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <?php
                                                        $field = ['name' => 'image', 'type' => 'file_image', 'label' => 'Ảnh đại diện', 'value' => ''];
                                                    ?>
                                                    @include(config('core.admin_theme').'.form.fields.file_image', ['field' => $field])
                                                </div>
                                            </div>
                                            {{-- email --}}
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">email</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" name="email" class="form-control" id="email">
                                                </div>
                                            </div>
                                            {{-- Số điện thoại --}}
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Số điện thoại</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" name="tel" class="form-control" id="tel">
                                                </div>
                                            </div>
                                            {{-- Địa chỉ --}}
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Địa chỉ</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" name="address" class="form-control" id="address">
                                                </div>
                                            </div>
                                            {{-- Mã số doanh nghiệp --}}
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Mã số doanh nghiệp</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" name="msdn" class="form-control" id="address">
                                                </div>
                                            </div>
                                            {{-- website --}}
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Website</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" name="website" class="form-control" id="website">
                                                </div>
                                            </div>
                                            {{-- Mô tả --}}
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Mô tả</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <textarea rows="5" class="form-control" name="intro" id="intro"></textarea>
                                                </div>
                                            </div>
                                            {{--   Giám đốc --}}
{{--                                            {{dd(\Modules\EworkingAdmin\Models\Admin::all()}}--}}
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Giám đốc</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <select name="admin_id" class="form-control">
                                                        @foreach(\Modules\EworkingAdmin\Models\Admin::all() as $admin_name)
                                                            <option value="{{ $admin_name->id }}">{{ $admin_name->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End:: App Content-->
        </div>
        <!--End::App-->
    </form>
    <!-- end:: Content -->
@endsection
@section('custom_head')
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset(config('core.admin_asset').'/css/form.css') }}">
@endsection
@push('scripts')

    <script src="{{ asset(config('core.admin_asset').'/js/pages/crud/metronic-datatable/advanced/vertical.js') }}"
            type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce_editor.js') }}"></script>
    <script type="text/javascript">
        editor_config.selector = ".editor";
        editor_config.path_absolute = "{{ (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]" }}/";
        tinymce.init(editor_config);

    </script>
    <script type="text/javascript" src="{{ asset(config('core.admin_asset').'/js/form.js') }}"></script>
@endpush
