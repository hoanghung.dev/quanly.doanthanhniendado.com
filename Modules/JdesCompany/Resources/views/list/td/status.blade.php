@if($item->status == 0)
    <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" data-id="{{ $item->id }}"
          data-column="{{ $field['name'] }}">Admin khóa</span>
@elseif(strtotime($item->exp_date) > time())
    <span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill" data-id="{{ $item->id }}"
          data-column="{{ $field['name'] }}">Hoạt động</span>
@else
    <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" data-id="{{ $item->id }}"
          data-column="{{ $field['name'] }}">Hết hạn</span>
@endif