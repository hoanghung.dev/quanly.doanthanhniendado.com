<li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
            href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i
                                class="kt-menu__link-icon flaticon-network"></i>
                    </span><span class="kt-menu__link-text">Công ty</span><i
                class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            @if(in_array('super_admin', $permissions))
                @if(in_array('super_admin', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/company" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Tất cả C.ty</span></a></li>
                @endif
                @if(in_array('super_admin', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/admin/all" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Tất cả thành viên</span></a></li>
                @endif
                @if(in_array('role_software_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/role_software" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Phân quyền hệ thống</span></a></li>
                @endif
            @else
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/company/{{ \Auth::guard('admin')->user()->last_company_id }}"
                            class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Profile công ty</span></a></li>

                @if(in_array('admin_company_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/admin" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Thành viên</span></a></li>
                @endif
                @if(in_array('role_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/role" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Phân quyền</span></a></li>
                @endif
                @if(in_array('invite_company_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/invite_company" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Mời thành viên</span></a></li>
                @endif
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/invite_history" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Lời mời chờ duyệt</span></a></li>
                @if(in_array('internal_notification_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/internal_notification" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Thông báo nội bộ</span></a></li>
                @endif
            @endif
        </ul>
    </div>
</li>
