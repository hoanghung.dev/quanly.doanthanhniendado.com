<?php

namespace Modules\JdesLabel\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Validator;

class LabelController extends CURDBaseController
{
    protected $module = [
        'code' => 'label',
        'table_name' => 'label',
        'label' => 'Nhãn',
        'modal' => '\Modules\JdesLabel\Models\Label',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên'],
            ['name' => 'code', 'type' => 'text', 'label' => 'Viết tắt'],
            ['name' => 'unit', 'type' => 'text', 'label' => 'Đơn vị'],
            ['name' => 'input_type', 'type' => 'select', 'options' => [
                'input_text' => 'input text',
                'input_number' => 'input number',
                'select' => 'Dropdown'
            ], 'label' => 'Hiển thị'],
            ['name' => 'group_label_ids', 'type' => 'custom', 'td' => 'jdeslabel::td.multi_relation', 'label' => 'Thuộc nhãn', 'filter' => 'text', 'inner' => 'placeholder="group_label_ids"'],
            ['name' => 'tag_ids', 'type' => 'custom', 'td' => 'jdeslabel::td.multi_relation', 'label' => 'Thuộc tag', 'filter' => 'text', 'inner' => 'placeholder="tag_ids"'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'require', 'label' => 'Mô tả kiẻu',],
                ['name' => 'code', 'type' => 'text', 'class' => '', 'label' => 'Viết tắt',],
                ['name' => 'input_type', 'type' => 'select', 'options' => [
                    'input_text' => 'input text',
                    'input_number' => 'input number',
                    'select' => 'Dropdown',
                    'select_tree' => 'Dropdown/Giá trị phụ thuộc'
                ], 'class' => '', 'label' => 'Hiển thị'],
                ['name' => 'tag_ids', 'type' => 'select2_model', 'class' => '', 'label' => 'Thuộc nhóm nhãn',
                    'model' => \Modules\JdesLabel\Models\GroupLabel::class, 'display_field' => 'name', 'where' => 'status=1 AND type=0', 'multiple' => true],
                ['name' => 'group_label_ids', 'type' => 'select2_model', 'class' => '', 'label' => 'Thuộc tags',
                    'model' => \Modules\JdesLabel\Models\GroupLabel::class, 'display_field' => 'name', 'where' => 'status=1 AND type=1', 'multiple' => true],
                ['name' => 'note', 'type' => 'textarea', 'class' => '', 'label' => 'Ghi chú'],
                ['name' => 'status', 'type' => 'checkbox', 'class' => '', 'label' => 'Kích hoạt', 'value' => 1],

            ],

            'info_tab' => [
                ['name' => 'input_number', 'type' => 'number', 'class' => 'form-action', 'label' => 'Giá trị'],
                ['name' => 'unit', 'type' => 'text', 'class' => '', 'label' => 'Đơn vị', 'inner' => 'placeholder="<Theo giá trị>"'],

            ],

        ],
    ];

    protected $filter = [
        'short_name' => [
            'label' => 'Tên nhãn',
            'type' => 'text',
            'query_type' => 'like'
        ],

        'code' => [
            'label' => 'Viết tắt',
            'type' => 'text',
            'query_type' => 'like'
        ],

        'unit' => [
            'label' => 'Đơn vị',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'input_type' => [
            'label' => 'Hiển thị',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Hiển thị',
                0 => 'input text',
                1 => 'input number',
                2 => 'Dropdown'
            ],
        ],
        'group_label_ids' => [
            'label' => 'Thuộc nhãn',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'tag_ids' => [
            'label' => 'Thuộc tag',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Hiển thị',
                'input_text' => 'input text',
                'input_number' => 'input number',
                'select' => 'Dropdown',
                'select_tree' => 'Dropdown/Giá trị phụ thuộc'
            ],
        ],
    ];

    public function getIndex(Request $request)
    {
        

        $data = $this->getDataList($request);

        return view('jdeslabel::list')->with($data);
    }

    public function add(Request $request)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('jdeslabel::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    if ($request->has('tag_ids')) {
                        $data['tag_ids'] = implode('|',$request->tag_ids);
                    }
                    if ($request->has('group_label_ids')) {
                        $data['group_label_ids'] = implode('|',$request->group_label_ids);
                    }
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('jdeslabel::edit')->with($data);
            } else if ($_POST) {

                

                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    if ($request->has('tag_ids')) {
                        $data['tag_ids'] = implode('|',$request->tag_ids);
                    }
                    if ($request->has('group_label_ids')) {
                        $data['group_label_ids'] = implode('|',$request->group_label_ids);
                    }
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {

                

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {

            

            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {

            

            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
