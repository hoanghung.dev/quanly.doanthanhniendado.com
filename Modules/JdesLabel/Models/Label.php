<?php

namespace Modules\JdesLabel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Label extends Model
{

    protected $table = "label";

    public $timestamps = false;

    protected $fillable = [
        'name', 'code', 'unit', 'input_type', 'input_number', 'note', 'image', 'group_label_ids','status','tag_ids'
    ];
    public function group_lable(){
        return $this->belongsTo(GroupLabel::class,'group_label_ids');
    }
    protected $appends = ['groups_label', 'tags'];

    public function getGroupsLabelAttribute()
    {
        if ($this->group_label_ids != null) {
            $group_label_ids = explode('|', $this->group_label_ids);
            unset($group_label_ids[count($group_label_ids) - 1]);
            unset($group_label_ids[0]);
            $items = GroupLabel::whereIn('id', $group_label_ids)->get();
            return count($items) > 0 ? $items : [];
        }
        return [];
    }

    public function getTagsAttribute()
    {
        if ($this->tag_ids != null) {
            $tag_ids = explode('|', $this->tag_ids);
            unset($tag_ids[count($tag_ids) - 1]);
            unset($tag_ids[0]);
            $items = GroupLabel::whereIn('id', $tag_ids)->get();
            return count($items) > 0 ? $items : [];
        }
        return [];
    }


}
