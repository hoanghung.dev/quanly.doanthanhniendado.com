<?php

namespace Modules\JdesLabel\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    protected $table = "tags";

    public $timestamps = false;

    protected $fillable = [
        'name', 'intro','status',
    ];



}
