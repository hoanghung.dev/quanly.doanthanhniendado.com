<?php

Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'menu'], function () {
        Route::get('', 'Admin\MenuController@getIndex')->name('menu')/*->middleware('permission:menu')*/;
    });
});
