<?php

namespace Modules\JdesProduct\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $table = 'products';

    protected $guarded = [];

    protected $fillable = [
        'name', 'image', 'image_extra', 'final_price', 'intro', 'price', 'content', 'category_id'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }


}
