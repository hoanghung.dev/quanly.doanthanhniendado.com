<?php

namespace Modules\JdesSetting\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\JdesBill\Models\Bill;
use Modules\JdesBill\Models\Order;
use Validator;

class FactoryController extends CURDBaseController
{
    protected $orderByRaw = 'status ASC, id DESC';

    protected $module = [
        'code' => 'factory',
        'table_name' => 'products',
        'label' => 'Sản phẩm',
        'modal' => '\Modules\JdesBill\Models\Order',
        'list' => [
            ['name' => 'order_id', 'type' => 'relation', 'object' => 'product', 'display_field' => 'name', 'label' => 'Tên',],
            ['name' => 'product_show_editor', 'type' => 'custom', 'td' => 'jdessetting::list.td.product_show_editor', 'label' => 'Editor',],
            ['name' => 'status', 'type' => 'custom', 'td' => 'jdessetting::list.td.factory_status', 'label' => 'Trạng thái'],
            ['name' => 'bill_id', 'type' => 'relation', 'object' => 'bill', 'display_field' => 'user_name', 'label' => 'Xem đơn'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'short_name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên ngắn gọn'],
                ['name' => 'name', 'type' => 'text', 'class' => '', 'label' => 'Tên đầy đủ'],
                ['name' => 'image', 'type' => 'file_image', 'label' => 'Ảnh đại diện'],
                ['name' => 'msdn', 'type' => 'text', 'label' => 'Mã số thuế'],
                ['name' => 'address', 'type' => 'text', 'label' => 'Địa chỉ'],
            ],
            'image_tab' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Ảnh sản phẩm'],
            ],
            'seo_tab' => [
                ['name' => 'slug', 'type' => 'slug', 'class' => 'required', 'label' => 'Slug', 'des' => 'Đường dẫn sản phẩm trên thanh địa chỉ'],
                ['name' => 'meta_title', 'type' => 'text', 'label' => 'Meta title'],
                ['name' => 'meta_description', 'type' => 'text', 'label' => 'Meta description'],
                ['name' => 'meta_keywords', 'type' => 'text', 'label' => 'Meta keywords'],
            ],
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên đầy đủ',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'msdn' => [
            'label' => 'Mã số doanh nghiệp',
            'type' => 'number',
            'query_type' => 'like'
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('jdessetting::factory.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        $bill_ids = Bill::whereIn('status' , [2, 3])->pluck('id')->toArray();
        $query = $query->where('key_editor_log', '!=', null)->whereIn('bill_id', $bill_ids);
        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('jdessetting::factory.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'short_name' => 'required'
                ], [
                    'short_name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;
                    $data['company_id'] = \Auth::guard('admin')->factory()->last_company_id;
                    $data['admin_id'] = \Auth::guard('admin')->factory()->id;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
            if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
                if (strpos(Auth::guard('admin')->factory()->company_ids, '|' . $item->company_id . '|') === false) {
                    CommonHelper::one_time_message('error', 'Bạn không có quyền!');
                    return back();
                }
            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);

                return view('jdessetting::factory.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'short_name' => 'required'
                ], [
                    'short_name.required' => 'Bắt buộc phải nhập tên gắn gọn',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.',
//                'msg' => $ex->getMessage()
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(Auth::guard('admin')->factory()->company_ids, '|' . $item->company_id . '|') === false) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
                return back();
            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function postSendBill(Request $request) {
        $bill = Bill::find($request->bill_id);
        $bill->status = 1;
        $bill->save();
        CommonHelper::one_time_message('success', 'Đã gửi đơn hàng đến xưởng thành công! Chúng tôi sẽ sớm liên hệ với bạn');
        return response()->json([
            'status' => true,
            'msg' => 'Đã gửi đơn hàng đến xưởng thành công! Chúng tôi sẽ sớm liên hệ với bạn'
        ]);
    }
}
