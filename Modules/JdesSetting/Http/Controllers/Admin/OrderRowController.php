<?php

namespace Modules\JdesSetting\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Modules\JdesSetting\Models\Attribute;
use Modules\JdesSetting\Models\Calculation;
use Modules\JdesLabel\Models\Label;
use Modules\JdesSetting\Models\Editor;
use Modules\JdesSetting\Models\OrderColRule;
use Modules\JdesSetting\Models\OrderCols;
use Modules\JdesSetting\Models\OrderRows;
use Modules\JdesSetting\Models\OrderSetPrice;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Illuminate\Support\Facades\Cache;
use Validator;

class OrderRowController extends CURDBaseController
{

    protected $_base;

    public function __construct()
    {
        parent::__construct();
        $this->_base = new BaseController();
    }

    protected $orderByRaw = 'order_no asc';

    protected $module = [
        'name' => 'order_row',
        'table_name' => 'order_rows',
        'label' => 'Hàng',
        'primaryKey' => 'id',
        'modal' => '\Modules\JdesSetting\Models\OrderRows',
        'list' => [

        ],
        'form' => [
            'view' => 'form_common.simple',
            'tabs' => [
                'general' => [
                    'td' => [
                        ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên hàng'],
                        /*['name' => 'note', 'type' => 'text', 'class' => '', 'label' => 'Ghi chú'],
                        ['name' => 'group_label_ids', 'type' => 'select2_model', 'class' => '', 'label' => 'Lọc theo nhóm nhãn',
                            'model' => \App\Models\GroupLabel::class, 'display_field' => 'name', 'where' => 'type=0', 'multiple' => true],
                        ['name' => 'tag_ids', 'type' => 'select2_model', 'class' => '', 'label' => 'Lọc theo tag',
                            'model' => \App\Models\GroupLabel::class, 'display_field' => 'name', 'where' => 'type=1', 'multiple' => true],
                        ['name' => 'image', 'type' => 'file_image', 'class' => '', 'label' => 'Ảnh'],*/
                    ]
                ]
            ],
        ]
    ];

    public function add(Request $request)
    {
        $data['order_id'] = $request->order_id;
        if (!$_POST) {

        } else {
//            dd($request->all());
            $calculations = Calculation::pluck('unit', 'id');

            //  Lay cac nhan
            $cols_data = [];
            $row_data = [
                'price' => 0,
                'count_col' => 0
            ];
            $next_calculation_id = false;  //  Phép tính cho label tiếp theo
            for ($i = 0; $i <= $request->count_col; $i++) {
                if ($request->get('col' . $i) != null) {    //  Neu đã chọn label thì thực hiện tính toán
                    //  Lấy giá trị col
                    $cols_data[] = $col = [
                        'label_name' => $request->get('col_label_name' . $i, null),
                        'label_id' => $request->get('col' . $i, null),
                        'value' => $request->get('col_value' . $i, null),
                        'calculation_id' => $request->get('col_calculation_id' . $i, null),
                        'order_no' => $request->get('order_no' . $i, null),
                        'note' => $request->get('note' . $i, null),
                    ];

                    if ($col['value'] != null) {    //  Nếu là col đầu tiên thì ko cần tinh toán
                        $row_data['price'] = $this->processingCalculations($row_data['price'], $col['label_id'], $col['value'], $next_calculation_id, $calculations);
                    }
                    //  Lấy phép tính để tính toán cho các col tiếp theo
                    if ($col['calculation_id'] != null) {
                        $next_calculation_id = $col['calculation_id'];
                    }
                }
            }

            $row_data['order_id'] = $request->order_id;
            $row_data['name'] = $request->name;
            $row_data['count_col'] = $request->count_col;
            $row_data['price_text'] = $request->price_text;
            if ($request->has('note')) $row_data['note'] = $request->note;
            $row = OrderRows::create($row_data);

            //  Tao cac col trong row
            $append_col_data = [
                'order_rows_id' => $row->id,
                'order_id' => $request->order_id
            ];
            foreach ($cols_data as $col) {
                OrderCols::create(array_merge($col, $append_col_data));
            }
            $this->updatePriceOrder($request->order_id);
            CommonHelper::one_time_message('success', 'Tạo thành công!');
            return redirect('admin/edit_order/' . $request->order_id);
        }
        $data['module'] = $this->module;
        $data['page_title'] = 'Thêm mới hàng';
        return view('jdessetting::editor.order_row.view')->with($data);
    }

    public function appendData($request, $data, $item = false)
    {
        if ($request->has('image')) {
            $file_name = $request->file('image')->getClientOriginalName();
            $file_name = str_replace(' ', '', $file_name);
            $file_name_insert = date('s_i_') . $file_name;
            $request->file('image')->move(base_path() . '/public/filemanager/userfiles/', $file_name_insert);
            $data['image'] = $file_name_insert;
        }
        return $data;
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->row_id);
        $data['row_id'] = $request->row_id;
        $data['order_id'] = $request->order_id;
        $data['module'] = $this->module;
        $data['page_title'] = 'Thêm mới hàng';
        if (!$_POST) {
            $data['result'] = $item;
            return view('jdessetting::editor.order_row.view')->with($data);
        } else {
            $item->name = $request->name;
            $item->price_text = $request->price_text;
            $item->save();

            if ($request->calculation_id != null) {
                foreach ($request->calculation_id as $col_id => $calculation_id) {
                    OrderCols::where('id', $col_id)->update([
                        'calculation_id' => $calculation_id
                    ]);
                }
            }
//            $this->updatePriceOrderRow($request->order_id, $request->row_id);
//            $this->updatePriceOrder($request->order_id);
            return response()->json([
                'status' => true,
            ]);

            $calculations = Calculation::pluck('unit', 'id');

            //  Lay cac nhan
            $cols_data = [];
            $row_data = [
                'price' => 0,
                'count_col' => 0
            ];
            $next_calculation_id = false;  //  Phép tính cho label tiếp theo
            for ($i = 0; $i <= $request->count_col; $i++) {
                if ($request->get('col' . $i) != null) {    //  Neu đã chọn label thì thực hiện tính toán
                    //  Lấy giá trị col
                    $cols_data[] = $col = [
                        'label_name' => $request->get('col_label_name' . $i, null),
                        'label_id' => $request->get('col' . $i, null),
                        'value' => $request->get('col_value' . $i, null),
                        'calculation_id' => $request->get('col_calculation_id' . $i, null),
                        'order_no' => $request->get('order_no' . $i, null),
                        'note' => $request->get('note' . $i, null),
                    ];

                    if ($col['value'] != null) {    //  Nếu là col đầu tiên thì ko cần tinh toán
                        $row_data['price'] = $this->processingCalculations($row_data['price'], $col['label_id'], $col['value'], $next_calculation_id, $calculations);
                    }
                    //  Lấy phép tính để tính toán cho các col tiếp theo
                    if ($col['calculation_id'] != null) {
                        $next_calculation_id = $col['calculation_id'];
                    }
                }
            }

            $row_data['order_id'] = $request->order_id;
            $row_data['name'] = $request->name;
            $row_data['count_col'] = $request->count_col;
            $row_data['price_text'] = $request->price_text;
            if ($request->has('note')) $row_data['note'] = $request->note;
            $row = OrderRows::find($request->row_id);
            foreach ($row_data as $k => $v) {
                $row->{$k} = $v;
            }
            $row->save();

            //  Tao cac col trong row
            $append_col_data = [
                'order_rows_id' => $row->id,
                'order_id' => $request->order_id
            ];
            //  Xóa col cũ
            OrderCols::where('order_id', $request->order_id)->where('order_rows_id', $row->id)->delete();
            //  Tạo các col mới
            foreach ($cols_data as $col) {
                OrderCols::create(array_merge($col, $append_col_data));
            }
            $this->updatePriceOrder($request->order_id);
            CommonHelper::one_time_message('success', 'Cập nhật thành công!');
            return redirect('admin/order/' . $request->order_id . '/edit_row/' . $request->row_id);
        }
    }

    public function updatePriceOrderRow($order_id, $row_id)
    {
        $row = OrderRows::find($row_id);
        $order = Editor::find($order_id);
        $cols = $row->cols;
        $calculations = Calculation::pluck('unit', 'id');
        $row_data = [
            'price' => 0,
            'count_col' => count($cols),
            'price_calculate' => 1
        ];
        $stop = false;
        $i = 1;
        while (!$stop && $i <= count($cols)) {
            $col = $cols[$i - 1];
            if ($col->value != null) {
                if ($col->calculation_id == 'order_price_division') {
                    $value_col = $this->getValueCol($col->label_id, $col->value);
                    $row_data['price'] = $order->price / $value_col;
                    $row_data['price_calculate'] = 0;   //  ko su dung row nay trong tinh gia order
                    $stop = true;
                } else {
                    $row_data['price'] = $this->processingCalculations($row_data['price'], $col->label_id, $col->value, $col->calculation_id, $calculations);
                }
            }
            $i++;
        }

        foreach ($row_data as $k => $v) {
            $row->{$k} = $v;
        }
        $row->save();
        return true;
    }

    public function delete(Request $request)
    {
        $item = $this->model->find($request->row_id);
        $item->delete();
        $this->updatePriceOrder($request->order_id);
        CommonHelper::one_time_message('success', 'Xóa thành công!');
        return redirect()->back();
    }

    public function deleteCol(Request $request)
    {
        $item = OrderCols::find($request->col_id);
        $row_id = $item->order_rows_id;
        $item->delete();
        $this->updatePriceOrderRow($request->order_id, $row_id);
        $this->updatePriceOrder($request->order_id);
        CommonHelper::one_time_message('success', 'Xóa thành công!');
        return redirect()->back();
    }

    /*
     * Tính giá trị dựa vào calculation đã chọn
     *
     */
    public function processingCalculations($total, $label_id, $value_label, $calculation_id, $calculations)
    {
        try {
            if ($value_label == '' || $calculation_id == null) {
                return $total;
            }
            $value_col = $this->getValueCol($label_id, $value_label);
            if (!$calculation_id || !isset($calculations[$calculation_id])) {
                return $value_col;
            }
            switch ($calculations[$calculation_id]) {
                case '+':
                    return $total + $value_col;
                    break;
                case '-':
                    return $total - $value_col;
                    break;
                case 'x':
                    return $total * $value_col;
                    break;
                case '/':
                    if ($value_col == 0) return $total;
                    return $total / $value_col;
                    break;
                default:
                    return $total;
                    break;
            }
        } catch (\Exception $ex) {
            return $total;
        }
    }

    public function getValueCol($label_id, $value_label)
    {
        $label = Label::find($label_id);
        if ($label->input_type == 'select') {
            $value_col = $value_label;
        } else {
            $value_col = $value_label * $label->input_number;
        }
        return $value_col;
    }

    public function updatePriceOrder($order_id)
    {
        $order = Editor::find($order_id);
        $order->price = OrderRows::where('order_id', $order_id)->where('price_calculate', 1)->sum('price');
        $order->save();
        return true;
    }

    /*public function getTotalPrice($request)
    {
        $cols_data = [];
        $row_data = [
            'price' => 0,
            'count_col' => 0
        ];
        $next_calculation_id = false;  //  Phép tính cho label tiếp theo
        for ($i = 0; $i <= $request->count_col; $i++) {
            if ($request->get('col' . $i) != null) {    //  Neu đã chọn label thì thực hiện tính toán
                //  Lấy giá trị col
                $cols_data[] = $col = [
                    'label_id' => $request->get('col' . $i, null),
                    'value' => $request->get('col_value' . $i, null),
                    'calculation_id' => $request->get('col_calculation_id' . $i, null),
                    'order_no' => $request->get('order_no' . $i, null),
                    'note' => $request->get('note' . $i, null),
                ];

                if ($col['value'] != null) {    //  Nếu là col đầu tiên thì ko cần tinh toán
                    $row_data['price'] = $this->processingCalculations($row_data['price'], $col['label_id'], $col['value'], $next_calculation_id, $calculations);
                }
                //  Lấy phép tính để tính toán cho các col tiếp theo
                if ($col['calculation_id'] != null) {
                    $next_calculation_id = $col['calculation_id'];
                }
            }
        }
    }*/

    public function insertLabelToOrder(Request $request)
    {
        $cols = [];
        $order_no = OrderCols::where('order_id', $request->cols[0]['order_id'])->where('order_rows_id', $request->order_rows_id)->count() + 1;
        if ($request->has('cols') && $request->cols != null && !empty($request->cols)) {
            if (isset($request->cols[0]['multi_value']) && $request->cols[0]['multi_value'] == 'true') {        //  Neu chen 1  nhan voi nhieu gia tri
                $col_data = $request->cols[0];
                $col_data['order_rows_id'] = $request->order_rows_id;
                $col_data['multi_val'] = '';
                foreach ($request->cols as $v) {
                    $col_data['multi_val'] .= $v['label_parameter'] . '|';
                }
                $col_data['multi_val'] = $col_data['multi_val'] != '' ? substr($col_data['multi_val'], 0, -1) : '';
                $col_data['order_no'] = $order_no;
                $col_data['order_id'] = $request->order_id;
                $cols[] = $col = OrderCols::create($col_data);
                $this->autoAddColRelation($col, $request);  //  Them cac cot lien quan
            } else {    //  Neu chen nhieu nhan voi 1 gia tri
                foreach ($request->cols as $col_data) {
                    $col_data['order_rows_id'] = $request->order_rows_id;
                    $col_data['order_no'] = $order_no;
                    $col_data['order_id'] = $request->order_id;
                    $cols[] = OrderCols::create($col_data);
                    $order_no++;
                }
            }
        }
        return response()->json([
            'status' => true,
            'msg' => 'Thành công'
        ]);
    }

    //  Khi  them cot vao bill, se tu dong them cac cot lien quan
    public function autoAddColRelation($col, $request)
    {
        $label = Label::find($col->label_id);
        if ($label->input_type == 'select_tree') {
            $attribute = Attribute::where('key', $col->label_parameter)->where('table', 'label')
                ->where('item_id', $label->id)->whereNull('parent_id')->where('type', 'select_tree')->orderBy('id', 'asc')->first();
            $attribute_childs = $attribute->childs;
            $order_no = $col->order_no + 1;
            foreach ($attribute_childs as $child) {
                OrderCols::create([
                    'value' => $child->value,
                    'order_id' => $col->order_id,
                    'order_rows_id' => $col->order_rows_id,
                    'order_no' => $order_no,
                    'label_name' => $child->key,
                    'label_parameter' => $child->value,
                    'unit' => $child->unit,
                    'from_type' => 'label_attribute',
                    'from_id' => @$col->id,
                ]);
                $order_no++;
            }
        }
    }

    //  Them khoi html cho phan set gia tri col
    public function addItemSetPrice(Request $request)
    {
        $rows = OrderRows::where('order_id', $request->order_id)->orderBy('id', 'asc')->pluck('name', 'id');
        $calculations = Calculation::select('id', 'unit')->get();
        $order = Editor::find($request->order_id);
        return view('jdessetting::editor.order_row.partials.item_set_price', compact('rows', 'calculations', 'order'));
    }

    //  Them khoi html cho phan set quy tac col
    public function addItemSetOrderColRule(Request $request)
    {
        $col = OrderCols::find($request->col_id);
        $rules = OrderColRule::where('col_id', $request->col_id)->get();
        $rule_id = $request->rule_id;
        $order_no = $request->order_no;
        return view('jdessetting::editor.partials.item_set_order_col_rule', compact('rules', 'col', 'order_no', 'rule_id'));
    }

    //  Set gia tri cho col
    public function itemOldSetPrice(Request $request)
    {
        $rows = OrderRows::where('order_id', $request->order_id)->orderBy('id', 'asc')->pluck('name', 'id');
        $calculations = Calculation::select('id', 'unit')->get();
        $order = Editor::find($request->order_id);
        $order_set_price = OrderSetPrice::where('object_id', $request->object_id)->where('object_type', $request->object_type)->orderBy('order_no', 'asc')->get();
        return view('jdessetting::editor.order_row.partials.item_old_set_price', compact('rows', 'calculations', 'order', 'order_set_price'));
    }

    public function getTextItemOldSetPrice(Request $request)
    {
        $order_set_price = OrderSetPrice::where('object_id', $request->object_id)->where('object_type', $request->object_type)->orderBy('order_no', 'asc')->get();
        $html = '';
        foreach ($order_set_price as $set_price) {
            if ($set_price->type == 'col') {
                $html .= @OrderCols::find($set_price->item_id)->label_name . ' ' . $set_price->calculation . ' ';
            } elseif ($set_price->type == 'row') {
                $html .= @OrderRows::find($set_price->item_id)->name . ' ' . $set_price->calculation . ' ';
            } elseif ($set_price->type == 'order') {
                $html .= @Editor::find($set_price->item_id)->name . ' ' . $set_price->calculation . ' ';
            }
        }
        $html = substr($html, 0, -3);
        return is_bool($html) ? '' : '= ' . $html;
    }

    //  get quy tac cu cho col
    public function itemOldSetOrderColRule(Request $request)
    {
        $col = OrderCols::find($request->object_id);
        $rules = OrderColRule::where('col_id', $request->object_id)->get();
        $last_id = OrderColRule::all();
        if (count($last_id) > 0) {
            $last_id = $last_id->last()->id;
        } else {
            $last_id = 0;
        }
        return view('jdessetting::editor.partials.item_old_order_col_rule', compact('col', 'rules', 'last_id'));
    }

    //ajax set role view row
    public function publishRow(Request $request)
    {
        $row = OrderRows::find($request->order_row_id);
        if ($row->publish == 1) {
            $row->publish = 0;
        } else {
            $row->publish = 1;
        }
        $row->save();
        return response()->json([
            'status' => true,
            'msg' => 'Thành công'
        ]);
    }

    //  Set quy tac cho col
    public function postOrderColRule(Request $request)
    {
        // update col
        if ($request->values) {
            foreach ($request->values as $keyId => $value) {
                if ($value[0] == null) {
                    unset($value[0]);
                }
                if (count($value) == 0) {
                    $values = '';
                } else {
                    $values = '|' . implode('|', $value) . '|';
                }
                if (!empty($request->unit_price_id)) {
                    foreach ($request->unit_price_id as $keyUnit => $valueUnit) {
                        if ($keyId == $keyUnit) {
                            $order_col_rule = OrderColRule::find($keyId)->update([
                                'value' => $values,
                                'col_affect' => $request->col_affect,
                                'unit_price_id' => $valueUnit,
                                'order_rows_id' => $request->row_id
                            ]);
                        }
                    }
                }
            }
        }
        // insert col
        if ($request->newValues) {
            foreach ($request->newValues as $keyId => $value) {
                foreach ($request->unit_price_id as $keyUnit => $valueUnit) {
                    if ($keyId == $keyUnit) {
                        OrderColRule::create([
                            'value' => '|' . implode('|', $value) . '|',
                            'col_affect' => $request->col_affect,
                            'unit_price_id' => $valueUnit,
                            'col_id' => $request->object_id,
                            'order_id' => $request->order_id,
                            'order_rows_id' => $request->row_id
                        ]);
                    }
                }
            }
        }
        return redirect()->back();
    }

    public function deleteOrderColRule(Request $request)
    {
        if ($request->dataId == 'undefined') {
            return response()->json([
                'success' => true,
            ]);
        } else {
            OrderColRule::find($request->dataId)->delete();
            return response()->json([
                'success' => true,
            ]);
        }
    }

    public function postItemSetPrice(Request $request)
    {
        $i = 0;
        if (!empty($request->item)) {
            foreach ($request->item as $key => $item) {
                if ($item != null) {
                    $i++;
                    $data['type'] = $request->type[$key];
                    $data['item_id'] = $request->item[$key];
                    $data['calculation_id'] = $request->calculation_id[$key];
                    $data['calculation'] = @Calculation::find($request->calculation_id[$key])->unit;
                    $data['object_type'] = $request->object_type;
                    $orderSetPrice = OrderSetPrice::updateOrCreate([
                        'order_id' => $request->order_id,
                        'object_id' => $request->object_id,
                        'order_no' => $i
                    ], $data);
                    $set_price_update_Ids[] = $orderSetPrice->id;
                }
            };
            OrderSetPrice::where('object_id', $request->object_id)->where('object_type', $request->object_type)->whereNotIn('id', $set_price_update_Ids)->delete();
            $this->setPrice($request->object_id, $request->object_type, $request->unit);
//            $this->updatePriceOrder($request->order_id);
        } else {
            OrderSetPrice::where('object_id', $request->object_id)->where('object_type', $request->object_type)->delete();
            $this->setPrice($request->object_id, $request->object_type, $request->unit);
        }
        return redirect()->back();
    }

    public function getPriceObject($object_id, $object_type, $key) {
        $items = OrderSetPrice::where('object_id', $object_id)->orderBy('order_no', 'asc')->get();
        $next_calculation = '+';
        $total = 0;

        if ($object_type == 'col') {
            $object = OrderCols::find($object_id);
            if (is_object($object)) {
                $editor = $object->order;
            }
        } elseif ($object_type == 'row') {
            $object = OrderRows::find($object_id);
            if (is_object($object)) {
                $editor = $object->order;
            }
        } elseif ($object_type == 'order') {
            $editor = Editor::find($object_id);
        }

        /*if (!isset($editor)) {
            return 0;
        }*/

        //  Lấy bản log của editor
        $filename = base_path() . '/' . config('jdessetting.editor_log_path') . 'editor_id_' . $editor->id . $key . '.txt';
        $col_log = (array) json_decode(@file_get_contents($filename));

        foreach ($items as $item) {
            if ($item->type == 'col') {
                $item_col = @OrderCols::find($item->item_id);  //  Lay gia tri cua order_col

                $item_value = isset($col_log[$item_col->id]) ? (int)$col_log[$item_col->id]->value : (int)$item_col->value;

                switch ($next_calculation) {
                    case '+':
                        $total = $total + $item_value;
                        break;
                    case '-':
                        $total = $total - $item_value;
                        break;
                    case 'x':
                        $total = $total * $item_value;
                        break;
                    case '/':
                        if ($item_value != 0) $total = $total / $item_value;
                        break;
                    default:

                        break;
                }
                $next_calculation = $item->calculation;
            } elseif($item->type == 'row') {
                $item_value = $this->getPriceObject($item->item_id, 'row', $key);

                switch ($next_calculation) {
                    case '+':
                        $total = $total + $item_value;
                        break;
                    case '-':
                        $total = $total - $item_value;
                        break;
                    case 'x':
                        $total = $total * $item_value;
                        break;
                    case '/':
                        if ($item_value == 0) $total = $total;
                        else $total = $total / $item_value;
                        break;
                    default:
                        $total = $total;
                        break;
                }
                $next_calculation = $item->calculation;
            }
        }

        if ($object_type == 'col') {
            $object = OrderCols::find($object_id);
            return isset($col_log[$object->id]) ? (int) @$col_log[$object->id]->value : (int)$object->value;
        } elseif ($object_type == 'row') {
            return $total;
        } elseif ($object_type == 'order') {
            return $total;
        }
        return 0;
    }

    //  Set gia tri cho cac order_col
    public function setPrice($object_id, $object_type, $unit = false)
    {
        $items = OrderSetPrice::where('object_id', $object_id)->orderBy('order_no', 'asc')->get();
        $next_calculation = '+';
        $total = 0;

        foreach ($items as $item) {
            $item_value = @OrderCols::find($item->item_id)->value;  //  Lay gia tri cua order_col
            $item_value = (int)$item_value;
            switch ($next_calculation) {
                case '+':
                    $total = $total + $item_value;
                    break;
                case '-':
                    $total = $total - $item_value;
                    break;
                case 'x':
                    $total = $total * $item_value;
                    break;
                case '/':
                    if ($item_value == 0) $total = $total;
                    else $total = $total / $item_value;
                    break;
                default:
                    $total = $total;
                    break;
            }
            $next_calculation = $item->calculation;
        }
        if ($object_type == 'col') {
            $object = OrderCols::find($object_id);
            if (is_object($object)) {
                $object->label_parameter = $total;
                $object->value = $total;
                if ($unit && $unit != null && $unit != '') {
                    $object->unit = $unit;
                }
            }
        } elseif ($object_type == 'row') {
            $object = OrderRows::find($object_id);
            if (is_object($object)) {
                $object->price = $total;
                if ($unit && $unit != null && $unit != '') {
                    $object->price_text = $unit;
                }
            }
        } elseif ($object_type == 'order') {
            $object = Editor::find($object_id);
            if (is_object($object)) {
                $object->price = $total;
            }
            /*if ($unit && $unit != null && $unit != '') {
                $object->price_text = $unit;
            }*/
        }
        if (is_object($object)) {
//            $object->save();
        }
        return true;
    }

    //  Thay doi nhan trong danh sach bill se tu dong luu & thay doi gia tri cac nhan lien quan
    public function changeValueCol(Request $request)
    {
        $reload = false;
        $col = OrderCols::find($request->col_id);

        $key_log = $request->get('key_log', '_admin_id_' .\Auth::guard('admin')->id());
        $filename = base_path() . '/' . config('jdessetting.editor_log_path') . 'editor_id_' . $col->order->id . $key_log . '.txt';
        $col_log = (array) json_decode(@file_get_contents($filename));

        $col->value = $request->val;
        if (!isset($col_log[$request->col_id])) {
            $col_log[$request->col_id] = new \stdClass();
        }
        $col_log[$request->col_id]->value = $request->val;

        if (@$col->label->input_type == 'select') {  //  Neu thay doi order_col label dang select thi doi label_parameter
            $attribute = Attribute::where('table', 'label')->where('item_id', $col->label->id)->where('type', 'select')->where('key', $col->value)->first();
            $col->label_parameter = $attribute->key;
            $col->value = $attribute->value;

            $col_log[$request->col_id]->label_parameter = $attribute->key;
            $col_log[$request->col_id]->value = $attribute->value;
        }

        //  Neu nhan nay thay doi cac don gia thi thuc hien thay doi
        $order_col_rule = OrderColRule::where('col_id', $request->col_id)->where('value', 'like', '%|' . $request->val . '|%')->first();
        if (is_object($order_col_rule)) {
            //  Thay doi nhan unit price lien quan
            $col_log = $this->changeColUnitPrice($col, $order_col_rule, $col_log);
        }

        $multi_val = $request->val;
        foreach (explode('|', $col->multi_val) as $val) {
            if ($val != $request->val) {
                $multi_val .= '|' . $val;
            }
        }
        $col->multi_val = $multi_val;
        $col_log[$col->id]->multi_val = $multi_val;

        file_put_contents($filename, json_encode($col_log));
//        $col->save();

        if ($col->label->input_type == 'select_tree') { //  Neu nhan vua thay doi la dang gia tri phu thuoc thi thay doi cac nhan phu thuoc no
            $this->autoChangeOrderColLabelItem($col, $col_log);
            $reload = true;
        }

        //  Tu dong thay doi gia tri cac order_col lien quan
        $this->autoChangeOrderColOrderSetPrice($col);
        $reload = true;

        return response()->json([
            'status' => true,
            'msg' => '',
            'reload' => $reload,
        ]);
    }

    //  Thay doi nhan unit price lien quan
    public function changeColUnitPrice($col, $order_col_rule, $col_log)
    {
        $unit_price = Editor::find($order_col_rule->unit_price_id);
        $col_affect = OrderCols::where('order_id', $col->order_id)->where('id', $order_col_rule->col_affect)->first();
        if (is_object($col_affect)) {
            $orderController = new EditorController();
            $orderController->autoChangeOrderColUnitPrice($col, $unit_price, $col_affect->from_id, $col_log);

            $col_affect->from_id = $unit_price->id;
            $col_affect->label_parameter = $unit_price->name;
            $col_affect->value = $unit_price->name;
            $col_affect->save();

            $col_log[$col_affect->id]->from_id = $unit_price->id;
            $col_log[$col_affect->id]->label_parameter = $unit_price->name;
            $col_log[$col_affect->id]->value = $unit_price->name;
        }

        return $col_log;
    }

    public function autoChangeOrderColOrderSetPrice($col, $col_log = [])
    {
        //  Tim cac order_col se bi thay doi gia tri
        $order_col_will_change_price = OrderSetPrice::where('item_id', $col->id)->where('type', 'col')
            ->where('order_id', $col->order_id)->pluck('object_id');
        foreach ($order_col_will_change_price as $order_col_id) {
            $this->setPrice($order_col_id, 'col');
        }
        return true;
    }

    public function autoChangeOrderColLabelItem($col, $col_log = [])
    {
        $attribute_selected = Attribute::where('table', 'label')->where('item_id', $col->label->id)->where('type', 'select_tree')->where('value', $col->value)->first();
        $attributes_relation = $attribute_selected->childs;
        $order_no_arr = OrderCols::where('order_id', $col->order_id)->where('from_type', 'label_attribute')->where('from_id', $col->id)->orderBy('order_no', 'asc')->pluck('order_no')->toArray();
        OrderCols::where('order_id', $col->order_id)->where('from_type', 'label_attribute')->where('from_id', $col->id)->delete();
        $lart_order_no = $col->order_no;
        foreach ($attributes_relation as $attribute_relation) {
            $lart_order_no = isset($order_no_arr[0]) ? $order_no_arr[0] : $col->order_no;
            OrderCols::create([
                'value' => $attribute_relation->value,
                'order_id' => $col->order_id,
                'order_rows_id' => $col->order_rows_id,
                'order_no' => $lart_order_no,
                'label_name' => $attribute_relation->key,
                'label_parameter' => $attribute_relation->value,
                'unit' => $attribute_relation->unit,
                'from_type' => 'label_attribute',
                'from_id' => @$col->id,
            ]);
            unset($order_no_arr[0]);
        }
        return true;
    }

    public function postChangeName($id, Request $request)
    {
        $row = OrderRows::find($request->row_id);
        $row->name = $request->row_name;
        $row->save();
        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
        return response()->json([
            'status' => true,
            'msg' => 'Cập nhật thành công'
        ]);
    }
}
