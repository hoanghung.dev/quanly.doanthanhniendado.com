<?php
namespace Modules\JdesSetting\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\EworkingUser\Models\User;
use Modules\JdesBill\Models\Bill;
use Modules\JdesOrder\Models\Order;

class BillFactory extends Model
{

    protected $table = 'bill_factory';

    protected $fillable = [
        'bill_id' , 'deadline', 'status' , 'price'
    ];

    public function bill() {
        return $this->belongsTo(Bill::class, 'user_id');
    }
}
