<?php
namespace Modules\JdesSetting\Models;

use App\Models\Admin;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Modules\JdesBill\Models\Bill;

class Editor extends Model
{
    protected $table = 'editors';

    protected $fillable = [
        'note', 'name', 'user_id', 'status', 'price', 'admin_id', 'type', 'image', 'group_label_id', 'is_root', 'group_id',
        'factory_id', 'time_date_from', 'time_date_to', 'time_description', 'sale_sale', 'sale_marketing', 'sale_exc', 'sale_status',
        'parent_id', 'plan', 'company_id', 'product_ids'
    ];

    protected $softDelete = true;

    protected $appends = ['order_root'];

    public function getOrderRootAttribute()
    {
        try {
            $group = OrderGroup::find($this->group_id);
            if ($group->parent_id != null) {
                $group = OrderGroup::find($group->parent_id);
            }
        } catch (\Exception $ex) {
            return null;
        }
        return Editor::where('group_id', $group->id)->where('is_root', 1)->where('parent_id', null)->first();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

    public function bill()
    {
        return $this->hasOne(Bill::class, 'order_id', 'id');
    }

    public function rows() {
        return $this->hasMany(OrderRows::class, 'order_id', 'id')->orderBy('order_no', 'asc')->orderBy('id', 'asc');
    }

    public function cols() {
        return $this->hasMany(OrderCols::class, 'order_id', 'id')->orderBy('order_no', 'asc')->orderBy('id', 'asc');
    }

    public function plan_childs() {
        return $this->hasMany($this, 'parent_id', 'id')->select(['id', 'plan', 'plan_active'])->orderBy('id', 'asc');
    }

    public function plan_parent() {
        return $this->hasOne($this, 'id', 'parent_id')->select(['id', 'plan', 'plan_active'])->orderBy('id', 'asc');
    }

    public function group()
    {
        return $this->belongsTo(OrderGroup::class, 'group_id');
    }
    public function order_prices(){
        return $this->hasMany(OrderSetPrice::class, 'order_id', 'id');
    }
}
