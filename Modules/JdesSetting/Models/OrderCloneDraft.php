<?php

/**
 * Page Model
 *
 * Page Model manages page operation. 
 *
 * @category   Page
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license    
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\JdesSetting\Models;

use Illuminate\Database\Eloquent\Model;

class OrderCloneDraft extends Model
{
    protected $table = 'order_clone_draft';

    public $timestamps = false;

    protected $fillable = ['order_id', 'admin_id', 'id_old', 'id_new', 'table_name'];
}
