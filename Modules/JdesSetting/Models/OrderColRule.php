<?php

/**
 * Banners Model
 *
 * Banners Model manages Banners operation. 
 *
 * @category   Banners
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license    
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\JdesSetting\Models;

use Illuminate\Database\Eloquent\Model;

class OrderColRule extends Model
{
	protected $table = 'order_col_rule';

    protected $fillable = [
        'col_id', 'order_id', 'order_rows_id', 'value', 'unit_price_id', 'col_affect'
    ];

    public $timestamps = false;
}
