<?php

namespace Modules\JdesSetting\Models;

use Illuminate\Database\Eloquent\Model;

class OrderGroup extends Model
{
    protected $table = "order_group";

    public $timestamps = false;

    protected $appends = ['order_root', 'order_root_of_group'];

    protected $fillable = [
        'name', 'parent_id'
    ];

    public function orders() {
        return $this->hasMany(Editor::class, 'group_id', 'id')->orderBy('id', 'asc');
    }

    public function getOrderRootAttribute()
    {
        return Editor::where('parent_id', null)->where('group_id', $this->parent_id)->first();
    }

    public function getOrderRootOfGroupAttribute()
    {
        return Editor::where('parent_id', null)->where('group_id', $this->id)->first();
    }
}
