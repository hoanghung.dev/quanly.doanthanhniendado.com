<?php

namespace Modules\JdesSetting\Models;

use Illuminate\Database\Eloquent\Model;

class OrderRows extends Model
{
    protected $table = "order_rows";

    public $timestamps = false;

    protected $fillable = [
        'name', 'order_no', 'order_id', 'note', 'price', 'price_text', 'count_col', 'image', 'role_user'
    ];

    public function order()
    {
        return $this->belongsTo(Editor::class, 'order_id');
    }

    public function cols() {
        return $this->hasMany(OrderCols::class, 'order_rows_id', 'id')->orderBy('order_no', 'asc')->orderBy('id', 'asc');
    }
}
