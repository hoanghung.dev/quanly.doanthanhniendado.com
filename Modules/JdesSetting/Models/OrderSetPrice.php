<?php

namespace Modules\JdesSetting\Models;

use Illuminate\Database\Eloquent\Model;

class OrderSetPrice extends Model
{
    protected $table = 'order_set_price';

    protected $fillable = [
        'order_id', 'type', 'item_id', 'order_no', 'calculation_id', 'calculation', 'object_id', 'object_type', 'value'
    ];

    public $timestamps = false;
}
