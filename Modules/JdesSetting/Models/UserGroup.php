<?php

namespace Modules\JdesSetting\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{

    protected $table = 'user_groups';

    protected $fillable = [
        'name' , 'intro'
    ];

    public function users() {
        return $this->hasMany(User::class, 'group_id', 'id');
    }

}
