<?php
$view_type = isset($_GET['view_type']) ? $_GET['view_type'] : '';
if ($view_type == ''){
    $view_type = Modules\JdesSetting\Models\UnitPriceColumn::where('role_id', $role_id)->get();
    if (count($view_type) > 0){
        $view_type = $view_type->first()->id;
    }
    else{
        $view_type = '';
    }
}
$role_id = @\App\Models\RoleAdmin::where('admin_id', \Auth::guard('admin')->user()->id)->first()->role_id;
if (!empty($view_type)){
    $unit_price_columnsId = Modules\JdesSetting\Models\UnitPriceColumn::find($view_type);
    $ths = \Modules\JdesLabel\Models\Label::whereIn('id', explode('|', $unit_price_columnsId->label_ids))->pluck('name', 'id');
}
else{
    $ths = [];
}
?>
@foreach($ths as $id => $v)
    <th>{{ $v }}</th>
@endforeach
