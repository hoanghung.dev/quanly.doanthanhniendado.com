<td class="item-{{$field['name']}} {{ isset($field['tooltip_info']) ? 'td-tooltip_info' : '' }}">
    @php $data = \Modules\JdesSetting\Models\Attribute::where('table', $module['table_name'])->where('type', $field['name'])->where('item_id', @$item->id)->pluck('value', 'key'); @endphp
    @foreach($data as $k => $v)
        {!! $k !!} : {!! $v !!}<br>
    @endforeach
</td>