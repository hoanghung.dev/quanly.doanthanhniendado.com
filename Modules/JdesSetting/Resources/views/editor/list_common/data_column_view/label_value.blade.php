<td class="item-{{$field['name']}}">{{ $item->value }}
    @if($item->input_type == 'input_number')
        {{ $item->input_number }}
    @elseif($item->input_type == 'select')
        @php $data = \Modules\JdesSetting\Models\Attribute::where('table', $module['table_name'])->where('type', 'select')->where('item_id', @$item->id)->pluck('value', 'key'); @endphp
        @foreach($data as $k => $v)
            {!! $k !!} : {!! $v !!}<br>
        @endforeach
    @endif
</td>