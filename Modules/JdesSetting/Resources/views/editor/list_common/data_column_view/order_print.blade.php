<td class="item-{{$field['name']}}">
    <?php $order = $item;?>
    <div class="col-xs-12 order">
        <?php
        $row_publish_id = \Modules\JdesSetting\Models\OrderRows::where('order_id', $order->id)->where('publish', 1)->pluck('id');
        $cols = \Modules\JdesSetting\Models\OrderCols::select(['label_name', 'multi_val', 'label_id', 'label_parameter', 'unit'])->whereIn('order_rows_id', $row_publish_id)->where('order_id', $order->id)->orderBy('id', 'asc')->get();
        ?>
        @foreach($cols as $col)
            <span>
                <label>{{ $col->label_name }}</label>
                <span>
                                                    @if ($col->value != null && $col->multi_val != null)
                        {{ array_pop(array_reverse(explode('|', $col->multi_val))) }} {{ @$col->label->unit }}
                    @else
                        {{ $col->label_parameter }}{{ isset($col->label) ? @$col->label->unit : $col->unit }}
                    @endif
                                                </span>
            </span>
        @endforeach
    </div>
</td>