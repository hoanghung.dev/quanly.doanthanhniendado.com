<td class="item-{{$field['name']}}">
    @foreach($item->rows_active as $k => $row)
        @if($k == 0)
            {{ $row->name }}
        @else
            , {{ $row->name }}
        @endif
    @endforeach
</td>