<td class="item-{{$field['name']}}">
    <?php $arr_ids = explode('|', $item->{$field['name']});
    $modal = new $field['model'];
    $data = $modal->whereIn('id', $arr_ids)->pluck($field['display_field']);
    ?>
    @foreach($data as $v)
        {{ $v }},
    @endforeach
    @if(isset($field['row_action']))
        @include('admin.list_common.data_column_view.partials.row_actions')
    @endif
</td>