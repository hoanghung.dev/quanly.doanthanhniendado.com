<td class="item-{{$field['name']}}">
    <?php $html = '';?>
    @foreach($item->{$field['object']} as $v)
        <?php $html .= @$v->{$field['display_field']} . ' | ';?>
    @endforeach
    {{ substr($html, 0, -3) }}
</td>