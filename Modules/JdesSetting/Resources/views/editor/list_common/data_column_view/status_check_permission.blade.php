@if(CommonHelper::has_permission(Auth::guard('admin')->user()->id, @$field['check_permission']))
<td style="text-align: center;" id="img{{ $field['name'] }}-{{ $item->id }}"><img
            data-id="{{ $item->id }}" class="publish"
            style="cursor:pointer;" data-column="{{ $field['name'] }}"
            src="@if($item->{$field['name']}==1){{ '/public/images_core/icons/published.png' }}@else{{ '/public/images_core/icons/unpublish.png' }}@endif">
</td>
@endif