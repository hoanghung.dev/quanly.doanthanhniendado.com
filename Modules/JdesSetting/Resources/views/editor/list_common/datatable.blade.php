@extends('admin.template')

@section('main')
    <!-- Content Wrapper. Contains Item content -->
    <div class="content-wrapper">
        <!-- Content Header (Item header) -->
        <section class="content-header">
            <h1>
                {{ $module['label'] }}
                <small>Bảng điều khiển</small>
            </h1>
            @include('admin.common.breadcrumb')
        </section>

        <!-- Main content -->
        <section class="content">
            <section class="vbox">
                <section class="scrollable padder">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Quản lý {{ $module['label'] }}</h3>
                                    <div style="float:right;">
                                        <button class="doSearch btn btn-info">Tìm kiếm</button>
                                        <button class="doExport btn btn-danger">Xuất Excel</button>
                                        @if(CommonHelper::has_permission(Auth::guard('admin')->user()->id, $module['code'].'_add'))
                                            <a class="btn btn-success" href="{{ url('admin/' . $module['code'] . '/add') }}">Tạo
                                                mới {{ $module['label'] }}</a>
                                        @endif
                                        <select class="form-control" name="action">
                                            <option>Hành động khác</option>
                                            <option value="multi_delete">Xóa nhiều</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <form id="search-form" action="{{ route('adm.'.$module['code'].'.list') }}" role="form"
                                          method="GET">
                                        <input type="hidden" name="export" value="">
                                        <table id="listItem" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th width="2%"><input type="checkbox" name="c_all" class="checkbox-master"></th>
                                                <th width="3%">ID</th>
                                                @foreach($module['list'] as $item)
                                                    <th>{{ $item['label'] }}</th>
                                                @endforeach
                                                <th width="5%">#</th>
                                            </tr>
                                            </thead>
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th><input type="text" name="{{ $module['primaryKey'] }}" placeholder=""
                                                           class="form-control"></th>
                                                @foreach($module['list'] as $item)
                                                    @if(isset($filter[$item['name']]))
                                                        @php $field = $filter[$item['name']]; @endphp
                                                        <th>
                                                            @include('admin.common.td.filter.' . $field['type'], ['name' => $item['name'], 'field'  => $field])
                                                        </th>
                                                    @else
                                                        <th></th>
                                                    @endif
                                                @endforeach
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </section>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('public/backend/dist/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            var oTable = null;

            function init() {
                $('.doSearch').on('click', function () {
                    $('input[name=export]').val('');
                    $("#search-form").submit();
                });

                $('.doExport').on('click', function () {
                    $('input[name=export]').val('1');
                    $("#search-form").submit();
                });

                $("#search-form").submit();
            }

            $("#search-form").submit(function (e) {
                if ($('input[name=export]').val() == '') {
                    e.preventDefault();
                    var args = {};
                    $formData = $(this).serializeArray();
                    for (i = 0; i < $formData.length; i++) {
                        args[$formData[i].name] = $formData[i].value;
                    }

                    if ($.fn.dataTable.isDataTable('#listItem')) {
                        oTable.destroy();
                    }

                    oTable = $('#listItem').DataTable({
                        dom: "<'row'<'col-xs-12'<'col-xs-6'l>>r>" +
                            "<'row-items'<'table-responsive't>>" +
                            "<'row'<'col-xs-12'<'col-md-6 col-xs-12'i><'col-md-6 col-xs-12'p>>>",
                        responsive: true,
                        processing: true,
                        serverSide: true,
                        hPaginate: true,
                        language: {
                            "processing": "<div class='loader'> Đang tải...</div>"
                        },
                        ajax:
                            {
                                "url": "{{ '/admin/api/'.$module['code'].'/list' }}",
                                "data": args
                            },
                        columns: [
                            {data: 'checkItems', name: 'checkItems', orderable: false, searchable: false},
                            {data: 'id', name: 'id'},
                                @foreach($module['list'] as $field)
                                @include('admin.list_common.datatable_column_view.' . $field['type'])
                                @endforeach
                            {
                                data: 'actions', name: 'actions', orderable: false, searchable: false
                            },
                        ]
                    });
                }
            });

            init();
        });
    </script>
@endpush
@section('custom_header')
    @if(isset($module['form']['include_header_script']))
        @foreach($module['form']['include_header_script'] as $v)
            <link type="text/css" rel="stylesheet" charset="UTF-8" href="{{ asset($v) }}">
        @endforeach
    @endif
@endsection
@section('custom_footer')
    <script type="text/javascript" src="{{ asset('public/backend/js/list.js') }}"></script>
    @if(isset($module['form']['include_footer_script']))
        @foreach($module['form']['include_footer_script'] as $v)
            <script type="text/javascript" src="{{ asset($v) }}"></script>
        @endforeach
    @endif
    @include('admin.common.js_common')
@endsection