{
data: '{{ $field["name"] }}', name: '{{ $field["name"] }}', render: function (data) {
return '<a href="'+data+'" class="btn btn-xs btn-primary">{{ $field["label"] }}</a>';
}
},