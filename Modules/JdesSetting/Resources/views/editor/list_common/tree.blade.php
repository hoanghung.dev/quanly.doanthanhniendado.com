@extends('admin.template')

@section('main')
    <!-- Content Wrapper. Contains Item content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <section class="vbox">
                <section class="scrollable padder">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">{!! @$module['list']['header_button'] !!}</h3>
                                    @if($permissions['add'])
                                        <div style="float:right;">
                                            <a class="btn btn-success" href="{{ url('admin/' . $module['code'] . '/add') }}">Tạo
                                                mới {{ $module['label'] }}</a>
                                        </div>
                                    @endif
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="col-xs-12" style="padding-left: 0;">
                                        <button class="btn btn-info btn-filter-action">Bộ lọc</button>
                                        <form class="col-xs-12 top-action" method="GET" action="">
                                            <div class="row">
                                                <div class="col-md-1 filter-item">
                                                    {{--Input sort--}}
                                                    @foreach($module['list']['td'] as $k => $field)
                                                        <input name="sorts[]" value="{{ @$_GET['sorts'][$k] }}" class="sort sort-{{ $field['name'] }}" type="hidden">
                                                    @endforeach
                                                    {{--END: Input sort--}}

                                                    <input type="text" name="{{ $module['primaryKey'] }}"
                                                           placeholder="ID"
                                                           value="{{ isset($_GET[$module['primaryKey']]) ? $_GET[$module['primaryKey']] : '' }}"
                                                           class="form-control">
                                                </div>
                                                @foreach($filter as $filter_name => $field)
                                                    <div class="col-md-2 filter-item">
                                                        @include('admin.common.td.filter.' . $field['type'], ['name' => $filter_name, 'field'  => $field])
                                                    </div>
                                                @endforeach
                                                <div class="col-md-1 btn_search filter-item">
                                                    <button class="btn btn-info">Tìm kiếm</button>
                                                </div>
                                                <div class="col-md-1 btn_search filter-item">
                                                    <input name="export" type="submit" value="Xuất Excel"
                                                           class="btn btn-danger">
                                                </div>
                                                <div class="col-md-2 btn_search filter-item">
                                                    <select class="form-control" name="action">
                                                        <option>Hành động khác</option>
                                                        <option value="multi_delete">Xóa nhiều</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <form id="search-form" action="" role="form">
                                        <div class="row-items">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th><input type="checkbox" value="" class="checkbox-master">
                                                        </th>
                                                        @php $count_sort = 0; @endphp
                                                        @foreach($module['list']['td'] as $field)
                                                            <th style="@if(in_array($field['type'], ['status', 'status_hot'])){{'text-align: center;'}}@endif"
                                                                onclick="sort('{{ $field['name'] }}')">{{ $field['label'] }}
                                                                @if(@$_GET['sorts'][$count_sort] == $field['name'].'|asc')
                                                                    <i class="fa fa-sort-asc" aria-hidden="true"></i>
                                                                @elseif(@$_GET['sorts'][$count_sort] == $field['name'].'|desc')
                                                                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                                                                @else
                                                                    <i class="fa fa-sort" aria-hidden="true"></i>
                                                                @endif
                                                            </th>
                                                            @php $count_sort++; @endphp
                                                        @endforeach
                                                        <th style="display: none;"></th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($listItem as $item)
                                                        <tr>
                                                            <td><input type="checkbox" class="ids" name="id[]"
                                                                       value="{{ $item->id }}">
                                                            </td>
                                                            @foreach($module['list']['td'] as $field)
                                                                @include('admin.list_common.data_column_view.'.$field['type'])
                                                            @endforeach
                                                            <td style="display: none;"
                                                                class="id id-{{ $item->id }}">{{ $item->id }}</td>
                                                            <td style="vertical-align: middle;"><a title="Chỉnh sửa"
                                                                        href="{{ url('admin/' . $module['code'] . '/' . $item->id) }}"
                                                                        class="icon-edit"><i
                                                                            class="flaticon-edit"></i></a>
                                                                <a href="{{ url('admin/' . $module['code'] . '/delete' . '/' . $item->id) }}"
                                                                   class="icon-delete delete-warning"><i
                                                                            class="fa fa-trash"
                                                                            style="margin-left: 4px;"></i></a></td>
                                                        </tr>
                                                        @if (isset($show_child))
                                                        {{--Menu level 2--}}
                                                        @php $childs = $item->childs; @endphp
                                                        @foreach($childs as $child)
                                                            @php $inner_html_first = '— '; $item = $child; @endphp
                                                            <tr>
                                                                <td><input type="checkbox" class="ids" name="id[]"
                                                                           value="{{ $child->id }}">
                                                                </td>
                                                                @foreach($module['list']['td'] as $field)
                                                                    @include('admin.list_common.data_column_view.'.$field['type'])
                                                                @endforeach
                                                                <td style="display: none;"
                                                                    class="id id-{{ $child->id }}">{{ $child->id }}</td>
                                                                <td style="vertical-align: middle;"><a title="Chỉnh sửa"
                                                                            href="{{ url('admin/' . $module['code'] . '/' . $child->id) }}"
                                                                            class="icon-edit"><i
                                                                                class="flaticon-edit"></i></a>
                                                                    <a href="{{ url('admin/' . $module['code'] . '/delete' . '/' . $child->id) }}"
                                                                       class="icon-delete delete-warning"><i
                                                                                class="fa fa-trash"
                                                                                style="margin-left: 4px;"></i></a>
                                                                </td>
                                                            </tr>
                                                            {{--Menu level 3--}}
                                                            @php $childs = $child->childs; @endphp
                                                            @foreach($childs as $child)
                                                                @php $inner_html_first = '— — '; $item = $child; @endphp
                                                                <tr>
                                                                    <td><input type="checkbox" class="ids" name="id[]"
                                                                               value="{{ $child->id }}">
                                                                    </td>
                                                                    @foreach($module['list']['td'] as $field)
                                                                        @include('admin.list_common.data_column_view.'.$field['type'])
                                                                    @endforeach
                                                                    <td style="display: none;"
                                                                        class="id id-{{ $child->id }}">{{ $child->id }}</td>
                                                                    <td style="vertical-align: middle;"><a title="Chỉnh sửa"
                                                                                href="{{ url('admin/' . $module['code'] . '/' . $child->id) }}"
                                                                                class="icon-edit"><i
                                                                                    class="flaticon-edit"></i></a>
                                                                        <a href="{{ url('admin/' . $module['code'] . '/delete' . '/' . $child->id) }}"
                                                                           class="icon-delete delete-warning"><i
                                                                                    class="fa fa-trash"
                                                                                    style="margin-left: 4px;"></i></a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            {{--END: Menu level 3--}}
                                                        @endforeach
                                                        @php unset($inner_html_first); @endphp
                                                        {{--END: Menu level 2--}}
                                                        @endif
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                {!! $listItem->appends(isset($param_url) ? $param_url : '')->links() !!}
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </section>
    </div>
@endsection

@push('scripts')
    <script>
        $('.btn-filter-action').click(function () {
            $('form.top-action').slideToggle();
        });
    </script>
@endpush

@section('custom_header')
    <link type="text/css" rel="stylesheet" charset="UTF-8" href="{{ asset('public/backend/css/list.css') }}">
    @if(isset($module['form']['include_header_script']))
        @foreach($module['form']['include_header_script'] as $v)
            <link type="text/css" rel="stylesheet" charset="UTF-8" href="{{ asset($v) }}">
        @endforeach
    @endif
@endsection
@section('custom_footer')
    <script type="text/javascript" src="{{ asset('public/backend/js/list.js') }}"></script>
    @if(isset($module['form']['include_footer_script']))
        @foreach($module['form']['include_footer_script'] as $v)
            <script type="text/javascript" src="{{ asset($v) }}"></script>
        @endforeach
    @endif
    @include('admin.common.js_common')
@endsection