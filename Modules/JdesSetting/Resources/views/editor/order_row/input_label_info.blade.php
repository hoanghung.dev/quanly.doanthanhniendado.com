<style>
    .khung-chon-gia-tri>.col-sm-9>.label-info-div {
        float: left;
    }
</style>
<div class="col-sm-12 col-md-4" style="border-bottom: 1px solid #eeeeee;">
    @if(is_object($label->group_label))
        <div class="item-checkbox-label-info">
            <div class="label-info-div">
                <?php $idRand = 'id' . $label->id . rand(1, 1000);?>
                <label id="{{ $idRand }}">Sản phẩm <input type="checkbox" name="label_info[]" class="label_info"
                                                      data-checkbox_id="{{ $idRand }}"
                                                      value="group_label_id"> {{ @$label->group_label->name }}</label>
            </div>
        </div>
    @endif
    <div class="item-checkbox-label-info">
        <div class="label-info-div">
            <?php $idRand = 'id' . $label->id . rand(1, 1000);?>
            <label id="{{ $idRand }}">Viết tắt <input type="checkbox" name="label_info[]" class="label_info"
                                                      data-checkbox_id="{{ $idRand }}" value="code"> {{ $label->code }}
            </label>
        </div>
    </div>
    <div class="item-checkbox-label-info">
        <div class="label-info-div">
            <?php $idRand = 'id' . $label->id . rand(1, 1000);?>
            <label id="{{ $idRand }}">Tên nhãn <input type="checkbox" name="label_info[]" class="label_info"
                                                      data-checkbox_id="{{ $idRand }}" value="name"> {{ $label->name }}
            </label>
        </div>
    </div>
    <div class="item-checkbox-label-info">
        <div class="label-info-div">
            <?php $idRand = 'id' . $label->id . rand(1, 1000);?>
            <label id="{{ $idRand }}">Icon <input type="checkbox" name="label_info[]" class="label_info"
                                                  data-checkbox_id="{{ $idRand }}" value="image"> {{ @$label->image }}
            </label>
        </div>
    </div>
    <div class="item-checkbox-label-info">
        <div class="label-info-div">
            <?php $idRand = 'id' . $label->id . rand(1, 1000);?>
            <label id="{{ $idRand }}">Đơn vị <input type="checkbox" name="label_info[]" class="label_info"
                                                    data-checkbox_id="{{ $idRand }}" value="unit"> {{ @$label->unit }}
            </label>
        </div>
    </div>
</div>
<div class="col-sm-12 col-md-4" style="border-bottom: 1px solid #eeeeee;">
    <div class="col-xs-12 text-left" style="padding-left: 0 !important;">
        <label >
            Mô tả: {!! $label->note !!}
        </label>
    </div>
</div>
<div class="col-sm-12 col-md-4 text-left khung-chon-gia-tri" style="padding-left: 0 !important;">
    <label class="col-sm-3" >
        Giá trị
    </label>
    <div class="col-sm-9">
        @if ($label->input_type == 'select' || $label->input_type == 'select_tree')
            @php $data = \Modules\JdesSetting\Models\Attribute::where('table', 'label')->where('type', $label->input_type)->whereNull('parent_id')->where('item_id', $label->id)->pluck('value', 'key'); @endphp
            @foreach($data as $k => $v)
                <div class="label-info-div">
                    <?php $idRand = 'id' . $label->id . rand(1, 1000);?>
                    <label id="{{ $idRand }}">{{ $k }} <input type="checkbox" name="label_info_value[]"
                                                              data-checkbox_id="{{ $idRand }}"
                                                              class="label_info label_info_value" value="{{ $v }}"
                                                              data-label_id="{{ $label->id }}"
                                                              data-value="{{ $v }}"
                                                              data-order_id="{{ @$order->id }}"
                                                              data-label_name="{{ $label->name }}"
                                                              data-multi_value="true"
                                                              data-unit="{{ $label->unit }}"
                                                              data-from_type="label"
                                                              data-from_id="{{ $label->id }}"
                                                              data-label_parameter="{{ $k }}"></label>
                </div>
            @endforeach
        @elseif($label->input_type == 'input_number' && $label->input_number != null)
            <div class="label-info-div">
                <?php $idRand = 'id' . $label->id . rand(1, 1000);?>
                <label id="{{ $idRand }}">{{ $label->input_number }} <input type="checkbox" name="label_info_value[]"
                                                                            data-checkbox_id="{{ $idRand }}"
                                                                            class="label_info label_info_value"
                                                                            value="{{ $label->input_number }}"
                                                                            data-label_id="{{ $label->id }}"
                                                                            data-value="{{ $label->input_number }}"
                                                                            data-order_id="{{ @$order->id }}"
                                                                            data-label_name="{{ $label->name }}"
                                                                            data-unit="{{ $label->unit }}"
                                                                            data-from_type="label"
                                                                            data-from_id="{{ $label->id }}"
                                                                            data-label_parameter="{{ $label->input_number }}"></label>
            </div>
        @endif
    </div>
</div>
<script>
    $('#preview-label-name').html('{{ $label->name }}');
    setDataFormInsert('{{ $label->name }}', '{{ $label->id }}', '', '');
    $('#insertBox input[name=unit]').val('{{ $label->unit }}');
    $('#preview-label-unit').html('{{ $label->unit }}');
</script>
