<div class="tab-pane fade in active" id="{{ $tab_code }}">
    <div class="col-sm-4 unit_price_column1 border-unit-price-colum" style="">
        <?php $cols = \Modules\JdesSetting\Models\OrderCols::where('order_id', $unit_price->id)->where('column', 1)->orderBy('order_no', 'asc')->get();
        ?>
        @foreach($cols as $k => $col)
            @include('jdessetting::editor.partials.input_unit_price_info_display', ['col' => $col, 'col_from' => 'unit_price'])
        @endforeach
    </div>
    <div class="col-sm-4 unit_price_column2 border-unit-price-colum">
        <?php $cols = \Modules\JdesSetting\Models\OrderCols::where('order_id', $unit_price->id)->where('column', 2)->orderBy('order_no', 'asc')->get();
        ?>
        @foreach($cols as $k => $col)
            @include('jdessetting::editor.partials.input_unit_price_info_display', ['col' => $col, 'col_from' => 'unit_price'])
        @endforeach
    </div>
    <div class="col-sm-4 unit_price_column3 border-unit-price-colum">
        <?php $cols = \Modules\JdesSetting\Models\OrderCols::where('order_id', $unit_price->id)->where('column', 3)->orderBy('order_no', 'asc')->get();
        ?>
        @foreach($cols as $k => $col)
            @if($col->type != 0 && in_array($col->type, [1, 3])) {{--'Nhãn', 'Nhãn phù hợp', 'Giá trị phù hợp', 'Loại trừ nhãn', 'Loại trừ giá trị'--}}
            <?php
            $values = explode('|', $col->value);
            $data_select = \Modules\JdesLabel\Models\Label::whereIn('id', $values)->pluck('name', 'id')->toArray();
            ?>
            <div class="item-checkbox-label-info">
                <div class="label-info-div">
                    <?php $idRand = 'id' . $col->id . rand(1,1000);?>
                    <label id="{{ $idRand }}">{{ $col->label_name }} <input type="checkbox" name="label_info[]" class="label_info" data-checkbox_id="{{ $idRand }}"
                                                                            value="name">
                        @foreach($data_select as $k => $v)
                            {{ $v }},
                        @endforeach
                    </label>
                </div>
            </div>
            @else
                @include('jdessetting::editor.partials.input_unit_price_info_display', ['col' => $col, 'order_id' => $order->id, 'col_from' => 'unit_price'])
            @endif
        @endforeach
    </div>
</div>
<style>
    .border-unit-price-colum {
        padding-right: 0;
        margin: 5px;
        border: 1px solid #eeeeee;
        width: 32%;
        min-height: 100px;
    }
</style>