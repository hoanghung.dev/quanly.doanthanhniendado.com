@if(count($labels) > 0)
    <option selected value="0">Chọn nhãn</option>
    @foreach($labels as $row)
        <option value="{{$row->id}}" data-select2-id="{{$row->id}}">{{$row->name}}</option>
    @endforeach
@else
    <option selected value="0">Không có nhãn</option>
@endif
<script>
    $(document).ready(function() {
        $('.select2-label_id').select2();
    });
</script>
