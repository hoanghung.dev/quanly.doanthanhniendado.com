@foreach($order_set_price as $set_price)
    <div class="set_price_item">
        <input type="hidden" name="type[]" class="set_price_type" value="{{ $set_price->type }}">
        <div class="calculation-container">
            <div class="col-xs-10" style="padding-right: 0">
                <select name="item[]" class="form-control set_price_item">
                    <option value="">Chọn nhãn</option>
                    @foreach($rows as $id => $name)
                        <option value="{{ $id }}" data-type="row"
                                @if($set_price->item_id == $id && $set_price->type == 'row') selected @endif>Giá trị
                            dòng: {{ $name }}</option>
                        <?php $cols = \Modules\JdesSetting\Models\OrderCols::where('order_id', $order->id)->where('order_rows_id', $id)->orderBy('id', 'asc')->get();?>
                        @foreach($cols as $col)
                            <option value="{{ $col->id }}" data-type="col"
                                    @if($set_price->item_id == $col->id && $set_price->type == 'col') selected @endif>
                                — {{ @$col->label_name }}
                                : {{ $col->label_parameter }}{{ @$col->label->unit }}</option>
                        @endforeach
                    @endforeach
                    <option value="order_price" data-type="order"
                            @if($set_price->item_id == $order->id && $set_price->type == 'order') selected @endif>Tổng
                        giá: {{ number_format($order->price, 0, '.', '.') }}</option>
                </select>
                <select name="calculation_id[]" class="form-control" style="width: 50%; float: right">
                    <option value="">Chọn toán tử</option>
                    @foreach($calculations as $v)
                        <option value="{{ $v->id }}"
                                @if($v->unit == $set_price->calculation) selected @endif>{{ $v->unit }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-xs-2">
                <button class="btn btn-danger delete-set_price" title="Xóa" type="button"><i
                            class="fa fa-trash"></i></button>
            </div>
        </div>
    </div>
@endforeach