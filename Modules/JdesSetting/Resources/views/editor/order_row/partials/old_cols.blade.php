@foreach($cols as $k => $col)
    <div class="col-xs-12 item-col item-col">
        <div class="col-sm-1"></div>
        <div class="col-sm-2 border-item-col" style="border-left: 1px solid #a9a6a6;">
            {{ @$col->label_name }}
        </div>
        <div class="col-sm-2 border-item-col">
            {{ $col->label_parameter }}
        </div>
        <div class="col-sm-2 border-item-col">
            <label>{{ @number_format($col->value, 0, '.', '.') }}</label>@if($col->multi_val != '')<br>
            ({{ $col->multi_val }})@endif
        </div>
        <div class="col-sm-1 border-item-col">
            {{ $col->unit !== null ? $col->unit : @$col->label->unit }}
        </div>
        {{--<div class="col-sm-1">

                <select name="calculation_id[{{ $col->id }}]">
                    <option value="">Chọn toán tử</option>
                    @foreach($calculations as $v)
                        <option value="{{ $v->id }}" @if($col->calculation_id == $v->id) selected @endif>{{ $v->unit }}</option>
                    @endforeach
                    <option value="order_price_division" @if($col->calculation_id == 'order_price_division') selected @endif>Tổng giá/P.Tử này</option>
                </select>

        </div>--}}
        <div class="col-sm-2 border-item-col text-left" style="">
            <a href="#setPriceCol" role="button" title="Set giá trị" class="btn btn-success setPriceCol_btn" data-object_id="{{ $col->id }}"
               data-object_type="col" data-row_id="{{ $col->order_rows_id }}"
               data-unit="{{ $col->unit != null ? $col->unit : @$col->label->unit }}"
               data-toggle="modal">$</a>
            <a href="#editOrderCol" title="Chỉnh sửa cột" role="button" class="btn btn-success editOrderCol_btn"
               data-object_id="{{ $col->id }}" data-object_type="col"
               data-col_id="{{ $col->id }}"
               data-label_name="{{ $col->label_name }}"
               data-label_parameter="{{ $col->label_parameter }}"
               data-value="{{ $col->value }}"
               data-unit="{{ $col->unit }}"
               data-col_name="{{ $col->label_parameter }}"
               data-toggle="modal"><i class="flaticon-edit"></i></a>
            <a class="btn btn-danger delete-col" title="Xóa" href="/admin/editor/{{ $order->id }}/delete_col/{{ $col->id }}"><i
                        class="fa fa-trash" style="margin-left: 4px;"></i></a>
        </div>
    </div>
@endforeach
@include('jdessetting::editor.order_row.popup.set_price_col')
@include('jdessetting::editor.order_row.popup.edit_order_col')
