<div id="editOrderCol" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="width:100%;height:100%">

        </div>
    </div>
</div>
<script>
    $('.editOrderCol_btn').click(function () {
        loading();
        var col_id = $(this).data('col_id');
        $.ajax({
            url: '/admin/editor/get-html-order-col',
            type: 'GET',
            data: {
                col_id: col_id
            },
            success: function (resp) {
                stopLoading();
                $('#editOrderCol .modal-content').html(resp);
            },
            error: function () {
                stopLoading();
                alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại');
            }
        });
        // $('#editOrderCol input[name=col_id]').val($(this).data('col_id'));
        // $('#editOrderCol input[name=label_name]').val($(this).data('label_name'));
        // $('#editOrderCol input[name=label_parameter]').val($(this).data('label_parameter'));
        // $('#editOrderCol input[name=value]').val($(this).data('value'));
        // $('#editOrderCol input[name=unit]').val($(this).data('unit'));
    });
</script>