<form method="POST" action="/admin/editor/edit-order-col">
    {!! csrf_field() !!}
    <input type="hidden" name="col_id" value="{{ $col->id }}">
    <div class="modal-header">
        <h4 class="modal-title" id="editOrderColLabel">Sửa cột: {{ $col->label_name }}</h4>
        <button type="button" class="close" data-dismiss="modal"></button>
    </div>
    <div class="modal-body">
        <div class="form-group">
            <label class="col-sm-3 control-label text-left">Tên</label>
            <div class="col-sm-12">
                <input type="text" name="label_name" class="form-control" value="{{ $col->label_name }}"
                       placeholder="Tên">
            </div>
        </div>
        @if($col->from_type == 'unit_price')
            <?php $field = ['name' => 'value', 'type' => 'select2_model', 'class' => '', 'label' => 'Đơn giá',
                'model' => \Modules\JdesSetting\Models\Editor::class, 'display_field' => 'name', 'where' => 'type=1', 'value' => $col->from_id];?>
            @if($field['type'] == 'custom')
                                                            @include($field['field'], ['field' => $field])
                                                        @else
                                                            @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                        @endif
        @else
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">Tham số</label>
                <div class="col-sm-12">
                    <input type="text" name="label_parameter" class="form-control" value="{{ $col->label_parameter }}"
                           placeholder="Tham số(Hiển thị)">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">Giá trị</label>
                <div class="col-sm-12">
                    <input type="text" name="value" class="form-control" value="{{ $col->value }}"
                           placeholder="Giá trị thực">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">Đơn vị</label>
                <div class="col-sm-12">
                    <input type="text" name="unit" class="form-control"
                           value="{{ $col->unit != null ? $col->unit : @$col->label->unit }}" placeholder="Đơn vị">
                </div>
            </div>
        @endif
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" type="button"
                aria-hidden="true">Quay lại
        </button>
        <button class="btn btn-primary" type="submit" id="submit-set_price">Áp dụng
        </button>
    </div>
</form>