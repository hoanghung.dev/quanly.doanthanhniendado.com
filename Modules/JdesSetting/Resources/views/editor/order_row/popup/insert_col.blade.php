<div id="insertBox" class="modal fade" role="dialog">
    <div class="modal-dialog" style="max-width: unset;width: 80%;">
        <div class="modal-content" style="width:100%;height:100%">
            <div class="modal-header">
                <h4 class="modal-title">Chèn thêm nhãn</h4>
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="modal-body">
                {{--<input type="hidden" name="label_name" value="">--}}
                <input type="hidden" name="value" value="">
                <input type="hidden" name="label_id" value="">
                <input type="hidden" name="unit" value="">
                <input type="hidden" name="label_parameter" value="">
                <input type="hidden" name="order_rows_id" value="">
                <div class="col-xs-12">
                    <label class="col-xs-5 text-left" style="padding-top: 10px">Tên nhãn</label>
                    <label class="col-xs-7"><input class="form-control" name="label_name" value=""></label>
                </div>
                <div class="col-xs-12 bg-black-opa">
                    <label>
                        1. Nguồn dữ liệu
                    </label>
                    <div>
                        <label><input type="radio" name="from" value="system"
                                      checked>Nhãn \</label>
                        <label><input type="radio" name="from"
                                      value="unit_price_object">Đơn giá \</label>
                        <label><input type="radio" name="from"
                                      value="order">Báo giá hiện tại</label>
                    </div>
                    <div class="system-area unit_price-area block-area">
                        <div class="row pr-4 pb-4">
                            <div class="col-sm-6">
                                <?php $field = ['name' => 'group_label_id', 'type' => 'custom', 'field' => 'jdessetting::form.fields.group_label', 'class' => 'required', 'label' => 'Nhóm nhãn',
                                    'multiple' => true];?>
                                @include($field['field'], ['field' => $field, 'name' => 'group_label_id'])
                            </div>
                            <div class="col-sm-6 ">
                                <?php $field = ['name' => 'label_id', 'type' => 'select2_model', 'class' => 'required', 'label' => 'Nhãn',
                                    'model' => \Modules\JdesLabel\Models\Label::class, 'display_field' => 'name'];?>
                                @if($field['type'] == 'custom')
                                    @include($field['field'], ['field' => $field])
                                @else
                                    @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="unit_price_object-area block-area" style="display: none;">
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $field = ['name' => 'group_price_id', 'type' => 'custom', 'field' => 'jdessetting::form.fields.group_label', 'class' => 'required', 'label' => 'Nhóm nhãn',
                                    'multiple' => true];?>
                                @include($field['field'], ['field' => $field, 'name' => 'group_price_id'])
                            </div>
                            <div class="col-sm-6">
                                <?php $field = ['name' => 'unit_price_object_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Đơn giá',
                                    'model' => \Modules\JdesSetting\Models\Editor::class, 'display_field' => 'name', 'where' => 'type=1'];?>
                                @if($field['type'] == 'custom')
                                    @include($field['field'], ['field' => $field])
                                @else
                                    @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 no-padding unit_price-area block-area" style="margin-top: 4px; display: none;">
                    <div class="unit_price-content w-con">
                        <div class="col-xs-12 no-padding d-flex" style="margin-bottom: 6px;">
                            <div class="col-xs-12 bg-pink"
                                 style="margin-right: 6px;">
                                <div class="col-sm-3">
                                    <label>2. Kho giá từ nhãn có số
                                        lượng</label>
                                </div>
                                <div class="col-sm-9 input-label-value input-tennhan">
                                    {{--Hien thi cac checkbox gia tri cua nhan--}}
                                </div>
                            </div>
                            <div class="col-xs-12 bg-grey-blue">
                                <div class="col-sm-3">
                                    <label>3. Chọn máy in</label>
                                </div>
                                <div class="col-sm-7">
                                    <?php $field = ['name' => 'unit_price_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Group',
                                        'model' => \Modules\JdesSetting\Models\Editor::class, 'display_field' => 'name', 'where' => "status in ('pending', 'active') AND type = 'order_template'"];?>
                                    @if($field['type'] == 'custom')
                                        @include($field['field'], ['field' => $field])
                                    @else
                                        @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                    @endif
                                </div>
                                <div class="col-sm-2">
                                    <button class="chon_xuong" type="button">Chọn xuống</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="unit_price-tab-name-data" style="display: none"></div>
                <div class="input-label-content">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 input-label-info system-area block-area">
                            {{--JS Hien thi cac checkbox thong tin nhan--}}
                        </div>
                        <div class="col-xs-12 col-md-12 input-label-info order-area block-area khung-chon-gia-tri label-left-ts"
                             style="display: none">
                            {{--JS Hien thi cac checkbox thong tin nhan--}}
                        </div>
                        <div class="unit_price-area block-area" style="display: none">
                            <ul id="chooseUnitPriceTab" class="nav nav-tabs">

                            </ul>
                            <div id="chooseUnitPriceTabContent"
                                 class="col-xs-12 col-md-12 tab-content input-label-info khung-chon-gia-tri">
                                {{--JS Hien thi cac checkbox thong tin nhan--}}
                            </div>
                        </div>
                        <div class="unit_price_object-area block-area" style="display: none">
                            <ul id="chooseUnitPriceTab" class="nav nav-tabs">

                            </ul>
                            <div id="unit_price_object-TabContent"
                                 class="col-xs-12 col-md-12 tab-content input-label-info khung-chon-gia-tri">
                                {{--JS Hien thi cac checkbox thong tin nhan--}}
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12 w-container">
                            <div class="col-sm-6 bg-pink">
                                <label>2. Kho giá trị sử dụng Chọn 1 hoặc nhiều
                                    để tạo
                                    một nhiều nhóm</label>
                                <div class="col-xs-12">
                                    <div class="col-sm-3 form-gan">
                                        <input type="text" name="name"
                                               class="form-control"
                                               placeholder="Đặt tên">
                                        <button type="button" class="btn btn-danger mt-2 gan  p-1 pl-3 pr-3">Gán
                                        </button>
                                    </div>

                                    <div class="col-sm-9 input-label-selected" id=""
                                         style="border: 1px solid #eeeeee;min-height: 54px;background: #fff;"></div>
                                    {{--Hien thi cac checkbox thon tin da chon o tren--}}
                                </div>
                            </div>
                            <div class="col-sm-6 bg-grey-blue">
                                <div style="display: flex">
                                    <label style="width: 24%">3. Kiểu hiện thị</label>
                                    <div style="width: 75%">
                                        <div class="col-sm-3"><label><input
                                                        type="radio"
                                                        name="display_type"
                                                        checked>Nhãn</label>
                                        </div>
                                        <div class="col-sm-3" style="padding: 0 !important;"><label><input
                                                        type="radio"
                                                        name="display_type">Tính
                                                toán</label></div>
                                        <div class="col-sm-3" style="padding: 0 !important;"><label><input
                                                        type="radio"
                                                        name="display_type">Kết
                                                quả</label></div>
                                        <div class="col-sm-3" style="padding: 0 !important;"><label><input
                                                        type="radio"
                                                        name="display_type">Gộp
                                                nhãn</label></div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="col-sm-3">
                                        <label style="min-width: 70px !important;">Xem trước: </label>
                                    </div>
                                    <div class="col-sm-9 input-label-result" style="background: #fff; min-height: 54px">
                                        <span id="preview-label-name"></span>
                                        <span
                                                id="preview-label-value"></span>
                                        <span
                                                id="preview-label-unit"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" id="btn-apply"
                        data-dismiss="modal" type="button">Áp dụng
                </button>
                <a class="btn btn-default" style="
    border: 1px solid #ccc;
    height: auto;
    padding: 9px 15px;
    cursor: pointer;
" data-dismiss="modal">Trở về</a>
            </div>
        </div>
    </div>
</div>




