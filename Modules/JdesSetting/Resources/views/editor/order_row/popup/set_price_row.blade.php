<div id="setPriceRow" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="setPriceRowLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="setPriceRowLabel">Đặt giá cho hàng</h3>
    </div>
    <div class="modal-body">
        <form action="" method="POST">
            <input type="hidden" name="row_id" value="">

        </form>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Quay lại</button>
        <button class="btn btn-primary">Lưu lại</button>
    </div>
</div>
