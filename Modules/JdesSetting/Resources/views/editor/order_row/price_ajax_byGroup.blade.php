@if(count($labels) > 0)
    <option selected value="0">Chọn đơn giá</option>
    @foreach($labels as $row)
        <option value="{{$row->id}}" data-select2-id="{{$row->id}}">{{$row->name}}</option>
    @endforeach
@else
    <option selected value="0">Không có đơn giá nào</option>
@endif
<script>
    $(document).ready(function() {
        $('.select2-unit_price_object_id').select2();
    });
</script>
