@extends('admin.template')
@section('main')
    <style>
        .border-item-col-title {
            border: 1px solid #a9a6a6;
            min-height: 26px;
            background: #e9fd3f;
        }

        .border-item-col {
            height: 46px;
            overflow: auto;
            border-right: 1px solid #a9a6a6;
            border-bottom: 1px solid #a9a6a6;
        }
    </style>
    <?php
    $calculations = \Modules\JdesSetting\Models\Calculation::select('id', 'unit')->get();
    $order = \Modules\JdesSetting\Models\Editor::find($order->id);
    ?>
    <div class="content-wrapper form-primary">
        <section class="content-header">
            <h1>
                {{ $page_title }}
            </h1>
            @include('admin.common.breadcrumb')
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div
                                class="form-horizontal" enctype="multipart/form-data">
                            <input type="hidden" name="row_id" value="{{ $row_id }}">
                            <input type="hidden" name="order_id" value="{{ $order->id }}">
                            <div class="box-body">
                                <div class="col-sm-9">
                                    <?php
                                    $fields = [];
                                    foreach ($module['form']['tabs'] as $tab_code => $tab) {
                                        foreach ($tab['td'] as $k => $field) {
                                            $field['value'] = @$order->{$field['name']};
                                            $tab['td'][$k] = $field;
                                            $fields[] = $field;
                                        }
                                        $module['form']['tabs'][$tab_code] = $tab;
                                    }
                                    ?>
                                    @foreach($module['form']['tabs'] as $key => $tab)
                                        @foreach($tab['td'] as $field)
                                            @if($field['type'] == 'custom')
                                                            @include($field['field'], ['field' => $field])
                                                        @else
                                                            @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                        @endif
                                        @endforeach
                                    @endforeach
                                </div>
                                <div class="col-xs-12"><h3 class="text-center">Các cột trong hàng</h3></div>
                                <?php $row = \Modules\JdesSetting\Models\OrderRows::find(@$row_id);
                                $cols = [];
                                if (is_object($row)) {
                                    $cols = $row->cols;
                                }
                                ?>
                                @if(count($cols) == 0)
                                    <div class="col-xs-12">
                                        <label>Không có hàng nào</label>
                                    </div>
                                @else
                                    <div class="col-xs-12">
                                        <div id="order-content">
                                            <div class="item-row-content text-center">
                                                <div class="col-xs-12 item-col item-col">
                                                    <div class="col-sm-1"></div>
                                                    <div class="col-sm-2 border-item-col-title">
                                                        <label>Tên</label>
                                                    </div>
                                                    <div class="col-sm-2 border-item-col-title"
                                                         style="border-left: none;">
                                                        <label>Tham số (hiển thị)</label>
                                                    </div>
                                                    <div class="col-sm-2 border-item-col-title"
                                                         style="border-left: none;">
                                                        <label>Giá trị thực</label>
                                                    </div>
                                                    <div class="col-sm-1 border-item-col-title"
                                                         style="border-left: none;">
                                                        <label>Đơn vị</label>
                                                    </div>
                                                    {{--<div class="col-sm-2" style="background: #c7c7c7;">
                                                        <label>Toán tử</label>
                                                    </div>--}}
                                                    <div class="col-sm-2 border-item-col-title" style="border-left: none;">
                                                        <label>Hành động</label>
                                                    </div>
                                                </div>
                                                <div class="old-col ">
                                                    @include('jdessetting::editor.order_row.partials.old_cols')
                                                </div>
                                                <div class="item-row-body add-col">
                                                    {{--Js hiển thị các nhãn ở đây--}}
                                                </div>
                                                <input value="{{ isset($cols) ? count($cols) + 1 : 0 }}"
                                                       name="count_col"
                                                       type="hidden">
                                                <div class="add-col">
                                                    <span class="price"><a title="Tổng giá trị" href="#setPriceCol" role="button"
                                                                           class="setPriceCol_btn" style="float: right;"
                                                                           data-row_id="{{ $row_id }}"
                                                                           data-object_id="{{ $order->id }}"
                                                                           data-object_type="row"
                                                                           data-unit="{{ $order->price_text }}"
                                                                           data-toggle="modal"><b>Tổng giá trị: {{ isset($result) ? number_format($order->price, 0, '.', '.') : 0 }}đ</b></a>
                                                        </span>
                                                    <span class="price_text">/<input name="price_text"
                                                                                     placeholder="Đơn vị giá"
                                                                                     value="{{ @$order->price_text }}"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <div class="col-sm-10">
                                    <a href="#insertBox" role="button" title="Thêm cột" class="btn btn-success insertOrderCol-btn" style="float: right; margin-right: 5px" data-order_rows_id="{{ $order->id }}"
                                       data-toggle="modal">Thêm
                                        cột</a>
                                </div>
                            </div>
                            <div class="box-footer">
                                <a type="reset" title="Quay lại" class="btn btn-s-md btn-default"
                                   href="{{ URL::to('/admin/product/editor/' . $order->id) }}">Quay lại</a>
                                <button type="reset" title="Reset dữ liệu" class="btn btn-s-md btn-primary">Reset dữ liệu</button>
                                <button type="button" title="Lưu hàng" class="btn btn-info pull-right" onclick="saveRow();">Lưu hàng
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('jdessetting::editor.order_row.popup.insert_col')
@endsection
@section('custom_header')
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset('public/libs/select2/css/select2.min.css') }}">
    <link type="text/css" rel="stylesheet" charset="UTF-8" href="{{ asset('public/backend/css/order.css') }}">
    <style>
        .add-col,
        #order-content {
            display: inline-block;
        }

        #order-content {
            width: 100%;
        }

        .item-select2 select {
            display: none;
        }

        .delete-col {
            right: 0;
        }

        .add-col {
            margin-top: 10px;
            width: 100%;
            text-align: right;
        }

        span.price,
        span.price_text {
            float: left;
        }

        span.price_text {
            margin-left: 10px;
        }

        .input-label-info {
            display: inline-block;
        }

        .input-label-info > div {
            margin: 0;
        }
    </style>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('public/backend/js/form.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/libs/select2/js/select2.min.js') }}"></script>
    <script>
        function addCol(col_id) {
            $.ajax({
                url: '/admin/add-label?order_no=' + col_id,
                type: 'GET',
                success: function (resp) {
                    $('#order-content .item-row-content .item-row-body').append(resp);
                }
            });
        }

        function increaseInput(input_name) {
            var count_row = parseInt($('input[name=' + input_name + ']').val());
            $('input[name=' + input_name + ']').val(count_row + 1);
        }

        function saveRow() {
            var name = $('input[name=name]').val();
            var price_text = $('input[name=price_text]').val();
            // var calculation_id = $('.calculation_id').val();
            $.ajax({
                url: '/admin/editor/{{ $order->id }}/edit_row/{{ $row_id }}',
                type: 'POST',
                data: {
                    name: name,
                    price_text: price_text
                },
                success: function (resp) {
                    alert('Lưu thành công!');
                    location.reload();
                }
            });
        }

        $(document).ready(function () {
            /*$('.btn-add-col').click(function () {
                var count_row = parseInt($('input[name=count_col]').val());
                addCol(count_row);
                increaseInput('count_col');
            });*/

            /*$('body').on('click', '.delete-col', function () {
                $(this).parents('.item-col').remove();
            });*/

            //  Thay đổi label thì đổi input nhập tham số tương ứng
            /*$('body').on('change', '.select-label', function () {
                var label_id = $(this).val();
                var order_no = $(this).parents('.item-col').data('order_no');
                var object = $(this);
                $.ajax({
                    url: '/admin/get-input-value-label?label_id=' + label_id + '&order_no=' + order_no,
                    type: 'GET',
                    success: function (resp) {
                        object.parents('.item-col').find('.tham-so').html(resp);
                    }
                });
            });*/

            // $('#insertBox').on('click', '.label_info', function () {
            //     var html = $(this).parents('.label-info-div').html();
            //     $('.system-content .input-label-selected').append(html);
            // });
        });
    </script>
@endpush