@if(isset($order) && is_object($order))
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset('public/backend/css/order.css') }}">
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset('public/backend/css/styles-fix.css') }}">
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset('public/backend/css/bao_gia.css') }}">
    <style>
        #order-total-price {
            position: relative;
            bottom: 0;
            color: red;
        }
    </style>
    <?php
    $rows = $order->rows;
    $count_cols_max = @\Modules\JdesSetting\Models\OrderCols::selectRaw('count(*) as total')->where('order_id', $order->id)->groupBy('order_rows_id')->orderBy('total', 'desc')->first()->total;
    $cols_db = $order->cols;
    $cols = [];
    if ($cols_db != null) {
        foreach ($cols_db as $col) {
            $cols[$col->order_rows_id][] = $col;
        }
    }
    $orderRowController = new \Modules\JdesSetting\Http\Controllers\Admin\OrderRowController();
    $filename = base_path() . '/' . config('jdessetting.editor_log_path') . 'editor_id_' . $order->id . $key_editor_log . '.txt';
    $editor_log = (array)json_decode(@file_get_contents($filename));
    ?>
    <div id="product-editor">
        <div class="table-container-edit-order" style="overflow: hidden">
            <div class="row">
                {{--@include('jdessetting::editor.partials.order_view_group')--}}
                @include('jdessetting::editor.partials.order_view_group_frontend')
                <div class="col-log-12">
                    {{--@include('jdessetting::editor.partials.order_view_option')--}}
                    @include('jdessetting::editor.partials.order_view_option_frontend')
                </div>
                {{--END: KHOI OPTION--}}
                <div class="col-xs-12 bg-white rows-content no-pt" id="rows-content">
                    {{--<div class="wrapper1-scroll">
                        <div class="div1-scroll"></div>
                    </div>--}}
                    <div class="table-responsive wrapper2-scroll" id="wrapper2-scroll" style="width: 420px;">
                        <table class="table table-bordered table-striped div2-scroll"
                               id="order-data">
                            <tbody>
                            @foreach($rows as $row)
                                <?php $count_col_in_row = 0;?>
                                @if ($row->publish == 1)
                                    <tr>
                                        <td class="orderRowName_btn edit-orderRowName pop-tool"
                                            role="button" title="Click vào để sửa tên dòng"
                                            data-row_name="{{ $row->name }}"
                                            data-row_id="{{ $row->id }}">
                                            {{ $row->name }}
                                            <div class="pop-show">
                                                <div class="pop-title"></div>
                                            </div>
                                        </td>
                                        @if(isset($cols[$row->id]))
                                            @foreach($cols[$row->id] as $col)
                                                <?php
                                                $count_col_in_row++;
                                                $col_label_parameter = isset($editor_log[$col->id]) ? @$editor_log[$col->id]->label_parameter : $col->label_parameter;
                                                $col_value = isset($editor_log[$col->id]) ? @$editor_log[$col->id]->value : $col->value;
                                                $col_multi_val = isset($editor_log[$col->id]) ? @$editor_log[$col->id]->multi_val : $col->multi_val;
                                                $col_from_id = isset($editor_log[$col->id]) ? @$editor_log[$col->id]->from_id : $col->from_id;
                                                ?>
                                                <td class="pop-tool show-order_set_price"
                                                    data-object_id="{{ $col->id }}"
                                                    data-object_type="col">
                                                    <div class="title-item">{{ @$col->label_name }}</div>
                                                    @if ($col_value != null && $col_multi_val != null)
                                                        <select data-col_id="{{ $col->id }}">
                                                            @foreach(explode('|', $col_multi_val) as $v)
                                                                <option value="{{ $v }}">{{ $v }}{{ @$col->label->unit }}</option>
                                                            @endforeach
                                                        </select>
                                                    @else
                                                        <div class="result-item">  {{ $col_label_parameter }}{{ isset($col->label) ? @$col->label->unit : $col->unit }}</div>
                                                    @endif
                                                    <div class="order_col_action pop-show"
                                                         style="display: none;">
                                                        <div class="pop-title">
                                                            {{ $col_label_parameter }}{{ isset($col->label) ? @$col->label->unit : $col->unit }}
                                                            <p></p>
                                                        </div>
                                                    </div>
                                                </td>
                                            @endforeach
                                        @endif

                                        @for($i = 1; $i <= $count_cols_max - $count_col_in_row; $i ++)  {{--Them cac dong trong--}}
                                        <td></td>
                                        @endfor
                                        {{--<td><a href="#setPriceRow" role="button" class="btn" data-toggle="modal" data-row_id="{{ $row->id }}">{{ number_format($row->price, 0, '.', '.') }}đ@if($row->price_text != '')/{{ $row->price_text }}@endif</a>
                                            </td>--}}
                                        <td class="pop-tool show-order_set_price"
                                            data-object_id="{{ $row->id }}"
                                            data-object_type="row"
                                            style="max-width: 200px;min-width: 150px; text-align: right; height: -webkit-fill-available;  ">
                                            <a role="button" style="border: none!important"
                                               title="Set giá trị">{{ number_format($orderRowController->getPriceObject($row->id, 'row', $key_editor_log), 0, '.', '.') }}
                                                đ@if($row->price_text != '')
                                                    /{{ $row->price_text }}@endif
                                                <div class="pop-show">
                                                    <div class="pop-title">
                                                        <p></p>
                                                    </div>
                                                </div>
                                            </a>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                <span class="price text-danger">
                                <div class="pop-tool" style="padding-top: 10px;">
                                    <b>Tổng giá trị: {{ number_format(@$orderRowController->getPriceObject($order->id, 'order', $key_editor_log), 0, '.', '.') }}đ</b>
                                    <div class="pop-show">
                                        <div class="pop-title"></div>
                                        <a title="Tổng giá trị" href="#setPriceCol" role="button"
                                           class="setPriceCol_btn"
                                           data-object_id="{{ $order->id }}"
                                           data-unit="đ"
                                           data-object_type="order" data-toggle="modal"
                                           data-row_id="">$</a>
                                    </div>
                                </div>
                            </span>
            </div>
        </div>
    </div>
    <script>
        function loading() {
            if ($('body').find('#loading').length == 0) {
                $('body').append('<div id="loading" style="width: 100%;position: fixed;height: 100%;z-index: 999999;top: 0;text-align: center;background-color: rgba(0, 0, 0, 0.3);"><img style="margin-top: 20%;" src="/public/images_core/icons/loading.gif"></div>');
            } else {
                $('#loading').show();
            }
        }

        function stopLoading() {
            $('#loading').hide();
        }

        $(document).ready(function () {
            let getSession = sessionStorage.getItem('checkshow');
            if (getSession == 'true') {
                $('#data_ready').addClass('in');
                $('#data_ready').css('display', 'block');
                $('body').css('padding-right', '10px');
                getSession = false;
            }
            $('#rows-content table select').change(function () {
                loading();
                var col_id = $(this).data('col_id');
                var val = $(this).val();
                if ($('#data_ready').hasClass('in')) {
                    getSession = true;
                }
                sessionStorage.setItem('checkshow', getSession);
                $.ajax({
                    url: '/admin/editor/change-value-col',
                    type: 'GET',
                    data: {
                        order_id: '{{ $order->id }}',
                        col_id: col_id,
                        val: val,
                        key_log: '_remote_addr_{{ $_SERVER['REMOTE_ADDR'] }}'
                    },
                    success: function (resp) {
                        stopLoading();
                        if (resp.reload) {
                            location.reload();
                        }
                    },
                    error: function () {
                        stopLoading();
                        alert('Có lỗi xảy ra. Vui lòng load lại trang và thử lại!');
                    }
                });
            });
        });
    </script>
@endif
