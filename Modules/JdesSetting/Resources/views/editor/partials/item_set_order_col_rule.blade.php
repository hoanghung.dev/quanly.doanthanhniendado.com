<div class="set_order_col_rule">
    <div class="calculation-container">
        <div class="col-xs-10" style="padding-right: 0">
            <?php
            $col_value_arr = explode('|', $col->multi_val);
            $rule_id = $rule_id+1;
            ?>
                @foreach($col_value_arr as $col_value)
                    <div class="form-group" style="cursor: pointer">
                        <input id="col_value_{{$rule_id}}_{{str_slug($col_value)}}"  name="newValues[{{$rule_id}}][]" value="{{ $col_value }}" type="checkbox">
                        <label style="cursor: pointer" for="col_value_{{$rule_id}}_{{str_slug($col_value)}}">  {{ $col_value }}</label>
                    </div>
                @endforeach
            <?php
            $field = ['name' => 'unit_price_id['.$rule_id.']', 'type' => 'select2_model', 'class' => '', 'label' => 'Đơn giá',
                'model' => \Modules\JdesSetting\Models\Editor::class, 'display_field' => 'name', 'where' => "type = 'unit_price' AND group_id is null"];
            ?>
            @if($field['type'] == 'custom')
                                                            @include($field['field'], ['field' => $field])
                                                        @else
                                                            @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                        @endif
        </div>
        <div class="col-xs-2">
            <button class="btn btn-danger delete-set_price" title="Xóa" type="button"><i
                        class="fa fa-trash"></i></button>
        </div>
    </div>
</div>