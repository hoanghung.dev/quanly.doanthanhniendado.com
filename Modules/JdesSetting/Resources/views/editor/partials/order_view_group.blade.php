<div class="navbar navbar-fix">
    <div class="navbar-inner">
        <?php
        $order_group = \Modules\JdesSetting\Models\OrderGroup::find($order->group_id);
        ?>
        @if(is_object($order_group))
            <?php
            if ($order_group->parent_id == null) {
                $data = \Modules\JdesSetting\Models\OrderGroup::where('parent_id', $order_group->id)->orWhere('id', $order_group->id)->orderBy('id', 'asc')->get();
            } else {
                $data = \Modules\JdesSetting\Models\OrderGroup::where('parent_id', $order_group->parent_id)->orWhere('id', $order_group->parent_id)->orderBy('id', 'asc')->get();
            }
            ?>
            @foreach($data as $v)
                <div class="order_group_item pop-tool">
                    <a class="brand {{ $v->id == $order->group_id ? 'active' : '' }}"
                       href="/admin/product/editor/{{ @$v->order_root_of_group->id }}">{{ $v->name }}
                    </a>
                    <div class="order_group_action pop-show" style="display: none;">
                        <div class="pop-title"></div>
                        <div class="option_more">
                            <button href="#orderGroupPopup" role="button" title="Sửa sản phẩm"
                                    class="btn edit-orderGroup" data-toggle="modal"
                                    type="button"
                                    data-group_id="{{ $v->id }}" data-group_name="{{ $v->name }}"><i
                                        class="flaticon-edit"
                                        style="margin-left: 4px;"></i></button>
                            <a title="Xóa" href="/admin/editor/{{ $order->id }}/order_group/{{ $v->id }}/delete"
                               class="icon-delete delete-warning"><i class="fa fa-trash"></i></a>
                        </div>

                    </div>
                </div>
            @endforeach
            @if(@$order->order_root->type == 'order_template')
                @include('jdessetting::editor.popup.add_order_group')
            @else
                @include('jdessetting::editor.popup.add_order_group')
                @include('jdessetting::editor.popup.add_group_product')
            @endif
        @endif
    </div>
</div>