<div class="navbar navbar-fix">
    <div class="navbar-inner">
        <?php
        $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('?editor_id=', $url)[0];
        $order_group = \Modules\JdesSetting\Models\OrderGroup::find($order->group_id);
        ?>
        @if(is_object($order_group))
            <?php
            if ($order_group->parent_id == null) {
                $data = \Modules\JdesSetting\Models\OrderGroup::where('parent_id', $order_group->id)->orWhere('id', $order_group->id)->orderBy('id', 'asc')->get();
            } else {
                $data = \Modules\JdesSetting\Models\OrderGroup::where('parent_id', $order_group->parent_id)->orWhere('id', $order_group->parent_id)->orderBy('id', 'asc')->get();
            }
            ?>
            @foreach($data as $v)
                <div class="order_group_item pop-tool">
                    <a class="brand {{ $v->id == $order->group_id ? 'active' : '' }}"
                       href="{{ $url }}?editor_id={{ $v->order_root_of_group->id }}">{{ $v->name }}
                    </a>
                </div>
            @endforeach
        @endif
    </div>
</div>