<div class="navbar navbar-fix">
    <div class="navbar-inner bg-fix">
        <?php
        $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('?editor_id=', $url)[0];
        ?>
        @if($order->parent_id == null)
            <div class="pop-tool">
                <a class="brand active"
                   href="{{ $url }}?editor_id={{ $order->id }}">{{ $order->plan }} @if($order->plan_active == 1) <img
                            src="{{ asset('public/images_core/icons/published.png') }}"> @endif</a>
            </div>

            <?php $data = $order->plan_childs;?>
        @else
            <div class="pop-tool">
                <a class="brand"
                   href="{{ $url }}?editor_id={{ $order->plan_parent->id }}">{{ $order->plan_parent->plan }} @if($order->plan_parent->plan_active == 1)
                        <img src="{{ asset('public/images_core/icons/published.png') }}"> @endif</a>
            </div>
            <?php $data = \Modules\JdesSetting\Models\Editor::select(['id', 'plan', 'plan_active'])->where('parent_id', $order->parent_id)->orderBy('id', 'asc')->get();?>
        @endif
        @foreach($data as $child)
            <div class="pop-tool">
                <a class="brand {{ $child->id == $order->id ? 'active' : '' }}"
                   href="{{ $url }}?editor_id={{ $child->id }}">{{ $child->plan }} @if($child->plan_active == 1) <img
                            src="{{ asset('public/images_core/icons/published.png') }}"> @endif</a>
            </div>
        @endforeach
    </div>
</div>
