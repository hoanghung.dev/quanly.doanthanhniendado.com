<script>
    $('.plan_active').click(function () {
        var id = $(this).data('id');
        $.ajax({
            url: '/admin/editor/active-plan',
            type: 'POST',
            data: {
                id: id
            },
            success: function (resp) {
                location.reload();
            },
            error: function () {
                alert('Có lỗi xảy ra. Vui lòng load lại website và thử lại!');
            }
        });
    });
</script>