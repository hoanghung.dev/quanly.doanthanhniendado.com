<div class="order_group_item">
    <a href="#productGroupPopup" title="Thêm sản phẩm" id="add-orderGroup" role="button" class="btn brand no-margin" data-toggle="modal">Thêm sản phẩm</a>
</div>
<div id="productGroupPopup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="productGroupPopupLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="productGroupPopupLabel">Sản phẩm</h3>
    </div>
    <div class="modal-body">
        <div id="form_productGroupPopup" action="/admin/editor/{{ $order->id }}/add-group-product" method="POST">
            {!! csrf_field() !!}
            <?php $field = ['name' => 'group_product_id', 'type' => 'select_model_tree', 'class' => 'nhom-sp', 'label' => 'Nhóm SP',
                'model' => \Modules\JdesLabel\Models\GroupLabel::class, 'where' => 'type=2', 'display_field' => 'name'];?>
            @if($field['type'] == 'custom')
                                                            @include($field['field'], ['field' => $field])
                                                        @else
                                                            @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                        @endif

            <?php $field = ['name' => 'product_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Sản phẩm',
                'model' => \Modules\JdesSetting\Models\Editor::class, 'where' => "type = 'order_template' AND parent_id is null AND is_root = 1", 'display_field' => 'name'];?>
            @if($field['type'] == 'custom')
                                                            @include($field['field'], ['field' => $field])
                                                        @else
                                                            @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                        @endif
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Quay lại</button>
        <button class="btn btn-primary" id="submit_productGroupPopup" type="button">Lưu lại</button>
    </div>
</div>
<style>
    .order_group_item_add {
        display: none;
    }
</style>
<script>
    $('select[name=group_product_id]').change(function () {
        var group_product_id = $(this).val();
        $.ajax({
            url: '/admin/editor/get-order-template-option',
            type: 'GET',
            data: {
                group_product_id : group_product_id,
            },
            success: function (resp) {
                $('select[name=product_id]').html(resp);
            },
            error: function () {
                alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
            }
        });
    });

    $('#submit_productGroupPopup').click(function () {
        $.ajax({
            url: $('#form_productGroupPopup').attr('action'),
            type: 'POST',
            data: {
                product_id : $('#form_productGroupPopup select[name=product_id]').val(),
            },
            success: function (resp) {
                window.location.href = resp.href;
            },
            error: function () {
                alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
            }
        });
    });

    $('#add-orderGroup').click(function () {
        $('div#form_productGroupPopup input').val('');
        $('div#productGroupPopup #productGroupPopupLabel').html('Thêm sản phẩm');
    });

    $('.edit-orderGroup').click(function () {
        $('div#form_productGroupPopup input[name=group_id]').val($(this).data('group_id'));
        $('div#form_productGroupPopup input[name=group_name]').val($(this).data('group_name'));
        $('div#productGroupPopup #productGroupPopupLabel').html('Sửa sản phẩm');
    });
</script>
