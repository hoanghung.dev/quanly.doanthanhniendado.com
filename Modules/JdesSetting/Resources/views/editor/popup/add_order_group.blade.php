<div class="order_group_item order_group_item_add">
    <a href="#orderGroupPopup" title="Thêm sản phẩm" id="add-orderGroup" role="button" class="btn brand no-margin"
       data-toggle="modal">Thêm sản phẩm</a>
</div>
<div id="orderGroupPopup" class="modal fade" role="dialog">
    <div class="modal-dialog" style="max-width: unset;width: 80%;">
        <div class="modal-content" style="width:100%;height:100%">
            <div class="modal-header">
                <h4 class="modal-title" id="orderGroupPopupLabel">Sản phẩm</h4>
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="modal-body">
                <div id="form_orderGroupPopup" action="/admin/editor/{{ $order->id }}/add-group" method="POST">
                    {!! csrf_field() !!}
                    <input type="hidden" name="group_id" value="">
                    <div class="form-group pl-4">
                        <label class="col-xs-12 text-left">Tên sản phẩm</label>
                        <div class="col-xs-12">
                            <input type="text" name="group_name" class="form-control" value=""
                                   placeholder="Tên sản phẩm">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Quay lại</button>
                <button class="btn btn-primary" id="submit_orderGroupPopup" type="button">Lưu lại</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('#submit_orderGroupPopup').click(function () {
        $.ajax({
            url: $('#form_orderGroupPopup').attr('action'),
            type: 'POST',
            data: {
                group_id: $('#form_orderGroupPopup input[name=group_id]').val(),
                group_name: $('#form_orderGroupPopup input[name=group_name]').val(),
            },
            success: function (resp) {
                window.location.href = resp.href;
            },
            error: function () {
                alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
            }
        });
    });

    $('#add-orderGroup').click(function () {
        $('div#form_orderGroupPopup input').val('');
        $('div#orderGroupPopup #orderGroupPopupLabel').html('Thêm sản phẩm');
    });

    $('.edit-orderGroup').click(function () {
        $('div#form_orderGroupPopup input[name=group_id]').val($(this).data('group_id'));
        $('div#form_orderGroupPopup input[name=group_name]').val($(this).data('group_name'));
        $('div#orderGroupPopup #orderGroupPopupLabel').html('Sửa sản phẩm');
    });
</script>
