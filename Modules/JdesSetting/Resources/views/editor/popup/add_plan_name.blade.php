<div id="planGroupPopup" class="modal fade" role="dialog">
    <div class="modal-dialog" style="max-width: unset;width: 80%;">
        <div class="modal-content" style="width:100%;height:100%">
            <div class="modal-header">
                <h4 class="modal-title" id="orderGroupPopupLabel">Sửa option</h4>
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>

            <div class="modal-body">
                <div id="form_planGroupPopup" action="/admin/editor/edit-plan" method="GET">
                    {!! csrf_field() !!}
                    <input type="hidden" name="group_id" value="">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên option</label>
                        <div class="col-sm-9">
                            <input type="text" name="group_name" class="form-control" value=""
                                   placeholder="Tên option">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Quay lại</button>
                <button class="btn btn-primary" id="submit_planGroupPopup" type="button">Lưu lại</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('#submit_planGroupPopup').click(function () {
        let urls = location.href;
        $.ajax({
            url: $('#form_planGroupPopup').attr('action') + '?id='
                + $('div#form_planGroupPopup input[name=group_id]').val()
                + '&name=' + $('div#form_planGroupPopup input[name=group_name]').val()
            ,
            type: 'GET',
            data: {
                id: $('#form_planGroupPopup input[name=group_id]').val(),
                name: $('#form_planGroupPopup input[name=group_name]').val(),
            },
            success: function (resp) {
                window.location.href = urls;
            },
            error: function () {
                alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
            }
        });
    });

    $('#add-orderGroup').click(function () {
        $('div#form_orderGroupPopup input').val('');
    });

    $('.edit-orderGroup').click(function () {
        $('div#form_planGroupPopup input[name=group_id]').val($(this).data('group_id'));
        $('div#form_planGroupPopup input[name=group_name]').val($(this).data('group_name'));
    });
</script>
