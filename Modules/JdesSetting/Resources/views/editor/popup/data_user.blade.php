@if(!empty($users))
    @foreach($users as $user)
        <div style="" class="box_user">
            @if($user->email != null || $user->email != '')
                <div class="parent-email">
                    <div class="l-left col-sm-12">
                        <div class="row">
                            <span class="ol col-sm-5 text-right">Email liên hệ - {{$user->name}} </span>
                            <span class="ol l-right col-sm-7 text-left">{{$user->email}}</span>
                        </div>
                    </div>
                </div>
            @endif
            @if($user->phone != null || $user->phone != '')
                <div class="parent-phone">
                    <div class="l-left col-sm-12">
                        <div class="row">
                            <span class="ol col-sm-5 text-right">Điện thoại - {{$user->name}} </span>
                            <span class="ol l-right col-sm-7 text-left">{{$user->phone}}</span>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    @endforeach
@endif