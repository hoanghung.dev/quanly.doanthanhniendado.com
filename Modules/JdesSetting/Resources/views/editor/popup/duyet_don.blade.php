<!-- Modal -->
<div class="modal fade" id="data_ready" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content text-center">
            <div class="modal-header">
                <h4 class="modal-title text-center" id="exampleModalLabel">Đơn hàng</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="new_line">

            </div>
            <div class="modal-body">
                <div class="box box-info order-box">
                    <form id="" method="post" action=""
                          class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body box-grey no-padding">
                            {{--KHOI OPTION--}}
                            <div class="table-container-edit-order">
                                <div class="row">
                                    {{--END: KHOI OPTION--}}
                                    <div class="col-xs-12 bg-white rows-content no-pt" id="rows-content">
                                        {{--<div class="wrapper1-scroll">
                                            <div class="div1-scroll"></div>
                                        </div>--}}
                                        <div class="table-responsive wrapper2-scroll" id="wrapper2-scroll">
                                            <table class="table table-bordered table-striped " id="order-data">
                                                <tbody>
                                                @foreach($rows as $row)
                                                    <?php $count_col_in_row = 0;?>
                                                    @if($row->publish == 1)
                                                        <tr>
                                                            <td class="orderRowName_btn edit-orderRowName pop-tool"
                                                                role="button" title="Click vào để sửa tên dòng"
                                                                data-row_name="{{ $row->name }}"
                                                                data-row_id="{{ $row->id }}">
                                                                {{ $row->name }}
                                                                <div class="pop-show">
                                                                    <div class="pop-title"></div>
                                                                    <div class="option_more">
                                                                        <a href="#orderRowName" data-toggle="modal"
                                                                           class="icon-edit" title="Chỉnh sửa hàng"><i
                                                                                    class="flaticon-edit"></i></a>
                                                                        <a href="{{ url('admin/editor/'.$order->id.'/delete_row/'.$row->id) }}"
                                                                           class="icon-delete delete-warning"
                                                                           title="Xóa dòng"><i
                                                                                    class="fa fa-trash"
                                                                                    style="margin-left: 4px;"></i></a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            @if(isset($cols[$row->id]))
                                                                @foreach($cols[$row->id] as $col)
                                                                    <?php $count_col_in_row++;?>
                                                                    <td class="pop-tool show-order_set_price"
                                                                        data-object_id="{{ $col->id }}"
                                                                        data-object_type="col">
                                                                        <div class="title-item">{{ @$col->label_name }}</div>
                                                                        @if ($col->value != null && $col->multi_val != null)
                                                                            <select data-col_id="{{ $col->id }}">
                                                                                @foreach(explode('|', $col->multi_val) as $v)
                                                                                    <option value="{{ $v }}">{{ $v }}{{ @$col->label->unit }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        @else
                                                                            <div class="result-item">  {{ $col->label_parameter }}{{ isset($col->label) ? @$col->label->unit : $col->unit }}</div>
                                                                        @endif
                                                                        <div class="order_col_action pop-show"
                                                                             style="display: none;">
                                                                            <div class="pop-title">
                                                                                {{ $col->label_parameter }}{{ isset($col->label) ? @$col->label->unit : $col->unit }}
                                                                                <p></p>
                                                                            </div>
                                                                            <div class="option_more">
                                                                                <a href="#setPriceCol"
                                                                                   title="Set giá trị"
                                                                                   role="button"
                                                                                   class="setPriceCol_btn"
                                                                                   data-object_id="{{ $col->id }}"
                                                                                   data-unit="{{ $col->unit != null ? $col->unit : @$col->label->unit }}"
                                                                                   data-object_type="col"
                                                                                   data-toggle="modal"
                                                                                   data-row_id="{{ $row->id }}">$</a>
                                                                                <a href="#editOrderCol"
                                                                                   title="Chỉnh sửa cột"
                                                                                   role="button"
                                                                                   class="editOrderCol_btn"
                                                                                   data-object_id="{{ $col->id }}"
                                                                                   data-object_type="col"
                                                                                   data-col_id="{{ $col->id }}"
                                                                                   data-label_name="{{ $col->label_name }}"
                                                                                   data-label_parameter="{{ $col->label_parameter }}"
                                                                                   data-value="{{ $col->value }}"
                                                                                   data-unit="{{ $col->unit }}"
                                                                                   data-toggle="modal"><i
                                                                                            class="flaticon-edit"></i></a>
                                                                                @if($col->value != null && $col->multi_val != null)
                                                                                    <a href="#setOrderColRule"
                                                                                       title="Set quy tắc"
                                                                                       role="button"
                                                                                       class="setOrderColRule_btn"
                                                                                       data-object_id="{{ $col->id }}"
                                                                                       data-object_type="col"
                                                                                       data-toggle="modal"
                                                                                       data-row_id="{{ $row->id }}"><i
                                                                                                class="fa fa-calculator"
                                                                                                aria-hidden="true"></i></a>
                                                                                @endif
                                                                                <a class="btn btn-danger delete-col icon-delete delete-warning"
                                                                                   title="Xóa giá trị"
                                                                                   href="/admin/editor/{{ $order->id }}/delete_col/{{ $col->id }}"><i
                                                                                            class="fa fa-trash"></i></a>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                @endforeach
                                                            @endif

                                                            @for($i = 1; $i <= $count_cols_max - $count_col_in_row; $i ++)  {{--Them cac dong trong--}}
                                                            <td></td>
                                                            @endfor
                                                        </tr>
                                                    @endif
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="">Tên báo giá</label>
                        <p><input name="name_clone" value="{{$order->name}}" class="form-control"></p>
                    </div>
                    <div class="col-md-4">
                        <?php $field = ['name' => 'user_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Khách hàng',
                            'model' => \App\Models\User::class, 'display_field' => 'name'];?>
                        @if($field['type'] == 'custom')
                                                            @include($field['field'], ['field' => $field])
                                                        @else
                                                            @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                        @endif
                        {{--<label for="">Xưởng in</label>
                        <p>@if(!empty(App\Models\Factory::find($order->factory_id))) {{App\Models\Factory::find($order->factory_id)->name}} @endif</p>--}}
                    </div>
                    <div class="col-md-4">
                        <?php $field = ['name' => 'factory_clone_id', 'type' => 'select2_model', 'class' => 'select-label item-select2 xuong_in', 'model' => \App\Models\Admin::class, 'where' => 'type="factory"',
                            'display_field' => 'name', 'label' => 'xưởng in', 'value' => $order->factory_id];?>
                        @if($field['type'] == 'custom')
                                                            @include($field['field'], ['field' => $field])
                                                        @else
                                                            @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                        @endif
                        {{--<label for="">Xưởng in</label>
                        <p>@if(!empty(App\Models\Factory::find($order->factory_id))) {{App\Models\Factory::find($order->factory_id)->name}} @endif</p>--}}
                    </div>
                    <div class="col-md-4">
                        @php
                            $date1 = new DateTime($order->time_date_to);
                            $date2 = new DateTime($order->time_date_from);
                            $interval = $date1->diff($date2);
                        @endphp
                        <label for="">Thời gian</label>
                        <p>sau đây {{$interval->days}} ngày</p>
                    </div>
                    <div class="col-md-4">
                        <label for="">Giá</label>
                        <p>{{number_format($order->price, 0, '.', '.')}} đ</p>
                    </div>
                </div>
            </div>
            <div class="nth-3"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close_modal" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary clone_order">Duyệt</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.clone_order').on('click', function () {
            $.ajax({
                type: 'POST',
                url: '/admin/editor/save-data-ajax-duplicate-order',
                data: {
                    order: '{{$order->id}}',
                    name: $('#data_ready input[name=name_clone]').val(),
                    user_id: $('#data_ready select[name=user_id]').val(),
                    factory_id: $('#data_ready select[name=factory_clone_id]').val(),
                },
                success: function (res) {
                    if (res.success == true) {
                        window.location.href = res.url;
                        $('.modal-dialog').scrollTop(10);
                    } else {
                        $('.new_line').html(`<h5 class="text-danger m-t-2">Chưa lưu được mời bạn thử lại sau !</h5>`);
                    }
                }
            })
        });

        $('#data_ready .close').click(function () {
            $('.new_line').children().remove();
            $('#data_ready').removeClass('in');
            $('#data_ready').css('display', 'none');
            $('body').css('padding-right', 'unset');
            sessionStorage.setItem('checkshow', false);
        });
        $('#data_ready .close_modal').click(function () {
            $('.new_line').children().remove();
            $('#data_ready').removeClass('in');
            $('#data_ready').css('display', 'none');
            $('body').css('padding-right', 'unset');
            sessionStorage.setItem('checkshow', false);
        });
        $('.data_ready').on('click', function () {
            var getId_use = '{{str_replace('|', ',', $order->apply_user_ids)}}';
            userData(getId_use);
            var getAdmin = '{{$order->apply_admin_ids}}';
            var factory = '{{$order->factory_id}}';
            $.ajax({
                type: "POST",
                url: "/admin/editor/get-data-ajax-bill",
                data: {
                    'getAdmin': getAdmin,
                    'factory': factory,
                },
                success: function (res) {
                    $('.nth-3').html(res)
                }
            })
        })
    })
    $('.select_id').change(function () {
        var getId_use = $(this).val();
        userData(getId_use);

    })

    function userData(getId_use) {
        $.ajax({
            type: 'GET',
            url: '/admin/editor/get-id-user?id=' + getId_use,
            success: function (res) {
                $('#data_email').html(res);
            }
        })
    }
</script>

{{-- end modal--}}

<style>
    #data_ready .box_user {
        display: grid;
        padding-bottom: 8px;
        border-top: solid 1px #f4f4f3;
    }

    #data_ready .select2-selection--multiple {
        border: 1px solid gray;
    }

    #data_ready > .modal-dialog {
        max-height: 90vh;
        width: 90%;
        overflow-y: scroll;
    }

    #data_ready .close {
        margin-top: -25px !important;
    }

    #form-group-user_id {
        margin: 0;
    }

    #data_ready .modal-body:last-child {
        border-bottom: unset;
    }

    #data_ready .l-left {
        font-weight: bold;
    }

    #data_ready .l-left .l-right {
        font-weight: normal;
    }

    #data_ready .box-body .rows-content {
        padding: unset !important;
        background: unset !important;
    }

    #data_ready .bg-white {
        background: #fff;
        margin: unset !important;
        padding: unset !important;
    }
</style>