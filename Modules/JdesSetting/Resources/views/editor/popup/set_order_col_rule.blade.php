@php
    $last_id = Modules\JdesSetting\Models\OrderColRule::all()->last();
if (!empty($last_id)){
    $last_id = $last_id->id;
}
else{
    $last_id = 0;
}
@endphp
<div id="setOrderColRule" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="width:100%;height:100%">

            <input id="count_set_order_col_rule_id" value="{{$last_id}}" type="hidden">
            <form method="POST" action="/admin/editor/post-order-col-rule">
                {!! csrf_field() !!}
                <input type="hidden" name="order_id" value="{{ $order->id }}">
                <input type="hidden" name="row_id" value="">
                <input type="hidden" name="object_id" value="">
                <input type="hidden" name="object_type" value="">
                <div class="modal-header">
                    <h4 class="modal-title" id="setOrderColRuleLabel">Set quy tắc</h4>
                    <button type="button" class="close" data-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <div id="set_order_col_rule_content">
                        <div class="set_order_col_rule_old">
                        </div>
                    </div>
                    <button class="btn btn-success add-calcu" title="Thêm Khối" id="add-item-set_price" type="button"><i
                                class="fa fa-plus"></i></button>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" type="button"
                            aria-hidden="true">Quay lại
                    </button>
                    <button class="btn btn-primary" type="submit" id="submit-set_price">Áp dụng
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $('.setOrderColRule_btn').click(function () {
        $('#count_set_order_col_rule_id').val({{$last_id}})
        $('.set_order_col_rule_old').html('');
        $('.set_order_col_rule').html('');
        loading();
        $('#setOrderColRule .set_price_item_new').remove(); //  Xoa cac khoi set_price cu
        $('#setOrderColRule input[name=object_id]').val($(this).data('object_id'));
        $('#setOrderColRule input[name=object_type]').val($(this).data('object_type'));
        $('#setOrderColRule input[name=row_id]').val($(this).data('row_id'));
        $('#setOrderColRule input[name=unit]').val($(this).data('unit'));
        $.ajax({
            url: '/admin/editor/item-old-set-order-col-rule',
            type: 'GET',
            data: {
                order_id: '{{ $order->id }}',
                object_id: $(this).data('object_id'),
                object_type: $(this).data('object_type')
            },
            success: function (resp) {
                $('#setOrderColRule .set_order_col_rule_old').html(resp);
                stopLoading();
            },
            error: function () {
                stopLoading();
            }
        });
    });

    function addItemSetOrderColRule(onThis) {
        onThis.attr('disabled', true);
        $.ajax({
            url: '/admin/editor/add-item-set-order-col-rule',
            type: 'GET',
            data: {
                order_id: '{{ $order->id }}',
                col_id: $('#setOrderColRule input[name=object_id]').val(),
                order_no: $('#count_set_order_col_rule').val(),
                rule_id: $('#count_set_order_col_rule_id').val()
            },
            success: function (resp) {
                onThis.removeAttr('disabled');
                $('#set_order_col_rule_content').append(resp);
                $('#count_set_order_col_rule').val(parseInt($('#count_set_order_col_rule').val()) + 1);
                $('#count_set_order_col_rule_id').val(parseInt($('#count_set_order_col_rule_id').val()) + 1);
            }
        });
    }

    /*$(document).ready(function () {
        addItemSetOrderColRule();
    });*/

    $('body').on('click', '#setOrderColRule #add-item-set_price', function () {
        addItemSetOrderColRule($(this));
    });

    $('body').on('click', '#setOrderColRule .delete-set_price', function () {
        let dataId = $(this).attr('data-value');
        let check = $(this);
        $.ajax({
            type: 'POST',
            url: '/admin/editor/delete-order-col-rule?dataId=' + dataId,
            success: function (data) {
                if (data.success == true) {
                    check.parents('#setOrderColRule .set_order_col_rule').remove();
                } else {
                    alert('Có lỗi xẩy ra mời bạn thử lại !')
                }
            }
        })
    });

    /*$('body').on('change', '.set_price_item', function () {
        var type = $(this).find(":selected").data('type');
        $(this).parents('.set_price_item').find('.set_price_type').val(type);
    });*/
</script>