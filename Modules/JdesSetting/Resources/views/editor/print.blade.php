@extends('admin.template_blank')
@section('main')
    <?php $order_root = $order->order_root;?>
    <div class="content-wrapper">
        <section class="content">
            <div class="table-responsive">
                <div>
                    <div class="col-xs-12">
                        <label>id</label>
                        <span>{{ $order_root->id }}</span>
                    </div>
                    <div class="col-xs-12">
                        <label>Tên khách</label>
                        <span>
                            <?php $arr_ids = explode('|', $order_root->apply_user_ids);
                            $data = \App\Models\User::whereIn('id', $arr_ids)->get();
                            ?>
                            @foreach($data as $v)
                                {{ $v->name }} - {{ $v->tel }}<br>
                            @endforeach
                        </span>
                    </div>
                    <div class="col-xs-12">
                        <label>Sản phẩm</label>
                        <span>{{ $order_root->name }}</span>
                    </div>
                    <?php $group_childs = \Modules\JdesSetting\Models\OrderGroup::where('parent_id', $order_root->group_id)->orWhere('id', $order_root->group_id)->orderBy('id', 'asc')->get();?>
                    @foreach($group_childs as $group)
                        <div class="col-xs-12 group">
                            <div class="col-sm-3">
                                <label>{{ $group->name }}</label>
                            </div>
                            <div class="col-sm-9">
                                <?php $orders = \Modules\JdesSetting\Models\Editor::where('group_id', $group->id)->orderBy('id', 'asc')->get();?>
                                @foreach($orders as $order)
                                    <div class="col-xs-12 order">
                                        <label class="col-sm-3">{{ $order->plan }}</label>
                                        <div class="col-sm-9">
                                            <?php
                                            $row_publish_id = \Modules\JdesSetting\Models\OrderRows::where('order_id', $order->id)->where('publish', 1)->pluck('id');
                                            $cols = \Modules\JdesSetting\Models\OrderCols::select(['label_name', 'multi_val', 'label_id', 'label_parameter', 'unit'])->whereIn('order_rows_id', $row_publish_id)->where('order_id', $order->id)->orderBy('id', 'asc')->get();
                                            ?>
                                            @foreach($cols as $col)
                                                <div class="col-xs-12">
                                                    <label>{{ $col->label_name }}</label>
                                                    <span>
                                                    @if ($col->value != null && $col->multi_val != null)
                                                        {{ array_pop(array_reverse(explode('|', $col->multi_val))) }} {{ @$col->label->unit }}
                                                    @else
                                                        {{ $col->label_parameter }}{{ isset($col->label) ? @$col->label->unit : $col->unit }}
                                                    @endif
                                                </span>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-xs-12">
                <div class="col-sm-4">
                    <input type="checkbox"> Khách đã duyệt
                </div>
                <div class="col-sm-4">
                    <input type="checkbox"> Admin đã duyệt
                </div>
                <div class="col-sm-4">
                    <button type="button" class="btn btn-primary">Download</button>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')

@endpush

@section('custom_header')
    <style>
        .group {
            border: 1px solid #ccc;
        }
        .order {
            border: 1px solid #ccc;
        }
    </style>
@endsection
@section('custom_footer')

@endsection