@extends(config('core.admin_theme').'.template')
@section('main')
    <form class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="product-editor"
          action="" method="POST"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <input name="return_direct" value="save_exit" type="hidden">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                     id="kt_page_   portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg" style="">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Editor tính giá sản phẩm
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="/admin/product/{{ $order->product_id }}"
                               class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Quay lại</span>
                            </a>
                            {{--<div class="btn-group">
                                @if(in_array($module['code'].'_add', $permissions))
                                    <button type="submit" class="btn btn-brand">
                                        <i class="la la-check"></i>
                                        <span class="kt-hidden-mobile">Lưu</span>
                                    </button>
                                    <button type="button"
                                            class="btn btn-brand dropdown-toggle dropdown-toggle-split"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_continue">
                                                    <i class="kt-nav__link-icon flaticon2-reload"></i>
                                                    Lưu và tiếp tục
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_exit">
                                                    <i class="kt-nav__link-icon flaticon2-power"></i>
                                                    Lưu & Thoát
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_create">
                                                    <i class="kt-nav__link-icon flaticon2-add-1"></i>
                                                    Lưu và tạo mới
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                            </div>--}}
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-12">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <!--begin::Form-->
                    <div class="kt-form">

                        <?php
                        $rows = $order->rows;
                        $count_cols_max = @\Modules\JdesSetting\Models\OrderCols::selectRaw('count(*) as total')->where('order_id', $order->id)->groupBy('order_rows_id')->orderBy('total', 'desc')->first()->total;
                        $cols_db = $order->cols;
                        $cols = [];
                        if ($cols_db != null) {
                            foreach ($cols_db as $col) {
                                $cols[$col->order_rows_id][] = $col;
                            }
                        }
                        $orderRowController = new \Modules\JdesSetting\Http\Controllers\Admin\OrderRowController();
                        ?>
                        {{--KHOI OPTION--}}
                        <div class="table-container-edit-order">

                            <div class="row">
                                @include('jdessetting::editor.partials.order_view_group')
                                <div class="col-log-12">
                                    @include('jdessetting::editor.partials.order_view_option')
                                </div>
                                {{--END: KHOI OPTION--}}
                                <div class="col-xs-12 bg-white rows-content no-pt" id="rows-content">
                                    {{--<div class="wrapper1-scroll">
                                        <div class="div1-scroll"></div>
                                    </div>--}}
                                    <div class="table-responsive wrapper2-scroll" id="wrapper2-scroll">
                                        <table class="table table-bordered table-striped div2-scroll"
                                               id="order-data">
                                            <tbody>
                                            @foreach($rows as $row)
                                                <?php $count_col_in_row = 0;?>
                                                @if ($row->publish == 0 && !CommonHelper::has_permission(Auth::guard('admin')->user()->id, 'super_admin'))
                                                    {{--Nếu ko có quyền xem dòng này và dòng này ko publish thì ko được xem--}}
                                                @else
                                                    <tr>
                                                        <td class="orderRowName_btn edit-orderRowName pop-tool"
                                                            role="button" title="Click vào để sửa tên dòng"
                                                            data-row_name="{{ $row->name }}"
                                                            data-row_id="{{ $row->id }}">
                                                            {{ $row->name }}
                                                            <div class="pop-show">
                                                                <div class="pop-title"></div>
                                                                <div class="option_more">
                                                                    <a href="#orderRowName"
                                                                       data-toggle="modal"
                                                                       class="icon-edit"
                                                                       title="Chỉnh sửa hàng"><i
                                                                                class="flaticon-edit"></i></a>
                                                                    <a href="{{ url('admin/editor/'.$order->id.'/delete_row/'.$row->id) }}"
                                                                       class="icon-delete delete-warning"
                                                                       title="Xóa dòng"><i
                                                                                class="fa fa-trash"
                                                                                style="margin-left: 4px;"></i></a>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        @if(isset($cols[$row->id]))
                                                            @foreach($cols[$row->id] as $col)
                                                                <?php
                                                                $count_col_in_row++;
                                                                $col_label_parameter = isset($editor_log[$col->id]) ? @$editor_log[$col->id]->label_parameter : $col->label_parameter;
                                                                $col_value = isset($editor_log[$col->id]) ? @$editor_log[$col->id]->value : $col->value;
                                                                $col_multi_val = isset($editor_log[$col->id]) ? @$editor_log[$col->id]->multi_val : $col->multi_val;
                                                                $col_from_id = isset($editor_log[$col->id]) ? @$editor_log[$col->id]->from_id : $col->from_id;
                                                                ?>
                                                                <td class="pop-tool show-order_set_price"
                                                                    data-object_id="{{ $col->id }}"
                                                                    data-object_type="col">
                                                                    <div class="title-item">{{ @$col->label_name }}</div>
                                                                    @if ($col_value != null && $col_multi_val != null)
                                                                        <select data-col_id="{{ $col->id }}">
                                                                            @foreach(explode('|', $col_multi_val) as $v)
                                                                                <option value="{{ $v }}">{{ $v }}{{ @$col->label->unit }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    @else
                                                                        <div class="result-item">  {{ $col_label_parameter }}{{ isset($col->label) ? @$col->label->unit : $col->unit }}</div>
                                                                    @endif
                                                                    <div class="order_col_action pop-show"
                                                                         style="display: none;">
                                                                        <div class="pop-title">
                                                                            {{ $col_label_parameter }}{{ isset($col->label) ? @$col->label->unit : $col->unit }}
                                                                            <p></p>
                                                                        </div>
                                                                        <div class="option_more">
                                                                            <a href="#setPriceCol"
                                                                               title="Set giá trị"
                                                                               role="button"
                                                                               class="setPriceCol_btn"
                                                                               data-object_id="{{ $col->id }}"
                                                                               data-unit="{{ $col->unit != null ? $col->unit : @$col->label->unit }}"
                                                                               data-object_type="col"
                                                                               data-toggle="modal"
                                                                               data-row_id="{{ $row->id }}">$</a>
                                                                            <a href="#editOrderCol"
                                                                               title="Chỉnh sửa cột"
                                                                               role="button"
                                                                               class="editOrderCol_btn"
                                                                               data-object_id="{{ $col->id }}"
                                                                               data-object_type="col"
                                                                               data-col_id="{{ $col->id }}"
                                                                               data-label_name="{{ $col->label_name }}"
                                                                               data-label_parameter="{{ $col_label_parameter }}"
                                                                               data-value="{{ $col_value }}"
                                                                               data-unit="{{ $col->unit }}"
                                                                               data-toggle="modal"><i
                                                                                        class="flaticon-edit"></i></a>
                                                                            @if($col_value != null && $col_multi_val != null)
                                                                                <a href="#setOrderColRule"
                                                                                   title="Set quy tắc"
                                                                                   role="button"
                                                                                   class="setOrderColRule_btn"
                                                                                   data-object_id="{{ $col->id }}"
                                                                                   data-object_type="col"
                                                                                   data-toggle="modal"
                                                                                   data-row_id="{{ $row->id }}"><i
                                                                                            class="fa fa-calculator"
                                                                                            aria-hidden="true"></i></a>
                                                                            @endif
                                                                            <a class="btn btn-danger delete-col icon-delete delete-warning"
                                                                               title="Xóa giá trị"
                                                                               href="/admin/editor/{{ $order->id }}/delete_col/{{ $col->id }}"><i
                                                                                        class="fa fa-trash"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            @endforeach
                                                        @endif

                                                        @for($i = 1; $i <= $count_cols_max - $count_col_in_row; $i ++)  {{--Them cac dong trong--}}
                                                        <td></td>
                                                        @endfor
                                                        {{--<td><a href="#setPriceRow" role="button" class="btn" data-toggle="modal" data-row_id="{{ $row->id }}">{{ number_format($row->price, 0, '.', '.') }}đ@if($row->price_text != '')/{{ $row->price_text }}@endif</a>
                                                            </td>--}}
                                                        <td class="pop-tool show-order_set_price"
                                                            data-object_id="{{ $row->id }}"
                                                            data-object_type="row"
                                                            style="max-width: 200px;min-width: 150px; text-align: right;">
                                                            <a role="button"
                                                               title="Set giá trị">{{ number_format($orderRowController->getPriceObject($row->id, 'row', '_admin_id_' .\Auth::guard('admin')->id()), 0, '.', '.') }}
                                                                đ@if($row->price_text != '')
                                                                    /{{ $row->price_text }}@endif
                                                                <div class="pop-show">
                                                                    <div class="pop-title">
                                                                        <p></p>
                                                                    </div>
                                                                    <div class="option_more">
                                                                        <a data-object_id="{{ $row->id }}"
                                                                           data-object_type="row"
                                                                           data-unit="{{ $row->price_text }}"
                                                                           data-row_id="{{ $row->id }}"
                                                                           href="#setPriceCol"
                                                                           data-toggle="modal"
                                                                           class="icon-edit setPriceCol_btn"
                                                                           title="Chỉnh sửa hàng">$</a>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </td>
                                                        <td style="vertical-align: middle;    width: 110px;">
                                                            @if(CommonHelper::has_permission(Auth::guard('admin')->user()->id, 'super_admin'))
                                                                <input data-value="{{$row->id}}"
                                                                       data-title="Hiển thị dòng đối với nhân viên"
                                                                       style="cursor:pointer; width: 100px;"
                                                                       class="form-control publish_row"
                                                                       type="checkbox"
                                                                       @if($row->publish == 1) checked
                                                                       @endif value="{{$row->id}}">
                                                            @else
                                                                <div style="width: 70px;"></div>
                                                                <span>&nbsp;</span>
                                                            @endif
                                                            <a href="#insertBox" title="Chèn thêm nhãn"
                                                               class="insertOrderCol-btn" role="button"
                                                               data-order_rows_id="{{ $row->id }}"
                                                               data-toggle="modal"><i
                                                                        class="fa fa-plus"></i>
                                                            </a>

                                                            {{--<a href="{{ url('admin/editor/'.$order->id.'/edit_row/' . $row->id) }}"
                                                               class="icon-edit" title="Chỉnh sửa hàng"><i
                                                                        class="flaticon-edit"></i></a>--}}
                                                            <a href="{{ url('admin/editor/'.$order->id.'/delete_row/'.$row->id) }}"
                                                               class="icon-delete delete-warning"
                                                               title="Xóa hàng"><i
                                                                        class="fa fa-trash"
                                                                        style="margin-left: 4px;"></i></a>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach

                                            </tbody>
                                        </table>
                                        <div class="footer-table-action">
                                                <span class="price" id="order-total-price">
                                                    <div class="pop-tool">
                                                        <b>Tổng giá trị: {{ number_format(@$orderRowController->getPriceObject($order->id, 'order', '_admin_id_' .\Auth::guard('admin')->id()), 0, '.', '.') }}đ</b>
                                                        <div class="pop-show">
                                                            <div class="pop-title"></div>
                                                            <a title="Tổng giá trị" href="#setPriceCol" role="button"
                                                               class="setPriceCol_btn"
                                                               data-object_id="{{ $order->id }}"
                                                               data-unit="đ"
                                                               data-object_type="order" data-toggle="modal"
                                                               data-row_id="">$</a>
                                                        </div>
                                                    </div>
                                                </span>
                                            <input style="color: #fff;" name="add_order_row"
                                                   title="Thêm hàng"
                                                   id="add_order_row"
                                                   value="Thêm hàng"
                                                   class="btn btn-success add_order_row" type="submit">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </form>

    @include('jdessetting::editor.order_row.popup.edit_order_col')
    @include('jdessetting::editor.order_row.popup.set_price_col')
    @include('jdessetting::editor.popup.set_order_col_rule')
    @include('jdessetting::editor.order_row.popup.insert_col')
    @include('jdessetting::editor.popup.order_row_name')
    @include('jdessetting::editor.partials.script_rows_content')
    @include('jdessetting::editor.partials.script_order')
@endsection
@section('custom_head')
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset(config('core.admin_asset').'/css/form.css') }}">
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset('public/backend/css/order.css') }}">
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset('public/backend/css/styles-fix.css') }}">
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset('public/backend/css/bao_gia.css') }}">
@endsection
@section('custom_footer')
    <script type="text/javascript" src="{{ asset(config('core.admin_asset').'/js/form.js') }}"></script>
@endsection
@push('scripts')
    <script>
        $('.publish_row').click(function () {
            var id = $(this).val();
            $.ajax({
                type: 'POST',
                url: '/admin/editor/publish-row',
                data: {
                    order_row_id: id
                },
                success: function (resp) {
                    toastr.success("Cập nhật thành công!");
                },
                error: function (resp) {
                    alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
                }
            });
        });


        //  Inert Col
        function resetDataResult() {
            $('.input-label-selected').html('');
            /*$('.input-label-info').html('');
            $('.input-label-value').html('');*/
            $('#preview-label-value').html('');
            $('#preview-label-unit').html('');
            $('#insertBox input[name=system]').click();
        }

        function setDataFormInsert(label_id, label_name, label_parameter, value) {
            if (label_id) {
                $('input[name=label_name]').val(label_id);
            }
            if (label_name) {
                $('input[name=label_id]').val(label_name);
            }
            if (label_parameter) {
                $('input[name=label_parameter]').val(label_parameter);
            }
            if (value) {
                $('input[name=value]').val(value);
            }
        }

        $('.insertOrderCol-btn').click(function () {
            $('#insertBox input[name=order_rows_id]').val($(this).data('order_rows_id'));
        });

        $('#btn-apply').click(function () {
            var label_id = $('#insertBox input[name=label_id]').val();
            var label_name = $('#insertBox input[name=label_name]').val();
            var value = $('#insertBox input[name=value]').val();
            var unit = $('#insertBox input[name=unit]').val();
            var label_parameter = $('#insertBox input[name=label_parameter]').val();
            var order_rows_id = $('#insertBox input[name=order_rows_id]').val();
            var cols = [];
            $('.input-label-selected input').each(function () {
                var col = {
                    label_id: $(this).data('label_id'),
                    value: $(this).data('value'),
                    order_id: $(this).data('order_id'),
                    order_rows_id: $(this).data('order_rows_id'),
                    label_name: $(this).data('label_name'),
                    label_parameter: $(this).data('label_parameter'),
                    multi_value: $(this).data('multi_value'),
                    from_type: $(this).data('from_type'),
                    from_id: $(this).data('from_id'),
                };
                cols.push(col);
            });
            $.ajax({
                url: '/admin/editor/insert-label-to-order',
                type: 'POST',
                data: {
                    cols: cols,
                    order_rows_id: order_rows_id,
                    order_id: '{{ $order->id }}',
                    label_name: label_name
                },
                success: function (resp) {
                    location.reload();
                    $('.input-label-result').html();
                    $('#order-content .old-col ').append(resp);
                    resetDataResult();
                }
            });
        });

        $('body').on('click', '#insertBox .khung-chon-gia-tri .label_info', function () {
            var idRand = $(this).data('checkbox_id');
            $('.input-label-selected #' + idRand).remove();
            if ($(this).prop("checked") == true) {
                var html = $(this).parents('.label-info-div').html();
                $('.input-label-selected').append(html);
                $('#insertBox input[name=label_name]').val($(this).data('label_name'));
            }
        });

        $('body').on('click', '#insertBox .unit_price-area .input-label-value .label_info', function () {
            var idRand = $(this).data('checkbox_id');
            $('#unit_price-tab-name-data #' + idRand).remove();
            if ($(this).prop("checked") == true) {
                var html = $(this).parents('.label-info-div').html();
                $('#unit_price-tab-name-data').append(html);
            }
        });

        $('#insertBox').on('click', '.label_info_value', function () {
            $('#preview-label-value').html($(this).val());
            var label_parameter = $(this).data('label_parameter');
            setDataFormInsert(false, false, label_parameter, $(this).val());
        });

        $('#insertBox input[name=from]').change(function () {
            resetDataResult();
            var val = $(this).val();

            if (val == 'system') {
                console.log('system');
                $('.block-area').hide();
                $('.system-area').show();
                /*$('.system-content').show();
                $('.unit_price-content').hide();
                $('.input-label-value').hide();
                $('.input-label-info').show();*/
            } else if (val == 'unit_price') {
                console.log('unit_price');
                $('.block-area').hide();
                $('.unit_price-area').show();
                /*$('.system-content').hide();
                $('.unit_price-content').show();
                $('.input-label-value').show();
                $('.input-label-info').hide();*/
            } else if (val == 'order') {
                console.log('order-area');
                $('.block-area').hide();
                $('.order-area').show();
                var order_id = '{{ $order->id }}';
                $.ajax({
                    url: '/admin/editor/input-order-info',
                    type: 'GET',
                    data: {
                        order_id: order_id,
                    },
                    success: function (resp) {
                        resetDataResult();
                        $('.input-label-info.order-area').html(resp);
                    }
                });
            } else {
                console.log('unit_price_object');
                $('.block-area').hide();
                $('.unit_price_object-area').show();
            }
        });

        $('#insertBox .chon_xuong').click(function () {
            loading();
            var unit_price_id = $('select[name=unit_price_id]').val();
            var tab_code = 'tab' + $('#unit_price-tab-name-data').html().length + unit_price_id;
            $.ajax({
                url: '/admin/editor/input-unit-price-info',
                type: 'GET',
                data: {
                    unit_price_id: unit_price_id,
                    tab_code: tab_code,
                    order_id: '{{ $order->id }}'
                },
                success: function (resp) {
                    stopLoading();
                    resetDataResult();
                    $('.input-label-content #chooseUnitPriceTab li').removeClass('active');
                    $('#chooseUnitPriceTabContent .tab-pane').removeClass('in active');
                    $('.input-label-content #chooseUnitPriceTab').append('<li class="active"><a href="#' + tab_code + '" data-id="#' + tab_code + '" data-toggle="tab">' + $('#unit_price-tab-name-data').html() + '</a></li>');
                    $('.unit_price-area .input-label-info').append(resp);
                },
                error: function () {
                    stopLoading();
                    alert('Có lỗi xảy ra! Vui lòng load lại trang và thử lại.')
                }
            });
        });

        $('#insertBox ul#chooseUnitPriceTab li').click(function () {
            var id = $(this).find('a').data('id');
            $('div#chooseUnitPriceTabContent ' + id).addClass('in active');
            $('div#chooseUnitPriceTabContent ' + id).siblings().removeClass('in active');
        });

        $('#insertBox select[name=unit_price_object_id]').change(function () {
            loading();
            var unit_price_id = $(this).val();
            $.ajax({
                url: '/admin/editor/input-unit-price-info',
                type: 'GET',
                data: {
                    unit_price_id: unit_price_id,
                    order_id: '{{ $order->id }}'
                },
                success: function (resp) {
                    stopLoading();
                    resetDataResult();
                    $('.unit_price_object-area .input-label-info').html(resp);
                },
                error: function () {
                    stopLoading();
                    alert('Có lỗi xảy ra! Vui lòng load lại trang và thử lại.')
                }
            });
        });

        // ajax nhom nhan select
        $('.select2-group_label_id').on('change', function () {
            var getData = $(this).val();
            $.ajax({
                type: 'GET',
                url: '/admin/editor/change-goroup-label?data=' + getData,
                success: function (res) {
                    $('#label_id').html(res);
                }
            })
        });
        $('.select2-group_price_id').on('change', function () {
            var getData = $(this).val();
            $.ajax({
                type: 'GET',
                url: '/admin/editor/change-goroup-label?data_price=' + getData,
                success: function (res) {
                    $('#unit_price_object_id').html(res);
                }
            })
        });
        $('body').on('change', '#insertBox select[name=label_id]', function () {
            var label_id = $(this).val();
            $.ajax({
                url: '/admin/editor/input-label-info',
                type: 'GET',
                data: {
                    label_id: label_id,
                    order_id: '{{ $order->id }}'
                },
                success: function (resp) {
                    resetDataResult();
                    $('#insertBox .input-label-value').html(resp);
                    $('#insertBox .system-area.input-label-info').html(resp);
                }
            });
        });
        //  END: insert col




        $('.save_editor').click(function () {
            $('input[name=return_direct]').val($(this).data('action'));
            $('form.{{ @$module['code'] }}').submit();
        });


    </script>
@endpush
