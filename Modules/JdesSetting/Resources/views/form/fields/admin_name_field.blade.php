<input type="text" class="form-control {{ @$field['class'] }}"
       id="{{ $field['name'] }}" {!! @$field['inner'] !!}
       value="{{ @$result->admin->name }}">