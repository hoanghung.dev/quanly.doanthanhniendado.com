<?php
$view_type = isset($_GET['view_type']) ? $_GET['view_type'] : '';
if ($view_type == ''){
    $view_type = App\Models\UnitPriceColumn::where('role_id', $role_id)->get();
    if (count($view_type) > 0){
        $view_type = $view_type->first()->id;
    }
    else{
        $view_type = '';
    }
}
// Lay loai hien thi
$role_id = @\App\Models\RoleAdmin::where('admin_id', \Auth::guard('admin')->user()->id)->first()->role_id;
if (!empty($view_type)){
    $unit_price_columnsId = App\Models\UnitPriceColumn::find($view_type);
    $ths = \Modules\JdesLabel\Models\Label::whereIn('id', explode('|', $unit_price_columnsId->label_ids))->pluck('id');    //  Lay danh sach cac label_id hien thi trang view
    $columns = \App\Models\OrderCols::select(['value', 'label_id', 'label_name', 'id'])->whereIn('label_id', $ths)
        ->where('order_id', $item->id)->get();
    foreach ($columns as $column) {
        $trs[$column->label_id] = $column;
    }
}
?>
@if(!empty($ths))
    @foreach($ths as $th)
        @if(isset($trs[$th]))
            <?php $label = \Modules\JdesLabel\Models\Label::find($trs[$th]->label_id);?>
            <td class="item-{{$field['name']}}">
                {{ $trs[$th]->value }} {{ $label->unit }}
            </td>
        @else
            <td></td>
        @endif
    @endforeach
@endif

