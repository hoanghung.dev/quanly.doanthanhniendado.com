<?php $bill_factory = \Modules\JdesSetting\Models\BillFactory::where('bill_id', $bill->id)->first();?>
<div class="kt-portlet bill-factory">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Xưởng in {{ @$settings['name'] }}
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <div class="kt-form">
        <div class="kt-portlet__body">
            <div class="kt-section kt-section--first">
                @if(in_array($bill->status, [0]) && !is_object($bill_factory))
                    <span title="Gửi đơn này tới xưởng in báo giá" id="send_to_factory" data-bill_id="{{ $bill->id }}"
                          style="cursor: pointer;"
                          class="icon-header btn-add-task btn btn-facebook btn-elevate btn-pill text-light"><i
                                class="la la-plus-circle"></i>Gửi đơn này tới xưởng in báo giá</span>
                @else
                    <div class="form-group-div form-group " id="form-group-total_price">
                        <label for="total_price">Deadline </label>
                        <div class="col-xs-12">
                            <input type="text" class="form-control value-number_price"
                                   value="{{date('d-m-Y',strtotime($bill_factory->date))}}" name="date">
                        </div>
                    </div>
                    {{--<div class="form-group-div form-group " id="form-group-total_price">
                        <label for="total_price">Trạng thái </label>
                        <div class="col-xs-12">
                            <?php
                            if ($bill_factory->status == 1) {
                                $bill->status = 'Hoàn thành';
                            } else {
                                $bill->status = 'Đang làm';
                            }
                            ?>
                            <input type="text" class="form-control value-number_price" value="{{$bill->status}}"
                                   name="status">
                        </div>
                    </div>--}}
                    @if(in_array('super_admin', $permissions))
                        <button id="save_bill_factory_info" class="btn btn-primary"
                                style="float: right; margin-right: 10px;">Lưu
                        </button>
                    @endif
                @endif
            </div>
        </div>
    </div>
    <!--end::Form-->
</div>


<script>
    $('#send_to_factory').click(function () {
        var bill_id = $(this).data('bill_id');
        loading();
        $.ajax({
            url: '/admin/factory/send-bill',
            type: 'POST',
            data: {
                bill_id: bill_id
            },
            success: function (data) {
                stopLoading();
                if (data.status) {
                    window.location.reload();
                    toastr.success(data.msg);
                } else {
                    toastr.error(data.msg);
                }
            },
            error: function () {
                toastr.success("Có lỗi xảy ra! Vui lòng load lại website và thử lại");
                stopLoading();
            }
        });
    });

    $('#save_bill_factory_info').click(function () {
        var bill_id = $(this).data('bill_id');
        loading();
        $.ajax({
            url: '/admin/factory/save-bill-info',
            type: 'POST',
            data: {
                bill_id: bill_id,

            },
            success: function (data) {
                stopLoading();
                if (data.status) {
                    window.location.reload();
                    toastr.success(data.msg);
                } else {
                    toastr.error(data.msg);
                }
            },
            error: function () {
                toastr.success("Có lỗi xảy ra! Vui lòng load lại website và thử lại");
                stopLoading();
            }
        });
    });

    @if(!in_array('super_admin', $permissions))
    $('.bill-factory input, .bill-factory select').attr('disabled', 'disabled');
    @endif
</script>