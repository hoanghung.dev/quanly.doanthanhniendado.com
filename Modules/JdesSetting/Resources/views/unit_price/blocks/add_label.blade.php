<?php
if (isset($col) && is_object($col)) {
    $label = \Modules\JdesLabel\Models\Label::find($col->label_id);
    $label_name = $col->label_name;
    $column = $col->column;
    $order_no = $col->order_no;
    $label_type = $col->type;
}
if (isset($label_id)) {
    $label = \Modules\JdesLabel\Models\Label::find($label_id);
}
?>
@if(isset($type) && $type == 'unit_price')
    <div class="col-xs-12 item-col item-col{{ $order_no }} {{ @$label->code }}_data"
         data-order_no="{{ $order_no }}"
         data-col-id="{{ @$col->id }}" data-label-code="{{ @$label->code }}">
        <div class="col-sm-2" style="padding: 0;">
            <label class="lb-name">{{ @$label_name }}</label>
            <input type="hidden" name="label_name{{ $column . '[' . $order_no . ']' }}" value="{{ @$label_name }}">
            <input type="hidden" name="label_id{{ $column . '[' . $order_no . ']' }}" value="{{ @$label->id }}">
            <input type="hidden" name="label_type{{ $column . '[' . $order_no . ']' }}" value="{{ @$label_type }}">
        </div>
        @if(@$label_type != 0) {{--Kieu nhan danh rieng cho bao gia don - 'Nhãn', 'Nhãn phù hợp', 'Giá trị phù hợp', 'Loại trừ nhãn', 'Loại trừ giá trị' 'don gia'--}}
            @if(in_array(@$label_type, [1, 3]))     {{--Lay cac label--}}
                <div class="col-sm-7 tham-so no-padding">
                    <?php $field = ['name' => 'value' . $column . '[' . $order_no . ']', 'type' => 'select2_model', 'class' => '', 'label' => 'Chọn kiểu', 'multiple' => true,
                        'model' => \Modules\JdesLabel\Models\Label::class, 'display_field' => 'name', 'result' => @$col];?>
                    @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                </div>
            @elseif(in_array(@$label_type, [2, 4]))     {{--Lay cac cac tham so--}}
                <div class="col-sm-7 tham-so no-padding">
                    <?php $field = ['name' => 'value' . $column . '[' . $order_no . ']', 'type' => 'text', 'class' => '', 'label' => 'Tham số',
                        'value' => isset($col) ? $col->value : 0];?>
                    @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                </div>
            @else {{--Lay tu don gia co san--}}
                <div class="col-sm-7 tham-so no-padding">
                    <?php $field = ['name' => 'value' . $column . '[' . $order_no . ']', 'type' => 'select2_model', 'class' => '', 'label' => 'Chọn đơn giá', 'multiple' => true,
                        'model' => \Modules\JdesSetting\Models\Editor::class, 'display_field' => 'name', 'result' => @$col, 'where' => "status in ('pending', 'active') AND type = 'unit_price'"];?>
                    @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                </div>
            @endif
        @elseif (@$label->code == 'xuong_in') {{--Neu la nhan xuong in--}}
            <div class="col-sm-7 no-padding">
                <?php $field = ['name' => 'value' . $column . '[' . $order_no . ']', 'type' => 'select2_model', 'class' => 'required select-label item-select2 xuong_in', 'label' => 'xưởng in',
                    'value' => isset($col) ? @$col->factory->id : 0, 'model' => \App\Models\Admin::class, 'where' => 'type = "factory"', 'display_field' => 'name'];?>
                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
            </div>
        @elseif (@$label->code == 'dinh_luong') {{--Neu la nhan xuong in--}}
            <div class="col-sm-7 tham-so no-padding">
                <?php $field = ['name' => 'value' . $column . '[' . $order_no . ']', 'type' => 'number', 'class' => '', 'label' => 'Định lượng',
                    'value' => isset($col) ? $col->value : 0, 'inner' => 'step=0.01'];?>
                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
            </div>
        @else
            @if(@$label->input_type == 'input_number')
                <div class="col-sm-7 tham-so no-padding">
                    <?php $field = ['name' => 'value' . $column . '[' . $order_no . ']', 'type' => 'number', 'class' => '', 'label' => 'Tham số',
                        'value' => isset($col) ? $col->value : 0, 'inner' => 'step=0.01'];?>
                    @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                </div>
            @elseif(@$label->input_type == 'input_text')
                <div class="col-sm-7 tham-so no-padding">
                    <?php $field = ['name' => 'value' . $column . '[' . $order_no . ']', 'type' => 'text', 'class' => '', 'label' => 'Tham số',
                        'value' => isset($col) ? $col->value : ''];?>
                    @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                </div>
            @else
                <div class="col-sm-7 tham-so no-padding">
                    <?php $data_select = \Modules\JdesSetting\Models\Attribute::where('table', 'label')->where('item_id', @$label->id)->where('type', 'select')->pluck('key', 'value')->toArray();?>
                    <?php $field = ['name' => 'value' . $column . '[' . $order_no . ']', 'type' => 'select', 'class' => '', 'label' => 'Tham số',
                        'value' => isset($col) ? $col->value : '', 'options' => $data_select];?>
                    @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                </div>
            @endif
        @endif
        <div class="col-sm-2">
            <span class="don_vi">
                {{ @$label->unit }}
            </span>
        </div>

        <button title="Xóa" class="btn btn-danger delete-col" type="button"><i class="fa fa-trash" style="margin-left: 6px!important;"></i></button>
    </div>

    @if (@$label->code == 'dinh_luong' && !isset($col)) {{--Neu la nhan xuong in--}}
        @include("admin.unit_price.blocks.add_label_dinh_luong", ['column' => $column])
    @endif
@else
    <?php if (!isset($order_no)) $order_no = @$_GET['order_no']; ?>
    <div class="col-xs-12 item-col item-col{{ $order_no }}" data-order_no="{{ $order_no }}">
        <div class="col-sm-2">
            <?php $field = ['name' => 'col_label_name' . $order_no, 'type' => 'text', 'class' => 'required', 'label' => 'Tên nhãn',
                'value' => isset($col) ? @$label_name : ''];?>
            @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
        </div>
        <div class="col-sm-2">
            <?php $field = ['name' => 'col' . $order_no, 'type' => 'select2_model', 'class' => 'required select-label item-select2', 'label' => 'Kiểu nhãn',
                'value' => isset($col) ? $col->label->id : 0, 'model' => \Modules\JdesLabel\Models\Label::class, 'display_field' => 'name'];?>
            @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
        </div>
        @if(!isset($col) || $col->label->input_type == 'input_number')
            <div class="col-sm-2 tham-so">
                <?php $field = ['name' => 'col_value' . $order_no, 'type' => 'number', 'class' => '', 'label' => 'Tham số',
                    'value' => isset($col) ? $col->value : 0, 'inner' => 'step=0.01'];?>
                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
            </div>
        @elseif($col->label->input_type == 'input_text')
            <div class="col-sm-2 tham-so">
                <?php $field = ['name' => 'col_value' . $order_no, 'type' => 'text', 'class' => '', 'label' => 'Tham số',
                    'value' => isset($col) ? $col->value : ''];?>
                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
            </div>
        @else
            <div class="col-sm-2 tham-so">
                <?php $data_select = \Modules\JdesSetting\Models\Attribute::where('table', 'label')->where('item_id', $col->label->id)->where('type', 'select')->pluck('key', 'value')->toArray();?>
                <?php $field = ['name' => 'col_value' . $order_no, 'type' => 'select', 'class' => '', 'label' => 'Tham số',
                    'value' => isset($col) ? $col->value : '', 'options' => $data_select];?>
                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
            </div>
        @endif
        <div class="col-sm-1">
            <?php $field = ['name' => 'col_calculation_id' . $order_no, 'type' => 'select2_model', 'class' => 'require', 'label' => 'Toán tử',
                'value' => isset($col) ? $col->calculation_id : 5, 'model' => \Modules\JdesSetting\Models\Calculation::class, 'display_field' => 'unit'];?>
            <div class="form-group" id="form-group-{{ $field['name'] }}">
                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
            </div>
        </div>
        <button class="btn btn-danger delete-col" type="button"><i class="fa fa-trash"  style="margin-left: 6px!important;"></i></button>
    </div>
@endif