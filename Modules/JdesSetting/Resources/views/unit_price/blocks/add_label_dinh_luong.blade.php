<?php $order_no++; $data = @\Modules\JdesLabel\Models\Label::where('code', 'dinh_luong_kho')->first();?>
<div class="col-xs-12 item-col xuong_in_data item-col{{ $order_no }}" data-order_no="{{ $order_no + 2 }}"
     data-col-id="">
    <div class="col-sm-2" style="padding: 0;">
        <label>Khổ</label>
        <input type="hidden" name="label_name{{ $column }}[{{ $order_no }}]" value="{{ $data->name }}">
        <input type="hidden" name="label_id{{ $column }}[{{ $order_no }}]"
               value="{{ $data->id }}">
        <input type="hidden" name="label_type{{ $column }}[{{ $order_no }}]" value="0">
    </div>
    <div class="col-sm-8 tham-so no-padding">
        <div class="form-group" id="form-group-value{{ $column }}[{{ $order_no }}]">
            <input type="text" name="value{{ $column }}[{{ $order_no }}]" class="form-control "
                   id="value{{ $column }}[{{ $order_no }}]"
                   value=""
                   placeholder="Tham số">
        </div>
    </div>
    <div class="col-sm-2">
<span class="don_vi">{{ $data->unit }}
        </span>
    </div>
    <button title="Xoá" class="btn btn-danger delete-col" type="button"><i class="fa fa-trash"
        ></i></button>
</div>

<?php $order_no++; $data = @\Modules\JdesLabel\Models\Label::where('code', 'dinh_luong_dai')->first();?>
<div class="col-xs-12 item-col xuong_in_data item-col{{ $order_no }}" data-order_no="{{ $order_no + 2 }}"
     data-col-id="">
    <div class="col-sm-2" style="padding: 0;">
        <label>Dài</label>
        <input type="hidden" name="label_name{{ $column }}[{{ $order_no }}]" value="{{ $data->name }}">
        <input type="hidden" name="label_id{{ $column }}[{{ $order_no }}]"
               value="{{ $data->id }}">
        <input type="hidden" name="label_type{{ $column }}[{{ $order_no }}]" value="0">
    </div>
    <div class="col-sm-8 tham-so no-padding">
        <div class="form-group" id="form-group-value{{ $column }}[{{ $order_no }}]">
            <input type="text" name="value{{ $column }}[{{ $order_no }}]" class="form-control "
                   id="value{{ $column }}[{{ $order_no }}]"
                   value=""
                   placeholder="Tham số">
        </div>
    </div>
    <div class="col-sm-2">
<span class="don_vi">{{ $data->unit }}
        </span>
    </div>
    <button title="Xóa " class="btn btn-danger delete-col" type="button"><i class="fa fa-trash"
        ></i></button>
</div>

<?php $order_no++; $data = @\Modules\JdesLabel\Models\Label::where('code', 'dinh_luong_rong')->first();?>
<div class="col-xs-12 item-col xuong_in_data item-col{{ $order_no }}" data-order_no="{{ $order_no + 2 }}"
     data-col-id="">
    <div class="col-sm-2" style="padding: 0;">
        <label>Rộng</label>
        <input type="hidden" name="label_name{{ $column }}[{{ $order_no }}]" value="{{ $data->name }}">
        <input type="hidden" name="label_id{{ $column }}[{{ $order_no }}]"
               value="{{ $data->id }}">
        <input type="hidden" name="label_type{{ $column }}[{{ $order_no }}]" value="0">
    </div>
    <div class="col-sm-8 tham-so no-padding">
        <div class="form-group" id="form-group-value{{ $column }}[{{ $order_no }}]">
            <input type="text" name="value{{ $column }}[{{ $order_no }}]" class="form-control "
                   id="value{{ $column }}[{{ $order_no }}]"
                   value=""
                   placeholder="Tham số">
        </div>
    </div>
    <div class="col-sm-2">
<span class="don_vi">{{ $data->unit }}
        </span>
    </div>
    <button title="Xóa" class="btn btn-danger delete-col" type="button"><i class="fa fa-trash"
        ></i></button>
</div>

<?php $order_no++; $data = @\Modules\JdesLabel\Models\Label::where('code', 'dinh_luong_gia')->first();?>
<div class="col-xs-12 item-col xuong_in_data item-col{{ $order_no }}" data-order_no="{{ $order_no + 2 }}"
     data-col-id="">
    <div class="col-sm-2" style="padding: 0;">
        <label>Giá</label>
        <input type="hidden" name="label_name{{ $column }}[{{ $order_no }}]" value="{{ $data->name }}">
        <input type="hidden" name="label_id{{ $column }}[{{ $order_no }}]"
               value="{{ $data->id }}">
        <input type="hidden" name="label_type{{ $column }}[{{ $order_no }}]" value="0">
    </div>
    <div class="col-sm-8 tham-so no-padding">
        <div class="form-group" id="form-group-value{{ $column }}[{{ $order_no }}]">
            <input type="text" name="value{{ $column }}[{{ $order_no }}]" class="form-control "
                   id="value{{ $column }}[{{ $order_no }}]"
                   value=""
                   placeholder="Tham số">
        </div>
    </div>
    <div class="col-sm-2">
<span class="don_vi">{{ $data->unit }}
        </span>
    </div>
    <button title="Xóa" class="btn btn-danger delete-col" type="button"><i class="fa fa-trash"
        ></i></button>
</div>