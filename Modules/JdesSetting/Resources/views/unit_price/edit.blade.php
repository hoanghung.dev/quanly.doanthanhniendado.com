@extends(config('core.admin_theme').'.template')
@section('main')
    <form class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid {{ @$module['code'] }}"
          action="" method="POST"
          enctype="multipart/form-data"
          id="product-editor">
        {{ csrf_field() }}
        <input name="return_direct" value="save_continue" type="hidden">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                     id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg" style="">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Chỉnh sửa {{ $module['label'] }}
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="/admin/{{ $module['code'] }}" class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Quay lại</span>
                            </a>
                            <div class="btn-group">
                                @if(in_array($module['code'].'_edit', $permissions))
                                    <button type="submit" class="btn btn-brand">
                                        <i class="la la-check"></i>
                                        <span class="kt-hidden-mobile">Lưu</span>
                                    </button>
                                    <button type="button"
                                            class="btn btn-brand dropdown-toggle dropdown-toggle-split"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_continue">
                                                    <i class="kt-nav__link-icon flaticon2-reload"></i>
                                                    Lưu và tiếp tục
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_exit">
                                                    <i class="kt-nav__link-icon flaticon2-power"></i>
                                                    Lưu & Thoát
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_create">
                                                    <i class="kt-nav__link-icon flaticon2-add-1"></i>
                                                    Lưu và tạo mới
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>

        <div class="row">
            {{ csrf_field() }}
            <input type="hidden" name="order_id" value="{{ @$order_id }}">
            <div class="box-body">
                <div class="col-xs-12">
                    <div class="row title-box-up">
                        <div class="col-xs-4 col-md-4 text-center">
                            <span class="no-margin">Sửa đơn giá gộp: admin, mode</span>
                        </div>
                        <div class="col-xs-4 col-md-4 text-center">
                            <span style="display: block; margin-bottom: 5px;"><strong>Đơn giá gộp: </strong>là những đơn giá tổng hợp từ nhiều đơn giá lẻ, nhãn và các quy tắc khác</span>
                        </div>
                        <div class="col-xs-4 col-md-4 text-center">
                            <span style="display: block; margin-bottom: 5px;">thông tin của giấy in phù hợp với máy in</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-color select-height no-padding">
                    <?php
                    $fields = [
                        ['name' => 'group_label_id', 'type' => 'select_model_tree', 'class' => 'required', 'label' => 'Nhóm nhãn',
                            'model' => \Modules\JdesLabel\Models\GroupLabel::class, 'display_field' => 'name', 'where' => 'type=0'],
                        ['name' => 'tag_ids', 'type' => 'select2_model', 'class' => '', 'label' => 'Lọc theo tag',
                            'model' => \Modules\JdesLabel\Models\GroupLabel::class, 'display_field' => 'name', 'where' => 'type=1', 'multiple' => true],
                    ];
                    foreach ($fields as $k => $field) {
                        $field['value'] = @$result->{$field['name']};
                        $tab['td'][$k] = $field;
                        $fields[$k] = $field;
                    }
                    ?>
                    <div class="row">
                        @foreach($fields as $field)
                            <div class="col-sm-4 col-md-4 col-xs-12 inner-input-pd">
                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                            </div>
                        @endforeach
                    </div>
                </div>
                <br>

                <div class="col-xs-12">
                    <div class="row box-color select-height">
                        <div class="col-md-4 col-sm-12 group-1 group-label no-padding mobile-all" data-col="1">
                            <p class="box-title">Thông tin cơ bản</p>
                            <div>
                                <div class="item-row-content row-cus">
                                    <div class="old-col">
                                        <?php $cols = \Modules\JdesSetting\Models\OrderCols::where('order_id', $order_id)->where('column', 1)->orderBy('order_no', 'asc')->get();
                                        ?>
                                        @foreach($cols as $k => $col)
                                            @include('jdessetting::unit_price.blocks.add_label', ['col' => $col, 'type' => 'unit_price'])
                                        @endforeach
                                    </div>
                                    <div class="item-row-body old-col">
                                        {{--Js hiển thị các nhãn ở đây--}}
                                    </div>
                                    <input value="{{ isset($cols) ? count($cols) + 1 : 0 }}"
                                           name="count_col1"
                                           type="hidden">
                                    <input value="{{ isset($cols) ? count($cols) + 1 : 0 }}"
                                           name="count_max_col1"
                                           type="hidden">
                                    <div class="add-col" style="display: flex; flex-wrap: wrap;">
                                        <div class="gom-col">
                                            <div class="col-md-4" style="">
                                                <?php $field = ['name' => 'add_label_name1', 'type' => 'text', 'class' => 'add_label_name', 'label' => 'Nhãn'];?>
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                            </div>
                                            <div class="col-md-7 edit-form">
                                                <?php $field = ['name' => 'add_label_id1', 'type' => 'select2_model', 'class' => 'add_label_id select-label item-select2', 'label' => 'Kiểu',
                                                    'model' => \Modules\JdesLabel\Models\Label::class, 'display_field' => 'name'];?>
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                            </div>
                                            <select style="display: none" name="add_type1" class="add_type">
                                                <option selected value="0">Nhãn</option>
                                            </select>
                                            <div class="col-btn">
                                                <button title="Thêm giá trị" class="btn btn-success btn-add-edit" data-col="1" type="button">
                                                    Thêm
                                                </button>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 group-2 group-label mobile-all no-padding" data-col="2">
                            <p class="box-title">Thuộc tính cơ bản</p>
                            <div>
                                <div class="item-row-content row-cus">
                                    <div class="old-col">
                                        <?php $cols = \Modules\JdesSetting\Models\OrderCols::where('order_id', $order_id)->where('column', 2)->orderBy('order_no', 'asc')->get();
                                        ?>
                                        @foreach($cols as $k => $col)
                                            @include('jdessetting::unit_price.blocks.add_label', ['col' => $col, 'type' => 'unit_price'])
                                        @endforeach
                                    </div>
                                    <div class="item-row-body row-cus">
                                        {{--Js hiển thị các nhãn ở đây--}}
                                    </div>
                                    <input value="{{ isset($cols) ? count($cols) + 1 : 0 }}"
                                           name="count_col2"
                                           type="hidden">
                                    <input value="{{ isset($cols) ? count($cols) + 1 : 0 }}"
                                           name="count_max_col2"
                                           type="hidden">
                                    <div class="add-col">
                                        <div class="gom-col">
                                            <div class="col-md-4">
                                                <?php $field = ['name' => 'add_label_name2', 'type' => 'text', 'class' => 'add_label_name', 'label' => 'Nhãn'];?>
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                            </div>
                                            <div class="col-md-7">
                                                <?php $field = ['name' => 'add_label_id2', 'type' => 'select2_model', 'class' => 'add_label_id select-label item-select2', 'label' => 'Kiểu',
                                                    'model' => \Modules\JdesLabel\Models\Label::class, 'display_field' => 'name'];?>
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                            </div>
                                            <select style="display: none" name="add_type2" class="add_type">
                                                <option selected value="0">Nhãn</option>
                                            </select>
                                            <div class="col-btn">
                                                <button title="Thêm giá trị" class="btn btn-success btn-add-edit" data-col="2" type="button">
                                                    Thêm
                                                </button>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 group-3 group-label mobile-all no-padding" data-col="3">
                            <p class="box-title">Các giá trị tính toán</p>
                            <div>
                                <div class="item-row-content row-cus">
                                    <div class="old-col">
                                        <?php $cols = \Modules\JdesSetting\Models\OrderCols::where('order_id', $order_id)->where('column', 3)->orderBy('order_no', 'asc')->get();
                                        ?>
                                        @foreach($cols as $k => $col)
                                            @include('jdessetting::unit_price.blocks.add_label', ['col' => $col, 'type' => 'unit_price'])
                                        @endforeach
                                    </div>
                                    <div class="item-row-body row-cus">
                                        {{--Js hiển thị các nhãn ở đây--}}
                                    </div>
                                    <input value="{{ isset($cols) ? count($cols) + 1 : 0 }}"
                                           name="count_col3"
                                           type="hidden">
                                    <input value="{{ isset($cols) ? count($cols) + 1 : 0 }}"
                                           name="count_max_col3"
                                           type="hidden">
                                    <div class="add-col">
                                        <div class="gom-col">
                                            <div class="col-md-4">
                                                <?php $field = ['name' => 'add_label_name3', 'type' => 'text', 'class' => 'add_label_name', 'label' => 'Nhãn'];?>
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                            </div>
                                            <div class="col-md-7">
                                                <?php $field = ['name' => 'add_label_id3', 'type' => 'select2_model', 'class' => 'add_label_id select-label item-select2', 'label' => 'Kiểu',
                                                    'model' => \Modules\JdesLabel\Models\Label::class, 'display_field' => 'name'];?>
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                            </div>
                                            <div class="col-sm-2 ed">
                                                <?php $field = ['name' => 'add_type3', 'type' => 'select', 'class' => 'add_type', 'label' => 'Loại',
                                                    'options' => [
                                                        'Nhãn', 'Nhãn phù hợp', 'Giá trị phù hợp', 'Loại trừ nhãn', 'Loại trừ giá trị', 'Đơn giá'
                                                    ]];?>
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                            </div>
                                            <div class="col-btn">
                                                <button title="Thêm giá trị" class="btn btn-success btn-add-edit" data-col="3" type="button">
                                                    Thêm
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <style>.btn-add-edit{padding:7px 5px 6.5px 5px; transform: translate(-14px); margin-top: 1.3px; font-size: 12px}</style>
                <div class="col-xs-12 f-col select-height">
                    <div class="col-sm-1">
                        <label style="padding-top: 10px;">Áp dụng cho</label>
                    </div>
                    <div class="col-sm-10 no-padding xs-col">
                        <?php
                        $fields = [
                            ['name' => 'apply_user_ids', 'type' => 'select2_model', 'class' => '', 'label' => 'Client',
                                'model' => \Modules\JdesUser\Models\BillPayment::class, 'display_field' => 'name', 'multiple' => true],
                            ['name' => 'apply_admin_ids', 'type' => 'select2_model', 'class' => '', 'label' => 'BillPayment',
                                'model' => \App\Models\Admin::class, 'display_field' => 'name', 'multiple' => true],
                            ['name' => 'apply_group_user_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Group',
                                'model' => \Modules\JdesSetting\Models\UserGroup::class, 'display_field' => 'name', 'multiple' => true],
                        ];
                        foreach ($fields as $k => $field) {
                            $field['value'] = @$result->{$field['name']};
                            $tab['td'][$k] = $field;
                            $fields[$k] = $field;
                        }
                        ?>
                        <div class="row">
                            @foreach($fields as $field)
                                <div class="col-sm-4 col-md-4 no-padding">
                                    @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-12 box-color">
                <div class="row">
                    <div class="col-md-2 box-note">
                        <div class="group-1"
                             style="width: 55px; height: 30px; display: inline-flex;"></div>
                        <span>nhãn chung lặp được</span>
                    </div>
                    <div class="col-md-2 box-note">
                        <div class="group-2"
                             style="width: 55px; height: 30px; display: inline-flex;"></div>
                        <span>nhãn riêng</span>
                    </div>
                    <div class="col-md-2 box-note">
                        <div class="group-3"
                             style="width: 55px; height: 30px; display: inline-flex;"></div>
                        <span>nhãn riêng quy tắc</span>
                    </div>
                </div>
            </div>
            <div class="box-footer">
{{--                <a type="reset" class="btn btn-s-md btn-default"--}}
{{--                   href="{{ URL::to('/admin/unit_price') }}">Quay lại</a>--}}
                <a href="{{ URL::to('/admin/unit_price') }}" class="btn btn-clean1 kt-margin-r-10">
                    <i class="la la-arrow-left"></i>
                    <span class="kt-hidden-mobile">Quay lại</span>
                </a>
                <button type="reset" class="btn btn-s-md btn-primary">Reset dữ liệu</button>
                <input type="submit" class="btn btn-s-md btn-primary" name="duplicate" value="Nhân bản" style="    margin-left: 10px;"/>
                <button type="submit" class="btn btn-info pull-right" style="    margin-left: 13px;">Áp dụng</button>
            </div>
        </div>
    </form>
@endsection
@section('custom_head')
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset(config('core.admin_asset').'/css/form.css') }}">


    <link type="text/css" rel="stylesheet" charset="UTF-8" href="{{ asset('public/backend/css/unit_price.css') }}?v={{ date('s') }}">
    <link href="{{ asset('public/backend/css/styles-fix.css') }}?v={{ date('s') }}" rel="stylesheet" type="text/css"/>
    <style>
        #product-editor .btn.btn-clean1:hover {
            color: #5d78ff;
            background: #fff;
        }
        #product-editor .row-cus .old-col div > .col-sm-2 > label {
            text-align: right;
            vertical-align: -webkit-baseline-middle;
        }
        #product-editor .add-col,
        #product-editor #order-content {
            display: inline-block;
        }

        #product-editor #order-content {
            width: 100%;
        }

        #product-editor .item-select2 select {
            display: none;
        }

        #product-editor .delete-col {
            position: absolute;
            right: 0;
        }

        #product-editor .add-col {
            width: 100%;
            text-align: right;
        }

        #product-editor span.price,
        #product-editor span.price_text {
            float: left;
        }

        #product-editor span.price_text {
            margin-left: 10px;
        }
        #product-editor .box-color .group-label .item-row-content .old-col .don_vi {
            line-height: 2.5;
        }
        #product-editor .box-color .group-label .item-row-content .old-col div.col-sm-2:last-child {
            padding: 0 5px;
            text-align: center;
        }
        #product-editor .box-color .group-label .item-row-content .old-col .delete-col {
            width: 24px;
            box-shadow: 0 0 10px rgba(0,0,0,.3);
        }
        #product-editor .item-col > div {
            display: inline-block;
            float: left;
        }

        #product-editor .item-col {
            display: inline-block;
            float: left;
            width: 100%;
            margin-bottom: 10px;
        }
        #product-editor .lb-name,
        #product-editor span.don_vi {
            width: 100%;
            height: 38.39px;
            margin: 0;
            line-height: 37px;
        }

        #product-editor .box-color .group-label .item-row-content .old-col .delete-col {
            margin: 0;
            padding: 0;
            margin-top: 7px;
            text-align: center;
            border: 0;
        }
        #product-editor .item-col > div.col-sm-8 {
            padding-right: 0;
        }
        #product-editor .item-col > div.col-sm-2 {
            padding: 0;
        }
        #product-editor .add-col .gom-col > div {
            padding: 10px 0px;
        }
        #product-editor span.select2-selection__rendered{
            text-align: left;
        }
        #product-editor .select2-container--default .select2-selection--single .select2-selection__arrow:before {
            position: absolute;
            left: 0;
        }
        #product-editor button.delete-col {margin-right: 10px!important;}

        #product-editor i.fa-trash {
            margin-left: 10px!important;
        }

        #product-editor label.lb-name {
            text-align: right;
        }
        #product-editor .add-col{
            padding: 0!important;
        }
    </style>
@endsection
@section('custom_footer')
    <script src="{{ asset(config('core.admin_asset').'/js/pages/crud/metronic-datatable/advanced/vertical.js') }}"
            type="text/javascript"></script>

    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce_editor.js') }}"></script>
    <script type="text/javascript">
        editor_config.selector = ".editor";
        editor_config.path_absolute = "{{ (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]" }}/";
        tinymce.init(editor_config);
    </script>
    <script type="text/javascript" src="{{ asset(config('core.admin_asset').'/js/form.js') }}"></script>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('public/backend/js/form.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/libs/select2/js/select2.min.js') }}"></script>
    <script>
        function addCol(label_name, label_id, type, add_label_range, order_no, column) {
            $.ajax({
                url: '/admin/unit_price/add-label',
                type: 'GET',
                data: {
                    label_name: label_name,
                    label_id: label_id,
                    label_type: type,
                    add_label_range: add_label_range,
                    type: 'unit_price',
                    order_no: order_no,
                    column: column
                },
                success: function (resp) {
                    $('.group-' + column + ' .item-row-body').append(resp);
                    increaseInput('count_col' + column);
                    increaseInput('count_max_col' + column);
                }
            });
        }

        function increaseInput(input_name) {
            var count_row = parseInt($('input[name=' + input_name + ']').val());
            $('input[name=' + input_name + ']').val(count_row + 1);
        }

        function downInput(input_name) {
            var count_row = parseInt($('input[name=' + input_name + ']').val());
            $('input[name=' + input_name + ']').val(count_row - 1);
        }

        $(document).ready(function () {
            $('.add-col button').click(function () {
                var label_name = $(this).parents('.add-col').find('.add_label_name').val();
                var label_id = $(this).parents('.add-col').find('.add_label_id').val();
                var type = $(this).parents('.add-col').find('.add_type').val();
                var add_label_range = false;
                var column = $(this).data('col');
                var order_no = parseInt($('input[name=count_max_col' + column + ']').val());
                addCol(label_name, label_id, type, add_label_range, order_no, column);
            });

            <?php   //  Cac nhan relation. thay doi nhan se keo theo cac nhan khac
            $label_relation = ['xuong_in'];
            ?>

            $('body').on('click', '.delete-col', function () {
                downInput('count_col' + $(this).parents('.group-label').data('col'));
                @foreach($label_relation as $v)
                if ($(this).parents('.item-col').data('label-code') == '{{ $v }}') { //  Neu xoa select xuong in thi xoa het  cac thong tin cua xuong in
                    $('.{{ $v }}_data').remove();
                }
                @endforeach
                $(this).parents('.item-col').remove();
            });

            @foreach($label_relation as $v)
            //  Thay doi nhan relation thi se tu dong them cac nhan keo theo
            $('body').on('change', '.{{ $v }}', function () {
                var item_id = $(this).val();
                var column = $(this).parents('.group-label').data('col');
                var order_no = parseInt($('input[name=count_max_col' + column + ']').val());
                var type = '{{ $v }}';
                $.ajax({
                    url: '/admin/auto-add-cols-to-unit-price',
                    type: 'GET',
                    data: {
                        item_id: item_id,
                        order_no: order_no,
                        type: type
                    },
                    success: function (resp) {
                        $('.xuong_in_items_data').remove();
                        $('.group-' + column + ' .item-row-body').append(resp);

                        if (type == 'xuong_in') {
                            //  Cong them 3 order_no vi chon xuong in ra xong se chen vao 3 nhan
                            increaseInput('count_col' + column);
                            increaseInput('count_max_col' + column);

                            increaseInput('count_col' + column);
                            increaseInput('count_max_col' + column);

                            increaseInput('count_col' + column);
                            increaseInput('count_max_col' + column);
                        }
                    }
                });
            });
            @endforeach
        });
    </script>
@endpush