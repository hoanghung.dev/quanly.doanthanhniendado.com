@extends('admin.template')
@section('main')
    <div class="content-wrapper form-primary">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <form id="" method="post" action=""
                              class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="col-xs-12">
                                    <?php
                                    $fields = [];
                                    foreach ($module['form']['tabs'] as $tab_code => $tab) {
                                        foreach ($tab['td'] as $k => $field) {
                                            $field['value'] = @$result->{$field['name']};
                                            $tab['td'][$k] = $field;
                                            $fields[] = $field;
                                        }
                                        $module['form']['tabs'][$tab_code] = $tab;
                                    }
                                    ?>
                                    @foreach($module['form']['tabs'] as $key => $tab)
                                        @foreach($tab['td'] as $field)
                                            @include("admin.common.td.".$field['type'], ['field' => $field])
                                        @endforeach
                                    @endforeach
                                </div>
                                <div class="col-xs-12"><h3>Các cột trong hàng</h3></div>
                                <div class="col-xs-12">
                                    <div id="order-content">
                                        <div class="item-row-content">
                                            <div class="col-xs-12 item-col item-col">
                                                <div class="col-sm-2">
                                                    <label>Tên nhãn <span class="color_btd">*</span></label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label>Kiểu nhãn <span class="color_btd">*</span></label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label>Tham số</label>
                                                </div>
                                                <div class="col-sm-1">
                                                    <label>Toán tử</label>
                                                </div>
                                                <div class="col-sm-1">
                                                    <label>Thứ tự</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Ghi chú</label>
                                                </div>
                                            </div>
                                            <div class="old-col">
                                                <?php $row = \Modules\JdesSetting\Models\OrderRows::find(@$row_id);
                                                $cols = [];
                                                if (is_object($row)) {
                                                    $cols = $row->cols;
                                                }
                                                ?>
                                                @foreach($cols as $k => $col)
                                                    @include('admin.custom.td.item_label', ['order_no' => $k + 1,'col' => $col])
                                                @endforeach
                                            </div>
                                            <div class="item-row-body old-col">
                                                {{--Js hiển thị các nhãn ở đây--}}
                                            </div>
                                            <input value="{{ isset($cols) ? count($cols) + 1 : 0 }}" name="count_col"
                                                   type="hidden">
                                            <div class="add-col">
                                                <span class="price"><b>Tổng giá trị: {{ isset($result) ? number_format($result->price, 0, '.', '.') : 0 }}đ</b></span>
                                                <span class="price_text">/<input name="price_text"
                                                                                 placeholder="Đơn vị giá"
                                                                                 value="{{ @$result->price_text }}"></span>
                                                <button class="btn btn-success" type="button">Thêm dòng</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <a type="reset" class="btn btn-s-md btn-default"
                                   href="{{ URL::to('/admin/edit_order/' . $order_id) }}">Quay lại</a>
                                <button type="reset" class="btn btn-s-md btn-primary">Reset dữ liệu</button>
                                <button type="submit" class="btn btn-info pull-right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('custom_header')
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset('public/libs/select2/css/select2.min.css') }}">
    <style>
        .add-col,
        #order-content {
            display: inline-block;
        }

        #order-content {
            width: 100%;
        }

        .item-select2 select {
            display: none;
        }

        .delete-col {
            position: absolute;
            right: 0;
        }

        .add-col {
            width: 100%;
            text-align: right;
        }

        span.price,
        span.price_text {
            float: left;
        }

        span.price_text {
            margin-left: 10px;
        }
    </style>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('public/backend/js/form.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/libs/select2/js/select2.min.js') }}"></script>
    <script>
        function addCol(col_id) {
            $.ajax({
                url: '/admin/add-label?col=' + col_id,
                type: 'GET',
                success: function (resp) {
                    $('#order-content .item-row-content .item-row-body').append(resp);
                }
            });
        }

        function increaseInput(input_name) {
            var count_row = parseInt($('input[name=' + input_name + ']').val());
            $('input[name=' + input_name + ']').val(count_row + 1);
        }

        $(document).ready(function () {
            @if(!isset($cols) || count($cols) == 0)
            //  Nếu không có sẵn col nào thì tự động hiển thị 1 col
            $.ajax({
                url: '/admin/add-label?col={{ isset($cols) ? count($cols) : 0 }}',
                type: 'GET',
                success: function (resp) {
                    $('#order-content .item-row-content .item-row-body').append(resp);
                    increaseInput('count_col');
                }
            });
            @endif

            $('.add-col button').click(function () {
                var count_row = parseInt($('input[name=count_col]').val());
                addCol(count_row);
                increaseInput('count_col');
            });

            $('body').on('click', '.delete-col', function () {
                $(this).parents('.item-col').remove();
            });

            //  Thay đổi label thì đổi input nhập tham số tương ứng
            $('body').on('change', '.select-label', function () {
                var label_id = $(this).val();
                var order_no = $(this).parents('.item-col').data('order_no');
                var object = $(this);
                $.ajax({
                    url: '/admin/get-input-value-label?label_id=' + label_id + '&order_no=' + order_no,
                    type: 'GET',
                    success: function (resp) {
                        object.parents('.item-col').find('.tham-so').html(resp);
                    }
                });
            });
        });
    </script>
@endpush