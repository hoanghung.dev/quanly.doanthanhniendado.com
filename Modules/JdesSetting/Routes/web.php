<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'factory'], function () {
        Route::post('send-bill', 'Admin\FactoryController@postSendBill')->middleware('permission:bill_edit');

        Route::get('', 'Admin\FactoryController@getIndex')->name('factory')->middleware('permission:factory_view');
        Route::get('publish', 'Admin\FactoryController@getPublish')->name('factory.publish')->middleware('permission:factory_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\FactoryController@add')->middleware('permission:factory_add');
        Route::get('delete/{id}', 'Admin\FactoryController@delete')->middleware('permission:factory_delete');
        Route::post('multi-delete', 'Admin\FactoryController@multiDelete')->middleware('permission:factory_delete');
        Route::get('search-for-select2', 'Admin\FactoryController@searchForSelect2')->name('factory.search_for_select2')->middleware('permission:factory_view');
        Route::get('{id}', 'Admin\FactoryController@update')->middleware('permission:factory_view');
        Route::post('{id}', 'Admin\FactoryController@update')->middleware('permission:factory_edit');
    });

    Route::group(['prefix' => 'unit_price'], function () {
        Route::get('', 'Admin\UnitPriceController@getIndex')->name('unit_price')->middleware('permission:unit_price_view');
        Route::get('publish', 'Admin\UnitPriceController@getPublish')->name('unit_price'.'.publish')->middleware('permission:unit_price_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\UnitPriceController@add')->middleware('permission:unit_price_add');
        Route::get('delete/{id}', 'Admin\UnitPriceController@delete')->middleware('permission:unit_price_delete');
        Route::post('multi-delete', 'Admin\UnitPriceController@multiDelete')->middleware('permission:unit_price_delete');
        Route::get('search-for-select2', 'Admin\UnitPriceController@searchForSelect2')->name('unit_price'.'.search_for_select2')->middleware('permission:unit_price_view');
        Route::get('add-label', 'Admin\UnitPriceController@addLabel');

        Route::get('{id}', 'Admin\UnitPriceController@update');
        Route::post('/{id}', 'Admin\UnitPriceController@update');
    });

    Route::group(['prefix' => 'product'], function () {
        Route::get('{id}/editor', 'Admin\EditorController@getEditor')->middleware('permission:product_view');

        Route::match(['GET', 'POST'], 'editor/{id}', 'Admin\EditorController@detail')->middleware('permission:product_view');
    });


    Route::group(['prefix' => 'editor'], function () {
        Route::get('add-bill-draft', 'Admin\EditorController@addBillDraft');

        Route::get('{order_id}/delete_row/{row_id}', 'Admin\OrderRowController@delete');
        Route::get('{order_id}/delete_col/{col_id}', 'Admin\OrderRowController@deleteCol');

        Route::match(array('GET', 'POST'), '{order_id}/edit_row/{row_id}', 'Admin\OrderRowController@update');

        Route::get('add-plan/{id}', 'Admin\EditorController@addPlan');
        Route::get('edit-plan', 'Admin\EditorController@editPlan');
        Route::get('delete-plan/{id}/{oder_id}', 'Admin\EditorController@deletePlan');
        Route::post('insert-label-to-order', 'Admin\OrderRowController@insertLabelToOrder');
        Route::get('input-label-info', 'Admin\EditorController@inputLabelInfo');
        Route::get('input-unit-price-info', 'Admin\EditorController@inputUnitPriceInfo');
        Route::get('input-order-info', 'Admin\EditorController@inputOrderInfo');
        Route::get('change-goroup-label', 'Admin\EditorController@delete_order');
        //add unit_price_columns
        Route::post('unit-price-columns', 'Admin\EditorController@addUniPriceColumn');
        //aja get id usre
        Route::post('get-data-ajax-bill', 'Admin\EditorController@getAjaxIdUserByBill');
        // save dup
        Route::post('save-data-ajax-duplicate-order', 'Admin\EditorController@saveDuplicateOrder');
        Route::get('item-old-set-price', 'Admin\OrderRowController@itemOldSetPrice');

        Route::post('edit-order-col', 'Admin\EditorController@postEditOrderCol');
        Route::get('get-html-order-col', 'Admin\EditorController@getHtmlOrderCol');
        Route::post('{id}/add-group', 'Admin\EditorController@postAddGroup');
        Route::get('{order_id}/order_group/{order_group_id}/delete', 'Admin\EditorController@deleteOrderGroup');

        //  Group product
        Route::post('{id}/add-group-product', 'Admin\EditorController@postAddGroupProduct');

        Route::post('{order_id}/order_row/change-name', 'Admin\OrderRowController@postChangeName');

        //  Set gia tri cho col
        Route::get('html-item-old-set-price', 'Admin\OrderRowController@getTextItemOldSetPrice');
        Route::get('add-item-set_price', 'Admin\OrderRowController@addItemSetPrice');
        Route::post('post-item-set_price', 'Admin\OrderRowController@postItemSetPrice');

        //  Set quy tac cho col
        Route::get('item-old-set-order-col-rule', 'Admin\OrderRowController@itemOldSetOrderColRule');
        Route::get('set-order-col-rule', 'Admin\EditorController@setOrderColRule');
        Route::get('add-item-set-order-col-rule', 'Admin\OrderRowController@addItemSetOrderColRule');
        Route::post('post-order-col-rule', 'Admin\OrderRowController@postOrderColRule');
        Route::post('delete-order-col-rule', 'Admin\OrderRowController@deleteOrderColRule');
        // ajax set role view row
        Route::post('publish-row', 'Admin\OrderRowController@publishRow');

        //  Duyệt đơn
        Route::post('approval', 'Admin\EditorController@approval');

        Route::get('get-order-template-option', 'Admin\EditorController@getOrderTemplateOption');

        //  Template
        Route::get('{id}/print', 'Admin\EditorController@getPrint');

        //  Plan
        Route::post('/active-plan', 'Admin\EditorController@activePlan');

    });
});

Route::group(['prefix' => 'admin'], function () {
    Route::group(['prefix' => 'editor'], function () {
        Route::get('change-value-col', 'Admin\OrderRowController@changeValueCol');

    });
});