<?php echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    <?php
    $page = str_replace('.xml', '', $page);
    $skip = ($page - 1) * 500;
    $take = 500;
    ?>
    {{--Sitemap danh muc--}}
    <?php
    $data = \App\Models\Product::select(['image', 'name', 'slug', 'category_id', 'updated_at'])->where('status', 1)->where('slug', '!=', '')
        ->skip($skip)->take($take)->get();
    ?>
    @foreach($data as $item)
        <url>
            <loc>{{ CommonHelper::getProductSlug($item) }}</loc>
            <image>
                <loc>{{ CommonHelper::getUrlImageThumb($item->image, 73, 107) }}</loc>
                <caption>{{ $item->name }}</caption>
                <license>{{ URL::to('/') }}</license>
                <family_friendly>yes</family_friendly>
            </image>
            <lastmod>{{ date("Y-m-d", strtotime($item->updated_at))}}T{{ date("H:i:s", strtotime($item->updated_at))}}+07:00</lastmod>
            <changefreq>always</changefreq>
            <priority>0.4</priority>
        </url>
    @endforeach
</sitemapindex>