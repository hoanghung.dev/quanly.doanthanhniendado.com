<?php

Route::get('sitemap.xml', function () {
    return response()->view('jdessitemap::sitemap')->header('Content-Type', 'text/xml');
});
Route::get('post-sitemap.xml', function () {
    return response()->view('jdessitemap::post_sitemap')->header('Content-Type', 'text/xml');
});
Route::get('category-sitemap.xml', function () {
    return response()->view('jdessitemap::category_sitemap')->header('Content-Type', 'text/xml');
});
Route::get('product-sitemap.xml', function () {
    return response()->view('jdessitemap::product_sitemap')->header('Content-Type', 'text/xml');
});
Route::get('post-sitemap/{page}', function ($page) {
    return response()->view('jdessitemap::post_detail_sitemap', ['page' => $page])->header('Content-Type', 'text/xml');
});
Route::get('product-sitemap/{page}', function ($page) {
    return response()->view('jdessitemap::product_detail_sitemap', ['page' => $page])->header('Content-Type', 'text/xml');
});