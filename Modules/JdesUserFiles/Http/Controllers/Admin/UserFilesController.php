<?php

namespace Modules\JdesUserFiles\Http\Controllers\Admin;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\EworkingUser\Models\User;
use Validator;

class UserFilesController extends Controller
{

    protected $module = [
        'code' => 'user',
        'label' => 'File lưu trữ của khách hàng',
    ];

    public function getIndex(Request $request, $id)
    {
        $data['result'] = User::find($id);

        $data['module'] = $this->module;
        $data['page_title'] = $this->module['label'];

        return view('jdesuserfiles::index')->with($data);
    }
}
