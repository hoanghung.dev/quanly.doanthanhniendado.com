<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'user_files'], function () {
        Route::get('/{id}', 'Admin\UserFilesController@getIndex')->middleware('permission:user_view');
    });
});
