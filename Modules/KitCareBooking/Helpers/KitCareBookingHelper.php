<?php

namespace Modules\KitCareBooking\Helpers;

use App\Mail\MailServer;
use App\Models\Admin;
use App\Models\Setting;
use Modules\EduMarketing\Models\MarketingMail;
use Modules\KitCareBooking\Models\ErrorCode;
use Modules\KitCareNotification\Http\Controllers\KitCareNotificationController;
use View;
use Session;

class KitCareBookingHelper
{

    public static function pushNotiFication($booking, $msg_des, $except_admin_ids, $append_admin_ids = [])
    {
        try {
            $notifiController = new KitCareNotificationController();

            //  Truy van ra super admin
            $admin_ids = \DB::table('admin')
                ->join('role_admin', 'role_admin.admin_id', '=', 'admin.id')
                ->join('roles', 'role_admin.role_id', '=', 'roles.id')
                ->where('roles.name', 'super_admin')
                ->whereNotIn('admin.id', $except_admin_ids)
                ->pluck('admin.id')->toArray();

            if (!empty($append_admin_ids)) {
                $admin_ids = array_merge($admin_ids, $append_admin_ids);
            }

            foreach ($admin_ids as $id) {
                $notifiController->sendMessage([
                    'content' => 'Đơn hàng #' . $booking->id . ': ' . $msg_des,
                    'user_id' => $id,
                    'item_id' => $booking->id
                ]);
            }

            //  Gửi thông báo cho KTV
            foreach (explode('|', $booking->ktv_ids) as $id) {
                if ($id != '' && !in_array($id, $except_admin_ids)) {
                    $notifiController->sendMessage([
                        'content' => 'Đơn hàng #' . $booking->id . ': ' . $msg_des,
                        'user_id' => $id,
                        'item_id' => $booking->id
                    ]);
                }
            }

            //  Truy van ra khach hang
            if (!in_array($booking->admin_id, $except_admin_ids)) {
                $notifiController->sendMessage([
                    'content' => 'Đơn hàng #' . $booking->id . ': ' . $msg_des,
                    'user_id' => $booking->admin_id,
                    'item_id' => $booking->id
                ]);
            }

            return [
                'status' => true,
                'msg' => 'Thành công!'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => false,
                'msg' => $ex->getMessage()
            ];
        }
    }

    public static function sendMailToAdmin($booking, $camp_id = 16)
    {
        //  Gửi mail cho khách
        try {

            $camp = MarketingMail::find($camp_id);
            $mailSetting = Setting::whereIn('type', ['mail'])->pluck('value', 'name')->toArray();

            //  Lấy danh sách email của admin thành dạng mảng
            $admin_emails = $mailSetting['admin_emails'];
            $admin_emails = explode(',', $admin_emails);
            //  Lấy thông tin người nhận là email đầu tiên trong mảng
            $user = (object)[
                'email' => $admin_emails[0],
                'name' => 'Admin',
                'id' => null
            ];

            //  Lấy những email khác cho vào cc
            $cc = $admin_emails;
            unset($cc[0]);

            $data = [
                'sender_account' => $camp->email_account,
                'user' => $user,
                'subject' => $camp->subject,
                'content' => KitCareBookingHelper::processContentMail($camp->email_template->content, $booking),
                'cc' => $cc
            ];

            \Mail::to($data['user'])->send(new MailServer($data));

            return response()->json([
                'status' => true,
                'msg' => 'Thành công',
                'errors' => (object)[],
                'data' => $data,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public static function processContentMail($html, $booking)
    {
//        $html = str_replace('{name}', $booking->name, $html);
//        $html = str_replace('{tel}', $booking->tel, $html);
//        $html = str_replace('{address}', $booking->address, $html);
//        $html = str_replace('{note}', $booking->note, $html);
        $html = str_replace('{product_code}', $booking->product_code, $html);
        $html = str_replace('{product_name}', @$booking->product->name, $html);
        $html = str_replace('{manufacturer_name}', @$booking->producer->name, $html);

        $error_code_id = explode('|', $booking->error_code_id);
        $str = '';
        $ErrorCode = ErrorCode::whereIn('id', $error_code_id)->pluck('name');
        foreach ($ErrorCode as $v) {
            $str .= $v . ',';
        }
        $html = str_replace('{error_code_id}', $str, $html);
        $html = str_replace('{bonus_services}', $booking->bonus_services == 1 ? 'Sửa DV' : 'Bảo hành MEEG', $html);
        $html = str_replace('{care}', $booking->care . ' tháng', $html);
        $html = str_replace('{customer_name}', @$booking->admin->name, $html);
        $html = str_replace('{customer_tel}', @$booking->admin->tel, $html);
        $html = str_replace('{customer_address}', @$booking->admin->address . '. ' . @$booking->admin->ward->name . ' - ' . @$booking->admin->district->name . ' - ' . @$booking->admin->province->name, $html);
        $html = str_replace('{time}', date('H:i d/m/Y', strtotime($booking->time)), $html);
        $html = str_replace('{booking_link}', 'https://quanly.kitcare.vn/admin/booking/' . $booking->id, $html);

        return $html;
    }

    public static function setDeadline($booking)
    {
        $date1 = date_create(date('Y-m-d', strtotime($booking->time)));
        $date2 = date_create(date('Y-m-d'));
        $diff = date_diff($date1, $date2);
        $booking->deadline = strtotime($booking->time) > time() ? $diff->d : 0 - $diff->d;
        $booking->save();
        return true;
    }
}