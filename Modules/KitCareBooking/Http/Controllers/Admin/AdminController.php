<?php

namespace Modules\KitCareBooking\Http\Controllers\Admin;

use App\Http\Controllers\Admin\RoleController;
use App\Http\Helpers\CommonHelper;
use App\Models\{Admin, RoleAdmin, Setting, Roles, User};
use Auth;
use Illuminate\Http\Request;
use Mail;
use Modules\KitCareBooking\Models\PhongBan;
use Session;
use Validator;

class AdminController extends CURDBaseController
{
    protected $orderByRaw = 'tong_diem DESC';

    protected $_role;

    public function __construct()
    {
        parent::__construct();
        $this->_role = new RoleController();
    }

    protected $module = [
        'code' => 'admin',
        'label' => 'Đoàn viên',
        'modal' => '\App\Models\Admin',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'admin.image'],
            ['name' => 'name', 'type' => 'text_admin_edit', 'label' => 'admin.name', 'sort' => true],
            ['name' => 'role_id', 'type' => 'role_name', 'label' => 'admin.permission'],
            ['name' => 'tel', 'type' => 'text', 'label' => 'admin.phone', 'sort' => true],
            ['name' => 'email', 'type' => 'text', 'label' => 'admin.email', 'sort' => true],
            ['name' => 'province_id', 'type' => 'relation_filter', 'label' => 'Địa chỉ', 'object' => 'province', 'display_field' => 'name'],
//            ['name' => 'count_booking', 'type' => 'custom', 'td' => 'kitcarebooking::list.td.count_booking', 'label' => 'Yêu cầu',],
//            ['name' => 'id', 'type' => 'count_log', 'label' => 'Số thao tác', 'sort' => true],
//            ['name' => 'status', 'type' => 'status', 'label' => 'admin.status', 'sort' => true],
//            ['name' => 'ngay_sinh', 'type' => 'text', 'label' => 'Ngày sinh', 'sort' => true],
//            ['name' => 'thang_sinh', 'type' => 'text', 'label' => 'Tháng sinh', 'sort' => true],
//            ['name' => 'nam_sinh', 'type' => 'text', 'label' => 'Năm sinh', 'sort' => true],
            ['name' => 'phong_ban_id', 'type' => 'relation_filter', 'label' => 'Đơn vị', 'object' => 'phong_ban', 'display_field' => 'name'],
            ['name' => 'name', 'type' => 'custom', 'td' => 'kitcarebooking::admin.list.td.so_su_kien', 'label' => 'Số sự kiện', 'sort' => true],
//            ['name' => 'name', 'type' => 'custom', 'td' => 'kitcarebooking::admin.list.td.so_diem', 'label' => 'Số điểm', 'sort' => true],
            ['name' => 'tong_diem', 'type' => 'number', 'td' => 'kitcarebooking::phong_ban.list.td.tong_diem', 'label' => 'Số điểm', 'sort' => true],
//            ['name' => 'ngay_bat_dau', 'type' => 'date_vi', 'label' => 'Ngày vào', 'sort' => true],
//            ['name' => 'ngay_nghi_viec', 'type' => 'date_vi', 'label' => 'Ngày nghỉ', 'sort' => true],
            ['name' => 'updated_at', 'type' => 'date_vi', 'label' => 'admin.update', 'sort' => true],
            ['name' => 'xep_hang', 'type' => 'number', 'label' => 'Xếp hạng thi đua', 'object' => 'admin_update', 'display_field' => 'name'],
//            ['name' => 'admin_id', 'type' => 'relation_filter', 'label' => 'Người tạo', 'object' => 'admin', 'display_field' => 'name'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'admin.full_name', 'group_class' => 'col-md-6'],
                ['name' => 'short_name', 'type' => 'text', 'class' => '', 'label' => 'Tên ngắn gọn', 'group_class' => 'col-md-6'],
                ['name' => 'email', 'type' => 'text', 'class' => 'required', 'label' => 'admin.email', 'group_class' => 'col-md-6'],
                ['name' => 'tel', 'type' => 'text', 'label' => 'admin.phone', 'group_class' => 'col-md-6'],
                ['name' => 'password', 'type' => 'password', 'class' => 'required', 'label' => 'admin.password', 'group_class' => 'col-md-6'],
                ['name' => 'password_confimation', 'type' => 'password', 'class' => 'required', 'label' => 'admin.re_password', 'group_class' => 'col-md-6'],
                ['name' => 'role_id', 'type' => 'select2_model', 'label' => 'Quyền','class' => 'required', 'model' => \App\Models\Roles::class, 'display_field' => 'display_name', 'group_class' => 'col-md-6'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'admin.active', 'value' => 1, 'group_class' => 'col-md-6'],
            ],
            'more_info_tab' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Ảnh đại diện'],
                ['name' => 'facebook', 'type' => 'text', 'class' => '', 'label' => 'facebook'],
                ['name' => 'skype', 'type' => 'text', 'class' => '', 'label' => 'skype'],
                ['name' => 'zalo', 'type' => 'text', 'class' => '', 'label' => 'zalo'],
            ],
        ]
    ];

    protected $quick_search = [
        'label' => 'ID, tên, sđt, email',
        'fields' => 'id, name, tel, email'
    ];

    protected $filter = [
        'status' => [
            'label' => 'admin.status',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'admin.status',
                0 => 'admin.hidden',
                1 => 'admin.active'
            ]
        ],
        'phong_ban_id' => [
            'label' => 'Phòng ban',
            'type' => 'select2_model',
            'display_field' => 'name',
            'object' => 'phong_ban',
            'model' => PhongBan::class,
            'query_type' => '='
        ],
        'role_id' => [
            'label' => 'Quyền',
            'type' => 'select2_model',
            'display_field' => 'display_name',
            'object' => 'role',
            'model' => Roles::class,
            'query_type' => 'custom'
        ],

    ];


    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('kitcarebooking::admin.list')->with($data);
    }

    public function searchForSelect2(Request $request)
    {

        $not_customer = \App\Models\RoleAdmin::where('role_id', '=', 3)->pluck('admin_id')->toArray();
        $data =   Admin::whereIn('id', $not_customer)->where('name','like','%'.$request->name.'%')->orderBy($request->col, 'asc')->limit(7)->get();
        return response()->json([
            'status' => true,
            'items' => $data
        ]);
    }


    public function appendWhere($query, $request)
    {

        if (!is_null($request->get('su_kien'))) {
            $ids = \Modules\KitCareBooking\Models\SuKienThanhVien::
            where('su_kien_id', $request->su_kien)->pluck('admin_id')->toArray();

            $query = $query->whereIn('id', $ids);
        }


        $not_customer = RoleAdmin::where('role_id', '!=', 3)->pluck('admin_id')->toArray();
        $query = $query->whereIn('id', $not_customer);

        if (!is_null($request->get('role_id'))) {
            $admin_id_arr = RoleAdmin::where('role_id', $request->role_id)->pluck('admin_id')->toArray();
            $query = $query->whereIn('id', $admin_id_arr);
        }

        return $query;
    }

    public function ajaxGetInfo(Request $r) {
        $admin = Admin::find($r->id);
        if (!is_object($admin)) {
            return response()->json([
                'status' => false,
                'msg' => 'Không tìm thấy bản ghi',
                'data' => $admin
            ]);
        }
        $admin->image = CommonHelper::getUrlImageThumb(@$admin->image, 250, null);
        $admin->province_name = @$admin->province->name;
        $admin->district_name = @$admin->district->name;
        $admin->ward_name = @$admin->ward->name;

        return response()->json([
            'status' => true,
            'msg' => '',
            'data' => $admin
        ]);
    }
}



