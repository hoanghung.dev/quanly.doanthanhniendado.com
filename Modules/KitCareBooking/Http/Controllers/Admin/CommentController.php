<?php

namespace Modules\KitCareBooking\Http\Controllers\Admin;

use App\Http\Controllers\Admin\ChangeDataHistoryController;
use App\Http\Helpers\CommonHelper;
use App\Models\ChangeDataHistory;
use Auth;
use Illuminate\Http\Request;

use Modules\KitCareBooking\Helpers\KitCareBookingHelper;
use Validator;

class CommentController extends CURDBaseController
{

    protected $whereRaw = 'reply is NULL';

    protected $module = [
        'code' => 'comment',
        'table_name' => 'comments',
        'label' => 'Bình luận',
        'modal' => '\Modules\KitCareBooking\Models\Comment',
        'list' => [
            ['name' => 'admin_id', 'type' => 'relation', 'label' => 'Người tạo', 'object' => 'admin', 'display_field' => 'name'],
            ['name' => 'content', 'type' => 'custom', 'td' => 'kitcarebooking::list.td.comment_content', 'label' => 'Nội dung'],
            ['name' => 'created_at', 'type' => 'date_vi', 'label' => 'Tạo lúc'],
//            ['name' => 'action', 'type' => 'custom', 'td' => 'kitcarebooking::list.td.action_booking', 'class' => '', 'label' => '#'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'content', 'type' => 'textarea_editor', 'label' => 'Nội dung', 'inner' => 'rows=15'],
            ],

            'info_tab' => [
                ['name' => 'image', 'type' => 'file_image', 'label' => 'Ảnh'],
                ['name' => 'image_present', 'type' => 'multiple_image_dropzone', 'label' => 'Ảnh khác'],
            ],
        ],
    ];

    protected $filter = [
        'content' => [
            'label' => 'Nội dung',
            'type' => 'text',
            'query_type' => 'like'
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('kitcarebooking::comment.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        $query = $query->where('booking_id', $request->booking_id);

        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('kitcarebooking::comment.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'content' => 'required'
                ], [
                    'content.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    $data['booking_id'] = $request->booking_id;
                    $data['reply'] = $request->get('reply', null);
                    $data['admin_id'] = \Auth::guard('admin')->id();
                    if ($request->has('image_present')) {
                        $data['image_present'] = implode('|', $request->image_present);
                    }

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        KitCareBookingHelper::pushNotiFication($this->model->booking, 'Có bình luận mới', [\Auth::guard('admin')->id()]);

                        //  Lưu lịch sử bình luận
                        ChangeDataHistory::create([
                            'table_name' => 'bookings',
                            'item_id' => $request->booking_id,
                            'admin_id' => \Auth::guard('admin')->id(),
                            'note' => 'Tạo bình luận mới: ' . $data['content']
                        ]);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }
                    $url = '';
                    if ($request->has('booking_id')) {
                        $url .= '?booking_id=' . $request->booking_id;
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . $url);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add' . $url);
                    }

                    return redirect('admin/' . $this->module['code'] . $url);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);


        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('kitcarebooking::comment.edit')->with($data);
        } else if ($_POST) {
            $validator = Validator::make($request->all(), [
                'content' => 'required'
            ], [
                'content.required' => 'Bắt buộc phải nhập tên',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                //  Tùy chỉnh dữ liệu insert

                $data['booking_id'] = $request->booking_id;
                $data['reply'] = $request->get('reply', null);
                if ($request->has('image_present')) {
                    $data['image_present'] = implode('|', $request->image_present);
                }
                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    //  Lưu lịch sử bình luận
                    ChangeDataHistory::create([
                        'table_name' => 'bookings',
                        'item_id' => $request->booking_id,
                        'admin_id' => \Auth::guard('admin')->id(),
                        'note' => 'Sửa bình luận: ' . $data['content']
                    ]);

                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }
                $url = '';
                if ($request->has('booking_id')) {
                    $url .= '?booking_id=' . $request->booking_id;
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id . $url);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add' . $url);
                }

                return redirect('admin/' . $this->module['code'] . $url);
            }
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            $url = '';
            if ($request->has('booking_id')) {
                $url .= '?booking_id=' . $request->booking_id;
            }

            return redirect('admin/' . $this->module['code'] . $url);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }


}
