<?php

namespace Modules\KitCareBooking\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\KitCareBooking\Models\District;
use Validator;

class LocationController extends Controller
{

    public function getDistrictHtmlByProvinceId(Request $request)
    {
        $province_id = $request->province_id;
        $districts = District::where('province_id', $province_id)->get();
        $html = '';
        foreach ($districts as $district) {
            $html .= '<option value="'.$district->id.'">'.$district->name.'</option>';
        }

        return $html;
    }

}
