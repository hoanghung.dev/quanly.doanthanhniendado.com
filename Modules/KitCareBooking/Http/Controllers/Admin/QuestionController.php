<?php

namespace Modules\KitCareBooking\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;

use Modules\KitCareBooking\Models\Exam;
use Modules\KitCareBooking\Models\PhongBan;
use Modules\KitCareBooking\Models\Product;
use Validator;

class QuestionController extends CURDBaseController
{

    protected $module = [
        'code' => 'question',
        'table_name' => 'cau_hoi',
        'label' => 'Câu hỏi',
        'modal' => '\Modules\KitCareBooking\Models\Question',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh'],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Câu hỏi', 'sort' => true],
            ['name' => 'bai_thi_id', 'type' => 'relation','object' => 'exam', 'display_field' => 'name', 'label' => 'Tên bài thi', 'sort' => true],
            ['name' => 'dap_an_a', 'type' => 'text',  'label' => 'Đáp án A'],
            ['name' => 'dap_an_b', 'type' => 'text',  'label' => 'Đáp án B'],
            ['name' => 'dap_an_c', 'type' => 'text',  'label' => 'Đáp án C'],
            ['name' => 'dap_an_d', 'type' => 'text',  'label' => 'Đáp án D'],
            ['name' => 'dap_an_dung', 'type' => 'text',  'label' => 'Đáp án đúng'],

//            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'width' => '60px'],
//            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên chi đoàn', 'sort' => true],
//            ['name' => 'name', 'type' => 'custom', 'td' => 'kitcarebooking::phong_ban.list.td.so_thanh_vien', 'label' => 'Số thành viên', 'sort' => true],
//            ['name' => 'name', 'type' => 'custom', 'td' => 'kitcarebooking::phong_ban.list.td.so_su_kien', 'label' => 'Số sự kiện tham gia', 'sort' => true],
//            ['name' => 'tong_diem', 'type' => 'number', 'td' => 'kitcarebooking::phong_ban.list.td.tong_diem', 'label' => 'Tổng điểm', 'sort' => true],
//            ['name' => 'so_diem', 'type' => 'number', 'label' => 'Số điểm', 'sort' => true],
//            ['name' => 'admin_id', 'type' => 'relation_filter', 'label' => 'Người tạo', 'object' => 'admin', 'display_field' => 'name'],
//            ['name' => 'admin_id', 'type' => 'relation', 'object' => 'admin', 'display_field' => 'name', 'label' => 'Người tạo',],
//            ['name' => 'count_booking', 'type' => 'custom', 'td' => 'kitcarebooking::list.td.phong_ban_count_booking', 'label' => 'Yêu cầu',],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'textarea', 'class' => 'required', 'label' => 'Câu hỏi'],
                ['name' => 'dap_an_a', 'type' => 'textarea', 'class' => 'required', 'label' => 'Đáp án A'],
                ['name' => 'dap_an_b', 'type' => 'textarea', 'class' => 'required', 'label' => 'Đáp án B'],
                ['name' => 'dap_an_c', 'type' => 'textarea', 'class' => 'required', 'label' => 'Đáp án C'],
                ['name' => 'dap_an_d', 'type' => 'textarea', 'class' => 'required', 'label' => 'Đáp án D'],
                ['name' => 'dap_an_dung', 'type' => 'text', 'class' => 'required', 'label' => 'Đáp án đúng','group_class' => 'col-md-3'],
                ['name' => 'bai_thi_id', 'type' => 'select2_model','model' => Exam::class,'object' => 'exam','display_field' => 'name', 'label' => 'Tên bài thi','group_class' => 'col-md-9'],

            ],

            'image_tab' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Ảnh'],

            ],
        ],
    ];

    protected $filter = [
        'bai_thi_id' => [
            'label' => 'Quyền',
            'type' => 'select2_model',
            'display_field' => 'name',
            'object' => 'exam',
            'model' => Exam::class,
            'query_type' => '='
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, Tên',
        'fields' => 'id, name'
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('kitcarebooking::question.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
//        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
//        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
//            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
//        }

        if (!is_null($request->get('su_kien'))) {
            $thanh_vien_ids = \Modules\KitCareBooking\Models\SuKienThanhVien::
            where('su_kien_id', $request->su_kien)->pluck('admin_id')->toArray();


            $ids = \Modules\KitCareBooking\Models\PhongBan::leftJoin('admin', 'admin.phong_ban_id', '=', 'phong_ban.id')
                ->whereIn('admin.id', $thanh_vien_ids)->pluck('phong_ban.id')->toArray();

            $query = $query->whereIn('id', $ids);
        }


        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('kitcarebooking::question.add')->with($data);
            } else if ($_POST) {
//                dd($request->all());
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert


                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    } elseif ($request->return_direct == 'save_editor') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . '/editor');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);

        //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('kitcarebooking::question.edit')->with($data);
        } else if ($_POST) {
//            dd($request->all());
            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ], [
                'name.required' => 'Bắt buộc phải nhập tên',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                //  Tùy chỉnh dữ liệu insert


                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }


}
