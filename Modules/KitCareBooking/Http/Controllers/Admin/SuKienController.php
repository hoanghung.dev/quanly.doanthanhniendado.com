<?php

namespace Modules\KitCareBooking\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;

use Modules\KitCareBooking\Models\Product;
use Modules\KitCareBooking\Models\TheLoaiSuKien;
use Validator;

class SuKienController extends CURDBaseController
{
    protected $module = [
        'code' => 'su_kien',
        'table_name' => 'su_kien',
        'label' => 'Sự kiện',
        'modal' => '\Modules\KitCareBooking\Models\SuKien',
        'list' => [
//            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'width' => '60px'],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên phong trào', 'sort' => true],
            ['name' => 'start_date', 'type' => 'date_vi', 'label' => 'Ngày bắt đầu', 'sort' => true],
            ['name' => 'name', 'type' => 'custom', 'td' => 'kitcarebooking::su_kien.list.td.so_phong_ban', 'label' => 'Đoàn tham gia', 'sort' => true],
            ['name' => 'name', 'type' => 'custom', 'td' => 'kitcarebooking::su_kien.list.td.so_thanh_vien', 'label' => 'Số thành viên', 'sort' => true],
            ['name' => 'the_loai_su_kien_id', 'type' => 'relation_filter', 'label' => 'Phong trào', 'object' => 'the_loai_su_kien', 'display_field' => 'name'],
            ['name' => 'status', 'type' => 'custom', 'td' => 'kitcarebooking::su_kien.list.td.status', 'label' => 'Trạng thái', 'sort' => true],
            ['name' => 'admin_id', 'type' => 'relation_filter', 'label' => 'Người tạo', 'object' => 'admin', 'display_field' => 'name'],
//            ['name' => 'admin_id', 'type' => 'relation', 'object' => 'admin', 'display_field' => 'name', 'label' => 'Người tạo',],
//            ['name' => 'count_booking', 'type' => 'custom', 'td' => 'kitcarebooking::list.td.su_kien_count_booking', 'label' => 'Yêu cầu',],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên sự kiện', 'group_class' => 'col-md-6'],
                ['name' => 'start_date', 'type' => 'date', 'class' => '', 'label' => 'Ngày bắt đầu', 'group_class' => 'col-md-6'],
                ['name' => 'the_loai_su_kien_id', 'type' => 'select2_model', 'label' => 'Thể loại', 'model' => TheLoaiSuKien::class, 'object' => 'the_loai_su_kien', 'display_field' => 'name', 'group_class' => 'col-md-6'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'Đã thực hiện', 'value' => 1, 'group_class' => 'col-md-6'],
                ['name' => 'content', 'type' => 'textarea', 'class' => '', 'label' => 'Mô tả', 'inner' => 'rows=8'],
//                ['name' => 'iframe', 'type' => 'iframe', 'class' => 'col-xs-12 col-md-12 padding-left'
//                    , 'src' => '/admin/bill_receipts?bill_id={id}', 'inner' => 'style="min-height: 550px;"']
            ],

            'image_tab' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Ảnh'],
            ],
        ],
    ];

    protected $filter = [
        'the_loai_su_kien_id' => [
            'label' => 'Thể loại',
            'type' => 'select2_model',
            'display_field' => 'name',
            'object' => 'the_loai_su_kien',
            'model' => TheLoaiSuKien::class,
            'query_type' => '='
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, Tên',
        'fields' => 'id, name'
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('kitcarebooking::su_kien.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
//        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
//        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
//            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
//        }

        if (!is_null($request->get('thanh_vien_tham_gia_id'))) {
            $ids = \Modules\KitCareBooking\Models\SuKienThanhVien::
            where('admin_id', $request->thanh_vien_tham_gia_id)->pluck('su_kien_id')->toArray();

            $query = $query->whereIn('id', $ids);
        }

        if (!is_null($request->get('phong_ban_id'))) {
            $thanh_vien_ids = \App\Models\Admin::where('phong_ban_id', $request->phong_ban_id)->pluck('id')->toArray();
            $ids = \Modules\KitCareBooking\Models\SuKienThanhVien::whereIn('admin_id', $thanh_vien_ids)->groupBy('su_kien_id')->pluck('su_kien_id')->toArray();

            $query = $query->whereIn('id', $ids);
        }


        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('kitcarebooking::su_kien.add')->with($data);
            } else if ($_POST) {
//                dd($request->all());
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    $data['admin_id'] = \Auth::guard('admin')->id();

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    } elseif ($request->return_direct == 'save_editor') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . '/editor');
                    }

                    return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);

        //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('kitcarebooking::su_kien.edit')->with($data);
        } else if ($_POST) {
//            dd($request->all());
            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ], [
                'name.required' => 'Bắt buộc phải nhập tên',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                //  Tùy chỉnh dữ liệu insert


                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }


}
