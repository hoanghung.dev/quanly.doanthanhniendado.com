<?php

namespace Modules\KitCareBooking\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use App\Models\Admin;
use Auth;
use Illuminate\Http\Request;
use Modules\KitCareBooking\Models\PhongBan;
use Modules\KitCareBooking\Models\Product;
use Modules\KitCareBooking\Models\SuKienThanhVien;
use Validator;

class SuKienThanhVienController extends CURDBaseController
{

    protected $module = [
        'code' => 'sukien_thanhvien',
        'table_name' => 'su_kien_thanh_vien_tham_gia',
        'label' => 'Thành viên tham gia',
        'modal' => '\Modules\KitCareBooking\Models\SuKienThanhVien',
        'list' => [
            ['name' => 'admin_id', 'type' => 'relation_edit', 'label' => 'Tên thành viên', 'object' => 'admin', 'display_field' => 'name'],
            ['name' => 'phong_ban', 'type' => 'custom', 'td' => 'kitcarebooking::list.td.thanhvien_phongban', 'label' => 'Phòng ban', 'object' => ''],
            ['name' => 'diem', 'type' => 'text', 'label' => 'Số điểm'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'admin_id', 'type' => 'select2_ajax_model', 'class' => '', 'label' => 'Thành viên', 'model' => Admin::class, 'object' => 'admin', 'display_field' => 'name', 'display_field2' => 'tel', 'group_class' => 'col-sm-6'],
//                ['name' => 'admin_id', 'type' => 'custom', 'field' => 'kitcarebooking::sukien_thanhvien.form.select_admin', 'class' => '', 'label' => 'Thành viên', 'model' => Admin::class, 'object' => 'admin', 'display_field' => 'name', 'group_class' => 'col-sm-6'],
                ['name' => 'diem', 'type' => 'number', 'class' => '', 'label' => 'Số điểm', 'group_class' => 'col-sm-3'],

                ['name' => 'admin_id2', 'type' => 'select2_ajax_model', 'class' => '', 'label' => 'Thành viên', 'model' => Admin::class, 'object' => 'admin', 'display_field' => 'name', 'display_field2' => 'tel', 'group_class' => 'col-sm-6'],
//                ['name' => 'admin_id', 'type' => 'custom', 'field' => 'kitcarebooking::sukien_thanhvien.form.select_admin', 'class' => '', 'label' => 'Thành viên', 'model' => Admin::class, 'object' => 'admin', 'display_field' => 'name', 'group_class' => 'col-sm-6'],
                ['name' => 'diem2', 'type' => 'number', 'class' => '', 'label' => 'Số điểm', 'group_class' => 'col-sm-3'],

                ['name' => 'admin_id3', 'type' => 'select2_ajax_model', 'class' => '', 'label' => 'Thành viên', 'model' => Admin::class, 'object' => 'admin', 'display_field' => 'name', 'display_field2' => 'tel', 'group_class' => 'col-sm-6'],
//                ['name' => 'admin_id', 'type' => 'custom', 'field' => 'kitcarebooking::sukien_thanhvien.form.select_admin', 'class' => '', 'label' => 'Thành viên', 'model' => Admin::class, 'object' => 'admin', 'display_field' => 'name', 'group_class' => 'col-sm-6'],
                ['name' => 'diem3', 'type' => 'number', 'class' => '', 'label' => 'Số điểm', 'group_class' => 'col-sm-3'],

                ['name' => 'admin_id4', 'type' => 'select2_ajax_model', 'class' => '', 'label' => 'Thành viên', 'model' => Admin::class, 'object' => 'admin', 'display_field' => 'name', 'display_field2' => 'tel', 'group_class' => 'col-sm-6'],
//                ['name' => 'admin_id', 'type' => 'custom', 'field' => 'kitcarebooking::sukien_thanhvien.form.select_admin', 'class' => '', 'label' => 'Thành viên', 'model' => Admin::class, 'object' => 'admin', 'display_field' => 'name', 'group_class' => 'col-sm-6'],
                ['name' => 'diem4', 'type' => 'number', 'class' => '', 'label' => 'Số điểm', 'group_class' => 'col-sm-3'],
            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, điểm',
        'fields' => 'id, diem'
    ];

    protected $filter = [
        'admin_id' => [
            'label' => 'Thành viên',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'admin',
            'model' => Admin::class,
            'query_type' => '-',
        ],
    ];

    public function getIndex(Request $request)
    {

        $data = $this->getDataList($request);

        return view('kitcarebooking::sukien_thanhvien.list')->with($data);
    }

    public function appendWhere($query, $request)
    {

        if (@$request->su_kien_id != null) {
            $query = $query->where('su_kien_id', $request->su_kien_id);
        }

        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('kitcarebooking::sukien_thanhvien.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
//                    'value' => 'required'
                ], [
//                    'value.required' => 'Bắt buộc phải nhập giá trị',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    \DB::beginTransaction();

                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    $data['su_kien_id'] = $request->su_kien_id;

//                    $data['admin_id'] = $data['saler_id'] = \Auth::guard('admin')->user()->id;

                    // if ($request->file('image') != null) {
                    //     $data['image'] = CommonHelper::saveFile($request->file('image'), $this->module['code']);

                    // } else {
                    //     unset($data['image']);
                    // }

                    #

                    if ($request->admin_id != null) {
                        $item = new $this->model;
                        $item->admin_id = $request->admin_id;
                        $item->diem = $request->diem;
                        $item->su_kien_id = $request->su_kien_id;
                        $item->save();
                    }

                    if ($request->admin_id2 != null) {
                        $item = new $this->model;
                        $item->admin_id = $request->admin_id2;
                        $item->diem = $request->diem2;
                        $item->su_kien_id = $request->su_kien_id;
                        $item->save();
                    }

                    if ($request->admin_id3 != null) {
                        $item = new $this->model;
                        $item->admin_id = $request->admin_id3;
                        $item->diem = $request->diem3;
                        $item->su_kien_id = $request->su_kien_id;
                        $item->save();
                    }

                    if ($request->admin_id4 != null) {
                        $item = new $this->model;
                        $item->admin_id = $request->admin_id4;
                        $item->diem = $request->diem4;
                        $item->su_kien_id = $request->su_kien_id;
                        $item->save();
                    }



                    $this->updateDiem($request->su_kien_id);

                    \DB::commit();
                    CommonHelper::flushCache();
                    CommonHelper::one_time_message('success', 'Tạo mới thành công!');

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->has('su_kien_id')) {
                        return redirect('admin/' . $this->module['code'] . '?su_kien_id=' . $request->su_kien_id);
                    } else {
                        return redirect('admin/' . $this->module['code']);
                    }
                }
            }
        } catch (\Exception $ex) {
            \DB::rollback();
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function updateDiem($su_kien_id) {
        $thanh_vien_ids = SuKienThanhVien::where('su_kien_id', $su_kien_id)->pluck('admin_id')->toArray();
        $phong_ban_ids = [];
        foreach ($thanh_vien_ids as $admin_id) {
            $tong_diem = SuKienThanhVien::where('admin_id', $admin_id)->sum('diem');
            $admin = Admin::where('id', $admin_id)->first();
            if (is_object($admin)) {
                $admin->tong_diem = $tong_diem;
                $admin->save();
                $phong_ban_ids[] = $admin->phong_ban_id;
            }
        }

        foreach ($phong_ban_ids as $phong_ban_id) {
            $admin_ids = Admin::where('phong_ban_id', $phong_ban_id)->pluck('id')->toArray();
            $tong_diem = SuKienThanhVien::whereIn('admin_id', $admin_ids)->sum('diem');
            PhongBan::where('id', $phong_ban_id)->update(['tong_diem' => $tong_diem]);
        }

        return true;
    }

    public function update(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('kitcarebooking::sukien_thanhvien.edit')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    // 'value' => 'required'
                ], [
                    // 'value.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    \DB::beginTransaction();

                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    #
                    unset($data['admin_id2']);
                    unset($data['diem2']);
                    unset($data['admin_id3']);
                    unset($data['diem3']);
                    unset($data['admin_id4']);
                    unset($data['diem4']);


                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {

                        $this->updateDiem($item->su_kien_id);

                        //  cập nhật tiền đã nhận vào HĐ
//                    $this->updateTienDaTraHD($item->su_kien_id);

                        \DB::commit();
                        CommonHelper::flushCache();
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        \DB::rollback();
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->has('su_kien_id')) {
                        return redirect('admin/' . $this->module['code'] . '?su_kien_id=' . $request->su_kien_id);
                    } else {
                        return redirect('admin/' . $this->module['code']);
                    }
                }
            }
        } catch (\Exception $ex) {
            \DB::rollback();
            dd($ex->getMessage());
        }

    }

    public function getPublish(Request $request)
    {
        // dd($request->all());
        try {

            \DB::beginTransaction();
            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0) {
                //  duyệt phiếu thu
                $item->{$request->column} = 1;
            } else {
                //  huỷ duyệt phiếu thu
                $item->{$request->column} = 0;
            }

            $item->save();

            //  cập nhật tiền đã nhận vào HĐ
//            $this->updateTienDaTraHD($item->su_kien_id);

            \DB::commit();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            \DB::rollback();
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => $ex->getMessage()
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);
            $su_kien_id = $item->su_kien_id;

            $item->delete();

            $this->updateDiem($su_kien_id);

            //  cập nhật lại tiền đã nhận cho hợp đồng
            if ($su_kien_id != null) {
                //  nếu là giao dịch của HĐ thì cập nhật tiền đã nhận của HĐ
//                $this->updateTienDaTraHD($su_kien_id);
            }

            CommonHelper::one_time_message('success', 'Xóa thành công!');

            if ($request->has('su_kien_id')) {
                return redirect('admin/' . $this->module['code'] . '?su_kien_id=' . $request->su_kien_id);
            } else {
                return redirect('admin/' . $this->module['code']);
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

}
