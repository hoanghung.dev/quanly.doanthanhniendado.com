<?php

namespace Modules\KitCareBooking\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\A4iOrganization\Models\Organization;
use Illuminate\Http\Request;
use Modules\A4iLocation\Models\Province;
use Modules\A4iLocation\Models\Ward;
use Modules\KitCareBooking\Models\TagBooking;
use Validator;

class TagController extends Controller
{

    protected $module = [
        'code' => 'tag',
        'table_name' => 'tags',
        'label' => 'Tags',
        'modal' => 'Modules\KitCareBooking\Models\TagBooking',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
    ];

    public function index(Request $request)
    {
        try {
            //  Filter
            $where = $this->filterSimple($request);
            $listItem = TagBooking::whereRaw($where);

            //  Sort
            $listItem = $this->sort($request, $listItem);
            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());
            

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
