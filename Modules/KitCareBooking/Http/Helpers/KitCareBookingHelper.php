<?php
namespace Modules\KitCareBooking\Http\Helpers;

use Modules\KitCareNotification\Http\Controllers\KitCareNotificationController;

class KitCareBookingHelper
{

    public static function pushNotiFication($booking, $msg_des, $except_admin_ids)
    {
        try {
            $notifiController = new KitCareNotificationController();

            //  Truy van ra super admin
            $admin_ids = \DB::table('admin')
                ->join('role_admin', 'role_admin.admin_id', '=', 'admin.id')
                ->join('roles', 'role_admin.role_id', '=', 'roles.id')
                ->where('roles.name', 'super_admin')
                ->whereNotIn('admin.id', $except_admin_ids)
                ->pluck('admin.id')->toArray();
            foreach ($admin_ids as $id) {
                $notifiController->sendMessage([
                    'content' => 'Đơn hàng #' . $booking->id . ': ' . $msg_des,
                    'user_id' => $id,
                    'item_id' => $booking->id
                ]);
            }


            //  Gửi thông báo cho KTV
            foreach (explode('|', $booking->ktv_ids) as $id) {
                if ($id != '' && !in_array($id, $except_admin_ids)) {
                    $notifiController->sendMessage([
                        'content' => 'Đơn hàng #' . $booking->id . ': ' . $msg_des,
                        'user_id' => $id,
                        'item_id' => $booking->id
                    ]);
                }
            }

            //  Truy van ra khach hang
            if (!in_array($booking->admin_id, $except_admin_ids)) {
                $notifiController->sendMessage([
                    'content' => 'Đơn hàng #' . $booking->id . ': ' . $msg_des,
                    'user_id' => $booking->admin_id,
                    'item_id' => $booking->id
                ]);
            }

            return [
                'status' => true,
                'msg' => 'Thành công!'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => false,
                'msg' => $ex->getMessage()
            ];
        }
    }
}