<?php

namespace Modules\KitCareBooking\Http\Middleware;

use App\Http\Helpers\CommonHelper;
use App\Models\Admin;
use App\Models\AdminLog;
use Closure;
use function GuzzleHttp\Promise\all;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $admin = Admin::select('id')->where('email', $request->email)->first();
        if (is_object($admin)) {
            if (in_array(CommonHelper::getRoleName($admin->id, 'name'), ['technicians', 'custommer'])) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền đăng nhập');
                return back();
            }
        }
        return $next($request);
    }
}
