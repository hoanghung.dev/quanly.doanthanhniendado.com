<?php

namespace Modules\KitCareBooking\Models;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{

    protected $table = 'bookings';
    public $timestamps = false;


    protected $fillable = [
        'product_id', 'producer_id', 'product_code', 'error_code_id', 'tel', 'province_id', 'district_id', 'image', 'image_present', 'image_extra', 'image_present2',
        'note', 'time', 'admin_id', 'payment_status', 'note_user', 'care', 'address', 'status', 'created_at', 'ktv_ids', 'job_id', 'cost', 'bonus_services', 'create_by'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

    public function createBy()
    {
        return $this->belongsTo(Admin::class, 'create_by', 'id');
    }

    public function ktv_ids()
    {
        return $this->hasMany(Admin::class, 'ktv_ids', 'id');
    }

    public function job()
    {
        return $this->belongsTo(Job::class, 'job_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }

    public function producer()
    {
        return $this->belongsTo(Producer::class, 'producer_id');
    }

    public function error_code()
    {
        return $this->hasMany(ErrorCode::class, 'error_code_id', 'id');
    }

    public function tag_booking()
    {
        return $this->hasMany(TagBooking::class, 'tag_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'booking_id');
    }


}
