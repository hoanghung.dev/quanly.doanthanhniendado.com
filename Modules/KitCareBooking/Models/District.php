<?php


namespace Modules\KitCareBooking\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
	protected $table = 'districts';
}
