<?php



namespace Modules\KitCareBooking\Models;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class ErrorCode extends Model
{
    protected $table = 'error_codes';
    public $timestamps = false;



    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function admin()
    {
        return $this->belongsTo(\App\Models\Admin::class, 'admin_id');
    }

}
