<?php

namespace Modules\KitCareBooking\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    protected $table = 'products';
    public $timestamps = false;





    public function admin()
    {
        return $this->belongsTo(\App\Models\Admin::class, 'admin_id');
    }


}