<?php



namespace Modules\KitCareBooking\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
	protected $table = 'provinces';
}
