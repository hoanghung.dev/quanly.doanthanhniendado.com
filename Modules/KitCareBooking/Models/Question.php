<?php



namespace Modules\KitCareBooking\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'cau_hoi';
//    public $timestamps = false;

    public function exam(){
        return $this->belongsTo(Exam::class, 'bai_thi_id');
    }
}
