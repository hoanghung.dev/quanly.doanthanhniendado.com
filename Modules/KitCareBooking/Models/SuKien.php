<?php



namespace Modules\KitCareBooking\Models;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class SuKien extends Model
{
    protected $table = 'su_kien';
//    public $timestamps = false;

    public function the_loai_su_kien()
    {
        return $this->belongsTo(TheLoaiSuKien::class, 'the_loai_su_kien_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
}
