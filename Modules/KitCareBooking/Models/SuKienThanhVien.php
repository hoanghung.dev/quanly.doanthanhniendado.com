<?php



namespace Modules\KitCareBooking\Models;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class SuKienThanhVien extends Model
{
    protected $table = 'su_kien_thanh_vien_tham_gia';
//    public $timestamps = false;

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
}
