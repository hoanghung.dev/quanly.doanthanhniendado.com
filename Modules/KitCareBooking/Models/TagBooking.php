<?php
namespace Modules\KitCareBooking\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class TagBooking extends Model
{

    protected $table = 'tags';
    public $timestamps = false;



    protected $fillable = [
        'name', 'slug', 'parent_id','status',
    ];



}
