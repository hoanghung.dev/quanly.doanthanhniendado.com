@if(in_array($item->status, [4, 5]))
    OK
@elseif(in_array($item->status, [7, 8]))
    Stop
@else
    @if($item->{$field['name']} > 0)
        <span style="color: green;">{{ $item->{$field['name']} }} ngày</span>
    @elseif($item->{$field['name']} === 0)
        <span style="color: red;">Hết hạn</span>
    @elseif($item->{$field['name']} < 0)
        <span style="color: red;">Quá {{ 0 - $item->{$field['name']} }} ngày</span>
    @endif
@endif