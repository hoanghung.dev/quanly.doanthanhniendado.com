<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Thông tin khách hàng
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <div class="kt-form">
        <div class="kt-portlet__body">
            <div class="row kt-section kt-section--first">
                <div class="col-md-6 kt-avatar kt-avatar--outline" id="kt_user_avatar_image">
                    <div class="kt-avatar__holder">
                        <img style="max-width:150px;padding-top:5px; cursor: pointer;"
                             src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$result->admin->image, 100, 100) }}"
                             class="file_image_thumb" title="CLick để phóng to ảnh">
                    </div>
                </div>
                <div class="col-md-6 kt-widget__body">
                    <div class="kt-widget__content">
                        <div class="kt-widget__info">
                            <span class="kt-widget__label">Họ & tên:</span>
                            <a href="/admin/profile/{{ @$result->admin->id }}" class="kt-widget__data">{{ @$result->admin->name }}</a>
                        </div>
                        {{--<div class="kt-widget__info">--}}
                            {{--<span class="kt-widget__label">SĐT:</span>--}}
                            {{--<span class="kt-widget__data">{{ @$result->admin->tel }}</span>--}}
                        {{--</div>--}}
                        <div class="kt-widget__info">
                            <span class="kt-widget__label">Email:</span>
                            <span class="kt-widget__data">{{ @$result->admin->email }}</span>
                        </div>
                        {{--<div class="kt-widget__info">--}}
                            {{--<span class="kt-widget__label">Địa chỉ:</span>--}}
                            {{--<span class="kt-widget__data">{{ @$result->admin->address }}</span>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end::Form-->
</div>