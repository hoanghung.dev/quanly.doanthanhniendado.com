<?php
    $thanh_vien_ids = \Modules\KitCareBooking\Models\SuKienThanhVien::
    where('su_kien_id', $item->id)->pluck('admin_id')->toArray();


    $so_phong_ban = \Modules\KitCareBooking\Models\PhongBan::leftJoin('admin', 'admin.phong_ban_id', '=', 'phong_ban.id')
                    ->whereIn('admin.id', $thanh_vien_ids)->groupBy('phong_ban.id')->pluck('phong_ban.id')->toArray();

    ?>
<a href="{{ url('/admin/phong_ban?search=true&su_kien=' . $item->id) }}" target="_blank"
   style="    font-size: 14px!important; ">{{ number_format(count($so_phong_ban), 0, '.', ',') }} chi đoàn</a>
