<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {

    //  booking
    Route::group(['prefix' => 'bookings', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\BookingController@index');
        Route::post('', 'Admin\BookingController@store');
        Route::get('{id}', 'Admin\BookingController@show');
        Route::post('{id}', 'Admin\BookingController@update');
        Route::delete('{id}', 'Admin\BookingController@delete');

    });
    //  product
    Route::group(['prefix' => 'products', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\ProductController@index');
    });
    //  producers
    Route::group(['prefix' => 'producers', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\ProducerController@index');
    });
    //  errorcodes
    Route::group(['prefix' => 'error_codes', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\ErrorCodeController@index');
    });


    Route::group(['prefix' => 'comments', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\CommentController@index');
        Route::post('', 'Admin\CommentController@store');
        Route::get('{id}', 'Admin\CommentController@show');
        Route::post('{id}', 'Admin\CommentController@update');
        Route::delete('{id}', 'Admin\CommentController@delete');

    });


    Route::group(['prefix' => 'tags'], function () {
        Route::get('', 'Admin\TagController@index')->name('tag');
    });
});
