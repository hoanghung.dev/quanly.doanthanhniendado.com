<?php
Route::get('dang-nhap', function () {
    return redirect('/admin');
});
Route::get('/', function () {
    return redirect('/admin');
});
Route::get('push-remind', 'Admin\PushRemindController@pushRemind');
Route::group(['prefix' => 'admin', 'middleware' => ['no_auth:admin']], function () {
    Route::post('authenticate', '\App\Http\Controllers\Admin\AuthController@authenticate')->middleware(Modules\KitCareBooking\Http\Middleware\CheckRole::class)->name('admin.login');
});
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::get('dashboard', function () {
        return redirect('/admin/admin');
    });
//    Route::group(['prefix' => 'dashboard'], function () {
//        Route::get('', 'Admin\DashboardController@dashboard');
//    });

    Route::group(['prefix' => 'booking'], function () {
        Route::get('', 'Admin\BookingController@getIndex')->name('booking')->middleware('permission:booking_view');
        Route::get('publish', 'Admin\BookingController@getPublish')->name('booking.publish')->middleware('permission:booking_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\BookingController@add')->middleware('permission:booking_add');
        Route::get('delete/{id}', 'Admin\BookingController@delete')->middleware('permission:booking_delete');
        Route::post('multi-delete', 'Admin\BookingController@multiDelete')->middleware('permission:booking_delete');
        Route::get('search-for-select2', 'Admin\BookingController@searchForSelect2')->name('booking.search_for_select2')->middleware('permission:booking_view');

        Route::post('import-excel', 'Admin\BookingController@importExcel')->middleware('permission:booking_add');

        Route::get('{id}/print', 'Admin\BookingController@print')->middleware('permission:booking_view');
        Route::get('{id}', 'Admin\BookingController@update')->middleware('permission:booking_view');
        Route::post('{id}', 'Admin\BookingController@update')->middleware('permission:booking_edit');
    });
    Route::get('search-for-select3', 'Admin\AdminController@searchForSelect2')->name('admin.search_for_select3');


    Route::group(['prefix' => 'product'], function () {
        Route::get('', 'Admin\ProductController@getIndex')->name('product')->middleware('permission:product_view');
        Route::get('publish', 'Admin\ProductController@getPublish')->name('product.publish')->middleware('permission:product_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ProductController@add')->middleware('permission:product_add');
        Route::get('delete/{id}', 'Admin\ProductController@delete')->middleware('permission:product_delete');
        Route::post('multi-delete', 'Admin\ProductController@multiDelete')->middleware('permission:product_delete');
        Route::get('search-for-select2', 'Admin\ProductController@searchForSelect2')->name('product.search_for_select2')->middleware('permission:product_view');
        Route::get('{id}', 'Admin\ProductController@update')->middleware('permission:product_view');
        Route::post('{id}', 'Admin\ProductController@update')->middleware('permission:product_edit');
    });

    Route::group(['prefix' => 'producer'], function () {
        Route::get('', 'Admin\ProducerController@getIndex')->name('producer')->middleware('permission:producer_view');
        Route::get('publish', 'Admin\ProducerController@getPublish')->name('producer.publish')->middleware('permission:producer_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ProducerController@add')->middleware('permission:producer_add');
        Route::get('delete/{id}', 'Admin\ProducerController@delete')->middleware('permission:producer_delete');
        Route::post('multi-delete', 'Admin\ProducerController@multiDelete')->middleware('permission:producer_delete');
        Route::get('search-for-select2', 'Admin\ProducerController@searchForSelect2')->name('producer.search_for_select2')->middleware('permission:producer_view');
        Route::get('{id}', 'Admin\ProducerController@update')->middleware('permission:producer_view');
        Route::post('{id}', 'Admin\ProducerController@update')->middleware('permission:producer_edit');
    });

    Route::group(['prefix' => 'error_code'], function () {
        Route::get('', 'Admin\ErrorCodeController@getIndex')->name('error_code')->middleware('permission:error_code_view');
        Route::get('publish', 'Admin\ErrorCodeController@getPublish')->name('error_code.publish')->middleware('permission:error_code_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ErrorCodeController@add')->middleware('permission:error_code_add');
        Route::get('delete/{id}', 'Admin\ErrorCodeController@delete')->middleware('permission:error_code_delete');
        Route::post('multi-delete', 'Admin\ErrorCodeController@multiDelete')->middleware('permission:error_code_delete');
        Route::get('search-for-select2', 'Admin\ErrorCodeController@searchForSelect2')->name('error_code.search_for_select2')->middleware('permission:error_code_view');
        Route::get('{id}', 'Admin\ErrorCodeController@update')->middleware('permission:error_code_view');
        Route::post('{id}', 'Admin\ErrorCodeController@update')->middleware('permission:error_code_edit');
    });

    Route::group(['prefix' => 'comment'], function () {
        Route::get('', 'Admin\CommentController@getIndex')->name('comment')->middleware('permission:comment_view');
        Route::get('publish', 'Admin\CommentController@getPublish')->name('comment.publish')->middleware('permission:comment_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\CommentController@add')->middleware('permission:comment_add');
        Route::get('delete/{id}', 'Admin\CommentController@delete')->middleware('permission:comment_delete');
        Route::post('multi-delete', 'Admin\CommentController@multiDelete')->middleware('permission:comment_delete');
        Route::get('search-for-select2', 'Admin\CommentController@searchForSelect2')->name('comment.search_for_select2')->middleware('permission:comment_view');
        Route::get('{id}', 'Admin\CommentController@update')->middleware('permission:comment_view');
        Route::post('{id}', 'Admin\CommentController@update')->middleware('permission:comment_edit');
    });


    Route::group(['prefix' => 'job'], function () {
        Route::get('', 'Admin\JobController@getIndex')->name('job')->middleware('permission:job_view');
        Route::get('publish', 'Admin\JobController@getPublish')->name('job.publish')->middleware('permission:job_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\JobController@add')->middleware('permission:job_add');
        Route::get('delete/{id}', 'Admin\JobController@delete')->middleware('permission:job_delete');
        Route::post('multi-delete', 'Admin\JobController@multiDelete')->middleware('permission:job_delete');
        Route::get('search-for-select2', 'Admin\JobController@searchForSelect2')->name('job.search_for_select2')->middleware('permission:job_view');
        Route::get('{id}', 'Admin\JobController@update')->middleware('permission:job_view');
        Route::post('{id}', 'Admin\JobController@update')->middleware('permission:job_edit');
    });

    //  phòng ban
    Route::group(['prefix' => 'phong_ban'], function () {
        Route::get('', 'Admin\PhongBanController@getIndex')->name('phong_ban');
        Route::get('publish', 'Admin\PhongBanController@getPublish')->name('phong_ban.publish')->middleware('permission:super_admin');
        Route::match(array('GET', 'POST'), 'add', 'Admin\PhongBanController@add')->middleware('permission:super_admin');
        Route::get('delete/{id}', 'Admin\PhongBanController@delete')->middleware('permission:super_admin');
        Route::post('multi-delete', 'Admin\PhongBanController@multiDelete')->middleware('permission:super_admin');
        Route::get('search-for-select2', 'Admin\PhongBanController@searchForSelect2')->name('phong_ban.search_for_select2');
        Route::get('{id}', 'Admin\PhongBanController@update');
        Route::post('{id}', 'Admin\PhongBanController@update')->middleware('permission:super_admin');
    });

    //  sự kiện
    Route::group(['prefix' => 'su_kien'], function () {
        Route::get('', 'Admin\SuKienController@getIndex')->name('su_kien');
        Route::get('publish', 'Admin\SuKienController@getPublish')->name('su_kien.publish')->middleware('permission:super_admin');
        Route::match(array('GET', 'POST'), 'add', 'Admin\SuKienController@add')->middleware('permission:super_admin');
        Route::get('delete/{id}', 'Admin\SuKienController@delete')->middleware('permission:super_admin');
        Route::post('multi-delete', 'Admin\SuKienController@multiDelete')->middleware('permission:super_admin');
        Route::get('search-for-select2', 'Admin\SuKienController@searchForSelect2')->name('su_kien.search_for_select2');
        Route::get('{id}', 'Admin\SuKienController@update');
        Route::post('{id}', 'Admin\SuKienController@update')->middleware('permission:super_admin');
    });

    //  thành viên tham gia sự kiện
    Route::group(['prefix' => 'sukien_thanhvien'], function () {
        Route::get('', 'Admin\SuKienThanhVienController@getIndex')->name('sukien_thanhvien');
        Route::get('publish', 'Admin\SuKienThanhVienController@getPublish')->name('sukien_thanhvien.publish')->middleware('permission:super_admin');
        Route::match(array('GET', 'POST'), 'add', 'Admin\SuKienThanhVienController@add')->middleware('permission:super_admin');
        Route::get('delete/{id}', 'Admin\SuKienThanhVienController@delete')->middleware('permission:super_admin');
        Route::post('multi-delete', 'Admin\SuKienThanhVienController@multiDelete')->middleware('permission:super_admin');
        Route::get('search-for-select2', 'Admin\SuKienThanhVienController@searchForSelect2')->name('sukien_thanhvien.search_for_select2');
        Route::get('{id}', 'Admin\SuKienThanhVienController@update');
        Route::post('{id}', 'Admin\SuKienThanhVienController@update')->middleware('permission:super_admin');
    });

    //  Thể loại sự kiện
    Route::group(['prefix' => 'the_loai_su_kien'], function () {
        Route::get('', 'Admin\TheLoaiSuKienController@getIndex')->name('the_loai_su_kien');
        Route::get('publish', 'Admin\TheLoaiSuKienController@getPublish')->name('the_loai_su_kien.publish')->middleware('permission:super_admin');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TheLoaiSuKienController@add')->middleware('permission:super_admin');
        Route::get('delete/{id}', 'Admin\TheLoaiSuKienController@delete')->middleware('permission:super_admin');
        Route::post('multi-delete', 'Admin\TheLoaiSuKienController@multiDelete')->middleware('permission:super_admin');
        Route::get('search-for-select2', 'Admin\TheLoaiSuKienController@searchForSelect2')->name('the_loai_su_kien.search_for_select2');
        Route::get('{id}', 'Admin\TheLoaiSuKienController@update');
        Route::post('{id}', 'Admin\TheLoaiSuKienController@update')->middleware('permission:super_admin');
    });

    // đề thi
    Route::group(['prefix' => 'exam'], function () {
        Route::get('', 'Admin\ExamController@getIndex')->name('exam')->middleware('permission:job_view');
        Route::get('publish', 'Admin\ExamController@getPublish')->name('exam.publish')->middleware('permission:job_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ExamController@add')->middleware('permission:job_add');
        Route::get('delete/{id}', 'Admin\ExamController@delete')->middleware('permission:job_delete');
        Route::post('multi-delete', 'Admin\ExamController@multiDelete')->middleware('permission:job_delete');
        Route::get('search-for-select2', 'Admin\ExamController@searchForSelect2')->name('exam.search_for_select2')->middleware('permission:job_view');
        Route::get('{id}', 'Admin\ExamController@update')->middleware('permission:job_view');
        Route::post('{id}', 'Admin\ExamController@update')->middleware('permission:job_edit');
    });

    // câu hỏi
    Route::group(['prefix' => 'question'], function () {
        Route::get('', 'Admin\QuestionController@getIndex')->name('question')->middleware('permission:job_view');
        Route::get('publish', 'Admin\QuestionController@getPublish')->name('question.publish')->middleware('permission:job_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\QuestionController@add')->middleware('permission:job_add');
        Route::get('delete/{id}', 'Admin\QuestionController@delete')->middleware('permission:job_delete');
        Route::post('multi-delete', 'Admin\QuestionController@multiDelete')->middleware('permission:job_delete');
        Route::get('search-for-select2', 'Admin\QuestionController@searchForSelect2')->name('question.search_for_select2')->middleware('permission:job_view');
        Route::get('{id}', 'Admin\QuestionController@update')->middleware('permission:job_view');
        Route::post('{id}', 'Admin\QuestionController@update')->middleware('permission:job_edit');
    });


    Route::group(['prefix' => 'tag_booking'], function () {
        Route::get('', 'Admin\TagBookingController@getIndex')->name('tag_booking')->middleware('permission:tag_booking_view');
        Route::get('publish', 'Admin\TagBookingController@getPublish')->name('tag_booking.publish')->middleware('permission:tag_booking_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TagBookingController@add')->middleware('permission:tag_booking_add');
        Route::get('delete/{id}', 'Admin\TagBookingController@delete')->middleware('permission:tag_booking_delete');
        Route::post('multi-delete', 'Admin\TagBookingController@multiDelete')->middleware('permission:tag_booking_delete');
        Route::get('search-for-select2', 'Admin\TagBookingController@searchForSelect2')->name('tag_booking.search_for_select2')->middleware('permission:tag_booking_view');
        Route::get('{id}', 'Admin\TagBookingController@update')->middleware('permission:tag_booking_view');
        Route::post('{id}', 'Admin\TagBookingController@update')->middleware('permission:tag_booking_edit');
    });

    Route::group(['prefix' => 'location'], function () {
        Route::get('get-district-html-by-province_id', 'Admin\LocationController@getDistrictHtmlByProvinceId');

    });


    //  User
    Route::group(['prefix' => 'user'], function () {
        Route::get('', 'Admin\UserController@getIndex')->name('user')->middleware('permission:user_view');
        Route::match(array('GET', 'POST'), 'add', 'Admin\UserController@add')->middleware('permission:user_add');
        Route::get('delete/{id}', 'Admin\UserController@delete')->middleware('permission:user_delete');
        Route::post('multi-delete', 'Admin\UserController@multiDelete')->middleware('permission:user_delete');
        Route::get('{id}', 'Admin\UserController@update')->middleware('permission:user_view');
        Route::post('{id}', 'Admin\UserController@update')->middleware('permission:user_edit');
    });

    //  Admin
    Route::group(['prefix' => 'admin'], function () {
        Route::get('ajax/ajax-get-info', 'Admin\AdminController@ajaxGetInfo')->middleware('permission:admin_view');
        Route::get('', 'Admin\AdminController@getIndex')->name('admin')->middleware('permission:admin_view');

        Route::get( '{id}', '\App\Http\Controllers\Admin\AdminController@update')->middleware('permission:admin_view');
    });
});
