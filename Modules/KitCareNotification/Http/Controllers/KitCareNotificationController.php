<?php

namespace Modules\KitCareNotification\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\KitCareNotification\Models\Notifications;

class KitCareNotificationController extends Controller
{
    public function sendMessage($data)
    {
        $settings = Setting::whereIn('type', ['onesignal_tab'])->pluck('value', 'name')->toArray();

        $content = array(
            "en" => $data['content'],
        );
        $hashes_array = array();

        array_push($hashes_array, array(
            "id" => "read-more-button",
            "text" => "Read more",
            "icon" => "http://i.imgur.com/N8SN8ZS.png",
            "url" => '',
        ));

        $fields = array(
            'app_id' => $settings['onesignal_app_id'],
            'filters' => [
                [
                    "field" => "tag",
                    "key" => "user_id",
                    "relation" => "=",
                    "value" => $data['user_id']
                ],
            ],
            'contents' => $content,
            'web_buttons' => $hashes_array,
            'data' => (object) [
                'item_id' => @$data['item_id']
            ]
        );
        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . $settings['onesignal_rest_api_key']
        ));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        $this->insertToNotificationTable($data);
        return $response;
    }

    public function insertToNotificationTable($data) {
        try {
            Notifications::create([
                'name' => $data['content'],
                'content' => $data['content'],
                'to_admin_id' => $data['user_id'],
                'item_id' => $data['item_id']
            ]);
        } catch (\Exception $ex) {
            dd($ex->getMessage());
        }
    }
}
