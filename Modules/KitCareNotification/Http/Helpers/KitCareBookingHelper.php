<?php
namespace Modules\KitCareBooking\Http\Helpers;

use App\Models\Admin;
use App\Models\RoleAdmin;
use Modules\KitCareNotification\Http\Controllers\KitCareNotificationController;

class KitCareBookingHelper
{

    public static function pushNotiFication($booking, $msg_des)
    {
        try {
            $notifiController = new KitCareNotificationController();

            //  Truy van ra super admin
            $admin_ids = \DB::table('admin')
                ->join('role_admin', 'role_admin.admin_id', '=', 'admin.id')
                ->join('roles', 'role_admin.role_id', '=', 'roles.id')
                ->where('roles.name', 'supper_admin')
                ->pluck('admin.id')->toArray();

            foreach ($admin_ids as $id) {
                $notifiController->sendMessage([
                    'content' => 'Đơn hàng #' . $booking->id . ': ' . $msg_des,
                    'user_id' => $id
                ]);
            }


            //  Gửi thông báo cho KTV
            foreach (explode('|', $booking->ktv_ids) as $id) {
                if ($id != '') {
                    $notifiController->sendMessage([
                        'content' => 'Đơn hàng #' . $booking->id . ': ' . $msg_des,
                        'user_id' => $id
                    ]);
                }
            }

            //  Truy van ra khach hang
            $notifiController->sendMessage([
                'content' => 'Đơn hàng #' . $booking->id . ': ' . $msg_des,
                'user_id' => $booking->admin_id
            ]);


            return [
                'status' => true,
                'msg' => 'Thành công!'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => false,
                'msg' => $ex->getMessage()
            ];
        }
    }
}