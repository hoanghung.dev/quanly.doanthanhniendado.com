<?php

use Illuminate\Http\Request;


Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {

    //  notification
    Route::group(['prefix' => 'notifications', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\NotificationController@index');
        Route::get('/count-no-readed', 'Admin\NotificationController@count_no_readed');
        Route::get('/read-all', 'Admin\NotificationController@readAll');
        Route::get('{id}/read', 'Admin\NotificationController@read');

        Route::get('test-onesignal', 'Admin\NotificationController@testOneSignal');
    });
});
