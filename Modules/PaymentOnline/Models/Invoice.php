<?php

namespace Modules\PaymentOnline\Models;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use Modules\ThemeHandymanServices\Models\Booking;
use Modules\ThemeSemicolonwebJdes\Models\Company;

class Invoice extends Model
{

    protected $table = 'invoice';

    protected $guarded = [];

    public function booking()
    {
        return $this->belongsTo(Booking::class, 'booking_id');
    }
}
