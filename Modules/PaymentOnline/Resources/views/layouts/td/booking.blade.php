
@if (@$item->booking->service_fields != null)
    <?php
    $service_fields = (array)json_decode($item->booking->service_fields);
    $number = 0;
    ?>
    @foreach ($service_fields as $service_field_id => $value)
        <?php
        $number++;
        $field_name = \Modules\HandymanServicesBooking\Models\ServiceFields::find($service_field_id)->{'name_' . $language};
        $service_controller = new \Modules\ThemeHandymanServices\Http\Controllers\Frontend\ServiceController();
        ?>
        @if($number != 1)| @endif<strong>{{$field_name}} : </strong> {{$value}}
    @endforeach
@endif