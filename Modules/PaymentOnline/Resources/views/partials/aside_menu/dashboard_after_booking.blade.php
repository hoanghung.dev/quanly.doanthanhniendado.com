@if(in_array('view_all_data', $permissions))
    <li class="kt-menu__item" aria-haspopup="true"><a href="/admin/invoice" class="kt-menu__link "><span class="kt-menu__link-icon"><i class="flaticon-price-tag"></i>
            </span><span class="kt-menu__link-text">{{trans('paymentonline::admin.pay_history')}}</span></a></li>
@endif