<script src="https://www.paypal.com/sdk/js?client-id=Af0s1SBavtXyqmsq6knE58N5XhY7C4lUJvJ4qnFZIfKMAyPAfAP3BJ_ereZSR9zcWRNrHjxvqi2BrR1J"></script>
<script>
    paypal.Buttons({
        createOrder: function (data, actions) {
            // This function sets up the details of the transaction, including the amount and line item details.
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        value: '{{$total_price}}'
                    }
                }]
            });
        },
        onApprove: function (data, actions) {
            // This function captures the funds from the transaction.
            return actions.order.capture().then(function (details) {
                // This function shows a transaction success message to your buyer.
                alert('Transaction completed by ' + details.payer.name.given_name);
                console.log(details);
                $.ajax({
                    url: '/admin/paymentonline/bill/create',
                    type: 'POST',
                    data: details,
                    success: function (resp) {

                    },
                    error: function () {
                        alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
                    }
                });
            });
        }
    }).render('#middle .child-middle');
</script>