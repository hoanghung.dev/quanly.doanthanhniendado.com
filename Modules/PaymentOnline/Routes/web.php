<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'paymentonline'], function () {
    Route::group(['prefix' => 'bill'], function () {
        Route::post('create', 'PaymentOnlineController@createBillPayment');
        Route::get('paypal', 'PaymentOnlineController@getExpressCheckout')->name('paypal');

    });
});
Route::get('paypal/checkout-success', 'PaymentOnlineController@getExpressCheckoutSuccess')->name('checkout-success');
Route::get('thank-you', 'PaymentOnlineController@thankYou')->name('thank-you');


Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions','locale']], function () {
    Route::group(['prefix' => 'invoice'], function () {
        Route::get('', 'Admin\InvoiceController@getIndex')->middleware('permission:super_admin');
    });
});


