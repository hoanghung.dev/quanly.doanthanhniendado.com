<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions','locale']], function () {
    Route::group(['prefix' => 'theme'], function () {
        Route::get('', 'Admin\ThemeController@getIndex')->name('theme')->middleware('permission:theme');
        Route::get('active', 'Admin\ThemeController@active')->name('theme.active')->middleware('permission:theme');
    });

    Route::group(['prefix' => 'plugin'], function () {
        Route::get('', 'Admin\PluginController@getIndex')->name('plugin')->middleware('permission:plugin');
        Route::get('active', 'Admin\PluginController@active')->name('plugin.active')->middleware('permission:plugin');

    });

    //  Admin
    Route::group(['prefix' => 'admin'], function () {
        Route::get('login-other-account', '\App\Http\Controllers\Admin\AdminController@loginOtherAccount')->middleware('permission:super_admin');

        Route::get('', '\App\Http\Controllers\Admin\AdminController@getIndex')->name('admin')->middleware('permission:admin_view');
        Route::get('publish', '\App\Http\Controllers\Admin\AdminController@getPublish')->name('admin.admin_publish')->middleware('permission:admin_edit');
        Route::match(array('GET', 'POST'), 'add', '\App\Http\Controllers\Admin\AdminController@add')->middleware('permission:admin_add');
        Route::get('delete/{id}', '\App\Http\Controllers\Admin\AdminController@delete')->middleware('permission:admin_delete');
        Route::post('multi-delete', '\App\Http\Controllers\Admin\AdminController@multiDelete')->middleware('permission:admin_delete');

        Route::get('search-for-select2', '\App\Http\Controllers\Admin\AdminController@searchForSelect2')->name('admin.search_for_select2')->middleware('permission:admin_view');
        Route::get('search-for-select2-all', '\App\Http\Controllers\Admin\AdminController@searchForSelect2All')->middleware('permission:admin_view');

        Route::post('import-excel', '\App\Http\Controllers\Admin\AdminController@importExcel')->middleware('permission:admin_add');

        Route::get( '{id}', '\App\Http\Controllers\Admin\AdminController@update')->middleware('permission:admin_view');
        Route::post( '{id}', '\App\Http\Controllers\Admin\AdminController@update')->middleware('permission:admin_edit');
    });

    //  Role
    Route::group(['prefix' => 'role'], function () {
        Route::get('', '\App\Http\Controllers\Admin\RoleController@getIndex')->name('role')->middleware('permission:role_view');
        Route::match(array('GET', 'POST'), 'add', '\App\Http\Controllers\Admin\RoleController@add')->middleware('permission:role_add');
        Route::get('delete/{id}', '\App\Http\Controllers\Admin\RoleController@delete')->middleware('permission:role_delete');
        Route::post('multi-delete', '\App\Http\Controllers\Admin\RoleController@multiDelete')->middleware('permission:role_delete');
        Route::get('search-for-select2', '\App\Http\Controllers\Admin\RoleController@searchForSelect2')->name('role.search_for_select2')->middleware('permission:role_view');
        Route::match(array('GET', 'POST'), '{id}', '\App\Http\Controllers\Admin\RoleController@update')->middleware('permission:role_edit');
    });

    //  change_data_history
    Route::group(['prefix' => 'change_data_history'], function () {
        Route::get('', '\App\Http\Controllers\Admin\ChangeDataHistoryController@getIndex')->name('change_data_history')->middleware('permission:super_admin');
        Route::match(array('GET', 'POST'), 'add', '\App\Http\Controllers\Admin\ChangeDataHistoryController@add')->middleware('permission:super_admin');
        Route::get('delete/{id}', '\App\Http\Controllers\Admin\ChangeDataHistoryController@delete')->middleware('permission:super_admin');
        Route::post('multi-delete', '\App\Http\Controllers\Admin\ChangeDataHistoryController@multiDelete')->middleware('permission:super_admin');
    });

    Route::post('{module}/import-excel', '\App\Http\Controllers\Admin\ImportController@importExcel')->middleware('permission:super_admin');
});
