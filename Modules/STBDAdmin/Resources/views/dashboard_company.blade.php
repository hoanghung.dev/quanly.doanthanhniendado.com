
@extends(config('core.admin_theme').'.template')
@section('main')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-xs-12 col-md-12" style="margin-bottom: 20px;">
                <a href="#" class="btn btn-success">Doanh nghiệp</a>
                <a href="/admin/dashboard/software" class="btn btn-success" style="margin-left: 20px;">Hệ thống</a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-lg-12 col-xl-12 col-lg-12 order-lg-1 order-xl-1">
                <!--begin:: Widgets/Finance Summary-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Tổng quan
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-widget12">
                            <div class="kt-widget12__content">
                                <div class="kt-widget12__item">
                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">Tổng tiền đơn hàng</span>
                                        <span class="kt-widget12__value">{{number_format($total_price_bill, 0, '.', '.')}}<sup>đ</sup></span>
                                    </div>

                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">Tổng đơn hàng</span>
                                        <span class="kt-widget12__value">{{number_format(count($total_bill), 0, '.', '.')}}</span>
                                    </div>

                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">Tổng sản phẩm</span>
                                        <span class="kt-widget12__value">{{number_format($total_product, 0, '.', '.')}}</span>
                                    </div>

                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">Tổng đơn đang chờ xử lý</span>
                                        <span class="kt-widget12__value">{{number_format($total_bill_waitting, 0, '.', '.')}}</span>
                                    </div>
                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">Tổng đơn đang thực hiện</span>
                                        <span class="kt-widget12__value">{{number_format($total_bill_doing, 0, '.', '.')}}</span>
                                    </div>
                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">Tổng đơn hoàn thành</span>
                                        <span class="kt-widget12__value">{{number_format($total_bill_done, 0, '.', '.')}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Finance Summary-->
            </div>

            <div class="col-xl-12 order-lg-2 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Đơn hàng mới
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <!--begin: Datatable -->
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                             id="kt_datatable_latest_orders" style="">
                            <table class="kt-datatable__table" style=" width: 100%;">
                                <thead class="kt-datatable__head" style="    overflow: unset;">
                                <tr class="kt-datatable__row" style="left: 0px;">
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 25px;">STT</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Tên khách hàng</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Số điện thoại</span></th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 200px;">Email</span></th>

                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 50px;">Tổng sản phẩm</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Tổng tiền</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Ngày đặt</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Ngày nhận</span>
                                    </th>

                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 50px;">Chi tiết</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="kt-datatable__body ps ps--active-y"
                                       style="">
                                @if($bill_news->count()>0)
                                    @foreach($bill_news as $k=>$v)
                                        <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 25px;">
                                                    <span class="kt-font-bold">{{$k+1}}</span>
                                                </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <a href="/admin/bill/{{$v->id}}"><span class="kt-font-bold">{{$v->user_name}}</span></a>
                                                </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">{{$v->user_tel}}</span>
                                                </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 200px;">
                                                    <span class="kt-font-bold">{{$v->user_email}}</span>
                                                </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 50px;">
                                                    <span class="kt-font-bold">{{number_format(@\Modules\JdesBill\Models\Order::where('bill_id',$v->id)->get()->count(), 0, '.', '.')}}</span>
                                                </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">{{number_format($v->total_price, 0, '.', '.')}}</span>
                                                </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">{{date('d-m-Y',strtotime($v->created_at))}}</span>
                                                </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">{{date('d-m-Y',strtotime($v->updated_at))}}</span>
                                                </span>
                                            </td>
                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 50px;">
                                                        <a href="/admin/bill/{{$v->id}}" class="btn btn-sm btn-label-brand btn-bold">Xem</a>
                                                    </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable -->
                        <div class="paginate">{{$bill_news->appends(Request::all())->links()}}</div>

                    </div>
                </div>
            </div>

            {{-- Danh sách các sản phẩm mới đăng --}}
            <div class="col-xl-12 order-lg-2 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Danh sách các sản phẩm mới đăng
                            </h3>
                        </div>
                    </div>


                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <!--begin: Datatable -->
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                             id="kt_datatable_latest_orders" style="">
                            <table class="kt-datatable__table" style="display: block; ">
                                <thead class="kt-datatable__head">
                                <tr class="kt-datatable__row" style="left: 0px;">
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Ảnh</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 252px;">Tên sản phẩm</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Mã</span></th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Giá bán</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 350px;">Danh mục</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 50px;">Chi tiết</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="kt-datatable__body ps ps--active-y">
                                @if($product_news->count()>0)
                                    @foreach($product_news as $product_new)
                                        <tr data-row="0" class="kt-datatable__row" style="left: 0px;">

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 100px;">
                                                        <span class="kt-font-bold">
                                                            <img class="file_image_thumb" style="cursor: pointer; border-radius: 50%; width: 40px; max-width: 40px; height: 40px; max-height: 40px;"
                                                                 src="/public/filemanager/userfiles/{{$product_new->image}}"
                                                                 alt="{{$product_new->name}}" title="{{$product_new->name}}">
                                                        </span>
                                                    </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 252px;">
                                                        <span class="kt-font-bold"><a href="/admin/product/{{$product_new->id}}">{{$product_new->name}}</a></span>
                                                    </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 100px;">
                                                        <span class="kt-font-bold">{{$product_new->code}}</span>
                                                    </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 100px;">
                                                        <span class="kt-font-bold">{{isset($product_new->final_price) ? number_format($product_new->final_price, 0, '.', '.') : number_format($product_new->base_price, 0, '.', '.')}}<sup>đ</sup></span>
                                                    </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 350px;">
                                                        <?php
                                                            $multi_cats = explode('|',trim(@$product_new->multi_cat,'|'));
                                                        ?>
                                                        @if(!empty($multi_cats))
                                                            @foreach($multi_cats as $multi_cat)
                                                                @if(@\Modules\JdesProduct\Models\Category::find($multi_cat)->name!='')
                                                                    <a class="cate" href="/admin/category_product/{{$multi_cat}}">{{@\Modules\JdesProduct\Models\Category::find($multi_cat)->name}}</a>
                                                                @else
                                                                        <span></span>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 50px;">
                                                        <a href="/admin/product/{{$product_new->id}}" class="btn btn-sm btn-label-brand btn-bold">Xem</a>
                                                    </span>
                                            </td>

                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable -->
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
@section('custom_head')
{{--    <link href="https://www.keenthemes.com/preview/metronic/theme/assets/global/css/components.min.css" rel="stylesheet"--}}
{{--          type="text/css">--}}
    <style type="text/css">

        .paginate>ul.pagination>li{
            padding: 5px 10px;
            border: 1px solid #ccc;
            margin: 0 5px;
            cursor: pointer;        }

        .paginate>ul.pagination span{
            color: #000;
        }
        .paginate>ul.pagination>li.active{
             background: #0b57d5;
             color: #fff!important;
        }
        .paginate>ul.pagination>li.active span{
            color: #fff!important;
        }
        .kt-widget12__desc, .kt-widget12__value {
            text-align: center;
        }

        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99 list_user
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
    <style type="text/css">
        .kt-datatable__cell>span>a.cate {
            color: #5867dd;
            margin-bottom: 3px;
            background: rgba(88, 103, 221, 0.1);
            height: auto;
            display: inline-block;
            width: auto;
            padding: 0.15rem 0.75rem;
            border-radius: 2px;
        }
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>

@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $('#active_service a').click(function (event) {
                event.preventDefault();
                var object = $(this);
                $.ajax({
                    url: '/admin/service_history/ajax-publish',
                    data: {
                        id: object.data('service_history_id')
                    },
                    success: function (result) {
                        if (result.status == true) {
                            toastr.success(result.msg);
                            object.parents('tr').remove();
                        } else {
                            toastr.error(result.msg);
                        }
                    },
                    error: function (e) {
                        console.log(e.message);
                    }
                })
            })
        })
    </script>
@endpush

