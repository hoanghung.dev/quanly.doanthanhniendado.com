<lable>{{ @$field['label'] }}</lable>
<input type="text" name="{{ @base64_decode($field['name']) }}" class="form-control {{ @$field['class'] }}"
       id="{{ $field['name'] }}" {!! @$field['inner'] !!}
       value="{{ old($field['name']) != null ? old(base64_decode($field['name'])) : @base64_decode($field['value']) }}"
        {{ strpos(@$field['class'], 'require') !== false ? 'required' : '' }}
>
