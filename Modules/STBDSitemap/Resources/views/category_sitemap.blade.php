<?php echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    <?php $data = \Modules\STBDSitemap\Models\Category::select(['slug', 'updated_at'])->where('status', 1)->where('slug', '!=', '')->get();?>
    @foreach($data as $item)
        <url>
            <loc>{{ URL::to($item->slug) }}</loc>
            <lastmod>{{ date("Y-m-d", strtotime($item->updated_at))}}T{{ date("H:i:s", strtotime($item->updated_at))}}+07:00</lastmod>
            <changefreq>always</changefreq>
            <priority>0.4</priority>
        </url>
    @endforeach

</sitemapindex>

