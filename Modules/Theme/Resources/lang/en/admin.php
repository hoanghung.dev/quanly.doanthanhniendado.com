<?php

return [
    'theme' => 'Theme',
    'post' => 'Post',
    'all_post' => 'All post',
    'create_post' => 'Create post',
    'category' => 'Category',
    'tag' => 'Tag',
    'contact' => 'Contact',
    'banner' => 'Banner',
    'widget' => 'Widget',
    'menu' => 'Menu',
];
