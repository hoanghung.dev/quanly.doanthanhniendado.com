<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {

    //  Post
    Route::group(['prefix' => 'posts', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\PostController@index');
        Route::get('{id}', 'Admin\PostController@show');


    });


    //  Banner
    Route::group(['prefix' => 'banners', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\BannerController@index');

    });
});
