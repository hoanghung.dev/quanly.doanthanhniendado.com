<?php

namespace Modules\ThemeEdu\Http\Controllers\Frontend;
use App\Http\Helpers\CommonHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\EduSettings\Repositories\Student\StudentRepository;
use Modules\ThemeEdu\Http\Requests\CreateRequest;
use Modules\ThemeEdu\Http\Requests\ForgotPasswordRequest;
use Modules\ThemeEdu\Http\Requests\LoginRequest;
use Modules\ThemeEdu\Models\Student;
use Validator;
use Modules\ThemeEdu\Models\Admin;
use Modules\ThemeEdu\Models\Post;
use Modules\themeedu\Models\Setting;
use Session;
use Auth;
use Mail;


class AuthController extends Controller
{
//    function getLogin()
//    {
//        return view('themeedu::pages.auth.login');
//    }
//
//    function getRegister()
//    {
//        return view('themeedu::pages.auth.register');
//    }
//
//    public function getForgotPassword() {
//        return view('themeedu::pages.auth.forgot_password');
//    }
    public function getLogin()
    {
        $data['page_title'] = 'Đăng nhập';
        $data['page_type'] = 'list';
        return view('themeedu::pages.auth.login');
    }

    public function authenticate(LoginRequest $request)
    {
        $student = \Modules\ThemeEdu\Models\Student::where('email', $request['email'])->first();
        if (!is_object($student)) {
            CommonHelper::one_time_message('danger', 'Vui lòng kiểm tra lại Email/Mật khẩu của bạn');
            return redirect('/dang-nhap');
        }
        if (@$student->status == 0) {
            CommonHelper::one_time_message('danger', 'Tài khoản của bạn chưa được kích hoạt!');
            return redirect('/dang-nhap');
        }

        if (@$student->status == -1) {
            CommonHelper::one_time_message('danger', 'Tài khoản của bạn đã bị khóa!');
            return redirect('/dang-nhap');
        }

        if (\Illuminate\Support\Facades\Auth::guard('student')->attempt(['email' => trim($request['email']), 'password' => trim($request['password'])], true)) {
            return redirect()->intended('/');
        } else {
            CommonHelper::one_time_message('danger', 'Vui lòng kiểm tra lại Email/Mật khẩu của bạn');
            return redirect('/dang-nhap');
        }
    }

    public function getRegister()
    {
        $data['page_title'] = 'Đăng ký';
        $data['page_type'] = 'list';
        return view('themeedu::pages.auth.register', $data);
    }

    public function postRegister(CreateRequest $request, StudentRepository $repository)
    {
        $data = $request->except('_token');
        $data['api_token'] = base64_encode(rand(1, 100) . time());
        $repository->create($data, 'success', 'Bạn đã đăng ký tài khoản thành công! Vui lòng đăng nhập!');
        return redirect('/dang-nhap');
    }

    public function getForgotPassword(Request $request, $change_password)
    {
        if (!$_POST) {

            $query = Student::where('change_password', $change_password);

            if (!$query->exists() || !isset($change_password)) {
                abort(404);
            }
            $data['page_title'] = 'Lấy lại mật khẩu';
            $data['page_type'] = 'list';
            return view('themeedu::pages.auth.change_password')->with($data);
        } else {

            if ($request->password == $request->re_password) {
                $student = Student::where('change_password', $change_password)->first();
                $student->password = bcrypt($request->password);
                $student->change_password = $student->id . '_' . time();
                $student->save();
                CommonHelper::one_time_message('success', 'Đổi mật khẩu thành công! vui lòng đăng nhập!');
                return redirect('/dang-nhap');
            } else {
                return back()->with('alert_re_password', 'Nhập lại mật khâu không khớp!');
            }
        }
    }


    public function getEmailForgotPassword(Request $request)
    {
        if (!$_POST) {
            $data['page_title'] = 'Quên mật khẩu';

            return view('themeedu::pages.auth.forgot_password')->with($data);

        } else {
            $query = Student::where('email', $request->email);

            if (!$query->exists()) {
                return back()->with('success', 'Email chưa được đăng ký');
            }
            $student = $query->first();
            $student->change_password = $student->id . '_' . time();
            $student->save();
            try {
                \Eventy::action('admin.restorePassword', [
                    'link' => \URL::to('forgot-password/' . @$student->change_password),
                    'name' => @$student->name,
                    'email' => $student->email
                ]);
                //CommonHelper::one_time_message('success', 'Email sẽ được gửi trong ít phút. Bạn vui lòng kiểm tra mail để lấy lại mật khẩu!');
                return back()->with('success', 'Email sẽ được gửi trong ít phút. Bạn vui lòng kiểm tra mail để lấy lại mật khẩu!');
            } catch (\Exception $ex) {
                CommonHelper::one_time_message('error', 'Xin vui lòng thử lại!');
            }
        }
    }


}




