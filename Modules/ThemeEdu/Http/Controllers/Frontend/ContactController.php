<?php

namespace Modules\ThemeEdu\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use Modules\EduCourse\Models\LessonItem;
use Modules\EduCourse\Models\MaketingMail;
use Modules\EduCourse\Models\Topic;
use Modules\ThemeEdu\Http\Helpers\CommonHelper;

use Modules\ThemeEdu\Models\Contact;
use Modules\ThemeEdu\Models\Menus;
use Modules\ThemeEdu\Models\Route;
use Modules\ThemeEdu\Models\Student;


class ContactController extends Controller
{
    function registerCourseWhenLogin(request $r)
    {

        $contact = new contact;
        $contact->name = @\Auth::guard('student')->user()->name;
        $contact->email = @\Auth::guard('student')->user()->email;
        $contact->tel = @\Auth::guard('student')->user()->phone;
        $contact->course_id = @$r->course_id;
        $contact->student_id = @\Auth::guard('student')->user()->id;
        $contact->save();
    try {
        \Eventy::action('admin.deactiveFromCompany', [
            'email' => $contact->email,
            'name' => $contact->name,
        ]);
    } catch (\Exception $ex) {

    }
        return response()->json([
            'status' => true,
            'msg' => 'Đăng ký khóa học thành công! Chúng tôi sẽ sớm liên hệ lại với bạn'
        ]);

    }
    function postContact(request $r)
    {
            $contact = new contact;
            $contact->name = @$r->name;
            $contact->email = @$r->email;
            $contact->tel = @$r->phone;
            $contact->content = @$r->message;
            $contact->course_id = @$r->course_id;
            $contact->student_id = @$r->student_id;
            $contact->save();

        try {
            \Eventy::action('admin.deactiveFromCompany', [
                'email' => $contact->email,
                'name' => $contact->name,
            ]);
        } catch (\Exception $ex) {

        }
        return response()->json([
            'status' => true,
            'msg' => 'Đăng ký khóa học thành công! Chúng tôi sẽ sớm liên hệ lại với bạn'
        ]);
    }
    function route(request $r)
    {
        $route = new Route();
        $route->student_id = @\Auth::guard('student')->user()->id;
        $route->lesson_item_id = @$r->lesson_item_id;
        $route->accumulated_points = LessonItem::find($r->lesson_item_id)->accumulated_points;
        $route->save();

        return response()->json([
            'status' => true,
            'msg' => 'Chúc mừng bạn đã hoàn thành bài học ngày hôm nay'
        ]);

    }
    function topic(request $r)
    {
        $topic = new Route();
        $topic->student_id = @\Auth::guard('student')->user()->id;
        $topic->topic_id = @$r->topic_id;
        $topic->save();

        return response()->json([
            'status' => true,
            'msg' => 'Chúc mừng bạn đã hoàn thành bước học này'
        ]);

    }
}
