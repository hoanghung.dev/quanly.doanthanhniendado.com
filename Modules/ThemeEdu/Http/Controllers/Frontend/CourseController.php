<?php

namespace Modules\ThemeEdu\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use DB;
use Illuminate\Http\Request;
use Mail;
use Modules\EduCourse\Models\LessonItem;
use Modules\EduCourse\Models\Quizzes;
use Modules\ThemeEdu\Models\Category;
use Modules\ThemeEdu\Models\Course;
use Modules\ThemeEdu\Models\Post;
use Session;
use Validator;

class CourseController extends Controller
{
    public function list($slug)
    {
        $user = \Auth::guard('student')->user();


            $category = Category::where('slug', $slug)->first();


        if (!is_object($category)) {
            abort(404);
        }
        $data['category'] = $category;
        $data['slug1'] = $slug;

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $category->meta_title != '' ? $category->meta_title : $category->name,
            'meta_description' => $category->meta_description != '' ? $category->meta_description : $category->name,
            'meta_keywords' => $category->meta_keywords != '' ? $category->meta_keywords : $category->name,
        ];
        view()->share('pageOption', $pageOption);

        if (in_array($category->type, [1])) {         //  Tài liệu

            $category_id = CommonHelper::getFromCache('categorys_id_post_slug'.$slug);
            if (!$category_id) {
                $category_id = Category::where('slug', $slug)->first()->id;
                CommonHelper::putToCache('categorys_id_post_slug'.$slug, $category_id);
            }

            $data['posts'] = CommonHelper::getFromCache('posts_multi_category_id'.$category_id);
            if (!$data['posts']) {
                $data['posts'] = @Post::where('multi_cat', 'like', '%|' . @$category_id . '|%')
                    ->orderBy('order_no', 'desc')->orderBy('id', 'asc')->paginate(8);
                CommonHelper::putToCache('posts_multi_category_id'.$category_id, $data['posts']);
            }




            return view('themeedu::pages.document.document')->with($data)->with('user', $user);
        } elseif (in_array($category->type, [5])) {

            //  Khóa học

            $category_id = CommonHelper::getFromCache('categorys_id_course_slug'.$slug);
            if (!$category_id) {
                $category_id = Category::where('slug', $slug)->first()->id;
                CommonHelper::putToCache('categorys_id_course_slug'.$slug, $category_id);
            }
            $data['courses'] = CommonHelper::getFromCache('courses_multi_category_id'.$category_id);
            if (!$data['courses']) {
                $data['courses'] = @Course::where('multi_cat', 'like', '%|' . @$category_id . '|%')
                    ->orderBy('order_no', 'desc')->orderBy('id', 'asc')->paginate(8);
                CommonHelper::putToCache('courses_multi_category_id'.$category_id, $data['courses']);
            }

            return view('themeedu::pages.course.course')->with($data)->with('user', $user);
        } elseif (in_array($category->type, [4])) {
            //  Bai kiem tra
            $category_id = CommonHelper::getFromCache('categorys_id_quiz_slug' . $slug);
            if (!$category_id) {
                $category_id = Category::where('slug', $slug)->first()->id;
                CommonHelper::putToCache('categorys_id_quiz_slug' . $slug, $category_id);
            }

            $data['quizs'] = CommonHelper::getFromCache('quizs_multi_cat_category_id' . $category_id);
            if (!$data['quizs']) {
                $data['quizs'] = @Quizzes::where('multi_cat', 'like', '%|' . @$category_id . '|%')
                    ->orderBy('order_no', 'desc')->orderBy('id', 'asc')->paginate(8);
                CommonHelper::putToCache('quizs_multi_cat_category_id' . $category_id, $data['quizs']);
            }

            return view('themeedu::pages.quiz.quiz')->with($data)->with('user', $user);
        } elseif (in_array($category->type, [2])) {
            //  Lich hoc

            $category_id = CommonHelper::getFromCache('categorys_id_calendar_slug'.$slug);
            if (!$category_id) {
                $category_id = Category::where('slug', $slug)->first()->id;
                CommonHelper::putToCache('categorys_id_calendar_slug'.$slug, $category_id);
            }

            $data['posts'] = CommonHelper::getFromCache('calendars_multi_category_id'.$category_id);
            if (!$data['posts']) {
                $data['posts'] = @Post::where('multi_cat', 'like', '%|' . @$category_id . '|%')
                    ->orderBy('order_no', 'desc')->orderBy('id', 'asc')->paginate(8);
                CommonHelper::putToCache('calendars_multi_category_id'.$category_id, $data['posts']);
            }
            return view('themeedu::pages.calendar.calendar')->with($data)->with('user', $user);
        }
    }


    public function oneParam($slug1)
    {
        //  Chuyen den trang danh sanh tin tuc | Danh sach san pham
        return $this->list($slug1);
    }

    //  VD: hobasoft/card-visit  | danh-muc-cha/danh-muc-con | danh-muc/bai-viet.html | danh-muc/san-pham.html
    public function twoParam($slug1, $slug2)
    {

        if (strpos($slug2, '.html')) {  //  Nếu là trang chi tiết

            $slug2 = str_replace('.html', '', $slug2);
            if (Course::where('slug', $slug2)->where('status', 1)->count() > 0) {        //  Chi tiet khóa học

                return $this->detail($slug1, $slug2);
            } elseif (LessonItem::where('slug', $slug2)->count() > 0) {  //  Chi tiet
                return $this->lesson_detail($slug1, $slug2);
            } elseif (Quizzes::where('slug', $slug2)->count() > 0) {  //  Chi tiet bai kiem tra
                $quizController = new QuizController();
                return $quizController->detail($slug1, $slug2);
            } elseif (Post::where('slug', $slug2)->where('type', 1)->count() > 0) {  //  Chi tiet lich hoc
                $calendarController = new CalendarController();
                return $calendarController->detail($slug1, $slug2);
            } else {        //  Chi tiet tai lieu
                $postController = new PostController();
                return $postController->detail($slug1, $slug2);
            }
            abort(404);
        }
        return $this->list($slug2);
    }

    //  VD: hobasoft/card-visit/card-truyen-thong
    public function threeParam($slug1, $slug2, $slug3)
    {
        if (strpos($slug3, '.html')) {  //  Nếu là trang chi tiết
            $slug3 = str_replace('.html', '', $slug3);
            if (Course::where('slug', $slug3)->where('status', 1)->count() > 0) {        //  Chi tiet san pham
                return $this->detail($slug3);
            } else {        //  Chi tiet tin tuc
                $postController = new PostController();
                return $postController->detail($slug3);
            }
        }
        return $this->list($slug3);
    }

    public function fourParam($slug1, $slug2, $slug3, $slug4)
    {
        if (strpos($slug4, '.html')) {  //  Nếu là trang chi tiết
            $slug4 = str_replace('.html', '', $slug4);
            if (Course::where('slug', $slug4)->where('status', 1)->count() > 0) {        //  Chi tiet san pham
                return $this->detail($slug4);
            } else {        //  Chi tiet tin tuc
                $postController = new PostController();
                return $postController->detail($slug4);
            }
        }
        abort(404);
    }

    //  Trang chi tiết sản phẩm
    public function detail($slug1, $slug)
    {
        $slug = str_replace('.html', '', $slug);
        $data['course'] = CommonHelper::getFromCache('course_slug'.$slug);
        if (!$data['course']) {
            $data['course'] = Course::where('slug', $slug)->where('status', 1)->first();
            CommonHelper::putToCache('course_slug'.$slug, $data['course']);
        }

//        $data['slug1'] = $slug1;
        if (!is_object($data['course'])) {
            abort(404);
        }
        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['course']->meta_title != '' ? $data['course']->meta_title : $data['course']->name,
            'meta_description' => $data['course']->meta_description != '' ? $data['course']->meta_description : $data['course']->name,
            'meta_keywords' => $data['course']->meta_keywords != '' ? $data['course']->meta_keywords : $data['course']->name,
        ];
        view()->share('pageOption', $pageOption);
        return view('themeedu::pages.course.list_lesson', $data);
    }

    public function lesson_detail($slug1, $slug)
    {
//        dd($slug1);
        $slug = str_replace('.html', '', $slug);
        $data['lessonitem'] = CommonHelper::getFromCache('lesson_item_slug'.$slug);
        if (!$data['lessonitem']) {
            $data['lessonitem'] = LessonItem::where('slug', $slug)->first();

            CommonHelper::putToCache('lesson_item_slug'.$slug, $data['lessonitem']);
        }

        $data['slug1'] = $slug1;
        if (!is_object($data['lessonitem'])) {
            abort(404);
        }

        /*$lesson_prev = LessonItem::select('id', 'slug')->where('lesson_id', $data['lessonitem']->lesson_id)->where('id', '!=', $data['lessonitem']->id)->orderBy('order_no', 'DESC')->orderBy('id', 'ASC')->limit(2)->get()->toArray();
        dd($lesson_prev);
        if (is_object($lesson_prev)) {
            if (Route::where('lesson_item_id', $lesson_prev->id)->where('student_id', \Auth::guard('student')->user()->id)->count() == 0) {
                CommonHelper::one_time_message('error', 'Bạn phải hoàn thành bài học này mới được học bài kế tiếp');
                return redirect('/khoa-hoc/' . $lesson_prev->slug . '.html');
            }
        }*/

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['lessonitem']->meta_title != '' ? $data['lessonitem']->meta_title : $data['lessonitem']->name,
            'meta_description' => $data['lessonitem']->meta_description != '' ? $data['lessonitem']->meta_description : $data['lessonitem']->name,
            'meta_keywords' => $data['lessonitem']->meta_keywords != '' ? $data['lessonitem']->meta_keywords : $data['lessonitem']->name,
        ];
        view()->share('pageOption', $pageOption);

        return view('themeedu::pages.course.course_detail', $data);
    }

    function getSearch(request $r)
    {
        $data['courses'] = CommonHelper::getFromCache('courses_name_like'.$r->q);
        if (!$data['courses']) {
            $data['courses'] = Course::where('name', 'like', '%' . $r->q . '%')->where('status', 1)->where('status', 1)->orderBy('id', 'desc')->paginate(8);

            CommonHelper::putToCache('courses_name_like'.$r->q, $data['courses']);
        }
        $data['q'] = $r->q;
        $pageOption = [
            'meta_title' => 'Tìm kiếm từ khóa: ' . @$r->q,
            'meta_description' => 'Tìm kiếm từ khóa: ' . @$r->q,
            'meta_keywords' => 'Tìm kiếm từ khóa: ' . @$r->q,
        ];
        view()->share('pageOption', $pageOption);

        return view('themeedu::pages.course.search_course', $data);
    }

}
