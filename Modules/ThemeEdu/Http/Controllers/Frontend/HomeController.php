<?php

namespace Modules\ThemeEdu\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\HandymanServicesTheme\Models\Category;
use Modules\ThemeEdu\Models\Booking;
use Modules\ThemeEdu\Models\Post;
use Session;


class HomeController extends Controller
{
    function getHome()
    {

        $student = \Auth::guard('student')->user();
        return view('themeedu::pages.home.home')->with('student', $student);
    }

}
