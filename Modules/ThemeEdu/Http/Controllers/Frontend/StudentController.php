<?php

namespace Modules\ThemeEdu\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\ThemeEdu\Models\Contact;
use Modules\ThemeEdu\Models\QuizLog;
use Modules\ThemeEdu\Models\Route;
use Modules\ThemeEdu\Models\Student;

class StudentController extends Controller
{

    public function myProfile() {
        $data['user'] = \Auth::guard('student')->user();

        $data['count_course'] = CommonHelper::getFromCache('contacts_count_course_id');
        if (!$data['count_course']) {
            $data['count_course'] = Contact::where('student_id', \Auth::guard('student')->user()->id)->where('status', 1)->count('course_id');
            CommonHelper::putToCache('contacts_count_course_id', $data['count_course']);
        }

        $data['count_quizz'] = CommonHelper::getFromCache('quiz_logs_count_quizz_id');
        if (!$data['count_quizz']) {
            $data['count_quizz'] = QuizLog::where('student_id',\Auth::guard('student')->user()->id)->count('quizz_id');
            CommonHelper::putToCache('quiz_logs_count_quizz_id', $data['count_quizz']);
        }

        $data['accumulated_points'] = CommonHelper::getFromCache('quiz_logs_sum_accumulated_point');
        if (!$data['accumulated_points']) {
            $data['accumulated_points'] = QuizLog::where('student_id',\Auth::guard('student')->user()->id)->sum('accumulated_points');
            CommonHelper::putToCache('quiz_logs_sum_accumulated_point', $data['accumulated_points']);
        }

        $data['accumulated_point'] = CommonHelper::getFromCache('routes_sum_accumulated_points');
        if (!$data['accumulated_point']) {
            $data['accumulated_point'] = Route::where('student_id',\Auth::guard('student')->user()->id)->sum('accumulated_points');
            CommonHelper::putToCache('routes_sum_accumulated_points', $data['accumulated_point']);
        }

        $data['count_scores'] = CommonHelper::getFromCache('quiz_logs_count_scores');
        if (!$data['count_scores']) {
            $data['count_scores'] = QuizLog::where('student_id',\Auth::guard('student')->user()->id)->count('scores');
            CommonHelper::putToCache('quiz_logs_count_scores', $data['count_scores']);
        }

        $sum_scores = CommonHelper::getFromCache('quiz_logs_student_id');
        if (!$sum_scores) {
            $sum_scores = QuizLog::where('student_id',\Auth::guard('student')->user()->id)->get();
            CommonHelper::putToCache('quiz_logs_student_id', $sum_scores);
        }
        $score=0;
        foreach ($sum_scores as $v){
            $str = explode('/', $v->scores);
            if (isset($str[0]) && isset($str[1])) {
                $score += (int) $str[0] / (int) $str[1];
            }

        }

        //  Kiểm tra các điểm số chưa được update thì update
        $this->updateQuizLogOfStudent(\Auth::guard('student')->user()->id);

        return view('themeedu::pages.student.profile')->with($data)->with('score',$score);
    }

    public function getProfile($id) {
        if ($id == \Auth::guard('student')->user()->id) {
            return redirect('/profile');
        }
        $data['user'] = Student::find($id);

        //  Kiểm tra các điểm số chưa được update thì update
        $this->updateQuizLogOfStudent($id);

        return view('themeedu::pages.student.profile')->with($data);
    }

    public function updateQuizLogOfStudent($student_id) {
        $quizz_log_no_update = CommonHelper::getFromCache('quiz_logs_synch_0');
        if (!$quizz_log_no_update) {
            $quizz_log_no_update = QuizLog::where('student_id', $student_id)->where('synch', 0)->get();
            CommonHelper::putToCache('quiz_logs_synch_0', $quizz_log_no_update);
        }

        foreach ($quizz_log_no_update as $quizz_log) {
            $quizController = new QuizController();
            $quizController->updateScores($quizz_log);
        }
        return true;
    }

    public function getEditProfile() {
        $data['user'] = \Auth::guard('student')->user();
        return view('themeedu::pages.student.edit_profile')->with($data);
    }

    public function postEditProfile(Request $request)
    {

//        $data = $request->only('name','phone','email','address','image','banner');
//        $repository->update(Auth::guard('student')->user()->id,$data,'success','Đã sửa thành công');
        $profile = Student::find(\Auth::guard('student')->user()->id);
        $profile->name = $request->name;
        $profile->phone = $request->phone;
        $profile->email = $request->email;
        $profile->address = $request->address;
        $profile->image = CommonHelper::saveFile($request->image, 'student');
        $profile->banner = CommonHelper::saveFile($request->banner, 'student/banner/');
        $profile->save();
        return redirect()->back();
    }

    public function course() {
        $data['user'] = \Auth::guard('student')->user();
        $data['courses'] = CommonHelper::getFromCache('contacts_course_id_desc');
        if (!$data['courses']) {
            $data['courses'] = @Contact::where('student_id', $data['user']->id)->where('status', 1)
                ->orderBy('id', 'desc')->paginate(8);
            CommonHelper::putToCache('contacts_course_id_desc', $data['courses']);
        }


        return view('themeedu::pages.student.course',$data);
    }

//    public function quiz($course_id) {
//        $data['quizs'] = @Contact::where('course_id', $course_id)
//            ->orderBy('id', 'desc')->get();
//        return view('themeedu::pages.student.quiz',$data);
//    }
    public function quiz() {
        $data['user'] = \Auth::guard('student')->user();
        $data['quizs'] = CommonHelper::getFromCache('quiz_logs_student_id_desc');
        if (!$data['quizs']) {
            $data['quizs'] = QuizLog::where('student_id', $data['user']->id)
                ->orderBy('id', 'desc')->get();
            CommonHelper::putToCache('quiz_logs_student_id_desc', $data['quizs']);
        }

        return view('themeedu::pages.student.quiz',$data);
    }
}
