<?php

namespace Modules\ThemeEdu\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $table = 'categories';

    protected $guarded = [];

}
