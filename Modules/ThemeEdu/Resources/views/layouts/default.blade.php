<!DOCTYPE html>
<html lang="vi" prefix="og: http://ogp.me/ns#" >
      {{--class="loading-site no-js">--}}
<head>
    @include('themeedu::partials.head_meta')
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700|Roboto:300,400,500,600,700">
    @include('themeedu::partials.header_script')

</head>

<body class="stretched">

{{--@include('themesemicolonwebjdes::partials.header')--}}
@yield('main_content')
<!-- #wrapper -->
@include('themeedu::partials.footer')
@yield('custom_script')


</body>
</html>
