@extends('themeedu::layouts.default')
@section('main_content')
    <section>
        <div class="gap no-gap signin whitish medium-opacity">
            <div class="bg-image" style="background-image:url(/public/frontend/themes/edu/images/resources/theme-bg.jpg)"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="big-ad">
                            <figure><img src="{{ URL::asset('public/filemanager/userfiles/' . @$settings['logo']) }}" alt="{{ @$settings['name'] }}" style="max-width: 150px;"></figure>
                            <h1>Chào mừng bạn đến với {{ @$settings['name'] }}</h1>
                            <p>
                                {!! @$settings['web_description'] !!}
                            </p>

                            @include('themeedu::pages.auth.count_student')
                            <div class="barcode">
                                <figure><img src="{{ URL::asset('public/frontend/themes/edu/images/resources/Barcode.jpg') }}" alt=""></figure>
                                <div class="app-download">
                                    <span>Tải xuống ứng dụng di động và quét mã QR để đăng nhập</span>
                                    <ul class="colla-apps">
                                        <li><a title="" href="https://play.google.com/store?hl=en"><img
                                                        src="{{ URL::asset('public/frontend/themes/edu/images/android.png') }}" alt="">Android</a></li>
                                        <li><a title="" href="https://www.apple.com/lae/ios/app-store/"><img
                                                        src="{{ URL::asset('public/frontend/themes/edu/images/apple.png') }}" alt="">iPhone</a></li>
                                        <li><a title="" href="https://www.microsoft.com/store/apps"><img
                                                        src="{{ URL::asset('public/frontend/themes/edu/images/windows.png') }}" alt="">Windows</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if (session('success'))
                        <div class="alert bg-success" role="alert">
                            <p style="color: red"><b>{!!session('success')!!}</b></p>
                        </div>
                    @endif
                    <div class="col-lg-4">
                        <div class="we-login-register"style="background-color: #003300;">
                            <div class="form-title">
                                <i class="fa fa-key"></i>Đổi mật khẩu
                                <span>Nhập mật khẩu mới của bạn vào</span>
                            </div>
                            <form class="we-form" method="post">
                                {{csrf_field()}}
                                <input name="change_password" value=""
                                       style="display: none;">
                                <input type="password" name="password" placeholder="Nhập mật khẩu mới" required>
                                <input type="password" name="re_password" placeholder="Nhập lại mật khẩu" required>
                                <button type="submit" data-ripple="">Đổi mật khẩu</button>
                            </form>
                            {{--<a class="with-smedia facebook" href="#" title="" data-ripple="">Register with facebook</a>
                            <a class="with-smedia twitter" href="#" title="" data-ripple="">Register with twitter</a>--}}
                            <span>Bạn đã có tài khoản ? <a class="we-account underline" href="/dang-nhap" title="">Đăng nhập</a></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection