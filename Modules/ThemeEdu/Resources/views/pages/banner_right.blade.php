<div class="widget stick-widget">
    <?php
    $widget = CommonHelper::getFromCache('widgets_location_home_sidebar_right');
    if (!$widget) {
        $widget = \Modules\ThemeEdu\Models\Widget::select(['name', 'content', 'location'])
            ->where('location', 'home_sidebar_right')->where('status', 1)
            ->first();
        CommonHelper::putToCache('widgets_location_home_sidebar_right', $widget);
    }
    ?>
        <div class="widget">
        <h4 class="widget-title">{{$widget->name}}</h4>
        <ul class="short-profile">
            <li>
                <p>{!! $widget->content !!}</p>
            </li>
        </ul>
        </div><!-- twitter feed-->
</div>
<!-- Modal -->
