@extends('themeedu::layouts.default')
@section('khoahoc')
    class="active"
@endsection
@section('main_content')
<div class="se-pre-con"></div>
<div class="theme-layout">
    <style>
        .name_contact{
            margin-left: 5%;
            margin-top: 5%;
            width: 90%;
            height: 50%;
        }
        .mess_contact{
            margin-left: 5%;
            margin-top: 5%;
            width: 90%;
            height: 100%;
        }
        .input_contact{
            height: 45px;
        }
        .input_mess_contact{
            height: 130px;
        }
    </style>

    @include('themeedu::template.top_bar')

    <section>
        <div class="gap2 gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row merged20" id="page-contents">
                            <div class="user-profile">
                                    @include('themeedu::template.menu')
                            </div><!-- user profile banner  -->
                            <div class="col-lg-3">
                                <aside class="sidebar static left">
                                    <div class="widget stick-widget">
                                        <?php
                                        $categories = CommonHelper::getFromCache('categorys_type_5_location_menu_cate');
                                        if (!$categories) {
                                            $categories = \Modules\EduCourse\Models\Category::where('type', 5)->where('location', 'menu_cate')->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
                                            CommonHelper::putToCache('categorys_type_5_location_menu_cate', $categories);
                                        }
                                        ?>
                                        <h4 class="widget-title">Chuyên mục</h4>
                                        <ul class="naves">
                                            @foreach($categories as $cat)
                                                <li>
                                                    <i class="ti-clipboard"></i>
                                                    <a href="/khoa-hoc/{{ @$cat->slug }}"
                                                       title="{{ @$cat->name }}">{{ @$cat->name }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div><!-- Shortcuts -->
                                </aside>
                            </div><!-- sidebar -->

                            <div class="col-lg-9">
                                <div class="row merged20">
                                    @foreach($courses as $course)
                            <div class="col-lg-6 col-md-6">
                                        @include('themeedu::partials.course_item')
                            </div>
                                    @endforeach
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-12 paginatee">
                                        {{ $courses->appends(Request::all())->links() }}
                                    </div>
                                </div>

                            </div><!-- centerl meta -->
                            @include('themeedu::pages.form_contact')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- content -->
</div>
@endsection