@extends('themeedu::layouts.default')
@section('khoahoc')
    class="active"
@endsection
@section('main_content')
    <div class="postoverlay"></div>
    @include('themeedu::template.top_bar')
    <section>
        <div class="gap2 gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row merged20" id="page-contents">
                            <div class="user-profile">
                                    @include('themeedu::template.menu')
                            </div><!-- user profile banner  -->

                            <div id="__nuxt"><!---->
                                <div id="__layout">
                                    <div>
                                        <div class="overlay "></div>
                                        <div class="wrapper">
                                            <div class="body-content">
                                                <div class="in-row-inline-flex">

                                                    <div class="main-content-wrapper">
                                                        <div class="main-content">
                                                            <div data-v-0c4294fa="" class="single_course">
                                                                <div data-v-0c4294fa="" class="single_course_header">
                                                                    <div data-v-0c4294fa="" class="container-fluid">
                                                                        <div data-v-0c4294fa="" class="row">
                                                                            <div data-v-0c4294fa="" class="col-md-8">
                                                                                <div data-v-0c4294fa="" class="row">
                                                                                    <div data-v-0c4294fa=""
                                                                                         class="col-md-3 text-center">
                                                                                        <img
                                                                                                data-v-0c4294fa=""
                                                                                                src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($course->image,177,110) }}"
                                                                                                alt=""
                                                                                                class="img-responsive">
                                                                                    </div>
                                                                                    <div data-v-0c4294fa=""
                                                                                         class="col-md-9 course_description">
                                                                                        <h2
                                                                                                data-v-0c4294fa="">{{@$course->name}}</h2>
                                                                                        <p data-v-0c4294fa="">{!! @$course->intro !!}</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-v-0c4294fa="" class="single_course_body bg-white">
                                                                    <div data-v-0c4294fa="" class="container-fluid">
                                                                        <div data-v-0c4294fa="" class="row">
                                                                            <div data-v-0c4294fa=""
                                                                                 class="course_single_content col-md-9">
                                                                                <div data-v-0c4294fa="">
                                                                                    {!! @$course->content !!}
                                                                                </div>
                                                                                <div data-v-0c4294fa=""
                                                                                     class="main-lesson-list">
                                                                                    <div data-v-0c4294fa=""
                                                                                         class="list_lesson_header">
                                                                                        <div data-v-0c4294fa=""
                                                                                             class="row">
                                                                                            <div style="color: black;" data-v-0c4294fa=""
                                                                                                 class="col-6 text-left">
                                                                                                Lesson
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div data-v-0c4294fa=""
                                                                                         class="list_lesson_expandation">
                                                                                        <?php
                                                                                        $lessons = CommonHelper::getFromCache('lesson_course_id'.$course->id);
                                                                                        if (!$lessons) {
                                                                                            $lessons = \Modules\EduCourse\Models\Lesson::where('course_id', $course->id)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();

                                                                                            CommonHelper::putToCache('lesson_course_id'.$course->id, $lessons);
                                                                                        }
                                                                                        ?>
                                                                                        @foreach($lessons as $lesson)
                                                                                            <div data-v-42fb8d25=""
                                                                                                 data-v-0c4294fa=""
                                                                                                 class="recording list_lesson">
                                                                                                <h4 data-v-42fb8d25="">
                                                                                                    <a data-v-42fb8d25=""
                                                                                                       class="lesson_completed topic_with_completed"style="color: black;"
                                                                                                       router-link-active="">Lesson
                                                                                                        - {{@$lesson->name}}</a>
                                                                                                    <p data-v-42fb8d25=""
                                                                                                       class="lesson-meta cursor">
                                                                        <span data-v-42fb8d25=""
                                                                              class="drop-list fa fa-chevron-down"></span>
                                                                                                    </p></h4>
                                                                                                <div data-v-42fb8d25=""
                                                                                                     id="eng-lesson-01"
                                                                                                     class="spoiler collapse"
                                                                                                     style="display: none;">
                                                                                                    <div data-v-42fb8d25=""
                                                                                                         class="topic_list_wrapper">
                                                                                                        <ul data-v-42fb8d25="">
                                                                                                            <?php
                                                                                                            $lessonItem = CommonHelper::getFromCache('lessonitem_by_lesson_id' . $lesson->id);
                                                                                                            if (!$lessonItem) {
                                                                                                                $lessonItem = $lesson->lessonitem;
                                                                                                                CommonHelper::putToCache('lessonitem_by_lesson_id' . $lesson->id, $lessonItem);
                                                                                                            }
                                                                                                            ?>
                                                                                                            @foreach($lessonItem as $k=>$item)
                                                                                                                <?php
                                                                                                                $check = CommonHelper::getFromCache('route_lesson_item_id'.$item->id);
                                                                                                                if (!$check) {
                                                                                                                    $check = \Modules\ThemeEdu\Models\Route::where('student_id', \Auth::guard('student')->user()->id)->where('lesson_item_id', $item->id)->count();

                                                                                                                    CommonHelper::putToCache('route_lesson_item_id'.$item->id, $check);
                                                                                                                }
                                                                                                                ?>
                                                                                                                <li data-v-42fb8d25="">
                                                                                                                    @if($check > 0 || $k==0)
                                                                                                                    <span data-v-42fb8d25=""
                                                                                                                            class="topic_item">
                                                                                                                        <a data-v-42fb8d25=""
                                                                                                                                href="{{@$item->slug}}.html"
                                                                                                                                class="topic_completed">{{@$item->name}}</a></span>
                                                                                                                        @else
                                                                                                                        <span data-v-42fb8d25=""
                                                                                                                              class="topic_item">
                                                                                                                        <a data-v-42fb8d25=""
                                                                                                                           rel="nofollow"
                                                                                                                           class="topic_completed">{{@$item->name}}</a></span>
                                                                                                                        @endif
                                                                                                                </li>
                                                                                                            @endforeach
                                                                                                                <?php
                                                                                                                $quizzes = CommonHelper::getFromCache('quizz_by_lesson_id' . $lesson->id);
                                                                                                                if (!$quizzes) {
                                                                                                                    $quizzes = $lesson->quiz;
                                                                                                                    CommonHelper::putToCache('quizz_by_lesson_id' . $lesson->id, $quizzes);
                                                                                                                }
                                                                                                                ?>
                                                                                                            @foreach(@$quizzes as $quiz)
                                                                                                                <li data-v-42fb8d25=""><span
                                                                                                                            data-v-42fb8d25=""
                                                                                                                            class="topic_item"><a
                                                                                                                                data-v-42fb8d25=""
                                                                                                                                href="{{@$quiz->slug}}.html"
                                                                                                                                class="quizz_completed">{{@$quiz->name}}</a></span>
                                                                                                                </li>
                                                                                                            @endforeach
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endforeach
                                                                                    </div>
                                                                                    <div data-v-0c4294fa=""
                                                                                         class="mark-complete-wrapper">
                                                                                        <!----></div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-v-0c4294fa=""
                                                                                 class="course_single_sidebar col-md-3 bg-white">

                                                                                <div data-v-0c4294fa="">
                                                                                    <?php
                                                                                    $banners = CommonHelper::getFromCache('banners_location_lessons');
                                                                                    if (!$banners) {
                                                                                        $banners = \Modules\ThemeEdu\Models\Banner::select(['name', 'link', 'image'])->where('location', 'lesson')->where('status', 1)
                                                                                            ->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
                                                                                        CommonHelper::putToCache('banners_location_lessons', $banners);
                                                                                    }

                                                                                    ?>
                                                                                    <h2 data-v-0c4294fa="">Kiến
                                                                                        thức</h2>
                                                                                    <div data-v-0c4294fa=""
                                                                                         class="recommend_course">
                                                                                        @foreach($banners as $banner)
                                                                                            <div data-v-0c4294fa=""
                                                                                                 class="recommend_course_content">
                                                                                                <a
                                                                                                        data-v-0c4294fa=""
                                                                                                        href="{{@$banner->link}}"><img
                                                                                                            data-v-0c4294fa=""
                                                                                                            src="{{@\App\Http\Helpers\CommonHelper::getUrlImageThumb($banner->image,266,162)}}"
                                                                                                            alt="Image 1"
                                                                                                            class="mx-auto d-block"
                                                                                                            style="padding: unset;"></a>
                                                                                                <h3 data-v-0c4294fa=""
                                                                                                    class="text-center">{{@$banner->name}}</h3>
                                                                                            </div>
                                                                                        @endforeach
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <script>
                                $(document).ready(function () {
                                    $(".recording").click(function () {
                                        $(this).children(".spoiler").toggle();
                                    });
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection