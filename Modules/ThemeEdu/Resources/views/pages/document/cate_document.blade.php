 <div class="widget">
            <?php
     $categories = CommonHelper::getFromCache('categorys_type_1_location_menu_cate');
     if (!$categories) {
         $categories = \Modules\EduCourse\Models\Category::where('type', 1)->where('location', 'menu_cate')->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
         CommonHelper::putToCache('categorys_type_1_location_menu_cate', $categories);
     }
            ?>
            <h4 class="widget-title">Danh mục</h4>
            <ul class="naves">
                @foreach($categories as $cat)
                    <li>
                        <i class="ti-clipboard"></i>
                        <a href="/tai-lieu/{{ @$cat->slug }}"
                           title="{{ @$cat->name }}">{{ @$cat->name }}</a>
                    </li>
                @endforeach
            </ul>
        </div><!-- Shortcuts -->

