<div class="show_dangky modal fade" id="exampleModalLong" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Đăng ký khóa học</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="cmsmasters_column_inner">
                <div id="cmsmasters_fb_667dcb9aeb" class="cmsmasters_featured_block">
                    <div class="featured_block_inner">
                        <div class="featured_block_text">
                            <div role="form" class="wpcf7" id="wpcf7-f13346-p7366-o1" lang="en-US" dir="ltr">
                                <div class="screen-reader-response"></div>
                                <form action="" method="post" class="wpcf7-form dang-ky-course">
                                    <div class="cmsmasters_row_margin cmsmasters_11">
                                        <input type="hidden" name="course_id" value="">
                                        <div class="name_contact one_half">
                                            <span><input type="text" name="name"
                                                                                              value=""
                                                                                              class="form-control"
                                                                                              placeholder="Họ & Tên"
                                                                                              required></span>
                                        </div>
                                        <div class="name_contact one_half">
                                            <span class="wpcf7-form-control-wrap Email"><input type="email" name="email"
                                                                                               value="" size="40"
                                                                                               class=" form-control "


                                                                                               placeholder="Email"></span>
                                        </div>
                                        <div class="name_contact one_first">
                                            <span class="wpcf7-form-control-wrap Phone"><input type="tel" name="phone"
                                                                                               value="" size="40"
                                                                                               class=" form-control "

                                                                                               placeholder="Số điện thoại"
                                                                                               required></span>
                                        </div>
                                        <div class="mess_contact one_first">
                                            <span class="wpcf7-form-control-wrap Message"><textarea name="message"
                                                                                                    cols="40" rows="5"
                                                                                                    class=" form-control"

                                                                                                    placeholder="Lời nhắn"></textarea></span>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="cmsmasters_button btn btn-primary">Gửi đi
                                            </button>
                                        </div>
                                    </div>
                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>