@extends('themeedu::layouts.default')
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <style>
            .name_contact {
                margin-left: 5%;
                margin-top: 5%;
                width: 90%;
                height: 50%;
            }

            .mess_contact {
                margin-left: 5%;
                margin-top: 5%;
                width: 90%;
                height: 100%;
            }

            .input_contact {
                height: 45px;
            }

            .input_mess_contact {
                height: 130px;
            }
        </style>
        @include('themeedu::template.top_bar')
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">

                                    @include('themeedu::template.menu')
                                </div><!-- user profile banner  -->
                                <div class="col-lg-3">
                                    <aside class="sidebar static left">
                                        <div class="widget">
                                            <h4 class="widget-title">Giới thiệu</h4>
                                            <ul class="fav-community">
                                                <li><i class=" fa fa-address-card"></i> Về chúng tôi
                                                    <p>Trung tâm ngoại ngữ uy tín hàng đầu</p>
                                                </li>
                                                <li><i class="fa fa-globe"></i><a style="color: grey;" href="/"
                                                                                  title="">{{ @$settings['name'] }}</a>
                                                </li>
                                                <li><i class="fa fa-phone"></i><a style="color: grey;" href="tel:{{$settings['hotline']}}"
                                                                                  title="">{!! @$settings['hotline'] !!}</a>
                                                </li>
                                                <li><i class="fa fa-map-marker"></i><a style="color: grey;"
                                                            href="mailto:{{ @$settings['email'] }}"
                                                            class="">{{ @$settings['email'] }}</a>
                                                </li>
                                                <li><i class="fa fa-globe"></i><a style="color: grey;" href="#"
                                                                                  title="">{{ @$settings['address'] }}</a>
                                                </li>
                                            </ul>
                                        </div><!-- Page Community-->
                                        <div class="widget">
                                            @include('themeedu::pages.widget_3')

                                        </div><!-- twitter feed-->

                                    </aside>
                                </div><!-- sidebar -->
                                <div class="col-lg-6">
                                    <div class="central-meta">
                                        <span class="create-post">TIN MỚI <a href="/tai-lieu"
                                                                             title="">Xem tất cả</a></span>
                                        <ul class="suggested-frnd-caro">
                                            <?php
                                            $news = CommonHelper::getFromCache('posts_created_at_desc');
                                            if (!$news) {
                                                $news = \Modules\ThemeEdu\Models\Post::where('status', 1)->orderBy('created_at', 'DESC')->orderBy('id', 'DESC')->get();
                                                CommonHelper::putToCache('posts_created_at_desc', $news);
                                            }
                                            ?>
                                            @foreach($news as $new)

                                                <li style="height: 270px">
                                                    <img src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($new->image,116,116) }}"
                                                         alt="{{@$new->name}}">
                                                    <div class="sugtd-frnd-meta">
                                                        <a href="/tai-lieu/{{@$new->slug}}.html"
                                                           title="{{@$new->name}}">{{@$new->name}}</a>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div><!-- suggested friends -->
                                    <div class="">
                                        <?php
                                        $courses = CommonHelper::getFromCache('courses_created_at_desc');
                                        if (!$courses) {
                                            $courses = \Modules\EduCourse\Models\Course::where('status', 1)->orderBy('created_at', 'DESC')->orderBy('id', 'DESC')->take(4)->get();
                                            CommonHelper::putToCache('courses_created_at_desc', $courses);
                                        }
                                        ?>
                                        @foreach($courses as $course)
                                            @include('themeedu::partials.course_item')
                                        @endforeach
                                    </div>
                                </div><!-- centerl meta -->

                                <div class="col-lg-3">
                                    <aside class="sidebar static right">
                                        @include('themeedu::pages.banner_right')
                                    </aside>
                                </div><!-- sidebar -->
                                @include('themeedu::pages.form_contact')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>
@endsection