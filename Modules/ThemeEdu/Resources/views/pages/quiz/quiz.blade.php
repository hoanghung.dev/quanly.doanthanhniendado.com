@extends('themeedu::layouts.default')
@section('baikiemtra')
    class="active"
@endsection
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <div class="postoverlay"></div>
        @include('themeedu::template.top_bar')
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">
                                    @include('themeedu::template.menu')
                                </div><!-- user profile banner  -->
                                <div class="col-lg-3">
                                    <aside class="sidebar static left">
                                        <div class="widget stick-widget">
                                            <h4 class="widget-title">Bảng xếp hạng</h4>
                                            <ul class="followers ps-container ps-theme-default ps-active-y"
                                                data-ps-id="42e41a80-d050-998e-adde-c3912d0eac78">
                                                <?php
                                                $quiz_logs = CommonHelper::getFromCache('quiz_logs_sum_accumulated_points');
                                                if (!$quiz_logs) {
                                                    $quiz_logs = \Modules\ThemeEdu\Models\QuizLog::select('student_id', DB::raw('sum(accumulated_points) as accumulated_points'))->where('accumulated_points','<>',0)->groupBy('student_id')
                                                        ->orderBy('accumulated_points', 'desc')->get();
                                                    CommonHelper::putToCache('quiz_logs_sum_accumulated_points', $quiz_logs);
                                                }
                                                ?>
                                                @foreach($quiz_logs as $quiz_log)
                                                        <?php
                                                        $rankStudent = CommonHelper::getFromCache('rank_student');
                                                        if (!$rankStudent) {
                                                            $rankStudent = $quiz_log->student;
                                                            CommonHelper::putToCache('rank_student', $rankStudent);
                                                        }
                                                        ?>
                                                    @if(is_object($rankStudent))
                                                        <li>
                                                            <figure><img
                                                                        src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$rankStudent->image, false, false) }}"
                                                                        alt="">
                                                            </figure>
                                                            <div class="friend-meta">
                                                                <h4><a href="/profile/{{ @$rankStudent->id }}"
                                                                       title="">{{ @$rankStudent->name }}</a></h4>
                                                                <a rel="nofollow" title="Điểm tích lũy đạt được" class="underline">{{ number_format($quiz_log->accumulated_points, 0, '.', '.') }} điểm</a>
                                                            </div>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div><!-- Shortcuts -->
                                        <div class="widget stick-widget">
                                            <?php
                                            $categories = CommonHelper::getFromCache('categorys_type_4_location_maenu_cate');
                                            if (!$categories) {
                                                $categories = \Modules\EduCourse\Models\Category::where('type', 4)->where('location', 'menu_cate')->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();

                                                CommonHelper::putToCache('categorys_type_4_location_maenu_cate', $categories);
                                            }
                                            ?>
                                            <h4 class="widget-title">Chuyên mục</h4>
                                            <ul class="naves">

                                                @foreach($categories as $category )
                                                    <li>
                                                        <i class="ti-clipboard"></i>
                                                        <a href="/bai-kiem-tra/{{@$category->slug}}"
                                                           title="{{@$category->name}}">{{@$category->name}}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div><!-- Shortcuts -->
                                    </aside>
                                </div><!-- sidebar -->
                                <div class="col-lg-6">
                                    <div class="load-more4">
                                        @foreach($quizs as $quiz)
                                            <div class="central-meta item">
                                                <div class="classi-pst">
                                                    <div class="row merged10">
                                                        <div class="col-lg-6">
                                                            <div class="image-bunch single">
                                                                <figure><img
                                                                            src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($quiz->image,244,122) }}"
                                                                            alt="{{$quiz->name}}"></figure>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="classi-pst-meta">
                                                                <h6><a href="/{{$slug1}}/{{$quiz->slug}}.html"
                                                                       title="">{{@$quiz->name}}</a>
                                                                </h6>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <p class="classi-pst-des">
                                                                {!! @$quiz->intro !!}
                                                            </p>
                                                            @if(\Auth::guard('student')->check())
                                                            <a class="active btn btn-info"
                                                               href="/{{$slug1}}/{{@$quiz->slug}}.html"
                                                               style="    background-color: #fa6342;float: right;border-color: #bc482e;">Kiểm
                                                                tra ngay</a>
                                                                @else
                                                                <a class="active btn btn-info"
                                                                   href="/dang-nhap"
                                                                   style="    background-color: #fa6342;float: right;border-color: #bc482e;">Đăng nhập để kiểm tra</a>
                                                                @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-md-12 paginatee">
                                            {{ @$quizs->appends(Request::all())->links() }}
                                        </div>
                                    </div>
                                </div><!-- centerl meta -->
                                <div class="col-lg-3">
                                    <aside class="sidebar static right">
                                        <div class="widget">
                                            @include('themeedu::pages.banner_right')
                                        </div>
                                        <div class="widget stick-widget">
                                            @include('themeedu::pages.widget_3')
                                        </div>
                                    </aside>
                                </div><!-- sidebar -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>
@endsection