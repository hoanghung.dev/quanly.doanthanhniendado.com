@extends('themeedu::layouts.default')
@section('khoahoc')
    class="active"
@endsection
@section('main_content')
<div class="se-pre-con"></div>
<div class="theme-layout">
    <style>
        .name_contact{
            margin-left: 5%;
            margin-top: 5%;
            width: 90%;
            height: 50%;
        }
        .mess_contact{
            margin-left: 5%;
            margin-top: 5%;
            width: 90%;
            height: 100%;
        }
        .input_contact{
            height: 45px;
        }
        .input_mess_contact{
            height: 130px;
        }
    </style>

    @include('themeedu::template.top_bar')
    <section>
        <div class="gap2 gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row merged20" id="page-contents">
                            <div class="user-profile">
                                @include('themeedu::partials.student_menu')
                            </div><!-- user profile banner  -->
                            <div class="col-lg-12">
                                <div class="row merged20 load-more4">
                                    @foreach($courses as $course)
                                        <div class="col-lg-6 col-md-6">
                                            <div class="central-meta item">
                                                <div class="classi-pst">
                                                    <div class="row merged10">
                                                        <div class="col-lg-6">
                                                            <div class="image-bunch single">
                                                                <?php
                                                                $courseStudent = CommonHelper::getFromCache('course_student');
                                                                if (!$courseStudent) {
                                                                    $courseStudent = $course->course;
                                                                    CommonHelper::putToCache('course_student', $courseStudent);
                                                                }
                                                                ?>
                                                                <figure><img
                                                                            src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$courseStudent->image,244,122) }}"
                                                                            alt="{{@$course->name}}"></figure>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="classi-pst-meta">
                                                                    <h6><a href="/khoa-hoc/{{(@$courseStudent->slug)}}.html"
                                                                           title="{{@$courseStudent->name}}">{{@$courseStudent->name}}</a></h6>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <p class="classi-pst-des">
                                                                {!!@$courseStudent->intro!!}
                                                            </p>
                                                                <a class="active btn btn-info" href="/khoa-hoc/{{(@$courseStudent->slug)}}.html"
                                                                   style="background-color: #fa6342;float: right;border-color: #bc482e;">Học ngay</a>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-12 paginatee">
                                        {{ $courses->appends(Request::all())->links() }}
                                    </div>
                                </div>
                            </div><!-- centerl meta -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- content -->
</div>
@endsection
