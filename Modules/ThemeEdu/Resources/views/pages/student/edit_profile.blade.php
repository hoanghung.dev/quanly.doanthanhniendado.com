@extends('themeedu::layouts.default')
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">



    @include('themeedu::template.top_bar')

    <!-- topbar -->
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">
                                    @include('themeedu::partials.student_menu')
                                </div><!-- user profile banner  -->

                                <div class="col-lg-8 col-md-8">
                                    <div class="forum-form">
                                        <div class="central-meta">
                                            <span class="create-post">Thông tin cơ bản</span>
                                            <form method="post" class="c-form" action="/edit-profile">
                                                {!! csrf_field() !!}
                                                <div>
                                                    <label>Họ & tên</label>
                                                    <input type="text" name="name" placeholder="Name" value="{{ Auth::guard('student')->user()->name }}">
                                                </div>
                                                <div>
                                                    <label>Số điện thoại</label>
                                                    <input type="text" name="phone" placeholder="Phone" value="{{ Auth::guard('student')->user()->phone }}">
                                                </div>
                                                <div>
                                                    <label>Email</label>
                                                    <input type="text" name="email" placeholder="Email" value="{{ Auth::guard('student')->user()->email }}">
                                                </div>
                                                <div>
                                                    <label>Địa chỉ</label>
                                                    <input type="text" name="address" placeholder="Address" value="{{ Auth::guard('student')->user()->address }}">
                                                </div>
                                                <div>
                                                    <label>Ảnh đại diện</label>
                                                    <input type="file" name="image" placeholder="Image" value="{{ Auth::guard('student')->user()->image }}">
                                                </div>
                                                <div>
                                                    <label>Ảnh banner</label>
                                                    <input type="file" name="banner" placeholder="Banner" value="{{ Auth::guard('student')->user()->banner }}">
                                                </div>
                                                <input type="hidden" name="student_id" value="{{ Auth::guard('student')->user()->id }}">
                                                <div>
                                                    <button class="main-btn" type="submit" data-ripple="">Cập nhật
                                                    </button>
                                                    <a class="main-btn3" href="/profile">Quay lại
                                                    </a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="central-meta stick-widget">
                                        <span class="create-post">Cấu hình khác</span>
                                        <div class="personal-head">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>

@endsection