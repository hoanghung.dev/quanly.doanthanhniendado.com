@extends('themeedu::layouts.default')
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
    @include('themeedu::template.top_bar')
    <!-- topbar -->
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">
                                @include('themeedu::partials.student_menu', ['user' => $user])
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="central-meta stick-widget">

                                        <span class="create-post">Giới thiệu</span>
                                        <div class="personal-head">
                                            <span class="f-title"><i class="fa fa-user"></i> Về tôi:</span>
                                            <p>
                                                Học viên gương mẫu, thấm nhuần tư tưởng của Đảng
                                            </p>
                                            <span class="f-title"><i class="fa fa-birthday-cake"></i> Ngày sinh:</span>
                                            <p>
                                                @if(@$user->birthday != '' && @$user->birthday != null)
                                                    {{date('d-m-Y',strtotime(@$user->birthday))}}
                                                @endif
                                            </p>
                                            <span class="f-title"><i class="fa fa-phone"></i> Điện thoại:</span>
                                            <p>
                                                {{@$user->phone}}
                                            </p>
                                            <span class="f-title"><i class="fa fa-male"></i> Giới tính:</span>
                                            <p>
                                                @if(@$user->gender==1)
                                                    Nam
                                                    @else
                                                        Nữ
                                            @endif
                                            </p>
                                            <span class="f-title"><i class="fa fa-globe"></i> Địa chỉ:</span>
                                            <p>
                                                {{@$user->address}}
                                            </p>
                                            <span class="f-title"><i class="fa fa-envelope"></i> Email:</span>
                                            <p>
                                                <a href="mailto:{{@$user->email}}"
                                                   class="__cf_email__"
                                                   data-cfemail="cb9ba2bfa5a2a08bb2a4beb9a6aaa2a7e5a8a4a6">
                                                    {{@$user->email}}
                                                </a>
                                            </p>
                                            <a href="/edit-profile"
                                               style="    color: #007bff;text-decoration: underline; cursor: pointer;">Chỉnh
                                                sửa profile</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8">
                                    <div class="central-meta">
                                        <span class="create-post">Thành tích học viên</span>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="gen-metabox">
                                                    <span><i class="fa fa-puzzle-piece"></i> Điểm tích lũy</span>
                                                    <p>
                                                        {{@$accumulated_points+@$accumulated_point}}
                                                    </p>
                                                </div>
                                                <div class="gen-metabox">
                                                    <span><i class="fa fa-plus"></i> Số khóa học đã đăng ký</span>
                                                    <p>
                                                        {{@$count_course}}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="gen-metabox">
                                                    <span><i class="fa fa-mortar-board"></i>Điểm trung bình bài kiểm tra</span>
                                                    <p>
                                                        @if($count_scores > 0)
                                                        {{round(($score/$count_scores)*100)}}/100
                                                        @endif
                                                    </p>
                                                </div>
                                                <div class="gen-metabox">
                                                    <span><i class="fa fa-certificate"></i>Bài kiểm tra đã làm</span>
                                                    <p>
                                                        {{@$count_quizz}}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="gen-metabox no-margin">
                                                    <span><i class="fa fa-trophy"></i> Đánh giá</span>
                                                    <p>
                                                        {!! @$user->review !!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="central-meta">
                                        <span class="create-post">Khóa học <a href="/student/{{ @$user->id }}/khoa-hoc"
                                                                              title="">Xem tất cả</a></span>
                                        <div class="row">
                                            <?php
                                            $courses = CommonHelper::getFromCache('course_created_at_desc');
                                            if (!$courses) {
                                                $courses = \Modules\EduCourse\Models\Course::where('status', 1)->orderBy('created_at', 'desc')->take(3)->get();
                                                CommonHelper::putToCache('course_created_at_desc', $courses);
                                            }

                                            ?>
                                            @foreach($courses as $course)
                                                <div class="col-lg-4 col-md-6 col-sm-6">
                                                    <div class="fav-play">
                                                        <figure>
                                                            <img src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($course->image,210,129) }}"
                                                                 alt="{{@$course->name}}">
                                                        </figure>
                                                        <span class="tv-play-title">{{@$course->name}} </span>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="central-meta">
                                        <span class="create-post">Bài kiểm tra <a href="/student/{{ @$user->id }}/bai-kiem-tra"
                                                                                  title="">Xem tất cả</a></span>
                                        <div class="row">
                                            <?php
                                            $quizs = CommonHelper::getFromCache('quizs_id_desc');
                                            if (!$quizs) {
                                                $quizs = \Modules\EduCourse\Models\Quizzes::orderBy('id', 'desc')->take(3)->get();
                                                CommonHelper::putToCache('quizs_id_desc', $quizs);
                                            }

                                            ?>
                                            @foreach($quizs as $quiz)
                                                <div class="col-lg-4 col-md-6 col-sm-6">
                                                    <div class="fav-play">
                                                        <figure>
                                                            <img src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($quiz->image,210,129) }}"
                                                                 alt="{{@$quiz->name}}">
                                                        </figure>
                                                        <span class="tv-play-title">{{@$quiz->name}} </span>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>
@endsection