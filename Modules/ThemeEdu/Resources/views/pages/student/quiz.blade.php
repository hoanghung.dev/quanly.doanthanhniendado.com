@extends('themeedu::layouts.default')
@section('baikiemtra')
    class="active"
@endsection
@section('main_content')
<div class="se-pre-con"></div>
<div class="theme-layout">
    <div class="postoverlay"></div>
    @include('themeedu::template.top_bar')
    <section style="min-height: 850px;background-color: #edf2f6;">
        <div class="gap2 gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row merged20" id="page-contents">
                            <div class="user-profile">
                                @include('themeedu::partials.student_menu')
                            </div><!-- user profile banner  -->

                            <div class="col-lg-12">
                                <div class="bg-white">
                                <h5 style="color: red;"><i>Ghi chú: Bài kiểm  tra chỉ ghi nhận lần làm đầu tiên. Nếu làm lại bài kiểm tra đã làm bạn sẽ không được tính thêm điểm tích lũy và không thay đổi được điểm bài kiểm tra đó</i></h5><br>
                                </div>
                                <table class="table table-striped" style="background-color: white">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tên bài kiểm tra</th>
                                        <th>Điểm làm bài</th>
                                        <th>Điểm tích lũy</th>
                                        <th>Thời gian làm</th>
                                        <th>Hành động</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($quizs as $quiz)
                                        <?php
                                        $quizLogStudent = CommonHelper::getFromCache('quiz_log_student');
                                        if (!$quizLogStudent) {
                                            $quizLogStudent = $quiz->quiz;
                                            CommonHelper::putToCache('quiz_log_student', $quizLogStudent);
                                        }
                                        ?>
                                    <tr>
                                        <th scope="row">{{@$k+=1}}</th>
                                        <td><a href="/bai-kiem-tra/{{@$quizLogStudent->slug}}.html">{{@$quizLogStudent->name}}</a> </td>
                                        <td>{{@$quiz->scores}}</td>
                                        <td>{{@$quiz->accumulated_points}}</td>
                                        <td>{{date('H:i:s d-m-Y',strtotime($quiz->created_at))}}</td>
                                        <td><a class="btn btn-info" href="/bai-kiem-tra/{{@$quizLogStudent->slug}}.html" >Làm lại</a></td>
                                    </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- content -->
</div>
@endsection