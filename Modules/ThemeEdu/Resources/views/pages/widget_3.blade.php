<h4 class="widget-title">Đánh giá</h4>
<ul class="short-profile">
    <?php
    $widgets = CommonHelper::getFromCache('widgets_location_home_sidebar_left');
    if (!$widgets) {
        $widgets = \Modules\ThemeEdu\Models\Widget::select(['name', 'content', 'location'])
            ->where('location', 'home_sidebar_left')->where('status', 1)
            ->orderBy('order_no', 'desc')->orderBy('id', 'desc')->get();
        CommonHelper::putToCache('widgets_location_home_sidebar_left', $widgets);
    }
    ?>
    @foreach($widgets as $widget)
        <li>
            <span>{{$widget->name}}</span>
            <p>{!! $widget->content !!} </p>
        </li>
    @endforeach
</ul>