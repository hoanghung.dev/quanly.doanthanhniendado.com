<div class="central-meta item">
    <div class="classi-pst">
        <div class="row merged10">
            <div class="col-lg-6">
                <div class="image-bunch single">
                    <figure><img
                                src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($course->image,244,122) }}"
                                alt="{{@$course->name}}"></figure>
                </div>
            </div>
            <?php
            $check_buy = 0;
            if (\Auth::guard('student')->check()) {
                $check_buy = CommonHelper::getFromCache('contacts_course_id_count');
                if (!$check_buy) {
                    $check_buy = \Modules\ThemeEdu\Models\Contact::where('status', 1)->where('course_id', $course->id)->where('student_id', \Auth::guard('student')->user()->id)->count();
                    CommonHelper::putToCache('contacts_course_id_count', $check_buy);
                }
            }
            ?>
            <div class="col-lg-6">
                <div class="classi-pst-meta">
                    @if($check_buy != 0)
                        <h6><a href="/khoa-hoc/{{(@$course->slug)}}.html"
                               title="{{@$course->name}}">{{@$course->name}}</a></h6>
                    @else
                        <h6><a data-toggle="modal" data-target="#exampleModalLong" class="dang_ky"
                               data-course_id="{{ $course->id }}" href="/khoa-hoc/{{@$course->slug}}.html"
                               title="">{{@$course->name}}</a></h6>
                    @endif
                </div>
            </div>
            <div class="col-lg-12">
                <p class="classi-pst-des">
                    {!!@$course->intro!!}
                </p>

                @if($check_buy != 0)
                    <a class="active btn btn-info" href="/khoa-hoc/{{(@$course->slug)}}.html"
                       style="background-color: #fa6342;float: right;border-color: #bc482e;">Học ngay</a>
                @else
                    @if(\Auth::guard('student')->check())
                    <button type="submit" class="active btn btn-info dang_ky_{{ $course->id }}"
                       data-course_id="{{ $course->id }}"
                       style="background-color: #fa6342;float: right;border-color: #bc482e;">Đăng ký</button>
                        @else
                        <a data-toggle="modal" data-target="#exampleModalLong"  class="active btn btn-info dang_ky_{{ $course->id }}"
                           data-course_id="{{ $course->id }}" href="/khoa-hoc/{{(@$course->slug)}}.html"
                           style="background-color: #fa6342;float: right;border-color: #bc482e;">Đăng ký</a>
                        @endif
                @endif
            </div>
        </div>
    </div>

</div>
<?php
$student = CommonHelper::getFromCache('contacts_course_id_first');
if (!$student) {
    $student=@\Modules\ThemeEdu\Models\Contact::where('student_id',\Auth::guard('student')->user()->id)->where('course_id',$course->id)->first();
    CommonHelper::putToCache('contacts_course_id_first', $student);
}
?>
@if(\Auth::guard('student')->check()&& !isset($student))
<script>
    $('.dang_ky_'+'{{ $course->id }}').click(function () {
        var that = $(this);
        var course_id = $(this).data('course_id');
        $.ajax({
            url: '{{route('register-course-when-login')}}',
            type: 'get',
            data: {
                course_id: course_id,
            },
            success: function (data) {
                if (data.status) {
                    alert(data.msg);
                } else {
                    alert('Có lỗi xảy ra. Vui lòng load lại website và thử lại!');
                }
            }
        });

    });
</script>
    @elseif(\Auth::guard('student')->check() && isset($student))
    <script>
        $('.dang_ky_'+'{{ $course->id }}').click(function () {
            var that = $(this);
            var course_id = $(this).data('course_id');
            alert('Bạn đã đăng ký khóa học này! Vui lòng chờ quản trị viên xét duyệt');
        });
    </script>
@else
<script>
    $('form.dang-ky-course').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/dang-ky-khoa-hoc',
            data: $('form.dang-ky-course').serialize(),
            type: 'POST',
            success: function (result) {
                $('form.dang-ky-course input, form.dang-ky-course textarea').val('');
                $('.show_dangky').hide();
                alert(result.msg);
            },
            error: function () {
                alert('Có lỗi xảy ra. Vui lòng load lại website và thử lại!');
            }
        });
    });

    $(document).ready(function(){
        $(".dang_ky").click(function(){
            $('.show_dangky').show();
            var course_id = $(this).data('course_id');
            $('form.dang-ky-course input[name=course_id]').val(course_id);
        });
    });
</script>
    @endif