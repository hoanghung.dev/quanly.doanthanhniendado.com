<div class="bottombar">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                $widget = CommonHelper::getFromCache('widgets_location_footer');
                if (!$widget) {
                    $widget=\Modules\ThemeEdu\Models\Widget::where('location','footer')->where('status',1)->first();
                    CommonHelper::putToCache('widgets_location_footer', $widget);
                }
                ?>
                <span class="copyright">{{@$widget->name}}</span>
            </div>
        </div>
    </div>
</div><!-- bottom bar -->