<figure>

        <img src="{{@\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$user->banner,1110,300)}}"
             alt="{{@@$user->name}}">
    </figure>
    <div class="profile-section">
        <div class="row">
            <div class="col-lg-2">
                <div class="profile-author">
                    <a class="profile-author-thumb"
                       href="/profile">
                        <img alt="author"
                             src="{{ asset('public/filemanager/userfiles/' . @$user->image) }}">
                    </a>
                    <div class="author-content">
                        <a class="h4 author-name"
                           href="/profile">{{ @$user->name }}</a>
                        <div class="country">{{ @$user->type != '' ? @$user->type : 'Học viên' }}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-10">
                <ul class="profile-menu">
                    <li>
                        <a @yield('khoahoc') href="/student/{{ @$user->id }}/khoa-hoc">Khóa
                            học</a>
                    </li>
                    <li>
                        <a @yield('baikiemtra') href="/student/{{ @$user->id }}/bai-kiem-tra">Bài
                            kiểm tra</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
