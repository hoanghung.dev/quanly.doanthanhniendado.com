<figure>
    <?php
    $banner = CommonHelper::getFromCache('banners_location_home_top');
    if (!$banner) {
        $banner = \Modules\ThemeEdu\Models\Banner::where('location', 'home_top')->where('status', 1)->first();
        CommonHelper::putToCache('banners_location_home_top', $banner);
    }
    ?>
    <img src="{{@\App\Http\Helpers\CommonHelper::getUrlImageThumb($banner->image,1110,300)}}"
         alt="{{@$banner->name}}">
</figure>
<div class="profile-section">
    <div class="row">
        <div class="col-lg-2">
            <div class="profile-author">
                <a class="profile-author-thumb" href="/">
                    <img alt="author" src="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}">
                </a>
                <div class="author-content">
                    <a class="h4 author-name" href="/">{{ @$settings['name'] }}</a>
                </div>
            </div>
        </div>
        <div class="col-lg-10">
            <ul class="profile-menu">
                <?php
                $actives = array(
                    'khoahoc',
                    'tailieu',
                    'baikiemtra',
                    'lichhoc'
                );
                $menus = CommonHelper::getFromCache('menus_location_main_menu');
                if (!$menus) {
                    $menus = \Modules\EduCourse\Models\Category::where('parent_id', 0)->where('location', 'main_menu')->where('status', 1)->orderby('order_no', 'DESC')
                        ->orderby('id', 'asc')->get();
                    CommonHelper::putToCache('menus_location_main_menu', $menus);
                }
                ?>
                @foreach($menus as $k =>$menu)
                    <li>
                        <a @yield($actives[$k]) href="/{{@$menu->slug}}">{{@$menu->name}}</a>
                    </li>
                @endforeach

            </ul>
        </div>
    </div>
</div>