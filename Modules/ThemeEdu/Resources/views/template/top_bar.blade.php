<div class="topbar stick">
    <div class="logo">
        <a title="" href="/"><img src="{{ asset('public/filemanager/userfiles/' . @$settings['logo-top']) }}" style="max-height: 70px;"
                                  alt="{{ @$settings['name'] }}"></a>
    </div>
    <div class="top-area">
        <div class="top-search">
            <form action="/tim-kiem" method="get" class="">
                <input type="search" name="q" class="form-control" value="" placeholder="Tìm kiếm khóa học">
                <button data-ripple=""><i class="ti-search"></i></button>
            </form>
        </div>
        @if(\Auth::guard('student')->check())
            <div class="user-img">
                <h5 style="margin-bottom: 5px;">{{@\Auth::guard('student')->user()->name}}</h5>
                <img style="margin-bottom: 10px;" src="{{@\App\Http\Helpers\CommonHelper::getUrlImageThumb(\Auth::guard('student')->user()->image,45,45)}}"
                     alt="{{@\Auth::guard('student')->user()->name}}">
                <span class="status f-online"></span>
                <div class="user-setting">
                    <ul class="log-out">
                        <li><a href="/profile" title=""><i class="ti-user"></i> Xem profile</a></li>
                        <li><a href="/edit-profile" title=""><i class="ti-pencil-alt"></i>Sửa profile</a></li>
                        <li><a href="/dang-xuat" title=""><i class="ti-power-off"></i>Đăng xuất</a></li>
                    </ul>
                </div>
            </div>
        @else
            <div class="user-img">
                <h5><a href="/dang-nhap">Đăng nhập</a></h5>
            </div>
        @endif
        <span class="" data-ripple=""></span>
    </div>
</div>