<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('register-course-when-login', 'Frontend\ContactController@registerCourseWhenLogin')->name('register-course-when-login');
Route::get('route', 'Frontend\ContactController@route')->name('route');
Route::get('topic', 'Frontend\ContactController@topic')->name('topic');
Route::group(['prefix' => '', 'middleware' => 'no_auth:student'], function () {
    Route::get('dang-nhap', 'Frontend\AuthController@getLogin');
    Route::post('dang-nhap', 'Frontend\AuthController@authenticate');
    Route::get('dang-ky', 'Frontend\AuthController@getRegister');
    Route::post('dang-ky', 'Frontend\AuthController@postRegister');
});

Route::match(array('GET', 'POST'), 'quen-mat-khau', 'Frontend\AuthController@getEmailForgotPassword');
Route::match(array('GET', 'POST'), 'forgot-password/{change_password}', 'Frontend\AuthController@getForgotPassword');
Route::get('dang-xuat', function () {
    \Auth::guard('student')->logout();
    return redirect('/');
});

//Route::match(array('GET', 'POST'), 'profile/{id}', 'Frontend\AuthController@profileAdmin')->name('profile');

Route::group(['prefix' => '', 'middleware' => ['guest:student']], function () {
    Route::get('edit-profile', 'Frontend\StudentController@getEditProfile');
    Route::post('edit-profile', 'Frontend\StudentController@postEditProfile');
    Route::group(['prefix' => 'student'], function () {
        Route::get('{id}/khoa-hoc', 'Frontend\StudentController@course');
        Route::get('{id}/bai-kiem-tra', 'Frontend\StudentController@quiz');

    });
});

Route::get('', 'Frontend\HomeController@getHome');

Route::get('profile/{id}', 'Frontend\StudentController@getProfile');
Route::get('profile', 'Frontend\StudentController@myProfile');

Route::post('dang-ky-khoa-hoc', 'Frontend\ContactController@postContact')->name('contact.post');
Route::get('tim-kiem', 'Frontend\CourseController@getSearch');

Route::get('{slug1}', 'Frontend\CourseController@oneParam');
Route::get('{slug1}/{slug2}', 'Frontend\CourseController@twoParam');
Route::get('{slug1}/{slug2}/{slug3}', 'Frontend\CourseController@threeParam');
Route::get('{slug1}/{slug2}/{slug3}/{slug4}', 'Frontend\CourseController@fourParam');
