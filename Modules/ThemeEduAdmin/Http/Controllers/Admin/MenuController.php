<?php

namespace Modules\ThemeEduAdmin\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;

class MenuController extends Controller
{

    protected $module = [
        'code' => 'menu',
        'label' => 'Menu',
    ];

    public function getIndex(Request $request)
    {
        $data['page_title'] = $this->module['label'];
        $data['page_type'] = 'list';

        return view('themeeduadmin::index')->with($data);
    }

}
