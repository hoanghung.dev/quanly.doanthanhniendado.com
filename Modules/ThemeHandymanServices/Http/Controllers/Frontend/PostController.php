<?php

namespace Modules\ThemeHandymanServices\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Modules\ThemeHandymanServices\Models\Menus;
use Modules\ThemeHandymanServices\Models\Post;

class PostController extends Controller
{

//      Trang chi tiết bài viết
    public function detail($slug) {
        $language = Setting::where('name','default_language')->where('type','general_tab')->first()->value;
        $slug = str_replace('.html', '', $slug);
//dd($slug);
        $data['post'] = Post::where('slug', $slug)->first();
        if (!is_object($data['post'])) {
            abort(404);
        }

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['post']->meta_title != '' ? $data['post']->meta_title : $data['post']->{'name_'.$language},
            'meta_description' => $data['post']->meta_description != '' ? $data['post']->meta_description : $data['post']->{'name_'.$language},
            'meta_keywords' => $data['post']->meta_keywords != '' ? $data['post']->meta_keywords : $data['post']->{'name_'.$language},
        ];
        view()->share('pageOption', $pageOption);

        return view('themehandymanservices::pages.post.detail',$data);
    }
    function searchPost(request $r)
    {
        $language = Setting::where('name','default_language')->where('type','general_tab')->first()->value;
        $data['posts']=Post::where('name_'.$language,'like','%'.$r->search.'%')->where('status', 1)->where('faq','<>', 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->paginate(5);
        $data['search']=$r->search;
        $pageOption = [
            'meta_title' => 'Tìm kiếm từ khóa: ' . @$r->search,
            'meta_description' => 'Tìm kiếm từ khóa: ' . @$r->search,
            'meta_keywords' => 'Tìm kiếm từ khóa: ' . @$r->search,
        ];
        view()->share('pageOption', $pageOption);

        return view('themehandymanservices::pages.post.search_post',$data);
    }
}
