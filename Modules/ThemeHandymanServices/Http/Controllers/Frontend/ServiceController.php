<?php

namespace Modules\ThemeHandymanServices\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Mail\MailServer;
use App\Models\Admin;
use App\Models\Setting;
use Illuminate\Http\Request;
use Modules\HandymanServicesMenu\Models\Menu;
use Modules\ThemeHandymanServices\Models\Booking;
use Modules\ThemeHandymanServices\Models\Menus;
use Modules\ThemeHandymanServices\Models\Category;
use Modules\ThemeHandymanServices\Models\Post;
use Modules\ThemeHandymanServices\Models\Service;
use Modules\ThemeHandymanServices\Models\ServiceFields;
use Modules\ThemeHandymanServices\Models\User;
use Validator;
use Mail;
use DB;
use Session;

class ServiceController extends Controller
{
    protected $_mailSetting;

    public function __construct()
    {
        $this->_mailSetting = Setting::whereIn('type', ['mail'])->pluck('value', 'name')->toArray();
    }


    //  Trang danh mục
    public function list($slug)
    {
        $language = Setting::where('name','default_language')->where('type','general_tab')->first()->value;
        $category = \Modules\HandymanServicesTheme\Models\Category::where('slug', $slug)->first();
//        $tags = Category::where('slug', $slug)->first();
        $service = Service::where('slug', $slug)->where('status', 1)->first();


//dd($category);
        if (!is_object($category)) {
            if (is_object($service)){
                $data['service'] = $service;
                $data['slug1'] = $slug;

                //  Thẻ meta cho seo
                $pageOption = [
                    'meta_title' => $service->meta_title != '' ? $service->meta_title : $service->{'name'.$language},
                    'meta_description' => $service->meta_description != '' ? $service->meta_description : $service->{'name'.$language},
                    'meta_keywords' => $service->meta_keywords != '' ? $service->meta_keywords : $service->{'name'.$language},
                ];
                view()->share('pageOption', $pageOption);
                return view('themehandymanservices::pages.service.service')->with($data);
            }
            abort(404);
        }
        $data['category'] = $category;
        $data['slug1'] = $slug;

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $category->meta_title != '' ? $category->meta_title : $category->{'name'.$language},
            'meta_description' => $category->meta_description != '' ? $category->meta_description : $category->{'name'.$language},
            'meta_keywords' => $category->meta_keywords != '' ? $category->meta_keywords : $category->{'name'.$language},
        ];
        view()->share('pageOption', $pageOption);


        if (in_array($category->type, [0, 1,2])) {         //  Tin tức//tag
            $posts = CommonHelper::getFromCache('posts_by_multi_cat_like_category_id' . $category->id . @$_GET['page']);

            if (!$posts) {
                $posts = Post::where('status', 1)->where(function ($query) use ($category) {
                    $cat_childs = $category->childs; //  Lấy các id của danh mục con
                    $query->orWhere('multi_cat', 'LIKE', '%|' . $category->id . '|%');  // truy vấn các tin thuộc danh mục hiện tại
                    foreach ($cat_childs as $cat_child) {
                        $query->orWhere('multi_cat', 'LIKE', '%|' . $cat_child->id . '|%');    //  truy vấn các tin thuộc các danh mục con của danh mục hiện tại
                    }
                })->where('faq','<>', 1)->orderBy('order_no', 'desc')->orderBy('updated_at', 'desc')->paginate(5);
                CommonHelper::putToCache('posts_by_multi_cat_like_category_id' . $category->id . @$_GET['page'], $posts);
            }
            $data['posts'] = $posts;

            return view('themehandymanservices::pages.post.list_post')->with($data);
        }
        elseif (in_array($category->type, [5])) {
                //  dịch vụ

//            $products = CommonHelper::getFromCache('posts_by_multi_cat_like_category_id' . $category->id . @$_GET['page']);
//            if (!$products) {
                $data['service'] = Service::where('slug', $slug)->first();
//                dd($slug);
                return view('themehandymanservices::pages.service.service')->with($data);
//                dd(1);
//                $products = Service::where('status', 1)
////                    ->where('company_id',$category->company_id)
//                    ->where(function ($query) use ($category) {
//                    $cat_childs = $category->childs; //  Lấy các id của danh mục con
//                    $query->orWhere('multi_cat', 'LIKE', '%|' . $category->id . '|%');  // truy vấn các tin thuộc danh mục hiện tại
//                    foreach ($cat_childs as $cat_child) {
//                        $query->orWhere('multi_cat', 'LIKE', '%|' . $cat_child->id . '|%');    //  truy vấn các tin thuộc các danh mục con của danh mục hiện tại
//                    }
//                })->orderBy('order_no', 'desc')->orderBy('updated_at', 'desc')->paginate(12);
//                CommonHelper::putToCache('posts_by_multi_cat_like_category_id' . $category->id . @$_GET['page'], $products);
//            }

//            $data['products'] = $products;

//            return view('themeHandymanservices::pages.service.service')->with($data);
        }
    }

    public function oneParam($slug1)
    {
//        dd($slug1);
        //  Neu la vao cty thi chuyen den trang danh sach san pham cua cong ty
//        $com = Company::where('slug', $slug1)->where('status', 1)->first();
//        if (is_object($com)) {
//            return redirect($slug1 . '/san-pham');
//        }

        //  Chuyen den trang danh sanh tin tuc | Danh sach san pham
        return $this->list($slug1);
    }

    //  VD: hobasoft/card-visit  | danh-muc-cha/danh-muc-con | danh-muc/bai-viet.html | danh-muc/san-pham.html
    public function twoParam($slug1, $slug2)
    {
        if (strpos($slug2, '.html')) {  //  Nếu là trang chi tiết
            $slug2 = str_replace('.html', '', $slug2);
            if (Service::where('slug', $slug2)->where('status', 1)->count() > 0) {
                //  Chi tiet san pham
                return $this->detail($slug2);
            } else {
                //  Chi tiet tin tuc
                $postController = new PostController();
                return $postController->detail($slug2);
            }
            abort(404);
        }

        return $this->list($slug2);
    }

    //  VD: hobasoft/card-visit/card-truyen-thong
    public function threeParam($slug1, $slug2, $slug3)
    {

        if (strpos($slug3, '.html')) {  //  Nếu là trang chi tiết
            $slug3 = str_replace('.html', '', $slug3);
            if (Service::where('slug', $slug3)->where('status', 1)->count() > 0) {        //  Chi tiet san pham
                return $this->detail($slug3);
            } else {        //  Chi tiet tin tuc
                $postController = new PostController();
                return $postController->detail($slug3);
            }
        }

        return $this->list($slug3);
    }

    public function fourParam($slug1, $slug2, $slug3, $slug4)
    {
        if (strpos($slug4, '.html')) {  //  Nếu là trang chi tiết
            $slug4 = str_replace('.html', '', $slug4);
            if (Service::where('slug', $slug4)->where('status', 1)->count() > 0) {        //  Chi tiet san pham
                return $this->detail($slug4);
            } else {        //  Chi tiet tin tuc
                $postController = new PostController();
                return $postController->detail($slug4);
            }
        }

        abort(404);
    }

    //  Trang chi tiết sản phẩm
    public function detail($slug)
    {
        $slug = str_replace('.html', '', $slug);
        $data['product'] = Service::where('slug', $slug)->where('status', 1)->first();

        if (!is_object($data['product'])) {

            abort(404);
        }

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['product']->meta_title != '' ? $data['product']->meta_title : $data['product']->{'name'.$language},
            'meta_description' => $data['product']->meta_description != '' ? $data['product']->meta_description : $data['product']->{'name'.$language},
            'meta_keywords' => $data['product']->meta_keywords != '' ? $data['product']->meta_keywords : $data['product']->{'name'.$language},
        ];
        view()->share('pageOption', $pageOption);

        return view('themesemicolonwebjdes::pages.service.service', $data);
    }





//    function getService()
//    {
//        $data['menu_mains'] = Menus::where('location','main_menu')->get();
//        $data['menu_top'] = Menus::where('location','menu-top')->get();
//        return view('themehandymanservices::pages.service.service',$data);
//    }
//    function getRequest()
//    {
//        $data['menu_mains'] = Menus::where('location','main_menu')->get();
//        $data['menu_top'] = Menus::where('location','menu-top')->get();
//        return view('themehandymanservices::pages.service.request-service',$data);
//    }

    //  Trang danh mục
//    public function list($slug)
//    {
//        $category = Menus::where('url', $slug)->first();
//        if (!is_object($category)) {
//            return $this->detail($slug);
//        }
//        $data['category'] = $category;
//        $data['slug1'] = $slug;
//
//        //  Thẻ meta cho seo
//        $pageOption = [
//            'meta_title' => $category->meta_title != '' ? $category->meta_title : $category->name,
//            'meta_description' => $category->meta_description != '' ? $category->meta_description : $category->name,
//            'meta_keywords' => $category->meta_keywords != '' ? $category->meta_keywords : $category->name,
//        ];
//        view()->share('pageOption', $pageOption);
//
//        if ($category->type==1) {
//            $posts = CommonHelper::getFromCache('posts_by_multi_cat_like_category_id' . $category->id . @$_GET['page']);
//            if (!$posts) {
////                $posts = Post::where('status', 1)->where(function ($query) use ($category) {
////                    $cat_childs = $category->child; //  Lấy các id của danh mục con
////
////                    $query->orWhere('multi_cat', 'LIKE', '%|' . $category->id . '|%');  // truy vấn các tin thuộc danh mục hiện tại
////
////                    foreach ($cat_childs as $cat_child) {
////                        $query->orWhere('multi_cat', 'LIKE', '%|' . $cat_child->id . '|%');    //  truy vấn các tin thuộc các danh mục con của danh mục hiện tại
////                    }
////                })->orderBy('order_no', 'desc')->orderBy('updated_at', 'desc')->paginate(12);
//
//                $posts = Post::where('multi_cat','LIKE', '%|' . $category->id . '|%')->orderBy('order_no', 'desc')->orderBy('id', 'desc')->paginate(5);
//                CommonHelper::putToCache('posts_by_multi_cat_like_category_id' . $category->id . @$_GET['page'], $posts);
//            }
//            $data['posts'] = $posts;
//            return view('themehandymanservices::pages.post.list_post')->with($data);
//
//        }
//
//        if ($category->type==3) {
//            return $this->detailPage($slug);
//
//        }
//    }
//
//    public function oneParam($slug1)
//    {
////        $ser = Service::where('slug', $slug1)->where('status', 1)->first();
////        if (is_object($ser)) {
////            return redirect($slug1);
////        }
//        return $this->list($slug1);
//    }
//
//
//    public function twoParam($slug1, $slug2)
//    {
//        if (strpos($slug2, '.html')) {
//            //  Nếu là trang chi tiết
//            $slug2 = str_replace('.html', '', $slug2);
//            if (Service::where('slug', $slug2)->where('status', 1)->count() > 0) {
//                //  Chi tiet san pham
//                return $this->detail($slug2);
//            }
//            else {
//                //  Chi tiet tin tuc
//                $postController = new PostController();
//                return $postController->detail($slug2);
//            }
//            abort(404);
//        }
//        return $this->list($slug2);
//    }
//
//
//    public function threeParam($slug1, $slug2, $slug3)
//    {
//
//        if (strpos($slug3, '.html')) {  //  Nếu là trang chi tiết
//            $slug3 = str_replace('.html', '', $slug3);
//            if (Service::where('slug', $slug3)->where('status', 1)->count() > 0) {        //  Chi tiet san pham
//                return $this->detail($slug3);
//            } else {        //  Chi tiet tin tuc
//                $postController = new PostController();
//                return $postController->detail($slug3);
//            }
//        }
//
//        return $this->list($slug3);
//    }
//
//    public function fourParam($slug1, $slug2, $slug3, $slug4)
//    {
//        if (strpos($slug4, '.html')) {  //  Nếu là trang chi tiết
//            $slug4 = str_replace('.html', '', $slug4);
//            if (Service::where('slug', $slug4)->where('status', 1)->count() > 0) {        //  Chi tiet san pham
//                return $this->detail($slug4);
//            } else {        //  Chi tiet tin tuc
//                $postController = new PostController();
//                return $postController->detail($slug4);
//            }
//        }
//
//        abort(404);
//    }
//
//    //  Trang chi tiết sản phẩm/ dịch vụ
//    public function detailPage($slug){
//        $data['menu_page'] = Menu::where('url', $slug)->where('status', 1)->first();
//        $data['relates'] = Post::where('multi_cat','like', '%|'.$data['menu_page']->id.'|%')
//            ->where('status',1)->orderBy('order_no', 'DESC')->orderBy('id','DESC')->get();
//
//        if (!is_object($data['menu_page'])) {
//            abort(404);
//        }
//        return view('themehandymanservices::pages.home.introduce', $data);
//    }
//    public function detail($slug)
//    {
//        $slug = str_replace('.html', '', $slug);
//        $data['service'] = Service::where('slug', $slug)->where('status', 1)->first();
//
//        if (!is_object($data['service'])) {
//            abort(404);
//        }
//
//        //  Thẻ meta cho seo
//        $pageOption = [
//            'meta_title' => $data['service']->meta_title != '' ? $data['service']->meta_title : $data['service']->name_vi,
//            'meta_description' => $data['service']->meta_description != '' ? $data['service']->meta_description : $data['service']->name_vi,
//            'meta_keywords' => $data['service']->meta_keywords != '' ? $data['service']->meta_keywords : $data['service']->name_vi,
//        ];
//        view()->share('pageOption', $pageOption);
//
//        return view('themehandymanservices::pages.service.service', $data);
//    }

//    function GetSearch(request $r)
//    {
//        $data['services']=Service::where('name','like','%'.$r->q.'%')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->paginate(12);
//        $data['q']=$r->q;
//        $pageOption = [
//            'meta_title' => 'Tìm kiếm từ khóa: ' . @$r->q,
//            'meta_description' => 'Tìm kiếm từ khóa: ' . @$r->q,
//            'meta_keywords' => 'Tìm kiếm từ khóa: ' . @$r->q,
//        ];
//        view()->share('pageOption', $pageOption);
//
//        return view('themehandymanservices::pages.service.search',$data);
//    }


    function submit_form_service(request $request)
    {
//        $service_field ='';

//        foreach ($request->service_fields as $service_field_id => $value) {
//            $service_field .= "'service_fields['.$service_field_id.']' => 'required',";
//            $service_field .= "'service_fields['.$service_field_id.'].required' => 'Nhập lại sai mật khẩu!',";
//            }
        $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'tel' => 'required',
                'email' => 'required',
                'address' => 'required',
                'date_start' => 'required',
                'time_start' => 'required',
                'time_end' => 'required',

            ]
            , [
                'first_name.required' => 'Bắt buộc phải nhập!',
                'last_name.required' => 'Bắt buộc phải nhập!',
                'tel.required' => 'Bắt buộc phải nhập!',
                'email.required' => 'Bắt buộc phải nhập!',
                'address.required' => 'Bắt buộc phải nhập!',
                'date_start.required' => 'Bắt buộc phải nhập!',
                'time_start.required' => 'Bắt buộc phải nhập!',
                'time_end.required' => 'Bắt buộc phải nhập!',

            ]
        );
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {

            DB::beginTransaction();
            try {

                $user = User::where('tel', trim($request->tel))->first();
                if ($user == Null) {
                    $user = User::create([
                        'name' => $request->first_name . ' ' . $request->last_name,
                        'tel' => $request->tel,
                        'email' => $request->email,
                        'address' => $request->address,
                        'gender' => $request->gender,
                    ]);
                }
                $price = 0;
                $service_fields = Null;
                if (isset($request->service_fields)) {
                    $service_fields = $request->service_fields;
                    foreach ($service_fields as $service_field_id => $value) {
                        $price += $this->getPriceService($service_field_id, $value);
                    }
                }

                $booking = Booking::create([
                    'service_fields' => json_encode($service_fields),
                    'price' => $price,
                    'note' => $request->note,
                    'date_start' => $request->date_start,
                    'time_start' => $request->time_start,
                    'time_end' => $request->time_end,
                    'user_id' => $user->id,
                    'service_id' => $request->service_name,
                ]);

                $admins = new Admin();
                if ($booking->time_start != null) {
                    $admins = $admins->where(function ($query) use ($booking) {
                        $query->orWhere('area1_start', '<=', $booking->time_start)->whereTime('area1_end', '>=', $booking->time_end);
                        // $query->orWhereRaw('area1_start <= ' . $booking->time_start . ' AND area1_end >= ' . $booking->time_end);
                        $query->orWhere('area2_start', '<=', $booking->time_start)->whereTime('area2_end', '>=', $booking->time_end);    //  truy vấn các tin thuộc các danh mục con của danh mục hiện tại
                        $query->orWhere('area3_start', '<=', $booking->time_start)->whereTime('area3_end', '>=', $booking->time_end);    //  truy vấn các tin thuộc các danh mục con của danh mục hiện tại
                    });
                }

                if ($booking->date_start != null) {
                    $day = date('w', strtotime($booking->date_start)); //trả về 1->7 tướng ứng cho thứ 2 ->cn
                    $admins = $admins->where('day_of_week', 'LIKE', '%|'.$day.'|%');

                }
                $admins = $admins->get();
                $info = [];
                foreach ($admins as $v) {
                    $info['email'] = $v->email;
                    $info['name'] = $v->name;
                    $info['booking_id'] = $booking->id;
                    $this->SendMailFullSlot($info);
                }
                Session::put('booking_id', $booking->id);
                DB::commit();
            } catch (\Exception $exception) {
                DB::rollBack();
                CommonHelper::one_time_message('error', $exception->getMessage());
            }

            return redirect()->route('booking-success');
        }
    }
    public function SendMailFullSlot($info)
    {

        $user = (object)[
            'email' => $info['email'],
            'name' => $info['name'],
            'booking_id' => $info['booking_id'],
        ];
        $data = [
            'view' => 'themehandymanservices::mail.email',
            'user' => $user,

            'name' => $this->_mailSetting['mail_name'],
            'subject' => 'Đơn hàng mới!'
        ];
        Mail::to($user)->send(new MailServer($data));
        return true;
    }

    function getPriceService($service_fied_id, $value)
    {
        $price_per_hours = (float)Setting::where('name', 'price_per_hours')->first()->value;
        if ($value > 0 && $price_per_hours > 0) {
            $hours = ServiceFields::find($service_fied_id)->hours * $value;
            return $hours * $price_per_hours;
        } else {
            return 0;
        }

    }
}
