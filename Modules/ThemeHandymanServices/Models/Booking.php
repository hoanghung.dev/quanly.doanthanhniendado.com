<?php

namespace Modules\ThemeHandymanServices\Models;

use Illuminate\Database\Eloquent\Model;
//use Modules\EworkingUser\Models\BillPayment;

class Booking extends Model
{

    protected $table = 'bookings';

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
