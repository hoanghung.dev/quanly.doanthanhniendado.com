<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */

namespace Modules\ThemeHandymanServices\Models ;

use Illuminate\Database\Eloquent\Model;

class Menus extends Model
{

    protected $table = 'menus';

    protected $guarded = [];

    public $timestamps = false;

    public function child()
    {
        return $this->hasMany(Menus::class, 'parent_id', 'id');
    }
}
