<?php
namespace Modules\ThemeHandymanServices\Models;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use Modules\ThemeHandyServices\Models\Company;

class User extends Model
{

    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password', 'tel', 'image', 'address', 'image', 'gender', 'birthday','balance','stk','change_password','api_token','customer_type'
    ];

}
