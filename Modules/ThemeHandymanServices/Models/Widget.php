<?php


namespace Modules\ThemeHandymanServices\Models ;

use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    protected $table = 'widgets';

    protected $fillable = ['name_vi','name_en','name_hu', 'content', 'location'];
}