<!DOCTYPE html>
<html lang="vi" prefix="og: http://ogp.me/ns#" >
      {{--class="loading-site no-js">--}}
<head>
    @include('themehandymanservices::partials.head_meta')
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700|Roboto:300,400,500,600,700">
    @include('themehandymanservices::partials.header_script')
    <style>
        *{
            font-family: Roboto!important;
        }
        i{
            font-family:font-icons !important;
        }
    </style>
</head>

<body class="stretched">

{{--@include('themesemicolonwebjdes::partials.header')--}}
@yield('main_content')
<!-- #wrapper -->
@include('themehandymanservices::partials.footer')
@yield('custom_script')

</body>
</html>
