
@extends('themehandymanservices::layouts.default')
@section('main_content')
    {{--<title>Handyman Services – Just another WordPress site</title>--}}
    <link rel="alternate" type="application/rss+xml" title="Handyman Services » Feed"
          href="https://handyman-services.cmsmasters.net/feed/">
    <link rel="alternate" type="application/rss+xml" title="Handyman Services » Comments Feed"
          href="https://handyman-services.cmsmasters.net/comments/feed/">
    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/",
            "svgExt": ".svg",
            "source": {"concatemoji": "https:\/\/handyman-services.cmsmasters.net\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.5"}
        };
        !function (a, b, c) {
            function d(a, b) {
                var c = String.fromCharCode;
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0);
                var d = k.toDataURL();
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0);
                var e = k.toDataURL();
                return d === e
            }

            function e(a) {
                var b;
                if (!l || !l.fillText) return !1;
                switch (l.textBaseline = "top", l.font = "600 32px Arial", a) {
                    case"flag":
                        return !(b = d([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819])) && (b = d([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]), !b);
                    case"emoji":
                        return b = d([55357, 56424, 55356, 57342, 8205, 55358, 56605, 8205, 55357, 56424, 55356, 57340], [55357, 56424, 55356, 57342, 8203, 55358, 56605, 8203, 55357, 56424, 55356, 57340]), !b
                }
                return !1
            }

            function f(a) {
                var c = b.createElement("script");
                c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
            }

            var g, h, i, j, k = b.createElement("canvas"), l = k.getContext && k.getContext("2d");
            for (j = Array("flag", "emoji"), c.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, i = 0; i < j.length; i++) c.supports[j[i]] = e(j[i]), c.supports.everything = c.supports.everything && c.supports[j[i]], "flag" !== j[i] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[j[i]]);
            c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () {
                c.DOMReady = !0
            }, c.supports.everything || (h = function () {
                c.readyCallback()
            }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onload", h), b.attachEvent("onreadystatechange", function () {
                "complete" === b.readyState && c.readyCallback()
            })), g = c.source || {}, g.concatemoji ? f(g.concatemoji) : g.wpemoji && g.twemoji && (f(g.twemoji), f(g.wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>

    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
        article.cmsmasters_project_puzzle.one_x_one.post-9410.project.type-project.status-publish.format-standard.has-post-thumbnail.hentry.pj-categs-services.shortcode_animated {
            width: 25%;
        }
    </style>
    {{--@include('themehandymanservices::partials.header_script')--}}

    <style id="rs-plugin-settings-inline-css" type="text/css">
        #rs-demo-id {
        }

        #rev_slider_1_1 .uranus.tparrows {
            width: 50px;
            height: 50px;
            background: rgba(255, 255, 255, 0);
        }

        #rev_slider_1_1 .uranus.tparrows:before {
            width: 50px;
            height: 50px;
            line-height: 50px;
            font-size: 40px;
            transition: all 0.3s;
            -webkit-transition: all 0.3s;
        }

        #rev_slider_1_1 .uranus.tparrows:hover:before {
            opacity: 0.75;
        }
    </style>
    <style id="woocommerce-inline-inline-css" type="text/css">
        .woocommerce form .form-row .required {
            visibility: visible;
        }
    </style>

    <style id="handyman-services-style-inline-css" type="text/css">

        html body {
            background-color: #f0f0f0;
            background-image: url(https://handyman-services.cmsmasters.net/wp-content/uploads/2017/06/pattern-bg.jpg);
            background-position: top center;
            background-repeat: repeat;
            background-attachment: scroll;
            background-size: auto;

        }

        .header_mid .header_mid_inner .logo_wrap {
            width: 185px;
        }

        .header_mid_inner .logo img.logo_retina {
            width: 185px;
        }

        .headline_color {
            background-color: rgba(50, 51, 56, 0);
        }

        @media (min-width: 540px) {
            .headline_aligner,
            .cmsmasters_breadcrumbs_aligner {
                min-height: 355px;
            }
        }

        .header_top {
            height: 38px;
        }

        .header_mid {
            height: 100px;
        }

        .header_bot {
            height: 50px;
        }

        #page.cmsmasters_heading_after_header #middle,
        #page.cmsmasters_heading_under_header #middle .headline .headline_outer {
            padding-top: 100px;
        }

        #page.cmsmasters_heading_after_header.enable_header_top #middle,
        #page.cmsmasters_heading_under_header.enable_header_top #middle .headline .headline_outer {
            padding-top: 138px;
        }

        #page.cmsmasters_heading_after_header.enable_header_bottom #middle,
        #page.cmsmasters_heading_under_header.enable_header_bottom #middle .headline .headline_outer {
            padding-top: 150px;
        }

        #page.cmsmasters_heading_after_header.enable_header_top.enable_header_bottom #middle,
        #page.cmsmasters_heading_under_header.enable_header_top.enable_header_bottom #middle .headline .headline_outer {
            padding-top: 188px;
        }

        @media only screen and (max-width: 1024px) {
            .header_top,
            .header_mid,
            .header_bot {
                height: auto;
            }

            .header_mid .header_mid_inner > div {
                height: 100px;
            }

            .header_bot .header_bot_inner > div {
                height: 50px;
            }

            #page.cmsmasters_heading_after_header #middle,
            #page.cmsmasters_heading_under_header #middle .headline .headline_outer,
            #page.cmsmasters_heading_after_header.enable_header_top #middle,
            #page.cmsmasters_heading_under_header.enable_header_top #middle .headline .headline_outer,
            #page.cmsmasters_heading_after_header.enable_header_bottom #middle,
            #page.cmsmasters_heading_under_header.enable_header_bottom #middle .headline .headline_outer,
            #page.cmsmasters_heading_after_header.enable_header_top.enable_header_bottom #middle,
            #page.cmsmasters_heading_under_header.enable_header_top.enable_header_bottom #middle .headline .headline_outer {
                padding-top: 0 !important;
            }
        }

        @media only screen and (max-width: 768px) {
            .header_mid .header_mid_inner > div,
            .header_bot .header_bot_inner > div {
                height: auto;
            }
        }

    </style>

    <style id="handyman-services-retina-inline-css" type="text/css">
        #cmsmasters_row_4b1bd64c9d .cmsmasters_row_outer_parent {
            padding-top: 0px;
        }

        #cmsmasters_row_4b1bd64c9d .cmsmasters_row_outer_parent {
            padding-bottom: 0px;
        }

        #cmsmasters_row_4b1bd64c9d .cmsmasters_row_inner.cmsmasters_row_fullwidth {
            padding-left: 0%;
        }

        #cmsmasters_row_4b1bd64c9d .cmsmasters_row_inner.cmsmasters_row_fullwidth {
            padding-right: 0%;
        }

        #rev_slider_1_1 .uranus.tparrows {
            width: 50px;
            height: 50px;
            background: rgba(255, 255, 255, 0)
        }

        #rev_slider_1_1 .uranus.tparrows:before {
            width: 50px;
            height: 50px;
            line-height: 50px;
            font-size: 40px;
            transition: all 0.3s;
            -webkit-transition: all 0.3s
        }

        #rev_slider_1_1 .uranus.tparrows:hover:before {
            opacity: 0.75
        }

        #cmsmasters_row_a65928170a {
            background-image: url(https://handyman-services.cmsmasters.net/wp-content/uploads/2017/06/2-2.jpg);
            background-position: top center;
            background-repeat: no-repeat;
            background-attachment: scroll;
            background-size: cover;
        }

        #cmsmasters_row_a65928170a .cmsmasters_row_outer_parent {
            padding-top: 70px;
        }

        #cmsmasters_row_a65928170a .cmsmasters_row_outer_parent {
            padding-bottom: 30px;
        }

        #cmsmasters_row_b9623bbfe8 .cmsmasters_row_outer_parent {
            padding-top: 40px;
        }

        #cmsmasters_row_b9623bbfe8 .cmsmasters_row_outer_parent {
            padding-bottom: 20px;
        }

        #cmsmasters_counters_7954cdd491 .cmsmasters_counter.counter_has_icon .cmsmasters_counter_inner,
        #cmsmasters_counters_7954cdd491 .cmsmasters_counter.counter_has_icon .cmsmasters_counter_inner, #cmsmasters_counters_7954cdd491 .cmsmasters_counter.counter_has_image .cmsmasters_counter_inner {
            padding-top: 70px;
        }

        #cmsmasters_counters_7954cdd491.counters_type_vertical .cmsmasters_counter .cmsmasters_counter_inner:before {
            margin-left: -30px;
        }

        #cmsmasters_counters_7954cdd491.counters_type_horizontal .cmsmasters_counter .cmsmasters_counter_inner .cmsmasters_counter_counter_wrap {
            line-height: 60px;
        }

        #cmsmasters_counters_7954cdd491 .cmsmasters_counter .cmsmasters_counter_inner:before {
            font-size: 30px;
            line-height: 60px;
            width: 60px;
            height: 60px;
            border-width: 0px;
            -webkit-border-radius: 50%;
            border-radius: 50%;
        }

        #cmsmasters_counter_ce36330b93 .cmsmasters_counter_inner:before {
        }

        #cmsmasters_counter_ce36330b93 .cmsmasters_counter_inner .cmsmasters_counter_counter_wrap span {
            font-size: 46px;
            line-height: 14px;
        }

        #cmsmasters_counter_3235d25751 .cmsmasters_counter_inner:before {
        }

        #cmsmasters_counter_3235d25751 .cmsmasters_counter_inner .cmsmasters_counter_counter_wrap span {
            font-size: 46px;
            line-height: 14px;
        }

        #cmsmasters_counter_4dd2b49f21 .cmsmasters_counter_inner:before {
        }

        #cmsmasters_counter_4dd2b49f21 .cmsmasters_counter_inner .cmsmasters_counter_counter_wrap span {
            font-size: 46px;
            line-height: 14px;
        }

        #cmsmasters_counter_360a2bf428 .cmsmasters_counter_inner:before {
        }

        #cmsmasters_counter_360a2bf428 .cmsmasters_counter_inner .cmsmasters_counter_counter_wrap span {
            font-size: 46px;
            line-height: 14px;
        }

        #cmsmasters_counter_125c4ff5dc .cmsmasters_counter_inner:before {
        }

        #cmsmasters_counter_125c4ff5dc .cmsmasters_counter_inner .cmsmasters_counter_counter_wrap span {
            font-size: 46px;
            line-height: 14px;
        }

        #cmsmasters_row_afbb8e23aa .cmsmasters_row_outer_parent {
            padding-top: 0px;
        }

        #cmsmasters_row_afbb8e23aa .cmsmasters_row_outer_parent {
            padding-bottom: 0px;
        }

        #cmsmasters_row_afbb8e23aa .cmsmasters_row_inner.cmsmasters_row_fullwidth {
            padding-left: 0%;
        }

        #cmsmasters_row_afbb8e23aa .cmsmasters_row_inner.cmsmasters_row_fullwidth {
            padding-right: 0%;
        }

        #cmsmasters_divider_57eef94424 {
            border-bottom-width: 2px;
            border-bottom-style: solid;
            margin-top: 0px;
            margin-bottom: 0px;
        }

        #cmsmasters_row_cb9cb1b953 {
            background-image: url(https://handyman-services.cmsmasters.net/wp-content/uploads/2015/11/3.jpg);
            background-position: top center;
            background-repeat: no-repeat;
            background-attachment: scroll;
            background-size: cover;
        }

        #cmsmasters_row_cb9cb1b953 .cmsmasters_row_outer_parent {
            padding-top: 60px;
        }

        #cmsmasters_row_cb9cb1b953 .cmsmasters_row_outer_parent {
            padding-bottom: 60px;
        }

        #cmsmasters_heading_d468f62a91 {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 0px;
        }

        #cmsmasters_heading_d468f62a91 .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_d468f62a91 .cmsmasters_heading, #cmsmasters_heading_d468f62a91 .cmsmasters_heading a {
            font-weight: 300;
        }

        #cmsmasters_heading_d468f62a91 .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_d468f62a91 .cmsmasters_heading_divider {
        }

        #cmsmasters_heading_3e8764f5d7 {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 10px;
        }

        #cmsmasters_heading_3e8764f5d7 .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_3e8764f5d7 .cmsmasters_heading, #cmsmasters_heading_3e8764f5d7 .cmsmasters_heading a {
            font-size: 32px;
            line-height: 36px;
        }

        #cmsmasters_heading_3e8764f5d7 .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_3e8764f5d7 .cmsmasters_heading_divider {
        }

        #cmsmasters_divider_3455575720 {
            border-bottom-width: 4px;
            border-bottom-style: solid;
            margin-top: 10px;
            margin-bottom: 10px;
            border-bottom-color: #f2a61f;
        }

        #cmsmasters_divider_41ca65e005 {
            border-bottom-width: 0px;
            border-bottom-style: solid;
            margin-top: 25px;
            margin-bottom: 0px;
        }

        #cmsmasters_counters_3662d27ee4 .cmsmasters_counter.counter_has_icon .cmsmasters_counter_inner,
        #cmsmasters_counters_3662d27ee4 .cmsmasters_counter.counter_has_icon .cmsmasters_counter_inner, #cmsmasters_counters_3662d27ee4 .cmsmasters_counter.counter_has_image .cmsmasters_counter_inner {
            padding-left: 70px;
        }

        #cmsmasters_counters_3662d27ee4.counters_type_vertical .cmsmasters_counter .cmsmasters_counter_inner:before {
            margin-left: -30px;
        }

        #cmsmasters_counters_3662d27ee4.counters_type_horizontal .cmsmasters_counter .cmsmasters_counter_inner .cmsmasters_counter_counter_wrap {
            line-height: 60px;
        }

        #cmsmasters_counters_3662d27ee4 .cmsmasters_counter .cmsmasters_counter_inner:before {
            font-size: 30px;
            line-height: 60px;
            width: 60px;
            height: 60px;
            border-width: 0px;
            -webkit-border-radius: 50%;
            border-radius: 50%;
        }

        #cmsmasters_counter_8d9d8f4762 .cmsmasters_counter_inner:before {
        }

        #cmsmasters_counter_8d9d8f4762 .cmsmasters_counter_inner .cmsmasters_counter_counter_wrap span {
            font-size: 100px;
            line-height: 22px;
        }

        #cmsmasters_icon_box_87e10bd928 {
            border-width: 0px;
        }

        #cmsmasters_icon_box_87e10bd928:before,
        #cmsmasters_icon_box_87e10bd928 .icon_box_heading:before {
            font-size: 30px;
            line-height: 25px;
            width: 25px;
            height: 25px;
            border-width: 0px;
            -webkit-border-radius: 50%;
            border-radius: 50%;
            color: #005c8c;
            background-color: rgba(255, 255, 255, 0);
        }

        #cmsmasters_icon_box_87e10bd928.cmsmasters_icon_box_left,
        #cmsmasters_icon_box_87e10bd928.cmsmasters_icon_box_left_top {
            padding-left: 42.5px;
            margin-left: 12.5px;
        }

        #cmsmasters_icon_box_87e10bd928.cmsmasters_icon_box_left:before,
        #cmsmasters_icon_box_87e10bd928.cmsmasters_icon_box_left_top:before {
            left: -12.5px;
        }

        #cmsmasters_icon_box_b554bd284f {
            border-width: 0px;
        }

        #cmsmasters_icon_box_b554bd284f:before,
        #cmsmasters_icon_box_b554bd284f .icon_box_heading:before {
            font-size: 30px;
            line-height: 25px;
            width: 25px;
            height: 25px;
            border-width: 0px;
            -webkit-border-radius: 50%;
            border-radius: 50%;
            color: #005c8c;
            background-color: rgba(255, 255, 255, 0);
        }

        #cmsmasters_icon_box_b554bd284f.cmsmasters_icon_box_left,
        #cmsmasters_icon_box_b554bd284f.cmsmasters_icon_box_left_top {
            padding-left: 42.5px;
            margin-left: 12.5px;
        }

        #cmsmasters_icon_box_b554bd284f.cmsmasters_icon_box_left:before,
        #cmsmasters_icon_box_b554bd284f.cmsmasters_icon_box_left_top:before {
            left: -12.5px;
        }

        #cmsmasters_divider_08a2236f70 {
            border-bottom-width: 0px;
            border-bottom-style: solid;
            margin-top: 80px;
            margin-bottom: 0px;
        }

        #cmsmasters_icon_box_1758a13b8d {
            border-width: 0px;
        }

        #cmsmasters_icon_box_1758a13b8d:before,
        #cmsmasters_icon_box_1758a13b8d .icon_box_heading:before {
            font-size: 30px;
            line-height: 25px;
            width: 25px;
            height: 25px;
            border-width: 0px;
            -webkit-border-radius: 50%;
            border-radius: 50%;
            color: #005c8c;
            background-color: rgba(255, 255, 255, 0);
        }

        #cmsmasters_icon_box_1758a13b8d.cmsmasters_icon_box_left,
        #cmsmasters_icon_box_1758a13b8d.cmsmasters_icon_box_left_top {
            padding-left: 42.5px;
            margin-left: 12.5px;
        }

        #cmsmasters_icon_box_1758a13b8d.cmsmasters_icon_box_left:before,
        #cmsmasters_icon_box_1758a13b8d.cmsmasters_icon_box_left_top:before {
            left: -12.5px;
        }

        #cmsmasters_icon_box_2a1ee0843e {
            border-width: 0px;
        }

        #cmsmasters_icon_box_2a1ee0843e:before,
        #cmsmasters_icon_box_2a1ee0843e .icon_box_heading:before {
            font-size: 30px;
            line-height: 25px;
            width: 25px;
            height: 25px;
            border-width: 0px;
            -webkit-border-radius: 50%;
            border-radius: 50%;
            color: #005c8c;
            background-color: rgba(255, 255, 255, 0);
        }

        #cmsmasters_icon_box_2a1ee0843e.cmsmasters_icon_box_left,
        #cmsmasters_icon_box_2a1ee0843e.cmsmasters_icon_box_left_top {
            padding-left: 42.5px;
            margin-left: 12.5px;
        }

        #cmsmasters_icon_box_2a1ee0843e.cmsmasters_icon_box_left:before,
        #cmsmasters_icon_box_2a1ee0843e.cmsmasters_icon_box_left_top:before {
            left: -12.5px;
        }

        #cmsmasters_icon_box_a441d688c6 {
            border-width: 0px;
        }

        #cmsmasters_icon_box_a441d688c6:before,
        #cmsmasters_icon_box_a441d688c6 .icon_box_heading:before {
            font-size: 30px;
            line-height: 25px;
            width: 25px;
            height: 25px;
            border-width: 0px;
            -webkit-border-radius: 50%;
            border-radius: 50%;
            color: #005c8c;
            background-color: rgba(255, 255, 255, 0);
        }

        #cmsmasters_icon_box_a441d688c6.cmsmasters_icon_box_left,
        #cmsmasters_icon_box_a441d688c6.cmsmasters_icon_box_left_top {
            padding-left: 42.5px;
            margin-left: 12.5px;
        }

        #cmsmasters_icon_box_a441d688c6.cmsmasters_icon_box_left:before,
        #cmsmasters_icon_box_a441d688c6.cmsmasters_icon_box_left_top:before {
            left: -12.5px;
        }

        #cmsmasters_fb_667dcb9aeb {
            padding-top: 0px;
            padding-bottom: 0px;
            background-color: #005c8c;
        }

        #cmsmasters_fb_667dcb9aeb .featured_block_inner {
            width: 100%;
            padding: 20px 20px 25px 20px;
            text-align: left;
            margin: 0 auto;
        }

        #cmsmasters_fb_667dcb9aeb .featured_block_text {
            text-align: left;
        }

        #cmsmasters_heading_4eb2a76563 {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 20px;
        }

        #cmsmasters_heading_4eb2a76563 .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_4eb2a76563 .cmsmasters_heading, #cmsmasters_heading_4eb2a76563 .cmsmasters_heading a {
            color: #ffba13;
        }

        #cmsmasters_heading_4eb2a76563 .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_4eb2a76563 .cmsmasters_heading_divider {
        }

        @media only screen and (max-width: 1024px) {
            .cmsmasters_contact_form .one_half {
                width: 96%;
            }
        }

        #cmsmasters_row_551947a170 {
            background-image: url(https://handyman-services.cmsmasters.net/wp-content/uploads/2017/06/10.jpg);
            background-position: top center;
            background-repeat: no-repeat;
            background-attachment: scroll;
            background-size: cover;
        }

        #cmsmasters_row_551947a170 .cmsmasters_row_outer_parent {
            padding-top: 40px;
        }

        #cmsmasters_row_551947a170 .cmsmasters_row_outer_parent {
            padding-bottom: 40px;
        }

        #cmsmasters_heading_8117f6fd8e {
            text-align: left;
            margin-top: 10px;
            margin-bottom: 0px;
        }

        #cmsmasters_heading_8117f6fd8e .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_8117f6fd8e .cmsmasters_heading, #cmsmasters_heading_8117f6fd8e .cmsmasters_heading a {
            font-size: 26px;
            color: #0e5377;
        }

        #cmsmasters_heading_8117f6fd8e .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_8117f6fd8e .cmsmasters_heading_divider {
        }

        @media only screen and (max-width: 1024px) {
            #cmsmasters_column_3b8441270f .cmsmasters_column_inner {
                padding: 15px 0 0 0;
            }
        }

        @media only screen and (max-width: 768px) {
            #cmsmasters_column_3b8441270f .cmsmasters_column_inner {
                padding: 0 0 0 0;
            }
        }

        #cmsmasters_button_c648f13c59 {

            text-align: center;

        }

        #cmsmasters_button_c648f13c59 .cmsmasters_button:before {
            margin-right: .5em;
            margin-left: 0;
            vertical-align: baseline;
        }

        #cmsmasters_button_c648f13c59 .cmsmasters_button {
            font-size: 15px;
            line-height: 48px;
            padding-right: 35px;
            padding-left: 35px;
            border-width: 1px;
            border-style: solid;
            background-color: #005c8c;
            color: #ffba13;
            border-color: #005c8c;
        }

        #cmsmasters_button_c648f13c59 .cmsmasters_button:hover {
            background-color: #2f2f2f;
            color: #ffffff;
            border-color: #2f2f2f;
        }

        #cmsmasters_row_42c2d2e5fd {
            background-image: url(https://handyman-services.cmsmasters.net/wp-content/uploads/2017/06/4-2.jpg);
            background-position: top center;
            background-repeat: no-repeat;
            background-attachment: scroll;
            background-size: cover;
        }

        #cmsmasters_row_42c2d2e5fd .cmsmasters_row_outer_parent {
            padding-top: 60px;
        }

        #cmsmasters_row_42c2d2e5fd .cmsmasters_row_outer_parent {
            padding-bottom: 75px;
        }

        #cmsmasters_heading_a5c0f401f3 {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 0px;
        }

        #cmsmasters_heading_a5c0f401f3 .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_a5c0f401f3 .cmsmasters_heading, #cmsmasters_heading_a5c0f401f3 .cmsmasters_heading a {
            font-weight: 300;
            color: #ffffff;
        }

        #cmsmasters_heading_a5c0f401f3 .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_a5c0f401f3 .cmsmasters_heading_divider {
        }

        #cmsmasters_heading_63716dfe04 {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 10px;
        }

        #cmsmasters_heading_63716dfe04 .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_63716dfe04 .cmsmasters_heading, #cmsmasters_heading_63716dfe04 .cmsmasters_heading a {
            font-size: 32px;
            line-height: 36px;
            color: #ffffff;
        }

        #cmsmasters_heading_63716dfe04 .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_63716dfe04 .cmsmasters_heading_divider {
        }

        #cmsmasters_divider_b699f9adb0 {
            border-bottom-width: 4px;
            border-bottom-style: solid;
            margin-top: 10px;
            margin-bottom: 15px;
            border-bottom-color: #f2a61f;
        }

        #cmsmasters_quote_5c2cb79ab1db8 .wrap_quote_title {
            background-color: #1c1c1c;
        }

        #cmsmasters_heading_ny1d5xvp3b {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 20px;
        }

        #cmsmasters_heading_ny1d5xvp3b .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_ny1d5xvp3b .cmsmasters_heading, #cmsmasters_heading_ny1d5xvp3b .cmsmasters_heading a {
            color: #ffba13;
        }

        #cmsmasters_heading_ny1d5xvp3b .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_ny1d5xvp3b .cmsmasters_heading_divider {
        }

        #cmsmasters_quote_5c2cb79ab244f .wrap_quote_title {
            background-color: #1c1c1c;
        }

        #cmsmasters_heading_bc5crjy22b {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 20px;
        }

        #cmsmasters_heading_bc5crjy22b .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_bc5crjy22b .cmsmasters_heading, #cmsmasters_heading_bc5crjy22b .cmsmasters_heading a {
            color: #ffba13;
        }

        #cmsmasters_heading_bc5crjy22b .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_bc5crjy22b .cmsmasters_heading_divider {
        }

        #cmsmasters_quote_5c2cb79ab2a8e .wrap_quote_title {
            background-color: #1c1c1c;
        }

        #cmsmasters_heading_prcr7npwdn {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 20px;
        }

        #cmsmasters_heading_prcr7npwdn .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_prcr7npwdn .cmsmasters_heading, #cmsmasters_heading_prcr7npwdn .cmsmasters_heading a {
            color: #ffba13;
        }

        #cmsmasters_heading_prcr7npwdn .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_prcr7npwdn .cmsmasters_heading_divider {
        }

        #cmsmasters_row_d3b8be021b .cmsmasters_row_outer_parent {
            padding-top: 60px;
        }

        #cmsmasters_row_d3b8be021b .cmsmasters_row_outer_parent {
            padding-bottom: 30px;
        }

        #cmsmasters_heading_da29b06a7c {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 4px;
        }

        #cmsmasters_heading_da29b06a7c .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_da29b06a7c .cmsmasters_heading, #cmsmasters_heading_da29b06a7c .cmsmasters_heading a {
            font-weight: 300;
        }

        #cmsmasters_heading_da29b06a7c .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_da29b06a7c .cmsmasters_heading_divider {
        }

        #cmsmasters_heading_405ba7c852 {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 10px;
        }

        #cmsmasters_heading_405ba7c852 .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_405ba7c852 .cmsmasters_heading, #cmsmasters_heading_405ba7c852 .cmsmasters_heading a {
            font-size: 32px;
        }

        #cmsmasters_heading_405ba7c852 .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_405ba7c852 .cmsmasters_heading_divider {
        }

        #cmsmasters_divider_d7da0ee4ee {
            border-bottom-width: 4px;
            border-bottom-style: solid;
            margin-top: 10px;
            margin-bottom: 10px;
            border-bottom-color: #f2a61f;
        }

        #cmsmasters_row_164c104c98 .cmsmasters_row_outer_parent {
            padding-top: 0px;
        }

        #cmsmasters_row_164c104c98 .cmsmasters_row_outer_parent {
            padding-bottom: 10px;
        }

        #cmsmasters_divider_8705e5b889 {
            border-bottom-width: 0px;
            border-bottom-style: solid;
            margin-top: 0px;
            margin-bottom: 0px;
        }

        .cmsmasters_custom_list ul li h5 {
            display: inline-block;
            margin-bottom: 0;
        }

        #cmsmasters_divider_af7f7ef255 {
            border-bottom-width: 0px;
            border-bottom-style: solid;
            margin-top: 5px;
            margin-bottom: 0px;
        }

        #cmsmasters_button_2c1f8a4577 {
            float: left;
            text-align: left;

        }

        #cmsmasters_button_2c1f8a4577 .cmsmasters_button:before {
            margin-right: .5em;
            margin-left: 0;
            vertical-align: baseline;
        }

        #cmsmasters_button_2c1f8a4577 .cmsmasters_button {
            font-size: 14px;
            padding-right: 20px;
            padding-left: 20px;
        }

        #cmsmasters_button_2c1f8a4577 .cmsmasters_button:hover {
        }

        #cmsmasters_divider_d08226b247 {
            border-bottom-width: 0px;
            border-bottom-style: solid;
            margin-top: 0px;
            margin-bottom: -80px;
        }

        #cmsmasters_row_7742d5464f .cmsmasters_row_outer_parent {
            padding-top: 0px;
        }

        #cmsmasters_row_7742d5464f .cmsmasters_row_outer_parent {
            padding-bottom: 0px;
        }

        #cmsmasters_row_7742d5464f .cmsmasters_row_inner.cmsmasters_row_fullwidth {
            padding-left: 0%;
        }

        #cmsmasters_row_7742d5464f .cmsmasters_row_inner.cmsmasters_row_fullwidth {
            padding-right: 0%;
        }

        #cmsmasters_divider_0b6ab0a9af {
            border-bottom-width: 2px;
            border-bottom-style: solid;
            margin-top: 0px;
            margin-bottom: 0px;
        }

        #cmsmasters_row_ac55a42ddf {
            background-image: url(https://handyman-services.cmsmasters.net/wp-content/uploads/2015/11/pattern-bg-1.jpg);
            background-position: top center;
            background-repeat: repeat;
            background-attachment: fixed;
            background-size: auto;
            position: static;
        }

        #cmsmasters_row_ac55a42ddf .cmsmasters_row_outer_parent {
            padding-top: 65px;
        }

        #cmsmasters_row_ac55a42ddf .cmsmasters_row_outer_parent {
            padding-bottom: 65px;
        }

        #cmsmasters_heading_23d9d51d05 {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 10px;
        }

        #cmsmasters_heading_23d9d51d05 .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_23d9d51d05 .cmsmasters_heading, #cmsmasters_heading_23d9d51d05 .cmsmasters_heading a {
            font-weight: 300;
        }

        #cmsmasters_heading_23d9d51d05 .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_23d9d51d05 .cmsmasters_heading_divider {
        }

        #cmsmasters_heading_ad206bf0f0 {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 35px;
        }

        #cmsmasters_heading_ad206bf0f0 .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_ad206bf0f0 .cmsmasters_heading, #cmsmasters_heading_ad206bf0f0 .cmsmasters_heading a {
            font-size: 26px;
        }

        #cmsmasters_heading_ad206bf0f0 .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_ad206bf0f0 .cmsmasters_heading_divider {
        }

        #cmsmasters_heading_ea14a44fbf {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 10px;
        }

        #cmsmasters_heading_ea14a44fbf .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_ea14a44fbf .cmsmasters_heading, #cmsmasters_heading_ea14a44fbf .cmsmasters_heading a {
            font-weight: 300;
        }

        #cmsmasters_heading_ea14a44fbf .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_ea14a44fbf .cmsmasters_heading_divider {
        }

        #cmsmasters_heading_89ab935fec {
            text-align: left;
            margin-top: 0px;
            margin-bottom: -45px;
        }

        #cmsmasters_heading_89ab935fec .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_89ab935fec .cmsmasters_heading, #cmsmasters_heading_89ab935fec .cmsmasters_heading a {
            font-size: 26px;
        }

        #cmsmasters_heading_89ab935fec .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_89ab935fec .cmsmasters_heading_divider {
        }

        #cmsmasters_divider_bf0ae3b3d7 {
            border-bottom-width: 0px;
            border-bottom-style: solid;
            margin-top: 50px;
            margin-bottom: 50px;
        }

        #cmsmasters_fb_97f43555e1 {
            padding-top: 0px;
            padding-bottom: 0px;
            background-image: url(https://handyman-services.cmsmasters.net/wp-content/uploads/2017/06/8.jpg);
            background-position: top center;
            background-repeat: no-repeat;
            background-attachment: scroll;
            background-size: cover;
        }

        #cmsmasters_fb_97f43555e1 .featured_block_inner {
            width: 100%;
            padding: 45px 15px 25px 35px;
            text-align: left;
            float: left;
        }

        #cmsmasters_fb_97f43555e1 .featured_block_text {
            text-align: left;
        }

        #cmsmasters_divider_8fb3111758 {
            border-bottom-width: 0px;
            border-bottom-style: solid;
            margin-top: 10px;
            margin-bottom: 0px;
        }

        #cmsmasters_button_10b2a1686f {
            float: left;
            text-align: left;

        }

        #cmsmasters_button_10b2a1686f .cmsmasters_button:before {
            margin-right: .5em;
            margin-left: 0;
            vertical-align: baseline;
        }

        #cmsmasters_button_10b2a1686f .cmsmasters_button {
            font-size: 16px;
            padding-right: 25px;
            padding-left: 25px;
            background-color: #ffba13;
            color: #1c1c1c;
            border-color: #ffba13;
        }

        #cmsmasters_button_10b2a1686f .cmsmasters_button:hover {
            background-color: #1c1c1c;
            color: #ffba13;
            border-color: #1c1c1c;
        }

        #cmsmasters_row_2561544d4b .cmsmasters_row_outer_parent {
            padding-top: 0px;
        }

        #cmsmasters_row_2561544d4b .cmsmasters_row_outer_parent {
            padding-bottom: 0px;
        }

        #cmsmasters_row_2561544d4b .cmsmasters_row_inner.cmsmasters_row_fullwidth {
            padding-left: 0%;
        }

        #cmsmasters_row_2561544d4b .cmsmasters_row_inner.cmsmasters_row_fullwidth {
            padding-right: 0%;
        }

        #cmsmasters_divider_e15335be0b {
            border-bottom-width: 2px;
            border-bottom-style: solid;
            margin-top: 0px;
            margin-bottom: 0px;
        }

        #cmsmasters_row_c1adc8917c .cmsmasters_row_outer_parent {
            padding-top: 50px;
        }

        #cmsmasters_row_c1adc8917c .cmsmasters_row_outer_parent {
            padding-bottom: 0px;
        }

        #cmsmasters_heading_94c0723f71 {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 0px;
        }

        #cmsmasters_heading_94c0723f71 .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_94c0723f71 .cmsmasters_heading, #cmsmasters_heading_94c0723f71 .cmsmasters_heading a {
            font-weight: 300;
        }

        #cmsmasters_heading_94c0723f71 .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_94c0723f71 .cmsmasters_heading_divider {
        }

        #cmsmasters_row_ce94bc5c7f .cmsmasters_row_outer_parent {
            padding-top: 0px;
        }

        #cmsmasters_row_ce94bc5c7f .cmsmasters_row_outer_parent {
            padding-bottom: 20px;
        }

        #cmsmasters_row_ce94bc5c7f .cmsmasters_row_inner.cmsmasters_row_fullwidth {
            padding-left: 5%;
        }

        #cmsmasters_row_ce94bc5c7f .cmsmasters_row_inner.cmsmasters_row_fullwidth {
            padding-right: 5%;
        }

        #cmsmasters_clients_a5961789bc .cmsmasters_clients_item {
            height: 90px;
            line-height: 90px;
        }

        #cmsmasters_clients_a5961789bc .cmsmasters_clients_item a {
            line-height: 90px;
        }

        #cmsmasters_row_e0bf4882c5 {
            background-color: #005c8c;
        }

        #cmsmasters_row_e0bf4882c5 .cmsmasters_row_outer_parent {
            padding-top: 30px;
        }

        #cmsmasters_row_e0bf4882c5 .cmsmasters_row_outer_parent {
            padding-bottom: 30px;
        }

        #cmsmasters_heading_b6e289653c {
            text-align: left;
            margin-top: 10px;
            margin-bottom: 0px;
        }

        #cmsmasters_heading_b6e289653c .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_b6e289653c .cmsmasters_heading, #cmsmasters_heading_b6e289653c .cmsmasters_heading a {
            font-size: 24px;
            line-height: 26px;
            color: #ffffff;
        }

        #cmsmasters_heading_b6e289653c .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_b6e289653c .cmsmasters_heading_divider {
        }

        @media only screen and (max-width: 1024px) {
            #cmsmasters_column_8d8e07f595 .cmsmasters_column_inner {
                padding: 15px 0 0 0;
            }
        }

        @media only screen and (max-width: 768px) {
            #cmsmasters_column_8d8e07f595 .cmsmasters_column_inner {
                padding: 0 0 0 0;
            }
        }

        #cmsmasters_button_4045fed7c3 {

            text-align: center;

        }

        #cmsmasters_button_4045fed7c3 .cmsmasters_button:before {
            margin-right: .5em;
            margin-left: 0;
            vertical-align: baseline;
        }

        #cmsmasters_button_4045fed7c3 .cmsmasters_button {
            font-size: 15px;
            line-height: 48px;
            padding-right: 35px;
            padding-left: 35px;
            border-width: 1px;
            border-style: solid;
            background-color: #ffba13;
            color: #1c1c1c;
            border-color: #ffba13;
        }

        #cmsmasters_button_4045fed7c3 .cmsmasters_button:hover {
            background-color: #ffffff;
            color: #005c8c;
            border-color: #ffffff;
        }


    </style>

    <script type="text/javascript">
        /* <![CDATA[ */
        var LS_Meta = {"v": "6.7.6"};
        /* ]]> */
    </script>

    <meta name="generator"
          content="Powered by LayerSlider 6.7.6 - Multi-Purpose, Responsive, Parallax, Mobile-Friendly Slider Plugin for WordPress.">
    <!-- LayerSlider updates and docs at: https://layerslider.kreaturamedia.com -->
    <link rel="https://api.w.org/" href="https://handyman-services.cmsmasters.net/wp-json/">
    <link rel="EditURI" type="application/rsd+xml" title="RSD"
          href="https://handyman-services.cmsmasters.net/xmlrpc.php?rsd">
    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
          href="https://handyman-services.cmsmasters.net/wp-includes/wlwmanifest.xml">
    <meta name="generator" content="WordPress 5.2.5">
    <meta name="generator" content="WooCommerce 3.6.4">
    <link rel="canonical" href="https://handyman-services.cmsmasters.net/">
    <link rel="shortlink" href="https://handyman-services.cmsmasters.net/">
    <link rel="alternate" type="application/json+oembed"
          href="https://handyman-services.cmsmasters.net/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fhandyman-services.cmsmasters.net%2F">
    <link rel="alternate" type="text/xml+oembed"
          href="https://handyman-services.cmsmasters.net/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fhandyman-services.cmsmasters.net%2F&amp;format=xml">
    <noscript>
        <style>.woocommerce-product-gallery {
                opacity: 1 !important;
            }</style>
    </noscript>
    <script type="text/javascript">
        var cli_flush_cache = 2;
    </script>
    <meta name="generator"
          content="Powered by Slider Revolution 5.4.8.3 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface.">
    <link rel="icon"
          href="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-32x32.png"
          sizes="32x32">
    <link rel="icon"
          href="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-192x192.png"
          sizes="192x192">
    <link rel="apple-touch-icon-precomposed"
          href="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-180x180.png">
    <meta name="msapplication-TileImage"
          content="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-270x270.png">
    <script type="text/javascript">function setREVStartSize(e) {
            try {
                e.c = jQuery(e.c);
                var i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
                if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                    f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                    var u = (e.c.width(), jQuery(window).height());
                    if (void 0 != e.fullScreenOffsetContainer) {
                        var c = e.fullScreenOffsetContainer.split(",");
                        if (c) jQuery.each(c, function (e, i) {
                            u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                        }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                    }
                    f = u
                } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                e.c.closest(".rev_slider_wrapper").css({height: f})
            } catch (d) {
                console.log("Failure at Presize of Slider:" + d)
            }
        };</script>
    </head>
    <div id="screen-shader" style="
            transition: opacity 0.1s ease 0s;
            z-index: 2147483647;
            margin: 0;
            border-radius: 0;
            padding: 0;
            background: #111111;
            pointer-events: none;
            position: fixed;
            top: -10%;
            right: -10%;
            width: 120%;
            height: 120%;
            opacity: 0.6000;
            mix-blend-mode: multiply;
            display: none;
        "></div>
    <body data-rsssl="1" class="home page-template-default page page-id-7366 woocommerce-js">

    <div class="cmsmasters_header_search_form">
        <span class="cmsmasters_header_search_form_close"></span>
        <form method="get" action="https://handyman-services.cmsmasters.net/">
            <div class="cmsmasters_header_search_form_field">
                <input type="search" name="s" placeholder="Enter Keywords" value="">
                <button type="submit" class="cmsmasters_theme_icon_search"></button>
            </div>
        </form>
    </div>
    <!--  Start Page  -->
    <div id="page"
         class="csstransition chrome_only cmsmasters_boxed fixed_header enable_header_top cmsmasters_heading_after_header hfeed site">

        <!--  Start Main  -->
        <div id="main">

            <!--  Start Header  -->
        @include('themehandymanservices::template.menu')
        <!--  Finish Header  -->


            <!--  Start Middle  -->
            <div id="middle">
                <div class="headline cmsmasters_color_scheme_default">
                    <div class="headline_outer">
                        <div class="headline_color"></div>
                    </div>
                </div>
                <div class="middle_inner">
                    <div class="content_wrap fullwidth">

                        <!-- Start Content  -->
                        <div class="middle_content entry"></div>
                    </div>
                    <div id="cmsmasters_row_4b1bd64c9d"
                         class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_fullwidth">
                        <div class="cmsmasters_row_outer_parent">
                            <div class="cmsmasters_row_outer">
                                <div class="cmsmasters_row_inner cmsmasters_row_fullwidth">
                                    <div class="cmsmasters_row_margin cmsmasters_11">
                                        <div id="cmsmasters_column_cd3bc69ed5" class="cmsmasters_column one_first">
                                            <div class="cmsmasters_column_inner">
                                                <div class="cmsmasters_slider">
                                                    <link href="{{ URL::asset('public/frontend/themes/handyman-services/css/css(1)') }}"
                                                          rel="stylesheet" property="stylesheet" type="text/css"
                                                          media="all">
                                                    <div id="rev_slider_1_1_wrapper"
                                                         class="rev_slider_wrapper fullwidthbanner-container"
                                                         data-source="gallery"
                                                         style="margin: 0px auto; background: transparent; padding: 0px; overflow: visible;">
                                                        <!-- START REVOLUTION SLIDER 5.4.8.3 auto mode -->
                                                        <link rel="stylesheet"
                                                              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
                                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                                                        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
                                                        <style>

                                                            .style-caption {
                                                                margin-bottom: 20px;
                                                            }

                                                            .style-caption > button {
                                                                color: #000;
                                                                text-transform: uppercase;
                                                                font-family: 'Titillium Web', Arial, Helvetica, 'Nimbus Sans L', sans-serif;
                                                                font-size: 16px;
                                                            }

                                                            .style-caption > button:hover {
                                                                color: #fff;
                                                                background: #000;
                                                            }

                                                            .style-caption > h3 {
                                                                font-size: 44px;
                                                                line-height: 54px;
                                                                color: #ffffff;
                                                                font-family: 'Titillium Web', Arial, Helvetica, 'Nimbus Sans L', sans-serif;
                                                                font-weight: 700;
                                                                font-style: normal;
                                                                text-transform: none;
                                                                text-decoration: none;
                                                            }
                                                        </style>
                                                        <?php
                                                        $banners = \Modules\ThemeHandymanServices\Models\Banner::where('location', 'slide_home')->where("status", 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->get();
                                                        ?>
                                                        <div class="">
                                                            <div id="myCarousel" class="carousel slide"
                                                                 data-ride="carousel">
                                                                <!-- Indicators -->
                                                                <ol class="carousel-indicators">
                                                                    @foreach($banners as $k=>$banner)
                                                                        <li data-target="#myCarousel"
                                                                            data-slide-to="{{$k}}"
                                                                            class="{{($k==0)?'active':''}}"></li>
                                                                    @endforeach
                                                                </ol>

                                                                <!-- Wrapper for slides -->
                                                                <div class="carousel-inner">
                                                                    @foreach($banners as $k=>$banner)
                                                                        <div class="item {{($k==0)?'active':''}}">
                                                                            <img src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$banner->{'image_' . $language},'1180',369)}}"
                                                                                 alt="{{@$banner->{'name_' . $language}  }}"
                                                                                 style="width:100%;">
                                                                            <div class="carousel-caption style-caption">
                                                                                <h3>{{@$banner->{'name_' . $language} }}</h3>
                                                                                <form action="/contact" method="get">
                                                                                    <button type="submit" class="btn ">
                                                                                        {{trans('themehandymanservices::site.contact')}}
                                                                                    </button>
                                                                                </form>

                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                </div>

                                                                <!-- Left and right controls -->
                                                                <a class="left carousel-control" href="#myCarousel"
                                                                   data-slide="prev">
                                                                    <span class="tp-leftarrow tparrows uranus glyphicon glyphicon-chevron-left" style="height: auto"></span>
                                                                    <span class="sr-only">Previous</span>
                                                                </a>
                                                                <a class="right carousel-control" href="#myCarousel"
                                                                   data-slide="next">
                                                                    <span class="tp-rightarrow tparrows uranus glyphicon glyphicon-chevron-right"  style="height: auto"></span>
                                                                    <span class="sr-only">Next</span>
                                                                </a>
                                                            </div>
                                                        </div>


                                                    </div><!-- END REVOLUTION SLIDER --></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{--                Các khối dịch vụ--}}
                    <div id="cmsmasters_row_a65928170a"
                         class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
                        <div class="cmsmasters_row_outer_parent">
                            <div class="cmsmasters_row_outer">
                                <div class="cmsmasters_row_inner">
                                    <div class="cmsmasters_row_margin cmsmasters_11">
                                        <div id="cmsmasters_column_18eb6bffe1" class="cmsmasters_column one_first">
                                            <div class="cmsmasters_column_inner">
                                                <div id="portfolio_f56263602e"
                                                     class="cmsmasters_wrap_portfolio entry-summary"
                                                     data-layout="puzzle"
                                                     data-layout-mode="perfect"
                                                     data-url="https://handyman-services.cmsmasters.net/wp-content/plugins/cmsmasters-content-composer/"
                                                     data-orderby="date" data-order="ASC" data-count="8"
                                                     data-categories="services" data-metadata="title,rollover">
                                                    <div class="portfolio puzzle large_gap perfect isotope"
                                                         style="position: relative; height: 376.174px;">
                                                        <!-- Start Project Puzzle Article  -->
                                                        <?php
                                                        $services = \Modules\ThemeHandymanServices\Models\Service::where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->take(8)->get();
                                                        ?>
                                                        @foreach($services as $service)
                                                            <article  class="cmsmasters_project_puzzle one_x_one post-9410 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-services shortcode_animated"
                                                                     data-category="services">
                                                                <div class="project_outer"
                                                                     style="height: 149px; background:url('{{ URL::asset('public/frontend/themes/handyman-services/images/4.jpg') }}');display: flex;justify-content: center;align-items: center;">
                                                                    <div class="cmsmasters_img_rollover_wrap">
                                                                        <header class="cmsmasters_project_header entry-header">
                                                                            <h2 class="cmsmasters_project_title entry-title">
                                                                                <a
                                                                                        href="/{{$service->slug}}">{{@$service->{'name_' . $language} }}</a>
                                                                            </h2>
                                                                        </header>
                                                                    </div>
                                                                    <div class="cl"></div>
                                                                </div>
                                                            </article>
                                                    @endforeach
                                                    <!-- Finish Project Puzzle Article  -->

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="cmsmasters_row_afbb8e23aa"
                         class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_fullwidth">
                        <div class="cmsmasters_row_outer_parent">
                            <div class="cmsmasters_row_outer">
                                <div class="cmsmasters_row_inner cmsmasters_row_fullwidth">
                                    <div class="cmsmasters_row_margin cmsmasters_11">
                                        <div id="cmsmasters_column_c04a024d9c" class="cmsmasters_column one_first">
                                            <div class="cmsmasters_column_inner">
                                                <div id="cmsmasters_divider_57eef94424"
                                                     class="cmsmasters_divider cmsmasters_divider_width_long cmsmasters_divider_pos_center"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="cmsmasters_row_cb9cb1b953"
                         class="cmsmasters_row cmsmasters_color_scheme_first cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
                        <div class="cmsmasters_row_outer_parent">
                            <div class="cmsmasters_row_outer">
                                <div class="cmsmasters_row_inner">
                                    <div class="cmsmasters_row_margin cmsmasters_11">
                                        <div id="cmsmasters_column_14a23a461c" class="cmsmasters_column one_first">
                                            <div class="cmsmasters_column_inner">
                                                <?php
                                                $widget = \Modules\ThemeHandymanServices\Models\Widget::where('location', 'home_content')->where('status', 1)->first();

                                                ?>
                                                <div id="cmsmasters_heading_3e8764f5d7"
                                                     class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                                    <h2 class="cmsmasters_heading">{!! @$widget->{'name_' . $language}  !!}</h2>
                                                </div>
                                                <div id="cmsmasters_divider_3455575720"
                                                     class="cmsmasters_divider cmsmasters_divider_width_short cmsmasters_divider_extrashort"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cmsmasters_row_inner">
                                    <div class="cmsmasters_row_margin cmsmasters_131313">
                                        {!! @$widget->{'content_' . $language}  !!}
                                        <div id="cmsmasters_column_bf4b051ae4" class="cmsmasters_column one_third">
                                            <div class="cmsmasters_column_inner">
                                                <div id="cmsmasters_fb_667dcb9aeb" class="cmsmasters_featured_block">
                                                    <div class="featured_block_inner">
                                                        <div class="featured_block_text">
                                                            <div id="cmsmasters_heading_4eb2a76563"
                                                                 class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                                                <h2 class="cmsmasters_heading">{{trans('themehandymanservices::site.form_contact')}}</h2>
                                                            </div>
                                                            <div class="cmsmasters_contact_form">
                                                                <div role="form" class="wpcf7"
                                                                     id="wpcf7-f13346-p7366-o1"
                                                                     lang="en-US" dir="ltr">
                                                                    <div class="screen-reader-response"></div>
                                                                    <form action="{{route('contact.post')}}"
                                                                          method="post" class="wpcf7-form"
                                                                          novalidate="novalidate">
                                                                        <div style="display: none;">
                                                                            <input type="hidden" name="_wpcf7"
                                                                                   value="13346">
                                                                            <input type="hidden" name="_wpcf7_version"
                                                                                   value="5.1.3">
                                                                            <input type="hidden" name="_wpcf7_locale"
                                                                                   value="en_US">
                                                                            <input type="hidden" name="_wpcf7_unit_tag"
                                                                                   value="wpcf7-f13346-p7366-o1">
                                                                            <input type="hidden"
                                                                                   name="_wpcf7_container_post"
                                                                                   value="7366">
                                                                        </div>
                                                                        <div class="cmsmasters_row_margin cmsmasters_11">
                                                                            <div class="one_half">
                                                                            <span class="wpcf7-form-control-wrap Name"><input
                                                                                        type="text" name="name" value=""
                                                                                        size="40"
                                                                                        class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                        aria-required="true"
                                                                                        aria-invalid="false"
                                                                                        placeholder="{{trans('themehandymanservices::site.name')}}"></span>
                                                                            </div>
                                                                            <div class="one_half">
                                                                            <span class="wpcf7-form-control-wrap Email"><input
                                                                                        type="email" name="email"
                                                                                        value="" size="40"
                                                                                        class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                        aria-required="true"
                                                                                        aria-invalid="false"
                                                                                        placeholder="{{trans('themehandymanservices::site.email')}}"></span>
                                                                            </div>
                                                                            <div class="one_first">
                                                                            <span class="wpcf7-form-control-wrap Phone"><input
                                                                                        type="tel" name="phone" value=""
                                                                                        size="40"
                                                                                        class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel"
                                                                                        aria-invalid="false"
                                                                                        placeholder="{{trans('themehandymanservices::site.phone')}}"></span>
                                                                            </div>
                                                                            <div class="one_first">
                                                                            <span class="wpcf7-form-control-wrap Message"><textarea
                                                                                        name="message" cols="40"
                                                                                        rows="10"
                                                                                        class="wpcf7-form-control wpcf7-textarea"
                                                                                        aria-invalid="false"
                                                                                        placeholder="{{trans('themehandymanservices::site.message')}}"></textarea></span>
                                                                            </div>
                                                                            <div class="one_first">
                                                                                <button name="submit" type="submit"
                                                                                        id="cmsmasters_704_formsend"
                                                                                        class="cmsmasters_button">{{trans('themehandymanservices::site.send_contact')}}
                                                                                </button>

                                                                            </div>
                                                                        </div>
                                                                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="cmsmasters_row_42c2d2e5fd"
                         class="cmsmasters_row cmsmasters_color_scheme_second cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
                        <div class="cmsmasters_row_outer_parent">
                            <div class="cmsmasters_row_outer">
                                <div class="cmsmasters_row_inner">
                                    <div class="cmsmasters_row_margin cmsmasters_11">
                                        <div id="cmsmasters_column_8972782313" class="cmsmasters_column one_first">
                                            <div class="cmsmasters_column_inner">
                                                <div id="cmsmasters_heading_a5c0f401f3"
                                                     class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                                    <h2 class="cmsmasters_heading"> {{trans('themehandymanservices::site.user_speak')}}</h2>
                                                </div>
                                                <div id="cmsmasters_heading_63716dfe04"
                                                     class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                                    <h2 class="cmsmasters_heading">{{trans('themehandymanservices::site.feedback')}}</h2>
                                                </div>
                                                <div id="cmsmasters_divider_b699f9adb0"
                                                     class="cmsmasters_divider cmsmasters_divider_width_short cmsmasters_divider_extrashort"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cmsmasters_row_inner">
                                    <div class="cmsmasters_row_margin cmsmasters_131313">
                                        <?php
                                        $clients = \Modules\ThemeHandymanServices\Models\ClientSay::orderBy('id', 'DESC')->take(3)->get();
                                        ?>
                                        @foreach($clients as $client)

                                            <div id="cmsmasters_column_668530fdc3" class="cmsmasters_column one_third">
                                                <div class="cmsmasters_column_inner">
                                                    <div class="cmsmasters_quotes_slider_wrap">
                                                        <div id="cmsmasters_quotes_slider_962b2a7e9e"
                                                             class="cmsmasters_owl_slider owl-carousel cmsmasters_quotes cmsmasters_quotes_slider cmsmasters_quotes_slider_type_box owl-theme"
                                                             data-auto-play="10000" data-pagination="true"
                                                             data-navigation="false"
                                                             style="opacity: 1; display: block;">
                                                            <div class="owl-wrapper-outer autoHeight"
                                                                 style="height: 305px;">
                                                                <div class="owl-wrapper"
                                                                     style="width: 574px; left: 0px; display: block; transition: all 0ms ease 0s; transform: translate3d(0px, 0px, 0px);">
                                                                    <div class="owl-item active" style="width: 287px;">
                                                                        <div class="cmsmasters_quote">
                                                                            <!-- Start Quote Slider Article  -->
                                                                            <article class="cmsmasters_quote_inner"
                                                                                     id="cmsmasters_quote_5e153d4d33f16">
                                                                                <style type="text/css">#cmsmasters_quote_5e153d4d33f16 .wrap_quote_title {
                                                                                        background-color: #1c1c1c;
                                                                                    }</style>
                                                                                <div class="quote_content">
                                                                                    <div style="    max-height: 150px;    overflow: hidden;">
                                                                                        <div id="cmsmasters_heading_ny1d5xvp3b"
                                                                                             class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                                                                            <h3 class="cmsmasters_heading">
                                                                                                {!! @$client->{'intro_' . $language}  !!}</h3>
                                                                                                {{--{{ @$client->intro }}</h3>--}}
                                                                                        </div>
                                                                                        {!! @$client->{'content_' . $language}  !!}
                                                                                        {{--{{ @$client->content }}--}}
                                                                                    </div>
                                                                                </div>
                                                                                <div class="quote_info_wrap">
                                                                                    <figure class="quote_image">
                                                                                        <img width="100" height="100"
                                                                                             src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($client->image),50,50}}"
                                                                                             class="attachment-thumbnail size-thumbnail"
                                                                                             alt="">
                                                                                    </figure>
                                                                                    <div class="wrap_quote_title">
                                                                                        <h6 class="quote_title">{!! @$client->{'name_' . $language}  !!}</h6>
                                                                                        {{--<h6 class="quote_title" style="margin: 0">{{ @$client->name  }}</h6>--}}
                                                                                    </div>
                                                                                </div>
                                                                            </article>
                                                                            <!-- Finish Quote Slider Article  -->

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="cmsmasters_row_d3b8be021b"
                         class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
                        <div class="cmsmasters_row_outer_parent">
                            <div class="cmsmasters_row_outer">
                                <div class="cmsmasters_row_inner">
                                    <div class="cmsmasters_row_margin cmsmasters_11">
                                        <div id="cmsmasters_column_b9a7b7da89" class="cmsmasters_column one_first">
                                            <div class="cmsmasters_column_inner">
                                                <div id="cmsmasters_heading_405ba7c852"
                                                     class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                                    <h2 class="cmsmasters_heading">{{trans('themehandymanservices::site.popular')}}</h2>
                                                </div>
                                                <div id="cmsmasters_divider_d7da0ee4ee"
                                                     class="cmsmasters_divider cmsmasters_divider_width_short cmsmasters_divider_extrashort"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="cmsmasters_row_164c104c98"
                         class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
                        <div class="cmsmasters_row_outer_parent">
                            <div class="cmsmasters_row_outer">
                                <div class="cmsmasters_row_inner">
                                    <div class="cmsmasters_row_margin cmsmasters_1434">
                                        <div id="cmsmasters_column_8a0a6668b0" class="cmsmasters_column one_fourth">
                                            <div class="cmsmasters_column_inner">

                                                <?php
                                                $categories = \Modules\HandymanServicesTheme\Models\Category::where('type', 1)->orderBy('id', 'asc')->where('status', 1)->get();

                                                ?>
                                                <div id="cmsmasters_divider_8705e5b889"
                                                     class="cl cmsmasters_divider_pos_center"></div>
                                                <div class="cmsmasters_text cmsmasters_custom_list">
                                                    <ul>
                                                        @foreach($categories as $cat)
                                                            <li class="entry-title">
                                                                <h5>
                                                                    <a href="/{{ $cat->slug }}">{{ @$cat->{'name_' . $language}  }}</a>
                                                                </h5>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                                <div id="cmsmasters_divider_af7f7ef255"
                                                     class="cl cmsmasters_divider_pos_center"></div>
                                                <div id="cmsmasters_button_2c1f8a4577" class="button_wrap"><a
                                                            href="/tin-tuc"
                                                            class="cmsmasters_button"><span>{{trans('themehandymanservices::site.detail')}}</span></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="cmsmasters_column_db25a06dec" class="cmsmasters_column three_fourth">
                                            <div class="cmsmasters_column_inner">
                                                <div id="cmsmasters_divider_d08226b247"
                                                     class="cl cmsmasters_divider_pos_center"></div>
                                                <div class="cmsmasters_posts_slider project">
                                                    <div id="cmsmasters_slider_bb7b05b2de"
                                                         class="cmsmasters_owl_slider owl-carousel owl-theme"
                                                         data-items="3"
                                                         data-single-item="false" data-pagination="false"
                                                         data-auto-play="5000" style="opacity: 1; display: block;">
                                                                <?php
                                                                $populars = \Modules\ThemeHandymanServices\Models\Post::where('popular', 1)->where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->get();

                                                                ?>
                                                                    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
                                                                    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
                                                                    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
                                                                    <style>
                                                                        @import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
                                                                        .
                                                                        .product_featured label {
                                                                            display: block;
                                                                            font: 16px/40px arial;
                                                                            text-align: center;
                                                                            margin-bottom: 16px;
                                                                            background: #f6f6f6;
                                                                        }


                                                                        .f2-hsx a {
                                                                            height: 25%;
                                                                        }



                                                                        .price>h3>a:hover {
                                                                            text-decoration: none;
                                                                            color: #ffa103;
                                                                        }
                                                                        .price>h3{
                                                                            padding: 0 10px;
                                                                            margin: 0;
                                                                            text-decoration: none;
                                                                            text-transform: uppercase;
                                                                        }
                                                                        .price>h3>a{

                                                                            font-weight: 700;
                                                                            color: #000;

                                                                        }
                                                                        .col-item .photo img
                                                                        {
                                                                            margin: 0 auto;
                                                                            width: 100%;
                                                                            transition: all .3s ease-in-out;
                                                                            object-fit: contain;
                                                                        }

                                                                        .photo>a {
                                                                            display: block;
                                                                            height: 100%;
                                                                        }
                                                                        .col-item .info
                                                                        {
                                                                            padding: 0 0 10px 0;
                                                                            border-radius: 0 0 5px 5px;
                                                                            margin-top: 1px;
                                                                        }

                                                                        .col-item:hover .info {
                                                                            /*background-color: #F5F5DC;*/
                                                                        }
                                                                        .col-item .price
                                                                        {
                                                                            /*width: 50%;*/
                                                                            /*float: left;*/
                                                                            margin-top: 5px;
                                                                        }

                                                                        .col-item .price h5
                                                                        {
                                                                            line-height: 20px;
                                                                            margin: 0;
                                                                        }

                                                                        .price-text-color
                                                                        {
                                                                            color: #219FD1;
                                                                        }

                                                                        .col-item .info .rating
                                                                        {
                                                                            color: #777;
                                                                        }

                                                                        .col-item .rating
                                                                        {
                                                                            /*width: 50%;*/
                                                                            float: left;
                                                                            font-size: 17px;
                                                                            text-align: right;
                                                                            line-height: 52px;
                                                                            margin-bottom: 10px;
                                                                            height: 52px;
                                                                        }

                                                                        .col-item .separator
                                                                        {
                                                                            /*border-top: 1px solid #E1E1E1;*/
                                                                        }

                                                                        .clear-left
                                                                        {
                                                                            clear: left;
                                                                        }

                                                                        .col-item .separator p
                                                                        {
                                                                            line-height: 20px;
                                                                            margin-bottom: 0;
                                                                            margin-top: 10px;
                                                                            width: 100%;
                                                                            text-align: center;
                                                                        }

                                                                        .col-item .separator p i
                                                                        {
                                                                            margin-right: 5px;
                                                                        }
                                                                        .col-item .btn-add
                                                                        {
                                                                            width: 50%;
                                                                            float: left;
                                                                        }


                                                                        [data-slide="prev"]
                                                                        {
                                                                            margin-right: 10px;
                                                                        }
                                                                        .f2-h label, .f2-topic label {
                                                                            font: 16px/40px arial;
                                                                        }
                                                                        .pro_hot>h3{
                                                                            border-left: 5px solid red;
                                                                            background: #eee;
                                                                            padding: 10px 0 10px 10px;
                                                                            margin: 20px 0 10px 0;
                                                                        }
                                                                        .pro_hot {
                                                                            margin-top: 20px;
                                                                            padding: 0;
                                                                        }
                                                                        @media (max-width: 991px){
                                                                            .f2-h, .product_featured{
                                                                                width: 100%!important;
                                                                            }
                                                                        }
                                                                        @media (max-width: 500px){
                                                                            .photo{
                                                                                height: unset;
                                                                            }
                                                                        }
                                                                        @media (max-width: 768px){
                                                                            #Product .gri{
                                                                                width: 100%!important;
                                                                            }
                                                                        }
                                                                        .slide_product{
                                                                            border: 1px solid #ddd;
                                                                            display: flex
                                                                        }
                                                                        .hpanelwrap{
                                                                            height: 290px!important;
                                                                        }
                                                                        .slide_product_content {
                                                                            width:100%;
                                                                        }
                                                                        .list_product {
                                                                            width: 100%;
                                                                            padding-right: 2%;
                                                                        }
                                                                        .list_product> .col-item {
                                                                            border: 1px solid #ccc;
                                                                        }

                                                                        .list_products {
                                                                            display: flex;
                                                                        }


                                                                        .intro-new{
                                                                            padding: 20px;
                                                                        }
                                                                    </style>

                                                                    <div id="carousel-example" class="carousel slide slide_product_content" data-ride="carousel">
                                                                        <a class="arow-left left cmsmasters_prev_arrow" href="#carousel-example"
                                                                           data-slide="prev" style="top: -40px;left: 85%;"><span></span></a>
                                                                        <a class="arow-right right cmsmasters_next_arrow" style="top: -40px;left: 85%;" href="#carousel-example" data-slide="next"><span></span></a>
                                                                        <!-- Wrapper for slides -->
                                                                        <div class="carousel-inner" style="margin-top: -18px">
                                                                            @foreach($populars as $k=>$popular)
                                                                                @if($k%2==0)
                                                                                    <div class="item {{($k==0)?'active':''}}">
                                                                                        <div class="list_products" style="{{($k==count($populars)-1)?'width:50%!important':''}}">
                                                                                            @endif
                                                                                            <div class="list_product">
                                                                                                <div class="col-item">
                                                                                                    <div class="photo">

                                                                                                        <a href="{{ Modules\ThemeHandymanServices\Http\Helpers\ThemeHelper::getUrlProduct(@$popular) }}"
                                                                                                           title="{{@$popular->{'name_' . $language} }}">
                                                                                                            <img class="img-responsive"
                                                                                                                 src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$popular->image,287,173)}}"
                                                                                                                 alt="{{@$popular->{'name_' . $language} }}"/>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                    <div class="info">
                                                                                                        <div class="">
                                                                                                            <div class="price  text-center">
                                                                                                                <h3>
                                                                                                                    <a href="{{ Modules\ThemeHandymanServices\Http\Helpers\ThemeHelper::getUrlProduct(@$popular) }}"
                                                                                                                       title="{{@$popular->{'name_' . $language} }}">{{$popular->{'name_' . $language} }}</a>
                                                                                                                </h3>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="clearfix">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            @if(($k+1)%2==0 || $k==count($populars)-1)
                                                                                        </div>
                                                                                    </div>
                                                                                @endif
                                                                            @endforeach
                                                                        </div>
                                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="cmsmasters_row_7742d5464f"
                         class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_fullwidth">
                        <div class="cmsmasters_row_outer_parent">
                            <div class="cmsmasters_row_outer">
                                <div class="cmsmasters_row_inner cmsmasters_row_fullwidth">
                                    <div class="cmsmasters_row_margin cmsmasters_11">
                                        <div id="cmsmasters_column_25a3368e62" class="cmsmasters_column one_first">
                                            <div class="cmsmasters_column_inner">
                                                <div id="cmsmasters_divider_0b6ab0a9af"
                                                     class="cmsmasters_divider cmsmasters_divider_width_long cmsmasters_divider_pos_center"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="cmsmasters_row_ac55a42ddf"
                         class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
                        <div class="cmsmasters_row_outer_parent">
                            <div class="cmsmasters_row_outer">
                                <div class="cmsmasters_row_inner">
                                    <div class="cmsmasters_row_margin cmsmasters_131313">
                                        <div id="cmsmasters_column_7ebe8e0c0e" class="cmsmasters_column one_third">
                                            <div class="cmsmasters_column_inner">
                                                <div id="cmsmasters_heading_ad206bf0f0"
                                                     class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                                    <h2 class="cmsmasters_heading">{{trans('themehandymanservices::site.faq')}}</h2>
                                                </div>
                                                <div class="cmsmasters_toggles toggles_mode_accordion">
                                                    <?php
                                                    $faqs = \Modules\ThemeHandymanServices\Models\Post::where('faq',1)->where('status',1)->get();
                                                    ?>
                                                    @foreach($faqs as $k => $faq)

                                                        <div class="cmsmasters_toggle_wrap @if(@$_GET['cate_id'] == $faq->id || (!isset($_GET['cate_id']) && $k == 0))current_toggle @endif"
                                                             data-tags="all ">
                                                            <div class="cmsmasters_toggle_title">
                                                    <span class="cmsmasters_toggle_plus">
                                                    <span class="cmsmasters_toggle_plus_hor"></span>
                                                    <span class="cmsmasters_toggle_plus_vert"></span>
                                                    </span>
                                                                <a href="#">{{@$faq->{'name_' . $language} }}</a>
                                                            </div>
                                                                <div class="cmsmasters_toggle">
                                                                <div class="cmsmasters_toggle_inner">
                                                                    <p>{!! @$faq->{'content_' . $language}  !!}</p>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $news = \Modules\ThemeHandymanServices\Models\Post::where('status',1)->where('faq','<>',1)->orderby('created_at', 'DESC')->where('status', 1)->orderBy('order_no', 'DESC')->take(6)->get();
                                        ?>

                                        <div id="cmsmasters_column_ad33e945de" class="cmsmasters_column one_third">
                                            <div class="cmsmasters_column_inner">
                                                <div id="cmsmasters_heading_89ab935fec"
                                                     class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                                    <h2 class="cmsmasters_heading">{{trans('themehandymanservices::site.new_post')}}</h2>
                                                </div>
                                                <div class="cmsmasters_posts_slider post">
                                                    <div id="cmsmasters_slider_0146e7bf9e"
                                                         class="cmsmasters_owl_slider owl-carousel owl-theme" data-items="2"
                                                         data-single-item="false" data-pagination="false"
                                                         data-auto-play="5000" style="opacity: 1; display: block;">
                                                        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
                                                        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
                                                        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
                                                        <style>
                                                            @import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);

                                                            .product_featured label {
                                                                display: block;
                                                                font: 16px/40px arial;
                                                                text-align: center;
                                                                margin-bottom: 16px;
                                                                background: #f6f6f6;
                                                            }


                                                            .f2-hsx a {
                                                                height: 25%;
                                                            }



                                                            .price>h3>a:hover {
                                                                text-decoration: none;
                                                                color: #ffa103;
                                                            }
                                                            .price>h3{
                                                                padding: 0 10px;
                                                                margin: 0;
                                                                text-decoration: none;
                                                                text-transform: uppercase;
                                                            }
                                                            .price>h3>a{

                                                                font-weight: 700;
                                                                color: #000;

                                                            }
                                                            .col-item .photo img
                                                            {
                                                                margin: 0 auto;
                                                                width: 100%;
                                                                transition: all .3s ease-in-out;
                                                                object-fit: contain;
                                                            }

                                                            .photo>a {
                                                                display: block;
                                                                height: 100%;
                                                            }
                                                            .col-item .info
                                                            {
                                                                padding: 0 0 10px 0;
                                                                border-radius: 0 0 5px 5px;
                                                                margin-top: 1px;
                                                            }

                                                            .col-item:hover .info {
                                                                /*background-color: #F5F5DC;*/
                                                            }
                                                            .col-item .price
                                                            {
                                                                /*width: 50%;*/
                                                                /*float: left;*/
                                                                margin-top: 5px;
                                                            }

                                                            .col-item .price h5
                                                            {
                                                                line-height: 20px;
                                                                margin: 0;
                                                            }

                                                            .price-text-color
                                                            {
                                                                color: #219FD1;
                                                            }

                                                            .col-item .info .rating
                                                            {
                                                                color: #777;
                                                            }

                                                            .col-item .rating
                                                            {
                                                                /*width: 50%;*/
                                                                float: left;
                                                                font-size: 17px;
                                                                text-align: right;
                                                                line-height: 52px;
                                                                margin-bottom: 10px;
                                                                height: 52px;
                                                            }

                                                            .col-item .separator
                                                            {
                                                                /*border-top: 1px solid #E1E1E1;*/
                                                            }

                                                            .clear-left
                                                            {
                                                                clear: left;
                                                            }

                                                            .col-item .separator p
                                                            {
                                                                line-height: 20px;
                                                                margin-bottom: 0;
                                                                margin-top: 10px;
                                                                width: 100%;
                                                                text-align: center;
                                                            }

                                                            .col-item .separator p i
                                                            {
                                                                margin-right: 5px;
                                                            }
                                                            .col-item .btn-add
                                                            {
                                                                width: 50%;
                                                                float: left;
                                                            }


                                                            [data-slide="prev"]
                                                            {
                                                                margin-right: 10px;
                                                            }
                                                            .f2-h label, .f2-topic label {
                                                                font: 16px/40px arial;
                                                            }
                                                            .pro_hot>h3{
                                                                border-left: 5px solid red;
                                                                background: #eee;
                                                                padding: 10px 0 10px 10px;
                                                                margin: 20px 0 10px 0;
                                                            }
                                                            .pro_hot {
                                                                margin-top: 20px;
                                                                padding: 0;
                                                            }
                                                            @media (max-width: 991px){
                                                                .f2-h, .product_featured{
                                                                    width: 100%!important;
                                                                }
                                                            }
                                                            @media (max-width: 500px){
                                                                .photo{
                                                                    height: unset;
                                                                }
                                                            }
                                                            @media (max-width: 768px){
                                                                #Product .gri{
                                                                    width: 100%!important;
                                                                }
                                                            }
                                                            .slide_product{
                                                                border: 1px solid #ddd;
                                                                display: flex
                                                            }
                                                            .hpanelwrap{
                                                                height: 290px!important;
                                                            }
                                                            .slide_product_content {
                                                                width:100%;
                                                            }
                                                            .list_product {
                                                                width: 100%;
                                                                padding-right: 2%;
                                                            }
                                                            .list_product> .col-item {
                                                                border: 1px solid #ccc;
                                                            }

                                                            .list_products {
                                                                display: flex;
                                                            }


                                                            .intro-new{
                                                                padding: 20px;
                                                            }
                                                        </style>

                                                        <div id="carousel-example" class="carousel slide slide_product_content" data-ride="carousel">
{{--                                                            <a class="arow-left left cmsmasters_prev_arrow" href="#carousel-example"--}}
{{--                                                               data-slide="prev" style="top: -70px;left: 70%;"><span></span></a>--}}
{{--                                                            <a class="arow-right right cmsmasters_next_arrow" style="top: -70px;left: 70%;" href="#carousel-example" data-slide="next"><span></span></a>--}}
{{--                                                            <!-- Wrapper for slides -->--}}

                                                            <div class="carousel-inner">
                                                                @foreach($news as $k=>$new)
                                                                    @if($k%1==0)
                                                                        <div class="item {{($k==0)?'active':''}}">
                                                                            <div class="list_products">
                                                                                @endif
                                                                                <div class="list_product">
                                                                                    <div class="col-item">
                                                                                        <div class="photo">
                                                                                            <a href="{{ Modules\ThemeHandymanServices\Http\Helpers\ThemeHelper::getUrlProduct(@$new) }}"
                                                                                               title="{{@$new->{'name_' . $language} }}">
                                                                                                <img class="img-responsive"
                                                                                                     src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$new->image,287,173)}}"
                                                                                                     alt="{{@$new->{'name_' . $language} }}"/>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="info">
                                                                                            <div class="">
                                                                                                <div class="price  text-center">
                                                                                                    <h3>
                                                                                                        <a href="{{ Modules\ThemeHandymanServices\Http\Helpers\ThemeHelper::getUrlProduct(@$new) }}"
                                                                                                           title="{{@$new->{'name_' . $language} }}">{{ @$new->{'name_' . $language} }}</a>
                                                                                                    </h3>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="clearfix">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                @if(($k+1)%1==0)
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                @endforeach
                                                            </div>
                                                        </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php
                                        $widget = \Modules\ThemeHandymanServices\Models\Widget::where('location', 'contact0')->where('status', 1)->first();
                                        ?>

                                        <div id="cmsmasters_column_618be6ad06" class="cmsmasters_column one_third">
                                            <div id="cmsmasters_heading_ad206bf0f0"
                                                 class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                                <h2 class="cmsmasters_heading">{!! @$widget->{'name_' . $language}  !!}</h2>
                                            </div>
                                            <div class="cmsmasters_column_inner">
                                                <div id="cmsmasters_divider_bf0ae3b3d7"
                                                     class="cl cmsmasters_divider_pos_center"></div>
                                                {!! @$widget->{'content_' . $language}  !!}
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="cmsmasters_row_2561544d4b"
                     class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_fullwidth">
                    <div class="cmsmasters_row_outer_parent">
                        <div class="cmsmasters_row_outer">
                            <div class="cmsmasters_row_inner cmsmasters_row_fullwidth">
                                <div class="cmsmasters_row_margin cmsmasters_11">
                                    <div id="cmsmasters_column_960e9c62e2" class="cmsmasters_column one_first">
                                        <div class="cmsmasters_column_inner">
                                            <div id="cmsmasters_divider_e15335be0b"
                                                 class="cmsmasters_divider cmsmasters_divider_width_long cmsmasters_divider_pos_center"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="cmsmasters_row_c1adc8917c"
                     class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
                    <div class="cmsmasters_row_outer_parent">
                        <div class="cmsmasters_row_outer">
                            <div class="cmsmasters_row_inner">
                                <div class="cmsmasters_row_margin cmsmasters_11">
                                    <div id="cmsmasters_column_ee2f5cb121" class="cmsmasters_column one_first">
                                        <div class="cmsmasters_column_inner">
                                            <div id="cmsmasters_heading_94c0723f71"
                                                 class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                                <h2 class="cmsmasters_heading">{{trans('themehandymanservices::site.partners')}}</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="cmsmasters_row_ce94bc5c7f"
                     class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_fullwidth">
                    <div class="cmsmasters_row_outer_parent">
                        <div class="cmsmasters_row_outer">
                            <div class="cmsmasters_row_inner cmsmasters_row_fullwidth">
                                <div class="cmsmasters_row_margin cmsmasters_11">
                                    <div id="cmsmasters_column_dbfd73c813" class="cmsmasters_column one_first">
                                        <div class="cmsmasters_column_inner">
                                            <div class="cmsmasters_clients_slider_wrap">
                                                <div id="cmsmasters_clients_a5961789bc"
                                                     class="cmsmasters_owl_slider owl-carousel cmsmasters_clients_slider owl-theme"
                                                     data-items="4" data-single-item="false" data-auto-play="false"
                                                     data-slide-speed="1000" data-pagination-speed="1000"
                                                     data-pagination="false" data-navigation="false"
                                                     style="opacity: 1; display: block;">
                                                    <div class="owl-wrapper-outer autoHeight" style="height: 90px;">
                                                        <div class="owl-wrapper"
                                                             style="width: 2410px; left: 0px; display: block; transition: all 0ms ease 0s; transform: translate3d(0px, 0px, 0px);">
                                                            <?php
                                                            $banners = \Modules\HandymanServicesTheme\Models\Banner::where('location', 'banners_partner')->where("status", 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->get();
                                                            ?>
                                                            @foreach($banners as $banner)
                                                                <div class="owl-item active" style="width: 241px;">
                                                                    <div class="cmsmasters_clients_item item cmsmasters_owl_slider_item">
{{--                                                                        <img src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($banner->image,161,32)}}"--}}
                                                                        <img src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$banner->{'image_'.$language},161,32)}}"
                                                                             alt="{{ @$banner->{'name_' . $language} }}"></div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


{{--                @include('themehandymanservices::pages.partner')--}}
                <div class="cl"></div>
                <div class="content_wrap fullwidth">

                    <div class="middle_content entry"></div>
                    <!--  Finish Content  -->


                </div>
            </div>
        </div>
        <!--  Finish Middle  -->
        <!--  Start Bottom  -->
        {{--        @include('themehandymanservices::partials.footer')--}}

    </div>
    <span class="cmsmasters_responsive_width"></span>
    <!--  Finish Page  -->

    <div class="cli-modal-backdrop cli-fade cli-settings-overlay"></div>
    <div class="cli-modal-backdrop cli-fade cli-popupbar-overlay"></div>
    </div>
    <script type="text/javascript">
        /* <![CDATA[ */
        cli_cookiebar_settings = '{"animate_speed_hide":"500","animate_speed_show":"500","background":"#FFF","border":"#b1a6a6c2","border_on":false,"button_1_button_colour":"#000","button_1_button_hover":"#000000","button_1_link_colour":"#fff","button_1_as_button":true,"button_1_new_win":false,"button_2_button_colour":"#333","button_2_button_hover":"#292929","button_2_link_colour":"#444","button_2_as_button":false,"button_2_hidebar":false,"button_3_button_colour":"#000","button_3_button_hover":"#000000","button_3_link_colour":"#fff","button_3_as_button":true,"button_3_new_win":false,"button_4_button_colour":"#000","button_4_button_hover":"#000000","button_4_link_colour":"#fff","button_4_as_button":true,"font_family":"inherit","header_fix":false,"notify_animate_hide":true,"notify_animate_show":false,"notify_div_id":"#cookie-law-info-bar","notify_position_horizontal":"right","notify_position_vertical":"bottom","scroll_close":false,"scroll_close_reload":false,"accept_close_reload":false,"reject_close_reload":false,"showagain_tab":true,"showagain_background":"#fff","showagain_border":"#000","showagain_div_id":"#cookie-law-info-again","showagain_x_position":"100px","text":"#000","show_once_yn":false,"show_once":"10000","logging_on":false,"as_popup":false,"popup_overlay":true,"bar_heading_text":"","cookie_bar_as":"banner","popup_showagain_position":"bottom-right","widget_position":"left"}';
        /* ]]> */
    </script>
    <script type="text/javascript">
        var c = document.body.className;
        c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
        document.body.className = c;
    </script>
    <script type="text/javascript">
        function revslider_showDoubleJqueryError(sliderID) {
            var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
            errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
            errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
            errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
            errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
            jQuery(sliderID).show().html(errorMessage);
        }
    </script>
    <script type="text/javascript">
        /* <![CDATA[ */
        var wpcf7 = {
            "apiSettings": {
                "root": "https:\/\/handyman-services.cmsmasters.net\/wp-json\/contact-form-7\/v1",
                "namespace": "contact-form-7\/v1"
            }
        };
        /* ]]> */
    </script>

    <script type="text/javascript">
        /* <![CDATA[ */
        var wc_add_to_cart_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "i18n_view_cart": "View cart",
            "cart_url": "https:\/\/handyman-services.cmsmasters.net\/cart\/",
            "is_cart": "",
            "cart_redirect_after_add": "no"
        };
        /* ]]> */
    </script>

    <script type="text/javascript">
        /* <![CDATA[ */
        var woocommerce_params = {"ajax_url": "\/wp-admin\/admin-ajax.php", "wc_ajax_url": "\/?wc-ajax=%%endpoint%%"};
        /* ]]> */
    </script>

    <script type="text/javascript">
        /* <![CDATA[ */
        var wc_cart_fragments_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "cart_hash_key": "wc_cart_hash_111d670d6faaadc1c57d42767a063ed0",
            "fragment_name": "wc_fragments_111d670d6faaadc1c57d42767a063ed0",
            "request_timeout": "5000"
        };
        /* ]]> */
    </script>

    <script type="text/javascript">
        /* <![CDATA[ */
        var cmsmasters_script = {
            "theme_url": "https:\/\/handyman-services.cmsmasters.net\/wp-content\/themes\/handyman-services",
            "site_url": "https:\/\/handyman-services.cmsmasters.net\/",
            "ajaxurl": "https:\/\/handyman-services.cmsmasters.net\/wp-admin\/admin-ajax.php",
            "nonce_ajax_like": "74e7bc310a",
            "nonce_ajax_view": "28fc9979c1",
            "project_puzzle_proportion": "0.7272",
            "gmap_api_key": "AIzaSyBz2LZYZ7NgCSc7JlVIDUADZ-aSw1mdDsY",
            "gmap_api_key_notice": "Please add your Google Maps API key",
            "gmap_api_key_notice_link": "read more how",
            "primary_color": "#005c8c",
            "ilightbox_skin": "dark",
            "ilightbox_path": "vertical",
            "ilightbox_infinite": "0",
            "ilightbox_aspect_ratio": "1",
            "ilightbox_mobile_optimizer": "1",
            "ilightbox_max_scale": "1",
            "ilightbox_min_scale": "0.2",
            "ilightbox_inner_toolbar": "0",
            "ilightbox_smart_recognition": "0",
            "ilightbox_fullscreen_one_slide": "0",
            "ilightbox_fullscreen_viewport": "center",
            "ilightbox_controls_toolbar": "1",
            "ilightbox_controls_arrows": "0",
            "ilightbox_controls_fullscreen": "1",
            "ilightbox_controls_thumbnail": "1",
            "ilightbox_controls_keyboard": "1",
            "ilightbox_controls_mousewheel": "1",
            "ilightbox_controls_swipe": "1",
            "ilightbox_controls_slideshow": "0",
            "ilightbox_close_text": "Close",
            "ilightbox_enter_fullscreen_text": "Enter Fullscreen (Shift+Enter)",
            "ilightbox_exit_fullscreen_text": "Exit Fullscreen (Shift+Enter)",
            "ilightbox_slideshow_text": "Slideshow",
            "ilightbox_next_text": "Next",
            "ilightbox_previous_text": "Previous",
            "ilightbox_load_image_error": "An error occurred when trying to load photo.",
            "ilightbox_load_contents_error": "An error occurred when trying to load contents.",
            "ilightbox_missing_plugin_error": "The content your are attempting to view requires the <a href='{pluginspage}' target='_blank'>{type} plugin<\\\/a>."
        };
        /* ]]> */
    </script>

    <script type="text/javascript"
            src="{{ URL::asset('public/frontend/themes/handyman-services/js/jquery.isotope.min.js.download') }}"></script>
    <script type="text/javascript">
        /* <![CDATA[ */
        var cmsmasters_isotope_mode = {
            "theme_url": "https:\/\/handyman-services.cmsmasters.net\/wp-content\/themes\/handyman-services",
            "site_url": "https:\/\/handyman-services.cmsmasters.net\/",
            "ajaxurl": "https:\/\/handyman-services.cmsmasters.net\/wp-admin\/admin-ajax.php",
            "nonce_ajax_like": "74e7bc310a",
            "nonce_ajax_view": "28fc9979c1",
            "project_puzzle_proportion": "0.7272",
            "gmap_api_key": "AIzaSyBz2LZYZ7NgCSc7JlVIDUADZ-aSw1mdDsY",
            "gmap_api_key_notice": "Please add your Google Maps API key",
            "gmap_api_key_notice_link": "read more how",
            "primary_color": "#005c8c",
            "ilightbox_skin": "dark",
            "ilightbox_path": "vertical",
            "ilightbox_infinite": "0",
            "ilightbox_aspect_ratio": "1",
            "ilightbox_mobile_optimizer": "1",
            "ilightbox_max_scale": "1",
            "ilightbox_min_scale": "0.2",
            "ilightbox_inner_toolbar": "0",
            "ilightbox_smart_recognition": "0",
            "ilightbox_fullscreen_one_slide": "0",
            "ilightbox_fullscreen_viewport": "center",
            "ilightbox_controls_toolbar": "1",
            "ilightbox_controls_arrows": "0",
            "ilightbox_controls_fullscreen": "1",
            "ilightbox_controls_thumbnail": "1",
            "ilightbox_controls_keyboard": "1",
            "ilightbox_controls_mousewheel": "1",
            "ilightbox_controls_swipe": "1",
            "ilightbox_controls_slideshow": "0",
            "ilightbox_close_text": "Close",
            "ilightbox_enter_fullscreen_text": "Enter Fullscreen (Shift+Enter)",
            "ilightbox_exit_fullscreen_text": "Exit Fullscreen (Shift+Enter)",
            "ilightbox_slideshow_text": "Slideshow",
            "ilightbox_next_text": "Next",
            "ilightbox_previous_text": "Previous",
            "ilightbox_load_image_error": "An error occurred when trying to load photo.",
            "ilightbox_load_contents_error": "An error occurred when trying to load contents.",
            "ilightbox_missing_plugin_error": "The content your are attempting to view requires the <a href='{pluginspage}' target='_blank'>{type} plugin<\\\/a>."
        };
        /* ]]> */
    </script>
    <script type="text/javascript"
            src="{{ URL::asset('public/frontend/themes/handyman-services/js/jquery.isotope.mode.js.download') }}"></script>
    <script type="text/javascript"
            src="{{ URL::asset('public/frontend/themes/handyman-services/js/comment-reply.min.js.download') }}"></script>
    <script type="text/javascript">
        /* <![CDATA[ */
        var cmsmasters_woo_script = {
            "currency_symbol": "\u00a3",
            "thumbnail_image_width": "50",
            "thumbnail_image_height": "50"
        };
        /* ]]> */
    </script>
    <script>


            $(document).on('click','.cmsmasters_toggle_wrap',function (event) {
                event.preventDefault();
                if ($(this).hasClass('current_toggle')){
                    $(this).removeClass('current_toggle');
                } else{
                    $(this).addClass('current_toggle');
                }

            });
        // })

    </script>

@endsection