<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<style>
    @import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);

    .product_featured label {
        display: block;
        font: 16px/40px arial;
        text-align: center;
        margin-bottom: 16px;
        background: #f6f6f6;
    }


    .f2-hsx a {
        height: 25%;
    }



    .price>h3>a:hover {
        text-decoration: none;
        color: #ffa103;
    }
    .price>h3{
        padding: 0 10px;
        margin: 0;
        text-decoration: none;
        text-transform: uppercase;
    }
    .price>h3>a{

        font-weight: 700;
        color: #000;

    }
    .col-item .photo img
    {
        margin: 0 auto;
        width: 100%;
        transition: all .3s ease-in-out;
        object-fit: contain;
    }

    .photo>a {
        display: block;
        height: 100%;
    }
    .col-item .info
    {
        padding: 0 0 10px 0;
        border-radius: 0 0 5px 5px;
        margin-top: 1px;
    }

    .col-item:hover .info {
        /*background-color: #F5F5DC;*/
    }
    .col-item .price
    {
        /*width: 50%;*/
        /*float: left;*/
        margin-top: 5px;
    }

    .col-item .price h5
    {
        line-height: 20px;
        margin: 0;
    }

    .price-text-color
    {
        color: #219FD1;
    }

    .col-item .info .rating
    {
        color: #777;
    }

    .col-item .rating
    {
        /*width: 50%;*/
        float: left;
        font-size: 17px;
        text-align: right;
        line-height: 52px;
        margin-bottom: 10px;
        height: 52px;
    }

    .col-item .separator
    {
        /*border-top: 1px solid #E1E1E1;*/
    }

    .clear-left
    {
        clear: left;
    }

    .col-item .separator p
    {
        line-height: 20px;
        margin-bottom: 0;
        margin-top: 10px;
        width: 100%;
        text-align: center;
    }

    .col-item .separator p i
    {
        margin-right: 5px;
    }
    .col-item .btn-add
    {
        width: 50%;
        float: left;
    }


    [data-slide="prev"]
    {
        margin-right: 10px;
    }
    .f2-h label, .f2-topic label {
        font: 16px/40px arial;
    }
    a.arow-left:before {
        background: #e4e4e4!important;
        position: absolute!important;
        top: -45px;
        right: 50px;
        border-radius: 3px!important;
        height: 42px!important;
        width: 40px!important;
        line-height: 42px!important;
        text-align: center!important;
        z-index: 1!important;
        font-size: 24px;
        opacity: 0.5;
        color: #999;
        /*content: '\f053' !important;*/
        /*font-family: 'FontAwesome' !important;*/
    }
    a.arow-right:before {
        background: #e4e4e4!important;
        position: absolute!important;
        right: 0!important;
        display: block;
        top: -45px;
        border-radius: 3px!important;
        opacity: 0.5;
        color: #999;
        height: 42px!important;
        width: 42px!important;
        line-height: 42px!important;
        font-size: 24px;
        text-align: center!important;
        z-index: 1!important;
        /*content: '\f054' !important;*/
        /*font-family: 'FontAwesome' !important;*/
    }
    .pro_hot>h3{
        border-left: 5px solid red;
        background: #eee;
        padding: 10px 0 10px 10px;
        margin: 20px 0 10px 0;
    }
    .pro_hot {
        margin-top: 20px;
        padding: 0;
    }
    @media (max-width: 991px){
        .f2-h, .product_featured{
            width: 100%!important;
        }
    }
    @media (max-width: 500px){
        .photo{
            height: unset;
        }
    }
    @media (max-width: 768px){
        #Product .gri{
            width: 100%!important;
        }
    }
    .slide_product{
        border: 1px solid #ddd;
        display: flex
    }
    .hpanelwrap{
        height: 290px!important;
    }
    .slide_product_content {
        width:100%;
    }
    .list_product {
        width: 25%;
        padding-right: 2%;
    }
    /*.list_product> .col-item {*/
    /*    padding: 30px;*/
    /*    border: none;*/
    /*}*/

    .list_products {
        display: flex;
    }


    .intro-new{
        padding: 20px;
    }
</style>
<div id="cmsmasters_row_c6b170148c"
     class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
    <div class="cmsmasters_row_outer_parent">
        <div class="cmsmasters_row_outer">
            <div class="cmsmasters_row_inner">
                <div class="cmsmasters_row_margin cmsmasters_11">
                    <div id="cmsmasters_column_3df5c36f0c" class="cmsmasters_column one_first">
                        <div class="cmsmasters_column_inner">
                            <div id="cmsmasters_heading_debcf86c55"
                                 class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                <h2 class="cmsmasters_heading">{{trans('themehandymanservices::site.partners')}}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="cmsmasters_row_022474f888"
     class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_fullwidth">
    <div class="cmsmasters_row_outer_parent">
        <div class="cmsmasters_row_outer">
            <div class="cmsmasters_row_inner cmsmasters_row_fullwidth">
                <div class="cmsmasters_row_margin cmsmasters_11">
                    <div id="cmsmasters_column_170d09ddc6" class="cmsmasters_column one_first">
                        <div class="cmsmasters_column_inner">
                            <div class="cmsmasters_clients_slider_wrap">
                                <div id="cmsmasters_clients_0622dbd888"
                                     class="cmsmasters_owl_slider owl-carousel cmsmasters_clients_slider owl-theme"
                                     data-items="4" data-single-item="false" data-auto-play="false"
                                     data-slide-speed="1000" data-pagination-speed="1000"
                                     data-pagination="false" data-navigation="false"
                                     style="opacity: 1; display: block;">

                                    <div id="carousel-example" class="carousel slide slide_product_content autoHeight"  data-ride="carousel">
                                        <a class="arow-left left cmsmasters_prev_arrow" href="#carousel-example" data-slide="prev" style="top: -40px;left: 85%;"><span></span></a>
                                        <a class="arow-right right cmsmasters_next_arrow" style="top: -40px;left: 85%;" href="#carousel-example" data-slide="next"><span></span></a>

                                        <div class="carousel-inner" style="height: 90px;max-height: 90px;">
                                            <?php
                                            $banners = \Modules\HandymanServicesTheme\Models\Banner::where('location', 'banners_partner')->where("status", 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->get();
                                            ?>
                                            @foreach($banners as $k=>$banner)
                                                @if($k%4==0)
                                                    <div class="item {{($k==0)?'active':''}}" style="height: 90px;">
                                                        <div class="list_products">
                                                            @endif
                                                            <div class="list_product">
                                                                <div class="col-item">
                                                                    <div class="photo">
                                                                        <img class="img-responsive"
                                                                             src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$banner->{'image_'.$language},161,32)}}"
                                                                             alt="{{@$banner->{'name_' . $language} }}"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @if(($k+1)%4==0)
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>