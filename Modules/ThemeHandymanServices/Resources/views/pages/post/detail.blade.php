@extends('themehandymanservices::layouts.default')
@section('main_content')
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="pingback" href="https://handyman-services.cmsmasters.net/xmlrpc.php">
    {{--<title>How Long Will the Tiny Homes Fad Last? – Handyman Services</title>--}}
    <link rel="dns-prefetch" href="https://fonts.googleapis.com/">
    <link rel="dns-prefetch" href="https://s.w.org/">
    <link rel="alternate" type="application/rss+xml" title="Handyman Services » Feed"
          href="https://handyman-services.cmsmasters.net/feed/">
    <link rel="alternate" type="application/rss+xml" title="Handyman Services » Comments Feed"
          href="https://handyman-services.cmsmasters.net/comments/feed/">
    <link rel="alternate" type="application/rss+xml"
          title="Handyman Services » How Long Will the Tiny Homes Fad Last? Comments Feed"
          href="https://handyman-services.cmsmasters.net/how-long-will-the-tiny-homes-fad-last/feed/">
    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/",
            "svgExt": ".svg",
            "source": {"concatemoji": "https:\/\/handyman-services.cmsmasters.net\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.5"}
        };
        !function (a, b, c) {
            function d(a, b) {
                var c = String.fromCharCode;
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0);
                var d = k.toDataURL();
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0);
                var e = k.toDataURL();
                return d === e
            }

            function e(a) {
                var b;
                if (!l || !l.fillText) return !1;
                switch (l.textBaseline = "top", l.font = "600 32px Arial", a) {
                    case"flag":
                        return !(b = d([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819])) && (b = d([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]), !b);
                    case"emoji":
                        return b = d([55357, 56424, 55356, 57342, 8205, 55358, 56605, 8205, 55357, 56424, 55356, 57340], [55357, 56424, 55356, 57342, 8203, 55358, 56605, 8203, 55357, 56424, 55356, 57340]), !b
                }
                return !1
            }

            function f(a) {
                var c = b.createElement("script");
                c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
            }

            var g, h, i, j, k = b.createElement("canvas"), l = k.getContext && k.getContext("2d");
            for (j = Array("flag", "emoji"), c.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, i = 0; i < j.length; i++) c.supports[j[i]] = e(j[i]), c.supports.everything = c.supports.everything && c.supports[j[i]], "flag" !== j[i] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[j[i]]);
            c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () {
                c.DOMReady = !0
            }, c.supports.everything || (h = function () {
                c.readyCallback()
            }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onload", h), b.attachEvent("onreadystatechange", function () {
                "complete" === b.readyState && c.readyCallback()
            })), g = c.source || {}, g.concatemoji ? f(g.concatemoji) : g.wpemoji && g.twemoji && (f(g.twemoji), f(g.wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    {{--    @include('themehandymanservices::partials.header_script')--}}
    <style id="rs-plugin-settings-inline-css" type="text/css">
        #rs-demo-id {
        }
    </style>
    <style id="woocommerce-inline-inline-css" type="text/css">
        .woocommerce form .form-row .required {
            visibility: visible;
        }
    </style>

    <style id="handyman-services-style-inline-css" type="text/css">

        html body {
            background-color: #f0f0f0;
            background-image: url(https://handyman-services.cmsmasters.net/wp-content/uploads/2017/06/pattern-bg.jpg);
            background-position: top center;
            background-repeat: repeat;
            background-attachment: scroll;
            background-size: auto;

        }

        .header_mid .header_mid_inner .logo_wrap {
            width: 185px;
        }

        .header_mid_inner .logo img.logo_retina {
            width: 185px;
        }

        .headline_outer {
            background-image: url(https://handyman-services.cmsmasters.net/wp-content/uploads/2017/06/heading.jpg);
            background-repeat: no-repeat;
            background-attachment: scroll;
            background-size: cover;
        }

        .headline_color {
            background-color: rgba(50, 51, 56, 0);
        }

        @media (min-width: 540px) {
            .headline_aligner,
            .cmsmasters_breadcrumbs_aligner {
                min-height: 200px;
            }
        }

        .header_top {
            height: 38px;
        }

        .header_mid {
            height: 100px;
        }

        .header_bot {
            height: 50px;
        }

        #page.cmsmasters_heading_after_header #middle,
        #page.cmsmasters_heading_under_header #middle .headline .headline_outer {
            padding-top: 100px;
        }

        #page.cmsmasters_heading_after_header.enable_header_top #middle,
        #page.cmsmasters_heading_under_header.enable_header_top #middle .headline .headline_outer {
            padding-top: 138px;
        }

        #page.cmsmasters_heading_after_header.enable_header_bottom #middle,
        #page.cmsmasters_heading_under_header.enable_header_bottom #middle .headline .headline_outer {
            padding-top: 150px;
        }

        #page.cmsmasters_heading_after_header.enable_header_top.enable_header_bottom #middle,
        #page.cmsmasters_heading_under_header.enable_header_top.enable_header_bottom #middle .headline .headline_outer {
            padding-top: 188px;
        }

        @media only screen and (max-width: 1024px) {
            .header_top,
            .header_mid,
            .header_bot {
                height: auto;
            }

            .header_mid .header_mid_inner > div {
                height: 100px;
            }

            .header_bot .header_bot_inner > div {
                height: 50px;
            }

            #page.cmsmasters_heading_after_header #middle,
            #page.cmsmasters_heading_under_header #middle .headline .headline_outer,
            #page.cmsmasters_heading_after_header.enable_header_top #middle,
            #page.cmsmasters_heading_under_header.enable_header_top #middle .headline .headline_outer,
            #page.cmsmasters_heading_after_header.enable_header_bottom #middle,
            #page.cmsmasters_heading_under_header.enable_header_bottom #middle .headline .headline_outer,
            #page.cmsmasters_heading_after_header.enable_header_top.enable_header_bottom #middle,
            #page.cmsmasters_heading_under_header.enable_header_top.enable_header_bottom #middle .headline .headline_outer {
                padding-top: 0 !important;
            }
        }

        @media only screen and (max-width: 768px) {
            .header_mid .header_mid_inner > div,
            .header_bot .header_bot_inner > div {
                height: auto;
            }
        }

    </style>


    {{--<script>if (document.location.protocol != "https:") {document.location = document.URL.replace(/^http:/i, "https:");}</script>--}}
    <script type="text/javascript">
        /* <![CDATA[ */
        var LS_Meta = {"v": "6.7.6"};
        /* ]]> */
    </script>

    <meta name="generator"
          content="Powered by LayerSlider 6.7.6 - Multi-Purpose, Responsive, Parallax, Mobile-Friendly Slider Plugin for WordPress.">
    <!-- LayerSlider updates and docs at: https://layerslider.kreaturamedia.com -->
    <link rel="https://api.w.org/" href="https://handyman-services.cmsmasters.net/wp-json/">
    <link rel="EditURI" type="application/rsd+xml" title="RSD"
          href="https://handyman-services.cmsmasters.net/xmlrpc.php?rsd">
    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
          href="https://handyman-services.cmsmasters.net/wp-includes/wlwmanifest.xml">
    <link rel="prev" title="How to Prepare a Deck for Stain"
          href="https://handyman-services.cmsmasters.net/how-to-prepare-a-deck-for-stain/">
    <link rel="next" title="How to Contain Dust During Messy Projects"
          href="https://handyman-services.cmsmasters.net/how-to-contain-dust-during-messy-projects/">
    <meta name="generator" content="WordPress 5.2.5">
    <meta name="generator" content="WooCommerce 3.6.4">
    <link rel="canonical" href="https://handyman-services.cmsmasters.net/how-long-will-the-tiny-homes-fad-last/">
    <link rel="shortlink" href="https://handyman-services.cmsmasters.net/?p=103">
    <link rel="alternate" type="application/json+oembed"
          href="https://handyman-services.cmsmasters.net/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fhandyman-services.cmsmasters.net%2Fhow-long-will-the-tiny-homes-fad-last%2F">
    <link rel="alternate" type="text/xml+oembed"
          href="https://handyman-services.cmsmasters.net/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fhandyman-services.cmsmasters.net%2Fhow-long-will-the-tiny-homes-fad-last%2F&amp;format=xml">
    <noscript>
        <style>.woocommerce-product-gallery {
                opacity: 1 !important;
            }</style>
    </noscript>
    <script type="text/javascript">
        var cli_flush_cache = 2;
    </script>
    <meta name="generator"
          content="Powered by Slider Revolution 5.4.8.3 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface.">
    <link rel="icon"
          href="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-32x32.png"
          sizes="32x32">
    <link rel="icon"
          href="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-192x192.png"
          sizes="192x192">
    <link rel="apple-touch-icon-precomposed"
          href="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-180x180.png">
    <meta name="msapplication-TileImage"
          content="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-270x270.png">
    <script type="text/javascript">function setREVStartSize(e) {
            try {
                e.c = jQuery(e.c);
                var i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
                if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                    f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                    var u = (e.c.width(), jQuery(window).height());
                    if (void 0 != e.fullScreenOffsetContainer) {
                        var c = e.fullScreenOffsetContainer.split(",");
                        if (c) jQuery.each(c, function (e, i) {
                            u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                        }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                    }
                    f = u
                } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                e.c.closest(".rev_slider_wrapper").css({height: f})
            } catch (d) {
                console.log("Failure at Presize of Slider:" + d)
            }
        };</script>
    </head>
    <div id="screen-shader" style="
            transition: opacity 0.1s ease 0s; 
            z-index: 2147483647;
            margin: 0; 
            border-radius: 0; 
            padding: 0; 
            background: #111111; 
            pointer-events: none; 
            position: fixed; 
            top: -10%; 
            right: -10%; 
            width: 120%; 
            height: 120%; 
            opacity: 0.6000;
            mix-blend-mode: multiply; 
            display: none;
        "></div>
    <body data-rsssl="1" class="post-template-default single single-post postid-103 single-format-image woocommerce-js">

    <div class="cmsmasters_header_search_form">
        <span class="cmsmasters_header_search_form_close"></span>
        <form method="get">
            <div class="cmsmasters_header_search_form_field">
                <input type="search" name="s" placeholder="Enter Keywords" value="">
                <button type="submit" class="cmsmasters_theme_icon_search"></button>
            </div>
        </form>
    </div>
    <!--  Start Page  -->
    <div id="page"
         class="csstransition chrome_only cmsmasters_boxed fixed_header enable_header_top cmsmasters_heading_after_header hfeed site">

        <!--  Start Main  -->
        <div id="main">

            <!--  Start Header  -->
        @include('themehandymanservices::template.menu')
        <!--  Finish Header  -->


            <!--  Start Middle  -->
            <div id="middle">
                <div class="headline cmsmasters_color_scheme_default">
                    <div class="headline_outer">
                        <div class="headline_color"></div>
                        <div class="headline_inner align_left">`
                            <div class="headline_aligner"></div>
                            <div class="headline_text"><h1 class="entry-title">{{$post->{'name_' . $language} }}</h1>
                            </div>
                            <div class="cmsmasters_breadcrumbs">
                                <div class="cmsmasters_breadcrumbs_aligner"></div>
                                <div class="cmsmasters_breadcrumbs_inner"><a
                                            href="/" class="cms_home">Home</a>
                                    <span class="breadcrumbs_sep"> / </span>
                                    <a href="#">Advice</a>
                                    <span class="breadcrumbs_sep"> / </span>
                                    <span>{{$post->{'name_' . $language} }}</span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="middle_inner">
                    <div class="content_wrap fullwidth">

                        <!-- Start Content  -->
                        <div class="middle_content entry">
                            <div class="blog opened-article"><!-- Start Post Single Article  -->
                                <article id="post-103"
                                         class="cmsmasters_open_post post-103 post type-post status-publish format-image has-post-thumbnail hentry category-advice post_format-post-format-image">
                                    <span class="cmsmasters_post_date"><abbr class="published"
                                                                             title="May 13, 2017">{{date('d-m-Y',strtotime($post->created_at))}}</abbr></span>
                                    {{--<abbr class="dn date updated" title="July 4, 2017">July 4, 2017</abbr></span>--}}
                                    <div class="cmsmasters_post_cont">
                                        <figure class="cmsmasters_img_wrap"><a
                                                    title="{{@$post->{'name_' . $language} }}"
                                                    rel="ilightbox[img_103_5e153f613c59f]"
                                                    class="cmsmasters_img_link"><img
                                                        width="1160" height="693"
                                                        src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$post->image,940,562)}}"
                                                        class=" wp-post-image" alt="{{@$post->{'name_' . $language} }}"
                                                        title="{{@$post->{'name_' . $language} }}"></a></figure>
                                        <header class="cmsmasters_post_header entry-header"><h2
                                                    class="cmsmasters_post_title entry-title">{{  @$post->{'name_' . $language} }}</h2>
                                        </header>
                                        <div class="cmsmasters_post_content entry-content">
                                            <p>{!! @$post->{'content_' . $language} !!}</p>
                                            <div class="cl"></div>
                                        </div>
                                        <div class="cmsmasters_post_cont_info entry-meta">
                                            <?php
                                            $tags = explode('|', trim(@$post->tags, '|'));
                                            //                                        dd($tags);
                                            ?>
                                            @foreach($tags as $tag)

                                                <span class="cmsmasters_post_user_name"><a
                                                            href="/tag/{{@\Modules\ThemeHandymanServices\Models\Category::find($tag)->slug}}"
                                                            class="vcard author"><span
                                                                class="fn">{{ @\Modules\HandymanServicesTheme\Models\Category::find($tag)->{'name_'.$language} }}</span></a></span>
                                            @endforeach

                                        </div>
                                        {{--<aside class="share_posts">--}}
                                        {{--<h6 class="share_posts_title">{{trans('themehandymanservices::site.share')}}</h6>--}}
                                        {{--<div class="share_posts_inner">--}}
                                        {{--<a href="{!! @$settings['facebook'] !!}">Facebook</a>--}}
                                        {{--<a href="{!! @$settings['twitter'] !!}">Twitter</a>--}}
                                        {{--<a href="{!! @$settings['pinterest'] !!}">Pinterest</a>--}}
                                        {{--</div>--}}
                                        {{--</aside>--}}
                                    </div>
                                </article>
                                <!-- Finish Post Single Article  -->
                                <?php
                                $relates = @\Modules\ThemeHandymanServices\Models\Post::where('multi_cat', 'like', '%|' . $post->category_id . '|%')
                                    ->where('id', '<>', $post->id)
                                    ->where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->get();
                                ?>
                                <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
                                <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
                                <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
                                <style>
                                    @import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
                                    .
                                    .product_featured label {
                                        display: block;
                                        font: 16px/40px arial;
                                        text-align: center;
                                        margin-bottom: 16px;
                                        background: #f6f6f6;
                                    }


                                    .f2-hsx a {
                                        height: 25%;
                                    }



                                    .price>h3>a:hover {
                                        text-decoration: none;
                                        color: #ffa103;
                                    }
                                    .price>h3{
                                        padding: 0 10px;
                                        margin: 0;
                                        text-decoration: none;
                                        text-transform: uppercase;
                                    }
                                    .price>h3>a{

                                        font-weight: 700;
                                        color: #000;

                                    }
                                    .col-item .photo img
                                    {
                                        margin: 0 auto;
                                        width: 100%;
                                        transition: all .3s ease-in-out;
                                        object-fit: contain;
                                    }

                                    .photo>a {
                                        display: block;
                                        height: 100%;
                                    }
                                    .col-item .info
                                    {
                                        padding: 0 0 10px 0;
                                        border-radius: 0 0 5px 5px;
                                        margin-top: 1px;
                                    }

                                    .col-item:hover .info {
                                        /*background-color: #F5F5DC;*/
                                    }
                                    .col-item .price
                                    {
                                        /*width: 50%;*/
                                        /*float: left;*/
                                        margin-top: 5px;
                                    }

                                    .col-item .price h5
                                    {
                                        line-height: 20px;
                                        margin: 0;
                                    }

                                    .price-text-color
                                    {
                                        color: #219FD1;
                                    }

                                    .col-item .info .rating
                                    {
                                        color: #777;
                                    }

                                    .col-item .rating
                                    {
                                        /*width: 50%;*/
                                        float: left;
                                        font-size: 17px;
                                        text-align: right;
                                        line-height: 52px;
                                        margin-bottom: 10px;
                                        height: 52px;
                                    }

                                    .col-item .separator
                                    {
                                        /*border-top: 1px solid #E1E1E1;*/
                                    }

                                    .clear-left
                                    {
                                        clear: left;
                                    }

                                    .col-item .separator p
                                    {
                                        line-height: 20px;
                                        margin-bottom: 0;
                                        margin-top: 10px;
                                        width: 100%;
                                        text-align: center;
                                    }

                                    .col-item .separator p i
                                    {
                                        margin-right: 5px;
                                    }
                                    .col-item .btn-add
                                    {
                                        width: 50%;
                                        float: left;
                                    }


                                    [data-slide="prev"]
                                    {
                                        margin-right: 10px;
                                    }
                                    .f2-h label, .f2-topic label {
                                        font: 16px/40px arial;
                                    }
                                    .pro_hot>h3{
                                        border-left: 5px solid red;
                                        background: #eee;
                                        padding: 10px 0 10px 10px;
                                        margin: 20px 0 10px 0;
                                    }
                                    .pro_hot {
                                        margin-top: 20px;
                                        padding: 0;
                                    }
                                    @media (max-width: 991px){
                                        .f2-h, .product_featured{
                                            width: 100%!important;
                                        }
                                    }
                                    @media (max-width: 500px){
                                        .photo{
                                            height: unset;
                                        }
                                    }
                                    @media (max-width: 768px){
                                        #Product .gri{
                                            width: 100%!important;
                                        }
                                    }
                                    .slide_product{
                                        border: 1px solid #ddd;
                                        display: flex
                                    }
                                    .hpanelwrap{
                                        height: 290px!important;
                                    }
                                    .slide_product_content {
                                        width:100%;
                                    }
                                    .list_product {
                                        width: 100%;
                                        padding-right: 2%;
                                    }
                                    .list_product> .col-item {
                                        border: 1px solid #ccc;
                                    }

                                    .list_products {
                                        display: flex;
                                    }


                                    .intro-new{
                                        padding: 20px;
                                    }
                                </style>
                                <h6
                                        class="cmsmasters_single_slider_title">{{trans('themehandymanservices::site.relate_news')}}</h6>
                                <div id="carousel-example" class="carousel slide slide_product_content" data-ride="carousel">
                                    <a class="arow-left left cmsmasters_prev_arrow" href="#carousel-example"
                                       data-slide="prev" style="top: -20px;left: 85%;"><span></span></a>
                                    <a class="arow-right right cmsmasters_next_arrow" style="top: -20px;left: 85%;" href="#carousel-example" data-slide="next"><span></span></a>
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner" style="margin-top: -18px">
                                        @foreach($relates as $k=>$relate)
                                            @if($k%4==0)
                                                <div class="item {{($k==0)?'active':''}}">
                                                    <div class="list_products" style="{{($k==count($relates)-1)?'width:25%!important':''}}">
                                                        @endif
                                                        <div class="list_product">
                                                            <div class="col-item">
                                                                <div class="photo">

                                                                    <a href="{{ Modules\ThemeHandymanServices\Http\Helpers\ThemeHelper::getUrlProduct(@$relate) }}"
                                                                       title="{{@$relate->{'name_' . $language} }}">
                                                                        <img class="img-responsive"
                                                                             src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$relate->image,287,173)}}"
                                                                             alt="{{@$relate->{'name_' . $language} }}"/>
                                                                    </a>
                                                                </div>
                                                                <div class="info">
                                                                    <div class="">
                                                                        <div class="price  text-center">
                                                                            <h3>
                                                                                <a href="{{ Modules\ThemeHandymanServices\Http\Helpers\ThemeHelper::getUrlProduct(@$relate) }}"
                                                                                   title="{{@$relate->{'name_' . $language} }}">{{$relate->{'name_' . $language} }}</a>
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @if(($k+1)%4==0 || $k==count($relates)-1)
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>


                            </div>
                        </div>
                        <!--  Finish Content  -->


                    </div>
                </div>
            </div>
            <!--  Finish Middle  -->
            <!--  Start Bottom  -->
        {{--@include('themehandymanservices::partials.footer')--}}
        <!--  Finish Footer  -->

        </div>
        <span class="cmsmasters_responsive_width"></span>
        <!--  Finish Page  -->

        <div class="cli-modal-backdrop cli-fade cli-settings-overlay"></div>
        <div class="cli-modal-backdrop cli-fade cli-popupbar-overlay"></div>
    </div>
    <script type="text/javascript">
        /* <![CDATA[ */
        cli_cookiebar_settings = '{"animate_speed_hide":"500","animate_speed_show":"500","background":"#FFF","border":"#b1a6a6c2","border_on":false,"button_1_button_colour":"#000","button_1_button_hover":"#000000","button_1_link_colour":"#fff","button_1_as_button":true,"button_1_new_win":false,"button_2_button_colour":"#333","button_2_button_hover":"#292929","button_2_link_colour":"#444","button_2_as_button":false,"button_2_hidebar":false,"button_3_button_colour":"#000","button_3_button_hover":"#000000","button_3_link_colour":"#fff","button_3_as_button":true,"button_3_new_win":false,"button_4_button_colour":"#000","button_4_button_hover":"#000000","button_4_link_colour":"#fff","button_4_as_button":true,"font_family":"inherit","header_fix":false,"notify_animate_hide":true,"notify_animate_show":false,"notify_div_id":"#cookie-law-info-bar","notify_position_horizontal":"right","notify_position_vertical":"bottom","scroll_close":false,"scroll_close_reload":false,"accept_close_reload":false,"reject_close_reload":false,"showagain_tab":true,"showagain_background":"#fff","showagain_border":"#000","showagain_div_id":"#cookie-law-info-again","showagain_x_position":"100px","text":"#000","show_once_yn":false,"show_once":"10000","logging_on":false,"as_popup":false,"popup_overlay":true,"bar_heading_text":"","cookie_bar_as":"banner","popup_showagain_position":"bottom-right","widget_position":"left"}';
        /* ]]> */
    </script>
    <script type="text/javascript">
        var c = document.body.className;
        c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
        document.body.className = c;
    </script>
    <script type="text/javascript">
        /* <![CDATA[ */
        var wpcf7 = {
            "apiSettings": {
                "root": "https:\/\/handyman-services.cmsmasters.net\/wp-json\/contact-form-7\/v1",
                "namespace": "contact-form-7\/v1"
            }
        };
        /* ]]> */
    </script>

    <script type="text/javascript">
        /* <![CDATA[ */
        var wc_add_to_cart_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "i18n_view_cart": "View cart",
            "cart_url": "https:\/\/handyman-services.cmsmasters.net\/cart\/",
            "is_cart": "",
            "cart_redirect_after_add": "no"
        };
        /* ]]> */
    </script>

    <script type="text/javascript">
        /* <![CDATA[ */
        var woocommerce_params = {"ajax_url": "\/wp-admin\/admin-ajax.php", "wc_ajax_url": "\/?wc-ajax=%%endpoint%%"};
        /* ]]> */
    </script>

    <script type="text/javascript">
        /* <![CDATA[ */
        var wc_cart_fragments_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "cart_hash_key": "wc_cart_hash_111d670d6faaadc1c57d42767a063ed0",
            "fragment_name": "wc_fragments_111d670d6faaadc1c57d42767a063ed0",
            "request_timeout": "5000"
        };
        /* ]]> */
    </script>

    <script type="text/javascript">
        /* <![CDATA[ */
        var cmsmasters_script = {
            "theme_url": "https:\/\/handyman-services.cmsmasters.net\/wp-content\/themes\/handyman-services",
            "site_url": "https:\/\/handyman-services.cmsmasters.net\/",
            "ajaxurl": "https:\/\/handyman-services.cmsmasters.net\/wp-admin\/admin-ajax.php",
            "nonce_ajax_like": "74e7bc310a",
            "nonce_ajax_view": "28fc9979c1",
            "project_puzzle_proportion": "0.7272",
            "gmap_api_key": "AIzaSyBz2LZYZ7NgCSc7JlVIDUADZ-aSw1mdDsY",
            "gmap_api_key_notice": "Please add your Google Maps API key",
            "gmap_api_key_notice_link": "read more how",
            "primary_color": "#005c8c",
            "ilightbox_skin": "dark",
            "ilightbox_path": "vertical",
            "ilightbox_infinite": "0",
            "ilightbox_aspect_ratio": "1",
            "ilightbox_mobile_optimizer": "1",
            "ilightbox_max_scale": "1",
            "ilightbox_min_scale": "0.2",
            "ilightbox_inner_toolbar": "0",
            "ilightbox_smart_recognition": "0",
            "ilightbox_fullscreen_one_slide": "0",
            "ilightbox_fullscreen_viewport": "center",
            "ilightbox_controls_toolbar": "1",
            "ilightbox_controls_arrows": "0",
            "ilightbox_controls_fullscreen": "1",
            "ilightbox_controls_thumbnail": "1",
            "ilightbox_controls_keyboard": "1",
            "ilightbox_controls_mousewheel": "1",
            "ilightbox_controls_swipe": "1",
            "ilightbox_controls_slideshow": "0",
            "ilightbox_close_text": "Close",
            "ilightbox_enter_fullscreen_text": "Enter Fullscreen (Shift+Enter)",
            "ilightbox_exit_fullscreen_text": "Exit Fullscreen (Shift+Enter)",
            "ilightbox_slideshow_text": "Slideshow",
            "ilightbox_next_text": "Next",
            "ilightbox_previous_text": "Previous",
            "ilightbox_load_image_error": "An error occurred when trying to load photo.",
            "ilightbox_load_contents_error": "An error occurred when trying to load contents.",
            "ilightbox_missing_plugin_error": "The content your are attempting to view requires the <a href='{pluginspage}' target='_blank'>{type} plugin<\\\/a>."
        };
        /* ]]> */
    </script>


    <script type="text/javascript">
        /* <![CDATA[ */
        var cmsmasters_woo_script = {
            "currency_symbol": "\u00a3",
            "thumbnail_image_width": "50",
            "thumbnail_image_height": "50"
        };
        /* ]]> */
    </script>

    <script async="async" type="text/javascript"
            src="{{ URL::asset('public/frontend/themes/handyman-services/js/form.js') }}"></script>

@endsection
{{--</body>--}}
{{--</html>--}}