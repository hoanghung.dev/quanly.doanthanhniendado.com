@extends('themehandymanservices::layouts.default')
@section('main_content')

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="pingback" href="https://handyman-services.cmsmasters.net/xmlrpc.php">
    {{--<title>Assembly – Handyman Services</title>--}}
    <link rel="dns-prefetch" href="https://fonts.googleapis.com/">
    <link rel="dns-prefetch" href="https://s.w.org/">
    <link rel="alternate" type="application/rss+xml" title="Handyman Services » Feed"
          href="https://handyman-services.cmsmasters.net/feed/">
    <link rel="alternate" type="application/rss+xml" title="Handyman Services » Comments Feed"
          href="https://handyman-services.cmsmasters.net/comments/feed/">
    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/",
            "svgExt": ".svg",
            "source": {"concatemoji": "https:\/\/handyman-services.cmsmasters.net\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.5"}
        };
        !function (a, b, c) {
            function d(a, b) {
                var c = String.fromCharCode;
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0);
                var d = k.toDataURL();
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0);
                var e = k.toDataURL();
                return d === e
            }

            function e(a) {
                var b;
                if (!l || !l.fillText) return !1;
                switch (l.textBaseline = "top", l.font = "600 32px Arial", a) {
                    case"flag":
                        return !(b = d([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819])) && (b = d([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]), !b);
                    case"emoji":
                        return b = d([55357, 56424, 55356, 57342, 8205, 55358, 56605, 8205, 55357, 56424, 55356, 57340], [55357, 56424, 55356, 57342, 8203, 55358, 56605, 8203, 55357, 56424, 55356, 57340]), !b
                }
                return !1
            }

            function f(a) {
                var c = b.createElement("script");
                c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
            }

            var g, h, i, j, k = b.createElement("canvas"), l = k.getContext && k.getContext("2d");
            for (j = Array("flag", "emoji"), c.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, i = 0; i < j.length; i++) c.supports[j[i]] = e(j[i]), c.supports.everything = c.supports.everything && c.supports[j[i]], "flag" !== j[i] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[j[i]]);
            c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () {
                c.DOMReady = !0
            }, c.supports.everything || (h = function () {
                c.readyCallback()
            }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onload", h), b.attachEvent("onreadystatechange", function () {
                "complete" === b.readyState && c.readyCallback()
            })), g = c.source || {}, g.concatemoji ? f(g.concatemoji) : g.wpemoji && g.twemoji && (f(g.twemoji), f(g.wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
{{--    @include('themehandymanservices::partials.header_script')--}}
    <style id="rs-plugin-settings-inline-css" type="text/css">
        #rs-demo-id {
        }
    </style>
    <style id="woocommerce-inline-inline-css" type="text/css">
        .woocommerce form .form-row .required {
            visibility: visible;
        }
    </style>

    <style id="handyman-services-style-inline-css" type="text/css">

        html body {
            background-color: #f0f0f0;
            background-image: url(https://handyman-services.cmsmasters.net/wp-content/uploads/2017/06/pattern-bg.jpg);
            background-position: top center;
            background-repeat: repeat;
            background-attachment: scroll;
            background-size: auto;

        }

        .header_mid .header_mid_inner .logo_wrap {
            width: 185px;
        }

        .header_mid_inner .logo img.logo_retina {
            width: 185px;
        }

        .headline_outer {
            background-image: url(https://handyman-services.cmsmasters.net/wp-content/uploads/2017/06/header-1.jpg);
            background-repeat: no-repeat;
            background-attachment: scroll;
            background-size: cover;
        }

        @media (min-width: 540px) {
            .headline_aligner,
            .cmsmasters_breadcrumbs_aligner {
                min-height: 201px;
            }
        }

        .header_top {
            height: 38px;
        }

        .header_mid {
            height: 100px;
        }

        .header_bot {
            height: 50px;
        }

        #page.cmsmasters_heading_after_header #middle,
        #page.cmsmasters_heading_under_header #middle .headline .headline_outer {
            padding-top: 100px;
        }

        #page.cmsmasters_heading_after_header.enable_header_top #middle,
        #page.cmsmasters_heading_under_header.enable_header_top #middle .headline .headline_outer {
            padding-top: 138px;
        }

        #page.cmsmasters_heading_after_header.enable_header_bottom #middle,
        #page.cmsmasters_heading_under_header.enable_header_bottom #middle .headline .headline_outer {
            padding-top: 150px;
        }

        #page.cmsmasters_heading_after_header.enable_header_top.enable_header_bottom #middle,
        #page.cmsmasters_heading_under_header.enable_header_top.enable_header_bottom #middle .headline .headline_outer {
            padding-top: 188px;
        }

        @media only screen and (max-width: 1024px) {
            .header_top,
            .header_mid,
            .header_bot {
                height: auto;
            }

            .header_mid .header_mid_inner > div {
                height: 100px;
            }

            .header_bot .header_bot_inner > div {
                height: 50px;
            }

            #page.cmsmasters_heading_after_header #middle,
            #page.cmsmasters_heading_under_header #middle .headline .headline_outer,
            #page.cmsmasters_heading_after_header.enable_header_top #middle,
            #page.cmsmasters_heading_under_header.enable_header_top #middle .headline .headline_outer,
            #page.cmsmasters_heading_after_header.enable_header_bottom #middle,
            #page.cmsmasters_heading_under_header.enable_header_bottom #middle .headline .headline_outer,
            #page.cmsmasters_heading_after_header.enable_header_top.enable_header_bottom #middle,
            #page.cmsmasters_heading_under_header.enable_header_top.enable_header_bottom #middle .headline .headline_outer {
                padding-top: 0 !important;
            }
        }

        @media only screen and (max-width: 768px) {
            .header_mid .header_mid_inner > div,
            .header_bot .header_bot_inner > div {
                height: auto;
            }
        }

    </style>

    <style id="handyman-services-retina-inline-css" type="text/css">
        #cmsmasters_row_8dce25e20f .cmsmasters_row_outer_parent {
            padding-top: 0px;
        }
        .margin-bot{
            margin-bottom: 20px;
        }
        #cmsmasters_row_8dce25e20f .cmsmasters_row_outer_parent {
            padding-bottom: 35px;
        }

        #cmsmasters_heading_68bbade1eb {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 10px;
        }

        #cmsmasters_heading_68bbade1eb .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_68bbade1eb .cmsmasters_heading, #cmsmasters_heading_68bbade1eb .cmsmasters_heading a {
            font-weight: 300;
        }

        #cmsmasters_heading_68bbade1eb .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_68bbade1eb .cmsmasters_heading_divider {
        }

        #cmsmasters_heading_1bd831246e {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 10px;
        }

        #cmsmasters_heading_1bd831246e .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_1bd831246e .cmsmasters_heading, #cmsmasters_heading_1bd831246e .cmsmasters_heading a {
            font-size: 32px;
        }

        #cmsmasters_heading_1bd831246e .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_1bd831246e .cmsmasters_heading_divider {
        }

        #cmsmasters_divider_eee81811e6 {
            border-bottom-width: 4px;
            border-bottom-style: solid;
            margin-top: 15px;
            margin-bottom: 40px;
            border-bottom-color: #f2a61f;
        }

        #cmsmasters_divider_b285927e31 {
            border-bottom-width: 0px;
            border-bottom-style: solid;
            margin-top: 15px;
            margin-bottom: 0px;
        }

        #cmsmasters_gallery_19b5f5d7bd .cmsmasters_gallery {
            margin: 0 0 0 -40px;
        }

        #cmsmasters_gallery_19b5f5d7bd .cmsmasters_gallery .cmsmasters_gallery_item {
            padding: 0 0 40px 40px;
        }

        #cmsmasters_heading_c31e67c2cf {
            text-align: left;
            margin-top: 0px;
            margin-bottom: -60px;
        }

        #cmsmasters_heading_c31e67c2cf .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_c31e67c2cf .cmsmasters_heading, #cmsmasters_heading_c31e67c2cf .cmsmasters_heading a {
            color: rgba(255, 255, 255, 0);
        }

        #cmsmasters_heading_c31e67c2cf .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_c31e67c2cf .cmsmasters_heading_divider {
        }

        #cmsmasters_heading_5dc26f59f5 {
            text-align: left;
            margin-top: 65px;
            margin-bottom: 10px;
        }

        #cmsmasters_heading_5dc26f59f5 .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_5dc26f59f5 .cmsmasters_heading, #cmsmasters_heading_5dc26f59f5 .cmsmasters_heading a {
            font-weight: 300;
        }

        #cmsmasters_heading_5dc26f59f5 .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_5dc26f59f5 .cmsmasters_heading_divider {
        }

        #cmsmasters_heading_c6c2c2f3a9 {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 10px;
        }

        #cmsmasters_heading_c6c2c2f3a9 .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_c6c2c2f3a9 .cmsmasters_heading, #cmsmasters_heading_c6c2c2f3a9 .cmsmasters_heading a {
            font-size: 32px;
        }

        #cmsmasters_heading_c6c2c2f3a9 .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_c6c2c2f3a9 .cmsmasters_heading_divider {
        }

        #cmsmasters_divider_47602616a5 {
            border-bottom-width: 4px;
            border-bottom-style: solid;
            margin-top: 10px;
            margin-bottom: 5px;
            border-bottom-color: #f2a61f;
        }

        #cmsmasters_row_66da517ca6 {
            background-color: #ffffff;
        }

        #cmsmasters_row_66da517ca6 .cmsmasters_row_outer_parent {
            padding-top: 0px;
        }

        #cmsmasters_row_66da517ca6 .cmsmasters_row_outer_parent {
            padding-bottom: 80px;
        }

        #cmsmasters_fb_76d7867239 {
            padding-top: 0px;
            padding-bottom: 0px;
            background-color: #005c8c;
        }

        #cmsmasters_fb_76d7867239 .featured_block_inner {
            width: 100%;
            padding: 40px 35px 40px 35px;
            text-align: center;
            margin: 0 auto;
        }

        #cmsmasters_fb_76d7867239 .featured_block_text {
            text-align: center;
        }


    </style>

    {{--<script>if (document.location.protocol != "https:") {document.location = document.URL.replace(/^http:/i, "https:");}</script>--}}


    <meta name="generator"
          content="Powered by LayerSlider 6.7.6 - Multi-Purpose, Responsive, Parallax, Mobile-Friendly Slider Plugin for WordPress.">
    <!-- LayerSlider updates and docs at: https://layerslider.kreaturamedia.com -->
    <link rel="https://api.w.org/" href="https://handyman-services.cmsmasters.net/wp-json/">
    <link rel="EditURI" type="application/rsd+xml" title="RSD"
          href="https://handyman-services.cmsmasters.net/xmlrpc.php?rsd">
    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
          href="https://handyman-services.cmsmasters.net/wp-includes/wlwmanifest.xml">
    <meta name="generator" content="WordPress 5.2.5">
    <meta name="generator" content="WooCommerce 3.6.4">
    <link rel="canonical" href="https://handyman-services.cmsmasters.net/assembly/">
    <link rel="shortlink" href="https://handyman-services.cmsmasters.net/?p=13523">
    <link rel="alternate" type="application/json+oembed"
          href="https://handyman-services.cmsmasters.net/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fhandyman-services.cmsmasters.net%2Fassembly%2F">
    <link rel="alternate" type="text/xml+oembed"
          href="https://handyman-services.cmsmasters.net/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fhandyman-services.cmsmasters.net%2Fassembly%2F&amp;format=xml">

    <meta name="generator"
          content="Powered by Slider Revolution 5.4.8.3 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface.">
    <link rel="icon"
          href="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-32x32.png"
          sizes="32x32">
    <link rel="icon"
          href="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-192x192.png"
          sizes="192x192">
    <link rel="apple-touch-icon-precomposed"
          href="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-180x180.png">
    <meta name="msapplication-TileImage"
          content="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-270x270.png">

</head>
<div id="screen-shader" style="
            transition: opacity 0.1s ease 0s; 
            z-index: 2147483647;
            margin: 0; 
            border-radius: 0; 
            padding: 0; 
            background: #111111; 
            pointer-events: none; 
            position: fixed; 
            top: -10%; 
            right: -10%; 
            width: 120%; 
            height: 120%; 
            opacity: 0.6000;
            mix-blend-mode: multiply; 
            display: none;
        "></div>
<body data-rsssl="1" class="page-template-default page page-id-13523 woocommerce-js">

<div class="cmsmasters_header_search_form">
    <span class="cmsmasters_header_search_form_close"></span>
    <form method="get" action="https://handyman-services.cmsmasters.net/">
        <div class="cmsmasters_header_search_form_field">
            <input type="search" name="s" placeholder="Enter Keywords" value="">
            <button type="submit" class="cmsmasters_theme_icon_search"></button>
        </div>
    </form>
</div>
<!--  Start Page  -->
<div id="page"
     class="csstransition chrome_only cmsmasters_boxed fixed_header enable_header_top cmsmasters_heading_after_header hfeed site">

    <!--  Start Main  -->
    <div id="main">

        <!--  Start Header  -->
    @include('themehandymanservices::template.menu')
    <!--  Finish Header  -->


        <!--  Start Middle  -->
        <div id="middle">
            <div class="headline cmsmasters_color_scheme_default">
                <div class="headline_outer">
                    <div class="headline_color"></div>
                    <div class="headline_inner align_left">
                        <div class="headline_aligner"></div>
                        <div class="headline_text"><h1 class="entry-title">{{@$service->{'name_' . $language} }}</h1></div>
                        <div class="cmsmasters_breadcrumbs">
                            <div class="cmsmasters_breadcrumbs_aligner"></div>
                            <div class="cmsmasters_breadcrumbs_inner"><a href="/" class="cms_home">{{trans('themehandymanservices::site.home')}}</a>
                                <span class="breadcrumbs_sep"> / </span>
                                <span>{{@$service->{'name_' . $language} }}</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="middle_inner">
                <div class="content_wrap l_sidebar">

                    <!-- Start Content  -->
                    <div class="content entry fr">
                        <div id="cmsmasters_row_8dce25e20f"
                             class="cmsmasters_row cmsmasters_color_scheme_third cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
                            <div class="cmsmasters_row_outer_parent">
                                <div class="cmsmasters_row_outer">
                                    <div class="cmsmasters_row_inner">
                                        <div class="cmsmasters_row_margin cmsmasters_11">
                                            <div id="cmsmasters_column_817a3aded6" class="cmsmasters_column one_first">
                                                <div class="cmsmasters_column_inner">
                                                    <div id="cmsmasters_heading_1bd831246e"
                                                         class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                                        <h2 class="cmsmasters_heading">{{@$service->{'name_' . $language} }}</h2>
                                                    </div>
                                                    <div id="cmsmasters_divider_eee81811e6"
                                                         class="cmsmasters_divider cmsmasters_divider_width_short cmsmasters_divider_extrashort"></div>
                                                    {!! @$service->{'content_' . $language}  !!}
                                                    <div id="cmsmasters_heading_5dc26f59f5"
                                                         class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                                        <h2 class="cmsmasters_heading">{{trans('themehandymanservices::site.form_service')}}</h2>
                                                    </div>
                                                    <div id="cmsmasters_heading_c6c2c2f3a9"
                                                         class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                                        <h2 class="cmsmasters_heading">{{trans('themehandymanservices::site.service_pro')}}</h2>
                                                    </div>
                                                    <div id="cmsmasters_divider_47602616a5"
                                                         class="cmsmasters_divider cmsmasters_divider_width_short cmsmasters_divider_extrashort"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="cmsmasters_row_66da517ca6"
                             class="cmsmasters_row cmsmasters_color_scheme_fourth cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
                            <div class="cmsmasters_row_outer_parent">
                                <div class="cmsmasters_row_outer">
                                    <div class="cmsmasters_row_inner">
                                        <div class="cmsmasters_row_margin cmsmasters_11">
                                            <div id="cmsmasters_column_cadb03a98d" class="cmsmasters_column one_first">
                                                <div class="cmsmasters_column_inner">
                                                    <div id="cmsmasters_fb_76d7867239"
                                                         class="cmsmasters_featured_block">
                                                        <div class="featured_block_inner">
                                                            <div class="featured_block_text">


                                                                <form method="post" class="form_service" id="form_service">
                                                                    <input type="hidden" name="service_name"  value="{{$service->id}}">
                                                                    <div class="row margin-bot">
                                                                        <div class="col">
                                                                            <input type="text" class="form-control" name="first_name" placeholder="{{trans('themehandymanservices::site.first_name')}}*">
                                                                            @if ($errors->has('email'))
                                                                                <p class="text-danger" style="padding-left: 10px;">{{$errors->first('first_name')}}</p>
                                                                            @endif
                                                                        </div>

                                                                        <div class="col">
                                                                            <input type="text" class="form-control" name="last_name" placeholder="{{trans('themehandymanservices::site.last_name')}}*">
                                                                            @if ($errors->has('email'))
                                                                                <p class="text-danger" style="padding-left: 10px;">{{$errors->first('last_name')}}</p>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                    <div class="row margin-bot">
                                                                        <div class="col">
                                                                            <input type="text" class="form-control" name="tel" placeholder="{{trans('themehandymanservices::site.phone_number')}}*">
                                                                            @if ($errors->has('email'))
                                                                                <p class="text-danger" style="padding-left: 10px;">{{$errors->first('tel')}}</p>
                                                                            @endif
                                                                        </div>

                                                                        <div class="col">
                                                                            <input type="text" class="form-control" name="email" placeholder="{{trans('themehandymanservices::site.email_form_service')}}*">
                                                                            @if ($errors->has('email'))
                                                                                <p class="text-danger" style="padding-left: 10px;">{{$errors->first('email')}}</p>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                    <div class="row margin-bot">
                                                                        <div class="col">
                                                                            <input type="text" class="form-control" name="address" placeholder="{{trans('themehandymanservices::site.address_form_service')}}*">
                                                                        </div>
                                                                        <div class="col">
                                                                            <select class="custom-select" name="gender" >
                                                                                <option value="0">{{trans('themehandymanservices::site.boy')}}</option>
                                                                                <option value="1">{{trans('themehandymanservices::site.girl')}}</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row margin-bot">
                                                                        <div class="col">
                                                                            <label>{{trans('themehandymanservices::site.date_start')}}*</label>
                                                                            <input type="date" class="form-control" name="date_start" placeholder="Date Start*">
                                                                            @if ($errors->has('email'))
                                                                                <p class="text-danger" style="padding-left: 10px;">{{$errors->first('date_start')}}</p>
                                                                            @endif
                                                                        </div>
                                                                        <div class="col">
                                                                            <label>{{trans('themehandymanservices::site.time_start')}}</label>
                                                                            <input type="time" class="form-control" name="time_start">
                                                                            @if ($errors->has('email'))
                                                                                <p class="text-danger" style="padding-left: 10px;">{{$errors->first('time_start')}}</p>
                                                                            @endif
                                                                        </div>
                                                                        <div class="col">
                                                                            <label>{{trans('themehandymanservices::site.time_end')}}</label>
                                                                            <input type="time" class="form-control" name="time_end">
                                                                            @if ($errors->has('email'))
                                                                                <p class="text-danger" style="padding-left: 10px;">{{$errors->first('time_end')}}</p>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                    @foreach($service->fileds as $k=>$v)
                                                                        @if($k%2==0)
                                                                        <div class="row margin-bot">
                                                                        @endif
                                                                            <div class="col">
                                                                                <input type="number" class="form-control service_fields_booking" data-hour="{{$v->hours}}" name="service_fields[{{ $v->id }}]" placeholder="{{$v->{'name_' . $language} }}">

                                                                            </div>
                                                                        @if($k%2!=0|| $k == count($service->fileds)-1)
                                                                        </div>
                                                                        @endif
                                                                    @endforeach
                                                                    <script>
                                                                        $(document).on('keyup','.service_fields_booking',function (event) {
                                                                            var total_price = 0;
                                                                            $('.service_fields_booking').each(function () {
                                                                                let hour = $(this).data('hour');
                                                                                let price = '{{(float) \App\Models\Setting::where('name', 'price_per_hours')->first()->value}}'
                                                                                if($(this).val() > 0){
                                                                                    total_price += hour * price * $(this).val();
                                                                                }
                                                                            })
                                                                            $('#price_service').text(total_price.toLocaleString())
                                                                        })
                                                                    </script>
                                                                    <div class="row margin-bot">
                                                                        <div class="col">
                                                                            <textarea style="color: #fff;" type="text" rows="5" class="form-control" name="note" placeholder="Notes..."></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row margin-bot">
                                                                        <div class="col">
                                                                            <label>{{trans('themehandymanservices::site.total_price')}}: <span id="price_service">0 </span><sup>EUR</sup></label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row margin-bot">
                                                                        <button type="submit" class="btn btn-default" style="margin: auto;padding: 10px 50px;">{{trans('themehandymanservices::site.send')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cl"></div>
                    </div>
                    <!--  Finish Content  -->


                    <!--  Start Sidebar  -->
                    <div class="sidebar fl">
                        <aside id="nav_menu-2" class="widget widget_nav_menu">
                            <div class="menu-custom-menu-container">
                                <ul id="menu-custom-menu" class="menu">
                                    <?php
                                    $services=\Modules\ThemeHandymanServices\Models\Service::where('status',1)->orderBy('order_no','DESC')->orderBy('id','ASC')->get();
                                    ?>
                                    @foreach($services as $service)
                                    <li id="menu-item-13556"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13556">
                                        <a href="/{{$service->slug}}" aria-current="page">{{@$service->{'name_' . $language} }}</a>
                                    </li>

                                        @endforeach
                                </ul>
                            </div>
                        </aside>
                    </div>
                    <!--  Finish Sidebar  -->


                </div>
            </div>
        </div>
        <!--  Finish Middle  -->
        <!--  Start Bottom  -->
    <!--  Finish Footer  -->

    </div>
    <span class="cmsmasters_responsive_width"></span>
    <!--  Finish Page  -->

    <div class="cli-modal-backdrop cli-fade cli-settings-overlay"></div>
    <div class="cli-modal-backdrop cli-fade cli-popupbar-overlay"></div>

    <script>

    </script>
    <link rel="stylesheet" id="cpcff_stylepublic-css"
          href="{{ URL::asset('public/frontend/themes/handyman-services/css/stylepublic.css') }}" type="text/css"
          media="all" property="stylesheet">
    <link rel="stylesheet" id="cpcff_jquery_ui-css"
          href="{{ URL::asset('public/frontend/themes/handyman-services/css/jquery.jquery-ui-1.8.20.custom.css') }}"
          type="text/css" media="all" property="stylesheet">
    <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
</div>
@endsection
