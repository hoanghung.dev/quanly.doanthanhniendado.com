<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/handyman-services/css/font-icons.css') }}"
      type="text/css"/>
<link rel="stylesheet" id="layerslider-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/layerslider.css') }}" type="text/css"
      media="all">
<link rel="stylesheet" id="wp-block-library-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/style.min.css') }}" type="text/css"
      media="all">
<link rel="stylesheet" id="wc-block-style-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/style.css') }}" type="text/css" media="all">
<link rel="stylesheet" id="contact-form-7-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/styles.css') }}" type="text/css"
      media="all">
<link rel="stylesheet" id="cookie-law-info-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/cookie-law-info-public.css') }}"
      type="text/css" media="all">
<link rel="stylesheet" id="cookie-law-info-gdpr-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/cookie-law-info-gdpr.css') }}"
      type="text/css" media="all">
<link rel="stylesheet" id="rs-plugin-settings-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/settings.css') }}" type="text/css"
      media="all">
<link rel="stylesheet" id="handyman-services-theme-style-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/style(1).css') }}" type="text/css"
      media="screen, print">
<link rel="stylesheet" id="handyman-services-style-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/style(2).css') }}"
      type="text/css" media="screen, print">
<link rel="stylesheet" id="handyman-services-adaptive-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/adaptive.css') }}" type="text/css"
      media="screen, print">
<link rel="stylesheet" id="handyman-services-retina-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/retina.css') }}" type="text/css"
      media="screen">
<link rel="stylesheet" id="handyman-services-icons-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/fontello.css') }}" type="text/css"
      media="screen">
<link rel="stylesheet" id="handyman-services-icons-custom-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/fontello-custom.css') }}" type="text/css"
      media="screen">
<link rel="stylesheet" id="animate-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/animate.css') }}" type="text/css"
      media="screen">
<link rel="stylesheet" id="ilightbox-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/ilightbox.css') }}" type="text/css"
      media="screen">
<link rel="stylesheet" id="ilightbox-skin-dark-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/dark-skin.css') }}" type="text/css"
      media="screen">
<link rel="stylesheet" id="handyman-services-fonts-schemes-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/handyman-services.css') }}" type="text/css"
      media="screen">
<link rel="stylesheet" id="google-fonts-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/css') }}" type="text/css" media="all">
<link rel="stylesheet" id="handyman-services-theme-vars-style-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/vars-style.css') }}" type="text/css"
      media="screen, print">
<link rel="stylesheet" id="handyman-services-gutenberg-frontend-style-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/frontend-style.css') }}" type="text/css"
      media="screen">
<link rel="stylesheet" id="handyman-services-woocommerce-style-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/plugin-style.css') }}" type="text/css"
      media="screen">
<link rel="stylesheet" id="handyman-services-woocommerce-adaptive-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/plugin-adaptive.css') }}" type="text/css"
      media="screen">
<script src="{{ URL::asset('/public/frontend/themes/handyman-services/js/custom.js?v=1') }}"
        type="text/javascript" defer=""></script>
<script src="{{ URL::asset('/public/frontend/themes/handyman-services/js/wp-emoji-release.min.js') }}"
        type="text/javascript" defer=""></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/greensock.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/jquery.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/jquery-migrate.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/layerslider.kreaturamedia.jquery.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/layerslider.transitions.js') }}"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var Cli_Data = {"nn_cookie_ids": [], "cookielist": []};
    var log_object = {"ajax_url": "https:\/\/handyman-services.cmsmasters.net\/wp-admin\/admin-ajax.php"};
    /* ]]> */
</script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/cookie-law-info-public.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/jquery.themepunch.tools.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/jquery.themepunch.revolution.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/debounced-resize.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/modernizr.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/respond.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/jquery.iLightBox.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/scripts.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/jquery.blockUI.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/add-to-cart.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/js.cookie.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/woocommerce.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/cart-fragments.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/jquery.megaMenu.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/cmsmasters-hover-slider.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/easing.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/easy-pie-chart.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/mousewheel.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/owlcarousel.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/imagesloaded.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/request-animation-frame.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/scrollspy.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/stellar.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/waypoints.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/jquery.tweet.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/smooth-sticky.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/comment-reply.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/jquery.plugin-script.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/wp-embed.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/jquery.theme-script.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/jquery.script.js') }}"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"/>
