<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//  Frontend
Route::group(['prefix' => '', 'middleware' => ['locale']], function () {
    Route::get('', 'Frontend\HomeController@getHome');

    Route::get('introduce', 'Frontend\HomeController@getAbout');

    Route::group(['prefix' => 'contact'], function () {
        Route::get('', 'Frontend\ContactController@getContact');
        Route::post('', 'Frontend\ContactController@postContact')->name('contact.post');
    });
    Route::get('search', 'Frontend\PostController@searchPost');
    Route::get('booking-success', 'Frontend\HomeController@bookingSuccess')->name('booking-success');

    Route::get('{slug1}', 'Frontend\ServiceController@oneParam');
    Route::post('{slug1}', 'Frontend\ServiceController@submit_form_service')->name('service.post');
    Route::get('{slug1}/{slug2}', 'Frontend\ServiceController@twoParam');
    Route::get('{slug1}/{slug2}/{slug3}', 'Frontend\ServiceController@threeParam');
    Route::get('{slug1}/{slug2}/{slug3}/{slug4}', 'Frontend\ServiceController@fourParam');
});