<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace Modules\ThemeSTBD\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Modules\ThemeSTBD\Models\Contact;
use Modules\ThemeSTBD\Models\District;
use Modules\ThemeSTBD\Models\Product;
use Modules\ThemeSTBD\Models\Settings;
use Illuminate\Http\Request;
use Mail;

class ContactController extends Controller
{
    public function contactsAjax(Request $request)
    {
        if ($request->ajax()){
            $contact = new Contact();
            $contact->name = $request->Name;
            $contact->tel = base64_encode($request->Phone);
            $contact->email = base64_encode($request->Email);
            $contact->province = $request->tinh_thanh;
            $contact->distric = $request->quan_huyen;
            $contact->address = $request->Address;
            if ($request->has('product_code')) {
                $product = Product::where('code', $request->product_code)->first();
                if (is_object($product)) {
                    $contact->product_id = $product->id;
                }
            } else {
                $contact->product_id = $request->pid;
            }
            $contact->content = $request->Content;
            $contact->save();



            try {
                $admin_email = Settings::where('name', 'email_notifi')->first();
                Mail::send(['html' => 'frontend.childs.mails.contact'], compact('contact'), function ($message) use ($admin_email) {
                    $message->to($admin_email->value, 'Admin');
                    $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] liên hệ mới');
                });
            } catch (\Exception $ex) {

            }

            return response()->json([
                'success' => true
            ]);
        }
    }

    public function provinceContactPost(Request $request){
        $html = '<select class="form-group contents" name="distric_contact_post" id="distric_contact_post" style="width: 100%;padding: 9px;border-radius: 3px;">';
        $disstrics = District::where('province_id',$request->province_contact_post)->orderBy('name','ASC')->get();
        foreach ($disstrics as $v){
            $html .= '<option value="'.$v->id.'">'.$v->name.'</option>';
        }
        $html .='</select>';
        return response()->json([
            'success' => true,
            'html' => $html
        ]);
    }
    public function addPhoneContact(Request $request){
        if ($request -> ajax()){

            $contact = new Contact();
            $contact->product_id = $request->ProductID;
            $contact->content = $request->State .' '. $request->Product;
            $contact->tel = base64_encode($request->Phone);
            $contact->province = $request->province_uu_dai;
            $contact->save();

            try {
                $admin_email = Settings::where('name', 'email_notifi')->first();
                Mail::send(['html' => 'frontend.childs.mails.contact'], compact('contact'), function ($message) use ($admin_email) {
                    $message->to($admin_email->value, 'Admin');
                    $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] liên hệ mới');
                });
            } catch (\Exception $ex) {

            }

            return response()->json([
                'success' => true,
            ]);
        }
    }

    public function sendContact(Request $request){
        if ($request->ajax()){
            $contact = Contact::create([
                'name' => $request->name,
                'email' => base64_encode($request->email),
                'content' => $request->message,
                'province' => $request->tinh_thanh,
                'tel' => base64_encode($request->tel)
            ]);
//            dd($contact);
            try {
                $admin_email = Settings::where('name', 'email_notifi')->first();
                Mail::send(['html' => 'frontend.childs.mails.contact'], compact('contact'), function ($message) use ($admin_email) {
                    $message->to($admin_email->value, 'Admin');
                    $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] liên hệ mới');
                });
            } catch (\Exception $ex) {

            }

            return response()->json([
                'success' => true
            ]);
        }
    }



    public function getIndex()
    {
        $pageOption = [
            'type'      => 'page',
            'pageName'  => 'Liên hệ',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('themestbd::childs.contact.index');
    }

    public function postIndex(Request $request) {
        $data = $request->except('_token');
        $data['user_ip'] = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
        $contact = Contact::create($data);

        try {
            Mail::send('emails.new_contact', ['bill' => $contact], function($message)
            {
                $admin_config = Settings::where('type', 'admin_info')->pluck('value', 'name')->toArray();
                $message->from(env('MAIL_USERNAME'), '['.Settings::select(['value'])->where('name', 'name')->value.']');
                $message->to($admin_config['admin_email'], $admin_config['admin_name'])->subject('Liên hệ mới');
            });
        } catch (\Exception $ex) {

        }

        return response()->json([
            'status'    => true
        ]);
    }

    public function ajax_contact(Request $request) {
        $data = $request->except('_token');
        $data['user_ip'] = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
        $contact = Contact::create($data);

        try {
            @Mail::send('emails.new_contact', ['bill' => $contact], function($message)
            {
                $admin_config = Settings::where('type', 'admin_info')->pluck('value', 'name')->toArray();
                $message->from(env('MAIL_USERNAME'), '['.Settings::select(['value'])->where('name', 'name')->value.']');
                $message->to($admin_config['admin_email'], $admin_config['admin_name'])->subject('Liên hệ mới');
            });
        } catch (\Exception $ex) {

        }

        return response()->json([
            'status'    => true
        ]);
    }
}