<?php

namespace Modules\ThemeSTBD\Http\Middleware;


use Closure;
use Modules\ThemeSTBD\Models\Settings;

class CheckMaintan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $maintan = Settings::where('name', 'maintan')->first();
        if (@$maintan->value == 1){
            return response()->view('themestbd::errors.405');
        }
       else{
           return $next($request);
       }
    }
}
