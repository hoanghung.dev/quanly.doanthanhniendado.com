<?php

namespace Modules\ThemeSTBD\Models;

use Illuminate\Database\Eloquent\Model;

class Origin extends Model
{
   protected $table = 'origins';
}
