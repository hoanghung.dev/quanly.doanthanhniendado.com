
<style>
    .popup-add-cart{
z-index: 999;
    }
    .gri-view-pro, .gri-add-cart{
        background: @if(@$settings['header_background']!=''){{@$settings['header_background']}} @else {{'linear-gradient(to bottom,#ffa103,#fb7d0f)'}}@endif!important;
        border-radius: 4px!important;
        border: none!important;
        color: #fff!important;
    }
    @media (min-width:990px) {
        .popup-add-cart-content{
            margin-top: 10%;
        }
    }
    @media (max-width:768px) {
        .gri{
            width: 100%;
        }
        .banner-abc ul.flexJus{
            height: unset;
        }
        .f2-head>.b>.f2-topic ul li{
            width: 100% !important;
        }
    }
</style>
<div class="f" style="background:#f6f6f6;">
    <div class="b" id="Product">
        @foreach($products as $product)
            @php
                $manufacture = \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getFromCache('get_$manufacture_index_by_product_id'.$product['id']);
                if (!$manufacture){
                $manufacture = \Modules\ThemeSTBD\Models\Manufacturer::find($product['manufacture_id']);
                \Modules\ThemeSTBD\Http\Helpers\CommonHelper::putToCache('get_$manufacture_index_by_product_id'.$product['id'], $manufacture);
                }
            @endphp
            <div class="gri"
                 href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                 title="{{@$product['meta_title']}}">
                <div class="gi">
                    @php
                        if (@$product['final_price'] != 0 && @$product['base_price'] != 0){
                        $discount = 100 - ((@$product['final_price'])/ @$product['base_price'] * 100);
                        }
                        else{
                        $discount = 0;
                        }
                    @endphp
                    @if($discount > 0)
                        <div class="badge @if(round($discount) >= 20) red_sale @else orange_sale @endif">
                            <div class="sale_text">- {{round($discount)}} %</div>
                        </div>
                    @endif
                    <a href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                       title="{{@$product['meta_title']}}">
                        <img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($product['image'], 250, 'auto')}}"
                             alt="{{$product['name']}}"/>
                    </a>
                    <div class="manuface_g">
                        @if(!empty($manufacture))
                            <a href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                               title="{{@$product['meta_title']}}">
                                <img height="50"
                                     src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($manufacture->image, 150 ,'auto')}}"
                                     alt="{{$manufacture->name}}"/>
                            </a>
                            @else
                            </br></br></br>
                        @endif
                    </div>
                    <h3 style="margin-bottom: 10px"><a href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                                                       title="{{@$product['meta_title']}}">{{$product['name']}}</a></h3>
                    <p class="product_intro">{!! $product['intro'] !!}</p>
                    @if($product['final_price'] < $product['base_price'])
                        <span style="padding-top: 12px;"><u
                                    style="text-decoration: line-through;font-size: 18px">{{number_format($product['base_price'], 0, '.', '.')}}</u><sup
                                    style="font-size: 18px;"> đ</sup></span>
                    @else
                        <div style="height: 32px;"></div>
                    @endif
                    @if($product['final_price'] != 0)
                        <span style="padding: 0px 2px;">@if($product['final_price'] < $product['base_price']) @else
                                Giá
                                bán @endif<b>{{number_format($product['final_price'], 0, '.', '.')}}&nbsp;<sup>đ</sup></b></span>
                        @else
                        </br> <span style="font-size: 25px; width: 100%!important;" class="pr">Liên hệ</span>
                    @endif
                    @if($product['final_price'] < $product['base_price'])
                        <span>Giảm: {{number_format($product['base_price'] - $product['final_price'], 0, '.', '.')}} &nbsp;<sup>đ</sup></span>
                    @else
                        <div style="height: 24px;border-top: none"></div>
                    @endif
                </div>

                <div class="gift">
                    <?php
                    //                        $sales = CommonHelper::getProductsSale($product['id'], @$category->id, @$manufacturer->id);

                    date_default_timezone_set('Asia/Ho_Chi_Minh');
                    $getDateToday = date_create(date('Y-m-d H:i:s'));
                    $sales = [];
                    $product_all = Modules\ThemeSTBD\Models\ProductSale::where(function ($query) use ($getDateToday) {
                        $query->orWhere('time_start', '<=', date_format($getDateToday, 'Y-m-d H:i:s'));
                        $query->orWhere('time_start', null);
                    })->where(function ($query) use ($getDateToday) {
                        $query->orWhere('time_end', '>=', date_format($getDateToday, 'Y-m-d H:i:s'));
                        $query->orWhere('time_end', null);
                    })->get();
                    $sale = [];
                    foreach ($product_all as $key => $sale_val) {
                        $manufacture = \Modules\ThemeSTBD\Models\Manufacturer::whereIn('id', explode('|', @$sale_val->manufacturer_ids))->get();
                        $cate_child = \Modules\ThemeSTBD\Models\Category::whereIn('id', explode('|', @$sale_val->category_ids))->get();
                        foreach ($cate_child as $rt) {
                            if ($rt->parent_id == 0 && in_array($rt->id, explode('|', @$sale_val->category_ids)) == true && in_array($rt->id, explode('|', @$product->multi_cat)) == true) {
                                $sale = Modules\ThemeSTBD\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                            } elseif (in_array($rt->id, explode('|', @$sale_val->category_ids)) == true && in_array($rt->id, explode('|', @$product->multi_cat)) == true) {
                                $sale = Modules\ThemeSTBD\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                            } else {
                                $sale = [];
                            }
                        }
                        if ($sale == []) {
                            foreach ($manufacture as $rts) {
                                if (in_array($rts->id, explode('|', @$sale_val->manufacturer_ids)) == true && in_array($rts->id, explode('|', @$product->manufacture_id)) == true) {
                                    $sale = Modules\ThemeSTBD\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                                } else {
                                    $sale = [];
                                }
                            }
                        }
                        if (array_diff([@$product->id], explode('|', @$sale_val->id_product)) == []) {
                            $sale = Modules\ThemeSTBD\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                        }
                        if (!empty($sale)) {
                            $sales = array_merge($sales, $sale->toArray());
                        }
                    }
                    $ids = array_column($sales, 'id');
                    $ids = array_unique($ids);
                    $sales = array_filter($sales, function ($key, $value) use ($ids) {
                        return in_array($value, array_keys($ids));
                    }, ARRAY_FILTER_USE_BOTH);
                    ?>

                    @include('themestbd::partials.qua_tang')
                </div>
                @if(@$settings['show_detail'] == 1 && @$settings['show_add_cart'] == 0)
                    <div style="border: none!important;">
                        <a href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                           title="{{@$product['meta_title']}}"><span class="gri-view-pro" style="width: 100%!important;
                                        ">XEM HÀNG</span></a>
                    </div>
                @elseif(@$settings['show_detail'] == 0 && @$settings['show_add_cart'] == 1)
                    <div  style="border: none!important;">
                        <span data-text="{{route('order.view')}}" style="cursor: pointer;width: 100%!important;"
                              onclick="(addCart1({{@$product['id']}}))"
                              class="gri-add-cart urlCart">Cho vào giỏ</span>
                    </div>

                @elseif(@$settings['show_detail'] == 1 && @$settings['show_add_cart'] == 1)
                    <div style="border: none!important;">
                        <a href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                           title="{{@$product['meta_title']}}"><span class="gri-view-pro">XEM HÀNG</span></a>
                        <span data-text="{{route('order.view')}}" style="cursor: pointer"
                              onclick="(addCart1({{@$product['id']}}))"
                              class="gri-add-cart urlCart">Cho vào giỏ</span>
                    </div>
                @elseif(@$settings['show_detail'] == 0 && @$settings['show_add_cart'] == 0)
                    <div style="display: none;"></div>
                @endif
            </div>
        @endforeach

    </div>
</div>

@if(count($products) < $countProduct)
    <div data-value="20" class="f" id="pmore"><p id="showModel">Xem thêm ...</p></div>
@endif

