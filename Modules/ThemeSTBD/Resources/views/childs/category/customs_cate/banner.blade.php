<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
{{--<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>--}}
<style>
    @import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
    .f2-topic {
        width: 69%!important;
    }
    .f2-h {
        width: 30%!important;
    }
    div#hpanel a.manuface{
        width: 25%;
    }
    .f2-hsx a {
        height: 25%;
    }
    @media (max-width: 991px){
        .f2-h, .f2-topic{
            width: 100%!important;
        }
    }
    .banner-abc .f2-topic a:before{
        content: unset!important;
    }
    .banner-abc .flexJus>li>a{
        overflow: hidden;
        width: 100%;
    }
    .banner-abc ul.flexJus{
        height: 328px;
        border: 1px solid #ddd;
    }
    .banner-abc .f2-topic img{
        width: 100%!important;
    }
    .banner-abc .f2-topic a{
        background: none!important;
        padding: 0!important;
        border-radius: unset!important;
    }
</style>
<div class="f f2-head banner-abc">
    <div class="b">
        <div class="flexCol f2-h fl">
            <label>CÁC THƯƠNG HIỆU NỔI TIẾNG</label>
            <div class="f flexCol f2-hsx ">
                <div class="hpanelwrap" style="overflow-y:auto;">
                    <div id="hpanel" style="width:100%;position:absolute;left:0;top:0; bottom: 0">
                        @php
                            $brans = \Modules\ThemeSTBD\Models\Manufacturer::where('status', 1)->whereIn('id', $bran_ids)->orderBy('order_no', 'asc')->get();
                        @endphp
                        @if(!empty($brans))
                            @foreach($brans as $bran)
                                <a class="manuface" data-value="{{$bran->id}}" title="{{$bran->name}}"
                                   href="{{route('cate.list',['slug' => $category->slug.'/'.$bran->slug])}}">
                                    <img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($bran->image, 126,'auto')}}"
                                         alt="{{$bran->image}}"/>
                                </a>
                            @endforeach
                        @endif
                    </div>
                </div>

            </div>
            <script>
                $(document).ready(function () {
                    $('#viewfullsapo').click(function () {
                        $('#sapo').css('height', 'auto');
                        $(this).hide();
                    });
                });
            </script>
        </div>
        <div class="flexCol f2-topic fl">
            <label>BANNER</label>
            <ul class="flexJus">
                    <li>
                        <a  href="{{ $category->link_banner_1}}">
                        <img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($category->banner_1,'auto','auto')}}"/></a>
                    </li>

                    <li>
                            <a  href="{{ $category->link_banner_2}}">
                                <img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($category->banner_2,'auto','auto')}}"/></a>
                    </li>

                    <li>
                            <a  href="{{$category->link_banner_3}}">
                                <img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($category->banner_3,'auto','auto')}}"/></a>
                    </li>
            </ul>
        </div>
    </div>
</div>