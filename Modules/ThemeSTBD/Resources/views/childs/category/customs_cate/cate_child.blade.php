<style>
    .f2-hsx a{
        height: 25%!important;
    }
    div#hpanel a.manuface{
        width: 25%!important;
    }
    .f2-h{
        width: 25%!important;
    }
    .f2-topic{
        width: 69%!important;
    }
    @media (max-width: 991px){
        .f2-h, .f2-topic{
            width: 100%!important;
        }
    }
</style>
<div class="f f2-head">
    <div class="b">
        <div class="flexCol f2-h fl">
            <label>CÁC THƯƠNG HIỆU NỔI TIẾNG</label>
            <div class="f flexCol f2-hsx ">
                <div class="hpanelwrap" style="overflow-y:auto;">
                    <div id="hpanel" style="width:100%;position:absolute;left:0;top:0;height: 100%">
                        @php
                            $brans = \Modules\ThemeSTBD\Models\Manufacturer::where('status', 1)->whereIn('id', $bran_ids)->orderBy('order_no', 'asc')->get();
                        @endphp
                        @if(!empty($brans))
                            @foreach($brans as $bran)
                                <a class="manuface" data-value="{{$bran->id}}" title="{{$bran->name}}"
                                   href="{{route('cate.list',['slug' => $category->slug.'/'.$bran->slug])}}">
                                    <img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($bran->image, 126,'auto')}}"
                                         alt="{{$bran->image}}"/>
                                </a>
                            @endforeach
                        @endif
                    </div>
                </div>

            </div>
            <script>
                $(document).ready(function () {
                    $('#viewfullsapo').click(function () {
                        $('#sapo').css('height', 'auto');
                        $(this).hide();
                    });
                });
            </script>
        </div>
        <div class="flexCol f2-topic fl" >
            <label>DANH MỤC {{mb_strtoupper($category->name)}}</label>
            <ul class="flexJus">
                @foreach($child_category as $cate)
                    <li>
                        <figure>
                            <div>
                                <h2>{{$cate->name}}</h2>
                                <a title="{{$cate->name}}" href="{{route('cate.list', ['slug' => $cate->slug])}}"
                                   target="_blank">Xem thêm</a>
                            </div>
                        </figure>
                        <p>
                            <img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($cate->banner_sidebar, 400, 'auto')}}"
                                 alt="{{$cate->name}}"/>
                        </p>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>