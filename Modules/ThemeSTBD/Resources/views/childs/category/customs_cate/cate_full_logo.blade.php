<style>
    .f2-head >.b>.f2-h{
        width: 100%!important;
    }

    .thuonghieu a {
        width: 14.6%;
        padding: 3px;
        float: left;
        background: #fff;
        height: 80px;
        overflow: hidden;
        text-align: center;
        border-radius: 10px;
        margin: 1%;
        border: 1px solid #eee;
        display: flex;
        justify-content: center;
        align-items: center;
    }
</style>
<div class="f f2-head">
    <div class="b">
        <div class="flexCol f2-h fl">
            <label>CÁC THƯƠNG HIỆU NỔI TIẾNG</label>
            <div class="thuonghieu">
                @php
                    $brans = \Modules\ThemeSTBD\Models\Manufacturer::where('status', 1)->whereIn('id', $bran_ids)->orderBy('order_no', 'asc')->get();
                @endphp
                @if(!empty($brans))
                    @foreach($brans as $bran)
                        <a class="manuface" data-value="{{$bran->id}}" title="{{$bran->name}}"
                           href="{{route('cate.list',['slug' => $category->slug.'/'.$bran->slug])}}">
                            <img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($bran->image, 126,'auto')}}"
                                 {{--                                    <img src="http://sieuthibepdien.com/timthumb.php?src=http://sieuthibepdien.com/public/filemanager/userfiles/manufacturer_files/teka.png&w=126"--}}
                                 alt="{{$bran->image}}"/>
                        </a>
                    @endforeach
                @endif
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('#viewfullsapo').click(function () {
                    $('#sapo').css('height', 'auto');
                    $(this).hide();
                });
            });
        </script>
    </div>

</div>