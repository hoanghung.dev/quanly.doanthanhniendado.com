<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
{{--<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>--}}
<style>
    @import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);

    .f2-h {
        width: 30%!important;
    }
    div#hpanel a.manuface{
        width: 25%;
    }
    .f2-hsx a {
        height: 25%;
    }

    .new_post {
        width: 69%!important;
        margin-left: 1%;

    }
    .new_post label {
        display: block;
        font: 16px/40px arial;
        text-align: center;
        margin-bottom: 16px;
        background: #f6f6f6;
    }
    .cate_new_1_content ul,.cate_new_2_content ul,.cate_new_3_content ul{
        margin-top: 10px;
        max-height: 260px;
        overflow-y: auto;
    }
    .cate_new_1_content li,.cate_new_2_content li,.cate_new_3_content li{
        list-style: disc inside;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        width: 100%;
    }

    .f2-h label, .f2-topic label{
        font: 16px/40px arial;
    }
    .cate_new_1_header h4, .cate_new_2_header h4, .cate_new_3_header h4{
        text-transform: uppercase;
        color: #fff;
    }
    .cate_new_1_header{
        background: #f6b26b;
    }
    .cate_new_2_header{
        background: #e69138;
    }
    .cate_new_3_header{
        background: #3d85c6;
    }
    @media (max-width: 991px){
        .f2-h, .new_post{
            width: 100%!important;
        }
    }
</style>
<div class="f f2-head">
    <div class="b">
        <div class="flexCol f2-h fl">
            <label>CÁC THƯƠNG HIỆU NỔI TIẾNG</label>
            <div class="f flexCol f2-hsx ">
                <div class="hpanelwrap" style="overflow-y:auto;">
                    <div id="hpanel" style="width:100%;position:absolute;left:0;top:0; bottom: 0">
                        @php
                            $brans = \Modules\ThemeSTBD\Models\Manufacturer::where('status', 1)->whereIn('id', $bran_ids)->orderBy('order_no', 'asc')->get();
                        @endphp
                        @if(!empty($brans))
                            @foreach($brans as $bran)
                                <a class="manuface" data-value="{{$bran->id}}" title="{{$bran->name}}"
                                   href="{{route('cate.list',['slug' => $category->slug.'/'.$bran->slug])}}">
                                    <img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($bran->image, 126,'auto')}}"
                                         alt="{{$bran->image}}"/>
                                </a>
                            @endforeach
                        @endif
                    </div>
                </div>

            </div>
            <script>
                $(document).ready(function () {
                    $('#viewfullsapo').click(function () {
                        $('#sapo').css('height', 'auto');
                        $(this).hide();
                    });
                });
            </script>
        </div>
        <div class="flexCol new_post fl">
            <label>TIN TỨC</label>
                <?php
            $category1 = \Modules\ThemeSTBD\Models\Category::find(@$category->cate_new_1);
            $category2 = \Modules\ThemeSTBD\Models\Category::find(@$category->cate_new_2);
            $category3 = \Modules\ThemeSTBD\Models\Category::find(@$category->cate_new_3);
                    $posts1 = \Modules\ThemeSTBD\Models\Post::where('multi_cat', 'like', '%|'.@$category->cate_new_1.'|%')->where('status',1)->limit(15)->orderBy('id', 'decs')->get();
                    $posts2 = \Modules\ThemeSTBD\Models\Post::where('multi_cat', 'like', '%|'.@$category->cate_new_2.'|%')->where('status',1)->limit(15)->orderBy('id', 'decs')->get();
                    $posts3 = \Modules\ThemeSTBD\Models\Post::where('multi_cat', 'like', '%|'.@$category->cate_new_3.'|%')->where('status',1)->limit(15)->orderBy('id', 'decs')->get();

                ?>
            <div class="row" style="margin: 0;height: 328px;overflow: hidden;    border: 1px solid #ddd;">
                @if(!empty($category1))
                <div class="col-sm-4">
                    <div class="row" style="border: 1px solid #ddd; border-radius: 5px; margin: 0">
                        <div class="col-sm-12 col-12 cate_new_1_header">
                            <h4>{{@$category1->name}}</h4>
                        </div>
                        @if($posts1->count()>0)
                        <div class="col-sm-12 col-12 cate_new_1_content">
                            <ul>

                                @foreach($posts1 as $post1)
                                    <li>
                                        <a title="{{$post1->name}}"
                                           href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getPostSlug($post1) }}">{{$post1->name}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>
                </div>
                @endif
                @if($posts2->count()>0)
                <div class="col-sm-4">
                    <div class="row" style="border: 1px solid #ddd; border-radius: 5px; margin: 0">
                        <div class="col-sm-12 col-12 cate_new_2_header">
                            <h4>{{@$category2->name}}</h4>
                        </div>
                        <div class="col-sm-12 col-12 cate_new_2_content">
                            <ul>

                                    @foreach($posts2 as $post2)
                                        <li>
                                            <a title="{{$post2->name}}"
                                               href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getPostSlug($post2) }}">{{$post2->name}}</a>
                                        </li>
                                    @endforeach

                            </ul>
                        </div>
                    </div>
                </div>
                @endif
                @if($posts3->count()>0)
                    <div class="col-sm-4">
                        <div class="row" style="border: 1px solid #ddd; border-radius: 5px; margin: 0">
                            <div class="col-sm-12 col-12 cate_new_3_header">
                                <h4>{{@$category3->name}}</h4>
                            </div>
                            <div class="col-sm-12 col-12 cate_new_3_content">
                                <ul>

                                    @foreach($posts3 as $post3)
                                        <li>
                                            <a title="{{$post3->name}}"
                                               href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getPostSlug($post3) }}">{{$post3->name}}</a>
                                        </li>
                                    @endforeach


                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>