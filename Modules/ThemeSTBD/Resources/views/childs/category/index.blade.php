
@extends('themestbd::layouts.master')
@section('main_content')

    <style>

        /*label#sort_prd_price {*/
        /*    position: relative;*/
        /*    border: 1px solid;*/
        /*    padding: 0 20px;*/
        /*    background: #fff;*/
        /*}*/
        .content_category_last>div {
            margin: 0 15px;
            padding: 15px;
            background: #ccc!important;
        }
        label#sort_prd_price:after {
            content: "\f0d7";
            font: 15px FontAwesome;
            padding-left: 8px;
            color: #333;
            cursor: pointer;
        }
        label#sort_prd_price {
            position: relative;
            border: 1px solid #ccc;
            background: #fff;
            border-radius: 5px;
            padding: 1px 10px;
            cursor: pointer;
        }
        label#sort_prd_price>ul>li {
            border-bottom: 1px solid #ccc;
            text-align: center;
        }
        label#sort_prd_price>ul>li>a {
            display: block;
        }

        label#sort_prd_price>ul {
            position: absolute;
            top: 100%;
            left: 0;
            display: none;
            background: #fff;
            border: 1px solid #ccc;
            z-index: 999;
            width: 100%;
        }
        .popup-add-cart {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;

        }
        .popup-add-cart-content{
            width: 320px;
            margin: 0 auto;
            border: 1px solid #ccc;
            background: #fff;
        }
        .popup-add-cart p {
            text-align: center;
            margin: 15px 0;
        }

        a.btn-cart {
            background: #63cc50;
            text-transform: capitalize;
        }

        button.close-pop {
            background: #5467f1;
            border: none;
            text-transform: capitalize;
        }
        .btn-check {
            text-align: center;
            padding: 10px;
        }
        a.btn-cart:hover, button.close-pop:hover{
            color: #000;
        }
{{--        .f2-head > .b > .f2-h {--}}
{{--            width: 100% !important;--}}
{{--        }--}}

{{--        .thuonghieu a {--}}
{{--            width: 14.6%;--}}
{{--            padding: 3px;--}}
{{--            float: left;--}}
{{--            background: #fff;--}}
{{--            height: 80px;--}}
{{--            overflow: hidden;--}}
{{--            text-align: center;--}}
{{--            border-radius: 10px;--}}
{{--            margin: 1%;--}}
{{--            border: 1px solid #eee;--}}
{{--            display: flex;--}}
{{--            justify-content: center;--}}
{{--            align-items: center;--}}
{{--        }--}}

{{--        .cate-none {--}}
{{--            display: none;--}}
{{--        }--}}
    </style>
    <div class="b" style="margin:0px auto;">
        @include('themestbd::partials.menu_master')
        @include('themestbd::partials.breadcrumb')
    </div>
    <div class="b  sapo flexCol">
        <div id="sapo">
            <div class="subsapo"> {!! isset($category_des) ? $category_des : $category->intro !!}</div>
        </div>
        <span id="viewfullsapo" class="showSapo" style="cursor: pointer">Xem thêm</span>
        <span id="viewfullsapo" class="hideSapo" style="display: none; cursor: pointer">Ẩn</span>
    </div>
    <script>
        $(document).ready(function () {
            $('.showSapo').click(function () {
                $('#sapo').css('height', 'auto');
                $(this).hide()
                $('.hideSapo').show();
            });
            $('.hideSapo').click(function () {
                $('#sapo').css('height', '75px');
                $(this).hide()
                $('.showSapo').show();
            });
        });
    </script>
<?php

?>
@if(@$category->cate_show == 0)
    @include('themestbd::childs.category.customs_cate.cate_full_logo')

@elseif(@$category->cate_show == 1)
    @include('themestbd::childs.category.customs_cate.cate_child')
@elseif(@$category->cate_show == 2)
    @include('themestbd::childs.category.customs_cate.news')
@elseif(@$category->cate_show == 3)
    @include('themestbd::childs.category.customs_cate.banner')
@elseif(@$category->cate_show == 4)
        @include('themestbd::childs.category.customs_cate.product_hot')
@endif
<script>

        var isRemove = false;
        var page = 0;
        var order = 3;
        var view = '';
        var ismore = true;

        function regetdata() {
            page = 0;
            ismore = false;
        }

        function thtt(cla) {
            var s = '';
            $('.' + cla).each(function () {
                var vl = $(this).attr('data-value');
                if (vl > 0) {
                    s += (s == '' ? '' : ',') + vl;
                }
            });
            return s;
        }


        function get_data() {
            regetdata();
            var s = thtt('pp');
        }

        function thuoctinh(obj) {
            var parent = $(obj).parent().get(0);
            var cvl = $(parent).attr('data-value');
            var vl = $(obj).attr('data-value');
            if (vl == cvl) return;
            else {
                $(parent).attr('data-value', vl);

                //
                var current = $('#' + cvl);
                $(current).removeClass('select');
                $(current).addClass('unselect');
                //
                $(obj).removeClass('unselect');
                $(obj).addClass('select');
                //
                if (vl > 0) {
                    var idall = $(parent).attr('data-id');
                    var all = $('#' + idall);
                    $(all).removeClass('select');
                    $(all).addClass('unselect');
                }
                //
                var label = $(parent).prev();
                $(label).text($(obj).attr('data-title').slice(0, 20) + '...');
                var width = $(window).width()
                if (width < 767) {
                    $(label).text($(obj).attr('data-title').slice(0, 12) + '...');
                }
            }
            get_data();
        }

        $(document).ready(function () {
            $('.oli label').each(function () {
                var width = $(window).width()
                var te = $(this).text();
                if (width < 767) {
                    $(this).text(te.slice(0, 12) + '...');
                }
            })
        })
        order = 3;
        $(document).ready(function () {

            //sap xep
            $('.bs').click(function () {
                var neworder = $(this).attr('data-v');
                if (order == neworder) return;
                order = neworder;
                $('.bs').attr('class', '');
                $(this).attr('class', 'bs curent');
                $('#orderlabel').text($(this).text());
                get_data();
            });
            $('.othpro ul li').click(function () {
                thuoctinh(this);
                get_data();
            });
        });
    </script>
    <div class="f" style="background:#eaeaea!important">
        <div class="b flexJus inline-bl" style="padding:10px 0;">
            <div class="fl flexJus hp-pro">
                <label><span id="number">{{$countProduct}}</span> sản phẩm <span
                            id="propertyCurrent"></span></label>
                <ul class="othpro flexL">
                    @php
                        $fiels = explode('|', @$category->properties_name_id);

                        if ($fiels['0'] == ''){
                            array_shift($fiels);
                        }
                        if (end($fiels) == ''){
                            array_pop($fiels);
                        }
                         $propretieName = Modules\ThemeSTBD\Models\PropertieName::whereIn('id', $fiels)->orderBy('order_no', 'desc')->get();
                    @endphp
                    @if(!empty($propretieName))
                        @foreach($propretieName as $filel)
                            @php
                                $propretieValue = Modules\ThemeSTBD\Models\PropertieValue::where('properties_name_id', $filel->id)->orderBy('order_no', 'desc')->get();
                            @endphp
                            <li class="oli">
                                <label>{{ucfirst($filel->name)}}</label>
                                <ul class="pp" id="Classify{{$filel->id}}" data-value="" data-id="25{{$filel->id}}">
                                    <li id="25{{$filel->id}}" data-title="{{ucfirst($filel->name)}}" data-value="0"
                                        class="radio select">Tất cả
                                    </li>
                                    @if(!empty($propretieValue))
                                        @foreach($propretieValue as $key => $value)
                                            <li id="{{$value->id}}" data-title="{{ucfirst($value->value)}}"
                                                data-value="{{$value->id}}"
                                                class="radio unselect">{!! ucfirst($value->value) !!}
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </li>
                        @endforeach
                    @endif
                </ul>
{{--                <select name="sort_prd_price" id="sort_prd_price">--}}
{{--                    <option value="1"><a href="/bep-tu?sort_prd_price=0&category_sort=1">Sắp xếp theo gía sản phẩm tăng dần</a></option>--}}
{{--                    <option value="0">Sắp xếp theo gía sản phẩm giảm dần</option>--}}
{{--                </select>--}}

                <label id="sort_prd_price" data-sort_prd_price="1">Sắp xếp theo giá
                    <ul>
{{--                        <li><a href="/{{$category->slug}}">Mặc định</a></li>--}}
                        <li><a href="?sort_prd_price=1">Tăng</a></li>
                        <li><a href="?sort_prd_price=0">Giảm</a></li>
                    </ul>
                </label>
            </div>
        </div>
    </div>


    @if(@$category->option_show_cate==0)
        @include('themestbd::childs.category.list_pro_cate')
    @else
        @include('themestbd::childs.category.colum_pro_cate')
    @endif

    <div class="f content_category_last">
        <div>
            {!! @$category->content !!}
        </div>

    </div>
    <div class="popup-add-cart" style="display: none">
        <div class="popup-add-cart-content">
            <p>Thêm thành công vào giỏ hàng!</p>
            <div class="btn-check">
                <a class="btn-cart btn" href="/gio-hang">Xem giỏ hàng</a>
                <button class="close-pop btn">Tiếp tục mua hàng</button>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('.close-pop').click(function(){
                $('.popup-add-cart').hide();
            });

            $('#sort_prd_price').click(function(){
                $(this).children('ul').toggle();
            });

        })
    </script>
    <script type="application/ld+json">
    {
      "@context": "https://schema.org/",
      "@type": "Store",
      "name": "{{@$category->name}}",
      "priceRange": "0-20.000.000 đ",
      "telephone": "{{$settings['phone']}}",
      "address": "{{$settings['address']}}",
      "image": [
        "{{'http://bephoangcuong.com' . \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($category,'100%','auto') }}"
       ],

      "description": "{!! $category->intro !!}",
      "review": {
        "@type": "Review",
        "reviewRating": {
          "@type": "Rating",
          "ratingValue": "4",
          "bestRating": "5"
        },
        "author": {
          "@type": "Person",
          "name": "Fred Benson"
        }
      },
      "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": "4.4",
        "reviewCount": "1963"
      }

    }
    </script>
@endsection


