<div style="text-align: center"><h3>Vừa có liên hệ mới gửi ở trang {{ $_SERVER['SERVER_NAME'] }}</h3></div>

<div style="text-align: center"><h4>Thông tin khách hàng</h4></div>
<div class="container-table100">
    <div class="wrap-table100">
        <div class="table100 ver1">
            <div class="wrap-table100-nextcols js-pscroll ps ps--active-x" style="width: 80%; margin: auto;">
                <div class="table100-nextcols">
                    <table style="border-collapse: collapse; width: 100%;margin: auto; border: 1px solid gray">
                        <thead>
                        <tr style="border: 1px solid gray;" class="row100 head">
                            <th style="border: 1px solid gray;" class="cell100 column2">Họ & tên</th>
                            <th style="border: 1px solid gray;" class="cell100 column3">SĐT</th>
                            <th style="border: 1px solid gray;" class="cell100 column4">Email</th>
                            <th style="border: 1px solid gray;" class="cell100 column4">Địa chỉ</th>
                            <th style="border: 1px solid gray;" class="cell100 column4">Lời nhắn</th>
                            <th style="border: 1px solid gray;" class="cell100 column4">Sản phẩm</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{ @$contact->name }}</td>
                            <td>{{ @$contact->tel }}</td>
                            <td>{{ @$contact->email }}</td>
                            <td>{{ @$contact->address }}</td>
                            <td>{!! @$contact->content !!}</td>
                            <td>
                                <?php $product = \Modules\ThemeSTBD\Models\Product::find($contact->product_id);?>
                                @if(is_object($product))
                                    <a href="{{ url(\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getProductSlug($product)) }}">{{ $product->name }}</a>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
