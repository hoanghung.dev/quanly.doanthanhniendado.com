<div style="text-align: center"><h3>Xin Chào: {{$user_name}}</h3></div>
<div style="text-align: center"><h3>Thông tin đơn hàng của bạn</h3></div>
<div class="container-table100">
    <div class="wrap-table100">
        <div class="table100 ver1">
            <div class="wrap-table100-nextcols js-pscroll ps ps--active-x" style="width: 80%; margin: auto;">
                <div class="table100-nextcols">
                    <table style="border-collapse: collapse; width: 100%;margin: auto; border: 1px solid gray">
                        <thead>
                        <tr style="border: 1px solid gray;" class="row100 head">
                            <th style="border: 1px solid gray;" class="cell100 column2">Tên sản phẩm</th>
                            <th style="border: 1px solid gray;" class="cell100 column3">Số lượng</th>
                            <th style="border: 1px solid gray;" class="cell100 column4">Giá</th>
                            <th>Quà tặng</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($getCart as $cart)
                            <tr style="border: 1px solid red; text-align: center;" class="row100 body">
                                <td style="border: 1px solid gray;" class="cell100 column2">{{$cart['name']}}</td>
                                <td style="border: 1px solid gray;" class="cell100 column3">{{$cart['quantity']}}</td>
                                <td style="border: 1px solid gray;" class="cell100 column4">{{number_format($cart['price'], 0, '.', '.')}} <sup>đ</sup></td>
                                <td class="qua-tang">
                                    <?php
                                    $product = \Modules\ThemeSTBD\Models\Product::find(@$cart['id']);
                                    $cate = \Modules\ThemeSTBD\Models\Category::whereIn('id', explode('|', @$product->multi_cat))->get()->first();
                                    $manufacture = \Modules\ThemeSTBD\Models\Manufacturer::find(@$product->manufacture_id);
                                    $getDateToday = date_create(date('Y-m-d H:i:s'));
                                    date_default_timezone_set('Asia/Ho_Chi_Minh');
                                    $getDateToday = date_create(date('Y-m-d H:i:s'));
                                    $sales = [];
                                    $sale = [];
                                    $product_all = \Modules\ThemeSTBD\Models\ProductSale::where(function ($query) use ($getDateToday) {
                                        $query->orWhere('time_start', '<=', date_format($getDateToday, 'Y-m-d H:i:s'));
                                        $query->orWhere('time_start', null);
                                    })->where(function ($query) use ($getDateToday) {
                                        $query->orWhere('time_end', '>=', date_format($getDateToday, 'Y-m-d H:i:s'));
                                        $query->orWhere('time_end', null);
                                    })->get();
                                    foreach ($product_all as $key => $sale_val) {
                                        $manufacture = \Modules\ThemeSTBD\Models\Manufacturer::whereIn('id', explode('|', @$sale_val->manufacturer_ids))->get();
                                        $cate_child = \Modules\ThemeSTBD\Models\Category::whereIn('id', explode('|', @$sale_val->category_ids))->get();
                                        foreach ($cate_child as $rt) {
                                            if ($rt->parent_id == 0 && in_array($rt->id, explode('|', @$sale_val->category_ids)) == true && in_array($rt->id, explode('|', @$product->multi_cat)) == true) {
                                                $sale = Modules\ThemeSTBD\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                                            } elseif (in_array($rt->id, explode('|', @$sale_val->category_ids)) == true && in_array($rt->id, explode('|', @$product->multi_cat)) == true) {
                                                $sale = Modules\ThemeSTBD\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                                            } else {
                                                $sale = [];
                                            }
                                        }
                                        if ($sale == []) {
                                            foreach ($manufacture as $rts) {
                                                if (in_array($rts->id, explode('|', @$sale_val->manufacturer_ids)) == true && in_array($rts->id, explode('|', @$product->manufacture_id)) == true) {
                                                    $sale = Modules\ThemeSTBD\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                                                } else {
                                                    $sale = [];
                                                }
                                            }
                                        }
                                        if (array_diff([@$product->id], explode('|', @$sale_val->id_product)) == []) {
                                            $sale = \Modules\ThemeSTBD\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                                        }
                                        if (!empty($sale)) {
                                            $sales = array_merge($sales, $sale->toArray());
                                        }
                                    }
                                    $ids = array_column($sales, 'id');
                                    $ids = array_unique($ids);
                                    $sales = array_filter($sales, function ($key, $value) use ($ids) {
                                        return in_array($value, array_keys($ids));
                                    }, ARRAY_FILTER_USE_BOTH);
                                    ?>
                                    @if(!empty($sales))
                                        @include('themestbd::partials.qua_tang')
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    <div style="text-align: right; border: 1px solid gray; border-top: none;"><p style="padding: 10px; margin: 0">Tổng: {{number_format($total_price, 0, '.', '.')}} <sup>đ</sup></p></div>
                </div>
                <div class="ps__rail-x" style="width: 563px; left: 0px; bottom: 10px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 160px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
        </div>
    </div>
</div>
