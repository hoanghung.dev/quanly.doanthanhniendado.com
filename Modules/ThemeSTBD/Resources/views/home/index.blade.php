@extends('themestbd::layouts.layout_homepage')
@section('main_content')
    <style>
        @media (min-width: 768px) {
            .tim {
                width: 72% !important;
            }
        }
        @media(min-width: 1300px) {
            .topic img, .topic object {
                display: inline-block;
            }
            @for($i = 0; $i < count($posts); $i++)
            @if($i % 2 == 0)
                .dd-{{ $i }} {
                display: inline-block;
                text-align: left;
            }
            .dd-{{ $i }} {
                display: inline-block;
                text-align: right;
            }
            @else
                .dd-{{ $i }} {
                display: inline-block;
                text-align: right;
            }
            .dd-{{ $i }} {
                display: inline-block;
                text-align: left;
            }
            @endif
            @endfor
        }
    </style>
    <div class="f f123" style="background:#fbfbfb;padding-bottom:30px;">
        @if(!empty($posts))
            @foreach($posts as $k => $post)
                @php
                    $cate_get_by_post = \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getFromCache('get_category_by_post');
                    if(!$cate_get_by_post){
                        $cate_get_by_post = Modules\ThemeSTBD\Models\Category::whereIn('id', explode('|', $post->multi_cat))->first();
                        \Modules\ThemeSTBD\Http\Helpers\CommonHelper::putToCache('get_category_by_post', $cate_get_by_post);
                    }
                @endphp
                <div class="topic">
                    <div class="ddimg flexL dd-{{ $k }}" style="order:{{$post->oder}};" id="nb0">
                        <img class="lazy0" src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($post->image, 675, 'auto')}}" alt="{{$post->name}}"/>
                    </div>
                    <div class="ddinfo padd1 dc-{{ $k }}" style="order:@if($post->oder == 1) 2 @else 1 @endif;">
                        <h2>{{$post->name}}</h2>
                        <p>
                        <p>
                            <strong>{{$post->intro}}</strong><br/>
                            {!!  $post->content !!}
                        </p>
                        <a class="hxn"
                           href=" {{route('cate.list', ['slug' => @$cate_get_by_post->slug])}}">Xem
                            ngay</a>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
    <script type="application/ld+json">
    </script>
@endsection
