<!DOCTYPE html>
<html lang="vi-vn" xml:lang="vi-vn">
@php
    function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
$dataShowrooms = \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getFromCache('get_showroom_footer');
    if (!$dataShowrooms){
        $dataShowrooms = \Modules\ThemeSTBD\Models\Showroom::all();
        \Modules\ThemeSTBD\Http\Helpers\CommonHelper::putToCache('get_showroom_footer', $dataShowrooms);
    }
$banner_top = \Modules\ThemeSTBD\Models\Banner::where('status',1)->where('location','banner_header_top')->first();
$banner_right = \Modules\ThemeSTBD\Models\Banner::where('status',1)->where('location','banner_header_right')->first();
$banner_left = \Modules\ThemeSTBD\Models\Banner::where('status',1)->where('location','banner_header_left')->first();
@endphp
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    @include('themestbd::partials.header_detail_script')
    @include('themestbd::partials.head_meta')
    @include('themestbd::partials.header_cate_script')
    @include('themestbd::partials.header_script')
    {{--    @if(isMobile())--}}
    {{--        <link rel="stylesheet" href="{{asset('public/frontend/themes/stbd/css/home.css')}}">--}}
    {{--        <link rel="stylesheet" href="{{asset('public/frontend/themes/stbd/css/style-responsive.css')}}">--}}
    {{--    @endif--}}

    <link rel="stylesheet" href="{{asset('public/frontend/themes/stbd/css/custom2.css')}}">
    {!! @$settings['head_code'] !!}



    <style>
        .tongdai {
            width: 20%;
        }
        header {
            padding: 0 !important;
        }

        .flexJus.hleft > a {
            line-height: 1;
        }

        /*header.f{*/
        /*    margin-top: -15px;*/
        /*}*/
        .f.regis {
            @if(@$settings['show_email_homepage']==0)
 display: none !important;
        @endif

        }

        header {
            @if(@$settings['header_background'] != '')  background: {{@$settings['header_background']}}  !important;
        @endif

        }

        footer {
            @if(@$settings['footer_background'] != '')  background: {{@$settings['footer_background']}}  !important;
        @endif

        }


        @if(@$settings['option_background']==0)
            body > div.f {
            background: none !important;
        }

        @elseif(@$settings['option_background']==1)
            @if(@$settings['background_color']!='')
                body > div.f {
            background-color: {{$settings['background_color']}}  !important;
        }

        @endif
        @elseif(@$settings['option_background']==2)
            @if(!empty($settings['background_image']))
                body > div.f {
            background-image: url('http://demo4.webhobasoft.com/public/filemanager/userfiles/{{$settings['background_image']}}') !important;
            background-attachment: fixed;
        }

        @endif
    @endif
 /*form.tim{*/
        /*    width: 70%;*/
        /*}*/
        /*.tongdai {*/
        /*    width: 50%;*/
        /*}*/

        .custom_fa:before {
            font-size: 24px !important;
        }

        blockquote {
            background: #0a95e2;
            display: inline-block;
            padding: 10px;
        }

        .nav-menu .nav-item {
            padding: 0 !important;
        }

        /*li.menu-san_pham:hover div#menu-show{*/
        /*    top: 100%!important;*/
        /*}*/
        #menu-show > .flexJus > .hover1 > span {
            border-bottom: none !important;
            padding: 0;
        }

        .head {

            height: 40px;
        }

        .contact_now_parent {
            position: fixed;
            bottom: 10px;
            right: 0;
            width: 50px;
            z-index: 999;
        }

        .fb_customer_chat_bubble_animated_no_badge {
            bottom: 295px !important;
            right: 0 !important;
        }

        .phone_now_detail {
            position: relative;
            display: inline-block;
        }

        .phone_now_detail {
            position: absolute;
            display: none;
            bottom: 5px;
            right: 100%;
            height: auto;
            width: max-content;
            z-index: 999;
            background: #fff;
            padding: 15px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        .contact_now-title {
            display: flex;
            height: 30px;
            line-height: 30px;
            margin-bottom: 10px;
        }

        .contact_now-title > span {
            width: 100%;
            margin-right: 10px;
        }

        .phone_now, .zalo_now, .contact_now-title {
            cursor: pointer;
        }

        .zalo_now {
            position: relative;
        }

        .zalo_now > span {
            display: none;
        }

        .zalo_now > span {
            position: absolute;
            top: 5px;
            right: 100%;
            width: max-content;
            background: #fff;
            border: 1px solid #ccc;
            /*display: block;*/
            padding: 10px;
        }

        nav.a {
            width: 500px !important;
        }



        .banner_header_right img,.banner_header_left img{
            max-width: 100%;
            max-height: 100%;

        }

        .banner_header_right{
            position: fixed;
            top: 10%;
            right: 0;
            width: 100px;
            height: 500px;
            /*background: #ccc;*/
        }
        .banner_header_left{
            position: fixed;
            top: 10%;
            left: 0;
            width: 100px;
            max-height: 500px;
        }
        @media (max-width: 1770px) {

            .banner_header_right,.banner_header_left{
                display: none;
            }
        }

    </style>
</head>
<body>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            xfbml: true,
            version: 'v6.0'
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
@if(!empty($banner_right))
<div class="banner_header_right">
    <a href="{{$banner_right->link}}">
    <img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($banner_right->iamge, '100%', 'auto')}}"
         alt="" width="100%">
    </a>
</div>
@endif
@if(!empty($banner_left))
<div class="banner_header_left">
    <a href="{{$banner_left->link}}">
    <img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($banner_left->image, '100%', 'auto')}}"
         alt="" width="100%">
    </a>
</div>
@endif
<div class="contact_now_parent">
    <div class="contact_now">
        <ul>
            <li class="fb_now">
                <div class="fb-customerchat"
                     attribution=setup_tool
                     page_id="169469587148351"
                     theme_color="#0084ff"
                     logged_in_greeting="Em chào anh / chị ạ. Mình đang cần tư vấn về sản phẩm gì ạ ?"
                     logged_out_greeting="Em chào anh / chị ạ. Mình đang cần tư vấn về sản phẩm gì ạ ?">
                </div>
                <div id="fb-root"></div>

            </li>
            <li class="phone_now">
                <img src="https://i.imgur.com/fmhOHUr.gif" width="50px">
            </li>
            <li class="zalo_now">
                <a href="http://zalo.me/0974329191" target="_blank">
                    <img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb('1.logo/zalo.png', '50px', 'auto')}}"
                         alt="">

                </a>
                <span>Zalo: 0974 32 91 91</span>
            </li>
        </ul>
        <div class="phone_now_detail">
            @foreach($dataShowrooms as $k=>$dataShowroom)
                <?php
                $str = explode('<p>', $dataShowroom->address);
                $phone = explode('<strong>', $str[2]);

                $phone = trim($phone[1], '</strong></p>');
                ?>

                <div class="phone_now_detail1">
                    <div class="contact_now-title">
                        <span>{!! trim($str[1],'Địa chỉ : ') !!}</span>
                        <a href="tel:{{$phone}}">
                            <img src="https://i.imgur.com/fmhOHUr.gif" width="30px">
                        </a>
                    </div>

                </div>
            @endforeach
        </div>
    </div>

</div>
<script>
    $(document).ready(function () {
        $("blockquote").before('<i class="fa fa-quote-left fa-pull-left custom_fa" STYLE="color: #ccc;    margin-top: 10px;"  aria-hidden="true"></i>');


        $(".phone_now").click(function () {
            $('.contact_now .phone_now_detail').toggle();
        });
        $(".zalo_now").hover(function () {
            $('.zalo_now>span').toggle();
        });
    });

    $(window).scroll(function (event) {
        let scrollHeight = $(window).scrollTop();
        if (scrollHeight > 280) {
            $('.contact_now_parent').css({
                'bottom': '180px'
            });
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').css({
                'bottom': '295px!important'
            });
        } else {
            $('.contact_now_parent').css({
                'bottom': '10px'
            });
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').css({
                'bottom': '130px!important'
            });
        }
    });
</script>

{{--@if(isMobile())--}}
{{--    @include('themestbd::partials.menu_home')--}}
{{--    @include('themestbd::partials.menu_cate_home')--}}
{{--    @if(@$settings['option_menu'] == 0)--}}
{{--        @include('themestbd::partials.menu_efect.menu_slidedown')--}}
{{--    @elseif(@$settings['option_menu'] == 2)--}}
{{--        @include('themestbd::partials.menu_efect.menu_default')--}}
{{--    @endif--}}
{{--@else--}}
{{--    @include('themestbd::partials.menu_cate_master')--}}

@if(!empty($banner_top))
<a href="{{$banner_top->link}}">
    <img src=" /public/filemanager/userfiles/{{ $banner_top->image  }}" style="margin: auto;display: block;" alt="{{ $banner_top->name  }}">
</a>
@endif
@if(@$settings['option_menu'] != 2)
    @include('themestbd::partials.menu_home')
@endif
@if(@$settings['option_menu'] == 0)

    @include('themestbd::partials.menu_efect.menu_slidedown')
@elseif(@$settings['option_menu'] == 2)
    @include('themestbd::partials.menu_cate_master')
    {{--        @include('themestbd::partials.menu_efect.menu_default')--}}
@endif
{{--@endif--}}
@yield('main_content')

@include('themestbd::partials.set_font')
{{--@include('themestbd::partials.footer_colum')--}}
@if(@$settings['footer_content'] == 1)
    @include('themestbd::partials.footer_colum')
@else
    @include('themestbd::partials.footer')
@endif
@include('themestbd::partials.footer_script')
@include('themestbd::partials.contact_us')
<div id="popup">
    <span class="button b-close"><span>X</span></span>
    <div id="Map"></div>
</div>
{!! @$settings['footer_code'] !!}


</body>
</html>