@php
    $brans = \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getFromCache('get_brans');
    if (!$brans){
        $brans = \Modules\ThemeSTBD\Models\Manufacturer::where('status', 1)->orderBy('order_no', 'asc')->get();
        \Modules\ThemeSTBD\Http\Helpers\CommonHelper::putToCache('get_brans', $brans);
    }
@endphp
<div class="f">
    <div class="b">
        <div class="f flexCen list-launcher">
            <div class="bra"><img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo_brand'], 230, 'auto')}}"alt="brands" /></div>
            <div class="flexCol">
                <span class="intro-thuonghieu">{{ @$settings['logo_brand_intro'] }}</span>
                <span class="bg_h bghtop"></span>
                @if(@$settings['show_bran_homepage'] == 1)
                <div>
                    @if(!empty($brans))
                        @foreach($brans as $data)
                            <a href="{{route('manufacturer.detail', ['slug' => $data->slug])}}" title="{{$data->name}}">
                                <img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($data->image, 153, 'auto')}}" alt="{{$data->name}}">
                            </a>
                        @endforeach
                    @endif
                </div>
                <span class="bg_h bghbot"></span>
                @endif
            </div>
        </div>
    </div>
</div>
