<div style="clear: both"></div>
<ul class="breadcrumb f">
    <?php
    $slug_arr = explode('/', $_SERVER['REQUEST_URI']);
    $breadcrumb = [
        '/' => 'Trang chủ'
    ];
    $url = '';
    foreach ($slug_arr as $slug) {
        if ($slug != '') {
            if (strpos($slug, '.html')) {
                $slug = str_replace('.html', '', $slug);
                $product = \Modules\ThemeSTBD\Models\Product::select('name')->where('slug', $slug)->first();
                if (is_object($product)) {
                    $url .= '/' . $slug;
                    $breadcrumb[$url] = $product->name;
                } else {
                    $post = \Modules\ThemeSTBD\Models\Post::select('name')->where('slug', $slug)->first();
                    if (is_object($post)) {
                        $url .= '/' . $slug;
                        $breadcrumb[$url] = $post->name;
                    }
                }
            }elseif($slug == 'chinh-hang'){
                $url .= '/' . $slug;
                $breadcrumb[$url] = 'Chính hãng';
            } else {
                $cat = \Modules\ThemeSTBD\Models\Category::select('name')->where('slug', $slug)->first();
                if (is_object($cat)) {
                    $url .= '/' . $slug;
                    $breadcrumb[$url] = $cat->name;
                } else {
                    $manu = \Modules\ThemeSTBD\Models\Manufacturer::select('name')->where('slug', $slug)->first();
                    if (is_object($manu)) {
                        $url .= '/' . $slug;
                        $breadcrumb[$url] = $manu->name;
                    }
                }
            }
        }
    }
    ?>
    @foreach($breadcrumb as $slug => $name)
        <li itemscope="itemscope" itemtype="">
            <a itemprop="url" href="{{ url($slug) }}" title="{{ $name }}">
                <span itemprop="title">{{ $name }}</span></a></li>
    @endforeach
</ul>