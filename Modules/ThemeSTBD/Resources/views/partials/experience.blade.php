@php
    $category = \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getFromCache('get_category_by_experi');
    if (!$category){
        $category = Modules\ThemeSTBD\Models\Category::where('status', 1)->where('type', 1)->where('show_homepage', 1)->orderBy('order_no', 'asc')->take(1)->first();
        \Modules\ThemeSTBD\Http\Helpers\CommonHelper::putToCache('get_category_by_experi', $category);
    }
     $getIdCate = \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getFromCache('get_all_index');
        if (!$getIdCate){
            $getIdCate = \Modules\ThemeSTBD\Models\Post::all()->pluck('multi_cat', 'id');
            \Modules\ThemeSTBD\Http\Helpers\CommonHelper::putToCache('get_all_index', $getIdCate);
        }
        $data_id = '|';
        if ($category) {
            if ($category->parent_id == 0 || $category->parent_id == null) {
                $data['category_childs'] = Modules\ThemeSTBD\Models\Category::where('parent_id', $category->id)->get();
                $data['category_childs']->push($category);
                foreach ($data['category_childs'] as $val) {
                    foreach ($getIdCate as $key => $val1) {
                        $t = explode('|', $val1);
                        if (in_array($val->id, $t) == true) {
                            $data_id .= $key . '|';
                        }
                    }
                }
            } else {
                foreach ($getIdCate as $key => $val1) {
                    $t = explode('|', $val1);
                    if (in_array($category->id, $t) == true) {
                        $data_id .= $key . '|';
                    }
                }
            }
        }
        $data_id = explode('|', $data_id);
        $data_id = array_unique($data_id);
        $arrayCate_id = [];
        if (!empty($category)){
            array_push($arrayCate_id, $category->id);
        }
   if (!empty($category)){
    $posts = \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getFromCache('get_posts_by_experi');
    if (!$posts){
        $posts = \Modules\ThemeSTBD\Models\Post::whereIn('id', $data_id)->where('status', 1)->limit(4)->orderBy('id', 'decs')->get();
        \Modules\ThemeSTBD\Http\Helpers\CommonHelper::putToCache('get_posts_by_experi', $posts);
    }
   }
@endphp
<style>
    .newest .flexnew{
        margin: 20px 0!important;
        border-left: 5px solid #ccc;
        border-left: 5px solid red;
        background: #eee;
        padding: 10px 0 10px 10px;
    }
    .newest{
        padding: 0 15px!important;
        max-width: unset!important;
    }
</style>
<div class="f bnews">
    <div class="b newest">
        @if(!empty($category))
            <div class="f flexL flexnew"><label>{{ucfirst(mb_strtoupper($category->name))}}</label><a style="float: right"
                        href="{{route('cate.list', ['slug' => $category->slug])}}">Xem
                    thêm ...</a></div>
            <ul class="f padb">
                @foreach($posts as $post)
                    <li>
                        <a href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getPostSlug($post) }}"
                           title="{{$post->name}}">
                            <div style="height: 250px!important;">
                                <img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($post->image, 300, 'auto')}}"
                                     alt="{{$post->name}}"/></div>
                            <h4>{{str_limit($post->name, 50)}}</h4>
                        </a>
                        <div class="bsum">{{str_limit($post->intro, 180)}}</div>
                        <a class="more" title="{{$post->name}}"
                           href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getPostSlug($post) }}">Tìm hiểu
                            thêm</a>
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
</div>

