<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/home.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/footer.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/slide.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/detailproduct.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/frontend/css/style-responsive.css')}}">
<script type="text/javascript" src="{{URL::asset('public/frontend/themes/stbd/front/js/jquery.js')}}"></script>
<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "Store",
      "image": [
        "https://bephoangcuong.com/timthumb.php?src=https://bephoangcuong.com/public/filemanager/userfiles/1.logo/BepHC-logo-Web8.png"
       ],
      "@id": "https://bephoangcuong.com/",
      "name": "Bếp Hoàng Cương",
      "address": {
        "@type": "PostalAddress",
        "streetAddress": "268 Tây Sơn - Quận Đống Đa Hà Nội",
        "addressLocality": "Quận Đống Đa",
        "addressRegion": "Hà Nội",
        "postalCode": "100000",
        "addressCountry": "VN"
      },
      "url": "https://bephoangcuong.com/store-locator/sl/San-Jose-Westgate-Store/1427",
      "priceRange": "vnđ",
      "telephone": "0974 32 91 91",
      "openingHoursSpecification": [
        {
          "@type": "OpeningHoursSpecification",
          "dayOfWeek": [
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday"
          ],
          "opens": "08:00",
          "closes": "21:00"
        },
        {
          "@type": "OpeningHoursSpecification",
          "dayOfWeek": "Sunday",
          "opens": "08:00",
          "closes": "21:00"
        }
      ]
    }
    </script>
