<?php
$cates = Modules\ThemeSTBD\Models\Category::where('status', 1)
    ->where('type', 5)->where('show_menu', 1)->orderBy('order_no', 'asc')
    ->where(function ($query) {
        $query->where('parent_id', 0)->orwhere('parent_id', null);
    })->get();

$dataShowrooms = \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getFromCache('get_showroom_footer');
if (!$dataShowrooms) {
    $dataShowrooms = Modules\ThemeSTBD\Models\Showroom::all();
    \Modules\ThemeSTBD\Http\Helpers\CommonHelper::putToCache('get_showroom_footer', $dataShowrooms);
}
$p = [];
$c = ['values' => [], 'location' => []];
foreach ($dataShowrooms as $key => $t) {
    array_push($p, mb_strtoupper($t['location']));
    foreach ($p as $key => $l) {
        $p = array_unique($p);
    }
}
foreach ($p as $keyp => $l) {
    array_push($c['values'], [$l => []]);
    array_push($c['location'], $l);
    foreach ($dataShowrooms as $key => $t) {
        if (mb_strtoupper($l) == mb_strtoupper($t['location'])) {
            array_push($c['values'][$keyp][$l], $t);
        }
    }
}
$fg = [];
foreach ($c['values'] as $f) {
    foreach ($f as $g) {
        foreach ($g as $gj) {
            $fg[] = $gj;
        }
    }
}
?>
<style>
    @media (max-width: 767px){
        .f .nav .flexJus>a.hover1 {
            width: 100%;
        }
    }

    .nav-menu .nav-item .nav-link{
        padding: 0.5rem 5px!important;
    }
    .nav a {
        text-align: left!important;
        width: 100%;
        margin: 0!important;
    }
    .nav span.menu-hover {
        display: inline;
        line-height: 30px;
        /*border-bottom: 1px dotted #ccc!important;*/
    }
    .nav-menu .nav-item {
        padding: 3px 0;
    }
    .flexJus>a.hover1:hover{
        border-left: 5px solid {{(@$settings['header_background'] != '')? @$settings['header_background']: '#69a4f0' }};
    }
    .nav span.menu-hover{
        margin: 0 10px;
        font-size: 15px!important;
        text-transform: capitalize;
    }
    .nav span.menu-hover:hover{

        background: none!important;
        color: #000;
    }
    .hleft div a{
        padding: 0!important;
    }
    div#menu-show{
        display: none;
        background: #fff;
    }
    .menu-san_pham {
        position: relative;
    }
    li.menu-san_pham:hover div#menu-show{
        position: absolute;
        display: block!important;
{{--        left: {{(@$settings['logo_position'] == 0)?'0':'-20vw'}};--}}
        top: 100%;
        width: {{(@$settings['logo_position'] == 1)?'700%':'350px'}};;
        height: max-content;
        z-index: 999;
    }
    .head {

        height: 40px;
    }
    .hover1>img{
        width: 30px!important;
        display: inline;
        height: 30px!important;
        line-height: 30px;

    }
    a.hover1 {
        border-bottom: 1px dotted #ddd;
    }
    a.hover1:hover {
        font-weight: 900;
    }
</style>
<div class="f nav" id="menu-show">
    <div class="b flexJus" style="display: block; padding: 0;">
        @foreach($cates as $cate)
            <a class="hover1" re href="{{route('cate.list', ['slug' => $cate->slug])}}" title="{{$cate->name}}">
                <img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($cate->image, 100, 'auto')}}"
                     alt="{{$cate->name}}"/>
                <span class="menu-hover">{{$cate->name}}</span>
            </a>
        @endforeach
    </div>
    <div class="f mcontact">
        <p><a class="fa-phone" href="tel:{{@$settings['phone']}}" rel="nofollow">Liên hệ mua hàng:
                <strong>{!! @$settings['hotline'] !!}</strong></a>
        </p>
        <p><a class="fa-phone" href="tel:{{@$settings['phone_bh']}}" rel="nofollow">Bảo hành:
                <strong>{{@$settings['phone_bh']}}</strong></a>
        </p>
        <p><a class="fa-phone" href="tel:{{@$settings['phone_kn']}}" rel="nofollow">Khiếu nại:
                <strong>{{@$settings['phone_kn']}}</strong></a>
        </p>
        @if(!empty($c))
            @foreach($c['location'] as $location)
                @php $shoroomLocation = Modules\ThemeSTBD\Models\Showroom::where('location', $location)->orWhere('location', mb_strtolower($location))->orWhere('location', ucfirst($location))->orWhere('location', ucwords($location))->get();   @endphp
                <p><a class="fa-showroom"
                      href="{{route('showroom.list', ['slug' => Modules\ThemeSTBD\Http\Helpers\CommonHelper::convertSlug($location)])}}"
                      title="hệ thống showroom">Xem địa chỉ {{count($shoroomLocation)}} Showroom tại {{$location}}</a>
                </p>
            @endforeach
        @endif
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#touch-menu').click(function () {
            $('#menu-show').slideToggle();
            icon = $('header').find("#touch-menu");
            icon.toggleClass("touch-menu mclo")
        })
    })

</script>