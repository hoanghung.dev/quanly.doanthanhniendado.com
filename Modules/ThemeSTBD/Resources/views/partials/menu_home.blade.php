@php
    $menus = \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getFromCache('getFromCache');
    if (!$menus){
            $menus = \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getMenusByLocation('main_menu', 5, false);
            \Modules\ThemeSTBD\Http\Helpers\CommonHelper::putToCache('get_menus_by_home', $menus);
    }
@endphp
@if(@$settings['option_menu'] == 0)
<style>

    .head {
        height: 40px;
    }
</style>
@endif
<header class="f">
    <div class="f head">
        <div class="b flexJus">
            <div class="flexJus hleft">
                @if(@$settings['logo_position']==1)
                    @include('themestbd::partials.logo')
                @endif
                <nav class="a">
                    <ul id="menu" class="nav-menu">
                        @if(!empty($menus))
                            @foreach($menus as $menu)
                                @if($menu['link'] == '#')
{{--                                    @if(@$settings['option_menu'] != 2)--}}
                                        <li class="nav-item menu-{{ str_slug($menu['name'], '_') }}">
                                            <a class="nav-link" href="javascript:;" @if($menu['name'] != 'Sản phẩm') onclick="showmenudv();" @endif title="{{$menu['name']}}">{{$menu['name']}}</a>
                                            @if(!isMobile())
                                                @if(@$settings['option_menu'] == 1 && str_slug($menu['name'], '_')=='san_pham')
                                                    @include('themestbd::partials.menu_efect.menu_dropdown')
                                                @endif
                                            @endif
                                        </li>
{{--                                    @endif--}}
                                @else
                                    <li class="nav-item menu-{{ str_slug($menu['name'], '_') }}">
                                        <a class="nav-link" href="{{ $menu['link'] != '#' ? $menu['link'] : 'javascript:;' }}" rel="nofollow" title="{{$menu['name']}}">{{$menu['name']}}</a>
                                    </li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                </nav>
                @if(@$settings['logo_position']==0)
                    @include('themestbd::partials.logo')
                @endif
                <div style="clear: both; "></div>
                <div class="tongdai">
                    {!! @\Modules\ThemeSTBD\Models\Widget::where('location', 'tongdai')->where('status', 1)->first()->content !!}
                </div>
            </div>
            <div class="flexR hright">
                @include('themestbd::partials.search')
                @include('themestbd::partials.cart_total')
            </div>
        </div>
    </div>
@include('themestbd::partials.show_dv')
    <i id="touch-menu" class="touch-menu"></i>
    @if(isMobile())
        @if(@$settings['option_menu'] == 1)
            @include('themestbd::partials.menu_efect.menu_dropdown')
        @endif
    @endif
</header>

