<div class="f bgwhite ld2-chinhsach">
    <div class="b flexCol">
        <label class="title-section">Chính sách bán hàng của {{ @$settings['name'] }}</label>
        <ul class="list-profit">
            {!! @$settings['policy_child'] !!}
        </ul>
    </div>
</div>
