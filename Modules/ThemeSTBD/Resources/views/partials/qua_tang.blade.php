@if(count($sales) > 0)
    <div class="qua-content">
        <label>Quà tặng</label>
        <span class="qua-anh"><img style="width: 60px; height: 60px;"
                                   src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo_qua_tang'], 60, 'auto')}}"
                                   alt="Qùa tặng"/></span>
        <span class="gift_content">
                              @php  $totalPrice = 0; @endphp
            <ul>

                @foreach(Session::get('cart') as $data1)
                    <li>
                        @if($data1['id']=$data['id'])
                    - {{@$data1['gift']}}
                        @endif
                </li>
{{--                    @php $totalPrice += $data['base_price'];  @endphp--}}
                @endforeach
            </ul>
            Trị giá: <b>{{number_format($totalPrice, 0, '.', '.')}} <sup>đ</sup></b>
        </span>
    </div>
@endif