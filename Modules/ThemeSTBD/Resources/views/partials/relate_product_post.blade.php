<?php
$relate_products = \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getFromCache('get_relate_products');
if (!$relate_products) {
    $relate_product_id = explode('|', trim(@$post->related_products, '|'));
    $relate_products = \Modules\ThemeSTBD\Models\Product::whereIn('id', $relate_product_id)->where('status', 1)->orderBy('order_no', 'asc')->get();
    \Modules\ThemeSTBD\Http\Helpers\CommonHelper::putToCache('get_relate_products', $relate_products);
}
$products_hots = \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getFromCache('get_products_hot');
if (!$products_hots) {
    $products_hots = \Modules\ThemeSTBD\Models\Product::where('featured', 1)->where('status', 1)->take(5)->orderBy('order_no', 'asc')->get();
    \Modules\ThemeSTBD\Http\Helpers\CommonHelper::putToCache('get_products_hot', $products_hots);
}

?>
<div class="newsright bgghi">
    <h4 style="
    text-align: center;
    padding: 15px;
    font-size: 20px;
    text-transform: uppercase;
    background: #308741;
">Sản phẩm liên quan</h4>
    <ul class="news-right2" style="background: #fff; border: 1px solid #fff576">
        @if(!empty($relate_products))
            @foreach($relate_products as $relate_product)
                <li style="border-bottom: 1px solid #ccc; display: flex;padding: 5px!important;">
                    <a class="img-news" style="width: 30%"
                       href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getProductSlug($relate_product, 'array') }}"
                       title="{{$relate_product->name}}">
                        <img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($relate_product->image, 300, 'auto')}}"
                             alt="{{$relate_product->name}}">
                    </a>
                    <div class="title_price_pro" style="width: 70%">
                        <h3 style="margin: 0; padding: 0 5px;    line-height: 1;">
                            <a href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getProductSlug($relate_product, 'array') }}"
                               title="{{$relate_product->name}}">{{$relate_product->name}}</a>
                        </h3>
                        <strike>{{number_format($relate_product->base_price, 0,'','.')}}<sup>đ</sup></strike>
                        <p style="font-weight: bold;color: red">{{number_format($relate_product->final_price, 0,'','.')}}
                            <sup>đ</sup></p>
                    </div>


                </li>
            @endforeach
        @endif
    </ul>
</div>
<div class="newsright bgghi">
    <h4 style="
    text-align: center;
    padding: 15px;
    font-size: 20px;
    text-transform: uppercase;
    background: #da2028;
">Sản phẩm bán chạy</h4>
    <ul class="news-right2" style="background: #fff;border: 1px solid #fff576">
        @if(!empty($products_hots))
            @foreach($products_hots as $products_hot)
                <li style="border-bottom: 1px solid #ccc; display: flex;padding: 5px!important;">
                    <a class="img-news" style="width: 30%"
                       href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getProductSlug($products_hot, 'array') }}"
                       title="{{$products_hot->name}}">
                        <img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($products_hot->image, 300, 'auto')}}"
                             alt="{{$products_hot->name}}">
                    </a>
                    <div class="title_price_pro" style="width: 70%">
                        <h3 style="margin: 0; padding: 0 5px;    line-height: 1;">
                            <a href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getProductSlug($products_hot, 'array') }}"
                               title="{{$products_hot->name}}">{{$products_hot->name}}</a>
                        </h3>
                        <strike>{{number_format($products_hot->base_price, 0,'','.')}}<sup>đ</sup></strike>
                        <p style="font-weight: bold;color: red">{{number_format($products_hot->final_price, 0,'','.')}}
                            <sup>đ</sup></p>
                    </div>


                </li>
            @endforeach
        @endif
    </ul>
</div>