@php

     $slug = urldecode(last(request()->segments()));
     $category = \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getFromCache('get_category_by_post'.$slug);
    if (!$category){
        $category = Modules\ThemeSTBD\Models\Category::where('slug', $slug)->where('status', 1)->first();
        \Modules\ThemeSTBD\Http\Helpers\CommonHelper::putToCache('get_category_by_post'.$slug, $category);
    }
     $newPosts = \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getFromCache('get_newpost_by_post');
    if (!$newPosts){
        $newPosts = \Modules\ThemeSTBD\Models\Post::where('status', 1)->where('slug', '<>', '#')
                                     ->orderBy('id', 'desc')->where('type_page', '<>', 'page_static')->limit(5)->get();
        \Modules\ThemeSTBD\Http\Helpers\CommonHelper::putToCache('get_newpost_by_post', $newPosts);
    }
     $postViews = \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getFromCache('get_postviews_by_post');
    if (!$postViews){
        $postViews = \Modules\ThemeSTBD\Models\Post::where('status', 1)->where('slug', '<>', '#')
                                     ->orderBy('view_total', 'desc')->where('type_page', '<>', 'page_static')->limit(5)->get();
        \Modules\ThemeSTBD\Http\Helpers\CommonHelper::putToCache('get_postviews_by_post', $postViews);
    }
@endphp
<!--chi danh cho tin thuoc trang tin hay-->
{{--<div class="newsright bgghi">--}}
    {{--<ul class="news-right2">--}}
        {{--@if(!empty($newPosts))--}}
            {{--@foreach($newPosts as $newPost)--}}
                {{--<li>--}}
                    {{--<a class="img-news" href="{{route('cate.list', ['slug' => $newPost->slug])}}.html" title="{{$newPost->name}}">--}}
                        {{--<img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($newPost->image, 300, 'auto')}}" alt="{{$newPost->name}}"></a>--}}
                    {{--<h3><a href="{{route('cate.list', ['slug' => $newPost->slug])}}.html" title="{{$newPost->name}}">{{$newPost->name}}</a></h3>--}}
                {{--</li>--}}
            {{--@endforeach--}}
        {{--@endif--}}
    {{--</ul>--}}
{{--</div>--}}
<!--chi danh cho tin thuoc trang tin hay-->
<div class="newsright">
    <label>Tư vấn xem nhiều nhất</label>
    <ul class="news-list">
        @if(!empty($postViews))
            @foreach($postViews as $key => $postView)
                    @if($postView->slug != '#')
                    <li>
                        <a href="{{route('cate.list', ['slug' => $postView->slug])}}.html" title="Nên mua bếp từ hay bếp hồng ngoại">{{$postView->name}}</a>
                        <span>{{$key+1}}</span>
                    </li>
                    @endif
            @endforeach
        @endif
    </ul>
</div>
<div id="endright" style="float: left; width: 100%;"></div>

<script>
   $(document).ready(function () {
       $('#menu_catalog ul a').each(function () {
           if ($(this).hasClass('a2act')){
               $(this).parents('ul').css('display', 'block');
               $(this).parents('ul').prev('a').addClass('a2act');
           }
       })
   })
</script>