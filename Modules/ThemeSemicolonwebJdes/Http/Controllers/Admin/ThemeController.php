<?php

namespace Modules\ThemeSemicolonwebJdes\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Helpers\CommonHelper;
use App\Models\Setting;
use Illuminate\Http\Request;

class ThemeController extends CURDBaseController
{
    protected $module = [
        'code' => 'setting',
        'label' => 'Cấu hình theme',
        'modal' => '\App\Models\Setting',
        'tabs' => [
            'homepage_tab' => [
                'label' => 'Trang chủ',
                'icon' => '<i class="kt-menu__link-icon flaticon-settings-1"></i>',
                'intro' => 'Tùy chỉnh hiển thị các khối ở trang chủ',
                'td' => [
                    ['name' => 'slides', 'type' => 'inner', 'label' => 'Slides', 'html' => '<a href="/admin/banner?search=true&location=banners_home">Quản lý slides</a>'],
                    ['name' => 'category_post_ids', 'type' => 'select2_ajax_model', 'label' => 'Danh mục tin tức hiển thị', 'model' => \Modules\JdesProduct\Models\Category::class,
                        'object' => 'category_post', 'display_field' => 'name', 'multiple' => true, 'where' => 'type=1', 'des' => 'Danh mục đầu tiên chọn là danh mục chính'],
                    ['name' => 'category_product_ids', 'type' => 'select2_ajax_model', 'label' => 'Danh mục sản phẩm hiển thị', 'model' => \Modules\JdesProduct\Models\Category::class,
                        'object' => 'category_post', 'display_field' => 'name', 'multiple' => true, 'where' => 'type=5', 'des' => 'Danh mục đầu tiên chọn là danh mục chính'],
                ]
            ],
            'social_tab' => [
                'label' => 'Mạng xã hội',
                'icon' => '<i class="kt-menu__link-icon flaticon-settings-1"></i>',
                'intro' => '',
                'td' => [
                    ['name' => 'fanpage', 'type' => 'text', 'label' => 'Fanpage FB', 'des' => 'Fanpage Facebook'],
                    ['name' => 'google_map', 'type' => 'text', 'label' => 'google_map', 'des' => 'Fanpage Facebook'],
                    ['name' => 'skype', 'type' => 'text', 'label' => 'skype', 'des' => 'Fanpage Facebook'],
                    ['name' => 'instagram', 'type' => 'text', 'label' => 'instagram', 'des' => 'Fanpage Facebook'],
                ]
            ],
        ]
    ];

    public function setting(Request $request)
    {

        $data['page_type'] = 'list';

        $module = \Eventy::filter('theme.custom_module', $this->module);
        if (!$_POST) {
            $listItem = $this->model->get();
            $tabs = [];
            foreach ($listItem as $item) {
                $tabs[$item->type][$item->name] = $item->value;
            }
            #
            $data['tabs'] = $tabs;
            $data['page_title'] = $module['label'];
            $data['module'] = \Eventy::filter('theme.custom_module', $module);
            return view(config('core.admin_theme') . '.setting.view')->with($data);
        } else {
            foreach ($module['tabs'] as $type => $tab) {
                $data = $this->processingValueInFields($request, $tab['td'], $type . '_');

                //  Tùy chỉnh dữ liệu insert
                if (isset($data['category_post_ids'])) {
                    $data['category_post_ids'] = '|' . implode('|', $data['category_post_ids']) . '|';
                }
                if (isset($data['category_product_ids'])) {
                    $data['category_product_ids'] = '|' . implode('|', $data['category_product_ids']) . '|';
                }
                #

                foreach ($data as $key => $value) {
                    $item = Setting::where('name', $key)->first();
                    if (!is_object($item)) {
                        $item = new Setting();
                        $item->name = $key;
                        $item->type = $type;
                    }
                    $item->value = $value;
                    $item->save();
                }
            }
            CommonHelper::one_time_message('success', 'Cập nhật thành công!');

            if ($request->return_direct == 'save_exit') {
                return redirect('admin/dashboard');
            }

            return back();
        }
    }
}
