<?php

namespace Modules\ThemeSemicolonwebJdes\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Modules\ThemeSemicolonwebJdes\Models\{Company, Order, Category, Product};
use Cart;


class CartController extends Controller
{
    function GetCart()
    {
        $data['cart'] = Cart::content();
        $data['total'] = Cart::total(0, '', ',');

        return view('themesemicolonwebjdes::pages.product.cart', $data);
    }

    public function AddCart(request $r, $id)
    {

        $product = product::find($id);

        $qty = $r->get('qty', 1);

        if ($r->get('key_editor_log') == null) {
            $product_price = $product->final_price;
        } else {
            $orderRowController = new \Modules\JdesSetting\Http\Controllers\Admin\OrderRowController();
            $order = \Modules\JdesSetting\Models\Editor::find($r->get('editor_id'));
            $key_editor_log = '_remote_addr_' . $_SERVER['REMOTE_ADDR'];
            $product_price = $orderRowController->getPriceObject($order->id, 'order', $key_editor_log);
        }

        Cart::add([
            'id' => $product->id,
            'name' => $product->name,

            'qty' => $qty,
            'price' => $product_price,
            'options' => [
                'image' => $product->image,
                'company_id' => $product->company_id,
                'key_editor_log' => $r->get('key_editor_log', null),
                'editor_id' => $r->get('editor_id', null)
            ]
        ]);
        return redirect('/gio-hang');
    }

    public function DelItem($rowId)
    {
        Cart::remove($rowId);
        return redirect()->back();
    }

    public function UpdateItem($rowId, $qty)
    {
        Cart::update($rowId, $qty);

    }


}
