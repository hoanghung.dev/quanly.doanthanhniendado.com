<?php

namespace Modules\ThemeSemicolonwebJdes\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use Modules\ThemeSemicolonwebJdes\Http\Helpers\CommonHelper;
use Modules\ThemeSemicolonwebJdes\Models\Company;
use Modules\ThemeSemicolonwebJdes\Models\Contact;

class ContactController extends Controller
{
    function getContact()

    {
        return view('themesemicolonwebjdes::pages.contact.contact-2');
    }

    function postContact(request $r)
    {
        if ($r->ajax()) {
            $contact = new contact;
            $contact->name = @$r->name;
            $contact->email = @$r->email;
            $contact->tel = @$r->phone;
            $contact->address = @$r->address;
            $contact->content = @$r->message;

            $contact->save();
            Mail::send('themesemicolonwebjdes::pages.email', ['contact' => $contact], function ($message) use ($contact) {
                $message->from(env('MAIL_USERNAME'), 'jdes');
                $message->to($contact->email, 'Khách hàng')->subject('Phản hồi');
            });
            return response()->json([
                'status' => true,
            ]);
        }
    }
    function GetContact2($comSlug)
    {
        $data['comSlug']=$comSlug;
        return view('themesemicolonwebjdes::pages.contact.contact-2',$data);
    }
    function PostContact2(request $r,$comSlug)
    {

        if ($r->ajax()) {
            $contact = new contact;
            $contact->name = @$r->name;
            $contact->email = @$r->email;
            $contact->tel = @$r->phone;
            $contact->address = @$r->address;
            $contact->content = @$r->message;
            $contact->company_id = Company::where('slug',$comSlug)->first()->id;
            $contact->save();

            Mail::send('themesemicolonwebjdes::pages.email', ['contact' => $contact], function ($message) use ($contact) {
                $message->from(env('MAIL_USERNAME'), 'jdes');
                $message->to($contact->email, 'Khách hàng')->subject('Phản hồi');
            });
            return response()->json([
                'status' => true,
            ]);
        }
    }
}
