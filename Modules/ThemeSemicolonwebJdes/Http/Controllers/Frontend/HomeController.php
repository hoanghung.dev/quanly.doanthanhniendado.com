<?php

namespace Modules\ThemeSemicolonwebJdes\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Modules\ThemeSemicolonwebJdes\Models\{Category, Banner, Company, Post, Product, Widget, Setting};
use Illuminate\Http\Request;


class HomeController extends Controller
{
    function GetHome()
    {

        return view('themesemicolonwebjdes::pages.home.index-magazine');
    }

    function GetFaqs()
    {
        return view('themesemicolonwebjdes::pages.faqs');
    }

    function GetLanding()
    {
        return view('themesemicolonwebjdes::pages.landing-4');
    }

    function GetMaintenance()
    {
        return view('themesemicolonwebjdes::pages.maintenance');
    }

    function GetAbout($comSlug)
    {
        $data['intro'] = Company::where('slug',$comSlug)->first();
        return view('themesemicolonwebjdes::pages.home.detail',$data);
    }
}
