<?php

namespace Modules\ThemeSemicolonwebJdes\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\ThemeSemicolonwebJdes\Models\Post;

class PostController extends Controller
{

    //  Trang chi tiết bài viết
    public function detail($slug) {

        $slug = str_replace('.html', '', $slug);

        $data['post'] = Post::where('slug', $slug)->first();
        if (!is_object($data['post'])) {
            abort(404);
        }

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['post']->meta_title != '' ? $data['post']->meta_title : $data['post']->name,
            'meta_description' => $data['post']->meta_description != '' ? $data['post']->meta_description : $data['post']->name,
            'meta_keywords' => $data['post']->meta_keywords != '' ? $data['post']->meta_keywords : $data['post']->name,
        ];
        view()->share('pageOption', $pageOption);

        return view('themesemicolonwebjdes::pages.home.detail',$data);
    }
    function SearchPost(request $r)
    {
        $data['posts']=\Modules\ThemeSemicolonwebJdes\Models\Post::where('name','like','%'.$r->search.'%')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->paginate(12);
        $data['search']=$r->search;
        $pageOption = [
            'meta_title' => 'Tìm kiếm từ khóa: ' . @$r->search,
            'meta_description' => 'Tìm kiếm từ khóa: ' . @$r->search,
            'meta_keywords' => 'Tìm kiếm từ khóa: ' . @$r->search,
        ];
        view()->share('pageOption', $pageOption);

        return view('themesemicolonwebjdes::pages.post.search_post',$data);
    }

}
