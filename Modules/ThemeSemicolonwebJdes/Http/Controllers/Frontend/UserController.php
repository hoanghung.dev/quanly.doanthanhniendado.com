<?php

namespace Modules\ThemeSemicolonwebJdes\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\ThemeSemicolonwebJdes\Models\{Company, Widget, Banner, User, Clientsay};

class UserController extends Controller
{
    function GetUser($comSlug)
    {

        $company = Company::where('slug',$comSlug)->first()->id;
        $data['users'] = User::where('company_id',$company)->where('status', 1)->orderBy('id', 'desc')->paginate(12);


        return view('themesemicolonwebjdes::pages.user.index-portfolio-4',$data);
    }
}
