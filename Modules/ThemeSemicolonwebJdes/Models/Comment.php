<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */

namespace Modules\ThemeSemicolonwebJdes\Models ;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comment_logs';
    protected $guarded = [];
    public $timestamps = false;
}
