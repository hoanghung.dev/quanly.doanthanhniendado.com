<?php

namespace Modules\ThemeSemicolonwebJdes\Models ;

use App\Http\Helpers\CommonHelper;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';

    public $timestamps = false;
    protected $fillable = [
        'name', 'link', 'image', 'intro', 'id', 'url', 'short_name', 'email', 'tel','admin_id', 'msdn', 'address', 'exp_date', 'account_max', 'service_id', 'company_id'
    ];

    public function invite_history()
    {
        return $this->hasMany(InviteHistory::class, 'company_id', 'id');
    }

    public function service_history()
    {
        return $this->hasMany(ServiceHistory::class, 'company_id', 'id');
    }
}
