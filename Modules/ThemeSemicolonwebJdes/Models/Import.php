<?php

namespace Modules\ThemeSemicolonwebJdes\Models ;

use Illuminate\Database\Eloquent\Model;

class Import extends Model
{
    protected $table = 'imports';

    protected $fillable = [
        'file', 'module', 'status', 'record_total', 'record_success', 'note', 'success_id'
    ];
}
