<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */

namespace Modules\ThemeSemicolonwebJdes\Models ;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model
{

    protected $table = 'jobs';

//    use SoftDeletes;

    protected $guarded = [];
    protected $fillable = [
        'name', 'end_date'
    ];

//    protected $dates = ['deleted_at'];
//
//    protected $softDelete = true;

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'employees_id');
    }

    public function subject()
    {
        return $this->hasMany(Subject::class, 'subject_id', 'id');
    }

    public function task()
    {
        return $this->hasMany(Task::class, 'task_id', 'id');
    }
//
//    public function deleteEverythingRelated()
//    {
//        $subject_query = Subject::where('job_id', $this->attributes['id']);
//        Task::where('job_id', $this->attributes['id'])->orWhere(function ($query) use ($subject_query) {
//            $query->whereIn('subject_id', $subject_query->pluck('id')->toArray());
//        })->delete();
//        $subject_query->delete();
//        return true;
//    }

//
    public function delete()
    {
        $this->task()->delete();
        $this->subject()->delete();
        parent::delete();
    }
//
//    public function type() {
//        return $this->belongsTo(ProjectType::class, 'project_type_id');
//    }
}
