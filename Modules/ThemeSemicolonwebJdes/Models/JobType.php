<?php

namespace Modules\ThemeSemicolonwebJdes\Models ;

use Illuminate\Database\Eloquent\Model;

class JobType extends Model
{

    protected $table = 'job_types';

    protected $fillable = [
        'admin_id' , 'company_id' , 'name', 'role_ids'
    ];

    public $timestamps = false;
}
