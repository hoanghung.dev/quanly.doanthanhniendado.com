<?php

/**
 * Page Model
 *
 * Page Model manages page operation. 
 *
 * @category   Page
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license    
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\ThemeSemicolonwebJdes\Models ;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menus';

    protected $fillable = [
        'name_vi','type','url','parent_id','item_id','created_at','updated_at','order_no','location','category_post_id','post_id','category_product_id','product_id','name_en'
    ];

    public function post() {
        return $this->hasMany(Post::class, 'item_id', 'id');
    }

    public function category() {
        return $this->hasMany(Category::class, 'item_id', 'id');
    }

    public function page() {
        return $this->hasMany(Category::class, 'item_id', 'id');
    }

    public function product() {
        return $this->hasMany(Product::class, 'item_id', 'id');
    }

    public function childs()
    {
        return $this->hasMany($this, 'parent_id', 'id')->orderBy('order_no', 'asc');
    }

    public function viewChilds()
    {
        return $this->hasMany($this, 'parent_id', 'id')->select(['id', 'name', 'url'])->orderBy('order_no', 'asc');
    }
}
