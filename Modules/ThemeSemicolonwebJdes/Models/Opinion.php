<?php

namespace Modules\ThemeSemicolonwebJdes\Models ;

use Illuminate\Database\Eloquent\Model;

class Opinion extends Model
{

    protected $table = 'opinion';

    protected $fillable = ['id',
        'status','slug','position_en','position_vi','img','content_en','content_vi','name_en','name_vi'
    ];

}