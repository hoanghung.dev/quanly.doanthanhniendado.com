<?php

namespace Modules\ThemeSemicolonwebJdes\Models ;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'orders';

    protected $fillable = [
        'weight','user_id','bill_id', 'price', 'product_id', 'quantity', 'product_name', 'product_price', 'product_image', 'product_type', 'product_link', 'color', 'size', 'customer_note', 'type_item', 'type_transport', 'status',
        'price_vi', 'price_cn', 'product_code', 'product_category_id', 'warehouse_id', 'employees_note', 'code_transport', 'bill_of_lading', 'base_price_cn', 'base_price_vi','supplier_name','supplier_address','company_id',
        'key_editor_log', 'editor_id'
    ];

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function bill()
    {
        return $this->belongsTo(Bill::class, 'id', 'bill_id');
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

}
