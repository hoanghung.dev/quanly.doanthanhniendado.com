<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */
namespace Modules\ThemeSemicolonwebJdes\Models ;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model {

    protected $table = 'posts';

    use SoftDeletes;

    protected $fillable = [
        'id','bill_id'
    ];


}