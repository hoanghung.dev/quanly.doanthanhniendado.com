<?php

namespace Modules\ThemeSemicolonwebJdes\Models ;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{

    protected $table = 'partner';

    protected $fillable = ['id',
        'slug','intro_en','intro_vi','img','img_logo','name_en','name_vi',
    ];

}