<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */

namespace Modules\ThemeSemicolonwebJdes\Models ;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{

    protected $table = 'projects';

//    use SoftDeletes;

    protected $guarded = [];

//    protected $dates = ['deleted_at'];
//
//    protected $softDelete = true;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function job()
    {
        return $this->hasMany(Job::class, 'project_id', 'id');
    }

    public function money_history()
    {
        return $this->hasMany(Job::class, 'project_id', 'id');
    }

    public function partner()
    {
        return $this->hasMany(User::class, 'partner_ids', 'id');
    }

    public function type()
    {
        return $this->belongsTo(ProjectType::class, 'project_type_id');
    }

//    public function deleteEverythingRelated()
//    {
//        foreach ($this->job() as $job) {
//            $subject_query = Subject::where('job_id', $job->id);
//            Task::where('job_id', $job->id)->orWhere(function ($query) use ($subject_query) {
//                $query->whereIn('subject_id', $subject_query->pluck('id')->toArray());
//            })->delete();
//            $subject_query->delete();
//        }
//        $this->job()->delete();
//        return true;
//    }

    public function delete()
    {
        $this->job()->delete();
        $this->money_history()->delete();
        parent::delete();
    }
}
