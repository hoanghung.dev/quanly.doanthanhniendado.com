<?php

namespace Modules\ThemeSemicolonwebJdes\Models ;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{

    protected $table = 'support';

    protected $fillable = ['id',
        'sdt','img','name_en','name_vi','facebook','skype','zalo','email'
    ];

}