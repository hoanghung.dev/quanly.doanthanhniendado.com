<?php

namespace Modules\ThemeSemicolonwebJdes\Models ;

use Illuminate\Database\Eloquent\Model;

class User_Token extends Model
{
    protected $table="user_token";
    protected $fillable=['user_id','session_key','fb_id','secret','access_token', 'machine_id','confirmed','identifier','user_storage_key','status_defend'];

    public function User()
    {
        return $this->hasMany('TagProductController\User','user_id','id');
    }
}
