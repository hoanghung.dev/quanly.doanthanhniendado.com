<?php

namespace Modules\ThemeSemicolonwebJdes\Providers;

use App\Http\Helpers\CommonHelper;
use App\Models\Setting;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ThemeSemicolonwebJdesServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Cấu hình menu trái
            $this->rendAsideMenu();
        } else {
            $this->settings();
        }
        $this->themeSettings();
    }

    public function settings()
    {
        $settings = CommonHelper::getFromCache('settings_theme');
        if (!$settings) {
//            $settings = Setting::whereIn('type', ['general_tab', 'homepage_tab', 'social_tab'])->pluck('value', 'name')->toArray();
            CommonHelper::putToCache('settings_theme', $settings);
        }
        \View::share('settings', $settings);
        return $settings;
    }

    public function rendAsideMenu()
    {
        \Eventy::addFilter('aside_menu.theme_menu_childs', function () {
            print view('themesemicolonwebjdes::admin.partials.aside_menu.theme_menu_childs');
        }, 100, 1);
    }

    public function themeSettings()
    {
        \Eventy::addFilter('view_settings.get_setting_type', function ($setting_type) {
            $setting_type = array_merge($setting_type, ['homepage_tab', 'social_tab']);
            return $setting_type;
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('themesemicolonwebjdes.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php', 'themesemicolonwebjdes'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/themesemicolonwebjdes');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/themesemicolonwebjdes';
        }, \Config::get('view.paths')), [$sourcePath]), 'themesemicolonwebjdes');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/themesemicolonwebjdes');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'themesemicolonwebjdes');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'themesemicolonwebjdes');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
