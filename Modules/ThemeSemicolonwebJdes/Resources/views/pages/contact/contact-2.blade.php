@extends('themesemicolonwebjdes::layouts.default')
@section('lienhe')
    class="color-menu"
@endsection
@section('main_content')
    <div id="wrapper" class="clearfix">
        @if(isset($company->slug))
            @include('themesemicolonwebjdes::template.menu_company')
        @else
            @include('themesemicolonwebjdes::template.menu_other')
        @endif
        <?php
        $widget = \Modules\ThemeSemicolonwebJdes\Models\Widget::select(['name', 'content'])->where('location', 'title5')->where('status', 1)->first();
//        if (isset($company->slug)) {
//            $widget = \Modules\ThemeSemicolonwebJdes\Models\Company::select('short_name')->where('slug', $company->slug)->first();
//        }
        ?>
        <section id="page-title">
            <div class="container clearfix">
                @if(isset($company->slug))
                    <h1>Liên hệ {{@$company->short_name}}</h1>
                @else
                    <h1>{{@$widget->name}}</h1>
                @endif

                @include('themesemicolonwebjdes::partials.breadcrumb')
            </div>
        </section>
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="col_half">
                        {{--<div class="fancy-title title-dotted-border" style="display: flex">--}}
                        <div class="fancy-title" style="display: flex">
                            <h3>Gửi liên hệ</h3>
                        </div>
                        <div class="form-widget">
                            <div class="form-result"></div>
                            <form class="nobottommargin" id="template-contactform" name="template-contactform"
                                  action="{{route('contact.post2',@$comSlug)}}" method="post">
                                <div class="form-process"></div>
                                <div class="col_full">
                                    <label for="template-contactform-name">Họ tên
                                        <small>*</small>
                                    </label>
                                    <input type="text" id="template-contactform-name" name="name" value=""
                                           class="sm-form-control required"/>
                                </div>
                                <div class="col_full">
                                    <label for="template-contactform-email">Địa chỉ email
                                        <small>*</small>
                                    </label>
                                    <input type="email" id="template-contactform-email" name="email" value=""
                                           class="required email sm-form-control"/>
                                </div>
                                <div class="col_full">
                                    <label for="template-contactform-address">Địa chỉ
                                        <small>*</small>
                                    </label>
                                    <input type="text" id="template-contactform-address" name="address" value=""
                                           class="sm-form-control required"/>
                                </div>
                                <div class="col_full">
                                    <label for="template-contactform-phone">Điện thoại</label>
                                    <input type="text" id="template-contactform-phone" name="phone" value=""
                                           class="sm-form-control"/>
                                </div>
                                <div class="clear"></div>
                                <div class="col_full">
                                    <label for="template-contactform-message">Lời nhắn
                                        <small>*</small>
                                    </label>
                                    <textarea class="required sm-form-control" id="template-contactform-message"
                                              name="message" rows="6" cols="30"></textarea>
                                </div>
                                <div class="col_full hidden">
                                    <input type="text" id="template-contactform-botcheck"
                                           name="template-contactform-botcheck" value="" class="sm-form-control"/>
                                </div>
                                <div class="col_full">
                                    <button name="submit" type="submit" id="submit-button" tabindex="5" value="Submit"
                                            class="button button-3d nomargin">Gửi
                                    </button>
                                </div>
                                <input type="hidden" name="prefix" value="template-contactform-">
                            </form>
                        </div>
                    </div>
                    <div class="col_half col_last">
                        {{--<section id="google-map" class="gmap" style="height: 410px;"></section>--}}
                        @if(isset($company->slug))
                            {!! @$company->google_map !!}
                        @else
                            {!! @$settings['google_map'] !!}
                        @endif

                    </div>
                    <div class="clear"></div>

                    <div class="row clear-bottommargin">
                        <div class="col-lg-3 col-md-6 bottommargin clearfix">
                            <div class="feature-box fbox-center fbox-bg fbox-plain">
                                <div class="fbox-icon">
                                    <a>
                                        <i class="icon-map-marker2"></i></a>
                                </div>
                                @if(isset($company->slug))
                                    <h3>Địa chỉ<span class="subtitle">{!! @$company->address !!}</span></h3>
                                @else
                                <h3>Địa chỉ<span class="subtitle">{!! @$settings['address'] !!}</span></h3>
                                    @endif
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 bottommargin clearfix">
                            <div class="feature-box fbox-center fbox-bg fbox-plain">
                                <div class="fbox-icon">
                                    <a target="_blank"><i class="icon-phone3"></i></a>
                                </div>
                                @if(isset($company->slug))
                                    <h3>Gọi cho chúng tôi<span class="subtitle">{!! @$company->tel !!}</span></h3>
                                @else
                                <h3>Gọi cho chúng tôi<span class="subtitle">{!!  @$settings['hotline'] !!}</span></h3>
                                    @endif
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 bottommargin clearfix">
                            <div class="feature-box fbox-center fbox-bg fbox-plain">
                                <div class="fbox-icon">
                                    <a target="_blank"><i class="icon-skype2"></i></a>
                                </div>
                                @if(isset($company->slug))
                                <h3>Video Call<span class="subtitle">{!! @$company->skype !!}</span></h3>
                                    @else
                                    <h3>Video Call<span class="subtitle">{!! @$settings['skype'] !!}</span></h3>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 bottommargin clearfix">
                            <div class="feature-box fbox-center fbox-bg fbox-plain">
                                <div class="fbox-icon">
                                    <a target="_blank"><i class="icon-email3"></i></a>
                                </div>
                                @if(isset($company->slug))
                                    <h3>Giải đáp thắc mắc<span class="subtitle">{!! @$company->email !!}</span></h3>
                                @else
                                    <h3>Giải đáp thắc mắc<span class="subtitle">{!! @$settings['email'] !!}</span></h3>
                                    @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

