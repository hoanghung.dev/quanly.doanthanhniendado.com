<!DOCTYPE html>
<html lang="vi">
<head>
    <title>Jdes</title>
</head>
<body>
<div style="width: 800px;">
    <div style="height: 80px; background-color: #FFFF00; text-align: center;"><h1>Phản hồi khách hàng</h1></div>
    <div style="text-align: center; margin: 20px 0;"><span
                style="font-weight: bold; font-size: 20px; text-transform:uppercase; color: #FF9600; ">{{ $contact->email }}</span>
    </div>
    <div style="border: 2px dotted #FFFF00;padding: 10px;  "><span
                style="font-size: 20px; font-weight: bold; background-color: #0066CC; color: white;padding:0 10px;">THÔNG TIN KHÁCH HÀNG</span>
        <p><b>Họ Tên</b>:{{  $contact->name }}</p>
        <p><b>Email</b>:{{ $contact->email }}</p>
        <p><b>Điện thoại</b>:{{  $contact->tel }}</p>
        <p><b>Địa chỉ</b>:{{  $contact->address }}</p>
        <p><b>Lời nhắn</b>:{!! $contact->content !!}</p>
    </div>

    <hr>
    <div style="text-align: center;">COPYRIGHT&copy; JDES</div>
    <div style="text-align: center;">Jdes.vn</div>
</div>
</body>
</html>