@extends('themesemicolonwebjdes::layouts.default')
@section('trogiup')
class="color-menu"
@endsection
@section('main_content')
    <div id="wrapper" class="clearfix">
        @include('themesemicolonwebjdes::template.menu_other')
        <?php
        $widget = \Modules\ThemeSemicolonwebJdes\Models\Widget::select(['name', 'content'])->where('location', 'title4')->where('status', 1)->first();
        ?>
        <section id="page-title">
            <div class="container clearfix">
                <h1>{{@$widget->name}}</h1>
                @include('themesemicolonwebjdes::partials.breadcrumb')
            </div>
        </section>
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="postcontent nobottommargin clearfix">
                        <?php
                        $categorys = \Modules\ThemeSemicolonwebJdes\Models\Category::where('parent_id', 172)->get();
                        ?>
                        <ul class="portfolio-filter customjs clearfix">
                            @foreach($categorys as $k => $cate)
                                <li @if(@$_GET['cate_id'] == $cate->id || (!isset($_GET['cate_id']) && $k == 0)) class="activeFilter" @endif><a href="/tro-giup?cate_id={{$cate->id}}">{{$cate->name}}</a></li>
                            @endforeach
                        </ul>
                        <div class="clear"></div>
                        <div id="faqs" class="faqs">
                            @if(isset($_GET['cate_id']))
                                <?php
                                $posts = \Modules\ThemeSemicolonwebJdes\Models\Post::where('multi_cat', 'LIKE', '%|' . $_GET['cate_id'] . '|%')->get();
                                ?>
                                @foreach($posts as $post)
                                    <div class="toggle faq faq-marketplace faq-authors">
                                        <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i
                                                    class="toggle-open icon-question-sign"></i>{{$post->name}}</div>
                                        <div class="togglec">{!! $post->content !!}.</div>
                                    </div>
                                @endforeach
                            @else
                                <?php
                                $posts = \Modules\ThemeSemicolonwebJdes\Models\Post::where('multi_cat', 'LIKE', '%|173|%')->get();
                                ?>
                                @foreach($posts as $post)
                                    <div class="toggle faq faq-marketplace faq-authors">
                                        <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i
                                                    class="toggle-open icon-question-sign"></i>{{$post->name}}</div>
                                        <div class="togglec">{!! $post->content !!}.</div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="sidebar nobottommargin col_last clearfix">
                        <div class="sidebar-widgets-wrap">
                            @include('themesemicolonwebjdes::pages.faqs_page_sidebar')
                            @include('themesemicolonwebjdes::pages.home.partials.popular_post')
                            <div class="widget clearfix">
                                <h4>Kết nối với chúng tôi</h4>
                                <a href="{!! @$settings['fanpage'] !!}" target="_blank"
                                   class="social-icon si-colored si-small si-facebook" data-toggle="tooltip"
                                   data-placement="top" title="Facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                {{--<a href="{!! @$settings['google_plus'] !!}  }}"--}}
                                   {{--class="social-icon si-colored si-small si-gplus" data-toggle="tooltip"--}}
                                   {{--data-placement="top" title="Google Plus">--}}
                                    {{--<i class="icon-gplus"></i>--}}
                                    {{--<i class="icon-gplus"></i>--}}
                                {{--</a>--}}
                                <a href="{!! @$settings['skype'] !!}" class="social-icon si-colored si-small si-skype"
                                   data-toggle="tooltip"
                                   data-placement="top" title="Skype">
                                    <i class="icon-skype"></i>
                                    <i class="icon-skype"></i>
                                </a>
                                {{--<a href="{!! @$settings['youtube'] !!}" class="social-icon si-colored si-small si-youtube"--}}
                                   {{--data-toggle="tooltip"--}}
                                   {{--data-placement="top" title="Youtube">--}}
                                    {{--<i class="icon-youtube"></i>--}}
                                    {{--<i class="icon-youtube"></i>--}}
                                {{--</a>--}}
                                <a href="{!! @$settings['instagram'] !!}"
                                   class="social-icon si-colored si-small si-instagram" data-toggle="tooltip"
                                   data-placement="top" title="Instagram">
                                    <i class="icon-instagram"></i>
                                    <i class="icon-instagram"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        jQuery(document).ready(function ($) {
            var $faqItems = $('#faqs .faq');
            if (window.location.hash != '') {
                var getFaqFilterHash = window.location.hash;
                var hashFaqFilter = getFaqFilterHash.split('#');
                if ($faqItems.hasClass(hashFaqFilter[1])) {
                    $('#portfolio-filter li').removeClass('activeFilter');
                    $('[data-filter=".' + hashFaqFilter[1] + '"]').parent('li').addClass('activeFilter');
                    var hashFaqSelector = '.' + hashFaqFilter[1];
                    $faqItems.css('display', 'none');
                    if (hashFaqSelector != 'all') {
                        $(hashFaqSelector).fadeIn(500);
                    } else {
                        $faqItems.fadeIn(500);
                    }
                }
            }
            $('#portfolio-filter a').on('click', function () {
                $('#portfolio-filter li').removeClass('activeFilter');
                $(this).parent('li').addClass('activeFilter');
                var faqSelector = $(this).attr('data-filter');
                $faqItems.css('display', 'none');
                if (faqSelector != 'all') {
                    $(faqSelector).fadeIn(500);
                } else {
                    $faqItems.fadeIn(500);
                }
                return false;
            });
        });
    </script>
@endsection
@section('custom_head')
    <style>
        .postcontent.nobottommargin.clearfix {
            overflow: hidden;
        }
    </style>
@endsection