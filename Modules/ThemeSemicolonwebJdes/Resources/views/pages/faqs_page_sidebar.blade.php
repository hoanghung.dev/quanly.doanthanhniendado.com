<div class="widget widget_links clearfix">
    <?php
    $widgets = \Modules\ThemeSemicolonwebJdes\Models\Widget::select(['name', 'content'])->whereIn('location', ['faq0', 'faq1', 'faq2'])->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->get();
    ?>
    @foreach($widgets as $widget)
        <h3>{{$widget->name}}</h3>
        <p class="notopmargin">{!! $widget->content !!}</p>
    @endforeach
</div>