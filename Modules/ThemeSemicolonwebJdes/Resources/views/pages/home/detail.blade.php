@extends('themesemicolonwebjdes::layouts.default')
@section('gioithieu')
    class="color-menu"
@endsection
@section('main_content')
    <div id="wrapper" class="clearfix">
        @if(isset($company->slug))
            @include('themesemicolonwebjdes::template.menu_company')
        @else
            @include('themesemicolonwebjdes::template.menu_other')
        @endif

        <section id="page-title">
            <div class="container clearfix">

                @if (isset($company->slug))
                    <h1>Giới thiệu {{@$company->short_name}}</h1>
                @else
                    <h1>{{@$post->name}}</h1><a href="/admin/post/edit/{{ $post->id }}" title="Sửa bài viết"></a>
                @endif
                    @if(\Auth::check() && CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'post_edit'))
                    <a href="/admin/post/edit/{{ $post->id }}" title="Sửa bài viết"><i class="icon-edit2"></i></a>
                    @endif
                @include('themesemicolonwebjdes::partials.breadcrumb')
            </div>
        </section>
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="postcontent nobottommargin clearfix">
                        <div class="single-post nobottommargin">
                            <div class="entry clearfix">
                                <ul class="entry-meta clearfix">
                                    <li><i class="icon-calendar3"></i> {{date('d-m-Y',strtotime(@$post->created_at))}}
                                    </li>
                                </ul>
                                <div class="entry-content notopmargin">
                                    {{--<div class="entry-image alignleft">
                                        @if (isset($company->slug))
                                            <a href="#"><img
                                                        src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$intro->image,350,162)}}"
                                                        alt="{{@$intro->name}}"></a>
                                        @else
                                            <a href="#"><img
                                                        src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$post->image,350,162)}}"
                                                        alt="{{@$post->name}}"></a>
                                        @endif
                                    </div>--}}
                                    <p>{!! @$post->content !!}</p>
                                    <p>{!! @$intro->intro !!}</p>
                                    @if(!isset($company->slug))
                                        <div class="tagcloud clearfix bottommargin">
                                            <?php
                                                $tags=explode('|',trim(@$post->tags,'|'));
                                            ?>
                                            @foreach($tags as $tag)
                                                <a href="/tag/{{@\Modules\ThemeSemicolonwebJdes\Models\Category::find($tag)->slug}}">{{ @\Modules\ThemeSemicolonwebJdes\Models\Category::find($tag)->name }}</a>
                                            @endforeach
                                        </div>
                                    @endif
                                    <div class="clear"></div>
                                    <div class="si-share noborder clearfix">
                                        <span>Chia sẻ tin tức:</span>
                                            <div id="fb-root"></div>
                                            <script async defer crossorigin="anonymous"
                                                    src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v5.0"></script>
                                            <div class="fb-share-button"
                                                 data-href="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>" data-layout="button"
                                                 data-size="large"><a target="_blank"
                                                                      href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"
                                                                      class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
                                        {{--<div>--}}
                                            {{--<a href="{!! @$settings['fanpage'] !!}" target="_blank"--}}
                                               {{--class="social-icon si-borderless si-facebook">--}}
                                                {{--<i class="icon-facebook"></i>--}}
                                                {{--<i class="icon-facebook"></i>--}}
                                            {{--</a>--}}
                                            {{--<a href="{!! @$settings['instagram'] !!}" target="_blank"--}}
                                               {{--class="social-icon si-borderless si-gplus">--}}
                                                {{--<i class="icon-instagram2"></i>--}}
                                                {{--<i class="icon-instagram2"></i>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="line"></div>
                            <?php
                            $categoryid = @\Modules\ThemeSemicolonwebJdes\Models\Post::find($post->id);
                            $relates = @\Modules\ThemeSemicolonwebJdes\Models\Post::where('multi_cat','like', '%|'.$categoryid->category_id.'|%')->where('id', '<>', $categoryid->id)->where('status',1)->orderBy('order_no', 'DESC')->orderBy('id','DESC')->take(4)->get();
//                            $relates = CommonHelper::getFromCache('post_relates_' . $post->id);
//                            if (!$relates) {
//                                $cat_ids = explode('|', $post->multi_cat);
//                                $relates = @\Modules\ThemeSemicolonwebJdes\Models\Post::where(function ($query) use ($cat_ids) {
//                                    foreach ($cat_ids as $v) {
//                                        if ($v != '') {
//                                            $query->orWhere('multi_cat', 'LIKE', '%|' . $v . '|%');
//                                        }
//                                    }
//                                })->where('id', '!=', $categoryid->id)->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->take(4)->get();
//
//                                CommonHelper::putToCache('post_relates_' . $post->id, $relates);
//                            }
                            ?>
                            <h4>Tin liên quan:</h4>
                            <div class="related-posts clearfix">
                                <div class="col_half nobottommargin" style="width: 100%;">
                                    @foreach($relates as $key=>$relate)
                                        <div class="mpost clearfix"
                                             style="width:46%;float:left;padding-top:20px;margin:20px 30px 30px 0!important;@if($key==2) clear:both; @endif">
                                            <div class="entry-image">
                                                <a href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct(@$relate) }}"><img
                                                            src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$relate->image,170,128)}}"
                                                            alt="{{@$relate->name}}"></a>
                                            </div>
                                            <div class="entry-c">
                                                <div class="entry-title">
                                                    <h4>
                                                        <a href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct(@$relate) }}">{{@$relate->name}}</a>
                                                    </h4>
                                                </div>
                                                <ul class="entry-meta clearfix">
                                                    <li>
                                                        <i class="icon-calendar3"></i> {{date('d-m-Y',strtotime(@$relate->created_at))}}
                                                    </li>
                                                </ul>
                                                <div class="entry-content">{{@$relate->intro}}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div id="comments" class="clearfix">
                                <h3 id="comments-title">Bình luận</h3>
                                <div id="fb-root"></div>
                                <script>(function (d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) return;
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=1618627438366755&autoLogAppEvents=1';
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }(document, 'script', 'facebook-jssdk'));</script>
                                <div class="fb-comments"
                                     data-href="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"
                                     data-numposts="5"></div>
                            </div>
                        </div>
                    </div>

                    <div class="sidebar nobottommargin col_last clearfix">
                        <div class="sidebar-widgets-wrap">
                            {{--<div class="widget clearfix">--}}
                                {{--<h4>Flickr Photostream</h4>--}}
                                {{--<div id="flickr-widget" class="flickr-feed masonry-thumbs" data-id="613394@N22"--}}
                                     {{--data-count="16" data-type="group" data-lightbox="gallery"></div>--}}
                            {{--</div>--}}
                            @include('themesemicolonwebjdes::pages.home.partials.popular_post')
                            @include('themesemicolonwebjdes::pages.home.partials.banner')
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
