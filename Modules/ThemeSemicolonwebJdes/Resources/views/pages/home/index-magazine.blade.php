@extends('themesemicolonwebjdes::layouts.default')
{{--@section('trangchu')--}}
    {{--class="color-menu"--}}
{{--@endsection--}}
@section('main_content')

    <div id="wrapper" class="clearfix">
        @include('themesemicolonwebjdes::template.menu_other')

        <section id="content">
            <div class="content-wrap">
                <div class="section header-stick bottommargin-lg clearfix" style="padding: 20px 0;">
                    <div>
                        <div class="container clearfix">
                            <?php
                            $hots = \Modules\ThemeSemicolonwebJdes\Models\Post::where('tin_hot', 1)->where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->take(3)->get();
                            ?>
                            <span class="badge badge-danger bnews-title">Tin hot:</span>
                            <div class="fslider bnews-slider nobottommargin" data-speed="800" data-pause="6000"
                                 data-arrows="false" data-pagi="false">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        @foreach($hots as $hot)
                                            <div class="slide"><a
                                                        href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($hot) }}"><strong>{{$hot->name}}</strong></a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container clearfix">
                    <div class="row">
                        <div class="col-lg-8 bottommargin">
                            @include('themesemicolonwebjdes::pages.home.partials.slides')
                            <div class="clear"></div>
                            @include('themesemicolonwebjdes::pages.home.partials.post1')
                            <?php
                            $banner = \Modules\ThemeSemicolonwebJdes\Models\Banner::where('location', 'banners_home')->where("status", 1)->first();
                            ?>
                            <div class="bottommargin-lg">
                                <a href="{{$banner->link}}"><img
                                            src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($banner->image,720,90) }}"
                                            alt="{{$banner->name}}"
                                            class="aligncenter notopmargin nobottommargin"></a>
                            </div>
                            @include('themesemicolonwebjdes::pages.home.partials.post2')
                            @include('themesemicolonwebjdes::pages.home.partials.new_picture')
                            @include('themesemicolonwebjdes::pages.home.partials.other_new')

                        </div>
                        <div class="col-lg-4">
                            <div class="line d-block d-lg-none d-xl-block"></div>
                            <div class="sidebar-widgets-wrap clearfix">
                                @include('themesemicolonwebjdes::pages.home.partials.connect')
                                <?php
                                $banner = \Modules\ThemeSemicolonwebJdes\Models\Banner::where('location', 'banner_right')->where("status", 1)->first();
                                ?>
                                <div class="widget clearfix">
                                    <a href="{{$banner->link}}"><img class="aligncenter"
                                                                     src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($banner->image, 300, 250) }}"
                                                                     alt="{{$banner->name}}"></a>
                                </div>
                                {{--@include('themesemicolonwebjdes::pages.home.partials.danhmuc')--}}
                                {{--<div class="widget clearfix">--}}
                                {{--<h4>Flickr Photostream</h4>--}}
                                {{--<div id="flickr-widget" class="flickr-feed masonry-thumbs grid-5"--}}
                                {{--data-id="613394@N22" data-count="15" data-type="group"--}}
                                {{--data-lightbox="gallery"></div>--}}
                                {{--</div>--}}
                                @include('themesemicolonwebjdes::pages.home.partials.popular_post')
                                <?php
                                $widgets = \Modules\ThemeSemicolonwebJdes\Models\Widget::select(['name', 'content'])->whereIn('location', ['home0', 'home1'])->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->get();
                                ?>
                                @foreach($widgets as $widget)
                                    <div class="widget clearfix">
                                        <h3>{{$widget->name}}</h3>
                                        <p class="notopmargin">{!! $widget->content !!}</p>
                                        <br>
                                    </div>
                                @endforeach
                                <div class="widget clearfix">
                                    <?php
                                    $widgets = \Modules\ThemeSemicolonwebJdes\Models\Widget::select(['name', 'content', 'location'])->where('location', 'facebook0')->where('status', 1)->first();
                                    ?>
                                    {!! @$widgets->content !!}
                                    {{--<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fjdesvietnam%2F&tabs=272&width=270&height=196&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId"--}}
                                            {{--width="270" height="196" style="border:none;overflow:hidden" scrolling="no"--}}
                                            {{--frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

