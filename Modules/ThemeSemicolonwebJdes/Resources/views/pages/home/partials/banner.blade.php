<div class="widget clearfix">
    <?php
    $banners = \Modules\ThemeSemicolonwebJdes\Models\Banner::where('location', 'banner_news')->where("status", 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->get();
    ?>
    <h4>Tin tức</h4>
    <div id="oc-portfolio-sidebar" class="owl-carousel carousel-widget" data-items="1"
         data-margin="10" data-loop="true" data-nav="false" data-autoplay="5000">
        @foreach($banners as $banner)
            <div class="oc-item">
                <div class="iportfolio">
                    <div class="portfolio-image">
                        <a href="{{$banner->link}}">
                            <img src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($banner->image,251,188)}}"
                                 alt="{{$banner->name}}">
                        </a>
                    </div>
                    <div class="portfolio-desc center nobottompadding">
                        <h3><a href="#">{{$banner->name}}</a></h3>
                        <span><a href="#">{{$banner->intro}}</a></span>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>