<?php
$menu_sidebar = Menu::getByName('menu sidebar');
?>
<div class="widget widget_links clearfix">
    <h4>Danh Mục</h4>
    <div class="col_half nobottommargin">
        @if($menu_sidebar)
            <ul class="menu">
                @foreach($menu_sidebar as $menu)
                    <li class="">
                        <a href="{{ $menu['link'] }}" title="">{{ $menu['label'] }}</a>
                        @if( $menu['child'] )
                            <ul class="sub-menu">
                                @foreach( $menu['child'] as $child )
                                    <li class="">
                                        <a href="{{ $child['link'] }}" title="">{{ $child['label'] }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
</div>
