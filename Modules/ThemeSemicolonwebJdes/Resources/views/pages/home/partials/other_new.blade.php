<?php
$category_post_ids = explode('|', trim(@$settings['category_post_ids'], '|'));
$cat = \Modules\ThemeSemicolonwebJdes\Models\Category::where('type', 1)->where('id', @$category_post_ids[0])
    ->orderBy('order_no', 'desc')->orderBy('id', 'desc')->first();
$cat2 = \Modules\ThemeSemicolonwebJdes\Models\Category::where('type', 1)->where('id', @$category_post_ids[1])
    ->orderBy('order_no', 'ASC')->orderBy('id', 'asc')->first();
$other = \Modules\ThemeSemicolonwebJdes\Models\Post::where('category_id', '<>', @$cat2->id)
    ->where('category_id', '<>', @$cat->id)->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->where('status', 1)->take(6)->get();

//$other = CommonHelper::getFromCache('other_' . @$cat2->id );
//if (!$other) {
//    $other = \Modules\ThemeSemicolonwebJdes\Models\Post::where('category_id', '<>', @$cat2->id)
//        ->where('category_id', '<>', @$cat->id)->orderBy('order_no', 'DESC')->orderBy('id','DESC')->take(6)->get();
//    CommonHelper::putToCache('other_' . @$cat2->id );
//}
?>
<div class="col_full nobottommargin clearfix">
    <div class="fancy-title title-border">
        <a href="tin-tuc"><h3>Tin tức khác</h3></a>
    </div>
    <div class="col_one_third" style="width: 100%;">
        @foreach($other as $key => $new)
            <div class="ipost clearfix"
                 style="width: 33%;float: left; padding: 20px; @if($key%3==0) clear:both; @endif ">
                <div class="entry-image">
                    <a href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($new) }}"><img class="image_fade"
                                                                                             src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($new->image, 230, 172) }}"
                                                                                             alt="{{$new->name}}"></a>
                </div>
                <div class="entry-title">
                    <h3><a href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($new) }}">{{$new->name}}</a></h3>
                </div>
                <ul class="entry-meta clearfix">
                    <li>
                        <i class="icon-calendar3"></i> {{date('d-m-Y',strtotime($new->created_at))}}
                    </li>
                </ul>
                <div class="entry-content">
                    <p>{{$new->intro}}</p>
                </div>
            </div>
        @endforeach
    </div>

</div>