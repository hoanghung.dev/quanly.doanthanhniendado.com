<?php
$populars = \Modules\ThemeSemicolonwebJdes\Models\Post::where('popular', 1)->where('status',1)->orderBy('order_no', 'DESC')->orderBy('id','DESC')->take(3)->get();
$news = \Modules\ThemeSemicolonwebJdes\Models\Post::orderby('created_at', 'DESC')->where('status',1)->orderBy('order_no', 'DESC')->take(3)->get();
?>
<div class="widget clearfix">
    <div class="tabs nobottommargin clearfix" id="sidebar-tabs">
        <ul class="tab-nav clearfix">
            <li><a href="#tabs-1" style="background: #ffffff">Nổi bật</a></li>
            <li><a href="#tabs-2">Tin mới</a></li>
        </ul>
        <div class="tab-container">
            <div class="tab-content clearfix" id="tabs-1">

                <div id="popular-post-list-sidebar">
                    @foreach($populars as $popular)
                        <div class="spost clearfix">
                            <div class="entry-image">
                                <a href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($popular) }}"
                                   class="nobg"><img class="rounded-circle"
                                                     src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($popular->image,48,48) }}"
                                                     alt="{{$popular->name}}"></a>
                            </div>
                            <div class="entry-c">
                                <div class="entry-title">
                                    <h4>
                                        <a href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($popular) }}">{{ $popular->name }} </a>
                                    </h4>
                                </div>
                                <ul class="entry-meta">
                                    <li>{{date('d-m-Y',strtotime($popular->created_at))}}</li>
                                </ul>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="tab-content clearfix" id="tabs-2" style="display: none">
                <div id="recent-post-list-sidebar">
                    @foreach($news as $new)
                        <div class="spost clearfix">
                            <div class="entry-image">
                                <a href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($new) }}"><img
                                            class="rounded-circle"
                                            src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($new->image,48,48) }}"
                                            alt="{{$new->name}}"></a>
                            </div>
                            <div class="entry-c">
                                <div class="entry-title">
                                    <h4>
                                        <a href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($new) }}">{{$new->name}} </a>
                                    </h4>
                                </div>
                                <ul class="entry-meta">
                                    <li>{{date('d-m-Y',strtotime($new->created_at))}}</li>
                                </ul>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
