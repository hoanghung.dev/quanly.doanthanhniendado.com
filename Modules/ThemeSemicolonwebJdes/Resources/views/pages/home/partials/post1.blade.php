<?php
$category_post_ids = explode('|', trim(@$settings['category_post_ids'], '|'));
$cat = \Modules\ThemeSemicolonwebJdes\Models\Category::where('type', 1)->where('id', $category_post_ids[0])
    ->orderBy('order_no', 'desc')->orderBy('id', 'desc')->first();
?>
@if(is_object($cat))
    <?php
    $p = \Modules\ThemeSemicolonwebJdes\Models\Post::where('multi_cat', 'like', '%|' . @$category_post_ids[0] . '|%')
        ->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->first();
    $posts = CommonHelper::getFromCache('post1_' . $cat->id);
    if (!$posts) {
        $posts = \Modules\ThemeSemicolonwebJdes\Models\Post::where('multi_cat', 'like', '%|' . @$cat->id . '|%')
            ->where('id', '<>', $p->id)->where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id', 'desc')->take(4)->get();
        CommonHelper::putToCache('post1_' . $cat->id, $posts);
    }
    ?>
    <div class="col_full bottommargin-lg clearfix">
        <div class="fancy-title title-border">
            <a href="/{{$cat->slug}}"><h3>{{@$cat->name}}</h3></a>
        </div>
        <div class="ipost clearfix">
            <div class="col_half bottommargin-sm">
                <div class="entry-image">
                    <a href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($p) }}"><img class="image_fade"
                                                                                           src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($p->image, 360, 202) }}"
                                                                                           alt="{{$p->name}}"></a>
                </div>
            </div>
            <div class="col_half bottommargin-sm col_last">
                <div class="entry-title">
                    <h3><a href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($p) }}">{{$p->name}}</a>
                    </h3>
                </div>
                <ul class="entry-meta clearfix">
                    <li>
                        <i class="icon-calendar3"></i> {{date('d-m-Y',strtotime($p->created_at))}}
                    </li>
                </ul>
                <div class="entry-content">
                    <p>{{$p->intro}}</p>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="col_half nobottommargin" style="width: 100%;">
            @foreach($posts as $key => $bep)
                <div class="spost clearfix"
                     style="width:46%;float:left;padding-top:20px;margin:20px 30px 30px 0!important; @if($key==2) clear:both; @endif">
                    <div class="entry-image">
                        <a href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($bep) }}"><img
                                    src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($bep->image,48,48) }}"
                                    alt="{{$bep->name}}"></a>
                    </div>
                    <div class="entry-c">
                        <div class="entry-title">
                            <h4><a href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($bep) }}">{{$bep->name}}</a>
                            </h4>
                        </div>
                        <ul class="entry-meta">
                            <li>
                                <i class="icon-calendar3"></i> {{date('d-m-Y',strtotime($bep->created_at))}}
                            </li>

                        </ul>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif