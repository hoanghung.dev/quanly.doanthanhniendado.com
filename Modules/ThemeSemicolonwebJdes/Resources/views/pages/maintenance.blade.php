@extends('themesemicolonwebjdes::layouts.default')
@section('main_content')
    <div id="wrapper" class="clearfix">
        <header id="header" class="full-header no-print">
            <div id="header-wrap">
                <div class="container clearfix">
                    <div id="logo" class="divcenter">
                        <a href="/" class="standard-logo" data-dark-logo="images/logo-dark.png"><img
                                    src="/public/frontend/themes/semicolonweb/images/logo.png" alt="Canvas Logo"
                                    class="divcenter"></a>
                        <a href="/" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img
                                    src="/public/frontend/themes/semicolonweb/images/logo@2x.png" alt="Canvas Logo"></a>
                    </div>
                </div>
            </div>
        </header>
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="heading-block center nobottomborder">
                        <h1>Site is Under Maintenance</h1>
                        <span>Please check back in sometime.</span>
                    </div>
                    <div class="col_one_third topmargin">
                        <div class="feature-box fbox-center fbox-light fbox-plain">
                            <div class="fbox-icon">
                                <a href="#"><i class="icon-warning-sign"></i></a>
                            </div>
                            <h3>Why is the Site Down?</h3>
                            <p>The site is under maintenance probably because we are working to improve this website
                                drastically.</p>
                        </div>
                    </div>
                    <div class="col_one_third topmargin">
                        <div class="feature-box fbox-center fbox-light fbox-plain">
                            <div class="fbox-icon">
                                <a href="#"><i class="icon-time"></i></a>
                            </div>
                            <h3>What is the Downtime?</h3>
                            <p>We are usually back within 10-15 minutes but it definitely depends on the issue.</p>
                        </div>
                    </div>
                    <div class="col_one_third topmargin col_last">
                        <div class="feature-box fbox-center fbox-light fbox-plain">
                            <div class="fbox-icon">
                                <a href="#"><i class="icon-email3"></i></a>
                            </div>
                            <h3>Do you need Support?</h3>
                            <p>You may simply send us an Email at <a
                                        href="/cdn-cgi/l/email-protection#6a03040c052a090b041c0b1944090507"><span
                                            class="__cf_email__" data-cfemail="cba2a5ada48ba8aaa5bdaab8e5a8a4a6">[email&#160;protected]</span></a>
                                if you need urgent support.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

