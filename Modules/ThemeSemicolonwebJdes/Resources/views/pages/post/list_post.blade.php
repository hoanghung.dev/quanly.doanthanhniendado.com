@extends('themesemicolonwebjdes::layouts.default')
@section('tintuc')
    class="color-menu"
@endsection
@section('main_content')
    <div id="wrapper" class="clearfix">
        @include('themesemicolonwebjdes::template.menu_other')
        <?php
        $widget = \Modules\ThemeSemicolonwebJdes\Models\Widget::select(['name', 'content'])->where('location', 'title6')->where('status', 1)->first();
        $posts = \Modules\ThemeSemicolonwebJdes\Models\Post::where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->paginate(8);
        $category_post_ids = explode('|', trim(@$settings['category_post_ids'], '|'));
        $cate_id = \Modules\ThemeSemicolonwebJdes\Models\Category::where('type', 1)->where('id', @$category_post_ids[0])
            ->orderBy('order_no', 'desc')->orderBy('id', 'desc')->first();
        $cate_id2 = \Modules\ThemeSemicolonwebJdes\Models\Category::where('type', 1)->where('id', @$category_post_ids[1])
            ->orderBy('order_no', 'ASC')->orderBy('id', 'asc')->first();

        $beps1 = CommonHelper::getFromCache('post1_cate' . @$cate_id->id);
        if (!$beps1) {
            $beps1 = \Modules\ThemeSemicolonwebJdes\Models\Post::where('multi_cat', 'like', '%|' . @$cate_id->id . '|%')
                ->where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->paginate(8);
            CommonHelper::putToCache('post1_cate' . @$cate_id->id, $beps1);
        }

        $beps2 = CommonHelper::getFromCache('post2_cate' . @$cate_id2->id);
        if (!$beps2) {
            $beps2 = \Modules\ThemeSemicolonwebJdes\Models\Post::where('multi_cat', 'like', '%|' . @$cate_id2->id . '|%')
                ->where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->paginate(8);
            CommonHelper::putToCache('post2_cate' . @$cate_id2->id, $beps2);
        }

        if ($slug1 == @$cate_id->slug) {
            $posts = $beps1;
        }
        if ($slug1 == @$cate_id2->slug) {
            $posts = $beps2;
        }
        ?>
        <section id="page-title">
            <div class="container clearfix">
                <?php
                if ($slug1 == @$cate_id->slug) {
                    $widget = @$cate_id;
                }
                if ($slug1 == @$cate_id2->slug) {
                    $widget = @$cate_id2;
                }
                ?>
                    <div class="row" style="height: 30px;">
                        <div class="title_search col-md-8">
                            <h1>{{@$widget->name}}</h1>
                        </div>
                        <div class="search_post col-md-4">
                            <form action="/search-bai-viet" class="form-inline">
                                <input style="width: 270px;" name="search" class="form-control boder" type="search"
                                       placeholder="Tìm kiếm bài viết" aria-label="Search">
                                <button class="btn btn-success " type="submit">Tìm kiếm</button>
                            </form>
                        </div>

                    </div>
                @include('themesemicolonwebjdes::partials.breadcrumb')

            </div>

        </section>
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="postcontent nobottommargin col_last clearfix">
                        <div id="posts" class="small-thumbs">
                            @foreach($posts as $post)
                                <div class="entry clearfix">
                                    <div class="entry-image">
                                        <a href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($post) }}"><img
                                                    class="image_fade"
                                                    src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($post->image,300,225)}}"
                                                    alt="{{$post->name}}"></a>
                                    </div>
                                    <div class="entry-c">
                                        <div class="entry-title">
                                            <h2>
                                                <a href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($post) }}">{{$post->name}}</a>
                                            </h2>
                                        </div>
                                        <ul class="entry-meta clearfix">
                                            <li>
                                                <i class="icon-calendar3"></i> {{date('d-m-Y',strtotime($post->created_at))}}
                                            </li>
                                        </ul>
                                        <div class="entry-content">
                                            <p>{{$post->intro}}</p>
                                            <a href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($post) }}"
                                               class="more-link">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-12 paginatee">
                                {{ $posts->appends(Request::all())->links() }}
                            </div>
                        </div>
                    </div>
                    <div class="sidebar nobottommargin clearfix">
                        <div class="sidebar-widgets-wrap">
                            {{--<div class="widget clearfix">--}}
                            {{--<h4>Ảnh bìa</h4>--}}
                            {{--<div id="flickr-widget" class="flickr-feed masonry-thumbs" data-id="613394@N22"--}}
                            {{--data-count="16" data-type="group" data-lightbox="gallery"></div>--}}
                            {{--</div>--}}
                            @include('themesemicolonwebjdes::pages.home.partials.popular_post')
                            @include('themesemicolonwebjdes::pages.home.partials.banner')
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
