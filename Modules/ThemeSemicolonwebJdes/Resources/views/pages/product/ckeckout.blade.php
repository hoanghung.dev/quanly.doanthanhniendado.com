@extends('themesemicolonwebjdes::layouts.default')
@section('main_content')
    <style>
        .current {
            display: none;
        }
    </style>
    <div id="wrapper" class="clearfix">
        @include('themesemicolonwebjdes::template.menu_other')
        <?php
        $widget = \Modules\ThemeSemicolonwebJdes\Models\Widget::select(['name', 'content'])->where('location', 'title3')->where('status', 1)->first();
        ?>
        <section id="page-title">
            <div class="container clearfix">
                <h1>{{$widget->name}}</h1>
                @include('themesemicolonwebjdes::partials.breadcrumb')
            </div>
        </section>
        <section id="content">
            <div class="content-wrap">
                <form id="billing-form" name="billing-form" class="nobottommargin" action="" method="post">
                    <div class="container clearfix">
                        <div class="col_half">
                            <div class="card">
                            </div>
                        </div>
                        <div class="col_half col_last">
                            <div class="card">
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="row clearfix">
                            <div class="col-lg-6">
                                <h3>Địa chỉ người mua</h3>
                                <div id="billing-form" name="billing-form" class="nobottommargin" action="#"
                                     method="post">
                                    <div class="col_half">
                                        <label for="billing-form-name">Họ tên</label><label style="color: red;">*</label>
                                        <input type="text" id="billing-form-name" name="name" value="{{ old('name') }}"
                                               class="sm-form-control"/>
                                        @if ($errors->has('name'))
                                            <p class="text-danger">{{$errors->first('name')}}</p>
                                        @endif
                                    </div>
                                    <div class="clear"></div>
                                    <div class="col_half">
                                        <label for="billing-form-phone">Điện thoại</label><label style="color: red;">*</label>
                                        <input type="text" id="billing-form-phone" name="phone" value="{{ old('phone') }}"
                                               class="sm-form-control"/>
                                        @if ($errors->has('phone'))
                                            <p class="text-danger">{{$errors->first('phone')}}</p>
                                        @endif
                                    </div>
                                    <div class="col_half col_last">
                                        <label for="billing-form-email">Email</label>
                                        <input type="email" id="billing-form-email" name="email" value="{{ old('email') }}"
                                               class="sm-form-control"/>
                                        @if ($errors->has('email'))
                                            <p class="text-danger">{{$errors->first('email')}}</p>
                                        @endif
                                    </div>
                                    <div class="col_full">
                                        <label for="billing-form-address">Địa chỉ</label><label style="color: red;">*</label>
                                        <input type="text" id="billing-form-address" name="address" value="{{ old('address') }}"
                                               class="sm-form-control"/>
                                        @if ($errors->has('address'))
                                            <p class="text-danger">{{$errors->first('address')}}</p>
                                        @endif
                                    </div>
                                    <div class="col_full">
                                        <label for="billing-form-date">Thời gian nhận hàng:</label>
                                        <input type="datetime-local" id="billing-form-date" name="date" value="{{ old('date') }}"
                                               class="sm-form-control"/>
                                        @if ($errors->has('date'))
                                            <p class="text-danger">{{$errors->first('date')}}</p>
                                        @endif
                                    </div>
                                    <div class="col_full">
                                        <label for="shipping-form-message">Ghi chú
                                            {{--<small>*</small>--}}
                                        </label>
                                        <textarea class="sm-form-control" id="shipping-form-message"
                                                  name="note" rows="6" cols="30">{{old('note')}}</textarea>
                                    </div>
                                    <label for="4"><input type="radio" name="thanhtoan" id="4" value="0" checked>
                                        Nhận hàng tại địa chỉ này</label><br>
                                    <label for="5"><input type="radio" name="thanhtoan" id="5" value="1">
                                        Nhận hàng tại địa chỉ khác</label>
                                </div>
                            </div>
                            <div id="nguoi-nhan" class="col-lg-6 current">
                                <h3 class="">Địa chỉ người nhận</h3>
                                <div class="col_half">
                                    <label for="shipping-form-name">Họ tên</label>
                                    <input type="text" id="shipping-form-name" name="shipping_form_name" value="{{ old('shipping_form_name') }}"
                                           class="sm-form-control"/>
                                </div>
                                <div class="clear"></div>
                                <div class="col_full">
                                    <label for="shipping-form-address">Địa chỉ</label>
                                    <input type="text" id="shipping-form-address" name="shipping_form_address" value="{{ old('shipping_form_address') }}"
                                           class="sm-form-control"/>
                                </div>
                                <div class="col_half">
                                    <label for="billing-form-email">Email</label>
                                    <input type="email" id="billing-form-email" name="shipping_form_email" value="{{ old('shipping_form_email') }}"
                                           class="sm-form-control"/>
                                </div>
                                <div class="col_half col_last">
                                    <label for="billing-form-lname">Giới tính</label>
                                    <select id="billing-form-date" class="sm-form-control"name="shipping_form_gender">
                                        <option value="Nam"{{ old('shipping_form_gender') == 'Nam' ? 'selected' :  ''}} >Nam</option>
                                        <option value="Nữ"{{ old('shipping_form_gender') == 'Nữ' ? 'selected' :  ''}} >Nữ</option>
                                    </select>
                                </div>
                                <div class="col_full">
                                    <label for="shipping-form-phone">Điện thoại</label>
                                    <input type="text" id="billing-form-phone" name="shipping_form_phone" value="{{ old('shipping_form_phone') }}"
                                           class="sm-form-control"/>
                                </div>
                                <div class="col_full">
                                    <label for="shipping-form-message">Ghi chú
                                        {{--<small>*</small>--}}
                                    </label>
                                    <textarea class="sm-form-control" id="shipping-form-message"
                                              name="shipping_form_message" rows="6" cols="30">{{ old('shipping_form_message') }}</textarea>
                                </div>
                            </div>
                            <div class="w-100 bottommargin"></div>
                            <div class="col-lg-6">
                                <h4>Đơn hàng</h4>
                                <div class="table-responsive">
                                    <table class="table cart">
                                        <thead>
                                        <tr>
                                            <th class="cart-product-thumbnail">&nbsp;</th>
                                            <th class="cart-product-name">Sản phẩm</th>
                                            <th class="cart-product-shop">Shop</th>
                                            <th class="cart-product-quantity">Số lượng</th>
                                            <th class="cart-product-subtotal">Giá</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($cart as $item)
                                            <?php
                                            $company_id = \Modules\ThemeSemicolonwebJdes\Models\Product::where('id', $item->id)->first()->company_id;
                                            $company = \Modules\EworkingCompany\Models\Company::where('id', $company_id)->first();
                                            ?>
                                            <tr class="cart_item">
                                                <td class="cart-product-thumbnail">
                                                    <a href="/{{ @$company->slug }}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct(\Modules\ThemeSemicolonwebJdes\Models\Product::find($item->id)) }}"><img
                                                                width="64" height="64"
                                                                src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($item->options->image) }}"
                                                                alt="{{ $item->name }}"></a>
                                                </td>
                                                <td class="cart-product-name">
                                                    <a href="/{{ @$company->slug }}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct(\Modules\ThemeSemicolonwebJdes\Models\Product::find($item->id)) }}">{{ $item->name }}</a>
                                                </td>
                                                <td class="cart-product-shop">
                                                    <a style="color: #333;" href="/{{ @$company->slug }}/san-pham">{{ $company->short_name }}</a>
                                                </td>
                                                <td class="cart-product-quantity">
                                                    <div class="quantity clearfix">
                                                        {{ $item->qty }}
                                                    </div>
                                                </td>
                                                <td class="cart-product-subtotal">
                                                <span class="amount">{{ number_format($item->price*$item->qty,0,'',',') }}
                                                    đ</span>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <h4>Tổng Giỏ Hàng</h4>
                                <div class="table-responsive">
                                    <table class="table cart">
                                        <tbody>
                                        <tr class="cart_item">
                                            <td class="notopborder cart-product-name">
                                                <strong>Tổng tiền hàng</strong>
                                            </td>
                                            <td class="notopborder cart-product-name">
                                                <span class="amount">{{ $total }}đ</span>
                                            </td>
                                        </tr>
                                        <tr class="cart_item">
                                            <td class="cart-product-name">
                                                <strong>Phí vận chuyển</strong>
                                            </td>
                                            <td class="cart-product-name">
                                                <span class="amount">Miễn phí</span>
                                            </td>
                                        </tr>
                                        <tr class="cart_item">
                                            <td class="cart-product-name">
                                                <strong>Tổng đơn hàng</strong>
                                            </td>
                                            <td class="cart-product-name">
                                                <span class="amount color lead"><strong>{{ $total }}đ</strong></span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="accordion clearfix">
                                    <div>
                                        <input type="radio" name="receipt_method" id="1"
                                               value="Thanh toán khi nhận hàng" checked>
                                        <label for="1">Thanh toán khi nhận hàng</label><br>
                                        <input type="radio" name="receipt_method" id="2" value="Chuyển khoản ngân hàng">
                                        <label for="2">Chuyển khoản ngân hàng</label><br>
                                    </div>
                                </div>
                                <button type="submit" class="button button-3d fright">Thanh toán</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection
@section('custom_script')
    <script>
        $(document).ready(function () {
            $('input[name=thanhtoan]').click(function () {
                if ($(this).prop("checked")) {
                    if ($(this).val() == 1) {
                        $('div#nguoi-nhan').removeClass('current');
                    } else {
                        $('div#nguoi-nhan').addClass('current');
                    }
                }
            });
        })
    </script>
@endsection