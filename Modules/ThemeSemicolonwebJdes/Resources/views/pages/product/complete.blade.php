@extends('themesemicolonwebjdes::layouts.default')
@section('main_content')

    <style type="text/css">
    #invoice{
    padding: 30px;
}

.invoice {
    position: relative;
    background-color: #FFF;
    min-height: 680px;
    padding: 15px
}

.invoice header {
    padding: 10px 0;
    margin-bottom: 20px;
    border-bottom: 1px solid #3989c6
}

.invoice .company-details {
    text-align: right
}

.invoice .company-details .name {
    margin-top: 0;
    margin-bottom: 0
}

.invoice .contacts {
    margin-bottom: 20px
}

.invoice .invoice-to {
    text-align: left
}

.invoice .invoice-to .to {
    margin-top: 0;
    margin-bottom: 0
}

.invoice .invoice-details {
    text-align: right
}

.invoice .invoice-details .invoice-id {
    margin-top: 0;
    color: #3989c6
}

.invoice main {
    padding-bottom: 50px
}

.invoice main .thanks {
    margin-top: -100px;
    font-size: 2em;
    margin-bottom: 50px
}

.invoice main .notices {
    padding-left: 6px;
    border-left: 6px solid #3989c6
}

.invoice main .notices .notice {
    font-size: 1.2em
}

.invoice table {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    margin-bottom: 20px
}

.invoice table td,.invoice table th {
    padding: 15px;
    background: #eee;
    border-bottom: 1px solid #fff
}

.invoice table th {
    white-space: nowrap;
    font-weight: 400;
    font-size: 16px
}

.invoice table td h3 {
    margin: 0;
    font-weight: 400;
    color: #3989c6;
    font-size: 1.2em
}

.invoice table .qty,.invoice table .total,.invoice table .unit {
    text-align: right;
    font-size: 1.2em
}

.invoice table .no {
    color: #fff;
    font-size: 1.6em;
    background: #3989c6
}

.invoice table .unit {
    background: #ddd
}

.invoice table .total {
    background: #3989c6;
    color: #fff
}

.invoice table tbody tr:last-child td {
    border: none
}

.invoice table tfoot td {
    background: 0 0;
    border-bottom: none;
    white-space: nowrap;
    text-align: right;
    padding: 10px 20px;
    font-size: 1.2em;
    border-top: 1px solid #aaa
}

.invoice table tfoot tr:first-child td {
    border-top: none
}

.invoice table tfoot tr:last-child td {
    color: #3989c6;
    font-size: 1.4em;
    border-top: 1px solid #3989c6
}

.invoice table tfoot tr td:first-child {
    border: none
}

.invoice footer {
    width: 100%;
    text-align: center;
    color: #777;
    border-top: 1px solid #aaa;
    padding: 8px 0
}

@media print {
    .invoice {
        font-size: 11px!important;
        overflow: hidden!important
    }

    .invoice footer {
        position: absolute;
        bottom: 10px;
        page-break-after: always
    }

    .invoice>div:last-child {
        page-break-before: always
    }
}    </style>
    <div id="wrapper" class="clearfix">
        @include('themesemicolonwebjdes::template.menu_other')
    <script type="text/javascript">
        window.alert = function(){};
        var defaultCSS = document.getElementById('bootstrap-css');
        function changeCSS(css){
            if(css) $('head > link').filter(':first').replaceWith('<link rel="stylesheet" href="'+ css +'" type="text/css" />');
            else $('head > link').filter(':first').replaceWith(defaultCSS);
        }
        $( document ).ready(function() {
          var iframe_height = parseInt($('html').height());
          window.parent.postMessage( iframe_height, 'https://bootsnipp.com');
        });
    </script>
    <div id="screen-shader" style="
            transition: opacity 0.1s ease 0s; 
            z-index: 2147483647;
            margin: 0; 
            border-radius: 0; 
            padding: 0; 
            background: #111111; 
            pointer-events: none; 
            position: fixed; 
            top: -10%; 
            right: -10%; 
            width: 120%; 
            height: 120%; 
            opacity: 0.6000;
            mix-blend-mode: multiply; 
            display: none;
        "></div>
<div>

<div id="invoice">

    {{--<div class="toolbar hidden-print">--}}
        {{--<div class="text-right">--}}
            {{--<button id="printInvoice" class="btn btn-info"><i class="icon-print"></i> In</button>--}}
            {{--<button class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Xuất dưới dạng PDF</button>--}}
        {{--</div>--}}
        {{--<hr>--}}
    {{--</div>--}}
    <div class="invoice overflow-auto">
        <div style="min-width: 600px">
            <header>
                <div class="row">
                    <div class="col">
                        <a target="_blank" href="/">
                            <img src="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}" data-holder-rendered="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}">
                            </a>
                    </div>
                    <div class="col company-details">
                        <h2 class="name">
                            <a>
                                {{ @$settings['name'] }}
                            </a>
                        </h2>
                        <div>{{ @$settings['address'] }}</div>
                        <div>{!! @$settings['hotline'] !!}  </div>
                        <div>{!! @$settings['email'] !!} </div>
                    </div>
                </div>
            </header>
            <main>
                <div class="row contacts">
                    <div class="col invoice-to">
                        <div class="text-gray-light">HÓA ĐƠN</div>
                        <h2 class="to">{{ @$bill->user->name }}</h2>
                        <div class="address">{{ @$bill->user->address }}</div>
                        <div class="email"><a href="mailto:john@example.com">{{ @$bill->user->email }}</a></div>

                    </div>
                    <div class="col invoice-to">
                        <div class="text-gray-light">NGƯỜI NHẬN</div>
                        <h2 class="to">{{ @$bill->user_name }}</h2>
                        <div class="address">{{ @$bill->user_address }}</div>
                        <div class="email"><a href="mailto:john@example.com">{{ @$bill->user_email }}</a></div>
                    </div>
                    <div class="col invoice-details">
                        <h1 class="invoice-id">HÓA ĐƠN :{{ @$bill->group_no }}</h1>
                        <div class="date">Ngày mua hàng: {{date('d/m/Y - H:i:s',strtotime(@$bill->created_at))}}</div>
                        <div class="date">Ngày nhận hàng:{{date('d/m/Y - H:i:s',strtotime(@$bill->date))}} </div>
                        <div>Phương thức thanh toán: {{ @$bill->receipt_method }}</div>
                    </div>
                </div>
                <table border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class="text-left">SẢN PHẨM</th>
                            <th class="text-right">SHOP</th>
                            <th class="text-right">ĐƠN GIÁ</th>
                            <th class="text-right">SỐ LƯỢNG</th>
                            <th class="cart-product-quantity">Editor</th>
                            <th class="text-right">THÀNH TIỀN</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach(@$bills as $bill_id)
                    @foreach(@$bill_id->order as $order)
                        <?php
                        $product = \Modules\ThemeSemicolonwebJdes\Models\Product::where('id', $order->product_id)->first();
                        ?>
                        <tr>
                            <td class="no">{{  @$key +=1 }}</td>
                            <td class="text-left"><h3>{{ @$order->product_name }}</h3></td>
                            <td class="unit">{{ @$order->company->short_name }}</td>
                            <td class="cart-product-price">
                                @if($order->key_editor_log == null)
                                    <span class="amount"> {{ number_format($order->price,0,'',',') }}đ</span>
                                @endif
                            </td>
                            <td class="cart-product-quantity">
                                @if($order->key_editor_log == null)
                                    <span class="quantity"> {{ number_format($order->quntity,0,'',',') }}</span>
                                @endif
                            </td>
                            <td>
                                @if($order->key_editor_log != null)
                                    <?php $editor = \Modules\JdesSetting\Models\Editor::find($order->editor_id);?>
                                    @include('jdessetting::editor.partials.product_show_editor', ['product_id' => $product->id, 'key_editor_log' => $order->key_editor_log, 'order' => $editor])
                                @endif
                            </td>
                            <td class="total">{{ number_format(@$order->price,0,'',',') }}
                                đ</td>
                        </tr>
                        @endforeach
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="1"></td>
                            <td colspan="2"></td>
                            <td colspan="3">TỔNG TIỀN HÀNG</td>
                            <td>{{ number_format(@$bills->sum('total_price'),0,'',',') }}đ</td>
                        </tr>
                        <tr>
                            <td colspan="1"></td>
                            <td colspan="2"></td>
                            <td colspan="3">PHÍ VẬN CHUYỂN</td>
                            <td>0đ</td>
                        </tr>
                        <tr>
                            <td colspan="1"></td>
                            <td colspan="2"></td>
                            <td colspan="3">TỔNG TIỀN</td>
                            <td>{{ number_format(@$bills->sum('total_price'),0,'',',') }}đ</td>
                        </tr>
                    </tfoot>
                </table>
                {{--<div class="thanks">Cảm ơn đã ủng hộ!</div>--}}
                <div class="notices">
                    <div>LƯU Ý:</div>
                    <div class="notice">KHI NHẬN HÀNG NẾU QUÝ KHÁCH PHÁT HIỆN SẢN PHẨM BỊ LỖI VUI LÒNG LIỆN HỆ ĐỂ ĐƯỢC TRỢ GIÚP VÀ ĐỔI TRẢ SỚM NHẤT . XIN CẢM ƠN.</div>
                </div>
            </main>

        </div>


    </div>
</div>
</div>
    </div>
    @endsection
@section('custom_script')
        <script type="text/javascript">
	 $('#printInvoice').click(function(){
            Popup($('.invoice')[0].outerHTML);
            function Popup(data)
            {
                window.print();
                return true;
            }
        });
        </script>
@endsection

