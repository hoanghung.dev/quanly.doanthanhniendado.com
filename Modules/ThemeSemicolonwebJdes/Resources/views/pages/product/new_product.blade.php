<h4>Sản Phẩm Mới</h4>
<div id="post-list-footer">
    <?php
    $news = \Modules\ThemeSemicolonwebJdes\Models\Product::where('company_id', $company->id)->where('status', 1)->orderBy('order_no', 'DESC')->orderby('created_at', 'DESC')->take(3)->get();
    ?>
    @foreach($news as $new)
        <div class="spost clearfix">
            <div class="entry-image">
                <a href="/{{$company->slug}}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($new) }}"><img
                            src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($new->image,48,48) }}"
                            alt="{{$new->name}}"></a>
            </div>
            <div class="entry-c">
                <div class="entry-title">
                    <h4>
                        <a href="/{{$company->slug}}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($new) }}">{{$new->name}}</a>
                    </h4>
                </div>
                <ul class="entry-meta">
                    <li class="color">{{ number_format($new->final_price,0,'',',') }}đ</li>
                </ul>
            </div>
        </div>
    @endforeach
</div>