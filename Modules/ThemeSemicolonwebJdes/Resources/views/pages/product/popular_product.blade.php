<h4>Sản phẩm nổi bật</h4>
<div id="Popular-item">
    <?php
    $populars = \Modules\ThemeSemicolonwebJdes\Models\Product::where('company_id', $company->id)->where('popular', 1)->where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id','DESC')->take(3)->get();
    ?>
    @foreach($populars as $popular)
        <div class="spost clearfix">
            <div class="entry-image">
                <a href="/{{$company->slug}}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($popular) }}"><img
                            src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($popular->image,48,48) }}"
                            alt="{{$popular->name}}"></a>
            </div>
            <div class="entry-c">
                <div class="entry-title">
                    <h4>
                        <a href="/{{$company->slug}}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($popular) }}">{{$popular->name}}</a>
                    </h4>
                </div>
                <ul class="entry-meta">
                    <li class="color">{{ number_format($popular->final_price,0,'',',') }}đ</li>

                </ul>
            </div>
        </div>
    @endforeach
</div>