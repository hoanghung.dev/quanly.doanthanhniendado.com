@extends('themesemicolonwebjdes::layouts.default')
@section('main_content')
    <div id="wrapper" class="clearfix">
        @include('themesemicolonwebjdes::template.menu_other')
        <section id="page-title">
            <div class="container clearfix">
                <h1>{{$q}}</h1>
                @include('themesemicolonwebjdes::partials.breadcrumb')
            </div>
        </section>
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="postcontent nobottommargin col_last">
                        <h2 style="text-align: center;"><span>Kết quả tìm kiếm với từ khóa: {{ $q }}</span></h2>
                        <div id="shop" class="shop product-3 grid-container clearfix" data-layout="fitRows">
                            @foreach($products as $prd)
                                <div class="product clearfix">
                                    <div class="product-image">
                                        <a href="/{{ @$prd->company->slug }}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($prd) }}"><img
                                                    src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($prd->image,265,352)}}"
                                                    alt="{{$prd->name}}"></a>
                                        <a href="/{{ @$prd->company->slug }}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($prd) }}"><img
                                                    src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($prd->image,265,352)}}"
                                                    alt="{{$prd->name}}"></a>
                                    </div>
                                    <div class="product-desc center">
                                        <div class="product-title"><h3><a
                                                        href="/{{ @$prd->company->slug }}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($prd) }}">{{$prd->name}}</a>
                                            </h3></div>
                                        <div class="product-price">
                                            <del>{{ number_format($prd->base_price,0,'',',') }}đ</del>
                                            <ins>{{ number_format($prd->final_price,0,'',',') }}đ</ins>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="col-md-9">
                                {{ $products->appends(Request::all())->links() }}
                            </div>
                        </div>
                    </div>
                    <div class="sidebar nobottommargin">
                        <div class="sidebar-widgets-wrap">
                            <div class="widget widget_links clearfix">
                                <?php
                                $menu_search = Menu::getByName('menu sidebar');
                                ?>
                                <h4>Danh mục</h4>
                                @if($menu_search)
                                    <ul class="menu">
                                        @foreach($menu_search as $menu)
                                            <li class="">
                                                <a href="{{ $menu['link'] }}" title="">{{ $menu['label'] }}</a>
                                                @if( $menu['child'] )
                                                    <ul class="sub-menu">
                                                        @foreach( $menu['child'] as $child )
                                                            <li class=""><a href="{{ $child['link'] }}"
                                                                            title="">{{ $child['label'] }}</a>
                                                                @if( $child['child'] )
                                                                    <ul class="sub-menu">
                                                                        @foreach( $child['child'] as $son )
                                                                            <li class=""><a href="{{ $son['link'] }}"
                                                                                            title="">{{ $son['label'] }}</a>
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                @endif
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                        @endif
                                    </ul>
                            </div>
                            <div class="widget clearfix">
                                <h4>Sản Phẩm Mới</h4>
                                <div id="post-list-footer">
                                    <?php
                                    $news = \Modules\ThemeSemicolonwebJdes\Models\Product::where('status', 1)->orderby('created_at', 'DESC')->orderBy('order_no', 'DESC')->take(3)->get();
                                    ?>
                                    @foreach($news as $new)
                                        <div class="spost clearfix">
                                            <div class="entry-image">
                                                <a href="{{ @$new->company->slug }}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($new) }}"><img
                                                            src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($new->image,48,48) }}"
                                                            alt="{{$new->name}}"></a>
                                            </div>
                                            <div class="entry-c">
                                                <div class="entry-title">
                                                    <h4>
                                                        <a href="{{ @$new->company->slug }}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($new) }}">{{$new->name}}</a>
                                                    </h4>
                                                </div>
                                                <ul class="entry-meta">
                                                    <li class="color">{{ number_format($new->final_price,0,'',',') }}đ
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="widget clearfix">
                                <h4>Sản phẩm nổi bật</h4>
                                <div id="Popular-item">
                                    <?php
                                    $populars = \Modules\ThemeSemicolonwebJdes\Models\Product::where('popular', 1)->where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id','DESC')->take(3)->get();
                                    ?>
                                    @foreach($populars as $popular)
                                        <div class="spost clearfix">
                                            <div class="entry-image">
                                                <a href="{{ @$popular->company->slug }}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($popular) }}"><img
                                                            src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($popular->image,48,48) }}"
                                                            alt="{{$popular->name}}"></a>
                                            </div>
                                            <div class="entry-c">
                                                <div class="entry-title">
                                                    <h4>
                                                        <a href="{{ @$popular->company->slug }}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($popular) }}">{{$popular->name}}</a>
                                                    </h4>
                                                </div>
                                                <ul class="entry-meta">
                                                    <li class="color">{{ number_format($popular->final_price,0,'',',') }}
                                                        đ
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="widget clearfix">
                                <?php
                                $widgets = \Modules\ThemeSemicolonwebJdes\Models\Widget::select(['name', 'content', 'location'])->where('location', 'facebook0')->where('status', 1)->first();
                                ?>
                                    @if(isset($company->slug))
                                        {!! @$company->fanpage !!}
                                    @else
                                        {!! @$widgets->content !!}
                                    @endif
                                {{--<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fjdesvietnam%2F&tabs=272&width=270&height=196&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId"--}}
                                        {{--width="270" height="196" style="border:none;overflow:hidden" scrolling="no"--}}
                                        {{--frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>--}}
                            </div>
                            <div class="widget subscribe-widget clearfix">
                                <h4>Đăng ký bản tin</h4>
                                <h5>Đăng ký email để chập nhật thông tin thường xuyên:</h5>
                                <div class="input-group divcenter">
                                    <div class="row">
                                        <input type="text" id="myInput" class="form-control col-10" name="email"
                                               placeholder="Nhập Email" required="">
                                        {{--<div class="input-group-append">--}}
                                        <button id="myBtn" style="cursor: pointer;" class="btn btn-success add-mail2 input-group-append col-2"><i class="icon-email2"></i>
                                        </button>
                                        {{--</div>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="widget clearfix" style="margin-top: 25px;">
                                <div class="viewall" style="display: flex">
                                    <h4>Khách Hàng</h4>
                                </div>
                                <div id="oc-clients-full" class="owl-carousel image-carousel carousel-widget"
                                     data-items="1" data-margin="10" data-loop="true" data-nav="false"
                                     data-autoplay="5000" data-pagi="false">
                                    <?php
                                    $users = \Modules\ThemeSemicolonwebJdes\Models\User::where('status', 1)->get();
                                    ?>
                                    @foreach($users as $user)
                                        <div class="oc-item"><a><img
                                                        src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($user->image,251,188)}}"
                                                        alt="{{$user->name}}"></a></div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

