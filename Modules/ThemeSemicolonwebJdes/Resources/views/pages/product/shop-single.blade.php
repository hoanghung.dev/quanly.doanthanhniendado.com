@extends('themesemicolonwebjdes::layouts.default')
@section('main_content')
    <?php
    if (!isset($order)) {
        if (isset($_GET['editor_id'])) {
            $order = \Modules\JdesSetting\Models\Editor::find($_GET['editor_id']);
        } else {
            $order = \Modules\JdesSetting\Models\Editor::where('product_ids', 'like', '%|' . $product->id . '|%')->first();
        }
    }
    ?>
    <div id="wrapper" class="clearfix">
        @include('themesemicolonwebjdes::template.menu_company')
        <section id="page-title">
            <div class="container clearfix productname">
                <h1>{{$product->name}}</h1>
                @include('themesemicolonwebjdes::partials.breadcrumb')
            </div>
        </section>
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="single-product">
                        <div class="product">
                            <div class="col_two_fifth">
                                <div class="product-image">
                                    @php
                                        $images = explode('|', @$product->image_extra);
                                        $inputs = explode('|', @$product->input_image_extra);
                                    @endphp

                                    <div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="true">
                                        <div class="flexslider">
                                            <div class="slider-wrap" data-lightbox="gallery">
                                                <div class="slide"
                                                     data-thumb="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($product->image,100,75)}}">
                                                    <a href="{{ asset('public/filemanager/userfiles/' . $product->image)  }}"
                                                       title="{{$product->name}}"
                                                       data-lightbox="gallery-item"><img
                                                                src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($product->image,429,571)}}"
                                                                alt="{{$product->name}}">
                                                    </a>
                                                </div>
                                                @foreach($images as $img)
                                                    @if($img != '')
                                                        <div class="slide"
                                                             data-thumb="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$img,100,75)}}">
                                                            <a href="{{ asset('public/filemanager/userfiles/' . @$img)  }}"
                                                               title="{{$product->name}}"
                                                               data-lightbox="gallery-item"><img
                                                                        src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$img,429,571)}}"
                                                                        alt="{{$product->name}}">
                                                            </a>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="downpic">
                                        <a class="pl-3" style="visibility:hidden;">Tải template</a>
                                        @foreach($inputs as $input)
                                            @if(@$input != '')
                                                <a class="pl-3"
                                                   href="{{$input}}" target="_blank">Tải template</a>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="col_two_fifth product-desc">
                                @if(!is_object($order))
                                    <div class="product-price" style="margin-bottom:10px; ">
                                        <del>{{ number_format($product->base_price,0,'',',') }}đ</del>
                                        <ins>{{ number_format($product->final_price,0,'',',') }}đ</ins>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="line"></div>
                                    <form class="cart nobottommargin clearfix" action="/gio-hang/add/{{ $product->id }}"
                                          method="get"
                                          enctype='multipart/form-data'>
                                        <div class="quantity clearfix">
                                            <input type="button" value="-" class="minus">
                                            <input type="text" step="1" min="1" name="qty" value="1" title="Qty"
                                                   class="qty" size="4"/>
                                            <input type="button" value="+" class="plus">
                                        </div>
                                        <button type="submit" class="add-to-cart button nomargin">Thêm vào giỏ hàng
                                        </button>
                                    </form>
                                    <div class="clear"></div>
                                    <div class="line"></div>
                                @endif

                                <p>{!! $product->intro !!}</p>
                                <div class="noborder clearfix">
                                    <span>Chia sẻ:</span>
                                    <div id="fb-root"></div>
                                    <script async defer crossorigin="anonymous"
                                            src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v5.0"></script>
                                    <div class="fb-share-button"
                                         data-href="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"
                                         data-layout="button"
                                         data-size="large"><a target="_blank"
                                                              href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"
                                                              class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
                                    {{--<div>--}}
                                    {{--<a href="{!! @$settings['instagram'] !!}" target="_blank"--}}
                                    {{--class="social-icon si-borderless si-gplus">--}}
                                    {{--<i class="icon-instagram2"></i>--}}
                                    {{--<i class="icon-instagram2"></i>--}}
                                    {{--</a>--}}
                                    {{--</div>--}}
                                    @if(is_object($order))
                                        <div class="col_full">
                                            <div>
{{--                                                <ul class="tab-nav clearfix">--}}
{{--                                                    <li><a href="#tabs-1"><i class="icon-align-justify2"></i><span--}}
{{--                                                                    class="d-none d-md-inline-block"> Editor tính giá sản phẩm</span></a>--}}
{{--                                                    </li>--}}
{{--                                                </ul>--}}
                                                <br>
                                                <div class="tab-content clearfix" id="tabs-1">
                                                    <div id="editor-fontend">
                                                        <?php $key_editor_log = '_remote_addr_' . $_SERVER['REMOTE_ADDR'];?>
                                                        @include('jdessetting::editor.partials.editor_frontend', ['order' => $order, 'key_editor_log' => $key_editor_log])
                                                    </div>
                                                    <form class="cart nobottommargin clearfix"
                                                          action="/gio-hang/add/{{ $product->id }}"
                                                          method="get"
                                                          enctype='multipart/form-data'>
                                                        <input name="key_editor_log" value="{{ $key_editor_log }}"
                                                               type="hidden">
                                                        <input name="editor_id" value="{{ $order->id }}"
                                                               type="hidden">
                                                        <div class="quantity clearfix" style="display: none;">
                                                            <input type="button" value="-" class="minus">
                                                            <input type="text" step="1" min="1" name="qty" value="1"
                                                                   title="Qty"
                                                                   class="qty" size="4"/>
                                                            <input type="button" value="+" class="plus">
                                                        </div>
                                                        <button type="submit" class="add-to-cart button nomargin">
                                                            Thêm vào
                                                            giỏ hàng
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col_one_fifth col_last">
                                <div class="feature-box fbox-plain fbox-dark fbox-small">
                                    <?php
                                    $widgets = \Modules\ThemeSemicolonwebJdes\Models\Widget::select(['name', 'content'])->whereIn('location', ['product0', 'product1', 'product2'])->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->get();
                                    ?>
                                    @foreach($widgets as $widget)
                                        <h3>{{$widget->name}}</h3>
                                        <p class="notopmargin">{!! $widget->content !!}</p>
                                        <br>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col_full nobottommargin">
                                <div class="tabs clearfix nobottommargin" id="tab-1">
                                    <ul class="tab-nav clearfix">
                                        <li><a href="#tabs-1"><i class="icon-align-justify2"></i><span
                                                        class="d-none d-md-inline-block"> Mô tả</span></a></li>
                                    </ul>
                                    <div class="tab-container">
                                        <div class="tab-content clearfix" id="tabs-1">
                                            <p>{!! $product->content !!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="line"></div>
                    <div class="col_full nobottommargin">
                        <h4>Sản phẩm liên quan</h4>
                        <div id="oc-product" class="owl-carousel product-carousel carousel-widget" data-margin="30"
                             data-pagi="false" data-autoplay="5000" data-items-xs="1" data-items-md="2"
                             data-items-lg="3" data-items-xl="4">
                            <?php
                            $categoryid = \Modules\ThemeSemicolonwebJdes\Models\Product::find($product->id)->category_id;
                            $relates = \Modules\ThemeSemicolonwebJdes\Models\Product::where('category_id', $categoryid)->where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->get();
                            ?>
                            @foreach($relates as $relate)
                                <div class="oc-item">
                                    <div class="product iproduct clearfix">
                                        <div class="product-image">
                                            <a href="/{{$company->slug}}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($relate) }}"><img
                                                        src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($relate->image,262,350)}}"
                                                        alt="{{$relate->name}}"></a>
                                            <a href="/{{$company->slug}}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($relate) }}"><img
                                                        src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($relate->image,262,350)}}"
                                                        alt="{{$relate->name}}"></a>
                                        </div>
                                        <div class="product-desc center">
                                            <div class="product-title"><h3><a
                                                            href="/hobasoft{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($relate) }}">{{$relate->name}}</a>
                                                </h3></div>
                                            <div class="product-price">
                                                <del>{{ number_format($relate->base_price,0,'',',') }}đ</del>
                                                <ins>{{ number_format($relate->final_price,0,'',',') }}đ</ins>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('custom_head')
    <style>
        @media (max-width: 767px) {
            #page-title h1 {
                max-width: 100% !important;
            }

            .downpic > a {
                max-width: 80px !important;
                padding: 0 !important;
            }
        }

        @media (max-width: 575.98px) {
            .portfolio .portfolio-item {
                display: contents;
                position: unset !important;
                text-align: center;
            }
        }
    </style>
@endsection
