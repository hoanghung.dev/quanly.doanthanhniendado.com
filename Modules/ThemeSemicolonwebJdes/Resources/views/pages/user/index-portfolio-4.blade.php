@extends('themesemicolonwebjdes::layouts.default')
@section('khachhang')
    class="color-menu"
@endsection
@section('main_content')
    <div id="wrapper" class="clearfix">
        @include('themesemicolonwebjdes::template.menu_company')
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <?php
                    $widgets = \Modules\ThemeSemicolonwebJdes\Models\Widget::select(['name', 'content'])->where('location', 'user0')->where('status', 1)->first();
                    ?>
                    <div class="heading-block nobottomborder center">
                        <h1>{{@$widgets->name}}</h1>
                        <span>{!! @$widgets->content !!}</span>
                    </div>
                    <div class="fslider bottommargin" data-animation="fade">
                        <div class="flexslider">
                            <div class="slider-wrap">
                                @include('themesemicolonwebjdes::pages.user.user_page_slide')
                            </div>
                        </div>
                    </div>
                    <div class="promo promo-border bottommargin">
                        <h3>Gọi cho chúng tôi ngay hôm nay <span>{!! @$company->tel !!}</span> hoặc gửi Email cho
                            chúng tôi <span><a
                                        href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                        data-cfemail="186b6d6868776a6c587b79766e796b367b7775">{!! @$company->email !!}</a></span>
                        </h3>
                        <span>Chúng tôi cố gắng cung cấp cho khách hàng của chúng tôi hỗ trợ xuất sắc hàng đầu để làm cho trải nghiệm của họ trở nên tuyệt vời</span>
                        <a href="#" class="button button-xlarge button-rounded">Bắt đầu</a>
                    </div>
                    <div id="portfolio-ajax-wrap">
                        <div id="portfolio-ajax-container"></div>
                    </div>
                    <div id="portfolio-ajax-loader"><img
                                src="/public/frontend/themes/semicolonweb/images/preloader-dark.gif"
                                alt="Preloader"></div>
                    <div id="portfolio-shuffle" class="portfolio-shuffle" data-container="#portfolio">
                        <i class="icon-random"></i>
                    </div>
                    <div class="clear"></div>
                    <div id="portfolio" class="portfolio grid-container portfolio-nomargin portfolio-ajax clearfix">
                        @foreach($users as $user)
                            <article id="portfolio-item-1" data-loader="include/ajax/portfolio-ajax-image.php"
                                     class="portfolio-item pf-media pf-icons" style="margin-right: 50px;">
                                <div class="portfolio-image">
                                    <a href="portfolio-single.html">
                                        <img src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($user->image,285,214)}}"
                                             alt="{{$user->name}}">
                                    </a>
                                    <div class="portfolio-overlay">
                                        <a href="#" class="center-icon"><i class="icon-line-expand"></i></a>
                                    </div>
                                </div>
                                <div class="portfolio-desc">
                                    <h3><a href="#">{{$user->name}}</a></h3>
                                    <p>{{$user->email}}</p>
                                </div>
                            </article>
                        @endforeach
                        <div class="col-md-12 paginatee">
                            {{ $users->appends(Request::all())->links() }}
                        </div>
                    </div>
                </div>
                {{--<div class="section footer-stick">--}}
                    {{--<h4 class="uppercase center">Khách hàng nói gì về chúng tôi</h4>--}}
                    {{--<div class="fslider testimonial testimonial-full" data-animation="fade" data-arrows="false">--}}
                        {{--<div class="flexslider">--}}
                            {{--<div class="slider-wrap">--}}
                                {{--<?php--}}
                                {{--$clients = \Modules\ThemeSemicolonwebJdes\Models\Clientsay::all();--}}
                                {{--?>--}}
                                {{--@foreach($clients as $client)--}}
                                    {{--<div class="slide">--}}
                                        {{--<div class="testi-image">--}}
                                            {{--<a><img--}}
                                                        {{--src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($client->image),72,72}}"--}}
                                                        {{--alt="{{$client->name}}"></a>--}}
                                        {{--</div>--}}
                                        {{--<div class="testi-content">--}}
                                            {{--<p>{!! $client->content !!}</p>--}}
                                            {{--<div class="testi-meta">--}}
                                                {{--{{$client->name}}--}}
                                                {{--<span>{!!$client->intro!!}</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--@endforeach--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </section>
    </div>
@endsection
