<?php
$banners = \Modules\ThemeSemicolonwebJdes\Models\Banner::where('location', 'banner_user')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->get();
?>
@foreach($banners as $banner)
    <div class="slide">
        <a href="{{$banner->link}}">
            <img src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($banner->image,1140,500)}}"
                 alt="{{$banner->name}} ">
        </a>
        <div class="flex-caption slider-caption-bg">{{$banner->name}}</div>
    </div>
@endforeach