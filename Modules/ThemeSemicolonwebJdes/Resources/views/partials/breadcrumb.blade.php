<?php
$uri = explode('/', $_SERVER['REQUEST_URI']);
$breadcrumb = [
    '/' => 'Trang chủ'
];
$url = '';
foreach ($uri as $v) {
    if ($v != '') {
        $url .= '/' . $v;
        if (strpos($v, '.html')) {      //  Bai viet | San pham
            $prd = \Modules\ThemeSemicolonwebJdes\Models\Product::select('name')->where('slug', str_replace('.html', '', $v))->where('status', 1)->first();
            if (is_object($prd)) {
                $breadcrumb[$url] = $prd->name;
            } else {
                $post = \Modules\ThemeSemicolonwebJdes\Models\Product::select('name')->where('slug', str_replace('.html', '', $v))->where('status', 1)->first();
                if (is_object($post)) {
                    $breadcrumb[$url] = $post->name;
                }
            }
        } else {        //  Company | category
            $com = \Modules\ThemeSemicolonwebJdes\Models\Company::select('name')->where('slug', $v)->where('status', 1)->first();
            if (is_object($com)) {
                $breadcrumb[$url] = $com->name;
            } else {
                $cat = \Modules\ThemeSemicolonwebJdes\Models\Category::select('name')->where('slug', $v)->where('status', 1)->first();
                if (is_object($cat)) {
                    $breadcrumb[$url] = $cat->name;
                }
            }
        }
    }
}
?>
<ol class="breadcrumb">
    @foreach($breadcrumb as $k => $v)
        <li class="breadcrumb-item" aria-current="page"><a href="{{ $k }}">{{ $v }}</a></li>
    @endforeach
</ol>
