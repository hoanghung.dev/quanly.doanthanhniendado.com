<footer id="footer" class="dark">

    <div class="container no-print">
        <?php
        $widgets = \Modules\ThemeSemicolonwebJdes\Models\Widget::select(['name', 'content', 'location'])->whereIn('location', ['footer_0', 'footer_1', 'footer_2'])->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->get();
        $widgets4 = \Modules\ThemeSemicolonwebJdes\Models\Widget::select(['name', 'content', 'location'])->where('location', 'footer_3')->where('status', 1)->first();
        ?>
        <div class="footer-widgets-wrap clearfix">
            <div class="col_two_third">
                @foreach($widgets as $key => $v)
                    <div class="col_one_third {{($v->location == 'footer_2')? 'col_last':''}}">
                        <div class="widget clearfix">
                            <h4>{{$v->name}}</h4>
                            <p>{!!$v->content!!}</p>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col_one_third col_last">
                <div class="widget clearfix" style="margin-bottom: -20px;">
                    <h4>{{$widgets4->name}}</h4>
                    <p>{!!$widgets4->content!!}</p>
                </div>
                <div class="widget subscribe-widget clearfix">
                    <h5><strong>Đăng ký</strong> để cập nhật thông tin thường xuyên:</h5>
                    <div class="widget-subscribe-form-result"></div>
                    <div class="input-group divcenter">
                        {{--<div class="input-group-prepend">--}}
                        {{--<div class="input-group-text"><i class="icon-email2"></i></div>--}}
                        {{--</div>--}}
                        <div class="row">
                            <input type="email" id="widget-subscribe-form-email" name="email"
                                   class="form-control required email col-9" placeholder="Nhập Email">
                            {{--<div class="input-group-append col-md-3">--}}
                            <button id="myBtn2" style="cursor: pointer;"
                                    class="btn btn-success add-mail2 col-3 input-group-append">Đăng ký
                            </button>
                            {{--</div>--}}
                        </div>

                    </div>
                </div>
                <div class="widget clearfix" style="margin-bottom: -20px;">
                    <div class="row">
                        <div class="col-lg-6 clearfix bottommargin-sm">
                            <a href="{!! @$settings['fanpage'] !!}" target="_blank"
                               class="social-icon si-dark si-colored si-facebook nobottommargin"
                               style="margin-right: 10px;">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>
                            <a href="{!! @$settings['fanpage'] !!}  ">
                                <small style="display: block; margin-top: 3px;"><strong>Thích chúng tôi</strong><br>trên
                                    Facebook
                                </small>
                            </a>
                        </div>
                        <div class="col-lg-6 clearfix bottommargin-sm">
                            <a href="{!! @$settings['instagram'] !!} " target="_blank"
                               class="social-icon si-dark si-colored si-facebook nobottommargin"
                               style="margin-right: 10px;">
                                <i class="icon-instagram"></i>
                                <i class="icon-instagram"></i>
                            </a>
                            <a href="{!! @$settings['instagram'] !!}">
                                <small style="display: block; margin-top: 3px;"><strong>Theo dõi chúng tôi</strong><br>trên
                                    Instagram
                                </small>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="copyrights no-print">
        <div class="container clearfix">
            <?php
            $widget_bot_1 = \Modules\ThemeSemicolonwebJdes\Models\Widget::select(['name', 'content'])->where('location', 'footer_bot_1')->where('status', 1)->first();
            $widget_bot_2 = \Modules\ThemeSemicolonwebJdes\Models\Widget::select(['name', 'content'])->where('location', 'footer_bot_2')->where('status', 1)->first();
            ?>
            <div class="col_half">
                Copyrights &copy; Jdes.vn<br>
                <div class="copyright-links">{!! @$widget_bot_1->name  !!} / {!! @$widget_bot_2->name  !!}
                </div>
            </div>
            <div class="col_half col_last tright">
                <div class="clear"></div>
                <i class="icon-envelope2"></i> {!! @$settings['email'] !!}
                <span class="middot">&middot;</span> <i class="icon-headphones"></i>{!! @$settings['hotline'] !!}<span
                        class="middot">&middot;</span>
            </div>
        </div>
    </div>
    <div class="container print">
        <h1>Custom footer khi in hóa đơn ở đây nhé ^^ </h1>
    </div>
</footer>
<div style="font-family: font-icons!important;" id="gotoTop" class="icon-angle-up"></div>
{{--<script data-cfasync="false" src="{{asset(config('frontend_asset').'/js/email-decode.min.js')}}"></script>--}}
<script src="{{asset(config('core.frontend_asset').'/js/plugins.js')}}"></script>
<script src="{{asset(config('core.frontend_asset').'/js/functions.js?v=27')}}"></script>
@section('custom_script')
    <script>
        $('.add-mail2').click(function () {
            let email = $(this).parents('.divcenter').find('input').val();
            $.ajax({
                url: '{{route('contact.post2',@$comSlug)}}',
                type: 'POST',
                data: {
                    email: email
                },
                success: function (data) {
                    if (data.status) {
                        toastr.success("Đã thêm email thành công");
                    } else {
                        toastr.error("Thêm thất bại ");
                    }
                }
            });
            $(this).prop("disabled", true);
            $(this).parent().parent().find('input[name=email]').val('');
        });
        $('#sidebar-tabs ul li').click(function (event) {
            event.preventDefault();
            $('#sidebar-tabs .tab-container .tab-content').hide();
            $('#sidebar-tabs ul li a').css('background', '#f2f2f2');
            $(this).children().css('background', '#ffffff');
            let id = $(this).find('a').attr('href');
            $(id).show();
        });
        $('.quantity .minus').click(function (event) {
            event.preventDefault();
            let input_qty = $(this).parent().find('.qty');
            let min = input_qty.attr('min');
            if (input_qty.val() > min) {
                input_qty.val(input_qty.val() - 1);
            }
        });

        $('.quantity .plus').click(function (event) {
            event.preventDefault();
            let input_qty = $(this).parent().find('.qty');
            input_qty.val(parseInt(input_qty.val()) + 1);
        });

    </script>
@endsection
