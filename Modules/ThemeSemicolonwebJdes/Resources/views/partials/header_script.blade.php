<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i"
      rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="{{ URL::asset(config('core.frontend_asset').'/css/bootstrap.css') }}" type="text/css"/>
<link rel="stylesheet" href="{{URL::asset(config('core.frontend_asset').'/css/style.css')}}" type="text/css"/>
<link rel="stylesheet" href="{{URL::asset(config('core.frontend_asset').'/css/custom.css')}}" type="text/css"/>
<link rel="stylesheet" href="{{URL::asset(config('core.frontend_asset').'/css/dark.css')}}" type="text/css"/>
<link rel="stylesheet" href="{{URL::asset(config('core.frontend_asset').'/css/font-icons.css')}}" type="text/css"/>
<link rel="stylesheet" href="{{URL::asset(config('core.frontend_asset').'/css/animate.css')}}" type="text/css"/>
<link rel="stylesheet" href="{{URL::asset(config('core.frontend_asset').'/css/magnific-popup.css')}}" type="text/css"/>
<link rel="stylesheet" href="{{URL::asset(config('core.frontend_asset').'/css/responsive.css')}}" type="text/css"/>
<link rel="stylesheet" href="{{asset('public/libs/bootstrap/css/bootstrap.min.css')}}">
<script src="{{asset(config('core.frontend_asset').'/js/jquery.js')}}"></script>
@yield('custom_head')
<style>
    .postcontent.nobottommargin.clearfix ,
    .portfolio-filter {
        word-wrap: break-word !important;
    }
    .color-menu{
        color: #1abc9c!important;
    }
    .print {
        display: none;
    }
    #wrapper{
        opacity: 1 !important;
    }
</style>