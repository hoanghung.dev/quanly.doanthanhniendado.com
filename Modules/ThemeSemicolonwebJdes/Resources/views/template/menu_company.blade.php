<?php
$cart = \Cart::content();
$total = \Cart::total(0, '', ',');
?>
<style>
    .color-menu{
        color: #1abc9c!important;
    }

</style>
<header id="header" class="full-header no-print" style="">
    <div id="header-wrap">
        <div class="container clearfix">
            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
            <div id="logo">
                <a href="/" class="standard-logo"
                   data-dark-logo="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}">
                    <img
                            src="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}"
                            alt="{{ @$settings['name'] }}">
                </a>
                <a href="/" class="retina-logo"
                   data-dark-logo="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}">
                    <img
                            src="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}"
                            alt="{{ @$settings['name'] }}">
                </a>
            </div>
            <a href="/{{@$company->slug}}/san-pham"><label class="company-name">{{@$company->short_name}}</label></a>
            <nav id="primary-menu">
                <ul>
                    <li><a @yield('sanpham') href="/{{@$company->slug}}/san-pham">
                            <div>Sản phẩm</div>
                        </a></li>
                    <li><a href="/tin-tuc">
                            <div>Tin tức</div>
                        </a>
                        <ul>
                            <li><a href="/tin-tuc/kien-thuc.html">
                                    <div><i class="icon-stack"></i>Kiến thức</div>
                                </a></li>
                            <li><a href="/tin-tuc">
                                    <div><i class="icon-gift"></i>Tin tức</div>
                                </a></li>
                            <li><a href="/tin-tuc/tuyen-dung.html">
                                    <div><i class="icon-umbrella"></i>Tuyển dụng</div>
                                </a></li>
                        </ul>
                    <li><a @yield('khachhang') href="/{{@$company->slug}}/khach-hang">
                            <div>Khách hàng</div>
                        </a></li>
                    <li><a @yield('gioithieu') href="/{{@$company->slug}}/gioi-thieu">
                            <div>Giới thiệu</div>
                        </a></li>
                    <li><a @yield('lienhe') href="/{{@$company->slug}}/lien-he">
                            <div>Liên hệ</div>
                        </a></li>.
                    <li><a @yield('trogiup') href="/tro-giup">
                            <div>Trợ giúp</div>
                        </a></li>
                    @if(!\Auth::guard('admin')->check())
                    <li><a href="/admin/login">
                            <div>Đăng nhập</div>
                        </a></li>
                        @endif
                </ul>
                <div id="top-search" style="float: left">
                    <a href="#" id="top-search-trigger"><i class="icon-search3"></i></a>
                    <form action="/tim-kiem" method="get">
                        <input type="text" name="q" class="form-control" value=""
                               placeholder="Tìm kiếm">
                    </form>
                </div>
                <div id="top-cart" style="float: left">
                    <a href="#" id="top-cart-trigger"><i
                                class="icon-shopping-cart"></i><span> {{ count(Cart::content()) }}</span></a>
                    <div class="top-cart-content">
                        <div class="top-cart-title">
                            <h4>Giỏ hàng</h4>
                        </div>
                        @foreach($cart as $item)
                            <div class="top-cart-items">
                                <div class="top-cart-item clearfix">
                                    <div class="top-cart-item-image">
                                        <a href="/{{@$company->slug}}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct(\Modules\ThemeSemicolonwebJdes\Models\Product::find($item->id)) }}"><img
                                                    src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($item->options->image) }}"
                                                    alt="{{ $item->name }}"/></a>
                                    </div>
                                    <div class="top-cart-item-desc">
                                        <a href="/{{@$company->slug}}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct(\Modules\ThemeSemicolonwebJdes\Models\Product::find($item->id)) }}">{{ $item->name }}</a>
                                        <span class="top-cart-item-price">{{ number_format($item->price,0,'',',') }}
                                            đ</span>
                                        <span class="top-cart-item-quantity">x {{ $item->qty }}</span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="top-cart-action clearfix">
                            <span class="fleft top-checkout-price">{{$total}} đ </span>
                            <a href="/gio-hang" class="button button-3d button-small nomargin fright">Xem giỏ
                                hàng</a>
                        </div>
                    </div>
                </div>

                @if(\Auth::guard('admin')->check())


                    <style>
                        .profile-users {
                            position: relative;

                        }

                        #icon-users {
                            padding: 0px 12px;
                            margin-bottom: 0;
                            font-size: 14px;
                            font-weight: 400;
                            /*line-height: 80px;*/
                            text-align: center;
                            white-space: nowrap;
                            vertical-align: middle;
                            -ms-touch-action: manipulation;
                            touch-action: manipulation;
                            cursor: pointer;
                            -webkit-user-select: none;
                            -moz-user-select: none;
                            -ms-user-select: none;
                            user-select: none;
                            background-image: none;
                            border: 1px solid transparent;
                            border-radius: 4px;
                        }

                        .profile-user {
                            display: none;
                        }

                        .profile-users:hover .profile-user {
                            position: absolute;
                            display: block;
                            right: 0;
                            top: 100%;
                            min-width: 160px;
                            padding: 5px 0;
                            font-size: 14px;
                            text-align: left;
                            list-style: none;
                            background-color: #fff;
                            -webkit-background-clip: padding-box;
                            background-clip: padding-box;
                            border: 1px solid rgba(0, 0, 0, .15);
                            border-radius: 4px;
                            -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
                            box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
                        }

                        #primary-menu .profile-users .profile-user > a {
                            display: block;
                            width: 100% !important;
                            padding: 10px 10px !important;
                            height: auto !important;
                            line-height: normal;
                            clear: both !important;
                            font-weight: 400 !important;
                            color: #212529 !important;
                            text-align: inherit !important;
                            white-space: nowrap !important;
                            background-color: transparent !important;
                            border: 0 !important;
                        }

                        #primary-menu .profile-users .profile-user > a:hover {
                            color: #16181b !important;
                            text-decoration: none !important;
                            background-color: #f8f9fa !important;
                        }
                        .sticky-header #header-wrap #primary-menu .profile-users{
                            line-height: 55px!important;
                            margin-left: 10px;
                            -webkit-transition: line-height .4s ease, opacity .3s ease;
                            -o-transition: line-height .4s ease, opacity .3s ease;
                            transition: line-height .4s ease, opacity .3s ease;
                        }
                        .full-header #header-wrap #primary-menu .profile-users{
                            line-height: 95px;
                            margin-left: 10px;
                        }
                        @media (max-width: 991px) {
                            .full-header #header-wrap #primary-menu .profile-users{
                                position: absolute;
                                top: 0;
                                right: 0;
                            }
                        }
                        /* -webkit-transition: line-height .4s ease, opacity .3s ease; */
                        /*-o-transition: line-height .4s ease, opacity .3s ease;*/
                        /* transition: line-height .4s ease, opacity .3s ease;
</style>
                    <div class="profile-users" style="float: left">
                        <i class="icon-user show" id="icon-users"></i>
                        <div class="profile-user">
                            <a href="/admin/profile">Profile</a>
                            <a href="/admin">Vào trang quản trị</a>
                            <a href="/admin/logout" title="">Đăng xuất</a>
                        </div>
                    </div>
                @endif

            </nav>
        </div>
    </div>
    <script>
        $(document).ready(function () {

        });
    </script>
</header>
{{--@section('custom_script')--}}
    {{--<script>--}}
        {{--$(document).ready(function () {--}}
            {{--$('.menu-company').click(function () {--}}
                {{--$(this).addClass('color-menu');--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
{{--@endsection--}}