<?php
$main_menu = Menu::getByName('Main menu');
$categorys = \Modules\ThemeSemicolonwebJdes\Models\Category::all();
$cart = \Cart::content();
$total = \Cart::total(0, '', ',');
?>
<style>
    .cong-ty{
        font-weight: 900;
    }
</style>
<header id="header" class="full-header">
    <div id="header-wrap ">
        <div class="container clearfix no-print">
            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
            <div id="logo">
                <a href="/" class="standard-logo"
                   data-dark-logo="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}"><img
                            src="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}"
                            alt="{!! @$settings['name'] !!}"></a>
                <a href="/" class="retina-logo"
                   data-dark-logo="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}"><img
                            src="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}"
                            alt="{!! @$settings['name'] !!}"></a>
            </div>
            <?php
            $actives = array(
//                'trangchu',
                'shop',
                'tintuc',
                'gioithieu',
                'lienhe',
                'trogiup'
            );
            ?>
            <nav id="primary-menu">
                @if($main_menu)
                    <ul class="menu">
                        @foreach($main_menu as $k=>$menu)

                            <li class="">
                                <a @yield($actives[$k]) href="{{ $menu['link'] }}" title="">{{ $menu['label'] }}</a>

                                @if( $menu['child'] )
                                    <ul class="sub-menu">
                                        @foreach( $menu['child'] as $child )
                                            <li class=""><a href="{{ $child['link'] }}"
                                                            title="">{{ $child['label'] }}</a></li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                        @endif
                    </ul>
                    <div id="top-search" style="float: left">
                        <a href="#" id="top-search-trigger"><i class="icon-search3"></i>
                            {{--<i class="icon-line-cross"></i>--}}
                        </a>
                        <form action="/tim-kiem" method="get">
                            <input type="text" name="q" class="form-control" value=""
                                   placeholder="Tìm kiếm">
                        </form>
                    </div>

                    <div id="top-cart" style="float: left">
                        <a href="#" id="top-cart-trigger"><i
                                    class="icon-shopping-cart"></i><span> {{ count(Cart::content()) }}</span></a>
                        <div class="top-cart-content">
                            <div class="top-cart-title">
                                <h4>Giỏ hàng</h4>
                            </div>
                            @foreach($cart as $item)
                                <?php
                                $company_id = \Modules\ThemeSemicolonwebJdes\Models\Product::where('id', $item->id)->first()->company_id;
                                $company = \Modules\EworkingCompany\Models\Company::where('id', $company_id)->first();
                                ?>
                                <div class="top-cart-items">
                                    <div class="top-cart-item clearfix">
                                        <div class="top-cart-item-image">
                                            <a href="/{{ @$company->slug }}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct(\Modules\ThemeSemicolonwebJdes\Models\Product::find($item->id)) }}"><img
                                                        src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($item->options->image) }}"
                                                        alt="{{ $item->name }}"/></a>
                                        </div>
                                        <div class="top-cart-item-desc">
                                            <a href="/{{ @$company->slug }}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct(\Modules\ThemeSemicolonwebJdes\Models\Product::find($item->id)) }}">{{ $item->name }}</a>
                                            <span class="top-cart-item-price">{{ number_format($item->price,0,'',',') }}
                                                đ</span>
                                            <span class="top-cart-item-quantity">x {{ $item->qty }}</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="top-cart-action clearfix">
                                <span class="fleft top-checkout-price">{{$total}} đ </span>
                                <a href="/gio-hang" class="button button-3d button-small nomargin fright">Xem giỏ
                                    hàng</a>

                            </div>
                        </div>
                        @if(\Auth::guard('admin')->check())
                            <style>
                                .profile-users{
                                    position: relative;
                                    margin-top: -15px;
                                    margin-left: 16px;
                                    z-index: 999;

                                }

                                #icon-users{
                                    padding: 0px 12px;
                                    margin-bottom: 0;
                                    font-size: 14px;
                                    font-weight: 400;
                                    /*line-height: 80px;*/
                                    text-align: center;
                                    white-space: nowrap;
                                    vertical-align: middle;
                                    -ms-touch-action: manipulation;
                                    touch-action: manipulation;
                                    cursor: pointer;
                                    -webkit-user-select: none;
                                    -moz-user-select: none;
                                    -ms-user-select: none;
                                    user-select: none;
                                    background-image: none;
                                    border: 1px solid transparent;
                                    border-radius: 4px;
                                }
                                .profile-user{
                                    display: none;
                                }
                                .profile-users:hover .profile-user{
                                    position: absolute;
                                    display: block;
                                    right: 0;
                                    top: 100%;
                                    min-width: 160px;
                                    padding: 5px 0;
                                    font-size: 14px;
                                    text-align: left;
                                    list-style: none;
                                    background-color: #fff;
                                    -webkit-background-clip: padding-box;
                                    background-clip: padding-box;
                                    border: 1px solid rgba(0,0,0,.15);
                                    border-radius: 4px;
                                    -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
                                    box-shadow: 0 6px 12px rgba(0,0,0,.175);
                                }
                                #primary-menu .profile-users .profile-user>a{
                                    display: block;
                                    width: 100%!important;
                                    padding: 10px 10px!important;
                                    height: auto!important;
                                    clear: both!important;
                                    font-weight: 400!important;
                                    color: #212529!important;
                                    text-align: inherit!important;
                                    white-space: nowrap!important;
                                    background-color: transparent!important;
                                    border: 0!important;
                                }
                                #primary-menu .profile-users .profile-user>a:hover{
                                    color: #16181b!important;
                                    text-decoration: none!important;
                                    background-color: #f8f9fa!important;
                                }
                                .sticky-header #header-wrap #primary-menu .profile-users{
                                    line-height: 55px!important;
                                    margin-left: 10px;
                                    -webkit-transition: line-height .4s ease, opacity .3s ease;
                                    -o-transition: line-height .4s ease, opacity .3s ease;
                                    transition: line-height .4s ease, opacity .3s ease;
                                }
                                .full-header #header-wrap #primary-menu .profile-users{
                                    line-height: 95px;
                                    margin-left: 10px;
                                }

                                @media (max-width: 991px) {
                                    .full-header #header-wrap #primary-menu .profile-users{
                                        position: absolute;
                                        top: 0;
                                        right: 0;
                                    }
                                }
                            </style>
                            <div  class="profile-users" style="float: left">
                                <i class="icon-user show" id="icon-users"></i>
                                <div class="profile-user">
                                    <a href="/admin/profile">Profile</a>
                                    <a href="/admin">Vào trang quản trị</a>
                                    <a href="/admin/logout" title="">Đăng xuất</a>
                                </div>
                            </div>
                        @endif
                    </div>

            </nav>
        </div>
        <div class="container clearfix print">
            <h1>Custom header khi in hóa đơn ở đây nhé ^^ </h1>
        </div>
    </div>
</header>