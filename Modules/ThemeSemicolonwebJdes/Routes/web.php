<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//  Admin
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'theme'], function () {
        Route::match(['get', 'post'], 'setting', 'Admin\ThemeController@setting')->name('theme')->middleware('permission:theme');
    });
});


//  Frontend
Route::group(['prefix' => ''],function () {
    Route::get('', 'Frontend\HomeController@GetHome')->name('home');
});

Route::get('tro-giup', 'Frontend\HomeController@GetFaqs');
Route::get('widget', 'Frontend\FooterController@GetWidget');
Route::get('viewcart', 'Frontend\MenuController@GetViewCart');
Route::get('tim-kiem', 'Frontend\ProductController@GetSearch');
Route::get('search-bai-viet', 'Frontend\PostController@SearchPost');
Route::get('cong-ty', 'Frontend\ProductController@ListCompany');
Route::group(['prefix' => 'lien-he'],function () {
    Route::get('', 'Frontend\ContactController@GetContact');
    Route::post('', 'Frontend\ContactController@PostContact')->name('contact.post2');
});


Route::get('landing-4', 'Frontend\HomeController@GetLanding');
Route::get('maintenance', 'Frontend\HomeController@GetMaintenance');


Route::get('search', 'Frontend\HomeController@GetSearch');
Route::group(['prefix' => '', 'middleware' => \Modules\JdesCompany\Http\Middleware\GetCompany::class], function () {
    Route::get('{comSlug}/san-pham', 'Frontend\ProductController@listProductsOfCompany');

    Route::get('{comSlug}/khach-hang', 'Frontend\UserController@GetUser');

    Route::group(['prefix' => '{comSlug}/lien-he'],function () {
        Route::get('', 'Frontend\ContactController@GetContact2');
        Route::post('', 'Frontend\ContactController@PostContact2')->name('contact.post2');
    });
    Route::get('{comSlug}/gioi-thieu', 'Frontend\HomeController@GetAbout');

    Route::group(['prefix' => 'thanh-toan'],function () {
        Route::get('', 'Frontend\CheckoutController@getCkeckout');
        Route::post('', 'Frontend\CheckoutController@postCkeckout');
        Route::get('hoan-thanh', 'Frontend\CheckoutController@getComplete');
    });

    Route::group(['prefix' => 'gio-hang'],function () {
        Route::get('', 'Frontend\CartController@GetCart');
        Route::get('add/{id}', 'Frontend\CartController@AddCart');
        Route::get('del_item/{rowId}','Frontend\CartController@DelItem');
        Route::get('update_item/{rowId}/{qty}','Frontend\CartController@UpdateItem');
    });
    Route::get('{slug1}/{slug2}', 'Frontend\ProductController@twoParam');
    Route::get('{slug1}/{slug2}/{slug3}', 'Frontend\ProductController@threeParam');
    Route::get('{slug1}/{slug2}/{slug3}/{slug4}', 'Frontend\ProductController@fourParam');
});

Route::get('{slug1}', 'Frontend\ProductController@oneParam');