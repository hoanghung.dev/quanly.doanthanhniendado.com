<?php

namespace Modules\Translate\Http\Controllers;

use App\Http\Helpers\CommonHelper;
use App\Models\Setting;
use Cookie;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use View;
use Modules\Translate\Models\Translate;

class TranslateController extends CURDBaseController
{
    protected $module = [
        'code' => 'translate',
        'table_name' => 'translate',
        'label' => 'translate::admin.translate',
        'modal' => '\Modules\Translate\Models\Translate',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'translate::admin.image', 'sort' => true],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'translate::admin.language_name', 'sort' => true],
            ['name' => 'code', 'type' => 'text', 'label' => 'translate::admin.language_code'],
            ['name' => '#', 'type' => 'custom', 'td' => 'translate::td.language', 'label' => 'translate::admin.language_default'],
            ['name' => 'admin', 'type' => 'status', 'label' => 'translate::admin.active_admin', 'sort' => true],
            ['name' => 'frontend', 'type' => 'status', 'label' => 'translate::admin.active_frontend', 'sort' => true],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'label' => 'translate::admin.language_name', 'class' => 'col-md-12'],
                ['name' => 'code', 'type' => 'text', 'label' => 'translate::admin.language_code', 'class' => 'col-md-12'],
                ['name' => 'frontend', 'type' => 'checkbox', 'label' => 'translate::admin.active_frontend', 'class' => 'col-md-12'],
                ['name' => 'admin', 'type' => 'checkbox', 'label' => 'translate::admin.active_admin', 'class' => 'col-md-12'],
            ],
            'image' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'translate::admin.image', 'class' => ''],
            ],
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'translate::admin.language_name',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'code' => [
            'label' => 'translate::admin.language_code',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'frontend' => [
            'label' => 'translate::admin.active_frontend',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'translate::admin.status',
                0 => 'translate::admin.no_active',
                1 => 'translate::admin.active'
            ]
        ],
        'admin' => [
            'label' => 'translate::admin.active_admin',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'translate::admin.status',
                0 => 'translate::admin.no_active',
                1 => 'translate::admin.active'
            ]
        ],
        'status' => [
            'label' => 'translate::admin.status',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'translate::admin.status',
                0 => 'translate::admin.no_active',
                1 => 'translate::admin.active'
            ]
        ],
    ];

    public function getIndex(Request $request)
    {

        $data = $this->getDataList($request);
        return view('translate::list')->with($data);
    }

    public function default(Request $request)
    {
        $item = Setting::where('name', 'default_language')->first();
        if (!is_object($item)) {
            $item = new Setting();
            $item->name = 'default_language';
            $item->type = 'general_tab';
        }
        $item->value = $request->language;
        $item->save();
        CommonHelper::one_time_message('success', 'Gán ngôn ngữ mặc định thành công !');
        return back();
    }


    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('translate::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('translate::edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên gắn gọn',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//               
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

//    protected $list_language = [
//        'vi' => [
//            'code' => 'vi',
//            'name' => 'Việt Nam',
//            'image' => './public/frontend/themes/handyman-services/images/vietnam.png',
//        ],
//        'en' => [
//            'code' => 'en',
//            'name' => 'Anh',
//            'image' => './public/frontend/themes/handyman-services/images/england.png',
//        ],
//    ];

    public function list_language()
    {
        $query = Translate::select('name', 'code', 'image')->where('status', 1);
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            $query = $query->where('admin', 1);
        } else {
            $query = $query->where('frontend', 1);
        }

        $languages = $query->get();
        $list_language = null;
        foreach ($languages as $key => $language) {
            $list_language[$language->code] = [
                'name' => $language->name,
                'code' => $language->code,
                'image' => $language->image,
            ];
        }
        return $list_language;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function set_session(Request $request)
    {
        if ($request->has('language')) {
            setcookie('language', $request->language, time() + (86400 * 365), "/"); // 86400 = 1 day
//            \App::setLocale($code);
            config(['app.locale' => $request->language]);
            if ($request->ajax()) {
                return Response()->json(
                    ['status' => true]
                );
            }
//            \App\Http\Helpers\CommonHelper::one_time_message('error', $code . ' và ' . app()->getLocale());
            return back();
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function getHtmlLanguage()
    {
        $list_language = $this->list_language();

        $render_html = '';
        $item = Setting::where('name', 'default_language')->first();
        $lang = (is_object($item)) ? $item->value : 'en';
        $present_language = !isset($_COOKIE['language']) ? $lang : $_COOKIE['language'];
        View::share('language', $present_language);
        $array_present_language = '';
        if (!is_null($list_language)) {
            foreach ($list_language as $key => $language) {
                if ($key == $present_language) {
                    $array_present_language = $language;
                } else {
                    $render_html .= '<li class="kt-nav__item kt-nav__item">
                    <a href="' . URL::to('/admin/translate/change') . '?language=' . $key . '" class="kt-nav__link">
                        <span class="kt-nav__link-icon"><img src="' . CommonHelper::getUrlImageThumb($language['image'], 'false', 'false') . '" alt="' . $language['name'] . '"></span>
                    </a>
                </li>';
                }
            }
        }

        if ($array_present_language == '') {
            $array_present_language = [
                'name' => 'Viêt Nam',
                'code' => 'vi',
                'image' => 'vietnam.jpg',
            ];
        }
        return [
            'list' => $render_html,
            'present' => $array_present_language
        ];
    }

}
