<?php

namespace Modules\Translate\Models;

use Illuminate\Database\Eloquent\Model;

class Translate extends Model
{

    protected $table = 'translate';

    protected $guarded = [];
    public $timestamps = false;
}
