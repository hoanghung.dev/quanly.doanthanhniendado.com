@if(in_array('view_all_data', $permissions))
    <li class="kt-menu__item" aria-haspopup="true"><a href="/admin/translate" class="kt-menu__link "><span class="kt-menu__link-icon"><i class="flaticon2-cube-1"></i>
            </span><span class="kt-menu__link-text">{{trans('translate::admin.translate')}}</span></a></li>
@endif