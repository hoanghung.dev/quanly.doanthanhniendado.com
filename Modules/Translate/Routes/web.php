<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions','locale']], function () {
    Route::group(['prefix' => 'translate'], function () {
        Route::get('', 'TranslateController@getIndex')->middleware('permission:super_admin');
        Route::get('change', 'TranslateController@set_session');
        Route::get('default', 'TranslateController@default')->middleware('permission:super_admin');
        Route::match(array('GET', 'POST'), 'add', 'TranslateController@add')->middleware('permission:super_admin');
        Route::get('{id}', 'TranslateController@update');
        Route::post('{id}', 'TranslateController@update')->middleware('permission:super_admin');
    });
});


