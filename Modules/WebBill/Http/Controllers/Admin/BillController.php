<?php

namespace Modules\WebBill\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\WebBill\Models\Bill;
use Modules\WebBill\Models\History_Bill;
use Modules\WebService\Models\Service;
use Validator;

class BillController extends CURDBaseController
{

    protected $orderByRaw = 'id DESC';

    protected $module = [
        'code' => 'bill',
        'table_name' => 'bills',
        'label' => 'Đơn hàng ',
        'modal' => '\Modules\WebBill\Models\Bill',
        'list' => [
            ['name' => 'user_id', 'type' => 'relation_edit',  'label' => 'Khách hàng', 'object' => 'user', 'display_field' => 'name'],
            ['name' => 'service_id', 'type' => 'relation', 'label' => 'Dịch vụ', 'object' => 'service', 'display_field' => 'name_vi'],
//            ['name' => 'count_product', 'type' => 'custom', 'td' => 'webbill::list.td.count_product', 'label' => 'Tổng SP'],
            ['name' => 'total_price', 'type' => 'price_vi', 'label' => 'Giá'],
            ['name' => 'exp_price', 'type' => 'price_vi', 'label' => 'Giá gia hạn'],
            ['name' => 'domain', 'type' => 'text', 'label' => 'Tên miền'],
            ['name' => 'auto_extend', 'type' => 'custom','td' => 'webbill::list.td.status', 'label' => 'Tự động gia hạn', 'options' => [
                0 => 'Không kích hoạt',
                1 => 'Kích hoạt',
            ],
            ],
            ['name' => 'status', 'type' => 'custom','td' => 'webbill::list.td.status', 'label' => 'Trạng thái', 'options' => [
                0 => 'Không kích hoạt',
                1 => 'Kích hoạt',
            ],
            ],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'user_id', 'type' => 'select2_model', 'class' => 'required', 'label' => 'Tên Khách hàng', 'model' => \App\Models\User::class, 'display_field' => 'name', 'display_field2' => 'tel'],
                ['name' => 'service_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Dịch vụ', 'model' => Service::class, 'display_field' => 'name_vi',],
                ['name' => 'domain', 'type' => 'text', 'label' => 'Tên miền'],
                ['name' => 'total_price', 'type' => 'text', 'class' => 'number_price', 'label' => 'Giá'],
                ['name' => 'created_at', 'type' => 'date', 'label' => 'Ngày bắt đầu'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'Kích hoạt','value' => 1, 'group_class' => 'col-md-6'],
            ],

            'info_tab' => [
                ['name' => 'date', 'type' => 'date', 'label' => 'Ngày hết hạn'],
                ['name' => 'exp_price', 'type' => 'text', 'class' => 'number_price', 'label' => 'Giá gia hạn'],
                ['name' => 'auto_extend', 'type' => 'checkbox', 'label' => 'Tự động gia hạn','value' => 1, 'group_class' => 'col-md-6'],
            ],
//            'histories_bill_tab' => [
//                ['name' => 'account_max', 'type' => 'number', 'label' => 'Số thành viên tôi đa', 'inner' => 'disabled', 'group_class' => 'col-md-6'],
//                ['name' => 'exp_date', 'type' => 'datetimepicker', 'label' => 'Ngày hết hạn', 'inner' => 'disabled',
//                    'date_format' => 'd-m-Y', 'group_class' => 'col-md-6'],
//            ],
        ],
    ];

    protected $filter = [
        'user_id' => [
            'label' => 'Tên khách hàng',
            'type' => 'select2_model',
            'display_field' => 'name',
            'model' => \App\Models\User::class,
            'query_type' => '='
        ],
        'name_vi' => [
            'label' => 'Dịch vụ',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'total_price' => [
            'label' => 'Tổng tiền',
            'type' => 'number',
            'query_type' => 'like'
        ],
        'exp_price' => [
            'label' => 'Giá gia hạn',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'domain' => [
            'label' => 'Tên miền',
            'type' => 'text',
            'query_type' => 'like'
        ],
//        'date' => [
//            'label' => 'Ngày nhận',
//            'type' => 'date',
//            'query_type' => 'custom'
//        ],
        'auto_extend' => [
            'label' => 'Tự động kích hoạt',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                0 => 'Không kích hoạt',
                1 => 'Kích hoạt',
            ],
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Vị trí',
                0 => 'Không kích hoạt',
                1 => 'Kích hoạt',
            ],
        ],

    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('webbill::list')->with($data);
    }

    /*public function appendWhere($query, $request)
    {
        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
        }

        return $query;
    }*/

    public function add(Request $request)
    {
        /*$history_bill=new History_Bill();
        $history_bill->bill_id=$request->id;
        $history_bill->price=$request->exp_price;
        $history_bill->date=$request->date;
        $history_bill->save();*/

//        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('webbill::add')->with($data);
            }
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;
//                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }

//        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', $ex->getMessage());
//            return redirect()->back()->withInput();
//        }


    public function update(Request $request)
    {

        try {

            $date_db = Bill::find($request->id)->date;
            $price_db=Bill::find($request->id)->exp_price;
            $item = $this->model->find($request->id);


            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('webbill::edit')->with($data);
            }
            if ($date_db != $request->date) {
                $history_bill = new History_Bill();
                $history_bill->bill_id=$request->id;
                $history_bill->price=$price_db;
                $history_bill->date=$date_db;

                $history_bill->save();
            }
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);

        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return redirect()->back()->withInput();
        }
    }


    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

//            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
    public function del(Request $request)
    {
        try {


            $del = History_Bill::find($request->id);
//            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }
            $del->delete();
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return back();
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

}
