<?php
namespace Modules\WebBill\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\EworkingCompany\Models\Company;
use App\Models\User;
use Modules\WebService\Models\Service;
use Modules\JdesOrder\Models\Order;

class Bill extends Model
{

    protected $table = 'bills';

    protected $fillable = [
        'service_id','receipt_method' , 'user_gender', 'date' , 'coupon_code' , 'note' , 'status' , 'total_price' , 'user_id', 'service_id', 'user_tel', 'user_name', 'user_email', 'user_address', 'user_wards', 'user_city_id'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function orders() {
        return $this->hasMany(Order::class, 'order_id', 'id');
    }

    public function company() {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function service() {
        return $this->belongsTo(Service::class, 'service_id');
    }
}
