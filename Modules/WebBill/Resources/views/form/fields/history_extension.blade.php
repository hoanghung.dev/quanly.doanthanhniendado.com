<?php
$bill_histories=\Modules\WebBill\Models\History_Bill::where('bill_id',$result->id)->orderBy('id','DESC')->take(5)->get();
?>
<div class="kt-portlet kt-portlet--height-fluid">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Lịch sử gia hạn
            </h3>
        </div>
    </div>

    <div class="kt-portlet__body">
        <div class="kt-widget6">
            <div class="kt-widget6__head">
                <div class="kt-widget6__item">
                    <span>Ngày hết hạn</span>
                    <span style="padding-left: 105px;">Giá gia hạn</span>
                    <span>Hành động</span>
                </div>
            </div>
            @foreach($bill_histories as $bill_history)
            <div class="kt-widget6__body">
                <div class="kt-widget6__item">
                    <span>{{date('d-m-Y',strtotime($bill_history->date))}}</span>
                    <span>{{number_format(@$bill_history->price, 0, '.', '.')}}đ</span>
                    <a href="/admin/del?id={{$bill_history->id}}"><span>Xóa</span></a>
                </div>
            </div>
            @endforeach
        </div>
    </div>


</div>


