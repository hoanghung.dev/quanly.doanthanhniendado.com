<?php

namespace Modules\WebService\Http\Controllers\Admin;

use App\Mail\MailServer;
use App\Models\Setting;
use Auth;
use Illuminate\Routing\Controller;
use Mail;
use URL;

class MailController extends Controller
{
    protected $_mailSetting;

    public function __construct()
    {
        $this->_mailSetting = Setting::whereIn('type', ['mail'])->pluck('value', 'name')->toArray();
    }

    /**
     * Gửi mail khi khách đăng ký gói dịch vụ mới
     * $serviceHistory: gói vừa đăng ký
     */
    public function registerServiceSendMail($serviceHistory)
    {

        $user = (object)[
            'email' => $serviceHistory['email'],
            'name' => $serviceHistory['name'],
            'service_name' => $serviceHistory['service_name'],
            'account_max' => $serviceHistory['account_max'],
            'use_date_max' => $serviceHistory['use_date_max'],
            'payment' => $serviceHistory['payment'],
            'deadline' => $serviceHistory['deadline'],
            'service_history_id' => $serviceHistory['service_history_id'],
        ];
        $data = [
            'view' => 'webservice::emails.service_email',
            'user' => $user,

            'name' => $this->_mailSetting['mail_name'],
            'subject' => 'Thư Xác nhận và thanh toán mua gói dịch vụ'
        ];
        Mail::to($user)->send(new MailServer($data));
        return true;
    }

    /**
     * Gửi mail khi nhân viên được mời chấp nhận, mà cty đã đủ người
     * $info: thông tin nhân viên
     */
    public function SendMailFullSlot($info)
    {

        $user = (object)[
            'email' => $info['email'],
            'name' => $info['name'],
        ];
        $data = [
            'view' => 'test',
            'user' => $user,

            'name' => $this->_mailSetting['mail_name'],
            'subject' => 'Thư phản hồi của ' . $info['name']
        ];
        Mail::to($user)->send(new MailServer($data));
        return true;
    }
}
