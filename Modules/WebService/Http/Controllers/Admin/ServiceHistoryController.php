<?php

namespace Modules\WebService\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use App\Models\Setting;
use Auth;
use Illuminate\Http\Request;
use Mail;
use Modules\EworkingCompany\Models\Company;
use Modules\WebService\Models\{Service, ServiceHistory};
use Validator;

class ServiceHistoryController extends CURDBaseController
{

    protected $module = [
        'code' => 'service_history',
        'table_name' => 'service_history',
        'label' => 'Lịch sử',
        'modal' => '\Modules\WebService\Models\ServiceHistory',
        'list' => [
//            ['name' => 'service_id', 'type' => 'relation_edit', 'label' => 'Tên Gói', 'object' => 'service', 'display_field' => 'name_vi'],
            ['name' => 'service_id', 'type' => 'custom', 'td' => 'webservice::list.service_name', 'label' => 'Tên Gói', 'object' => 'service', 'display_field' => 'name_vi'],
            ['name' => 'payment', 'type' => 'price_vi', 'label' => 'Số tiền'],
            ['name' => 'service_id', 'type' => 'custom', 'td' => 'webservice::list.service_number', 'label' => 'Số tài khoản', 'object' => 'service', 'display_field' => 'account_max'],
            ['name' => 'start_date', 'type' => 'date_vi', 'label' => 'Ngày mua'],
            ['name' => 'exp_date', 'type' => 'date_vi', 'label' => 'Ngày hết hạn'],
            ['name' => 'status', 'type' => 'custom', 'td' => 'webservice::list.status', 'label' => 'Kích hoạt'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'service_id', 'type' => 'custom', 'field' => 'webservice::form.fields.select_service', 'class' => 'required', 'label' => 'Đăng ký gói', 'model' => \Modules\WebService\Models\Service::class, 'display_field' => 'name_vi', 'display_field2' => 'account_max'],
                ['name' => 'use_date_max', 'type' => 'custom', 'field' => 'webservice::form.fields.select_service_day', 'class' => 'required', 'label' => 'Thời gian sử dụng', 'model' => \Modules\WebService\Models\Service::class,
                    'display_field' => 'value'],
            ],
        ],
    ];

    protected $filter = [

        'service_id' => [
            'label' => 'Gói dịch vụ',
            'type' => 'select2_model',
            'display_field' => 'name_vi',
            'model' => \Modules\WebService\Models\Service::class,
            'query_type' => '='
        ],
        'payment' => [
            'label' => 'Giá',
            'type' => 'number',
            'query_type' => '='
        ],
        'start_date' => [
            'label' => 'Ngày mua',
            'type' => 'date',
            'query_type' => '='
        ],
        'exp_date' => [
            'label' => 'Ngày hết hạn',
            'type' => 'date',
            'query_type' => '='
        ],
    ];


    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('webservice::service_history.list')->with($data);
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('webservice::service_history.add')->with($data);
            } else if ($_POST) {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                $service_tab_payment_before_date = @Setting::where('name', 'payment_before_date')->first()->value;
                if (empty($service_tab_payment_before_date)) {
                    $service_tab_payment_before_date = 3;
                }
//                  Gói cpi đăng kí mới
                $service = @Service::find($request->service_id);
//                  Gói đang dùng
                $service_current = @Service::find(@Company::find(\Auth::guard('admin')->user()->last_company_id)->service_id);

                if (@$service->account_max <= @$service_current->account_max) {
//                      Gia hạn
                    $service_type = 1;
                } else {
//                      Nâng cấp
                    $service_type = 2;
                }
                //  Tùy chỉnh dữ liệu insert
                $serviceController = new ServiceController();
                $data['payment'] = $serviceController->resolePrice(Service::find($request->service_id), $request->service_id, $request->use_date_max, $service_type)['price'];
                $data['start_date'] = date('Y-m-d');
                $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                $data['exp_date'] = strftime("%Y-%m-%d", strtotime(date("d-m-Y", time()) . '+' . $request->use_date_max . ' day'));
                $data['status'] = $request->has('status') ? 1 : 0;
                $data['account_max'] = @$service->account_max;

                foreach ($data as $k => $v) {
                    $this->model->$k = $v;
                }
                if ($data['status'] == 1) {
                    $this->activeServiceHistory($this->model);
                }

                if ($this->model->save()) {
                    try {
                        \Eventy::action('service_history.add', [
                            'name' => @\Auth::guard('admin')->user()->name,
                            'email' => @\Auth::guard('admin')->user()->email,
                            'service_name' => @Service::find($request->service_id)->name_vi,
                            'account_max' => @Service::find($request->service_id)->account_max,
                            'use_date_max' => $data['use_date_max'],
                            'payment' => $data['payment'],
                            'deadline' => strftime("%d-%m-%Y", strtotime(date("d-m-Y", time()) . '+' . $service_tab_payment_before_date . ' day')),
                            'service_history_id' => $this->model->id
                        ]);
                    } catch (\Exception $ex) {
                    }
                    $this->afterAddLog($request, $this->model);

                    CommonHelper::one_time_message('success', 'Đăng ký gói thành công vui lòng kiểm tra email!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                }

                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $this->model
                    ]);
                }
//                    return $this->addReturn($request);
                return redirect('admin/' . $this->module['code']);
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
                return back();
            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('webservice::edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    if ($request->has('status') && $request->status == 1 && $item->status == 0) {
                        $this->activeServiceHistory($item);
                    }

                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    #

                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {

                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    return $this->updateReturn($request, $item);
                }
            }
        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {
            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            if (!is_object($item)) {
                CommonHelper::one_time_message('error', 'Không tìm thấy bản ghi');
                return back();
            }

            //Kích hoạt gói dịch vụ mới đăng ký
            if ($item->status == 0) {
                $this->activeServiceHistory($item);
            } else {
                CommonHelper::one_time_message('error', 'Hóa đơn này đã được kích hoạt');
                return back();
            }

            CommonHelper::one_time_message('success', 'Kích hoạt gói dịch vụ thành công!');
            return back();
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    /*
     * Cài gói dùng thử
     * $company : đối tượng công ty muốn cài
     * */
    public function registerTrialService($company)
    {
        try {
            $serviceTrialId = @Setting::where('name', 'service_trial_id')->first()->value;

            if ($serviceTrialId == null) {
                return [
                    'status' => false,
                    'msg' => 'Chưa cấu hình gói dùng thử'
                ];
            }
            $serviceTrial = Service::find($serviceTrialId);
            $dayPrice = json_decode($serviceTrial->price);
            $serviceHistory = ServiceHistory::create([
                'service_id' => $serviceTrialId,
                'company_id' => $company->id,
                'use_date_max' => $dayPrice[0]->day,
                'account_max' => $serviceTrial->account_max,
                'payment' => 0,
                'start_date' => date('Y/m/d'),
                'exp_date' => strftime("%Y-%m-%d", strtotime(date("d-m-Y", time()) . '+' . $dayPrice[0]->day . ' day')),
                'status' => 0,
                'admin_id' => @\Auth::guard('admin')->user()->id,
                'service_type' => 1,
            ]);

            $this->activeServiceHistory($serviceHistory);
            return [
                'status' => true,
                'msg' => 'Đã kích hoạt gói dùng thử thành công'
            ];
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * @param $service_history
     * @return bool
     * Hàm sử lý khi click vào nút kích hoạt  gói dịch vụ
     * $service_history: bản ghi kích hoạt
     */
    public function activeServiceHistory($service_history)
    {
        try {
            //  Cập nhật gói đang kích hoạt về dạng đã sử dụng
            ServiceHistory::where('status', 1)->where('company_id', $service_history->company_id)
                ->where('id', '!=', $service_history->id)->update([
                    'status' => -1
                ]);

            //  Cập nhật gói chờ kích hoạt khác về dạng hủy
            ServiceHistory::where('status', 0)->where('company_id', $service_history->company_id)
                ->where('id', '!=', $service_history->id)->update([
                    'status' => -2
                ]);

            //  Cật nhập gói mới cho công ty
            $company = Company::find(@$service_history->company_id);

            $company->exp_date = @$service_history->exp_date;
            $company->account_max = @$service_history->account_max;
            $company->service_id = @$service_history->service_id;

            $company->save();

            //  Cập nhật trạng thái gói thành đang kích hoạt
            $service_history->status = 1;
            $service_history->save();
        } catch (\Exception $ex) {
            dd($ex->getMessage());
        }

        return true;
    }

    public function linkActiveServiceHistory(Request $request)
    {
        $serviceHistoryId = $request->id;

        $serviceHistory = ServiceHistory::find($serviceHistoryId);
        if (!is_object($serviceHistory)) {
            CommonHelper::one_time_message('error', 'Không tìm thấy bản ghi!');
            return back();
        }

        if ($this->activeServiceHistory($serviceHistory)) {
            CommonHelper::one_time_message('success', 'Kích hoạt thành công!');
            return back();
        }

        CommonHelper::one_time_message('success', 'Kích hoạt thất bại!');
        return back();
    }

    public function ajaxActiveServiceHistory(Request $request)
    {
        $serviceHistoryId = $request->id;

        $serviceHistory = ServiceHistory::find($serviceHistoryId);
        if (!is_object($serviceHistory)) {
            return response()->json([
                'status' => false,
                'msg' => 'Không tìm thấy bản ghi'
            ]);
        }

        if ($this->activeServiceHistory($serviceHistory)) {
            return response()->json([
                'status' => true,
                'msg' => 'Kích hoạt thành công!'
            ]);
        }


        return response()->json([
            'status' => false,
            'msg' => 'Kích hoạt thất bại!'
        ]);
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
                return back();
            }

            $item->delete();
            CommonHelper::one_time_message('success', 'Xóa thành công!');
//            dd(1);
            return back();
//            return $this->deleteReturn($request);
        } catch (\Exception $ex) {
//            dd(2);
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công!'
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }


    public function appendWhere($query, $request)
    {
        if (@$request->view == 'all' && CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'super_admin')) {
            $query = $query
                ->orderByRaw('CASE status
                                  WHEN 0 THEN 1
                                  WHEN 1 THEN 2
                                  WHEN -1 THEN 3
                                  ELSE 4
                              END');
        } else {
            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
        }
        return $query;
    }

//    public function appendData($request, $data, $item = false)
//    {
//
//        $data['start_date']=date('Y-m-d');
//        $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
//        $data['exp_date'] = strftime("%Y-%m-%d", strtotime(date("d-m-Y", strtotime($data['start_date'])) . '+' . $data['use_date_max'] . ' day'));
//        $data['payment'] = $this->resolePrice(\Auth::guard('admin')->user()->last_company_id, $request->service_id,$request->price);
//        unset($data['note_service_history']);
//        unset($data['money']);
//        $data['status'] = 0;
//
//        return $data;
//    }

    public function getServiceHistory(Request $request)
    {
        $attr = Service::find($request->id);
//        dd(json_decode($attr->time_price)[0]->price);
        $time_price = json_decode($attr->time_price);
        $html = '';
        foreach (json_decode($attr->time_price) as $val) {
            $html = $html . '<option  value="' . $val->use_date_max . '"  data-value="' . $val->price . '" data-price="' . number_format($val->price, 0, ',', '.') . '">' . $val->use_date_max . ' Ngày </option>';
        }

        //        gói mới
        $service_now = service::find($request->id);
//      Gói đang dùng
        $service_ids = Company::find(\Auth::guard('admin')->user()->last_company_id)->service_id;
        $service = service::find($service_ids);
        $note = '';
        if ($service_now->account_max > $service->account_max) {
            $note = 'Tiền thanh toán = Tiền gói đăng kí mới - (Tiền cao nhất của gói đang dùng / Số ngày cao nhất của gói đang dùng)';
        }

        return response()->json([
            'html' => $html,
            'first_price' => number_format($time_price[0]->price, 0, ',', '.'),
            'payment' => number_format($this->resolePrice(\Auth::guard('admin')->user()->last_company_id, $request->id, $request->price), 0, ',', '.'),
            'note' => $note
        ]);
    }


    public function resolePrice($company_id, $service_id, $prices = false)
    {
        $attr = Service::find($service_id);
        $time_price = json_decode($attr->time_price);
//        gói mới
        $service_now = service::find($service_id);
//      Gói đang dùng
        $service_ids = Company::find($company_id)->service_id;
        $service = service::find($service_ids);

        $time_price_now = json_decode($service->time_price);

        if ($service_now->account_max <= $service->account_max) {
            $payment = (isset($prices)) ? $prices : $time_price[0]->price;
        } else {
            $day = Company::select('exp_date')->find($company_id);
            $exp_date = strtotime($day->exp_date);
            $today = strtotime(date('Y-m-d'));
            $date_diff = abs($exp_date - $today);
            $day_abs = floor($date_diff / (60 * 60 * 24));

            $max_use_date_max = 0;
            $max_price = 0;
            foreach ($time_price_now as $val) {
                if ($val->use_date_max > $max_use_date_max) {
                    $max_use_date_max = $val->use_date_max;
                }
                if ($val->price > $max_price) {
                    $max_price = $val->price;
                }
            }
            $time_price[0]->price = (isset($prices)) ? $prices : $time_price[0]->price;
            $payment = $time_price[0]->price - $day_abs * $max_price / $max_use_date_max;

        }
        return $payment;
    }

    public function getServiceHistoryNotification()
    {
        return view('admin.service_history_notification');
    }

}
