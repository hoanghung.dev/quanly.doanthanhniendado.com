@if(in_array('service_history_publish', $permissions))
    @if($item->{$field['name']} == 1)
        <a href="{{URL::to('/admin/service_history/publish?id='.$item->id.'&column=status')}}"
           class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill"

           style="cursor:pointer;" data-column="{{ $field['name'] }}">Đang kích hoạt</a>
    @elseif($item->{$field['name']} == 0)
        <a href="{{URL::to('/admin/service_history/publish?id='.$item->id.'&column=status')}}"
           class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill"

           style="cursor:pointer;" data-column="{{ $field['name'] }}">Chờ kích hoạt</a>
    @elseif($item->{$field['name']} == -1)
        <a href="{{URL::to('/admin/service_history/publish?id='.$item->id.'&column=status')}}"
           class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill"
           style="cursor:pointer;" data-column="{{ $field['name'] }}">Đã dùng</a>
    @elseif($item->{$field['name']} == -2)
        <a href="{{URL::to('/admin/service_history/publish?id='.$item->id.'&column=status')}}"
           class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill"
           style="cursor:pointer;" data-column="{{ $field['name'] }}">Hủy</a>
    @endif
@else
    @if($item->{$field['name']} == 1)
        <span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill"

           style="cursor:pointer;" data-column="{{ $field['name'] }}">Đang kích hoạt</span>
    @elseif($item->{$field['name']} == 0)
        <span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill"
           style="cursor:pointer;" data-column="{{ $field['name'] }}">Chờ kích hoạt</span>
    @elseif($item->{$field['name']} == -1)
        <span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill"
           style="cursor:pointer;" data-column="{{ $field['name'] }}">Đã dùng</span>
    @elseif($item->{$field['name']} == -2)
        <span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill"
           style="cursor:pointer;" data-column="{{ $field['name'] }}">Hủy</span>
    @endif
@endif


