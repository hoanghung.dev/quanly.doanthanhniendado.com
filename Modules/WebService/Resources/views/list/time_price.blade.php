<?php
$data = json_decode($item->price);
?>
@if(is_array($data))
    @foreach($data as $v)
        {{ @$v->day }} ngày - {{ number_format((int)$v->price, 0, '.', '.') }}<sup>đ</sup><br>
    @endforeach
@endif