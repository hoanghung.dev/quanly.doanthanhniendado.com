<?php
//dd($permissions);
?>
@if(!in_array('super_admin', $permissions))
    <?php
    if (!isset($company_choosing)) {
        $company_choosing = \Modules\EworkingCompany\Models\Company::find(\Auth::guard('admin')->user()->last_company_id);
        \View::share('company_choosing', $company_choosing);

    }
    $end_date = '1970-01-01';
    if (!empty(\Auth::guard('admin')->user()->last_company_id) && !empty($company_choosing)) {
        $end_date = @\Modules\EworkingCompany\Models\Company::find($company_choosing->id)->exp_date;

    }


    ?>
    @if(date('Y-m-d') > $end_date)

        @if(!Request::is('admin/service_history/add') && !Request::is('admin/dashboard') && !Request::is('admin/dashboard/company')
         && !Request::is('admin/service/show') && !Request::is('admin/service_history'))

            <div style="position: absolute;
                            background: #000;
                            width: 86%;
                            height: 100%;
                            opacity: 0.6;
                            z-index: 1;
                            right: 0; ">

                <div style="font-size: 20px;
                                color: #fff;
                                width: 86%;
                                position: fixed;
                                top: 250px;
                                margin: auto;
                                text-align: center;">
                    @if(!empty($company_choosing))
                        <b>Bạn đã hết hạn sử dụng gói dịch vụ! Vui lòng đăng ký gói dịch vụ mới để sử dụng!</b><br>

                        <a href="/admin/service_history/add"
                           style="
                                   border-radius: 5px;
                                   color: #fff;
                                   padding: 10px 20px;
                                   background: #469408;
                                   display: inline-block;">Đăng ký gói dịch vụ</a>
                    @else
                        <b>Công ty bạn đang chọn không tồn tại! Vui lòng tạo công ty mới hoặc chọn công ty khác</b><br>

                        <a href="/create-company"
                           style="
                                   border-radius: 5px;
                                   color: #fff;
                                   padding: 10px 20px;
                                   background: #469408;
                                   display: inline-block;">Tạo công ty</a>
                    @endif
                </div>
            </div>
        @endif

    @endif
@endif
