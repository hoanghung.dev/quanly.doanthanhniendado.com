const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/assets/js/app.js', 'js/webservice.js')
    .sass( __dirname + '/Resources/assets/sass/app.scss', 'css/webservice.css');

if (mix.inProduction()) {
    mix.version();
}