<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Baithi;
use Socialite;
use Illuminate\Http\Request;
use Auth;

class CauHoiController extends Controller
{
    public function displayCauHoi() {		
		$data = Baithi::with('cau_hoi')->get()->all();	
		return view('frontend.bai-thi.list',['cauhoi'=>($data)]);
    }
}
