<?php

namespace App\Http\Middleware;

use App\Http\Helpers\CommonHelper;
use Closure;
use Auth;

class Guest
{
    public function handle($request, Closure $next, $guard = null)
    {
        if ($guard == 'student') {
            if (!Auth::guard($guard)->check()) {
                return \Redirect::guest('/')->with(['openLogin' => true]);
            }
            if (@Auth::user()->status == -1) {
                CommonHelper::one_time_message('error', 'Tài khoản của bạn bị khóa');
                return redirect()->back();
            }
        } else if ($guard == 'admin') {
            if (!Auth::guard($guard)->check()) {
                return \Redirect::guest('admin/login');
            }
        }
        return $next($request);
    }
}
