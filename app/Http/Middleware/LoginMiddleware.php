<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Input;
use Auth;

class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            return $next($request);
        } else {
            if ($request->path() == 'admin') {
                return redirect('admin/login');
            } else {
                return redirect()->route('login')->with('flash_message', 'Đăng nhập thất bại!');
            }
        }
    }
}
