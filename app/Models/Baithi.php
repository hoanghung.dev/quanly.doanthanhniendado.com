<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Cauhoi;

class BaiThi extends Model
{
    protected $table = 'bai_thi';
    protected $guard = [];

    public function cau_hoi()
    {
        return $this->hasMany('App\Models\Cauhoi', 'bai_thi_id');
    }
}

