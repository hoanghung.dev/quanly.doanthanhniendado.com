<input type="number" name="{{ @$field['name'] }}" class="form-control {{ @$field['class'] }}"
       {{ strpos(@$field['class'], 'require') !== false ? 'required' : '' }}
       id="{{ $field['name'] }}" {!! @$field['inner'] !!}
       value="{{ old($field['name']) != null ? old($field['name']) : @$field['value'] }}"
       placeholder="{{ @$field['label'] }}"
       >