<img
        data-id="{{ $item->id }}" class="{{$field['name']}}"
        style="cursor:pointer;"
        src="@if($item->{$field['name']}==1){{ '/public/images_core/icons/published.png' }}@else{{ '/public/images_core/icons/unpublish.png' }}@endif">