<!DOCTYPE html>
<html lang="en-US">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <title>Đấu thầu GXD</title>
  <meta name="generator" content="VuePress 1.9.5">
  <link rel="icon" href="/favicon.ico">
  <link rel="icon" href="/logo.png">
  <link rel="manifest" href="/manifest.json">
  <link rel="apple-touch-icon" href="/icons/apple-touch-icon-152x152.png">
  <link rel="mask-icon" href="/icons/safari-pinned-tab.svg" color="#3eaf7c">
  <meta name="description" content="Hướng dẫn sử dụng phần mềm QLDA GXD vào quản lý dự án xây dựng">
  <meta name="theme-color" content="#3eaf7c">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/icons/ms-icon-144x144.png">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.7.14/vue.min.js" integrity="sha512-BAMfk70VjqBkBIyo9UTRLl3TBJ3M0c6uyy2VMUrq370bWs7kchLNN9j1WiJQus9JAJVqcriIUX859JOm12LWtw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
  <script>
    var cauhoi = @json($cauhoi);
    var danh_gia_dap_an_dung = @json($danh_gia_dap_an_dung);
    var danh_gia_dap_an_sai = @json($danh_gia_dap_an_sai);
  </script>
</head>

<body>
  <div id="app">
    <div class="theme-container" :class="{'sidebar-open':activeMenu}" @click="closeMenu()">
      <header class="navbar">
        <div class="sidebar-button" @click="openMenu()"><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" role="img" viewBox="0 0 448 512" class="icon">
            <path fill="currentColor" d="M436 124H12c-6.627 0-12-5.373-12-12V80c0-6.627 5.373-12 12-12h424c6.627 0 12 5.373 12 12v32c0 6.627-5.373 12-12 12zm0 160H12c-6.627 0-12-5.373-12-12v-32c0-6.627 5.373-12 12-12h424c6.627 0 12 5.373 12 12v32c0 6.627-5.373 12-12 12zm0 160H12c-6.627 0-12-5.373-12-12v-32c0-6.627 5.373-12 12-12h424c6.627 0 12 5.373 12 12v32c0 6.627-5.373 12-12 12z"></path>
          </svg></div> <a href="/" class="home-link router-link-active"><!----> <span class="site-name">Đấu thầu GXD</span></a>
        <div class="links" style="max-width: 1179px;">
          <form id="search-form" role="search" class="meilisearch-search-wrapper search-box">
            <div class="docs-searchbar-js" data-ds-theme="light"><span class="meilisearch-autocomplete" style="position: relative; display: inline-block; direction: ltr;">
            <!-- <input id="meilisearch-search-input" class="search-query dsb-input" placeholder="" autocomplete="off" spellcheck="false" role="combobox" aria-autocomplete="list" aria-expanded="false" aria-label="search input" aria-owns="meilisearch-autocomplete-listbox-0" dir="auto" style="position: relative; vertical-align: top;"> -->
                <pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: Arial; font-size: 14.4px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: normal; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre><span class="dsb-dropdown-menu" role="listbox" id="meilisearch-autocomplete-listbox-0" style="position: absolute; top: 100%; z-index: 100; display: none; left: 0px; right: auto;">
                  <div class="dsb-dataset-1"></div>
                </span>
              </span></div>
          </form>
          <nav class="nav-links can-hide">
            <div class="nav-item"><a href="#" class="nav-link">
                Trang chủ
              </a></div>
            <div class="nav-item"><a href="#" class="nav-link">
                Liên hệ
              </a></div>
            <div class="nav-item"><a href="#" rel="noopener noreferrer" class="nav-link external">
                Học online
                <span><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" x="0px" y="0px" viewBox="0 0 100 100" width="15" height="15" class="icon outbound">
                    <path fill="currentColor" d="M18.8,85.1h56l0,0c2.2,0,4-1.8,4-4v-32h-8v28h-48v-48h28v-8h-32l0,0c-2.2,0-4,1.8-4,4v56C14.8,83.3,16.6,85.1,18.8,85.1z"></path>
                    <polygon fill="currentColor" points="45.7,48.7 51.3,54.3 77.2,28.5 77.2,37.2 85.2,37.2 85.2,14.9 62.8,14.9 62.8,22.9 71.5,22.9"></polygon>
                  </svg> <span class="sr-only">(opens new window)</span></span></a></div> <!---->
          </nav>
        </div>
      </header>
      <aside class="sidebar">
        <div class="carbon-ads" style="text-align: center; margin-top: 15px"><a>Đơn vị tài trợ cho dự án</a> <a href="https://gxd.edu.vn" target="_blank"></a></div>
        <nav class="nav-links">
          <div class="nav-item"><a href="#" class="nav-link">
              Trang chủ
            </a></div>
          <div class="nav-item"><a href="#" class="nav-link">
              Liên hệ
            </a></div>
          <div class="nav-item"><a href="#" rel="noopener noreferrer" class="nav-link external">
              Học online
              <span><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" x="0px" y="0px" viewBox="0 0 100 100" width="15" height="15" class="icon outbound">
                  <path fill="currentColor" d="M18.8,85.1h56l0,0c2.2,0,4-1.8,4-4v-32h-8v28h-48v-48h28v-8h-32l0,0c-2.2,0-4,1.8-4,4v56C14.8,83.3,16.6,85.1,18.8,85.1z"></path>
                  <polygon fill="currentColor" points="45.7,48.7 51.3,54.3 77.2,28.5 77.2,37.2 85.2,37.2 85.2,14.9 62.8,14.9 62.8,22.9 71.5,22.9"></polygon>
                </svg> <span class="sr-only">(opens new window)</span></span></a></div>
        </nav>
        <ul class="sidebar-links">
          <li>
            <section class="sidebar-group collapsable depth-0">
              <p class="sidebar-heading open"><span>Bài trắc nghiệm CC đấu thầu</span> <span class="arrow down"></span></p>
              <ul class="sidebar-links sidebar-group-items">
                <li v-for="(baithi,index) in cauhoi" @click="selectBaithi(index)"><a href="javascript:void(0)" class="sidebar-link" :class="{'text-green':activeQuestion==index}">Bài thi trắc nghiệm số @{{index+1}}</a></li>
              </ul>
            </section>
          </li>
        </ul>
      </aside>
      <main class="page">
        <div class="theme-default-content content__default">
          <p><strong>
              <div style="font-size: 22px; text-align: center;">HỆ THỐNG DUY TRÌ BỞI ...</div>
            </strong> <strong>
              <div style="text-align: center; font-style: italic;">Đề thi giúp bạn ngồi đâu cũng ôn thi Chứng chỉ hành nghề đấu thầu, cũng như ôn luyện kiến thức về đấu thầu và lựa chọn nhà thầu. Dùng Máy tính hoặc Điện thoại đọc và chạm để chọn đáp án.</div>
            </strong></p>
          <div style="text-align: center; font-weight: 700; margin: 0px 0px 10px;">Để ghi danh lớp ôn thi Chứng chỉ hành nghề đấu thầu<br>Liên hệ ...</div>
          <p><strong>
              <div style="text-align: center; font-size: 22px;">@{{currentBaithi.name}}</div>
            </strong></p>
          <div>
            <div>

              <div v-for="(question,index) in currentBaithi.cau_hoi" class="pb-5">
                <div class="wrap-item-list" :class="{'text-red':question.isCorrect===1,'text-green':question.isCorrect===2,'disabled':question.isCorrect}">
                  <div><b>Câu hỏi @{{index+1}}: @{{question.name}}:</b></div>
                  <div v-if="question.dap_an_a"><label @click="chooseAnswer(index,'A')">
                      <input v-model="question.answer" type="radio" value="A">
                      a. @{{question.dap_an_a}}
                    </label></div>
                  <div v-if="question.dap_an_b"><label @click="chooseAnswer(index,'B')">
                      <input v-model="question.answer" type="radio" value="B">
                      b. @{{question.dap_an_b}}
                    </label></div>
                  <div v-if="question.dap_an_c"><label @click="chooseAnswer(index,'C')">
                      <input v-model="question.answer" type="radio" value="C">
                      c. @{{question.dap_an_c}}
                    </label></div>
                  <div v-if="question.dap_an_d"><label @click="chooseAnswer(index,'D')">
                      <input v-model="question.answer" type="radio" value="D">
                      d. @{{question.dap_an_d}}
                    </label></div>
                  @{{question.answerNotes}}
                </div>
              </div>
            </div>
          </div>
          <p><strong><em>Ghi chú:</em></strong></p>
          <ul>
            <li>
              <p>Trên đây để thi chỉ để 25 câu để bạn ôn luyện. Khi dự thi thực tế sẽ: Thi viết trong thời gian 180 phút và thi trắc nghiệm trong thời gian 60 phút.</p>
            </li>
            <li>
              <p>Thi viết: Bạn có 120 phút để làm đề thi có 10 câu.</p>
            </li>
            <li>
              <p>Thi trắc nghiệm: trong 60 phút bạn phải hoàn thành bài có 90 câu trắc nghiệm. Điểm 50/100 là đạt.</p>
            </li>
            <li>
              <p>Phải phản xạ rất nhanh thì mới kịp thời gian. Thi trắc nghiệm tra tài liệu sẽ không kịp. Hãy luyện tập thật nhiều các bài trắc nghiệm ở đây thành phản xạ. Bạn tự chủ động đọc đề và chọn đáp án đúng, kiểu gì cũng vượt qua. Còn phụ thuộc tra tài liệu có thể sẽ không qua vì hết thời gian.</p>
            </li>
          </ul>
          <p>👉 <strong>Call/Zalo ...</strong></p>
        </div>
        <footer class="page-edit">
          <div class="last-updated"><span class="prefix">Last Updated:</span> <span class="time">@{{formatDate(latestUpdate)}}</span></div>
        </footer>
        <div class="page-nav">
          <p class="inner">
            <span v-if="activeQuestion>0" class="prev" @click="selectBaithi(--activeQuestion)">
              ←
              <a href="javascript:void(0)" class="prev">
                Bài thi trắc nghiệm số @{{activeQuestion}}
              </a></span>
            <span v-if="activeQuestion < cauhoi.length -1" class="next" @click="selectBaithi(++activeQuestion)"><a href="javascript:void(0)">
                Bài thi trắc nghiệm số @{{activeQuestion+2}}
              </a>
              →
            </span>
          </p>
        </div>
      </main>
    </div>
    <div class="global-ui"></div>
  </div>
</body>
<script>
  var app = new Vue({
    el: '#app',
    data: {
      cauhoi: cauhoi,
      activeMenu: false,
      activeQuestion: 0,
      currentBaithi: cauhoi[0],
      wrongAnswerNote: danh_gia_dap_an_sai,
      correctAnswerNote: danh_gia_dap_an_dung
    },
    mounted: function() {},
    computed: {
		latestUpdate(){
			let listCauhoi = this.currentBaithi.cau_hoi;
			if(listCauhoi.length){
				listCauhoi.sort(function(a, b) {
				  var keyA = new Date(a.updated_at),
					keyB = new Date(b.updated_at);
				  // Compare the 2 dates
				  if (keyA < keyB) return 1;
				  if (keyA > keyB) return -1;
				  return 0;
				});
				console.log(listCauhoi[0]);
				return listCauhoi[0].updated_at;
			}
		}
	},
    methods: {
      openMenu() {
        if(this.activeMenu){
          this.activeMenu = false;
          return false;
        }
        setTimeout(function() {
          app.activeMenu = true;
        }, 100);
      },
      closeMenu() {
        if (this.activeMenu) {
          this.activeMenu = false;
        }
        return;
      },
      selectBaithi(index) {
        this.activeQuestion = index;
        this.currentBaithi = {...this.cauhoi[index]};
      },
      chooseAnswer(question, answer) {
        this.currentBaithi.cau_hoi[question]['answer'] = answer;
        if (answer == this.currentBaithi.cau_hoi[question].dap_an_dung) {
          this.currentBaithi.cau_hoi[question]['isCorrect'] = 2;
          this.currentBaithi.cau_hoi[question].answerNotes = this.getCorrectMessage();
        } else {
          this.currentBaithi.cau_hoi[question]['isCorrect'] = 1;
          this.currentBaithi.cau_hoi[question].answerNotes = this.getWrongMessage();
        }
        this.currentBaithi = {
          ...this.currentBaithi
        }
        console.log(this.currentBaithi.cau_hoi[question])
      },
      formatDate(date){
        return new Date(date).toLocaleString('vi');
      },
      getCorrectMessage() {
        return this.correctAnswerNote[Math.floor(Math.random() * this.correctAnswerNote.length)];
      },
      getWrongMessage() {
        return this.wrongAnswerNote[Math.floor(Math.random() * this.wrongAnswerNote.length)];
      }
    },
    watch: {},
  });
</script>

@include('frontend.bai-thi.partials.style');

</html>